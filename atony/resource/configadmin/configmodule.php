<? 
include("config_security.php"); 

// Khai bao bien
$record_id 		= getValue("record_id", "arr", "POST", array());
$iQuick 			= getValue("iQuick","str","POST","");

$field_id		= "mod_id";
$fs_table		= "tbl_modules";
$errorMsg 		= "";

$myform = new generate_form();
$myform->add("mod_name","mod_name",0,0,"",0,"",0,"Bạn chưa nhập tiêu đề bài viết");
$myform->add("mod_path","mod_path",0,0,"",0,"",0,"");
$myform->add("mod_listname","mod_listname",0,0,"",0,"",0,"");
$myform->add("mod_listfile","mod_listfile",0,0,"",0,"",0,"");
$myform->add("mod_order","mod_order",1,0,0,0,"",0,"Bạn chưa nhập ngày đăng tin");
$myform->addTable($fs_table);

if ($iQuick == "update"){
	$errorMsg  = '';
	$errorMsg .= $myform->checkdata();
	if($errorMsg == ""){
		//echo $myform->generate_insert_SQL();
		$db_ex = new db_execute($myform->generate_insert_SQL());
		unset($db_ex);
		//Hien thi loi
	}
	redirect($_SERVER['REQUEST_URI']);
}

$myform->addFormname("add_new");
$myform->evaluate();
$myform->checkjavascript();
?>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?=$load_header?>
</head>
<body>
<? template_top("Quản trị module")?>

<form action="<?=$_SERVER['REQUEST_URI']?>" method="post">
	<input type="hidden" name="iQuick" value="update">
	<div style="border:solid 1px #DDD; padding: 1px; width:650px; margin:auto">
	<div class=" bgmenu"><?=tt("Thêm mới module")?></div>
		<table cellpadding="5" cellspacing="5" width="100%">
			<tr>
				<td colspan="2"></td>
			</tr>
			<tr>
				<td nowrap align="right"><?=tt("Tên module")?> : </td>
				<td><input type="text" class="form" name="mod_name" id="mod_name" value="<?=$mod_name?>"></td>
			</tr>
			<tr>
				<td nowrap align="right"><?=tt("Thư mục")?> : </td>
				<td><input type="text" class="form" name="mod_path" id="mod_path" value="<?=$mod_path?>"></td>
			</tr>
			<tr>
				<td nowrap align="right"><?=tt("Thứ tự")?> : </td>
				<td><input type="text" class="form" name="mod_order" id="mod_order" value="<?=$mod_order?>"></td>
			</tr>
			<tr>
				<td nowrap align="right"><?=tt("Tiêu đề")?> : </td>
				<td><input type="text" class="form" name="mod_listname" id="mod_listname" value="<?=$mod_listname?>" size="100"></td>
			</tr>
			<tr>
				<td nowrap align="right"><?=tt("URL file")?> : </td>
				<td><input type="text" class="form" name="mod_listfile" id="mod_listfile" value="<?=$mod_listfile?>" size="100"></td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td>
					<input type="submit" value="Cập nhật" class="form_button">
					<input type="reset" value="Làm lại" class="form_button">
				</td>
			</tr>
		</table>
	</div>
</form>

<form action="quickeditmodule.php" method="post">
<input type="hidden" name="iQuick" value="update">
<table cellpadding="5" cellspacing="0" align="left" width="100%" border="1" bordercolor="#EFEFEF" style="border-collapse:collapse">
<?
$db_module = new db_query("SELECT * FROM tbl_modules ORDER BY mod_order ASC");
$i=0;
while($mod = mysql_fetch_assoc($db_module->result)){
	if(file_exists("../../modules/" . $mod["mod_path"] . "/config_security.php")){
		require_once("../../modules/" . $mod["mod_path"] . "/config_security.php");
      $i++;
      ?>
		<tbody>
			<tr <? if(intval($mod["mod_id"]) != intval($module_id)){?> bgcolor="#FFA500"<? }else{?> bgcolor="#EEE"<? }?>>
				<td style="font-weight:bold" width="20"><?=$i?> </td>
				<td>
					<div style="color:#FF0000; font-size:20px">
						<?=$mod["mod_name"]?>&nbsp;
						<input type="image" src="../images/save.png" style="width:20px; height:20px;">&nbsp;
						<img align="texttop" border="0" align="baseline" src="<?=$fs_imagepath?>delete.png" alt="DELETE" onClick="if (confirm('Bạn muốn xóa?')) { window.location.href='deletemodule.php?record_id=<?=$mod["mod_id"];?>&returnurl=<?=base64_encode(getURL())?>'}" style="cursor:pointer">
					</div>
				</td>
			</tr>
		</tbody>
		<tbody id="list_<?=$mod["mod_id"]?>">
			<tr>
				<td></td>
				<td colspan="3">
					<input type="hidden" name="record_id[]" value="<?=$mod["mod_id"]?>">
					<table cellpadding="5" cellspacing="5">
						<tr>
							<td nowrap align="right"><?=tt("Module id")?> : </td>
							<td><?=$mod["mod_id"]?></td>
						</tr>
						<tr>
							<td nowrap align="right"><?=tt("Tên module")?> : </td>
							<td><input type="text" class="form" name="mod_name<?=$mod["mod_id"]?>" id="mod_name<?=$mod["mod_id"]?>" value="<?=$mod["mod_name"]?>"></td>
						</tr>
						<tr>
							<td nowrap align="right"><?=tt("Thư mục")?> : </td>
							<td><input type="text" class="form" name="mod_path<?=$mod["mod_id"]?>" id="mod_path<?=$mod["mod_id"]?>" value="<?=$mod["mod_path"]?>"></td>
						</tr>
						<tr>
							<td nowrap align="right"><?=tt("Thứ tự")?> : </td>
							<td><input type="text" class="form" name="mod_order<?=$mod["mod_id"]?>" id="mod_order<?=$mod["mod_id"]?>" value="<?=$mod["mod_order"]?>"></td>
						</tr>
						<tr>
							<td nowrap align="right"><?=tt("Tiêu đề")?> : </td>
							<td><input type="text" class="form" name="mod_listname<?=$mod["mod_id"]?>" id="mod_listname<?=$mod["mod_id"]?>" value="<?=$mod["mod_listname"]?>" size="100"></td>
						</tr>
						<tr>
							<td nowrap align="right"><?=tt("URL file")?> : </td>
							<td><input type="text" class="form" name="mod_listfile<?=$mod["mod_id"]?>" id="mod_listfile<?=$mod["mod_id"]?>" value="<?=$mod["mod_listfile"]?>" size="100"></td>
						</tr>
					</table>
				</td>
			</tr>
		</tbody>
		<?
		unset($module_id);
     
	} // End if(file_exists("../../modules/" . $mod["mod_path"] . "/config_security.php"))
     
} // End while($mod=mysql_fetch_assoc($db_module->result))
?>
</table>
</form>
<? template_bottom() ?>	
</body>
</html>
