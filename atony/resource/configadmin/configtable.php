<? 
include("config_security.php"); 
$fs_redirect	= "listingtable.php";
//Call Class generate_form();
$myform = new generate_form();
//Loại bỏ chuc nang thay the Tag Html
$myform->removeHTML(0);

//Warning Error!
$errorMsg = "";
$myform->add("conf_table","conf_table",0,0,"",0,"",0,"");
$myform->add("conf_field","conf_field",0,0,"",1,tt("Please enter field name"),0,"");
$myform->add("conf_type","conf_type",0,0,"",0,"",0,"");
$myform->add("conf_type_form","conf_type_form",1,0,0,0,"",0,"");
$myform->add("conf_show","conf_show",1,0,0,0,"",0,"");
$myform->add("conf_form_width","conf_form_width",1,0,0,1,tt("Please enter length name"),0,"");
$myform->add("conf_length","conf_length",0,0,"",0,"",0,"");
$myform->add("conf_default","conf_default",0,0,"",0,"",0,"");
$myform->add("conf_title","conf_title",0,0,"",1,tt("Please enter label name"),0,"");
$myform->add("conf_field_form","conf_field_form",0,0,"",1,tt("Please enter from name"),0,"");
$myform->add("conf_data_store","conf_data_store",1,0,0,0,"",0,"");
$myform->add("conf_type_value","conf_type_value",1,0,0,0,"",0,"");
$myform->add("conf_data_require","conf_data_require",1,0,0,0,"",0,"");
$myform->add("conf_data_error_message","conf_data_error_message",0,0,"",0,"",0,"");
$myform->add("conf_data_unique","conf_data_unique",1,0,0,0,"",0,"");
$myform->add("conf_data_error_message2","conf_data_error_message2",0,0,"",0,"",0,"");
$myform->add("conf_show_listing","conf_show_listing",1,0,0,0,"",0,"");
$myform->add("conf_order","conf_order",1,0,0,0,"",0,"");
//Add table
$myform->addTable($fs_table);
//Get Action.
$action	= getValue("action", "str", "POST", "");
if($action == "insert"){
	//Check Error!
	$errorMsg .= $myform->checkdata();
	if($errorMsg == ""){
		$db_ex = new db_execute($myform->generate_insert_SQL());
		//Redirect to:
		redirect($fs_redirect);
		exit();
	}
}

//add form for javacheck
$myform->addFormname("add_new");
//add more javacode
$myform->evaluate();

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="<?=$fs_stype_css?>" rel="stylesheet" type="text/css">
<link href="<?=$fs_template_css?>" rel="stylesheet" type="text/css"> 
<?=$myform->checkjavascript();?>
</head>
<body topmargin="0" bottommargin="0" leftmargin="0" rightmargin="0" bgcolor="#F9F9F9">
<?=template_tab($arrayTab)?>
	<? /*---------Body------------*/ ?>
<form ACTION="<?=$_SERVER["REQUEST_URI"]?>" METHOD="POST" name="add_new" enctype="multipart/form-data">
	<table border="0" cellpadding="5" cellspacing="0">
			<tr> 
				<td align="right" nowrap class="textBold"><?=tt("Choose table name")?>:</td>
				<td>
					<select name="conf_table" id="conf_table" class="form">
						<?
						foreach($arrayTable as $key=>$value){
						?>
							<option value="<?=$key?>" <? if($key==$conf_table) echo 'selected';?>><?=$value["table"]?></option>
						<?
						}
						?>
					</select>
				</td>
			</tr>
			<tr> 
				<td align="right" nowrap class="textBold"><?=tt("Field name")?>:</td>
				<td><input type="text" name="conf_field" id="conf_field" value="<?=$conf_field?>" size="30" class="form"></td>
			</tr>
			<tr> 
				<td align="right" nowrap class="textBold"><?=tt("Type data")?>:</td>
				<td>
					<select name="conf_type" id="conf_type" class="form">
						<?
						foreach($arrayType as $key=>$value){
						?>
							<option value="<?=$key?>" <? if($key==$conf_type) echo 'selected';?>><?=$key?></option>
						<?
						}
						?>
					</select>
			</tr>
			<tr> 
				<td align="right" nowrap class="textBold"><?=tt("Length form")?>:</td>
				<td>
					<input type="text" name="conf_form_width" id="conf_form_width" value="<?=$conf_form_width?>" size="10" class="form">
					px
				</td>
			</tr>
			<tr> 
				<td align="right" nowrap class="textBold"><?=tt("Length/Values")?>:</td>
				<td><input type="text" name="conf_length" id="conf_length" value="<?=$conf_length?>" size="10" class="form"></td>
			</tr>
			<tr> 
				<td align="right" nowrap class="textBold"><?=tt("Default")?>:</td>
				<td><input type="text" name="conf_default" id="conf_default" value="<?=$conf_default?>" size="10" class="form"></td>
			</tr>
			<tr> 
				<td align="right" nowrap class="textBold"><?=tt("Label")?>:</td>
				<td><input type="text" name="conf_title" id="conf_title" value="<?=$conf_title?>" size="10" class="form"></td>
			</tr>
			<tr> 
				<td align="right" nowrap class="textBold"><?=tt("Form name")?>:</td>
				<td><input type="text" name="conf_field_form" id="conf_field_form" value="<?=$conf_field_form?>" size="10" class="form"></td>
			</tr>
			<tr> 
				<td align="right" nowrap class="textBold"><?=tt("Data store")?>:</td>
				<td>
					<?=tt("Get from form")?>: <input type="radio" name="conf_data_store" id="conf_data_store" <? if($conf_data_store==0) echo 'checked';?> value="0" class="form"> &nbsp;&nbsp;
					<?=tt("Get from variable")?>: <input type="radio" name="conf_data_store" id="conf_data_store" <? if($conf_data_store==1) echo 'checked';?> value="1" class="form">
				</td>
			</tr>
			<tr> 
				<td align="right" nowrap class="textBold"><?=tt("Form type")?>:</td>
				<td>
					<select name="conf_type_value" id="conf_type_value" class="form">
						<?
						foreach($arrayTypeValue as $key=>$value){
						?>
							<option value="<?=$key?>" <? if($key==$conf_type_value) echo 'selected';?>><?=$value?></option>
						<?
						}
						?>
					</select>
			</tr>
			<tr> 
				<td align="right" nowrap class="textBold"><?=tt("Form show")?>:</td>
				<td>
					<select name="conf_show" id="conf_show" class="form">
						<?
						foreach($arrayTypeShow as $key=>$value){
						?>
							<option value="<?=$key?>" <? if($key==$conf_show) echo 'selected';?>><?=$value?></option>
						<?
						}
						?>
					</select>
			</tr>         
			<tr> 
				<td align="right" nowrap class="textBold"><?=tt("Data require")?>:</td>
				<td><input type="checkbox" name="conf_data_require" id="conf_data_require" <? if($conf_data_require==1) echo 'checked';?> value="1" class="form"></td>
			</tr>
			<tr> 
				<td align="right" nowrap class="textBold"><?=tt("Msgbox error message")?>:</td>
				<td><input type="text" name="conf_data_error_message" id="conf_data_error_message" value="<?=$conf_data_error_message?>" size="50" class="form"></td>
			</tr>
			<tr> 
				<td align="right" nowrap class="textBold"><?=tt("Only one")?>:</td>
				<td><input type="checkbox" name="conf_data_unique" id="conf_data_unique" <? if($conf_data_unique==1) echo 'checked';?> value="1" class="form"></td>
			</tr>
			<tr> 
				<td align="right" nowrap class="textBold"><?=tt("Msgbox error message")?>:</td>
				<td><input type="text" name="conf_data_error_message2" id="conf_data_error_message2" value="<?=$conf_data_error_message2?>" size="50" class="form"></td>
			</tr>
			<tr> 
				<td align="right" nowrap class="textBold"><?=tt("Show listing")?>:</td>
				<td><input type="checkbox" name="conf_show_listing" id="conf_show_listing" <? if($conf_show_listing==1) echo 'checked';?> value="1" class="form"></td>
			</tr>
			<tr> 
				<td align="right" nowrap class="textBold"><?=tt("Order")?>:</td>
				<td><input type="text" name="conf_order" id="conf_order" value="<?=$conf_order?>" class="form"></td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td>
              <input type="button" class="form" value="<?=tt("Add new record")?>" style="cursor:hand; width:150px" onClick="validateForm();">&nbsp;
              <input type="reset" class="form" value="<?=tt("Reset")?>" style="cursor:hand; width:100px">
              <input type="hidden" name="active" value="1">
              <input type="hidden" name="action" value="insert">
				</td>
			</tr>
		</table>
	</form>
   <? /*---------Body------------*/ ?>
<?=template_bottom() ?>	
</body>
</html>