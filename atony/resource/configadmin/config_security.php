<?
require_once("../security/security.php");
$arrayTab = array(tt("Config menu")=>"configmodule.php"
					   ,tt("Config module")=>"configtable.php"
						,tt("Listing field in table")=>"listingtable.php"
						);
$arrayType 	= array(
                "VARCHAR"=>"CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT "
                ,"TINYINT"=>" NULL DEFAULT "
                ,"TEXT"=>"CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT "
                ,"SMALLINT"=>" NULL DEFAULT "
                ,"MEDIUMINT"=>" NULL DEFAULT "
                ,"INT"=>" NULL DEFAULT "
                ,"BIGINT"=>" NULL DEFAULT "
                ,"FLOAT"=>" NULL DEFAULT "
                ,"DOUBLE"=>" NULL DEFAULT "
                ,"CHAR"=>"CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT "
                ,"MEDIUMBLOB"=>"CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT "
                ,"MEDIUMTEXT"=>"CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT "
                ,"LONGBLOB"=>"CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT "
                ,"LONGTEXT"=>"CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT "
);
$arrayTypeForm 	= array(0=>"Không hiển thị",1=>"Textbox",2=>"Combobox",3=>"Checkbox",4=>"Radio",5=>"Textarea",6=>"Editor",7=>"Ảnh");
$arrayTypeValue 	= array(0=>"Kiểu text, kiểu varchar",1=>"Kiểu số nguyên",2=>"Kiểu email",3=>"Kiểu số thực (giá..vv)",4=>"Kiểu mật khẩu");						
$arrayTypeShow 	= array(0=>"Kiểu textbox",1=>"Editor");						
$fs_table			= "tbl_config_table";
$arrayTable = array(
							"poetries"=>array("id"=>"poe_id","table"=>"Poetry")
							,"thisi"=>array("id"=>"thi_id","table"=>"Poet")
							,"statics"=>array("id"=>"sta_id","table"=>"Statics page")
						);

?>