$(document).ready(function()
{
    //Proccessing bar
    $("#loadingBar").ajaxSend(function() {
        $(this).show();
    }).ajaxComplete(function() {
        $(this).fadeOut("fast");
    });
    //End
    
	//Publish
    $('.publish, .featured, .frontpage, .active, .approve, .show, .hot , .new , .popular, .daily, .km, .home, .follow, .phongthuy').click(function()
    {
        var $object = $(this);
        var $id     = $object.attr('id');
        var $task   = $object.attr('class');
        var $path   = $object.attr('path');
        var $state  = $object.attr('name');
        var $control = $('input.controller').val();
        if( !$control ){
            $control = 'controllers';
        }
        
        $.ajax({
            type: 'POST',
            url: $control+'.php',
            data: { 'task' : $task, 'id': $id, 'state' : $state },
            dataType: 'json',
            success: function(msg)
            {   
                if( msg.error=='' )
                {
                    //alert(msg.mess);
                    if($state==1){
                        $object.attr('src', $path+'active_0.png').attr('name', '0');
                    }else{
                        $object.attr('src', $path+'active_1.png').attr('name', '1');   
                    }
                }
                else
                {
                    alert(msg.error); return;
                }
            }
        });
    })
    //End
	
	//Remove
    $('.remove').click(function()
    {
        if( !confirm('Bạn chắc chắn muốn xóa?') ){
            return false;    
        }
        
        var $object  = $(this);
        var $id      = $object.attr('id');
        var $control = $('input.controller').val();
        if( !$control ){
            $control = 'controllers';
        }
        
        $.ajax({
            type: 'POST',
            url: $control+'.php',
            data: { 'task' : 'remove', 'id': $id },
            dataType: 'json',
            success: function(msg)
            {
                if( msg.error=='' )
                {
					
                    $('tr.row-'+$id).fadeOut(600,function() {
                        $(this).remove();
                        if( parseInt($('.item-row').length) <= 0 ){
                            window.location.reload();
                        }
                    });
					
					//alert(msg.mess); return;
                }
                else
                {
                    alert(msg.error); return;
                }
            }
        });
        return true;
    })

    $('.remove_reply').click(function()
    {
        if( !confirm('Bạn chắc chắn muốn xóa?') ){
            return false;    
        }
        
        var $object  = $(this);
        var $id      = $object.attr('id');
        var $control = $('input.controller').val();
        if( !$control ){
            $control = 'controllers';
        }
        
        $.ajax({
            type: 'POST',
            url: $control+'.php',
            data: { 'task' : 'remove_reply', 'id': $id },
            dataType: 'json',
            success: function(msg)
            {
                if( msg.error=='' )
                {
                    
                    $('tr.row-'+$id).fadeOut(600,function() {
                        $(this).remove();
                        if( parseInt($('.item-row').length) <= 0 ){
                            window.location.reload();
                        }
                    });
                    
                    //alert(msg.mess); return;
                }
                else
                {
                    alert(msg.error); return;
                }
            }
        });
        return true;
    })
    //End
});