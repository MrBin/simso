function addCommas(nStr) {
	nStr += '';
	x = nStr.split('.');
	x1 = x[0];
	x2 = x.length > 1 ? '.' + x[1] : '';
	var rgx = /(\d+)(\d{3})/;
	while (rgx.test(x1)) {
		x1 = x1.replace(rgx, '$1' + '.' + '$2');
	}
	return x1;
}

function formatCurrency(div_id, str_number) {
	/*Convert tu 1000->1.000*/
	var mynumber = 1000;
	str_number = str_number.replace(/\./g, "");
	document.getElementById(div_id).innerHTML = addCommas(str_number);
}

// JavaScript Document
function MM_findObj(n, d) { //v4.01
	var p, i, x;
	if (!d) d = document;
	if ((p = n.indexOf("?")) > 0 && parent.frames.length) {
		d = parent.frames[n.substring(p + 1)].document;
		n = n.substring(0, p);
	}
	if (!(x = d[n]) && d.all) x = d.all[n];
	for (i = 0; !x && i < d.forms.length; i++) x = d.forms[i][n];
	for (i = 0; !x && d.layers && i < d.layers.length; i++) x = MM_findObj(n, d.layers[i].document);
	if (!x && d.getElementById) x = d.getElementById(n);
	return x;
}

function MM_validateForm() { //v4.0
	var i, p, q, nm, test, num, min, max, errors = '',
		args = MM_validateForm.arguments;
	for (i = 0; i < (args.length - 2); i += 3) {
		test = args[i + 2];
		val = MM_findObj(args[i]);
		if (val) {
			if (val.title != "") {
				nm = val.title;
			} else {
				nm = val.name;
			}
			if ((val = val.value) != "") {
				if (test.indexOf('isEmail') != -1) {
					p = val.indexOf('@');
					if (p < 1 || p == (val.length - 1)) errors += '- "' + nm + '" ph?i l� m?t d?a ch? email.\n';
				} else if (test != 'R') {
					num = parseFloat(val);
					if (isNaN(val)) errors += '- "' + nm + '" ph?i l� m?t s?.\n';
					if (test.indexOf('inRange') != -1) {
						p = test.indexOf(':');
						min = test.substring(8, p);
						max = test.substring(p + 1);
						if (num < min || max < num) errors += '- "' + nm + '" ph?i l� s? n?m gi?a ' + min + ' v� ' + max + '.\n';
					}
				}
			} else if (test.charAt(0) == 'R') errors += '- B?n chua nh?p "' + nm + '".\n';
		}
	}
	if (errors) alert('C� nh?ng l?i sau:\n' + errors);
	document.MM_returnValue = (errors == '');
}

function changeTypeForm(id) {
	var valuetext = document.getElementById('pro_des_' + id).value;
	document.getElementById('span_' + id).innerHTML = '';
	var element = document.getElementById('span_' + id);
	var image = document.getElementById('image_' + id).src;
	var imagename = image.indexOf('1.png');
	if (imagename != -1) {
		var newElement = document.createElement('textarea');
		newElement.cols = 72;
		newElement.rows = 6;
		newElement.className = 'form';
		newElement.name = 'pro_des_' + id;
		newElement.id = 'pro_des_' + id;
		newElement.value = valuetext;
		element.appendChild(newElement);
		document.getElementById('image_' + id).src = '../images/0.png';
	} else {
		var newElement = document.createElement('input');
		newElement.type = 'text';
		newElement.className = 'form';
		newElement.name = 'pro_des_' + id;
		newElement.id = 'pro_des_' + id;
		newElement.value = valuetext;
		newElement.style.width = '400px';
		element.appendChild(newElement);
		document.getElementById('image_' + id).src = '../images/1.png';
	}
}

function changeTypePro(t, id) {
	var valuetext = document.getElementById('pro_' + t + '_' + id).value;
	document.getElementById('span_' + t + '_' + id).innerHTML = '';
	var element = document.getElementById('span_' + t + '_' + id);
	var image = document.getElementById('image_' + t + '_' + id).src;
	var imagename = image.indexOf('1.png');
	if (imagename != -1) {
		var newElement = document.createElement('textarea');
		newElement.style.width = '400px';
		newElement.rows = 6;
		newElement.className = 'form';
		newElement.name = 'pro_' + t + '_' + id;
		newElement.id = 'pro_' + t + '_' + id;
		newElement.value = valuetext;
		element.appendChild(newElement);
		document.getElementById('image_' + t + '_' + id).src = '../images/0.png';
	} else {
		var newElement = document.createElement('input');
		newElement.type = 'text';
		newElement.className = 'form';
		newElement.name = 'pro_' + t + '_' + id;
		newElement.id = 'pro_' + t + '_' + id;
		newElement.value = valuetext;
		newElement.style.width = '400px';
		element.appendChild(newElement);
		document.getElementById('image_' + t + '_' + id).src = '../images/1.png';
	}
}

function changethuoctinh(key, value) {
	document.getElementById('category_thuoctinh_' + key).style.height = (parseInt(value) * 23) + 'px';
	for (i = 1; i < 50; i++) {
		if (i > parseInt(value)) {
			document.getElementById('cat_' + key + '_' + i).value = '';
		}
	}
}

function check_all(start_loop, end_loop) {
	for (i = start_loop; i <= end_loop; i++) {
		try {
			document.getElementById("check_" + i).checked = true;
			document.getElementById("tr_" + i).className = "on_check";
		} catch (e) {}
	}
}

function uncheck_all(start_loop, end_loop) {
	for (i = start_loop; i <= end_loop; i++) {
		try {
			document.getElementById("check_" + i).checked = false;
			document.getElementById("tr_" + i).className = "on_uncheck";
		} catch (e) {}
	}
}

function change_bg(check_id, object_id) {
	if (document.getElementById(check_id).checked == true) {
		document.getElementById(object_id).className = "on_check";
	} else {
		document.getElementById(object_id).className = "on_uncheck";
	}
}

function creat_link(object) {
	window.open("../link/selected.php?object=" + object, "", "height=600,width=700,menubar=0,resizable=1,scrollbars=1,statusbar=0,titlebar=0,toolbar=0");
}

function Setcode(object, path, char) {

	var pos = path.lastIndexOf(char);
	var pos2 = path.lastIndexOf(".");

	if (pos2 > -1)
		fileName = path.substring(pos + 1, pos2);
	else
		fileName = path.substring(pos + 1);
	document.getElementById(object).value = fileName;
}

function check(start_loop, end_loop) {
	if (document.all.check_all.checked == false) {
		uncheck_all(start_loop, end_loop);
	} else {
		check_all(start_loop, end_loop);
	}
}

function showhide(divid) {
	var tinhtrang = 'none';
	tinhtrang = document.getElementById(divid);
	if (tinhtrang.style.display == 'none') {
		tinhtrang.style.display = 'block';
	} else {
		tinhtrang.style.display = 'none';
	}
}

function submitAll(formname, iCat, id) {
	document.getElementById("record_" + iCat + "_" + id).checked = true;
	document.getElementById(formname).submit();
}

function changeCheck(iCat, id) {
		document.getElementById("record_" + iCat + "_" + id).checked = true;
	}
	// JavaScript Document
var isopen = 0;
var widthLeft = 200;

function change_left() {
	//dong left
	if (isopen == 0) {
		parent.resize_left(0);
		document.all.mybutton.value = ' Hi?n th? thanh menu ';
		isopen = 1;
		widthLeft = 0;
	}
	//mo left
	else {
		parent.resize_left(1);
		document.all.mybutton.value = ' ?n thanh menu ';
		isopen = 0;
		widthLeft = 200;
	}
}

function tabdiv(id, count) {
	for (i = 1; i <= count; i++) {
		try {
			document.getElementById('tabt_' + i).className = 'n';
			document.getElementById('tabc_' + i).style.display = 'none';
		} catch (e) {}
	}
	document.getElementById('tabt_' + id).className = 's';
	document.getElementById('tabc_' + id).style.display = 'block';
}

function tabdiv1(id, count) {
	for (i = 1; i <= count; i++) {
		try {
			document.getElementById('tabg_' + i).className = 'n';
			document.getElementById('tabh_' + i).style.display = 'none';
		} catch (e) {}
	}
	document.getElementById('tabg_' + id).className = 's';
	document.getElementById('tabh_' + id).style.display = 'block';
}


// JavaScript Document
function check_edit(idobj) {
	document.getElementById(idobj).checked = true;
}

function loadactive(obj) {
	obj.innerHTML = 'load...';
	$(obj).load(obj.href + '&ajax=1');
	return false;
}




//Ajax
// JavaScript Document
function makeRequest(url, obj_response, method, parameters) {
	var http_request = false;
	var show_id = document.getElementById(obj_response);
	if (!show_id) {
		alert('Cannot find object response data !');
		return false;
	}
	if (url == "") {
		return false;
	}
	show_id.innerHTML = '<div class="load_data"><img src="../../resource/images/loading.gif" /></div>';
	if (window.XMLHttpRequest) { // Mozilla, Safari,...
		http_request = new XMLHttpRequest();
		if (http_request.overrideMimeType) {
			//set type accordingly to anticipated content type
			http_request.overrideMimeType('text/html');
		}
	} else if (window.ActiveXObject) { // IE
		try {
			http_request = new ActiveXObject('Msxml2.XMLHTTP');
		} catch (e) {
			try {
				http_request = new ActiveXObject('Microsoft.XMLHTTP');
			} catch (e) {}
		}
	}
	if (!http_request) {
		alert('Cannot create XMLHTTP instance');
		return false;
	}

	http_request.onreadystatechange = function() {
		if (http_request.readyState == 4) {
			if (http_request.status == 200) {
				//alert(http_request.responseText);
				show_id.innerHTML = http_request.responseText;
			} else {
				//alert('There was a problem with the request.');
				return false;
			}
		}
	}
	if (method == 'GET') {
		http_request.open('GET', url, true);
		http_request.send(null);
	} else if (method == 'POST') {
		http_request.open('POST', url, true);
		http_request.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
		http_request.setRequestHeader('Content-length', parameters.length);
		http_request.setRequestHeader('Connection', 'close');
		http_request.send(parameters);
	}
}

function submitFormByAjax(file_action, obj_response, method, arrObject) {
	if (typeof(arrObject) !== 'object') {
		alert('Array object is do not match !');
		return false;
	}
	var parameters = '';
	for (i = 0; i < arrObject.length; i++) {
		parameters = parameters + arrObject[i] + '=' + encodeURI(document.getElementById(arrObject[i]).value);
		if (i < arrObject.length - 1) parameters = parameters + '&';
	}
	if (method == 'GET') file_action = file_action + '?' + parameters;
	makeRequest(file_action, obj_response, method, parameters);
}

function load_data(file_action, obj_response) {
	makeRequest(file_action, obj_response, 'GET', '')
}
var offsetfromcursorX = 12;
var offsetfromcursorY = 10;
var offsetdivfrompointerX = 10;
var offsetdivfrompointerY = 14;

document.write('<div id="dhtmltooltip"></div>');
document.write('<img id="dhtmlpointer" src="../../resource/images/tooltiparrow.gif">');

var ie = document.all;
var ns6 = document.getElementById && !document.all;
var enabletip = false;

if (ie || ns6) var tipobj = document.all ? document.all["dhtmltooltip"] : document.getElementById ? document.getElementById("dhtmltooltip") : "";

var pointerobj = document.all ? document.all["dhtmlpointer"] : document.getElementById ? document.getElementById("dhtmlpointer") : "";

function ietruebody() {
	return (document.compatMode && document.compatMode != "BackCompat") ? document.documentElement : document.body;
}

function showtip(thetext, thewidth, thecolor) {
	if (ns6 || ie) {
		if (typeof thewidth != "undefined")
			tipobj.style.width = thewidth + "px";
		if (typeof thecolor != "undefined" && thecolor != "")
			tipobj.style.backgroundColor = thecolor;
		tipobj.innerHTML = thetext;
		enabletip = true;
		return false;
	}
}

function positiontip(e) {
	if (enabletip) {
		var nondefaultpos = false;
		var curX = (ns6) ? e.pageX : event.clientX + ietruebody().scrollLeft;
		var curY = (ns6) ? e.pageY : event.clientY + ietruebody().scrollTop;

		var winwidth = ie && !window.opera ? ietruebody().clientWidth : window.innerWidth - 20;
		var winheight = ie && !window.opera ? ietruebody().clientHeight : window.innerHeight - 20;

		var rightedge = ie && !window.opera ? winwidth - event.clientX - offsetfromcursorX : winwidth - e.clientX - offsetfromcursorX;
		var bottomedge = ie && !window.opera ? winheight - event.clientY - offsetfromcursorY : winheight - e.clientY - offsetfromcursorY;

		var leftedge = (offsetfromcursorX < 0) ? offsetfromcursorX * (-1) : -1000;

		if (rightedge < tipobj.offsetWidth) {
			tipobj.style.left = curX - tipobj.offsetWidth + "px";
			nondefaultpos = true;
		} else if (curX < leftedge)
			tipobj.style.left = "5px";
		else {
			tipobj.style.left = curX + offsetfromcursorX - offsetdivfrompointerX + "px";
			pointerobj.style.left = curX + offsetfromcursorX + "px";
		}

		if (bottomedge < tipobj.offsetHeight) {
			tipobj.style.top = curY - tipobj.offsetHeight - offsetfromcursorY + "px";
			nondefaultpos = true;
		} else {
			tipobj.style.top = curY + offsetfromcursorY + offsetdivfrompointerY + "px";
			pointerobj.style.top = curY + offsetfromcursorY + "px";
		}

		tipobj.style.visibility = "visible";

		if (!nondefaultpos)
			pointerobj.style.visibility = "visible";
		else
			pointerobj.style.visibility = "hidden";
	}
}

function hidetip() {
	if (ns6 || ie) {
		enabletip = false;
		tipobj.style.visibility = "hidden";
		pointerobj.style.visibility = "hidden";
		tipobj.style.left = "-1000px";
		tipobj.style.backgroundColor = '';
		tipobj.style.width = '';
	}
}

document.onmousemove = positiontip;