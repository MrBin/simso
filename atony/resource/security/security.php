<?
error_reporting(0);
@session_start();

#+
#+ Class + function
include('../../../config.php');
include('../../../kernel/classes/fs_database.php');
include('../../../kernel/classes/fs_struc_classified.php');
include('../../../kernel/classes/fs_form.php');
include('../../../kernel/classes/fs_generate_form.php');
include('../../../kernel/classes/fs_generate_quicksearch.php');
include('../../../kernel/classes/fs_upload.php');
include('../../../kernel/classes/fs_menu.php');
include('../../../kernel/classes/fs_generate_table.php');
include('../../../kernel/classes/fs_tag.php');
include('../../../kernel/classes/fs_json.php');
#+
include('../../../kernel/functions/functions_all.php');
include('../../../kernel/functions/functions_file.php');
include('../../../kernel/functions/functions_date.php');
include('../../../kernel/functions/functions_image.php');
include('../../../kernel/functions/functions_translate.php');
include('../../../kernel/functions/functions_pagebreak.php');


require_once("functions.php");
require_once("template.php");
$admin_id = isset($_SESSION["user_id"]) ? intval($_SESSION["user_id"]) : 0;
$lang_id	 = isset($_SESSION["lang_id"]) ? intval($_SESSION["lang_id"]) : 0;

//phan khai bao bien dung trong admin
$fs_stype_css			= "../css/css.css";
$fs_template_css		= "../css/template.css";
$fs_border 				= "#DAE6F6";
$fs_bgtitle 			= "#DBE3F8";
$bordercolor 			= "#EFEFEF";
$bgcolor				= "";
$fs_imagepath 			= "../../resource/images/";
$fs_no_image			= "../../resource/images/noimage.png";
$fs_scriptpath 			= "../../resource/js/";
$wys_path				= "../../resource/wysiwyg_editor/";
$wys_cssadd				= array();
$wys_cssadd				= "/css/all.css";
$lang_path				= "/";
$city_rewrite			= "toan-quoc";
$sqlcategory 			= "";

//phan include file css
$load_header 			= '<link href="../../resource/css/css.css" rel="stylesheet" type="text/css">';
$load_header 			.= '<link href="../../resource/js/jQuery/jquery-ui.custom.css" rel="stylesheet" type="text/css">';

//phan include file script
$load_header 			.= '<script src="../../resource/js/jQuery/jquery.min.js"></script>';
$load_header 			.= '<script src="../../resource/js/jQuery/jquery-ui.custom.min.js"></script>';
$load_header 			.= '<script src="../../resource/js/functions.js" type="text/javascript"></script>';
$load_header 			.= '<script src="../../resource/js/library.js" type="text/javascript"></script>';
$fs_change_bg			= 'onMouseOver="this.style.background=\'#87B6DA\'" onMouseOut="this.style.background=\'#FFF\'"';

//phan ngon ngu admin
$db_language			= new db_query("SELECT tra_text,tra_keyword FROM tbl_translate_admin");
$langAdmin 				= array();
while($row=mysql_fetch_assoc($db_language->result)){
	$langAdmin[$row["tra_keyword"]] = $row["tra_text"];
}

//
// Get config variable from database
$db_config = new db_query("SELECT * " 
	." FROM tbl_config WHERE con_lang_id = " . $lang_id
	);
if($row = mysql_fetch_array($db_config->result)){
	while(list($data_field, $data_value) = each($row)){
		if(!is_int($data_field)){
			$$data_field = $data_value;
		}
	}
}
$db_config->close();
unset($db_config);

//Set time zone
date_default_timezone_set("Asia/Ho_Chi_Minh");

//Quyen dac biet
$checkme =  $_SESSION["userlogin"] == "phongnt";
?>