<?
// Mr.LeToan - NTP.VnJp
require_once("config_security.php");

//Khai bao Bien
$fs_table		= "tbl_admin";
$fs_redirect	= $_SERVER['REQUEST_URI'];
$errorMsgEmail = "";
$action	= getValue("action", "str", "POST", "");

//
$myform = new generate_form();
$myform->removeHTML(0);

//
if($action == "update_email"){

	$faq_question	= getValue("adm_email","str","POST","");
	
	//Insert to database
	$myform->add("adm_email","adm_email",2,0,"",1,tt("Vui lòng nhập email của bạn"),0,"");
	
	//Add table
	$myform->addTable($fs_table);
	
	//Check Error!
	$errorMsgEmail .= $myform->checkdata();
	if($errorMsgEmail == ""){
		$db_ex = new db_execute($myform->generate_update_SQL("adm_id", $admin_id));
		//echo $myform->generate_update_SQL("adm_id", $admin_id);
		echo "<script language='javascript'>alert('" . tt("Thông tin đã được cập nhật") . "');</script>";
		//Redirect to:
		redirect($fs_redirect);
		exit();
	}
}
//add form for javacheck
$myform->addFormname("change_email");

//Change your password ---------------------------------------->
//Get $Errormessage
$Errormessage = "";
//Get Action.
$action	= getValue("action", "str", "POST", "");
if($action == "update_password"){
	$adm_password_old	= getValue("adm_password_old","str","POST","",1);
	$adm_password 		= getValue("adm_password","str","POST","",1);
	//update to database
	$myform->add("adm_password","adm_password",4,0,"",1,tt("Vui lòng nhập password cũ"),0,"");
	//Add table
	$myform->addTable($fs_table);
	//Kiem tra do dai cua mat khau
	$allow_update = 1;
	//check current password
	if (md5($adm_password_old) != $_SESSION["password"]){
		$allow_update = 0;
		$Errormessage .= tt("Passwword cữ không chính xác") . " <br>";
	}
	if (strlen($adm_password) < 6){
		$allow_update = 0;
		$Errormessage .= tt("Password phải có ít nhất 6 ký tự") . " <br>";
	}
	//Check Error!
	if($allow_update == 1){
		$db_ex = new db_execute($myform->generate_update_SQL("adm_id", $admin_id));
		//echo $myform->generate_update_SQL("adm_id", $admin_id);
		$_SESSION["password"] = md5($adm_password);
		echo "<script language='javascript'>alert('"  . tt("Pass word mới đã được cập nhật")  . " !');</script>";
		redirect($fs_redirect);
		exit();
	}
}
//Select data
$db_data = new db_query("SELECT * FROM tbl_admin WHERE adm_id = " . $admin_id);
if (mysql_num_rows($db_data->result) > 0)
{
	$row = mysql_fetch_array($db_data->result);
	$db_data->close();
	unset($db_data);
}
else{
	echo "Không tìm thấy dữ liệu";
	exit();
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Add New</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="<?=$fs_stype_css?>" rel="stylesheet" type="text/css">
<link href="<?=$fs_template_css?>" rel="stylesheet" type="text/css"> 
<script language="javascript">
function check_form_change_password(){
	if (document.getElementById("adm_password_old").value==''){
		alert('<?=tt("Vui lòng nhập pass word cũ")?>');
		return;
	}
	if (document.getElementById("adm_password").value==''){
		alert('<?=tt("Vui lòng nhập pass word mới")?>');
		return;
	}
	if (document.getElementById("adm_password").value.length < 6 ){
		alert('<?=tt("Password mới phải có ít nhất 6 ký tự")?>');
		return;
	}
	if (document.getElementById("adm_password_con").value==''){
		alert('<?=tt("Vui lòng nhập password xác nhận")?>');
		return;
	}
	if (document.getElementById("adm_password_con").value!=document.getElementById("adm_password").value){
		alert('<?=tt("Password cũ và password mới không giống nhau")?> !');
		return;
	}
	document.change_password.submit();
}
</script>
<?
$myform->checkjavascript();
?>
</head>

<body>
<?=template_top(tt("Thay đổi thông tin cá nhân"))?>
<table cellpadding="5" cellspacing="5" width="80%" align="center" style="margin-top:50px;">
	<tr>
		<td width="50%" valign="top">
        	<form ACTION="<?=$_SERVER['SCRIPT_NAME'] . "?" . @$_SERVER['QUERY_STRING'];?>" METHOD="POST" name="change_email" onsubmit="validateForm(); return false;">
            	<div style=" border:solid 1px #DDD; padding: 1px; height:200px;">
                	<div class="bgmenu" ><?=tt("Thay đổi email")?></div>
                    <table border="0" cellpadding="10" cellspacing="10" width="100%">
                    	 <? if($errorMsgEmail != ""){?>
                         <tr>
                              <td colspan="2" align="center" style="color:#FF0000">
                                    <?=$errorMsgEmail?>
                              </td>
                         </tr>
                         <? }?>
                         <tr>
                              <td nowrap="nowrap" align="right"><?=tt("Tên đăng nhập")?> :</td>
                              <td><?=$row["adm_loginname"]?></td>
                         </tr>
                         <tr>
                              <td nowrap="nowrap" align="right"><?=tt("Email")?> :</td>
                              <td><input type="text" name="adm_email" id="adm_email" value="<?=$row["adm_email"]?>" class="form" style="width:200px"></td>
                         </tr>
                         <tr>
                              <td></td>
                              <td>
                                    <input type="submit" class="form_button" value="<?=tt("Cập nhật")?>" style="cursor:hand;">&nbsp;
                                    <input type="reset" class="form_button" value="<?=tt("Làm lại")?>" style="cursor:hand;">
                                    <input type="hidden" name="action" value="update_email">
                              </td>
                         </tr>
                    </table>
				</div>
			</form>
		</td>
		<td width="50%" valign="top">
        	<form ACTION="<?=$_SERVER['SCRIPT_NAME'] . "?" . @$_SERVER['QUERY_STRING'];?>" METHOD="POST" name="change_password">
                <div style=" border:solid 1px #DDD; padding: 1px; height:200px;">
                    <div class="bgmenu" ><?=tt("Thay đổi mật khẩu")?></div>
                    <table border="0" cellpadding="8" cellspacing="0" width="100%">
                          <tr>
                                <td colspan="2" align="center" style="color:#FF0000">
                                     <?=$Errormessage;?>
                                </td>
                          </tr>
                          <tr>
                                <td nowrap="nowrap" align="right"><?=tt("Mật khẩu cũ")?> :</td>
                                <td><input type="password" name="adm_password_old" id="adm_password_old" class="form" size="20"></td>
                          </tr>
                          <tr>
                                <td nowrap="nowrap" align="right"><?=tt("Mật khẩu mới")?> :</td>
                                <td><input type="password" name="adm_password" id="adm_password" class="form" size="20"></td>
                          </tr>
                          <tr>
                                <td nowrap="nowrap" align="right"><?=tt("Xác nhận mật khẩu mới")?> :</td>
                                <td><input type="password" name="adm_password_con" id="adm_password_con" class="form" size="20"></td>
                          </tr>
                          <tr>
                                <td colspan="2" align="center">
                                     <input type="button" class="form_button" value="<?=tt("Cập nhật")?>" style="cursor:hand;" onClick="check_form_change_password();">&nbsp;
                                     <input type="reset" class="form_button" value="<?=tt("Làm lại")?>" style="cursor:hand;">
                                     <input type="hidden" name="action" value="update_password">
                                </td>
                          </tr>
                     </table>
                     
                    </div>                 
                </div>    
			</form>            
		</td>
	</tr>
</table>
<?=template_bottom() ?>
</body>
</html>