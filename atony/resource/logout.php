<?
session_start();	

if(isset($_SESSION["logged"])){
	$_SESSION["logged"] = "0";
	unset($_SESSION["logged"]);
}
if(isset($_SESSION["user_id"]))	unset($_SESSION["user_id"]);
if(isset($_SESSION["userlogin"]))unset($_SESSION["userlogin"]);
if(isset($_SESSION["password"]))	unset($_SESSION["password"]);
if(isset($_SESSION["isAdmin"]))	unset($_SESSION["isAdmin"]);
if(isset($_SESSION["lang_path"]))	unset($_SESSION["lang_path"]);
session_destroy();

if(isset($_COOKIE["admin_login_id"])){ unset($_COOKIE["admin_login_id"]); setcookie("admin_login_id", "", time() - 3600, "/"); }
if(isset($_COOKIE["admin_login_time"])){ unset($_COOKIE["admin_login_time"]); setcookie("admin_login_time", "", time() - 3600, "/"); }
if(isset($_COOKIE["admin_login_checksum"])){ unset($_COOKIE["admin_login_checksum"]); setcookie("admin_login_checksum", "", time() - 3600, "/"); }
?>
<script language="javascript">
	parent.location.href = "../login.php";
</script>
