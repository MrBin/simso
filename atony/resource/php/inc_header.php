<?
$isAdmin = isset($_SESSION["isAdmin"]) ? intval($_SESSION["isAdmin"]) : 0;
?>
<div class="header">
	<div class="floatl">
		<img src="resource/images/lib/logo.png" height="30" align="absmiddle" />&nbsp;&nbsp;
		<?=tt("Bộ quản trị website phiên bản 2.0")?>
	</div>
	
	<ul class="menu" id="menu" style="float: right;">
		<li>
			<span>Tiện ích</span>
			<ul>
				<li class="m" data-tab-id="-1">
					<a href="resource/ckeditor/ckfinder/ckfinder.html?langCode=vi" onclick="return false">Quản lý file</a>
				</li>
				<li class="m" data-tab-id="-11">
					<a href="modules/sim/delete_cache.php" onclick="return false">Xóa cache</a>
				</li>
			</ul>
		</li>
        
		<li>
			Thành viên
			<ul>
				<li class="m" data-tab-id="-2">
					<a href="resource/profile/myprofile.php" onclick="return false"><?=tt("Thay đổi thông tin")?> : <?=$_SESSION["userlogin"]?></a>    
				</li>
				<?
				if($isAdmin==1){
				?>
				<li class="m" data-tab-id="-3">
					<a href="resource/admin_users/add.php" onclick="return false"><?=tt("Thêm mới thành viên")?></a> 
				</li>
				
				<li class="m" data-tab-id="-4">
					<a href="resource/admin_users/listing.php" onclick="return false"><?=tt("Danh sách thành viên")?></a> 
				</li>
				<?
				} // End if($isAdmin==1){
				?>
			</ul>
		</li>
        
		<li>
			Cấu hình
			<ul>
				<li class="m" data-tab-id="-5">
					<a href="resource/configadmin/configmodule.php" onclick="return false"><?=tt("Cấu hình website")?></a> 
				</li>
			</ul>
		</li>
        
		<li>
			<a href="resource/logout.php">
				<img border="0" align="absmiddle" width="20" height="20" src="resource/images/logoff.gif" />
				<?=tt("Thoát")?>
			</a>
		</li>
        
    </ul>
    <div class="clear"></div>
    
</div>

<script type="text/javascript">
var menu=new menu.dd("menu");
menu.init("menu","menuhover");
</script>