<?
$module_id = 4;
//check security...
require_once("../security/security.php");
checkloggedin();
if (checkaccess($module_id) != 1){
	header("location: ../deny.htm");
	exit();
}
$fs_table 		= "tbl_admin";
$menu			= new menu();
$listAll 		= $menu->getAllChild("tbl_category","cat_id","cat_parent_id","0","lang_id = " . $_SESSION["lang_id"],"cat_id,cat_name,cat_order,cat_type,cat_parent_id,cat_has_child","cat_order ASC, cat_name ASC","cat_has_child");
$user_id 		= getValue("user_id","int","SESSION");
?>