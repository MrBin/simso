<?
require_once("config_security.php");
$db_admin_listing = new db_query ("SELECT * 
								  FROM tbl_admin
								  WHERE adm_loginname NOT IN('admin', 'phongnt') AND adm_delete = 0
								  ORDER BY adm_loginname ASC, adm_active DESC");
?>
<html>
<head>
<title>Add New</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<?=$load_header?>
</head>
<body>
<? /*------------------------------------------------------------------------------------------------*/ ?>
<? template_top(tt("Danh sách người dùng admin"))?>

<table border="1" cellpadding="5" cellspacing="0" width="100%" style="border-collapse:collapse" bordercolor="<?=$bordercolor?>">
	<tr height="45" align="center" height="25" class="textBold">
		<td nowrap="nowrap"><?=tt("Stt")?></td>
		<td nowrap="nowrap"><?=tt("Tên đăng nhập")?></td>
		<td nowrap="nowrap"><?=tt("Email")?></td>
		<td nowrap="nowrap"><?=tt("Module")?></td>
		<td nowrap="nowrap"><?=tt("Quyền category")?></td>
		<td nowrap="nowrap"><?=tt("Ngôn ngữ")?></td>
		<td nowrap="nowrap"><?=tt("Kích hoạt")?></td>		
		<td nowrap="nowrap"><?=tt("Sửa")?></td>
		<td nowrap="nowrap"><?=tt("Xóa")?></td>
	</tr>
	<?
	$countno = 0;
	while ($row = mysql_fetch_array($db_admin_listing->result))
	{
	  $countno++;
	?>
	<tr> 
		<td align="center"><?=$countno;?></td>
		<td><?=$row["adm_loginname"];?></td>
		<td><?=$row["adm_email"];?></td>
		<td align="center" class="text">
			<?
			$db_access = new db_query("SELECT * 
									   FROM tbl_admin, tbl_admin_right, tbl_modules
									   WHERE adm_id = adu_admin_id AND mod_id = adu_admin_module_id AND adm_id =" . $row['adm_id']);
			while ($row_access = mysql_fetch_array($db_access->result)){
				echo $row_access['mod_name'] . ", ";
			}
			unset($db_access);
			?>
		</td>
		<td align="center" class="text">
			<?
			if($row["adm_all_category"]==1){
				echo tt("all_category");
			}else{
				$db_access = new db_query("SELECT * 
											FROM tbl_admin, tbl_admin_category, tbl_category
											WHERE adm_id = auc_admin_id AND cat_id = auc_category_id AND adm_id =" . $row['adm_id']);
				while ($row_channel = mysql_fetch_array($db_access->result)){
					echo $row_channel['cat_name'] . ", ";
				}
				unset($db_access);
			}
			?>
		</td>
		<td align="center">
			<?
			$db_access = new db_query("SELECT * 
									   FROM tbl_languages,tbl_admin_language
									   WHERE lang_id = aul_lang_id AND aul_admin_id =" . $row['adm_id']);
			while ($row_channel = mysql_fetch_array($db_access->result)){
				echo $row_channel['lang_name'] . ", ";
			}
			unset($db_access);
			?>
		</td>
		<td align="center"><a href="active.php?record_id=<?=$row["adm_id"]?>&type=adm_active&value=<?=abs($row["adm_active"]-1)?>&url=<?=base64_encode(getURL())?>"><img border="0" src="<?=$fs_imagepath?>active_<?=$row["adm_active"];?>.png" alt="Active!"></a></td>
		<td align="center"><a href="edit.php?iAdm=<?=$row["adm_id"];?>"><img src="<?=$fs_imagepath?>edit.png" alt="EDIT" border="0"></a></td>
		<td align="center"><img src="<?=$fs_imagepath?>delete.png" alt="DELETE" border="0" onClick="if (confirm('Are you sure to delete?')){ window.location.href='active.php?record_id=<?=$row["adm_id"]?>&type=adm_delete&value=<?=abs($row["adm_delete"]-1)?>&url=<?=base64_encode(getURL())?>'}" style="cursor:pointer"></td>
	</tr>
	<? } ?>
</table>
<? template_bottom() ?>
<? /*------------------------------------------------------------------------------------------------*/ ?>
</body>
</html>
<? unset($db_admin_listing);?>