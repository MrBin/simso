<?
require_once("config_security.php");
//check quyền them sua xoa
checkAddEdit("edit");


$ff_action				= getURL();
$ff_redirect_succ 	= "listing.php";
$ff_redirect_fail 	= "";
$iAdm 					= getValue("iAdm");
$ff_table 				= "tbl_admin";

$fs_redirect			= base64_decode(getValue("returnurl","str","GET",base64_encode("listing.php")));
$record_id				= getValue("iAdm","int","GET");
$field_id				= "adm_id";
//kiểm tra quyền sửa xóa của user xem có được quyền ko
checkRowUser($fs_table,$field_id,$record_id,$fs_redirect);
$errorMsg				= "";
$Action 					= getValue("Action","str","POST","");
//Call Class generate_form();
$myform = new generate_form();
$myform->add("adm_email","adm_email",2,0,"",1," Email không chính xác !",0,"");
$myform->add("adm_name","adm_name",0,0,"",0,"",0,"");
$myform->add("adm_phone","adm_phone",0,0,"",0,"",0,"");
$myform->add("adm_all_category","adm_all_category",1,0,0,0,"",0,"");
$myform->add("adm_edit_all","adm_edit_all",1,0,0,0,"",0,"");
$myform->add("admin_id","admin_id",1,1,0,0,"",0,"");
$myform->add("adm_right_qtdh","adm_right_qtdh",1,1,0,0,"",0,"");

$myform->addTable($fs_table);

//Edit user profile
if ($Action =='update')
{
		$right_qtdh = getValue("right_qtdh","arr","POST",array());
		$adm_right_qtdh = array_sum($right_qtdh);
		$errorMsg .= $myform->checkdata();
		if($errorMsg == ""){
			$db_ex = new db_execute($myform->generate_update_SQL("adm_id",$iAdm));
			unset($db_ex);
			$module_list  			= getValue("mod_id","arr","POST","");
			$user_lang_id_list  	= getValue("user_lang_id","arr","POST","");
			$arelate_select  		= getValue("arelate_select","arr","POST","");
			
			$db_delete = new db_execute("DELETE FROM tbl_admin_right WHERE adu_admin_id =" . $iAdm);		
			unset($db_delete);
			if(isset($module_list[0])){
				for ($i=0; $i< count($module_list); $i++){
					$query_str = "INSERT INTO tbl_admin_right VALUES(" . $iAdm . "," . $module_list[$i] . ", " . getValue("adu_add" . $module_list[$i] , "int","POST") . ", " . getValue("adu_edit" . $module_list[$i] , "int","POST") . ", " . getValue("adu_delete" . $module_list[$i] , "int","POST") . ")";
					$db_ex = new db_execute($query_str);
					unset($db_ex);
				}
			}
			$db_delete = new db_execute("DELETE FROM tbl_admin_language WHERE aul_admin_id =" . $iAdm);		
			unset($db_delete);
			if(isset($user_lang_id_list[0])){
				for ($i=0; $i< count($user_lang_id_list); $i++){
					$query_str = "INSERT INTO tbl_admin_language VALUES(" . $iAdm . "," . $user_lang_id_list[$i] .")";
					$db_ex = new db_execute($query_str);
					unset($db_ex);
				}
			}
			$db_delete = new db_execute("DELETE FROM tbl_admin_category WHERE auc_admin_id =" . $iAdm);		
			unset($db_delete);
			if(getValue("adm_all_category","int","POST")==0){
				if(isset($arelate_select[0])){
					for ($i=0; $i<count($arelate_select); $i++){
						$query_str = "INSERT INTO tbl_admin_category VALUES(" . $iAdm . "," . $arelate_select[$i] .")";
						echo $query_str . '<br>';
						$db_ex = new db_execute($query_str);
						unset($db_ex);
					}
				}
			}
			redirect($ff_redirect_succ);
			exit();
		}
}

//Edit user password
$errorMsgpass = '';
if ($Action =='update_password')
{
	$myform = new generate_form();
	$myform->add("adm_password","adm_password",4,0,"",1,tt("Vui lòng nhập password"),0,"");
	$myform->addTable($fs_table);
	$errorMsgpass .= $myform->checkdata();
	if($errorMsgpass == ""){
		$db_ex = new db_execute($myform->generate_update_SQL("adm_id",$iAdm));
		unset($db_ex);
		echo '<script>alert("' . tt("Password đã được cập nhật") . '")</script>';
		redirect($ff_redirect_succ);
	}
}




//Select access module
$acess_module			= "";
$arrayAddEdit 			= array();
$db_access = new db_query("SELECT * 
									FROM tbl_admin, tbl_admin_right, tbl_modules
									WHERE adm_id = adu_admin_id AND mod_id = adu_admin_module_id AND adm_id =" . $iAdm);
while ($row_access = mysql_fetch_array($db_access->result)){
	$acess_module 			.= "[" . $row_access['mod_id'] . "]";
	$arrayAddEdit[$row_access['mod_id']] = array($row_access["adu_add"],$row_access["adu_edit"],$row_access["adu_delete"]);
}
unset($db_access);

//Select access channel
$access_channel="";
$db_category = new db_query("SELECT *
										FROM tbl_category
										WHERE 1 " . checkCategory($iAdm));

//Select access languages
$access_language="";
$db_access = new db_query("SELECT *
										FROM tbl_admin, tbl_admin_language, tbl_languages
										WHERE adm_id = aul_admin_id AND tbl_languages.lang_id = aul_lang_id AND adm_id =" . $iAdm);
while($row_access = mysql_fetch_array($db_access->result)) $access_language .="[" . $row_access['lang_id'] . "]";
unset($row_access);

//Check user exist or not
$db_admin_sel = new db_query("SELECT *
										  FROM tbl_admin
										  WHERE adm_id = " . $iAdm);
$db_getallmodule = new db_query("SELECT * 
												FROM tbl_modules
												ORDER BY mod_order DESC");

?>


<html>
<head>
<title>Add New</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<?=$load_header?>
</head>
<body>
<? /*------------------------------------------------------------------------------------------------*/ ?>
<? template_top(tt("Sửa đổi thành viên quản trị"))?>
<table width="100%" border="0" cellspacing="0" cellpadding="0" bordercolor="<?=$bordercolor?>">
	<tr> 
		<td colspan="2" align="center">
			<font color="#FF0000"><?=$errorMsg;?></font>
		</td>
	</tr>
	<tr>
		<td align="center">
			<table width="100%" border="0" cellspacing="0" cellpadding="0" bordercolor="<?=$bordercolor?>">
				<tr> 
				<? $row = mysql_fetch_array($db_admin_sel->result); ?>
				<form ACTION="<?=$ff_action;?>" METHOD="POST" name="edit_user">
				<td>
					<table width="100%" border="1" cellspacing="0" cellpadding="8" style="border-collapse:collapse" bordercolor="<?=$bordercolor?>">
						<tr> 
							<td align="right" nowrap="nowrap"><?=tt("Tên đăng nhập")?> :</td>
							<td><?=$row['adm_loginname'];?></td>
						</tr>
						<tr> 
							<td align="right" valign="middle" nowrap><?=tt("Email")?> :</td>
							<td> <input type="text" name="adm_email" id="adm_email" value="<?=$row["adm_email"]?>" size="50" maxlength="50" class="form">
						</td>
						</tr>
						<tr> 
							<td align="right" valign="middle" nowrap><?=tt("Phân quyền module")?> :</td>
							<td> 
								<table cellpadding="8" cellspacing="0" style="border-collapse:collapse" border="1" bordercolor="<?=$bordercolor?>">
									<tr bgcolor="<?=$bgcolor?>" height="30">
										<td><?=tt("Chọn")?></td>
										<td><?=tt("Module")?></td>
										<td><?=tt("Thêm")?></td>
										<td><?=tt("Sửa")?></td>
										<td><?=tt("Xóa")?></td>
									</tr>
									<?
									while ($mod=mysql_fetch_array($db_getallmodule->result)){
										if(file_exists("../../modules/" . $mod["mod_path"] . "/config_security.php")===true){
										?>
											<tr>
												<td align="center"><input type="checkbox" name="mod_id[]" id="mod_id" value="<?=$mod['mod_id'];?>" <? if (strpos($acess_module, "[" . $mod['mod_id'] . "]") !== false) {?> checked="checked"<? } ?> ></td>
												<td><?=tt($mod['mod_name']);?></td>
												<td align="center"><input type="checkbox" name="adu_add<?=$mod['mod_id'];?>" id="adu_add<?=$mod['mod_id'];?>" <? if(isset($arrayAddEdit[$mod['mod_id']])){ if($arrayAddEdit[$mod['mod_id']][0]==1) echo ' checked="checked"'; }?> value="1"></td>
												<td align="center"><input type="checkbox" name="adu_edit<?=$mod['mod_id'];?>" id="adu_edit<?=$mod['mod_id'];?>" <? if(isset($arrayAddEdit[$mod['mod_id']])){ if($arrayAddEdit[$mod['mod_id']][1]==1) echo ' checked="checked"'; }?> value="1"></td>
												<td align="center"><input type="checkbox" name="adu_delete<?=$mod['mod_id'];?>" id="adu_delete<?=$mod['mod_id'];?>" <? if(isset($arrayAddEdit[$mod['mod_id']])){ if($arrayAddEdit[$mod['mod_id']][2]==1) echo ' checked="checked"'; }?> value="1"></td>
											</tr>
										<?
										}
									}
									unset($db_getall_channel);
									?>
								</table>
							</td>
						</tr>
                        <tr> 
                            <td align="right" valign="middle" nowrap><?=tt("Phân ngôn ngữ")?> :</td>
                            <td> 
								<?
                                $db_getall_languages = new db_query("SELECT * 
                                                                     FROM tbl_languages
                                                                     ORDER BY lang_id ASC");
                                $cha_type="";
                                ?>
                                <table cellpadding="2" cellspacing="0">
                                    <tr>
                                    <?
                                    while ($lan=mysql_fetch_array($db_getall_languages->result)){
                                    ?>
                                        <td><input type="checkbox" name="user_lang_id[]" id="user_lang_id" value="<?=$lan['lang_id'];?>" <? if (strpos($access_language, "[" . $lan['lang_id'] . "]") !== false) {?> checked="checked"<? } ?>></td>
                                        <td><?=$lan['lang_name'];?></td>
                                    <?
                                    }
                                    unset($db_getall_channel);
                                    ?>
                                    </tr>
                                </table>
                        	</td>
                        </tr>
						<tr> 
						<td align="right" valign="middle" nowrap><?=tt("Tất cả danh mục")?> : </td>
						<td><input type="checkbox" name="adm_all_category" id="adm_all_category" <? if($row["adm_all_category"]==1) echo ' checked="checked"';?> value="1"></td>
						</tr>
						<tr> 
							<td nowrap align="right"></td>
							<td>
								<input onClick="document.edit_user.submit();" type="button" class="form_button" value="<?=tt("Cập nhật")?>">&nbsp;
							</td>
						</tr>
					</table>
				</td>
				<input type="hidden" name="Action" value="update">
				<input type="hidden" name="record_id" value="<?=$row["adm_id"]; ?>">
				</form>
				
				<!--Change password FORM -->
				<form ACTION="<?=$ff_action;?>?iAdm=<?=$iAdm?>" METHOD="POST" name="edit_password" onSubmit="formchangepass(); return false;">
					<td valign="top">
						<table align="center" cellpadding="8" cellspacing="1" bordercolor="<?=$bordercolor?>" border="1" style="border-collapse:collapse">
							<tr class="bgTableBorder"> 
								<td colspan="2" align="center"><?=tt("Thay đổi mật khẩu người dùng")?></td>
							</tr>
							<tr> 
								<td align="right" nowrap="nowrap"><?=tt("Password mới")?> :</td>
								<td><input type="password" name="adm_password" size="20" maxlength="50" class="form"></td>
							</tr>
							<tr> 
								<td align="right" nowrap="nowrap"><?=tt("Password xác nhận")?> :</td>
								<td><input type="password" name="adm_password_con" size="20" maxlength="50" class="form"></td>
							</tr>
							<tr> 
								<td nowrap align="right"></td>
								<td> 
									<input type="submit" class="form_button" value="<?=tt("Cập nhật")?>">&nbsp;
								</td>
							</tr>
						</table>
					</td>
				<input type="hidden" name="Action" value="update_password">
				<input type="hidden" name="record_id" value="<?=$row["adm_id"]; ?>">
				</form>
				</tr>
			</table>
		</td>
	</tr>
</table>
<script language="javascript">
 function formchangepass(){
 	if(document.getElementById("adm_password").value==''){
		document.getElementById("adm_password").focus();
		alert("<?=tt("Vui lòng nhập password")?>");
		return false;
	}
 	if(document.getElementById("adm_password").value!=document.getElementById("adm_password_con").value){
		document.getElementById("adm_password_con").focus();
		alert("<?=tt("Password mới và password xác nhận không giống nhau")?>");
		return false;
	}
	document.edit_password.submit();
 }
</script>
<? template_bottom() ?>
</body>
<?
$db_admin_sel->close();
unset($db_admin_sel);
?>