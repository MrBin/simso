<?
require_once("config_security.php");
//check quyền them sua xoa
checkAddEdit("add");

$ff_action 					= $_SERVER['REQUEST_URI'];
$ff_redirect_succ 		= "listing.php";
$ff_redirect_fail 		= "";


$Action = getValue("Action","str","POST","");
$errorMsg = "";
$allow_insert = 1;
$myform = new generate_form();
$myform->add("adm_loginname","adm_loginname",0,0,"",1,tt("Username is not empty and> 3 characters"),1,tt("Administrator account already exists"));
$myform->add("adm_password","adm_password",4,0,"",1,tt("Password must be greater than 4 characters"),0,"");
$myform->add("adm_email","adm_email",2,0,"",1,tt("Email is invalid"),0,"");
$myform->add("adm_name","adm_name",0,0,"",0,"",0,"");
$myform->add("adm_phone","adm_phone",0,0,"",0,"",0,"");
$myform->add("adm_all_category","adm_all_category",1,0,0,0,"",0,"");
$myform->add("adm_edit_all","adm_edit_all",1,0,0,0,"",0,"");
$myform->add("admin_id","admin_id",1,1,0,0,"",0,"");
$myform->addTable("tbl_admin");
//get vaule from POST
$adm_loginname = getValue("adm_loginname","str","POST","",1);
//password hash md5 --> only replace \' = '
$adm_password = getValue("adm_password","str","POST","",1);
//get Access Module list
$module_list			= "";
$module_list  			= getValue("mod_id","arr","POST","");
$user_lang_id_list  	= getValue("user_lang_id","arr","POST","");
$arelate_select  		= getValue("arelate_select","arr","POST","");

if ($Action =='insert')
{
	if ($module_list ==""){
		$allow_insert = 0;
		$errorMsg 			.= tt("Please_select_modules!");
	}
	
	//insert new user to database
	if ($allow_insert == 1){
		//Call Class generate_form();
		$right_qtdh = getValue("right_qtdh","arr","POST",array());
		$adm_right_qtdh = array_sum($right_qtdh);
		$querystr = $myform->generate_insert_SQL();
		$errorMsg .= $myform->checkdata();
		echo $myform->strErrorFeld;
		$last_id = 0;
		if($errorMsg == ""){
			$db_ex = new db_execute_return();
			$last_id = $db_ex->db_execute($querystr);
			unset($db_ex);
			if($last_id!=0){
				//insert user right\
				if(isset($module_list[0])){
					for ($i=0; $i< count($module_list); $i++){
						$query_str = "INSERT INTO tbl_admin_right VALUES(" . $last_id . "," . $module_list[$i] . ", " . getValue("adu_add" . $module_list[$i] , "int","POST") . ", " . getValue("adu_edit" . $module_list[$i] , "int","POST") . ", " . getValue("adu_delete" . $module_list[$i] , "int","POST") . ")";
						$db_ex = new db_execute($query_str);
						unset($db_ex);
					}
				}
				if(isset($user_lang_id_list[0])){
					for ($i=0; $i< count($user_lang_id_list); $i++){
						$query_str = "INSERT INTO tbl_admin_language VALUES(" . $last_id . "," . $user_lang_id_list[$i] .")";
						$db_ex = new db_execute($query_str);
						unset($db_ex);
					}
				}
				//category right
				$arelate_select  		= getValue("arelate_select","arr","POST","");
				if(isset($arelate_select[0])){
					for ($i=0; $i<count($arelate_select); $i++){
						$query_str = "INSERT INTO tbl_admin_category VALUES(" . $last_id . "," . $arelate_select[$i] .")";
						echo $query_str . '<br>';
						$db_ex = new db_execute($query_str);
						unset($db_ex);
					}
				}
			redirect($ff_redirect_succ);
			exit();
			}
		}
	}
}
$myform->evaluate();
$db_getallmodule = new db_query("SELECT * 
											FROM tbl_modules
											ORDER BY mod_order DESC");
?>
<html>
<head>
<title>Add New</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<?=$load_header?>
</head>
<body>
<? /*------------------------------------------------------------------------------------------------*/ ?>
<? template_top(tt("Thêm mới thành viên quản trị"))?>
<form ACTION="<?=$ff_action;?>" METHOD="POST" name="add_user" enctype="multipart/form-data">
<table align="center" cellpadding="8" cellspacing="0" border="0">
	<tr valign="baseline"> 
		<td colspan="2" align="center"><font color="#0000FF"><?=$errorMsg;?></font></td>
	</tr>
	<tr> 
		<td align="right" valign="middle" nowrap><?=tt("Tên đăng nhập")?> : </td>
		<td><input type="text" name="adm_loginname" id="adm_loginname" value="<?=$adm_loginname?>" size="50" class="form"> </td>
	</tr>
    <tr> 
        <td align="right" valign="middle" nowrap><?=tt("Tên đầy đủ")?> : </td>
        <td>
        	<input type="text" name="adm_name" id="adm_name" value="<?=$adm_name?>" size="50" class="form"> 
        </td>
    </tr>
    <tr> 
        <td align="right" valign="middle" nowrap><?=tt("Điện thoại")?> : </td>
        <td>
        	<input type="text" name="adm_phone" id="adm_phone" value="<?=$adm_phone?>" size="50" class="form"> 
        </td>
    </tr>
	<tr> 
		<td align="right" valign="middle" nowrap><?=tt("Mật khẩu")?> :</td>
		<td><input type="password" name="adm_password" size="50" maxlength="100" class="form"> </td>
	</tr>
	<tr> 
		<td align="right" valign="middle" nowrap><?=tt("Nhập lại mật khẩu")?> :</td>
		<td><input  type="password" name="adm_password_con" size="50" maxlength="100" class="form"> </td>
	</tr>
	<tr> 
		<td align="right" valign="middle" nowrap><?=tt("Email")?> :</td>
		<td> <input type="text" name="adm_email" value="<?=$adm_email?>" size="50" maxlength="50" class="form"></td>
	</tr>
	<tr> 
		<td align="right" valign="middle" nowrap><?=tt("Phân quyền module")?> :</td>
		<td> 
			<table cellpadding="8" cellspacing="0" style="border-collapse:collapse" border="1" bordercolor="<?=$bordercolor?>">
				<tr bgcolor="<?=$bgcolor?>" height="30" align="center">
					<td><?=tt("Chọn")?></td>
					<td><?=tt("Module")?></td>
					<td><?=tt("Thêm")?></td>
					<td><?=tt("Sửa")?></td>
					<td><?=tt("Xóa")?></td>
				</tr>
				<?
				while ($row=mysql_fetch_array($db_getallmodule->result)){
					if(file_exists("../../modules/" . $row["mod_path"] . "/config_security.php")===true){
					?>
						<tr>
							<td align="center"><input type="checkbox" name="mod_id[]" id="mod_id" value="<?=$row['mod_id'];?>"></td>
							<td><?=tt($row['mod_name']);?></td>
							<td align="center"><input type="checkbox" name="adu_add<?=$row['mod_id'];?>" id="adu_add<?=$row['mod_id'];?>" value="1"></td>
							<td align="center"><input type="checkbox" name="adu_edit<?=$row['mod_id'];?>" id="adu_edit<?=$row['mod_id'];?>" value="1"></td>
							<td align="center"><input type="checkbox" name="adu_delete<?=$row['mod_id'];?>" id="adu_delete<?=$row['mod_id'];?>" value="1"></td>
						</tr>
					<?
					}
				}
				unset($db_getall_channel);
				?>
			</table>
		</td>
	</tr>
    <!--
	<tr> 
		<td align="right" valign="middle" nowrap><?=tt("Phạm vi hoạt động")?> :</td> 
		<td><input type="checkbox" name="adm_edit_all" value="1"><?=tt("Quyền được phép tác động tới bản ghi của thành viên khác")?></td>
	</tr>
    -->
	<tr> 
		<td align="right" valign="middle" nowrap><?=tt("Phân ngôn ngữ")?> :</td>
		<td> 
			<?
			$db_getall_languages = new db_query("SELECT * 
												 FROM tbl_languages
												 ORDER BY lang_id ASC");
			$cha_type="";
			?>
			<table cellpadding="2" cellspacing="0">
				<tr>
				<?
				while ($row=mysql_fetch_array($db_getall_languages->result)){
				?>
					<td><input type="checkbox" name="user_lang_id[]" checked="checked" id="user_lang_id" value="<?=$row['lang_id'];?>"></td>
					<td><?=$row['lang_name'];?></td>
				<?
				}
				unset($db_getall_channel);
				?>
				</tr>
			</table>
		</td>
	</tr>
	<tr> 
		<td align="right" valign="middle" nowrap><?=tt("Tất cả danh mục")?>:</td> 
		<td> <input type="checkbox" name="adm_all_category" id="adm_all_category" checked="checked" value="1"></td>
	</tr>
	<tr valign="baseline"> 
		<td nowrap align="right"> </td>
		<td>
			<input onClick="document.add_user.submit();" type="button" class="form_button" value="<?=tt("Cập nhật")?>" style="cursor:hand;  background:url(../images/button_2.gif) no-repeat;">&nbsp;
			<input type="hidden" name="Action" value="insert">
		</td>
	</tr>


</table>
</form>

<? template_bottom() ?>
<? /*------------------------------------------------------------------------------------------------*/ ?>
</body>
<?
$db_getallmodule->close();
unset($db_getallmodule);
?>