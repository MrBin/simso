<?
#***
# * Thiết kế website - VNAZ - vnaz.vn
#***
@session_start();	
	
define('ROOTPATH', '../');

define('KERNEL_PATH', ROOTPATH.'kernel/');
define('CLASSES_PATH', KERNEL_PATH.'classes/');
define('FUNCTIONS_PATH', KERNEL_PATH.'functions/');

//
include(ROOTPATH.'config.php');
include(CLASSES_PATH.'fs_database.php');
//
include(FUNCTIONS_PATH.'functions_all.php');
include(FUNCTIONS_PATH.'functions_translate.php');
include('resource/security/functions.php');

$username	= getValue("username", "str", "POST", "", 1);
$password	= getValue("password", "str", "POST", "", 1);
$action		= getValue("action", "str", "POST", "");

if($action == "login"){
	$user_id	= 0;
	$user_id = checkLogin($username, $password);
	if($user_id != 0){
		$isAdmin		= 0;
		$db_isadmin	= new db_query("SELECT adm_isadmin, lang_id FROM tbl_admin WHERE adm_id = " . $user_id);
		$row			= mysql_fetch_array($db_isadmin->result);
		if($row["adm_isadmin"] != 0) $isAdmin = 1;
		//Set SESSION
		$_SESSION["logged"]			= 1;
		$_SESSION["user_id"]		= $user_id;
		$_SESSION["userlogin"]		= $username;
		$_SESSION["password"]		= md5($password);
		$_SESSION["isAdmin"]		= $isAdmin;
		$_SESSION["lang_id"]		= $row["lang_id"];
		$_SESSION["lang_id"] 		= get_curent_language();
		$_SESSION["lang_path"] 		= get_curent_path();
		unset($db_isadmin);

		setCookieAdminLogin($user_id);
	}
}

//Check logged
$logged = getValue("logged", "int", "SESSION", 0);
$db_language			= new db_query("SELECT tra_text,tra_keyword FROM tbl_translate_admin");
$langAdmin 				= array();
while($row=mysql_fetch_assoc($db_language->result)){
	$langAdmin[$row["tra_keyword"]] = $row["tra_text"];
}
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?
if($logged == 1){
	?>
	<script language="javascript">
   	window.parent.location.href="index.php";
   </script>
	<?
}
?>
<title><?=tt("Management website version 1.0")?></title>
<link rel="stylesheet" type="text/css" href="resource/css/layout.css">
<link rel="stylesheet" type="text/css" href="resource/css/css.css">
</head>
<body>

<div style="width:350px; margin:150px auto 0px; border:solid 1px #DDD; padding: 1px;">
	<div class="bgmenu" style="text-transform: uppercase;">
		<?=tt("Đăng nhập quản trị")?>
	</div>

	<form action="<?=$_SERVER['REQUEST_URI']?>" method="post" style="margin:0px; padding:0px;">
		<input name="action" type="hidden" value="login">
		
		<table cellpadding="10" cellspacing="10" width="100%" style="margin:10px 0px;">
			<tr>
				<td align="right" style="color:#333"><?=tt("Tài khoản")?> :</td>
				<td><input class="form_control" type="text" name="username" id="username" style="width:150px;" /></td>
			</tr>
			
			<tr>
				<td align="right" style="color:#333"><?=tt("Mật khẩu")?> :</td>
				<td><input class="form_control" type="password" name="password" id="password" style="width:150px;" /></td>
			</tr>
			
			<tr>
				<td colspan="2" align="center">
					<input type="submit" class="form_button" value="<?=tt("Đăng nhập")?>" />
					<input type="reset" class="form_button" value="<?=tt("Làm lại")?>" />
				</td>
			</tr>
		</table>
	</form>
	<div class="bgmenu" align="center">Copyright © <a style="font-size:11px; color:#FFF; text-decoration: none;" href="http://vnaz.vn">VNAZ</a> . All right reserved.</div>
</div>
    
</body>
</html>