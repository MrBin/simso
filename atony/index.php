<?
#***
# * Thiết kế website - VNAZ - vnaz.vn
#***
@session_start();	

define('ROOTPATH', '../');

define('KERNEL_PATH', ROOTPATH.'kernel/');
define('CLASSES_PATH', KERNEL_PATH.'classes/');
define('FUNCTIONS_PATH', KERNEL_PATH.'functions/');

//
include("aut.php");
include(ROOTPATH.'config.php');
include(CLASSES_PATH.'fs_database.php');
//
include(FUNCTIONS_PATH.'functions_all.php');
include(FUNCTIONS_PATH.'functions_translate.php');


//
$loginpath="login.php";
if (!isset($_SESSION["logged"])){
	redirect($loginpath);
}
else{
	if ($_SESSION["logged"] != 1){
		redirect($loginpath);
	}
}	

//
$db_language			= new db_query("SELECT tra_text,tra_keyword FROM tbl_translate_admin");
$langAdmin 				= array();
while($row=mysql_fetch_assoc($db_language->result)){
	$langAdmin[$row["tra_keyword"]] = $row["tra_text"];
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>	
	<title><?=tt("Bộ quản trị website phiên bản 1.0")?></title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <? require_once("index_head.php")?>
</head>
<body>
	<div id="TopPane" class="ui-layout-north"><? include("resource/php/inc_header.php");?></div>

	<div id="LeftPane" class="ui-layout-west"><? include('resource/php/inc_left.php');?></div>
	
	<div id="RightPane" class="ui-layout-center">
		<ul><li><a href="#tabs-0">Trang chủ</a></li></ul>
		<div id="tabs-0">
			<iframe id="idframe_0" src="intro.php" frameborder="0" width="100%" height="100%" onload="calcHeight();"></iframe>
		</div>	
	</div>
</body>
</html>