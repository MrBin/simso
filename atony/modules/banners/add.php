<?
require_once("config_security.php");	//check security	 

//check quyền thêm sửa xóa
checkAddEdit("add");

// Khai bao bien
$errorMsg 			= "";
$ban_type			= getValue("ban_type", "str", "POST","");
$action				= getValue("action", "str", "POST", "");	//Get Action

$add					= "add.php";
$listing				= "listing.php";
$edit					= "edit.php";
$after_save_data	= getValue("after_save_data", "str", "POST", $add);

// generate_form
$myform = new generate_form();	//Call Class generate_form();
$myform->removeHTML(0);	//Loại bỏ chức năng không cho điền tag html trong form
$myform->addTable($fs_table);
$myform->add("ban_name","ban_name",0,0,"",1,tt("Bạn chưa nhập tên quảng cáo"),0,tt("Bạn chưa nhập tên quảng cáo"));
$myform->add("ban_type","ban_type",1,0,0,1,tt("Chọn loại quảng cáo"),0,tt("Chọn loại quảng cáo"));
$myform->add("ban_url","ban_url",0,0,"",0,"",0,"");
$myform->add("ban_target","ban_target",0,0,"_self",1,tt("Chọn thực thi khi mở quảng cáo"),0,tt("Chọn thực thi khi mở quảng cáo"));
$myform->add("ban_width","ban_width",1,0,0,0,"",0,"");
$myform->add("ban_height","ban_height",1,0,0,0,"",0,"");
$myform->add("ban_order","ban_order",1,0,0,1,tt("Bạn chưa thiết lập vị trí ưu tiên"),0,tt("Bạn chưa thiết lập vị trí ưu tiên"));
$myform->add("ban_description","ban_description",0,0,"",0,"",0,"");
$myform->evaluate(); // đổi tên trường thành biến và giá trị

// Neu co form gui len
if($action == "submitForm"){
	
	// Tao duong dan cho folder anh
	$fs_filepath = $fs_filepath.date('Y',$new_date).'/'.date('m',$new_date).'/'.date('d',$new_date).'/'.$new_title_index.'/';
	if(!is_dir($fs_filepath)){
		$oldumask = umask(0);
		mkdir($fs_filepath,0777,true);
		umask($oldumask);	
	} // End if
	
	// Xu ly anh được up lên
	$upload_pic = new upload("picture", $fs_filepath, $extension_list, $limit_size, $new_title_index);
	if ($upload_pic->file_name != ""){
		$picture = $upload_pic->file_name;
		//resize anh
		resize_image($fs_filepath,$upload_pic->file_name,$small_width,$small_heght,$small_quantity,"s_");
		resize_image($fs_filepath,$upload_pic->file_name,$medium_width,$medium_heght,$medium_quantity,"m_");
		$myform->add("ban_picture","picture",0,1,"",0,"",0,"");
	} // End if ($upload_pic->file_name != ""){
		
	// Check Error!
	$errorMsg .= $upload_pic->show_warning_error();
	$errorMsg .= $myform->checkdata();
	$errorMsg .= $myform->strErrorFeld ;	//Check Error!
	if($errorMsg == ""){
		
		$db_ex	 		= new db_execute_return();
		$last_id 		= $db_ex->db_execute($myform->generate_insert_SQL());
		$record_id 		= $last_id;
		//echo $myform->generate_insert_SQL();exit();
		
		$bannercategory = getValue("bannercategory","arr","POST",array());
		foreach($bannercategory as $key=>$value){
			$db_relate_execute = new db_execute("INSERT INTO tbl_banners_categories VALUES(" . $record_id . ", " . intval($value) . ")");
		}
		
		redirect($after_save_data . "?record_id=" . $record_id . "&ban_type=" . $ban_type);
		exit();
	}
}
//
$myform->addFormname("submitForm"); //add  tên form để javacheck
$myform->addjavasrciptcode("");
$myform->checkjavascript();	//check java
?>

<html>
<head>
<title>Channel Add</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<?=$load_header?>
</head>

<body>
<? template_top(tt("Thêm mới quảng cáo"))?>
<? $fs_action = $_SERVER['SCRIPT_NAME'] . "?" . @$_SERVER['QUERY_STRING'];?>
<? $form = new form();?>
<?=$form->create_form("submitForm", $fs_action, "post", "multipart/form-data","");?>
<?=$form->create_table(7,3,"");?>
<?=$form->text_note()?>
<?=$form->errorMsg($errorMsg)?>
<?=$form->text(1 , "Tên quảng cáo", "ban_name", "ban_name", $ban_name, "Tên quảng cáo", 250, "", 255, "", "", "")?>
<?=$form->select(1 , "Loại quảng cáo", "ban_type", "ban_type", $arrType, $ban_type, "", 1, 0, "", "")?>
<?=$form->text(1 , "Liên kết quảng cáo", "ban_url", "ban_url", $ban_url, "Liên kết quảng cáo", 250, "", 255, "", "", '<img title="Tạo link cho menu" align="absmiddle" src="' . $fs_imagepath . 'edit.png" style="cursor:pointer; margin-left:5px" onClick="creat_link(\'mnu_link\')">')?>
<?=$form->select(1 , "Mở ra", "ban_target", "ban_target", $arrTarget, $ban_target, "", 1, 0, "", "")?>
<?=$form->text(0 , "Thứ tự", "ban_order", "ban_order", $ban_order, "Thứ tự", 50, "", 5, "", "", "")?>
<?=$form->getFile(0,"Ảnh minh họa", "picture", "picture" , 32, "", '<br />(<i>Dung lượng tối đa <font color="#993300">' . number_format($limit_size,0,'','.') . ' Kb</font>)</i>')?>
<?=$form->text(0,"Kích thước", "ban_width" . $form->ec . "ban_height", "ban_width" . $form->ec . "ban_height", $ban_width . $form->ec . $ban_height, "Chiều rộng" . $form->ec . "Chiều cao", 50 . $form->ec . 50, $form->ec, 4 . $form->ec . 4, " x ", $form->ec, "&nbsp; (Chiều rộng x Chiều cao)");?>
<?=$form->textarea(0,"Tóm tắt", "ban_description", "ban_description", $ban_description, 350, 70, "", "")?>
<?=$myform->radio(0 , "Sau khi lưu dữ liệu", "add_new" . $myform->ec . "return_listing" . $myform->ec . "return_edit", "after_save_data", $add . $myform->ec . $listing . $myform->ec . $edit, $after_save_data, "Thêm mới" . $myform->ec . "Quay về danh sách" . $myform->ec . "Sửa bản ghi", "");?>
<?=$form->button("button" . $form->ec . "reset", "button" . $form->ec . "reset", "button" . $form->ec . "reset", "Cập nhật"  . $form->ec .  "Làm lại", "", "");?>
<?=$form->hidden("action", "action", "submitForm", "");?>
<?=$form->close_table();?>
<?=$form->close_form();?>
<? unset($form);?>
<? template_bottom() ?>
</body>
</html>