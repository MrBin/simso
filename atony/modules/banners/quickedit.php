<?
require_once("config_security.php");

// Check quyen sua ban ghi
checkAddEdit("edit");

// Khai bao Bien
$cat_list 		= "";
$errorMsg 		= "";
$url				= base64_decode(getValue("url","str","GET",base64_encode("listing.php")));
$iQuick 			= getValue("iQuick","str","POST","");
$record_id		= getValue("record_id", "int", "POST", 0);

// Call Class generate_form();
$myform = new generate_form();
$myform->removeHTML(0); //Loại bỏ chuc nang thay the Tag Html
$myform->addTable($fs_table); //Add table
$myform->add("ban_type","ban_type",1,0,0,1,"Do not set position !",0,"Do not set position");
$myform->add("ban_name","ban_name",0,0,"",1,"Please enter banner name !",0,"Please enter banner name");
$myform->add("ban_url","ban_url",0,0,"",0,"Please enter URL !",0,"Please enter URL");
$myform->add("ban_target","ban_target",0,0,"",1,"Please choose a target !",0,"Please choose a target");
$myform->add("ban_order","ban_order",1,0,0,1,"Please set order",0,"Please set order");
$myform->add("ban_description","ban_description",0,0,"",0,"",0,"");
$myform->add("ban_width","ban_width",1,0,0,0,"",0,"");
$myform->add("ban_height","ban_height",1,0,0,0,"",0,"");

if ($iQuick == 'update'){
	
	// Kiem tra quyen cua user vơi ban ghi
	checkRowUser($fs_table,$field_id,$record_id,$url);
	
	// Tao duong dan cho folder anh
	$fs_filepath = $fs_filepath.'/';
	if(!is_dir($fs_filepath)){
		$oldumask = umask(0);
		mkdir($fs_filepath,0777,true);
		umask($oldumask);	
	} // End if(!is_dir($fs_filepath))

	// Xu ly anh được up lên
	$upload_pic = new upload("picture", $fs_filepath, $extension_list, $limit_size, $new_title_index);
	if ($upload_pic->file_name != ""){
		$picture = $upload_pic->file_name;
		//resize anh
		resize_image($fs_filepath,$upload_pic->file_name,$small_width,$small_heght,$small_quantity,"s_");
		resize_image($fs_filepath,$upload_pic->file_name,$medium_width,$medium_heght,$medium_quantity,"m_");
		$myform->add("ban_picture","picture",0,1,"",0,"",0,"");
	} // End if ($upload_pic->file_name != ""){
		
	// Delete picture
	//if ($upload_pic->file_name != "") delete_file($fs_table,"ban_id",$record_id,"ban_picture",$fs_filepath);
	
	//Check Error!
	$errorMsg .= $upload_pic->show_warning_error();
	$errorMsg .= $myform->checkdata();
	if($errorMsg == ""){

		$db_relate_execute = new db_execute("DELETE FROM banners_categories WHERE bcs_banner = " . $record_id);
		
		$bannercategory = getValue("bannercategory","arr","POST",array());
		foreach($bannercategory as $key=>$value){
			$db_relate_execute = new db_execute("INSERT INTO banners_categories VALUES(" . $record_id . ", " . intval($value) . ")");
		}
		
		$db_ex = new db_execute($myform->generate_update_SQL("ban_id",$record_id));
		
		redirect($url);
		exit();
	}else{
		echo $errorMsg;
	} // End if($errorMsg == "")
	
} // End if ($iQuick == 'update')
?>