<?
require_once("config_security.php");

// Khai bao bien
$sql			= "";
$iType 		= getValue("iType", "int", "GET",0);
$iCat 		= getValue("iCat", "int", "GET",0);
									
//Sort data
$sort			= getValue("sort");
switch($sort){
	case 1: $sqlOrderBy = "ban_title ASC"; break;
	case 2: $sqlOrderBy = "ban_title DESC"; break;
	case 3: $sqlOrderBy = "ban_date ASC"; break;
	case 4: $sqlOrderBy = "ban_date DESC"; break;
	case 5: $sqlOrderBy = "ban_hits ASC"; break;
	case 6: $sqlOrderBy = "ban_hits DESC"; break;
	default:$sqlOrderBy = "ban_date DESC"; break;
}

$db_category  = new db_query("SELECT bcs_banner,bcs_category
										FROM tbl_banners_categories 
										");										
$arrayBannerCat = array();										
while($row=mysql_fetch_assoc($db_category->result)){
	$arrayBannerCat[$row["bcs_banner"]][$row["bcs_category"]] = 0;
}
					
if ($iType !=0) $sql .= " AND ban_type=" . $iType;

$db_banner = new db_query("SELECT * 
									FROM tbl_banners
									WHERE tbl_banners.lang_id = " . $_SESSION["lang_id"] . $sql . "
									ORDER BY  ban_order ASC"
									);
?>
<html>
<head>
<title>Channel Listing</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<?=$load_header?>
<script language="javascript" src="../../resource/js/AC_RunActiveContent.js"></script>
<script language="javascript">	
function trim(sString) {
	while (sString.substring(0, 1) == ' ') {
		sString = sString.substring(1, sString.length);
	}
	while (sString.substring(sString.length - 1, sString.length) == ' ') {
		sString = sString.substring(0, sString.length - 1);
	}
	return sString;
}

function checkblank(str) {
	if (trim(str) == '')
		return true;
	else
		return false;
}

function ValidateForm(formobj) {
	if (checkblank(formobj.ban_name.value)) {
		alert('Please enter the banner name!');
		return false;
	}
	formobj.submit();
}
</script>
</head>
<body>
<? template_top("Danh sách quảng cáo")?>

<div style="position: absolute; top: 10px; right: 5px;"> 
	<form>
		<?=tt("Loại quảng cáo")?> : 
		<select name="iType" id="iType" onChange="window.location.href='listing.php?iType='+this.value" class="form">
			<option value="0"><?=tt("Loại quảng cáo")?></option>
			<?
			foreach($arrType as $key=>$value){
			?>
			<option value="<?=$key?>" <? if($iType == $key) echo 'selected';?>><?=$value?></option>
			<?
			}
			?>
		</select>
	</form>
</div>

<table border="1" cellpadding="3" cellspacing="2" width="100%" style="border-collapse:collapse" bordercolor="<?=$bordercolor?>">
	<tr class="textBold" align="center">
		<td width="30"><?=tt("Lưu")?></td>
		<td width="100"><?=tt("Ảnh")?></td>
		<td>
			<div><?=tt("Tổng thể")?></div>
			<div>
				<?=generate_sort("asc", 1, $sort, $fs_imagepath)?>
				<?=generate_sort("desc", 2, $sort, $fs_imagepath)?>
			</div>
		</td>
		<td><?=tt("Tóm tắt")?></td>
		<td nowrap="nowrap" width="80">
			<div><?=tt("Thứ tự")?></div>
			<div>
				<?=generate_sort("asc", 3, $sort, $fs_imagepath)?>
				<?=generate_sort("desc", 4, $sort, $fs_imagepath)?>
			</div>
		</td>
		<td nowrap="nowrap" width="80">
			<div><?=tt("Lượt xem")?></div>
			<div>
				<?=generate_sort("asc", 5, $sort, $fs_imagepath)?>
				<?=generate_sort("desc", 6, $sort, $fs_imagepath)?>
			</div>
		</td>
		<td width="40"><?=tt("Follow")?></td>
		<td width="40"><?=tt("Kích hoạt")?></td>
		<td width="40"><?=tt("Sao chép")?></td>
		<td width="40"><?=tt("Xóa")?></td>
	</tr>
	<?
	$countno = 0;
	while ($row = mysql_fetch_array($db_banner->result)){
		$countno++;
	?>
	<form action="quickedit.php?iQuick=update&iType=<?=$iType;?>&url=<?=base64_encode(getURL())?>" id="data<?=$row["ban_id"]?>" name="data<?=$row["ban_id"]?>" method="post" enctype="multipart/form-data">
	<tr <?=$fs_change_bg?> align="center">
		<td>
			<img title="<?=tt("Lưu bản ghi")?>" alt="<?=tt("Lưu")?>" src="<?=$fs_imagepath?>save.png" style="cursor:pointer" onClick="ValidateForm(data<?=$row["ban_id"]?>);">
		</td>
		<td>
			<div class="image_style">
			<? if($row["ban_picture"] != ""){?>
				<a style="color:#993333; font-weight:bold" href='delete_picture.php?record_id=<?=$row["ban_id"]?>&url="<? base64_encode(getURL())?>"' class="text">[<?=tt("Xóa ảnh")?>]</a>
			<? }?>
			<? if($row["ban_picture"] != ""){?>
				<? if(getExtension($row["ban_picture"]) == "swf"){?>				
					<script type="text/javascript">AC_FL_RunContent( 'codebase','http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=9,0,28,0','width','65','height','50','title','<?=$row["ban_picture"]?>','src','<?=$fs_filepath?><?=substr($row["ban_picture"], 0, strrpos($row["ban_picture"], "."))?>','quality','high','pluginspage','http://www.adobe.com/shockwave/download/download.cgi?P1_Prod_Version=ShockwaveFlash','wmode','transparent' );</script>
				<? }else{?>
					<a target="_blank" onMouseOver="showtip('<img src=\'<?=$fs_filepath?><?=$row["ban_picture"]?>\' />')" onMouseOut="hidetip()"><img src="<?=$fs_filepath?><?=$row["ban_picture"]?>" width="90" height="65" onError="this.src='<?=$fs_error_image?>'" /></a>
				<? }?>
			<? }else{?>
				<a title="Không có ảnh" href="" target="_blank"><img src="<?=$fs_no_image?>" width="90" height="65" /></a>
			<? }?>
			<input type="file" name="picture" id="picture" class="form" size="1">
			</div>
		</td>
		<td>
			<table border="0" cellpadding="2" cellspacing="1" width="100%">
				<? $form = new form();?>
				<?=$form->select(0 , "Loại", "ban_type", "ban_type", $arrType, $row["ban_type"], "", 1, 0, "", "")?>
				<?=$form->text(0, "Tên", "ban_name", "ban_name", $row["ban_name"], "Tên quảng cáo", 250, "", 255, "", "", "")?>
				<?=$form->text(0, "Link", "ban_url", "ban_url", $row["ban_url"], "Liên kết", 250, "", 255, "", "", "")?>
				<?=$form->select(0 , "Mở ra", "ban_target", "ban_target", $arrTarget, $row["ban_target"], "", 1, 0, "", "")?>
				<?=$form->text(0,"Kích thước", "ban_width" . $form->ec . "ban_height", "ban_width" . $form->ec . "ban_height", $row["ban_width"] . $form->ec . $row["ban_height"], "Chiều rộng" . $form->ec . "Chiều cao", 50 . $form->ec . 50, $form->ec, 4 . $form->ec . 4, " x ", $form->ec, "&nbsp; (Chiều rộng x Chiều cao)");?>
				<? unset($form)?>
			</table>
		</td>
		<td><textarea class="form" rows="4" cols="30" name="ban_description" id="ban_description"><?=$row["ban_description"];?></textarea></td>
		<td align="center"><input title="<?=tt("Thứ tự")?>" type="text" class="form_control" id="ban_order" name="ban_order" value="<?=$row["ban_order"]?>" style="text-align:right; width:40px" /></td>
		<td><?=$row["ban_hits"]?></td>
		<td><a href="active.php?record_id=<?=$row["ban_id"]?>&type=ban_follow&value=<?=abs($row["ban_follow"]-1)?>&url=<?=base64_encode(getURL())?>"><img title="<?=tt("Kích hoạt bản ghi")?>" alt="<?=tt("Kích hoạt")?>" border="0" src="<?=$fs_imagepath?>active_<?=$row["ban_follow"];?>.png"></a></td>
		<td><a href="active.php?record_id=<?=$row["ban_id"]?>&type=ban_active&value=<?=abs($row["ban_active"]-1)?>&url=<?=base64_encode(getURL())?>"><img title="<?=tt("Kích hoạt bản ghi")?>" alt="<?=tt("Kích hoạt")?>" border="0" src="<?=$fs_imagepath?>active_<?=$row["ban_active"];?>.png"></a></td>
		<td><img src="<?=$fs_imagepath?>copy.png" title="<?=tt("Sao chép bản ghi")?>" alt="<?=tt("Sao chép")?>" border="0" onClick="if (confirm('Bạn muốn sao chép nội dung này thành nội dung mới?')){ window.location.href='copy.php?record_id=<?=$row["ban_id"]?>&returnurl=<?=base64_encode(getURL())?>'}" style="cursor:pointer"></td>
		<td><img title="<?=tt("Xóa bản ghi")?>" alt="<?=tt("Xóa")?>" src="<?=$fs_imagepath?>delete.png" border="0" style="cursor:pointer" onClick="if (confirm('Are your sure to delete')){ window.location.href='delete.php?record_id=<?=$row["ban_id"];?>&iType=<?=$iType;?>&url=<?=base64_encode(getURL())?>';}"></td>
	</tr>	
	<input type="hidden" name="iQuick" value="update">
	<input type="hidden" name="record_id" value="<?=$row["ban_id"];?>">
	</form>
	<? 
	} // End while ($row = mysql_fetch_array($db_banner->result))
	unset($db_banner);
	?>
</table>

<? template_bottom() ?>
</body>
</html>