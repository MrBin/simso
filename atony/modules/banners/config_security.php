<?
$module_id = 4;
require_once("../../resource/security/security.php");

checkloggedin();
if (checkaccess($module_id) != 1){
	header("location: ../deny.htm");
	exit();
}

// Khai bao bien
$fs_table		= "tbl_banners";
$field_id		= "ban_id";
$field_name		= "ban_name";
$fs_filepath	= $root_path_admin . $path_banner;
		
$extension_list 	= "jpg,gif,swf,png";
$limit_size			= 1300000;

$small_width		= 	125;
$small_heght		=	97;
$small_quantity	=	100;

$medium_width		= 	250;
$medium_heght		=	200;
$medium_quantity	=	90;

// Type
$arrType  = array(1 => tt("Banner top"),
						2 => tt("Banner bot"),
						3 => tt("Banner giá rẻ"),
						4 => tt("Banner bên trái"),
						5 => tt("Banner bên phải"),
						5 => tt("Banner 4G"),
						);
						
// Target
$arrTarget   = array("_self" => tt("Hiện hành"),
							"_blank" => tt("Trang mới"));
?>