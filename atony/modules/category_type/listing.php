<?
require_once("config_security.php");

// Khai bao bien
$sql				= "";
$sqlWhere		= "";
$sqlOrderBy		= "typ_order DESC, typ_date DESC, typ_id DESC";
$limit     	 	= getValue("limit", "int", "GET");
$keyword			= getValue("keyword", "str", "GET", "", 1);

// Dua ra sql cho phu hop
if($keyword!="") $sql.= " AND typ_name LIKE '%" . $keyword . "%'";

if($show > 0){
	switch($show){
		case 1:	$sql .= " AND typ_active = 1 "; break;
		case 2:	$sql .= " AND typ_active = 0 "; break;
	}
}

$normal_class    	= "page";
$selected_class  	= "pageselect";
$page_prefix     	= "Trang : ";
$current_page		= ( getValue("page") < 1 ) ? 1 : getValue("page");
$page_size			= ( $limit > 0 ) ? $limit : 50;
$url 					= "listing.php?keyword=".$keyword."&limit=" . $limit . "&show=" . $show."&page=";

// Dem so luong ban ghi
$db_query = new db_query("SELECT Count(*) AS count
								FROM " . $fs_table . "
								WHERE ". $fs_table .".lang_id = " . $_SESSION["lang_id"] . $sql . $sqlWhere
								);
$row_count 		= mysql_fetch_assoc($db_query->result);
$total_record 	= $row_count['count'];
$db_query->close();
unset($db_query);

$db_query = new db_query("SELECT *
								FROM " . $fs_table . "
								WHERE " . $fs_table . ".lang_id = " . $_SESSION["lang_id"] . $sql . $sqlWhere . "
								ORDER BY " . $sqlOrderBy . "
								LIMIT " . ($current_page-1) * $page_size . ',' . $page_size
								);					 										
?>

<html>
<head>
<title>Channel Listing</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<?=$load_header?>
</head>

<body>
<!-- Load css và xử lý dữ liệu của jquery(kích hoạt, xóa bản ghi) -->
<div id="loadingBar">
    Đang xử lý ... Vui lòng đợi trong giây lát ... <br>
    <img align="absmiddle" src="../../resource/images/loading.gif">
</div>
<input class="controller" type="hidden" value="controllers" />

<? template_top(tt("Quản lý bản ghi") . "&nbsp;[" . number_format($total_record,0,".",",") . "]" )?>

<div style="position: absolute; top: 0; right: 5px;">
	<form action="listing.php" method="get" name="formSearch">
	<table border="0" cellpadding="5" align="center">
		<tr>
			<td>
				<select name="limit" id="limit" class="form" onChange="this.form.submit();">
				<? foreach($arrLimit as $key => $row){?>
				<option value="<?=$key?>" <? if($key == $page_size){ echo 'selected';}?> ><?=$row?></option>
				<? }?>
				</select>
			</td>
			<td>
				<select title="Kiểu hiển thị video" name="show" class="form" onChange="this.form.submit()">
					<option value="0">Tùy chọn</option>
					<?
					$arrShow = array(1 => "Kích hoạt", 2 => "Không kích hoạt");
					foreach($arrShow as $key => $value){
					?>
					<option title="<?=$value?>" value="<?=$key?>" <? if($key == $show){echo 'selected="selected"';}?>><?=$value?></option>
					<?
					}
					?>
				</select>
			</td>
			<td><input type="text" class="form" name="keyword" value="<?=$keyword?>" style="width:120px" /></td>       
			<td><input type="submit" value="<?=tt("Tìm kiếm")?>" class="form_button" /></td>
		</tr>
	</table>
	</form>
</div>

<form action="quickedit.php?returnurl=<?=base64_encode(getURL())?>" method="post" name="form_listing" enctype="multipart/form-data">
	<input type="hidden" name="iQuick" value="update">
	<table border="1" cellpadding="5" cellspacing="0" width="100%" style="border-collapse:collapse" bordercolor="<?=$bordercolor?>">
		<tr class="textBold" align="center" style="height:40px;">
			<td>&nbsp;</td>
			<td width="30"><?=translate_text("Lưu")?></td>
			<td><div><?=translate_text("Keyword")?></div></td>
			<td width="80"><div><?=translate_text("Cập nhật")?></div></td>
			<td class="textBold" width="5%"><?=tt("Home")?></td>
			<td class="textBold" width="5%"><?=tt("Active")?></td>
			<td class="textBold" width="5%"><?=tt("Sửa")?></td>
			<td class="textBold" width="5%"><?=tt("Xóa")?></td>
		</tr>

		<? 
		$countno = ($current_page-1) * $page_size;
		while($row=mysql_fetch_assoc($db_query->result)){
			$countno++;
		?>
		<tr <?=$fs_change_bg?> class="row-<?=$row["typ_id"]?> item-row" align="center">
			<td><input type="checkbox" name="record_id[]" id="record_id_<?=$countno?>" value="<?=$row["typ_id"]?>"></td>
			<td><img src="<?=$fs_imagepath?>save.png" border="0" style="cursor:pointer" onClick=" document.getElementById('record_id_<?=$countno;?>').checked=true ; document.form_listing.submit();" alt="Save"></td>
			<td align="left">
				<input type="text" id="typ_order_<?=$row['typ_id'];?>" name="typ_order_<?=$row['typ_id'];?>" value="<?=$row['typ_order']?>" class="form" style="width:20px;" onKeyUp="document.getElementById('record_id_<?=$countno?>').checked=true">
				<input type="text" name="typ_name_<?=$row["typ_id"]?>" size="40" id="typ_name_<?=$row["typ_id"]?>" value="<?=replaceQ($row["typ_name"])?>" class="form" onKeyUp="document.getElementById('record_id_<?=$countno?>').checked=true">
			</td>
			<td>
			<div><?=date("d/m/Y", $row["typ_date"])?></div>
				<div style="color:#666666; font-size:10px"><?=date("H:i A", $row["typ_date"])?></div>
			</td>
			<td align="center"><img class="home" name="<?=$row["typ_home"]?>" id="<?=$row["typ_id"];?>" path="<?=$fs_imagepath?>" src="<?=$fs_imagepath?>active_<?=$row["typ_home"];?>.png" title="<?=tt("Kích hoạt bản ghi")?>" alt="<?=tt("Kích hoạt")?>" /></td>
			<td align="center"><img class="active" name="<?=$row["typ_active"]?>" id="<?=$row["typ_id"];?>" path="<?=$fs_imagepath?>" src="<?=$fs_imagepath?>active_<?=$row["typ_active"];?>.png" title="<?=tt("Kích hoạt bản ghi")?>" alt="<?=tt("Kích hoạt")?>" /></td>
			<td><a href="edit.php?record_id=<?=$row["typ_id"];?>&url=<?=base64_encode(getURL())?>"><img src="<?=$fs_imagepath?>edit.png" alt="<?=tt("Edit")?>" border="0"></a></td>
			<td align="center"><img class="remove" id="<?=$row["typ_id"];?>" src="<?=$fs_imagepath?>delete.png" title="<?=tt("Xóa bản ghi")?>" alt="<?=tt("Xóa")?>" /></td>   
		</tr>
		<? }?>
    </table>
</form>

<? if($total_record > $page_size){ ?>
<div class="pagi"><?=generatePageBar($page_prefix,$current_page,$page_size,$total_record,$url,$normal_class,$selected_class);?></div>
<? } ?>


<? template_bottom() ?>
</body>
</html>
