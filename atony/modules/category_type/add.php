<?
require_once("config_security.php");	//check security	 
checkAddEdit("add"); //check quyền thêm sửa xóa

// Khai bao bien
$errorMsg 			= "";		//Warning Error!
$action				= getValue("action", "str", "POST", "");

$add					= "add.php";
$listing				= "listing.php";
$edit					= "edit.php";
$after_save_data	= getValue("after_save_data", "str", "POST", $edit);

$typ_strdate		= getValue("typ_strdate", "str", "POST", date("d/m/Y"));
$typ_strtime		= getValue("typ_strtime", "str", "POST", date("H:i:s"));
$typ_date			= convertDateTime($typ_strdate, $typ_strtime);

$typ_name_index 	= getValue("typ_name_index", "str", "POST", "");
if($typ_name_index == '') $typ_name_index 	= removeTitle(getValue("typ_name", "str", "POST", ""),'/');
	
// generate form
$myform = new generate_form();	//Call Class generate_form();
$myform->removeHTML(0);	//Loại bỏ chức năng không cho điền tag html trong form
$myform->addTable($fs_table);	// Add table
$myform->add("typ_type","typ_type",0,0,"",1,tt("Chọn định nghĩa danh mục"),0,"");
$myform->add("typ_name","typ_name",0,0,"",1,tt("Điền tên loại danh mục"),0,"");
$myform->add("typ_name_index","typ_name_index",0,1,"",0,"",0,"");
$myform->add("typ_link","typ_link",0,0,"",0,"",0,"");
$myform->add("meta_title","meta_title",0,0,"",0,"",0,"");
$myform->add("meta_description","meta_description",0,0,"",0,"",0,"");
$myform->add("meta_keyword","meta_keyword",0,0,"",0,"",0,"");
$myform->add("meta_title_cat","meta_title_cat",0,0,"",0,"",0,"");
$myform->add("meta_description_cat","meta_description_cat",0,0,"",0,"",0,"");
$myform->add("meta_keyword_cat","meta_keyword_cat",0,0,"",0,"",0,"");
$myform->add("meta_title_detail","meta_title_detail",0,0,"",0,"",0,"");
$myform->add("meta_description_detail","meta_description_detail",0,0,"",0,"",0,"");
$myform->add("meta_keyword_detail","meta_keyword_detail",0,0,"",0,"",0,"");
$myform->add("typ_order","typ_order",1,0,0,0,"",0,"");
$myform->add("typ_date","typ_date",1,1,0,0,"",0,"");
$myform->evaluate(); // đổi tên trường thành biến và giá trị

// Nếu như có form được submit
if($action == "submitForm"){
	// Kiểm tra lỗi
	$errorMsg .= $myform->checkdata();
	$errorMsg .= $myform->strErrorFeld ;	//Check Error!

	// Nếu như không có lỗi
	if($errorMsg == ""){
    	
 		// Thuc hien query
		$db_ex	 		= new db_execute_return();
		$query			= $myform->generate_insert_SQL();
		$last_id 		= $db_ex->db_execute($query);
		$record_id 		= $last_id;
		//echo $query;exit();
	
		redirect($after_save_data . "?record_id=" . $record_id);
		exit();
		
    } // End if($errorMsg == ""){
	
} // End if($action == "submitForm")

// Khai bao ten form 
$myform->addFormname("submitForm"); //add  tên form để javacheck

// Xử lý javascript
$myform->addjavasrciptcode('');
$myform->checkjavascript();
?>

<html>
<head>
<title>Administrator Control panel</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<?=$load_header?>
</head>
<body>
<? template_top(tt("Quản lý bản ghi"))?>

<table class="tab" border="0" cellpadding="0" cellspacing="0">
	<tr align="center">
	<td nowrap><div class="n" id="tabt_1" onClick="tabdiv(1,5);document.cookie='tab_select=1'"><?=tt("Chung")?></div></td>
	<td nowrap><div class="n" id="tabt_2" onClick="tabdiv(2,5);document.cookie='tab_select=2'"><?=tt("SEO")?></div></td>
	<td nowrap><div class="n" id="tabt_3" onClick="tabdiv(3,5);document.cookie='tab_select=3'"><?=tt("SEO Cat")?></div></td>
	<td nowrap><div class="n" id="tabt_4" onClick="tabdiv(4,5);document.cookie='tab_select=4'"><?=tt("Seo Detail")?></div></td>
	<td style="border-bottom:solid 1px #DADADA;" width="99%">&nbsp;</td>
	</tr>
</table>

<? $fs_action = $_SERVER['SCRIPT_NAME'] . "?" . @$_SERVER['QUERY_STRING'];?>
<?=$myform->create_form("submitForm", $fs_action, "post", "multipart/form-data","");?>

<div style="border:solid 1px #DADADA; border-top-width:0; padding:10px;">
	<div id="tabc_1" class="tabc">
		<?=$myform->create_table(8,0,"");?>
		<?=$myform->text_note()?>
		<?=$myform->errorMsg($errorMsg)?>
		<?=$myform->select(1 , "Định nghĩa loại danh mục", "typ_type", "typ_type", $arrType, $typ_type, "", 1, 0, '', "")?>
		<?=$myform->showform("text","typ_name","Tên danh mục",$typ_name,array("width"=>350))?>
		<?=$myform->text(0 , "Đường dẫn tĩnh", "typ_link", "typ_link", $typ_link, "Đường dẫn tĩnh", 400, "", 255, "", "", '&nbsp;<img title="Tạo link cho menu" align="absmiddle" src="'.$fs_imagepath.'edit.png" style="cursor:pointer" onClick="creat_link(\'typ_link\')">')?>
		<?=$myform->text(0 , "Thứ tự", "typ_order", "typ_order", $typ_order, "Thứ tự", 50, "", 5, "", "", "")?>
		<?=$myform->text(0, "Ngày cập nhật", "typ_strdate" . $myform->ec . "typ_strtime", "typ_strdate" . $myform->ec . "typ_strtime", $typ_strdate . $myform->ec . $typ_strtime, "Ngày (dd/mm/yyyy)" . $myform->ec . "Giờ (hh/mm/ss)", "70" . $myform->ec . "70", $myform->ec, "10" . $myform->ec . "10", " - ", $myform->ec, "");?>
		<?=$myform->close_table();?>
	</div>
    
	<div id="tabc_2" class="tabc">
		<div class="mart10 tB" align="center" style="color:#F00;">Đây là title theo module Vd: Giới thiệu, tin tức, tuyển dụng ...</div>
		
		<div class="mart10">Google Title :</div>
		<textarea name="meta_title" id="meta_title" class="form_control" style="width:90%; height:50px;"><?=$meta_title?></textarea>
		
		<div class="mart10">Google Description :</div>
		<textarea name="meta_description" id="meta_description" class="form_control" style="width:90%; height:100px;"><?=$meta_description?></textarea>
		
		<div class="mart10">Google Keyword : <span> - <i class="font11">(Các keyword cách nhau bằng dầu phẩy ',')</i></span></div>
    	<textarea name="meta_keyword" id="meta_keyword" class="form_control" style="width:90%; height:100px;"><?=$meta_keyword?></textarea>
	</div>
    
	<div id="tabc_3" class="tabc">
		<div class="mart10 tB" align="center" style="color:#F00;">Đây là title chung cho Category được nhập ở danh mục cha</div>
		<div align="center" style="line-height:1.6em;">
			Dùng {ten_danh_muc} : để thay thế tên danh mục được nhập ở danh mục cha<br />
			Dùng {tu_khoa} : để thay thế cho từ khóa khi search hoặc tag trong bài viết
		</div>
		<div class="mart10">Google Title Category :</div>
		<textarea name="meta_title_cat" id="meta_title_cat" class="form_control" style="width:90%; height:50px;"><?=$meta_title_cat?></textarea>
		
		<div class="mart10">Google Description Category :</div>
		<textarea name="meta_description_cat" id="meta_description_cat" class="form_control" style="width:90%; height:100px;"><?=$meta_description_cat?></textarea>
		
		<div class="mart10">Google Keyword Category : <span> - <i class="font11">(Các keyword cách nhau bằng dầu phẩy ',')</i></span></div>
    	<textarea name="meta_keyword_cat" id="meta_keyword_cat" class="form_control" style="width:90%; height:100px;"><?=$meta_keyword_cat?></textarea>
    </div>
    
	<div id="tabc_4" class="tabc">
	 	<div class="mart10 tB" align="center" style="color:#F00;">Đây là title chung cho chi tiết bài viết</div>
		<div align="center" style="line-height:1.6em;">
			Dùng {ten_danh_muc} : để thay thế tên danh mục được nhập ở danh mục cha<br />
			Dùng {ten_bai_viet} : để thay thế tên bài viết trong mục<br />
			Dùng {mo_ta_bai_viet} : để thay thế mô tả của bài viết trong mục
		</div>
	     
		<div class="mart10">Google Title Detail :</div>
		<textarea name="meta_title_detail" id="meta_title_detail" class="form_control" style="width:90%; height:50px;"><?=$meta_title_detail?></textarea>
		
		<div class="mart10">Google Description Detail :</div>
		<textarea name="meta_description_detail" id="meta_description_detail" class="form_control" style="width:90%; height:100px;"><?=$meta_description_detail?></textarea>
		
		<div class="mart10">Google Keyword Detail : <span> - <i class="font11">(Các keyword cách nhau bằng dầu phẩy ',')</i></span></div>
	 	<textarea name="meta_keyword_detail" id="meta_keyword_detail" class="form_control" style="width:90%; height:100px;"><?=$meta_keyword_detail?></textarea>
	</div>

	<?=$myform->create_table(8,3,"");?>
	<?=$myform->radio(0 , "Sau khi lưu dữ liệu", "add_new" . $myform->ec . "return_listing" . $myform->ec . "return_edit", "after_save_data", $add . $myform->ec . $listing . $myform->ec . $edit, $after_save_data, "Thêm mới" . $myform->ec . "Quay về danh sách" . $myform->ec . "Sửa bản ghi", "");?>
	<?=$myform->button("button" . $myform->ec . "reset", "button" . $myform->ec . "reset", "button" . $myform->ec . "reset", "Cập nhật"  . $myform->ec .  "Làm lại", "", "");?>
	<?=$myform->hidden("action", "action", "submitForm", "");?>
	<?=$myform->close_table();?>
</div>

<?
$tab_select = getValue("tab_select","int","COOKIE",1);
?>
<script>tabdiv(<?=$tab_select?>,5);</script>

<?=$myform->close_form();?>
<? unset($myform);?>
<? template_bottom() ?>
<? /*------------------------------------------------------------------------------------------------*/ ?>
</body>
</html>