<?
// Mr.LeToan - NTP.VnJp
require_once("config_security.php");	//check security	
require_once("func.php");	//check security	  
checkAddEdit("add"); //check quyền thêm sửa xóa

#+
#+ Loai sim
$query = ' SELECT simtp_id,simtp_name,simtp_index'
		.' FROM tbl_simtype'
		.' WHERE simtp_active = 1'
		.' ORDER BY simtp_order'
		;
$db_query = new db_query($query);
while($row = mysql_fetch_assoc($db_query->result)){
	$loai_sim_idx[$row["simtp_index"]] = $row["simtp_id"];
} // End while($row = mysql_fetch_assoc($db_query->result))

#+
#+ Gia sim
$query = ' SELECT pri_id,pri_min,pri_max'
		.' FROM tbl_simprice'
		;
$db_query = new db_query($query);
while($row = mysql_fetch_assoc($db_query->result)){
	$gia_sim_idx[$row['pri_id']] = array(
										'pri_min'=>$row['pri_min'],
										'pri_max'=>$row['pri_max']
										);
} // End while($row = mysql_fetch_assoc($db_catDauSo->result))

#+
#+ Dau so
$prefix_num = array();
$query = ' SELECT sds_id,sds_category,sds_name,sds_name'
		.' FROM tbl_simdauso'
		.' WHERE sds_active = 1'
		;	
$db_query = new db_query($query);
while($row = mysql_fetch_assoc($db_query->result)){
	$index = intval($row['sds_name']);
	$dauso_sim_idx[$index] = array(
							'sds_id'=>$row["sds_id"],
							'sds_category'=>$row["sds_category"]
							);
} // End while($ds = mysql_fetch_assoc($db_query->result))

#+
#+ Dai ly
$query = ' SELECT *'
		.' FROM tbl_simdaily'
		.' WHERE simch_active = 1'
		.' ORDER BY simch_order'
		;
		
$db_query = new db_query($query);
$i = 0;
while($ch = mysql_fetch_assoc($db_query->result)){
	$i++;
	
	#+
	$dai_ly[$i] = $ch;
	$daily_sim_idx[$ch["simch_viettat"]] = $ch['simch_id'];
} // End while($ch = mysql_fetch_assoc($db_query->result))

#+
$tacvu = getValue("tacvu","str","POST","dang_sim");

if( isset($_POST['submit']) )
{
	quanlysim($_POST['upload_sim'],$tacvu);
} // End if( isset($_POST['submit']) ) 
?>

<html>
<head>
<title>Channel Add</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<?=$load_header?>
</head>

<body>
<? // ---------------------------------------------------------------------------------------------------- //?>
<? template_top(tt("Xử lý sim qua file Excel"))?>
<div class="textBlue">Upload file Excel &nbsp;&nbsp; <img src="../../resource/images/line_red.gif"></div>

<form action="?" method="post" name="adminForm" id="adminForm" enctype="multipart/form-data">
	<table cellpadding="8" cellspacing="0">
        <tr>
        	<tr>
            	<td>Đăng sim từ text : </td>
                <td>
                	<div class="marb10">
                		Copy 3 cột Sim - Giá - Kho vào đây : 
                    </div>
                    <textarea name="upload_sim" style="width:300px; height:150px; outline:none"></textarea>
                </td>
            </tr>
            
        </tr>
		<tr>
			<td>Tác vụ với file excel này : </td>
			<td valign="top">
				<div><input type="radio" name="tacvu" value="dang_sim" <? if($tacvu == 'dang_sim') echo 'checked'?>> Tạo sim mới trên hệ thống</div>
                <div><input type="radio" name="tacvu" value="dang_xoa_sim" <? if($tacvu == 'dang_xoa_sim') echo 'checked'?>> Tạo mới và xóa sim cũ trên hệ thống</div>
				<div><input type="radio" name="tacvu" value="xoa_sim" <? if($tacvu == 'xoa_sim') echo 'checked'?>> Xóa sim tương ứng trên hệ thống </div>
			</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td><input type="submit" name="submit" class="form_button" value="Cập nhật"></td>
		</tr>
	</table>
    <br /><br />
    <a class="textBold textRed" href="simphongthuy.php" target="_blank">
    	Cập nhật điểm phong thủy
    </a>
    <br /><br />
    <a class="textBold textRed" href="simphongthuy1.php" target="_blank">
    	Lấy ra những sim có điểm cao
    </a>
</form>
<? template_bottom() ?>
<? // ---------------------------------------------------------------------------------------------------- //?>
</body>
</html>