<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?
require_once("config_security.php");	//check security
require_once('../../../kernel/functions_ext/functions_phongthuy.php');

/*/
$db_ex = new db_execute("UPDATE `tbl_sim` SET  
										`sim_amduong` =  0,
										`sim_nguhanh` =  0,
										`sim_sonut` =  0,
										`sim_diem_vietaa` =  0,
										`sim_diem_vanmenh` = 0,
										`sim_diem_tcpt` = 0,
										`sim_diem_stst` = 0
									");
unset($db_ex);
exit();
//*/

$db_count = new db_query("SELECT COUNT(*) AS count 
									FROM tbl_sim 
									WHERE LENGTH(sim_sim2) = 9 AND LOCATE('8', sim_sim2) > 0 AND sim_amduong = 0 AND sim_nguhanh = 0 AND sim_sonut = 0 AND sim_diem_vietaa = 0 AND sim_diem_vanmenh = 0 AND sim_diem_tcpt = 0 AND sim_diem_stst = 0");
$row = mysql_fetch_assoc($db_count->result);
$total_record = $row['count'];
$db_count->close();
unset($db_count);

$limit = 1000;
if($total_record > $limit) echo '<meta http-equiv="refresh" content="0" >';

// sim_amduong = 0 AND sim_nguhanh = 0 AND sim_sonut = 0 AND sim_diem_vietaa = 0 AND sim_diem_vanmenh = 0 AND sim_diem_tcpt = 0 AND sim_diem_stst = 0
$db_sim = new db_query("SELECT sim_sim2, sim_typeid
								FROM tbl_sim 
								WHERE LENGTH(sim_sim2) = 9 AND LOCATE('8', sim_sim2) > 0 AND sim_amduong = 0 AND sim_nguhanh = 0 AND sim_sonut = 0 AND sim_diem_vietaa = 0 AND sim_diem_vanmenh = 0 AND sim_diem_tcpt = 0 AND sim_diem_stst = 0
								LIMIT " . $limit
								);
echo 'Số sim chưa xử lý: <b style="color: red">' . $total_record . '</b> (Nếu số này > 0 tiếp tục F5)';
while($sim = mysql_fetch_assoc($db_sim->result)){
	
	// So dien thoai can xem
	$so_dien_thoai 		= '0' . $sim['sim_sim2'];
	
	// Be chuoi so dien thoai
	$arrSimStrSplit 	= str_split($so_dien_thoai);
	
	// So cuoi cua so dien thoai
	$so_cuoi = substr($so_dien_thoai,-1,1);
	
	// So ky tu cua so dien thoai
	$count_sodienthoai = strlen($so_dien_thoai);
	
	// Chia doi so ky tu day so
	$count_sodienthoaiChia2 = round($count_sodienthoai / 2);
	
	// Tinh tong so nut cua day so
	$tong_day_so = 0;
	foreach($arrSimStrSplit as $key => $row) $tong_day_so = $tong_day_so + $row;
	$tong_day_so = substr($tong_day_so,-1,1);
	
	// Xem co bao nhieu so 8 trong day so
	$count_char_8 = substr_count($so_dien_thoai,8);
	
	if($count_char_8 <= 0) continue;

	$sim_diem_stst 		= 0;
	$sim_diem_vietaa 		= 0;
	$sim_diem_vanmenh 	= 0;
	$sim_diem_tcpt 		= 0;
	
	// Kiem tra day so ve am duong tuong phoi
	$count_so_am = 0;
	$count_so_duong = 0;
	foreach($arrSimStrSplit as $key => $row){
		$dau = $row%2 == 0 ? '-' : '+';
		#+
		if($dau == '-') $count_so_am++;
		else $count_so_duong++;
	} // End foreach($arrSoSim as $key => $row)
	$hieu_so_am_duong = $count_so_am - $count_so_duong;
	
	//
	// 1. ====================================================================================================
	// Xem am duong cua day so
	$sim_amduong = 0;
	if($hieu_so_am_duong > 0) $sim_amduong = 1;
	elseif($hieu_so_am_duong < 0) $sim_amduong = 2;
	
	// Tinh diem phong thuy cho van am duong nay
	$diem_vietaa 	= 0;
	$diem_vanmenh	= 0;
	$diem_tcpt		= 0;
	$diem_stst		= 0;
	
	$diem_vanmenh = 0;
	if($hieu_so_am_duong == 0){	
		$diem_vietaa 	= 3;
		$diem_vanmenh	= 2;
		$diem_tcpt		= 2.5;
		$diem_stst		= 3;
	} // End if($hieu_so_am_duong == 0)
	
	$sim_diem_vietaa	= $sim_diem_vietaa + $diem_vietaa;
	$sim_diem_vanmenh 	= $sim_diem_vanmenh + $diem_vanmenh;
	$sim_diem_tcpt 		= $sim_diem_tcpt + $diem_tcpt;
	$sim_diem_stst 		= $sim_diem_stst + $diem_stst;

	//
	// 2.1 + 2.3 ====================================================================================================
	// Xem ngu hanh cua day so
	$sim_nguhanh = 0;
	
	// Tinh toan - Dat mac dinh ngu hanh cua day so la thuy
	$ngu_hanh_day_so = 'thuy';
	
	if($so_cuoi != 0){
		if( in_array ( $so_cuoi, array(1) ) ) $ngu_hanh_day_so = 'thuy';
		elseif( in_array ( $so_cuoi, array(2,5,8) ) ) $ngu_hanh_day_so = 'tho';
		elseif( in_array ( $so_cuoi, array(3,4) ) ) $ngu_hanh_day_so = 'moc';
		elseif( in_array ( $so_cuoi, array(6,7) ) ) $ngu_hanh_day_so = 'kim';
		elseif( in_array ( $so_cuoi, array(9) ) ) $ngu_hanh_day_so = 'hoa';
	} // End if($so_cuoi != 0)
	
	if($count_sodienthoai == 10){
		$so_dien_thoai_substr27 = substr($so_dien_thoai,2,7);
		
		$count_so_menh_thuy 	= substr_count($so_dien_thoai_substr27,1);
		$count_so_menh_tho 	= substr_count($so_dien_thoai_substr27,2)+substr_count($so_dien_thoai_substr27,5)+substr_count($so_dien_thoai_substr27,8);
		$count_so_menh_moc 	= substr_count($so_dien_thoai_substr27,3)+substr_count($so_dien_thoai_substr27,4);
		$count_so_menh_kim 	= substr_count($so_dien_thoai_substr27,6)+substr_count($so_dien_thoai_substr27,7);
		$count_so_menh_hoa 	= substr_count($so_dien_thoai_substr27,9);
	
		if($ngu_hanh_day_so === 'thuy'){
			
			if($count_so_menh_tho >= 4) $ngu_hanh_day_so = 'tho';
			if($count_so_menh_moc >= 4) $ngu_hanh_day_so = 'moc';
			if($count_so_menh_kim >= 4) $ngu_hanh_day_so = 'kim';
				
		}elseif($ngu_hanh_day_so === 'tho'){
			
			if($count_so_menh_thuy >= 4) $ngu_hanh_day_so = 'thuy';
			if($count_so_menh_moc >= 4) $ngu_hanh_day_so = 'moc';
			if($count_so_menh_kim >= 4) $ngu_hanh_day_so = 'kim';
				
		}elseif($ngu_hanh_day_so === 'moc'){
			
			if($count_so_menh_thuy >= 4) $ngu_hanh_day_so = 'thuy';
			if($count_so_menh_tho >= 4) $ngu_hanh_day_so = 'tho';
			if($count_so_menh_kim >= 4) $ngu_hanh_day_so = 'kim';
				
		}elseif($ngu_hanh_day_so === 'kim'){
			
			if($count_so_menh_thuy >= 4) $ngu_hanh_day_so = 'thuy';
			if($count_so_menh_tho >= 4) $ngu_hanh_day_so = 'tho';
			if($count_so_menh_moc >= 4) $ngu_hanh_day_so = 'moc';
				
		}elseif($ngu_hanh_day_so === 'hoa'){
			
			if($count_so_menh_thuy >= 4) $ngu_hanh_day_so = 'thuy';
			if($count_so_menh_tho >= 4) $ngu_hanh_day_so = 'tho';
			if($count_so_menh_moc >= 4) $ngu_hanh_day_so = 'moc';
			if($count_so_menh_kim >= 4) $ngu_hanh_day_so = 'kim';
				
		} // End if($ngu_hanh_day_so === 'thuy')
		
	}elseif($count_sodienthoai == 11){
		$so_dien_thoai_substr28 = substr($so_dien_thoai,2,8);
		
		$count_so_menh_thuy = substr_count($so_dien_thoai_substr28,1);
		$count_so_menh_tho 	= substr_count($so_dien_thoai_substr28,2)+substr_count($so_dien_thoai_substr28,5)+substr_count($so_dien_thoai_substr28,8);
		$count_so_menh_moc 	= substr_count($so_dien_thoai_substr28,3)+substr_count($so_dien_thoai_substr28,4);
		$count_so_menh_kim 	= substr_count($so_dien_thoai_substr28,6)+substr_count($so_dien_thoai_substr28,7);
		$count_so_menh_hoa 	= substr_count($so_dien_thoai_substr28,9);
	
		if($ngu_hanh_day_so === 'thuy'){
			
			if($count_so_menh_tho >= 5) $ngu_hanh_day_so = 'tho';
			if($count_so_menh_moc >= 5) $ngu_hanh_day_so = 'moc';
			if($count_so_menh_kim >= 5) $ngu_hanh_day_so = 'kim';
				
		}elseif($ngu_hanh_day_so === 'tho'){
			
			if($count_so_menh_thuy >= 4) $ngu_hanh_day_so = 'thuy';
			if($count_so_menh_moc >= 5) $ngu_hanh_day_so = 'moc';
			if($count_so_menh_kim >= 5) $ngu_hanh_day_so = 'kim';
				
		}elseif($ngu_hanh_day_so === 'moc'){
			
			if($count_so_menh_thuy >= 4) $ngu_hanh_day_so = 'thuy';
			if($count_so_menh_tho >= 5) $ngu_hanh_day_so = 'tho';
			if($count_so_menh_kim >= 5) $ngu_hanh_day_so = 'kim';
				
		}elseif($ngu_hanh_day_so === 'kim'){
			
			if($count_so_menh_thuy >= 4) $ngu_hanh_day_so = 'thuy';
			if($count_so_menh_tho >= 5) $ngu_hanh_day_so = 'tho';
			if($count_so_menh_moc >= 5) $ngu_hanh_day_so = 'moc';
					
		}elseif($ngu_hanh_day_so === 'hoa'){
			
			if($count_so_menh_thuy >= 4) $ngu_hanh_day_so = 'thuy';
			if($count_so_menh_tho >= 5) $ngu_hanh_day_so = 'tho';
			if($count_so_menh_moc >= 5) $ngu_hanh_day_so = 'moc';
			if($count_so_menh_kim >= 5) $ngu_hanh_day_so = 'kim';
		
		} // End if($ngu_hanh_day_so === 'thuy')
			
	} //End if($count_sodienthoai == 10)
		
	// Ket luan ngu hanh cua day so
	$sim_nguhanh = $arrNguHanh1[$ngu_hanh_day_so];
	
	$diem_vietaa 	= 1;
	$diem_vanmenh	= 0;
	$diem_tcpt		= 0;
	$diem_stst		= 1;
	
	$sim_diem_vietaa	= $sim_diem_vietaa + $diem_vietaa;
	$sim_diem_vanmenh 	= $sim_diem_vanmenh + $diem_vanmenh;
	$sim_diem_tcpt 		= $sim_diem_tcpt + $diem_tcpt;
	$sim_diem_stst 		= $sim_diem_stst + $diem_stst;
	
	// Tinh ngu hanh sinh khac trong day so
	$cap_so_tuong_sinh = 0;
	$cap_so_tuong_khac = 0;
	
	foreach($arrSimStrSplit as $key => $row){
		if( $key < $count_sodienthoai-1){
			$cap_so 	= $arrSimStrSplit[$key].$arrSimStrSplit[$key+1];
			$cap_so 	= intval($cap_so);
			if( isset($arrNguHanhDaySoSinhKhac[$cap_so]) ){
				if($arrNguHanhDaySoSinhKhac[$cap_so] === 'sinh')
					$cap_so_tuong_sinh++;
				elseif($arrNguHanhDaySoSinhKhac[$cap_so] === 'khac')
					$cap_so_tuong_khac++;
			} // End if( isset($arrNguHanhDaySoSinhKhac[$cap_so]) )
		} // End if( $key < $count_sodienthoai-1)
	} // ENd foreach($arrSimStrSplit as $key => $row)
	
	$diem_vietaa 	= 0;
	$diem_vanmenh	= 0;
	$diem_tcpt		= 0;
	$diem_stst		= 0;
	
	$hieu_cap_so_tuong_sinh_khac = $cap_so_tuong_sinh-$cap_so_tuong_khac;
	if($hieu_cap_so_tuong_sinh_khac >= 0){
		$diem_vietaa 	= 1;
		$diem_vanmenh	= 0;
		$diem_tcpt		= 1;
		$diem_stst		= 1;
	} // End if($so_tuong_sinh-$so_tuong_khac > 0)
	
	$sim_diem_vietaa		= $sim_diem_vietaa + $diem_vietaa;
	$sim_diem_vanmenh 	= $sim_diem_vanmenh + $diem_vanmenh;
	$sim_diem_tcpt 		= $sim_diem_tcpt + $diem_tcpt;
	$sim_diem_stst 		= $sim_diem_stst + $diem_stst;

	//
	// 4. ====================================================================================================
	// Xac dinh hanh que bat quai cua day so (Viet AA)
	$noi_quai = 0;
	$ngoai_quai = 0;
	
	if($count_sodienthoai > 1){
		
		// Tinh noi quai
		$noi_quai 		= substr($so_dien_thoai, 0, $count_sodienthoai - $count_sodienthoaiChia2);
		$tong = str_split($noi_quai);
		$noi_quai = 0;
		foreach($tong as $key => $row) $noi_quai = $noi_quai + $row;
		$noi_quai 		= $noi_quai%8;
		
		// Tinh ngoai quai
		$ngoai_quai 	= substr($so_dien_thoai, 0 - $count_sodienthoaiChia2);
		$tong = str_split($ngoai_quai);
		$ngoai_quai = 0;
		foreach($tong as $key => $row) $ngoai_quai = $ngoai_quai + $row;
		$ngoai_quai 	= $ngoai_quai%8;
	}else{
		$noi_quai 	= 0;
		$ngoai_quai = $so_dien_thoai;	
		$ngoai_quai = $ngoai_quai%8;
	} // End if($count_sodienthoai > 1)
	
	$que_chu = $noi_quai.$ngoai_quai;
	$que_chu = intval($que_chu);
	$que_ho = $arrQue[$que_chu]['que_ho_id'];
	
	if($que_ho < 10){
		$ngoai_quai_ho 	= 0;	
		$noi_quai_ho 		= $que_ho;	
	}else{
		$ngoai_quai_ho 	= substr($que_ho,0,1);	
		$noi_quai_ho 		= substr($que_ho,-1,1);	
	} // End if($que_ho < 10)
	
	// Diem cho que chu
	$diem_vietaa 	= 0;
	$diem_vanmenh	= 0;
	$diem_tcpt		= 0;
	$diem_stst		= 0;
	
	if($arrQue[$que_chu]['trang_thai_que'] == 1){
		$diem_vietaa 	= 1;
		$diem_vanmenh	= 0;
		$diem_tcpt		= 1;
		$diem_stst		= 1;
	} // End if($arrQue[$que_chu]['trang_thai_que'] == 2)
	
	$sim_diem_vietaa		= $sim_diem_vietaa + $diem_vietaa;
	$sim_diem_vanmenh 	= $sim_diem_vanmenh + $diem_vanmenh;
	$sim_diem_tcpt 		= $sim_diem_tcpt + $diem_tcpt;
	$sim_diem_stst 		= $sim_diem_stst + $diem_stst;

	// Diem cho que ho
	$diem_vietaa 	= 0;
	$diem_vanmenh	= 0;
	$diem_tcpt		= 0;
	$diem_stst		= 0;
	
	if($arrQue[$que_ho]['trang_thai_que'] == 1){
		$diem_vietaa 	= 1;
		$diem_vanmenh	= 0;
		$diem_tcpt		= 1;
		$diem_stst		= 1;	
	} // End if($arrQue[$que_chu]['trang_thai_que'] == 2)
	
	$sim_diem_vietaa		= $sim_diem_vietaa + $diem_vietaa;
	$sim_diem_vanmenh 	= $sim_diem_vanmenh + $diem_vanmenh;
	$sim_diem_tcpt 		= $sim_diem_tcpt + $diem_tcpt;
	$sim_diem_stst 		= $sim_diem_stst + $diem_stst;

	//
	// 3. ====================================================================================================
	// Xem thien thoi (cuu tinh do phap) cua day so
	$diem_vietaa 	= 0;
	$diem_vanmenh	= 0;
	$diem_tcpt		= 0;
	$diem_stst		= 0;
	if($count_char_8 > 0){
		$diem_vanmenh 	= 2;
		$diem_tcpt 		= $count_char_8*0.4;
		$diem_stst		= 1;
	} // End if($count_char_8 > 0)

	$sim_diem_vietaa	= $sim_diem_vietaa + $diem_vietaa;
	$sim_diem_vanmenh	= $sim_diem_vanmenh + $diem_vanmenh;
	$sim_diem_tcpt 		= $sim_diem_tcpt + $diem_tcpt;
	$sim_diem_stst 		= $sim_diem_stst + $diem_stst;
	
	
	//
	// 5. ====================================================================================================
	// Xem quan niem dan gian cua day so
	$diem_vietaa 	= 0;
	$diem_vanmenh	= 0;
	$diem_tcpt		= 0;
	$diem_stst		= 0;
	if($tong_day_so >= 4 && $tong_day_so <= 7){
		$diem_vanmenh 	= 1;
		$diem_stst		= 0.5;
	}elseif($tong_day_so >= 8){
		$diem_vanmenh 	= 2;
		$diem_stst		= 1;
	} // End if($tong_day_so >= 0 && $tong_day_so <= 1)
	
	$sim_diem_vietaa	= $sim_diem_vietaa + $diem_vietaa;
	$sim_diem_vanmenh	= $sim_diem_vanmenh + $diem_vanmenh;
	$sim_diem_tcpt 		= $sim_diem_tcpt + $diem_tcpt;
	$sim_diem_stst 		= $sim_diem_stst + $diem_stst;
	
	// Danh gia diem theo quan niem dan gian sim so dep theo loai
	$iType = checkSimType($so_dien_thoai,$arrSimTypeIndex);	
	
	$so_dien_thoai_type = '';
	if(isset($arrSimType[$iType]['simtp_index']) && $iType != 13) $so_dien_thoai_type = $arrSimType[$iType]['simtp_index'];
	
	// Cho diem stst
	$diem = 0;
	if($so_dien_thoai_type == 'sim-luc-quy') $diem = 0.5;	
	elseif($so_dien_thoai_type == 'sim-ngu-quy') $diem = 0.5;	
	elseif($so_dien_thoai_type == 'sim-tu-quy') $diem = 0.5;	
	elseif($so_dien_thoai_type == 'sim-tam-hoa') $diem = 0.5;	
	elseif($so_dien_thoai_type == 'sim-loc-phat') $diem = 0.5;	
	elseif($so_dien_thoai_type == 'sim-than-tai') $diem = 0.5;	
	elseif($so_dien_thoai_type == 'sim-so-tien') $diem = 0.5;	
	elseif($so_dien_thoai_type == 'sim-taxi') $diem = 0.5;	
	elseif($so_dien_thoai_type == 'sim-ganh-dao') $diem = 0.5;	
	elseif($so_dien_thoai_type == 'sim-so-kep') $diem = 0.5;	
	elseif($so_dien_thoai_type == 'sim-so-lap') $diem = 0.5;	
	elseif($so_dien_thoai_type == 'sim-nam-sinh') $diem = 0.5;	
	elseif( substr($so_dien_thoai,-4,4) == '1102' || substr($so_dien_thoai,-4,4) == '6789' ) $diem = 0.5;
	$sim_diem_stst 		= $sim_diem_stst + $diem;
	
	//
	// 6. Sim phong thuy stst ====================================================================================================
	// Sim co 3 so 8 va tong nut >=8 va la sim tam hoa, tu quy, ngu quy, luc quy
	$diem_vietaa 	= 0;
	$diem_vanmenh	= 0;
	$diem_tcpt		= 0;
	if($count_char_8 >= 1 && $tong_day_so >= 8){
		$diem_vietaa 	= 1;	
	}

	$sim_diem_vietaa	= $sim_diem_vietaa + $diem_vietaa;
	$sim_diem_vanmenh	= $sim_diem_vanmenh + $diem_vanmenh;
	$sim_diem_tcpt 		= $sim_diem_tcpt + $diem_tcpt;
	
	// Tong so nut
	$sim_sonut = $tong_day_so;
	
	$db_ex = new db_execute("UPDATE `tbl_sim` SET  
										`sim_amduong` =  '".$sim_amduong."',
										`sim_nguhanh` =  '".$sim_nguhanh."',
										`sim_sonut` =  '".$sim_sonut."',
										`sim_diem_vietaa` =  '".$sim_diem_vietaa."',
										`sim_diem_vanmenh` =  '".$sim_diem_vanmenh."',
										`sim_diem_tcpt` =  '".$sim_diem_tcpt."',
										`sim_diem_stst` =  '".$sim_diem_stst."' 
									WHERE `sim_sim2` = ".$sim['sim_sim2']." 
									LIMIT 1 ;
									");
	unset($db_ex);

} // End while($sim = mysql_fetch_assoc($db_sim->result))
unset($db_sim);
?>
