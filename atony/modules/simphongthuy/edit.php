<?
require_once("config_security.php");	//check security

$record_id	 = getValue("record_id","int","GET",0);

// Kiem tra quyen voi ban ghi
checkAddEdit("edit");
checkRowUser($fs_table, $field_id, $record_id, base64_encode('listing.php'));

//	Khai báo biến
$errorMsg 			= "";
$action				= getValue("action", "str", "POST", "");

$add					= "add.php";
$listing				= "listing.php";
$edit					= "edit.php";
$after_save_data	= getValue("after_save_data", "str", "POST", $listing);

$sim_sim1 			= getValue("sim_sim1","str","POST");
$sim_sim2			= preg_replace("/[^0-9]/si","",$sim_sim1);

$myform = new generate_form();	//Call Class generate_form();
$myform->removeHTML(0);	//Loại bỏ chức năng không cho điền tag html trong form
$myform->addTable($fs_table); //Add table
$myform->add("sim_sim1","sim_sim1",0,0,"",1,"Điền số sim",0,"");
$myform->add("sim_sim2","sim_sim2",1,1,"",0,"",0,"");
$myform->add("sim_price","sim_price",0,0,"",0,"",0,"");
$myform->add("sim_category","sim_category",1,0,"",1,"Chọn mạng",0,"");
$myform->add("sim_typeid","sim_typeid",1,0,"",1,"Chọn phân loại",0,"");
$myform->add("sim_dailyid","sim_dailyid",1,0,"",1,"Chọn cửa hàng",0,"");

// Nếu như có form được submit
if($action == "submitForm"){
	// Kiểm tra lỗi
	$errorMsg .= $myform->checkdata();
	$errorMsg .= $myform->strErrorFeld ;	//Check Error!
	if($errorMsg == ""){	
	
		// Thuc hien query
		$query = $myform->generate_update_SQL($field_id,$record_id);
		$db_ex = new db_execute($query);
		//echo $query;exit();

		// Chuyen ve trang khac khi xu ly du lieu oki
		redirect($after_save_data . "?record_id=" . $record_id);
		exit();
		
    } // End if($errorMsg == ""){
	
} // End if($action == "submitForm")

$myform->evaluate(); // đổi tên trường thành biến và giá trị
$myform->addFormname("add_new"); //add  tên form để javacheck
$myform->addjavasrciptcode(""); // them java
$myform->checkjavascript(); //check java

$db_data 	= new db_query("SELECT * FROM " . $fs_table . " WHERE " . $field_id . " = " . $record_id);
if($row 		= mysql_fetch_assoc($db_data->result)){
	foreach($row as $key=>$value){
		if($key!='lang_id' && $key!='admin_id') $$key = $value;
	}
}else{
	exit();
} // End if($row 		= mysql_fetch_assoc($db_data->result))
unset($db_data);
?>

<html>
<head>
<title>Channel Add</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<?=$load_header?>
</head>

<body>
<? template_top(tt("Thêm mới bản ghi"))?>
<? $fs_action = $_SERVER['SCRIPT_NAME'] . "?" . @$_SERVER['QUERY_STRING'];?>
<?=$myform->create_form("add_new", $fs_action, "post", "multipart/form-data","");?>
<?=$myform->create_table(5,0,"");?>
<?=$myform->text_note()?>
<?=$myform->errorMsg($errorMsg)?>
<?=$myform->text(1 , "Số sim", "sim_sim1", "sim_sim1", $sim_sim1, "Số sim", 250, "", 255, "", "", "")?>
<?=$myform->text(1 , "Giá", "sim_price", "sim_price", $sim_price, "Giá", 250, "", 255, "", "", "")?>
<?=$myform->select(1 , "Mạng", "sim_category", "sim_category", $arrSimCat, $sim_category, "", 1, 0, '', "")?>
<?=$myform->select(1 , "Phân loại", "sim_typeid", "sim_typeid", $arrSimType, $sim_typeid, "", 1, 0, '', "")?>
<?=$myform->select(1 , "Đại lý", "sim_dailyid", "sim_dailyid", $arrSimDaily, $sim_dailyid, "", 1, 0, '', "")?>
<?=$myform->radio(0 , "Sau khi lưu dữ liệu", "add_new" . $myform->ec . "return_listing" . $myform->ec . "return_edit", "after_save_data", $add . $myform->ec . $listing . $myform->ec . $edit, $after_save_data, "Thêm mới" . $myform->ec . "Quay về danh sách" . $myform->ec . "Sửa bản ghi", "");?>
<?=$myform->button("button" . $myform->ec . "reset", "button" . $myform->ec . "reset", "button" . $myform->ec . "reset", "Cập nhật"  . $myform->ec .  "Làm lại", "", "");?>
<?=$myform->hidden("action", "action", "submitForm", "");?>
<?=$myform->close_table();?>
<?=$myform->close_form();?>
<? unset($form);?>
<? template_bottom() ?>
</body>
</html>