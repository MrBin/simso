<?
require_once("../../resource/security/security.php");

$module_id			 	= 19;

checkloggedin();
if (checkaccess($module_id) != 1){
	redirect("../deny.htm");
	exit();
}

$fs_table			= "tbl_simphongthuy";
$field_id			= "sim_sim2";
$field_name			= "sim_sim1";
$fs_filepath		= "";

// Limit
$arrLimit = array('5' => '5 items',
						'15' => '15 items',
						'30' => '30 items',
						'50' => '50 items',
						'80' => '80 items',
						'120' => '120 items'
						);
// Show
$arrShow  = array(1 => "Kích hoạt", 
						2 => "Chưa kích hoạt",
						3 => "Khuyến mãi",
						4 => "Hot", 
						5 => "New"
						);

// Sim Category
$db_cat = new db_query("SELECT cat_id, cat_name
								FROM tbl_category 
								WHERE cat_parent_id = 0 AND cat_type = 'sim' AND lang_id = " . $_SESSION["lang_id"] . " 
								ORDER BY cat_order
								");
$arrSimCat = array();
while($row = mysql_fetch_assoc($db_cat->result)) $arrSimCat[$row['cat_id']]	 = $row['cat_name'];	
unset($db_cat);

// Sim Type
$db_sim_type  = new db_query("SELECT simtp_id, simtp_name 
										FROM tbl_simtype 
										ORDER BY simtp_order
										");
$arrSimType = array();
while($row = mysql_fetch_assoc($db_sim_type->result)) $arrSimType[$row['simtp_id']]	 = $row['simtp_name'];	
unset($db_sim_type);

// Sim Dai Ly
$db_sim_daily	  = new db_query("SELECT simch_id, simch_name 
											FROM tbl_simdaily 
											ORDER BY simch_order
											");
$arrSimDaily = array();
while($row = mysql_fetch_assoc($db_sim_daily->result)) $arrSimDaily[$row['simch_id']]	 = $row['simch_name'];	
unset($db_sim_daily);
?>