<?
require_once("config_security.php");

// Khai bao bien
$sql			= "";
$sqlWhere	= "";
$limit		= getValue("limit", "int", "GET");
$sort			= getValue("sort");
$show			= getValue("show");
$iCat			= getValue("iCat");
$keyword		= getValue("keyword", "str", "GET", "", 1);

switch($sort){
	case 1: $sqlOrderBy = "new_title ASC"; break;
	case 2: $sqlOrderBy = "new_title DESC"; break;
	case 3: $sqlOrderBy = "new_date ASC"; break;
	case 4: $sqlOrderBy = "new_date DESC"; break;
	case 5: $sqlOrderBy = "new_hits ASC"; break;
	case 6: $sqlOrderBy = "new_hits DESC"; break;
	default:$sqlOrderBy = "new_id DESC"; break;
}

// Dua ra sql cho phu hop
if($keyword!="") $sql.=" AND new_title LIKE '%" . replaceMQ($keyword) . "%'";
if($iCat!="") $sql.=" AND new_category = " . $iCat . "";

if($show > 0){
	switch($show){
		case 1:	$sql .= " AND new_picture <> '' "; break;
		case 2:	$sql .= " AND new_picture IS NULL "; break;
		case 3:	$sql .= " AND new_active = 1 "; break;
		case 4:	$sql .= " AND new_active = 0 "; break;
		case 5:	$sql .= " AND new_hot = 1 "; break;
		case 6:	$sql .= " AND new_new = 1 "; break;
		case 6:	$sql .= " AND new_phongthuy = 1 "; break;
	}
}

$normal_class    	= "page";
$selected_class  	= "pageselect";
$page_prefix     	= "Trang : ";
$current_page    	= ( getValue("page") < 1 ) ? 1 : getValue("page");
$page_size	     	= ( $limit > 0 ) ? $limit : 15;
$url 					= "listing.php?keyword=" . $keyword . "&iCat=" . $iCat . "&limit=" . $limit . "&show=" . $show . "&sort=" . $sort."&page=";

// Dem so luong ban ghi
$db_query = new db_query("SELECT Count(*) AS count
								FROM " . $fs_table . "
									INNER JOIN tbl_category ON (cat_id = new_category)
								WHERE " . $fs_table . ".lang_id = " . $_SESSION['lang_id'] . $sql . $sqlWhere
								);
$row_count 		= mysql_fetch_assoc($db_query->result);
$total_record 	= $row_count['count'];
$db_query->close();
unset($db_query);

$db_news = new db_query("SELECT new_id,new_title,new_phongthuy,new_title_index,new_picture,new_picture_web,new_active,new_hot,new_new,new_hits,new_date,new_auto
								FROM " . $fs_table . "
									INNER JOIN tbl_category ON (cat_id = new_category)
								WHERE " . $fs_table . ".lang_id = " . $_SESSION['lang_id'] . $sql . $sqlWhere . "
								ORDER BY " . $sqlOrderBy . ", new_date DESC, new_id DESC
								LIMIT " .($current_page-1) * $page_size . ',' . $page_size
								);					 										
?>

<html>
<head>
<title>Channel Listing</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<?=$load_header?>
</head>

<body>
<!-- Load css và xử lý dữ liệu của jquery(kích hoạt, xóa bản ghi) -->
<div id="loadingBar">
    Đang xử lý ... Vui lòng đợi trong giây lát ... <br>
    <img align="absmiddle" src="../../resource/images/loading.gif">
</div>
<input class="controller" type="hidden" value="controllers" />

<? template_top(tt("Quản lý bản ghi") . "&nbsp;[" . number_format($total_record,0,".",",") . "]" )?>

<div class="pagi" style="position: absolute; top: 0; left: 50%; transform: translatex(-50%);">
	<?=generatePageBar($page_prefix,$current_page,$page_size,$total_record,$url,$normal_class,$selected_class);?>
</div>

<form action="listing.php" method="get" name="formSearch">
<table border="0" cellpadding="5" align="center">
	<tr>
		<td>
			<select name="limit" id="limit" class="form" onChange="this.form.submit();">
				<? foreach($arrLimit as $key => $row){?>
				<option value="<?=$key?>" <? if($key == $page_size){ echo 'selected';}?> ><?=$row?></option>
				<? }?>
			</select>
		</td>
        
		<td>
			<select title="Kiểu hiển thị video" name="show" class="form" onChange="this.form.submit()">
				<option value="0">Tùy chọn</option>
				<?
				$arrShow = array(1 => "Có ảnh", 2 => "Không có ảnh", 3 => "Kích hoạt", 4 => "Không kích hoạt", 5 => "Hot", 6 => "New", 7 => "Phong thủy");
				foreach($arrShow as $key => $value){
				?>
				<option title="<?=$value?>" value="<?=$key?>" <? if($key == $show){echo 'selected="selected"';}?>><?=$value?></option>
				<?
				}
				?>
			</select>
		</td>

		<td>
			<select name="iCat" id="iCat" class="form" onChange="this.form.submit()">
				<option value=""><?=tt("Chọn danh mục")?></option>
				<?
				$cat_type = "";
				foreach($listAll as $i=>$cat){

					if($cat_type != $cat["cat_type"]){
						$cat_type = $cat["cat_type"];
						?>
						<optgroup label="---- <?=ucwords($cat["typ_name"])?> -----"></optgroup>
						<?
					}
					?>
					
					<option value="<?=$cat["cat_id"]?>" <? if($iCat == $cat["cat_id"]){?>selected<? }?>>
						<?
						for($j=0;$j<$cat["level"];$j++) echo '|--';
						echo $cat["cat_name"];
						?>
					</option>
				<?
				}
				?>
			</select>
		</td>
		<td><input type="text" class="form" name="keyword" value="<?=$keyword?>" style="width:120px" /></td>       
		<td><input type="submit" value="<?=tt("Tìm kiếm")?>" class="form_button" /></td>
	</tr>
</table>
</form>

<form action="quickedit.php?returnurl=<?=base64_encode(getURL())?>" method="post" name="form_listing" enctype="multipart/form-data">
	<input type="hidden" name="iQuick" value="update">
    <table border="1" cellpadding="5" cellspacing="0" width="100%" style="border-collapse:collapse" bordercolor="<?=$bordercolor?>">
		<tr class="textBold" align="center">
			<td>&nbsp;</td>
			<td width="30"><?=translate_text("Lưu")?></td>
			<td><?=translate_text("Ảnh")?></td>
			<td>
				<div><?=translate_text("Tiêu đề")?></div>
				<div>
					<?=generate_sort("asc", 1, $sort, $fs_imagepath)?>
					<?=generate_sort("desc", 2, $sort, $fs_imagepath)?>
				</div>
			</td>
			<td>Link</td>
			<td width="80">
				<div><?=translate_text("Cập nhật")?></div>
				<div>
					<?=generate_sort("asc", 3, $sort, $fs_imagepath)?>
					<?=generate_sort("desc", 4, $sort, $fs_imagepath)?>
				</div>
			</td>
			<td width="80">
				<div><?=translate_text("Lượt xem")?></div>
				<div>
					<?=generate_sort("asc", 5, $sort, $fs_imagepath)?>
					<?=generate_sort("desc", 6, $sort, $fs_imagepath)?>
				</div>
			</td>
			<td class="textBold" width="5%"><?=tt("Hot")?></td>
			<td class="textBold" width="5%"><?=tt("New")?></td>
			<td class="textBold" width="5%"><?=tt("Phong Thủy")?></td>
			<td class="textBold" width="5%"><?=tt("Active")?></td>
			<td class="textBold" width="5%"><?=tt("Copy")?></td>
			<td class="textBold" width="5%"><?=tt("Sửa")?></td>
			<td class="textBold" width="5%"><?=tt("Xóa")?></td>
		</tr>
        
		<? 
		$countno = ($current_page-1) * $page_size;
		while($row = mysql_fetch_assoc($db_news->result)){
			$countno++;
		?>
		<tr <?=$fs_change_bg?> class="row-<?=$row["new_id"]?> item-row" align="center">
			<td><input type="checkbox" name="record_id[]" id="record_id_<?=$countno?>" value="<?=$row["new_id"]?>"></td>
			<td><img src="<?=$fs_imagepath?>save.png" border="0" style="cursor:pointer" onClick=" document.getElementById('record_id_<?=$countno;?>').checked=true ; document.form_listing.submit();" alt="Save"></td>
			<td>
				<? 
				$picture_path = $fs_filepath.$row['new_title_index'].'/';
				
				if( is_file($picture_path.$row['new_picture'])){
					$picture 	= $row['new_picture'];
					$picture_o 	= $picture_path.'m_'.$picture;
					$picture_s 	= $picture_path.'s_'.$picture;
				}else{
					$picture 	= $row['new_picture_web'];
					$picture_o 	= $picture;
					$picture_s 	= $picture;	
				} // End if($row["new_picture_web"] != '')
				
				if($row["new_picture"]!='' || $row["new_picture_web"]!=''){
				?>
					<img width="35" height="25" src="<?=$picture_s?>" onMouseOver="showtip('<img src=\'<?=$picture_o?>\'>','<?=$bordercolor?>')" onMouseOut="hidetip()">
				<? 
				} // End if($row["new_picture"]!='')
				?>
			</td>
			<td align="left"><input type="text" onKeyUp="document.getElementById('record_id_<?=$countno?>').checked=true" name="new_title_<?=$row["new_id"]?>" size="40" id="new_title_<?=$row["new_id"]?>" value="<?=replaceQ($row["new_title"])?>" class="form"></td>
			<td align="left">
				<input type="text" onKeyUp="document.getElementById('record_id_<?=$countno?>').checked=true" name="new_title_index_<?=$row["new_id"]?>" size="30" id="new_title_index_<?=$row["new_id"]?>" value="<?=replaceQ($row["new_title_index"])?>" class="form">
				<input name="new_title_check_<?=$row["new_id"]?>" value="<?=replaceQ($row["new_title_index"])?>" type="hidden">
			</td>
			<td>
				<div><?=date("d/m/Y", $row["new_date"])?></div>
				<div style="color:#666666; font-size:10px"><?=date("H:i A", $row["new_date"])?></div>
			</td> 
			<td><input type="text" id="new_hits_<?=$row["new_id"]?>" name="new_hits_<?=$row["new_id"]?>" class="form" style="width:40px; text-align:center" value="<?=$row["new_hits"]?>"></td>
			<td align="center"><img class="hot" name="<?=$row["new_hot"]?>" id="<?=$row["new_id"];?>" path="<?=$fs_imagepath?>" src="<?=$fs_imagepath?>active_<?=$row["new_hot"];?>.png" title="<?=tt("Kích hoạt bản ghi")?>" alt="<?=tt("Kích hoạt")?>" /></td>
			<td align="center"><img class="new" name="<?=$row["new_daily"]?>" id="<?=$row["new_id"];?>" path="<?=$fs_imagepath?>" src="<?=$fs_imagepath?>active_<?=$row["new_new"];?>.png" title="<?=tt("Kích hoạt bản ghi")?>" alt="<?=tt("Kích hoạt")?>" /></td>
			<td align="center"><img class="phongthuy" name="<?=$row["new_phongthuy"]?>" id="<?=$row["new_id"];?>" path="<?=$fs_imagepath?>" src="<?=$fs_imagepath?>active_<?=$row["new_phongthuy"];?>.png" title="<?=tt("Kích hoạt bản ghi")?>" alt="<?=tt("Kích hoạt")?>" /></td>
			<td align="center"><img class="active" name="<?=$row["new_active"]?>" id="<?=$row["new_id"];?>" path="<?=$fs_imagepath?>" src="<?=$fs_imagepath?>active_<?=$row["new_active"];?>.png" title="<?=tt("Kích hoạt bản ghi")?>" alt="<?=tt("Kích hoạt")?>" /></td>
			<td align="center"><img title="<?=tt("Sao chép bản ghi")?>" alt="<?=tt("Sao chép")?>" src="<?=$fs_imagepath?>copy.png" onClick="if (confirm('<?=tt("Bạn muốn sao chép bản ghi này?")?>')){ window.location.href='copy.php?record_id=<?=$row["new_id"]?>&returnurl=<?=base64_encode(getURL())?>'}" border="0" style="cursor:pointer"></td>
			<td><a href="edit.php?record_id=<?=$row["new_id"];?>&url=<?=base64_encode(getURL())?>"><img src="<?=$fs_imagepath?>edit.png" alt="<?=tt("Edit")?>" border="0"></a></td>
			<td align="center"><img class="remove" id="<?=$row["new_id"];?>" src="<?=$fs_imagepath?>delete.png" title="<?=tt("Xóa bản ghi")?>" alt="<?=tt("Xóa")?>" /></td>  
		</tr>
	<? 
	} // End while($row = mysql_fetch_assoc($db_news->result))
	unset($db_news);
	?>
	</table>
</form>

<? if($total_record > $page_size){ ?>     
<div class="pagi"><?=generatePageBar($page_prefix,$current_page,$page_size,$total_record,$url,$normal_class,$selected_class);?></div>
<? } ?>

<? template_bottom() ?>
</body>
</html>
