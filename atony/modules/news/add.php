<?
#+
#+ Mr.LeToan - NTP.VnJp
require_once("config_security.php");	//check security	 
checkAddEdit("add"); //check quyền thêm sửa xóa

#+
#+ Khai bao bien
$errorMsg 			= "";		//Warning Error!
$action				= getValue("action", "str", "POST", "");

$add				= "add.php";
$listing			= "listing.php";
$edit				= "edit.php";
$after_save_data	= getValue("after_save_data", "str", "POST", $edit);

$new_strdate		= getValue("new_strdate", "str", "POST", date("d/m/Y"));
$new_strtime		= getValue("new_strtime", "str", "POST", date("H:i:s"));
$new_date			= convertDateTime($new_strdate, $new_strtime);
$new_title_heading= "";
$new_description	= getValue("new_description", "str", "POST", "");
$new_title_index 	= getValue("new_title_index", "str", "POST", "");
#+
if($new_title_index == ''){
	$new_title_index 	= removeTitle(getValue("new_title", "str", "POST", ""),'/');
	$new_title_index 	= strtolower($new_title_index);
} // End if($new_title_index == ''){
	
#+
#+ Goi class generate form
$myform = new generate_form();	//Call Class generate_form();
$myform->removeHTML(0);	//Loại bỏ chức năng không cho điền tag html trong form
#+
#+ Khai bao bang du lieu
$myform->addTable($fs_table);	// Add table
#+
#+ Khai bao thong tin cac truong
$myform->add("new_category","new_category",1,0,0,1,tt("Chọn danh mục"),0,"");
$myform->add("new_title","new_title",0,0,"",1,tt("Điền tên tour"),0,"");
$myform->add("new_title_index","new_title_index",0,1,"",0,tt("Điền link rewrite tour"),1,tt("Đã tồn tại link rewrite"));
$myform->add("new_picture_web","new_picture_web",0,0,"",0,"",0,"");
$myform->add("new_source","new_source",0,0,"",0,"",0,"");
$myform->add("new_cache","new_cache",0,0,"",0,"",0,"");
$myform->add("new_date","new_date",1,1,0,0,"",0,"");
$myform->add("new_hot","new_hot",1,0,0,0,"",0,"");
$myform->add("new_new","new_new",1,0,0,0,"",0,"");
$myform->add("new_phongthuy","new_phongthuy",1,0,0,0,"",0,"");
$myform->add("new_teaser","new_teaser",0,0,"",0,"",0,"");
$myform->add("meta_title","meta_title",0,0,"",0,"",0,"");
$myform->add("meta_keyword","meta_keyword",0,0,"",0,"",0,"");
$myform->add("meta_description","meta_description",0,0,"",0,"",0,"");

#+
#+ đổi tên trường thành biến và giá trị
$myform->evaluate();

#+
#+ Nếu như có form được submit
if($action == "submitForm"){
	#+
	#+ Tao duong dan cho folder anh
	//*/
	$fs_filepath = $fs_filepath.$new_title_index.'/';
	if(!is_dir($fs_filepath)){
		$oldumask = umask(0);
		mkdir($fs_filepath,0777,true);
		umask($oldumask);	
	} // End if(!is_dir($fs_filepath))
	//*/
	//
	
	$arrHeading      = parse_heading($new_description);
	
	if(is_array($arrHeading) && count($arrHeading) > 0){

		$new_title_heading = base64_url_encode(json_encode($arrHeading["list"]));
		
		$new_description   = (isset($arrHeading["data"]) ? $arrHeading["data"] : $new_description);

	}
	$myform->add("new_description","new_description",0,1,"",0,"",0,"");
	$myform->add("new_title_heading","new_title_heading",0,1,"",0,"",0,"");

	#+
	#+ Xu ly anh được up lên
	$upload_pic = new upload("picture", $fs_filepath, $extension_list, $limit_size, $new_title_index);
	if ($upload_pic->file_name != ""){
		$picture = $upload_pic->file_name;
		//resize anh
		resize_image($fs_filepath,$upload_pic->file_name,$small_width,$small_heght,$small_quantity,"s_");
		resize_image($fs_filepath,$upload_pic->file_name,$medium_width,$medium_heght,$medium_quantity,"m_");
		$myform->add("new_picture","picture",0,1,"",0,"",0,"");
	} // End if ($upload_pic->file_name != ""){

	#+
	#+ Kiểm tra lỗi
    $errorMsg .= $myform->checkdata();
	$errorMsg .= $myform->strErrorFeld ;	//Check Error!
	$errorMsg .= $upload_pic->show_warning_error();
	
	#+
	#+ Nếu như không có lỗi
    if($errorMsg == ""){
				
		#+
		#+ Them gia tri truong search
		$myform->add_Field_Seach("new_search",array("new_title"=>0,"new_teaser"=>0,"new_description"=>0));
		
        #+
		#+ Thuc hien query
		$db_ex	 		= new db_execute_return();
		$query			= $myform->generate_insert_SQL();
		$last_id 		= $db_ex->db_execute($query);
		$record_id 		= $last_id;
		//echo $query;exit();
		
		#+
		#+ Xu ly tags
		$array_tags = explode(',',$meta_keyword);
		$tag = new tag($new_description,$array_tags);
		$tag_keyword = $tag->insert_tag();
		unset($tag);
		#+
		#+ Neu nhu meta_keyword = '' thi luu meta_keyword dang tag tu dong
		if($meta_keyword == ''){
			$tag_keyword = $tag_keyword;
			$query = 'UPDATE tbl_news SET meta_keyword = "'.$tag_keyword.'" WHERE new_id = '.$record_id;
			$db_ex = new db_execute($query);
			unset($db_ex);
		} // End if($meta_keyword == '')

		#+
		#+ Chuyen ve trang khac khi xu ly du lieu oki
		redirect($after_save_data . "?record_id=" . $record_id);
		exit();
		
    } // End if($errorMsg == ""){
	
} // End if($action == "submitForm")


#+
#+ Khai bao ten form 
$myform->addFormname("submitForm"); //add  tên form để javacheck
#+
#+ Xử lý javascript
$myform->addjavasrciptcode('');
$myform->checkjavascript();
?>

<html>
<head>
<title>Administrator Control panel</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<?=$load_header?>
</head>
<body>
<? template_top(tt("Quản lý bản ghi"))?>

<table class="tab" border="0" cellpadding="0" cellspacing="0">
    <tr align="center">
        <td nowrap>
            <div class="n" id="tabt_1" onClick="tabdiv(1,5);document.cookie='tab_select=1'"><?=tt("Chung")?></div>
        </td>
        <td nowrap>
            <div class="n" id="tabt_2" onClick="tabdiv(2,5);document.cookie='tab_select=2'"><?=tt("Tóm tắt")?></div>
        </td>
        <td nowrap>
            <div class="n" id="tabt_3" onClick="tabdiv(3,5);document.cookie='tab_select=3'"><?=tt("Chi tiết")?></div>
        </td>
		<td nowrap>
            <div class="n" id="tabt_4" onClick="tabdiv(4,5);document.cookie='tab_select=4'"><?=tt("SEO")?></div>
        </td>
        <td style="border-bottom:solid 1px #DADADA;" width="99%">&nbsp;</td>
    </tr>
</table>

<?
$fs_action			= $_SERVER['SCRIPT_NAME'] . "?" . @$_SERVER['QUERY_STRING'];
?>
<?=$myform->create_form("submitForm", $fs_action, "post", "multipart/form-data","");?>


<div style="border:solid 1px #DADADA; border-top-width:0; padding:10px;">
	<div id="tabc_1" class="tabc">
		<?=$myform->create_table(8,0,"");?>
		<?=$myform->text_note()?>
        <?=$myform->errorMsg($errorMsg)?>							
        <tr>
            <td nowrap="nowrap" align="right"><font color="#FF0000">*</font> <?=tt("Chọn danh mục")?> :</td>
            <td>
                <select name="new_category" id="new_category" class="form">
                    <option value=""><?=tt("Chọn danh mục")?></option>
                    
                    <?
					$cat_type = "";
                    foreach($listAll as $i=>$cat){
						
						#+
						if($cat_type != $cat["cat_type"]){
							$cat_type = $cat["cat_type"];
						?>
							<optgroup label="---- <?=ucwords($cat["typ_name"])?> -----"></optgroup>
						<?
						}
						?>
                            <option value="<?=$cat["cat_id"]?>" <? if($new_category == $cat["cat_id"]){?>selected<? }?>>
                            <?
                            for($j=0;$j<$cat["level"];$j++) echo '|--';
                            echo $cat["cat_name"];
                            ?>
                            </option>
                        <?
                    }
                    ?>
                </select>	
            </td>
        </tr>
        
        <?=$myform->text(1 , "Tiêu đề tin tức", "new_title", "new_title", $new_title, "Tiêu đề tin tức", 250, "", 255, "", "", "")?>
        <script language="javascript">
            $(document).ready(function(){
                $('#getDate').click(function(){
                    $('#new_strdate').val('<?=date("d/m/Y")?>');
                    $('#new_strtime').val('<?=date("H:i:s")?>');
                })
            })
        </script>
        <?=$myform->text(0, "Ngày cập nhật", "new_strdate" . $myform->ec . "new_strtime", "new_strdate" . $myform->ec . "new_strtime", $new_strdate . $myform->ec . $new_strtime, "Ngày (dd/mm/yyyy)" . $myform->ec . "Giờ (hh/mm/ss)", "70" . $myform->ec . "70", $myform->ec, "10" . $myform->ec . "10", " - ", $myform->ec, "&nbsp; <input id='getDate' type='button' value='Get date' class='form_button'>");?>
        <?=$myform->checkbox(0, "Loại tin tức", "new_hot" . $myform->ec . "new_new" . $myform->ec . "new_phongthuy", "new_hot" . $myform->ec . "new_new" . $myform->ec . "new_phongthuy", "1" . $myform->ec . "1" . $myform->ec . "1", "0" . $myform->ec . "0" . $myform->ec . "0", " Tin hot " . $myform->ec . " Tin mới " . $myform->ec . " Tin phong thủy ", "");?>
        <tr> 
            <td align="right" nowrap><?=tt("Picture")?>:</td>
            <td><input type="file" name="picture" id="picture" size="30" class="form"></td>
        </tr>
        <?=$myform->text(0 , "Picture Web", "new_picture_web", "new_picture_web", $new_picture_web, "Picture Web", 600, "", 255, "", "", '<i class="tS11"> Vd: http://ten-mien.com/*.jpg</i>')?>
        <?=$myform->text(0 , "Nguồn", "new_source", "new_source", $new_source, "Nguồn", 600, "", 255, "", "", '<i class="tS11"> Vd: Vnexpress, dân trí</i>')?>
        <?=$myform->text(0 , "Cache", "new_cache", "new_cache", $new_cache, "Cache", 600, "", 255, "", "", '<i class="tS11"> Vd: http://ten-mien-nguon.com</i>')?>
        <?=$myform->close_table();?>
    </div>
    
    <div id="tabc_2" class="tabc">
		<textarea name="new_teaser" id="new_teaser" class="form_control" style="width:90%; height:300px;"><?=$new_teaser?></textarea>
    </div>
    
    <div id="tabc_3" class="tabc">
		<?=$myform->wysiwyg("", "new_description", $new_description, "../../resource/ckeditor/", "99%", 300)?>
    </div>
	
	<div id="tabc_4" class="tabc">
		<div class="mart10">
			Đường dẫn :
		</div>
		<input type="text" name="new_title_index" id="new_title_index" value="<?=$new_title_index?>" class="form_control" style="width:50%">
		<div class="mart10">
			Google Title :
		</div>
        <textarea name="meta_title" id="meta_title" class="form_control" style="width:90%; height:50px;"><?=$meta_title?></textarea>
        
		<div class="mart10">
			Google Description :
		</div>
		<textarea name="meta_description" id="meta_description" class="form_control" style="width:90%; height:100px;"><?=$meta_description?></textarea>
		<div class="mart10">
			Google Keyword : <span> - <i class="font11">(Các keyword cách nhau bằng dầu phẩy ',')</i></span>
		</div>
    	<textarea name="meta_keyword" id="meta_keyword" class="form_control" style="width:90%; height:100px;"><?=$meta_keyword?></textarea>
		
    </div>

	<?=$myform->create_table(8,3,"");?>
    <?=$myform->radio(0 , "Sau khi lưu dữ liệu", "add_new" . $myform->ec . "return_listing" . $myform->ec . "return_edit", "after_save_data", $add . $myform->ec . $listing . $myform->ec . $edit, $after_save_data, "Thêm mới" . $myform->ec . "Quay về danh sách" . $myform->ec . "Sửa bản ghi", "");?>
    <?=$myform->button("button" . $myform->ec . "reset", "button" . $myform->ec . "reset", "button" . $myform->ec . "reset", "Cập nhật"  . $myform->ec .  "Làm lại", "", "");?>
    <?=$myform->hidden("action", "action", "submitForm", "");?>
    <?=$myform->close_table();?>
</div>

<?
$tab_select = getValue("tab_select","int","COOKIE",1);
?>
<script>tabdiv(<?=$tab_select?>,5)</script>


<?=$myform->close_form();?>
<? unset($myform);?>
<? template_bottom() ?>
<? /*------------------------------------------------------------------------------------------------*/ ?>
</body>
</html>