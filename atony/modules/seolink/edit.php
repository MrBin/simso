<?
#+
#+ Mr.LeToan - NTP.VnJp
require_once("config_security.php");

#+
$record_id			= getValue("record_id","int","GET");

#+
#+ Kiem tra quyen voi ban ghi
checkAddEdit("edit");	
checkRowUser($fs_table, $field_id, $record_id, base64_encode('listing.php'));

#+
#+ Lay ra ban ghi dang sua
$query  = "SELECT * FROM $fs_table WHERE ".$field_id." = '".$record_id."'";
$db_query = new db_query($query);
$row = mysql_fetch_assoc($db_query->result);
$db_query->close();
unset($db_query);

#+
#+ Khai bao bien
$errorMsg 			= "";		//Warning Error!
$action				= getValue("action", "str", "POST", "");

$add				= "add.php";
$listing			= "listing.php";
$edit				= "edit.php";
$after_save_data	= getValue("after_save_data", "str", "POST", $listing);

$seo_strdate		= getValue("seo_strdate", "str", "POST", date("d/m/Y",$row['seo_date']));
$seo_strtime		= getValue("seo_strtime", "str", "POST", date("H:i:s",$row['seo_date']));
$seo_date			= convertDateTime($seo_strdate, $seo_strtime);
	
#+
#+ Goi class generate form
$myform = new generate_form();	//Call Class generate_form();
$myform->removeHTML(0);	//Loại bỏ chức năng không cho điền tag html trong form
#+
#+ Khai bao bang du lieu
$myform->addTable($fs_table);	// Add table
#+
#+ Khai bao thong tin cac truong
$myform->add("seo_link","seo_link",0,0,"",0,tt("Điền link SEO"),0,"");
$myform->add("seo_link_redirect","seo_link_redirect",0,0,"",0,"",0,"");
$myform->add("seo_title","seo_title",0,0,"",0,tt("Điền title SEO"),0,"");
$myform->add("seo_description","seo_description",0,0,"",0,tt("Điền description SEO"),0,"");
$myform->add("seo_keyword","seo_keyword",0,0,"",0,tt("Điền keyword SEO"),0,"");
$myform->add("seo_h1","seo_h1",0,0,"",0,"",0,"");
$myform->add("seo_h1d","seo_h1d",0,0,"",0,"",0,"");
$myform->add("seo_h2","seo_h2",0,0,"",0,"",0,"");
$myform->add("seo_h2d","seo_h2d",0,0,"",0,"",0,"");
$myform->add("seo_h3","seo_h3",0,0,"",0,"",0,"");
$myform->add("seo_h3d","seo_h3d",0,0,"",0,"",0,"");
$myform->add("seo_date","seo_date",1,1,0,0,"",0,"");

#+
#+ Nếu như có form được submit
if($action == "submitForm"){

	#+
	#+ Kiểm tra lỗi
    $errorMsg .= $myform->checkdata();
	$errorMsg .= $myform->strErrorFeld ;	//Check Error!

	#+
	#+ Nếu như không có lỗi
    if($errorMsg == ""){
		
        #+
		#+ Thuc hien query
		$query = $myform->generate_update_SQL($field_id,$record_id);
		$db_ex = new db_execute($query);
		//echo $query;exit();
		
		#+
		#+ Chuyen ve trang khac khi xu ly du lieu oki
		redirect($after_save_data . "?record_id=" . $record_id);
		exit();
		
    } // End if($errorMsg == ""){
	
} // End if($action == "submitForm")

#+
#+ Khai bao ten form 
$myform->addFormname("submitForm"); //add  tên form để javacheck
#+
#+ đổi tên trường thành biến và giá trị
$myform->evaluate();
#+
#+ Xử lý javascript
$myform->addjavasrciptcode('');
$myform->checkjavascript();

#+
#+ Chuyen ten truong thanh bien cho de dung
$db_data 	= new db_query("SELECT * FROM " . $fs_table . " WHERE " . $field_id . " = " . $record_id);
if($row 		= mysql_fetch_assoc($db_data->result)){
	foreach($row as $key=>$value){
		if($key!='lang_id' && $key!='admin_id') $$key = $value;
	}
}else{
		exit();
} // End if($row 		= mysql_fetch_assoc($db_data->result))
?>

<html>
<head>
<title>Administrator Control panel</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<?=$load_header?>
</head>
<body>
<? template_top(tt("Quản lý bản ghi"))?>

<table class="tab" border="0" cellpadding="0" cellspacing="0">
    <tr align="center">
        <td nowrap>
            <div class="n" id="tabt_1" onClick="tabdiv(1,5);document.cookie='tab_select=1'"><?=tt("Chung")?></div>
        </td>
        <td nowrap>
            <div class="n" id="tabt_2" onClick="tabdiv(2,5);document.cookie='tab_select=2'"><?=tt("Header 1")?></div>
        </td>
        <td nowrap>
            <div class="n" id="tabt_3" onClick="tabdiv(3,5);document.cookie='tab_select=3'"><?=tt("Header 2")?></div>
        </td>
        <td nowrap>
            <div class="n" id="tabt_4" onClick="tabdiv(4,5);document.cookie='tab_select=4'"><?=tt("Header 3")?></div>
        </td>
        <td style="border-bottom:solid 1px #DADADA;" width="99%">&nbsp;</td>
    </tr>
</table>

<?
$fs_action			= $_SERVER['SCRIPT_NAME'] . "?" . @$_SERVER['QUERY_STRING'];
?>
<?=$myform->create_form("submitForm", $fs_action, "post", "multipart/form-data","");?>


<div style="border:solid 1px #DADADA; border-top-width:0; padding:10px;">
	<div id="tabc_1" class="tabc">
		<?=$myform->create_table(8,0,"");?>
		<?=$myform->text_note()?>
		<?=$myform->errorMsg($errorMsg)?>							
      <div class="mart10">
			Đường dẫn :
		</div>
		<input type="text" name="seo_link" id="seo_link" value="<?=$seo_link?>" class="form_control" style="width:50%">
		
		<div class="mart10">
			<span style="color: red;">Link Redirect 301 : (Chú ý khi dùng vì nếu điền sai sẽ ảnh hưởng đến website)</span>
		</div>
		<input type="text" name="seo_link_redirect" id="seo_link_redirect" value="<?=$seo_link_redirect?>" class="form_control" style="width:50%">
		
		<div class="mart10">
			Google Title :
		</div>
        <textarea name="seo_title" id="seo_title" class="form_control" style="width:90%; height:50px;"><?=$seo_title?></textarea>
        
		<div class="mart10">
			Google Description :
		</div>
		<textarea name="seo_description" id="seo_description" class="form_control" style="width:90%; height:100px;"><?=$seo_description?></textarea>
		<div class="mart10">
			Google Keyword : <span> - <i class="font11">(Các keyword cách nhau bằng dầu phẩy ',')</i></span>
		</div>
    	<textarea name="seo_keyword" id="seo_keyword" class="form_control" style="width:90%; height:100px;"><?=$seo_keyword?></textarea>
	
        <?=$myform->text(0, "Ngày cập nhật", "seo_strdate" . $myform->ec . "seo_strtime", "seo_strdate" . $myform->ec . "seo_strtime", $seo_strdate . $myform->ec . $seo_strtime, "Ngày (dd/mm/yyyy)" . $myform->ec . "Giờ (hh/mm/ss)", "70" . $myform->ec . "70", $myform->ec, "10" . $myform->ec . "10", " - ", $myform->ec, "");?>
        <?=$myform->close_table();?>
    </div>
    
    <div id="tabc_2" class="tabc">
    	<div>
    		Header 1 (&lt;h1&gt;...&lt;/h1&gt;)
        </div>
    	<input type="text" name="seo_h1" id="seo_h1" value="<?=$seo_h1?>" class="form_control" style="width:50%">
        <div>
    		Mô tả H1
        </div>
		<?=$myform->wysiwyg("", "seo_h1d", $seo_h1d, "../../resource/ckeditor/", "99%", 250)?>
    </div>
    
     <div id="tabc_3" class="tabc">
    	<div>
    		Header 2 (&lt;h2&gt;...&lt;/h2&gt;)
        </div>
    	<input type="text" name="seo_h2" id="seo_h2" value="<?=$seo_h2?>" class="form_control" style="width:50%">
        <div>
    		Mô tả H2
        </div>
		<?=$myform->wysiwyg("", "seo_h2d", $seo_h2d, "../../resource/ckeditor/", "99%", 250)?>
    </div>
    
     <div id="tabc_4" class="tabc">
    	<div>
    		Header 3 (&lt;h3&gt;...&lt;/h3&gt;)
        </div>
    	<input type="text" name="seo_h3" id="seo_h3" value="<?=$seo_h3?>" class="form_control" style="width:50%">
        <div>
    		Mô tả H3
        </div>
		<?=$myform->wysiwyg("", "seo_h3d", $seo_h3d, "../../resource/ckeditor/", "99%", 250)?>
    </div>

	<?=$myform->create_table(8,3,"");?>
    <?=$myform->radio(0 , "Sau khi lưu dữ liệu", "add_new" . $myform->ec . "return_listing" . $myform->ec . "return_edit", "after_save_data", $add . $myform->ec . $listing . $myform->ec . $edit, $after_save_data, "Thêm mới" . $myform->ec . "Quay về danh sách" . $myform->ec . "Sửa bản ghi", "");?>
    <?=$myform->button("button" . $myform->ec . "reset", "button" . $myform->ec . "reset", "button" . $myform->ec . "reset", "Cập nhật"  . $myform->ec .  "Làm lại", "", "");?>
    <?=$myform->hidden("action", "action", "submitForm", "");?>
    <?=$myform->close_table();?>
</div>

<?
$tab_select = getValue("tab_select","int","COOKIE",1);
?>
<script>tabdiv(<?=$tab_select?>,5)</script>

<?=$myform->close_form();?>
<? unset($myform);?>
<? template_bottom() ?>
<? /*------------------------------------------------------------------------------------------------*/ ?>
</body>
</html>