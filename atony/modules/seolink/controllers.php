<?php
require_once("config_security.php");

if( !checkAddEdit('edit', true) )
{
    $data['error'] = tt('Không đủ quyền thực hiện hành động');
    echo json_encode($data); return;
}

$task  = !empty($_POST) ? getValue("task", "str", "POST") : getValue("task", "str", "GET");
$name  = !empty($_POST) ? getValue("name", "str", "POST") : getValue("name", "str", "GET");
$value = !empty($_POST) ? getValue("value", "str", "POST") : getValue("value", "str", "GET");
$state = !empty($_POST) ? getValue("state", "int", "POST") : getValue("state", "int", "GET");
$id    = !empty($_POST) ? getValue("id", "int", "POST") : getValue("id", "int", "GET");

$data          = array();
$data['error'] = '';
$data['mess']  = '';


switch($task)
{
	case 'active':
	case 'hot':
	case 'new':
		$publish = ($state==1) ? 0 : 1;
		
		if( !singleUpdate($fs_table, $field_id, 'seo_' . $task, $publish, $id) ){
			$data['error'] = tt('Có lỗi xảy ra khi thực hiện câu lệnh query');
		}else{
			$data['mess']  = tt('Kích hoạt bản ghi thành công');
		}
		
		echo json_encode($data); return;
	break;
	

	case 'remove':
		if( !removeItem($fs_table, $field_id, $id) ){
			$data['error']  = tt('Có lỗi khi thực hiện câu lệnh query');	
		}else{
			$data['mess']  = tt('Bản ghi đã được xóa');
		}

		echo json_encode($data); return;
	break;
	
    default:
        return false;
}

function singleUpdate($table, $index='cat_id', $name, $value, $id='')
{
    if( (int)$id > 0 ){
        $where = " WHERE ".$index." = ".$id;
    }
    $query      = " UPDATE ".$table." SET ".$name." = '".$value."' ".$where;
    $dbExute    = new db_execute($query);
    
    if(!$dbExute){
        return false;
    }
	
	return true;
}


function removeItem($table, $index='seo_id', $ids='')
{
	global $fs_table;
	global $field_id;
	global $field_name;
	global $fs_filepath;
	global $fs_filepathgallery;

    if( is_integer($ids) ){
        $ids = array($ids);
    }
    if( !empty($ids) ){
        $where = " WHERE ".$index." IN(".implode(',', $ids).") ";
    }
    
    $query      = " DELETE FROM ".$table.$where;
    $dbExute    = new db_execute($query);
	
    if(!$dbExute){
        return false;
    }

    return true;
}
?>