<?php
require_once("config_security.php");

//Keyword filter
$limit     	 	= getValue("limit", "int", "GET");
$keyword		= getValue("keyword", "str", "GET", "", 1);
$iType			=  getValue("iType");

$sql 			= "";
if($keyword!="") $sql.=" AND (simcard LIKE '%" . $keyword . "%')";
if($iType!="") $sql.=" AND phanloai =" . $iType . "";


$show			= getValue("show");
if($show > 0){
	switch($show){
		case 1:	
			$sql .= " AND tinhtrang = 0 "; 
			break;
		case 2:	
			$sql .= " AND tinhtrang = 1 "; 
			break;
	}
}
$dateFrom = getValue("dateFrom","str","GET","Từ");
$dateTo = getValue("dateTo","str","GET","Đến");

if($dateFrom != "Từ"){
	$sql	.= 	"AND thoigian >= '" . $dateFrom . "'";
}
if($dateTo != "Đến"){
	$sql	.= 	"  AND thoigian <= '" . $dateTo . "'";
}

//Set table name & initializing database
$db	      = new dbQuery;

//
$normal_class    = "page";
$selected_class  = "pageselect";
$page_prefix     = "Trang : ";
$current_page    = ( getValue("page") < 1 ) ? 1 : getValue("page");
$page_size	     = ( $limit > 0 ) ? $limit : 15;


$db_count = new db_query("SELECT Count(*) AS count"
		." FROM " . $fs_table
		." WHERE lang_id=" . $_SESSION["lang_id"]
		." ".$sql
		);
$row_count 		= mysql_fetch_assoc($db_count->result);
$total_record 	= $row_count['count'];
$db_count->close();
$url = "listing.php?keyword=" . $keyword . "&limit=" . $limit . "&iType=" . $iType . "&dateFrom=".$dateFrom."&dateTo=".$dateTo."&show=".$show."&page=";


$query  =" SELECT * FROM " . $fs_table
		." WHERE lang_id=" . $_SESSION["lang_id"]
		." ".$sql
		." ORDER BY id DESC, thoigian DESC"
		." LIMIT " . ($current_page-1) * $page_size . "," . $page_size;
$db->setQuery($query);
$rows   = $db->loadObjectList();
?>

<html>
<head>
<title>Channel Listing</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<?=$load_header?>


<link rel="stylesheet" href="../../resource/js/colorbox/colorbox.css" />
<script language="javascript" src="../../resource/js/colorbox/jquery.colorbox-min.js"></script>
<script type="text/javascript">
	$(document).ready(function(){
		//Examples of how to assign the ColorBox event to elements
		//$(".example6").colorbox({iframe:true, innerWidth:1100, innerHeight:300});
		$(".example7").colorbox({width:"90%", height:"100%", iframe:true});
		$(".example7").click(function(){
			$idCss = $(this).attr('id');
			$('.bgview-'+$idCss).css('background','#FFF');
		})
	});
</script>


<script type="text/javascript" src="../../resource/js/date/jquery.ui.datepicker-vi.js"></script>
<script type="text/javascript">
$(function() {
	$('#dateFrom, #dateTo').datepicker({
		numberOfMonths: 1,
		showButtonPanel: true,
		dateFormat: 'yy-mm-dd',
		changeMonth: true,
	});
});
</script>
</head>

<body>
<!-- Load css và xử lý dữ liệu của jquery(kích hoạt, xóa bản ghi) -->
<div id="loadingBar">
    Đang xử lý ... Vui lòng đợi trong giây lát ... <br>
    <img align="absmiddle" src="../../resource/images/loading.gif">
</div>
<input class="controller" type="hidden" value="controllers" />

<?=template_top(tt("Danh sách bản ghi") . "&nbsp;[" . number_format($total_record,0,".",",") . "]")?>

<form action="listing.php" method="GET" name="formSearch">
<table border="0" cellpadding="2" align="center">
	<tr>
    	<td>
        	<input type="text" id="dateFrom" name="dateFrom" value="<?=$dateFrom?>" class="form" style="text-align:center; width:90px;">
			<input type="text" id="dateTo" name="dateTo" value="<?=$dateTo?>" class="form" style="text-align:center; width:90px;">
        </td>
        <td>
        	<select title="Kiểu hiển thị tin" name="show" class="form">
                <option value="0"><?=tt("Chọn kiểu hiển thị")?></option>
                <?
                $arrShow = array(1 => "Chưa đọc", 2 => "Đã đọc");
                foreach($arrShow as $key => $value){
                ?>
                    <option title="<?=$value?>" value="<?=$key?>" <? if($key == $show){echo 'selected="selected"';}?>><?=$value?></option>
                <?
                }
                ?>
            </select>
        </td>
    	<td>
			<?
            $arrLimit = array(
                '5' => '5 items',
                '15' => '15 items',
                '30' => '30 items',
                '50' => '50 items',
                '80' => '80 items',
                '120' => '120 items'
            );
            ?>
            <select name="limit" id="limit" class="form" onChange="this.form.submit();">
            	<? foreach($arrLimit as $key => $row){?>
                <option value="<?=$key?>" <? if($key == $page_size){ echo 'selected';}?> ><?=$row?></option>
                <? }?>
            </select>
        </td>
        
        <td>
        	<select name="iType" id="iType" class="form">
            	<option value=""><?=tt("Chọn phân loại")?></option>
				<?
                foreach($arrType as $key=>$type){
                $selected = $key == $iType ? 'selected' : '';
                ?>
                    <option <?=$selected?> value="<?=$key?>"><?=$type?></option>
                <?
                }
                ?>
            </select>
        </td>
        
        <td><input type="text" class="form" name="keyword" value="<?=$keyword?>" style="width:120px" /></td>       
		<td><input type="submit" value="<?=tt("Tìm kiếm")?>" class="form_button" /></td>
	</tr>
</table>
</form>

<div class="pagi"><?=generatePageBar($page_prefix,$current_page,$page_size,$total_record,$url,$normal_class,$selected_class);?></div>
<form action="quickedit.php?returnurl=<?=base64_encode(getURL())?>" method="post" name="form_listing" enctype="multipart/form-data">
	<input type="hidden" name="iQuick" value="update">
    <table border="1" cellpadding="2" cellspacing="0" width="100%" style="border-collapse:collapse" bordercolor="<?=$bordercolor?>">
        <tr align="center" height="40" class="textBold">
            <td width="5%">&nbsp;</td>
            <td><?=tt("Thời gian")?></td>
            <td><?=tt("Khách hàng")?></td>
            <td><?=tt("Điện thoại")?></td>
            <td><?=tt("Phân loại")?></td>
            <td><?=tt("Xem")?></td>
            <td><?=tt("Xóa")?></td>
        </tr>
        
        <?
		$countno = 0;
        foreach( $rows as $row ){
		$countno++;
		$bg 		= "";
		$bg 		= $row->phanloai == 1 ? "background:#DAF2D8" : $bg;
		$bg 		= $row->phanloai == 2 ? "background:#99CCFF" : $bg;
		$bg 		= $row->phanloai == 3 ? "background:#FFFFCC" : $bg;
		$bg 		= $row->phanloai == 4 ? "background:#FC6" : $bg;
		$bgactive 	= $row->tinhtrang == 0 ? "background:#99FF00" : "";
        ?>
        
        <tr <?=$fs_change_bg?> class="row-<?=$row->id?> item-row" align="center">
            <td><input type="checkbox" name="record_id[]" id="record_id_<?=$countno?>" value="<?=$row->id?>"></td>
			
            <td><?=$row->thoigian?></td>
            <td><?=$row->hoten?></td>
            <td><?=$row->dienthoai?></td>
            <td style=" <?=$bg?>"><?=$arrType[$row->phanloai]?></td>
            <td class="bgview-<?=$row->id?>" style=" <?=$bgactive?>;"><a id="<?=$row->id?>" class='example7' href="view.php?record_id=<?=$row->id?>"><img src="<?=$fs_imagepath?>view.gif" border="0"></a></td>
            <td><img class="remove" id="<?=$row->id?>" src="<?=$fs_imagepath?>delete.png" /></td>
        </tr>       
        <?
        }
        ?>	
    </table>
    <div class="pagi"><?=generatePageBar($page_prefix,$current_page,$page_size,$total_record,$url,$normal_class,$selected_class);?></div>

<?php echo template_bottom(); ?>
</body>
</html>