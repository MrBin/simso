<?
require_once("config_security.php");

// Kiem tra quyen xoa
checkAddEdit("delete");	// Check quyền thêm sửa xóa

// Khai bao bien
$fs_redirect	= base64_decode(getValue("returnurl","str","GET",base64_encode("listing.php")));
$record_id		= getValue("record_id","int","GET");

// Kiem tra quyen user voi ban ghi
checkRowUser($fs_table,$field_id,$record_id,$fs_redirect);

// Xoa file
delete_file($fs_table,$field_id,$record_id,"cat_picture",$fs_filepath);

// Xoa ban ghi
$db_delete = new db_execute("UPDATE " . $fs_table . " SET cat_picture = '' WHERE cat_id = " . $record_id);
unset($db_delete);

redirect($fs_redirect);
?>