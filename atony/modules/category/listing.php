<?
require_once("config_security.php");

// Khai bao bien
$fs_action			= $_SERVER['SCRIPT_NAME'] . "?" . @$_SERVER['QUERY_STRING'];
$onChange			= "submit()";
$sql					= "";
$sort					= getValue("sort");
$cat_parent_id 	= getValue("cat_parent_id");
$cat_type			= getValue("cat_type","str","GET","");

// sap xep du lieu
switch($sort){
	case 1: $sqlOrderBy = "cat_type, cat_order ASC"; break;
	case 2: $sqlOrderBy = "cat_type, cat_order DESC"; break;
	case 3: $sqlOrderBy = "cat_type, cat_name ASC"; break;
	case 4: $sqlOrderBy = "cat_type, cat_name DESC"; break;
	default:$sqlOrderBy = "cat_type, cat_order ASC, cat_id"; break;
}

// Dua ra sql cho phu hop
if($cat_type!="") $sql = "cat_type = '" . $cat_type . "' AND ";
	
// Lay ra category phu hop
$menu				= new menu();
$listAll			= $menu->getAllChild("tbl_category INNER JOIN tbl_category_type ON (typ_name_index = cat_type)", "cat_id", "cat_parent_id", $cat_parent_id, $sql . " tbl_category.lang_id = " . $_SESSION["lang_id"], "cat_id,cat_name,cat_order,cat_type,cat_parent_id,cat_has_child,cat_picture,cat_active,cat_show,cat_home,cat_name_index", 'typ_order,'.$sqlOrderBy, "cat_has_child", 0);
unset($menu);

// Dem so luong ban ghi
$total_record	= count($listAll);
?>

<html>
<head>
<title>Channel Listing</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<?=$load_header?>
</head>

<body>
<!-- Load css và xử lý dữ liệu của jquery(kích hoạt, xóa bản ghi) -->
<div id="loadingBar">
    Đang xử lý ... Vui lòng đợi trong giây lát ... <br>
    <img align="absmiddle" src="../../resource/images/loading.gif">
</div>
<input class="controller" type="hidden" value="controllers" />

<? template_top(tt("Quản lý bản ghi") . "&nbsp;[" . number_format($total_record,0,".",",") . "]" )?>

<div style="position: absolute; top: 0; right: 5px;">
	<? $form = new form();?>
	<?=$form->create_form("search", $fs_action, "get", "multipart/form-data","");?>
	<div style="display: inline-block;">
		<?=$form->create_table();?>
			<?=$form->select(0 ,"Loại danh mục", "cat_type", "cat_type", $arrCatType, $cat_type, "", 1, 0, 'onChange="' . $onChange . '"', "")?>
		<?=$form->close_table();?>
	</div>
	<div style="display: inline-block;">
		<?=$form->create_table();?>	
			<tr>
				<td class="form_name"><?=tt("Danh mục")?></td>
				<td class="form_text">
					<div id="content_loader">		
						<select title="<?=tt("Danh mục cấp trên")?>" id="cat_parent_id" name="cat_parent_id" class="form_control" onChange="<?=$onChange?>">
							<option value=""><?=tt("Danh mục cấp trên")?></option>
							<?
							for($i=0; $i<count($listAll); $i++){
								$selected = ($cat_parent_id == $listAll[$i]["cat_id"]) ? ' selected="selected"' : '';
								echo '<option title="' . htmlspecialbo($listAll[$i]["cat_name"]) . '" value="' . $listAll[$i]["cat_id"] . '"' . $selected . '>';
								for($j=0; $j<$listAll[$i]["level"]; $j++) echo ' |--';
								echo ' ' . cut_string($listAll[$i]["cat_name"], 55) . '</option>';
								
							}
							?>
						</select>
					</div>
				</td>
			</tr>
		<?=$form->close_table();?>
	</div>
	<?=$form->close_form();?>
	<? unset($form);?>	
</div>	

<form action="quickedit.php?returnurl=<?=base64_encode($_SERVER['REQUEST_URI'])?>" method="post" name="form_listing" id="form_listing" enctype="multipart/form-data">
<table border="1" cellpadding="0" cellspacing="0" width="100%" style="border-collapse:collapse" bordercolor="<?=$bordercolor?>">
	<tr class="textBold" align="center"> 
		<td><input type="checkbox" id="check_all" onClick="check('1','1000')"></td>
		<td><?=tt("Lưu")?></td>
		<td><?=tt("Ảnh")?></td>
		<td>
			<div><?=tt("Thứ tự")?> - <?=tt("Danh mục")?></div>
			<div>
				<?=generate_sort("asc", 1, $sort, $fs_imagepath)?>
				<?=generate_sort("desc", 2, $sort, $fs_imagepath)?>
				&nbsp;&nbsp;
				<?=generate_sort("asc", 3, $sort, $fs_imagepath)?>
				<?=generate_sort("desc", 4, $sort, $fs_imagepath)?>
				
			</div>
		</td>	
		<td width="50"><?=tt("Link")?></td>
		<td width="50"><?=tt("Home")?></td>
		<td width="50"><?=tt("Show")?></td>
		<td width="50"><?=tt("Active")?></td>
		<td width="50"><?=tt("Copy")?></td>
		<td width="50"><?=tt("Sửa")?></td>
		<td width="50"><?=tt("Xóa")?></td>
	</tr>
	<? 
	$category_type ='';
	$countno=0;
	for($i=0;$i<count($listAll);$i++){ 
		$countno++;

	if($category_type != $listAll[$i]["cat_type"]){
		$category_type = $listAll[$i]["cat_type"];
	?>
		<? if($countno!=1){?><tr><td colspan="11" height="30">&nbsp;</td></tr><? }?>
		<tr>
			<td colspan="11" bgcolor="<?=$bgcolor?>" align="center" class="textBold" height="30" style="color:#3C70B2">
			<?=$arrCatType[$listAll[$i]["cat_type"]]?>
			<? //isset($category_type) ? $category_type : $category_type?>
		</td>
		</tr>
	<?
	}
	?>
	<tr <?=$fs_change_bg?> class="row-<?=$listAll[$i]["cat_id"]?> item-row" align="center">
		<td>
			<input title="<?=tt("Tích vào giá trị muốn thay đổi")?>" type="checkbox" id="record_<?=$listAll[$i]["cat_id"]?>_<?=$countno?>" name="record_id[]" value="<?=$listAll[$i]["cat_id"]?>">
			<input type="hidden" name="iQuick" value="update">
		</td>
		<td>			
			<img title="<?=tt("Lưu bản ghi")?>" alt="<?=tt("Lưu")?>" src="<?=$fs_imagepath?>save.png" style="cursor:pointer" onClick="submitAll('form_listing',<?=$listAll[$i]["cat_id"]?>,<?=$countno?>)">
		</td>
		<td align="center">
			<?
			$path = $fs_filepath . $listAll[$i]["cat_picture"];
			if($listAll[$i]["cat_picture"] != "" && file_exists($path)){
			?>
			<img width="120" height="20" onMouseOver="showtip('<img src=\'<?=$fs_filepath . "m_" .$listAll[$i]["cat_picture"] ?>\'>','#DAF2D8')" ;="" onMouseOut="hidetip()"  src="<?=$fs_filepath . "s_" . $listAll[$i]["cat_picture"]?>" <? if(file_exists("../../resource/images/noimage.jpg")) echo ' onError="this.src=\'../../resource/images/noimage.jpg\'"';?> style="cursor:pointer" border="0">
			<img title="<?=tt("Xóa")?>" src="../../resource/images/deletepic.png"onClick="if (confirm('Bạn chắc chắn muốn xóa ảnh?')){ window.location.href='deletepic.php?record_id=<?=$listAll[$i]["cat_id"]?>&returnurl=<?=base64_encode(getURL())?>'}" style="cursor:pointer"><br>
			<?
			}else{
			?>
				<input type="file" name="picture<?=$listAll[$i]["cat_id"]?>" id="picture<?=$listAll[$i]["cat_id"]?>" class="form" size="10">			
			<?
			}
			?>
		</td>
		<td nowrap="nowrap" align="left" style="padding-left:10px;">				
			<? for($j=0;$j<$listAll[$i]["level"];$j++) echo '&nbsp;<font style="color:#CCCCCC">---</font>&nbsp;';?>	
			<input title="<?=tt("Thứ tự")?>" type="text" id="cat_order<?=$listAll[$i]['cat_id'];?>" name="cat_order<?=$listAll[$i]['cat_id'];?>" value="<?=$listAll[$i]['cat_order']?>" class="form" style="width:20px;<? if($listAll[$i]['level'] == 0) echo 'font-weight:bold';?>" onKeyUp="changeCheck(<?=$listAll[$i]["cat_id"]?>,<?=$countno?>)">
			<input title="<?=tt("Tên danh mục")?>" type="text" id="cat_name<?=$listAll[$i]['cat_id'];?>"  name="cat_name<?=$listAll[$i]['cat_id'];?>" value="<?=$listAll[$i]['cat_name'];?>" class="form" style="width:200px;<? if($listAll[$i]['level'] == 0) echo 'font-weight:bold';?>" onKeyUp="changeCheck(<?=$listAll[$i]["cat_id"]?>,<?=$countno?>)">
		</td>
		<td nowrap="nowrap" align="left" class="padlr10">				
			<input title="<?=tt("Link danh mục")?>" type="text" id="cat_name_index<?=$listAll[$i]['cat_id'];?>"  name="cat_name_index<?=$listAll[$i]['cat_id'];?>" value="<?=$listAll[$i]['cat_name_index'];?>" class="form" style="width:200px;<? if($listAll[$i]['level'] == 0) echo 'font-weight:bold';?>" onKeyUp="changeCheck(<?=$listAll[$i]["cat_id"]?>,<?=$countno?>)">
		</td>	
		<td align="center"><img class="home" name="<?=$listAll[$i]["cat_home"];?>" id="<?=$listAll[$i]["cat_id"];?>" path="<?=$fs_imagepath?>" src="<?=$fs_imagepath?>active_<?=$listAll[$i]["cat_home"];?>.png" title="<?=tt("Kích hoạt bản ghi")?>" alt="<?=tt("Kích hoạt")?>" /></td>
		<td align="center"><img class="show" name="<?=$listAll[$i]["cat_show"];?>" id="<?=$listAll[$i]["cat_id"];?>" path="<?=$fs_imagepath?>" src="<?=$fs_imagepath?>active_<?=$listAll[$i]["cat_show"];?>.png" title="<?=tt("Kích hoạt bản ghi")?>" alt="<?=tt("Kích hoạt")?>" /></td>
		<td align="center"><img class="active" name="<?=$listAll[$i]["cat_active"];?>" id="<?=$listAll[$i]["cat_id"];?>" path="<?=$fs_imagepath?>" src="<?=$fs_imagepath?>active_<?=$listAll[$i]["cat_active"];?>.png" title="<?=tt("Kích hoạt bản ghi")?>" alt="<?=tt("Kích hoạt")?>" /></td>
		<td align="center"><img title="<?=tt("Sao chép bản ghi")?>" alt="<?=tt("Sao chép")?>" src="<?=$fs_imagepath?>copy.png" onClick="if (confirm('<?=tt("Bạn muốn sao chép bản ghi này?")?>')){ window.location.href='copy.php?record_id=<?=$listAll[$i]["cat_id"]?>&returnurl=<?=base64_encode(getURL())?>'}" border="0" style="cursor:pointer"></td>
		<td align="center"><img title="<?=tt("Sửa bản ghi")?>" alt="<?=tt("Sửa")?>" src="<?=$fs_imagepath?>edit.png" onClick="window.location.href='edit.php?record_id=<?=$listAll[$i]["cat_id"]?>&returnurl=<?=base64_encode(getURL())?>'" border="0" style="cursor:pointer"></td>
		<td align="center"><img class="remove" id="<?=$listAll[$i]["cat_id"];?>" src="<?=$fs_imagepath?>delete.png" title="<?=tt("Xóa bản ghi")?>" alt="<?=tt("Xóa")?>" /></td>
	</tr>
	<? 
	} 
	?>
</table>
</form>
<? template_bottom() ?>
</body>
</html>