<?
require_once("../../resource/security/security.php");

$module_id = 1;		// module id trong field modules

checkloggedin();
if (checkaccess($module_id) != 1){
	redirect("../deny.htm");
	exit();
}

// Khai bao thong tin table, field id va name
$fs_table			= "tbl_category"; //Add tên table
$field_id			= "cat_id";		//Add tên 
$field_name			= "cat_name";

$fs_filepath		= $root_path_admin.$path_category;
$extension_list 	= "jpg,gif,png,swf";
$limit_size			= 300000;

$small_width		= 	125;
$small_heght		=	97;
$small_quantity		=	100;

$medium_width		= 	250;
$medium_heght		=	100;
$medium_quantity	=	90;
?>