<?
require_once("config_security.php");

$record_id			= getValue("record_id","int","GET");

// Kiem tra quyen voi ban ghi
checkAddEdit("edit");	
checkRowUser($fs_table, $field_id, $record_id, base64_encode('listing.php'));

// Khai bao bien
$errorMsg 			= "";		//Warning Error!
$action				= getValue("action", "str", "POST", "");	//Get Action.
$cat_type			= getValue("cattype","str","GET","");
$cat_parent_id 	= getValue("cat_parent_id","str","GET","");
$cat_thongso 		= getValue("cat_thongso","int","POST",10);

$add					= "add.php";
$listing				= "listing.php";
$edit					= "edit.php";
$after_save_data	= getValue("after_save_data", "str", "POST", $listing);

$cat_name_index 	= getValue("cat_name_index", "str", "POST", "");
if($cat_name_index == '') $cat_name_index 	= removeTitle(getValue("cat_name", "str", "POST", ""),'/');

for($i=1;$i<=$cat_thongso;$i++){
	$valueform 		= getValue("cat_form_" . $i,"str","POST","");
	$valueform		= str_replace("|","",$valueform);
	$cat_form		.= $i != 1 && $valueform != '' ? '|'.$valueform : $valueform;
} // End for($i=1;$i<=$cat_thongso;$i++){
 
// Goi class generate form
$myform = new generate_form();	//Call Class generate_form();
$myform->removeHTML(0);	//Loại bỏ chức năng không cho điền tag html trong form
$myform->addTable($fs_table);	// Add table
$myform->add("cat_name","cat_name",0,0,"",1,tt("Bạn chưa nhập tên danh mục"),0,tt("Đã có tên danh mục trong dữ liệu"));
$myform->add("cat_name_index","cat_name_index",0,1,"",0,"",0,"");
$myform->add("cat_link","cat_link",0,0,"",0,"",0,"");
$myform->add("cat_order","cat_order",1,0,0,0,tt("Bạn chưa thiết lập vị trí ưu tiên"),0,tt("Bạn chưa thiết lập vị trí ưu tiên"));
$myform->add("cat_form","cat_form",0,1,"",0,"",0,"");
$myform->add("cat_parent_id","cat_parent_id",1,0,0,0,"",0,"");	
$myform->add("cat_description","cat_description",0,0,"",0,"",0,"");
$myform->add("meta_title","meta_title",0,0,"",0,"",0,"");
$myform->add("meta_description","meta_description",0,0,"",0,"",0,"");
$myform->add("meta_keyword","meta_keyword",0,0,"",0,"",0,"");

// Nếu như có form được submit
if($action == "submitForm"){
	
	// Kiểm tra lỗi
	$errorMsg .= $myform->checkdata();
	$errorMsg .= $myform->strErrorFeld ;
	
	// Nếu như không có lỗi
	if($errorMsg == ""){
		
		// Thuc hien query
		$query = $myform->generate_update_SQL($field_id,$record_id);
		$db_ex = new db_execute($query);
		//echo $query;exit();
		
		// Chuyen ve trang khac khi xu ly du lieu oki
		redirect($after_save_data . "?record_id=" . $record_id);
		exit();
		
	} // End if($errorMsg == "")
	
} // End if($action == "submitForm")

// đổi tên trường thành biến và giá trị
$myform->evaluate();

// Khai bao ten form 
$myform->addFormname("submitForm"); //add  tên form để javacheck

// Xử lý javascript
$myform->addjavasrciptcode('');
$myform->checkjavascript();

// Chuyen ten truong thanh bien cho de dung
$db_data 	= new db_query("SELECT * FROM " . $fs_table . " WHERE " . $field_id . " = " . $record_id);
if($row 		= mysql_fetch_assoc($db_data->result)){
	foreach($row as $key=>$value){
		if($key!='lang_id' && $key!='admin_id') $$key = $value;
	}
}else{
	exit();
} // End if($row 		= mysql_fetch_assoc($db_data->result))
?>

<html>
<head>
<title>Administrator Control panel</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<?=$load_header?>
<script type="text/javascript">
$(document).ready(function(){
	$('#cat_type').change(function(){
		$cat_type = $('#cat_type').val();
		$('#content_loader').empty();
		$('#content_loader').load('load.php?cat_id=<?=$cat_id?>&cat_type='+$catType+'&cat_form=<?=$cat_form?>');	
	})
	$('#content_loader').empty();
	$('#content_loader').load('load.php?cat_id=<?=$cat_id?>&cat_parent_id=<?=$cat_parent_id?>&cat_type=<?=$cat_type?>&cat_form=<?=base64_encode($cat_form)?>');	
})
</script>
</head>

<body>
<? template_top(tt("Quản lý bản ghi"))?>

<table class="tab" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td nowrap><div class="n" id="tabt_1" onClick="tabdiv(1,5);document.cookie='tab_select=1'"><?=tt("Chung")?></div></td>
		<td nowrap><div class="n" id="tabt_2" onClick="tabdiv(2,5);document.cookie='tab_select=2'"><?=tt("SEO")?></div></td>
		<td nowrap><div class="n" id="tabt_3" onClick="tabdiv(3,5);document.cookie='tab_select=3'"><?=tt("Mô tả")?></div></td>
		<td style="border-bottom:solid 1px #DADADA;" width="99%">&nbsp;</td>
</tr>
</table>

<? $fs_action = $_SERVER['SCRIPT_NAME'] . "?" . @$_SERVER['QUERY_STRING'];?>
<?=$myform->create_form("submitForm", $fs_action, "post", "multipart/form-data","");?>

<div style="border:solid 1px #DADADA; border-top-width:0; padding:10px;">
	<div id="tabc_1" class="tabc">
		<?=$myform->create_table(8,3,"");?>
		<?=$myform->text_note()?>
		<?=$myform->errorMsg($errorMsg)?>
		<?=$myform->showform("text","cat_name","Tên danh mục",$cat_name,array("width"=>350))?>
		<?=$myform->text(0 , "Đường dẫn tĩnh", "cat_link", "cat_link", $cat_link, "Đường dẫn tĩnh", 400, "", 255, "", "", '&nbsp;<img title="Tạo link cho menu" align="absmiddle" src="'.$fs_imagepath.'edit.png" style="cursor:pointer" onClick="creat_link(\'cat_link\')">')?>
		<?=$myform->getFile(0,"Ảnh minh họa", "picture", "picture" , 32, "", '(<i>Dung lượng tối đa <font color="#993300">' . number_format($limit_size,0,'','.') . ' Kb</font>)</i>')?>
		<?=$myform->text(0 , "Thứ tự", "cat_order", "cat_order", $cat_order, "Thứ tự", 50, "", 5, "", "", "")?>
		<tbody id="content_loader"></tbody>
		<?=$myform->close_table();?>
	</div>

	<div id="tabc_2" class="tabc">
		<div class="mart10">Đường dẫn tự động :</div>
		<input type="text" name="cat_name_index" id="cat_name_index" value="<?=$cat_name_index?>" class="form_control" style="width:50%">
		
		<div class="mart10">Google Title :</div>
		<textarea name="meta_title" id="meta_title" class="form_control" style="width:90%; height:50px;"><?=$meta_title?></textarea>
		
		<div class="mart10">Google Description :</div>
		<textarea name="meta_description" id="meta_description" class="form_control" style="width:90%; height:100px;"><?=$meta_description?></textarea>
		
		<div class="mart10">Google Keyword : <span> - <i class="font11">(Các keyword cách nhau bằng dầu phẩy ',')</i></span></div>
    	<textarea name="meta_keyword" id="meta_keyword" class="form_control" style="width:90%; height:100px;"><?=$meta_keyword?></textarea>
    </div>

	<div id="tabc_3" class="tabc">
		<?=$myform->create_table(5,0,"");?>
		<?=$myform->wysiwyg("Chi tiết", "cat_description", $cat_description, "../../resource/ckeditor/", "99%", 300)?>
		<?=$myform->close_table();?>
	</div>

	<?=$myform->create_table(5,0,"");?>
	<?=$myform->radio(0 , "Sau khi lưu dữ liệu", "add_new" . $myform->ec . "return_listing" . $myform->ec . "return_edit", "after_save_data", $add . $myform->ec . $listing . $myform->ec . $edit, $after_save_data, "Thêm mới" . $myform->ec . "Quay về danh sách" . $myform->ec . "Sửa bản ghi", "");?>
	<?=$myform->button("button" . $myform->ec . "reset", "button" . $myform->ec . "reset", "button" . $myform->ec . "reset", "Cập nhật"  . $myform->ec .  "Làm lại", $after_save_data, "");?>
	<?=$myform->hidden("action", "action", "submitForm", "");?>
	<?=$myform->close_table();?>
</div>

<?=$myform->close_form();?>
<? unset($myform);?>

<? $tab_select = getValue("tab_select","int","COOKIE",1);?>
<script>tabdiv(<?=$tab_select?>,5)</script>

<? template_bottom() ?>

</body>
</html>