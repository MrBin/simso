<?
require_once("config_security.php");

// Khai bao bien
$cat_id			= getValue("cat_id");
$cat_type		= getValue("cat_type", "str", "GET","");
$cat_parent_id	= getValue("cat_parent_id");
$cat_form		= getValue("cat_form", "str", "GET","");
$cat_form		= base64_decode($cat_form);
$cat_thongso 	= getValue("cat_thongso","int","POST",1);

#+
$arrCatForm 		= explode("|",$cat_form);

// Lay tat ca category theo dieu kien
$menu		= new menu();
$listAll	= $menu->getAllChild("tbl_category", "cat_id", "cat_parent_id", 0, "cat_type = '" . $cat_type . "' AND cat_id <> ".$cat_id." AND lang_id = " . $_SESSION["lang_id"], "cat_id,cat_name,cat_type", "cat_order ASC,cat_name ASC", "cat_has_child", 0);
unset($menu);
?>

<tr>
	<td align="right"><?=tt("Danh mục cấp trên")?> : </td>
	<td>
		<select title="<?=tt("Danh mục cấp trên")?>" id="cat_parent_id" name="cat_parent_id" class="form_control">
			<option value=""><?=tt("Danh mục cấp trên")?></option>
			<?
			for($i=0; $i<count($listAll); $i++){
				$selected = ($cat_parent_id == $listAll[$i]["cat_id"]) ? ' selected="selected"' : '';
				echo '<option title="' . htmlspecialbo($listAll[$i]["cat_name"]) . '" value="' . $listAll[$i]["cat_id"] . '"' . $selected . '>';
				for($j=0; $j<$listAll[$i]["level"]; $j++) echo ' |--';
				echo ' ' . cut_string($listAll[$i]["cat_name"], 55) . '</option>';
			}
			?>
		</select>
	</td>
</tr>


<? 
if($cat_type == 'san-pham'){
?>
    <tr>
        <td>&nbsp;</td>
        <td>
            <div style="color:#F00; margin:0 0 10px;">
                Mẫu để nhập sản phẩm :
            </div>
            <div>
                Chọn thông số :
                <select id="cat_thongso" name="cat_thongso" class="form">
                    <?
                    for($i=count($arrCatForm);$i<=50;$i++){
                    ?>
                        <option value="<?=$i?>" <? if($cat_thongso==$i) echo 'selected';?>><?=$i?></option>
                    <?
                    }
                    ?>
                </select>
            </div>
        </td>	
    </tr>
    <tr>
    	<td colspan="2">
        	<?
			$i = 0;
			foreach($arrCatForm as $k => $v){
				$i ++;
				$j = $i < 10 ? '0'.$i : $i;
				if(trim($v) != ''){
				?>
                <table cellpadding="0" cellspacing="0">
                    <tr>
                        <td>
                            <input type="text" />
                        </td>
                        <td>
                            <select id="cat_thongso_sub" name="cat_thongso_sub" class="form">
                                <?
                                for($i=count($arrCatForm);$i<=50;$i++){
                                ?>
                                    <option value="<?=$i?>" <? if($cat_thongso==$i) echo 'selected';?>><?=$i?></option>
                                <?
                                }
                                ?>
                            </select>
                        </td>
                        <td>
                        	Thông số <?=$j?> : 
                        	<input type="text" name="cat_form_<?=$i?>" id="cat_form_<?=$i?>" value="<?=$v?>" class="countTs form" style="width:400px;"><br />    
                        </td>
                    </tr>
                </table>
				<?	
				} // End if(trim($v) != '')
			} // End foreach($arrCatForm as $k => $v)
			?>
        	<div id="catThongSo"></div>
        </td>
    </tr>
	
<?
} // End if($type == 'san-pham'){
?>

<script type="text/javascript">
$(document).ready(function(){
	function htmlThongSo(){
		value 	= $('#cat_thongso').val();
		str		= '';
		count = $('input.countTs').length;
		count = count > 0 ? count : 0;
		
		for(i=count+1;i<=value;i++){
			j = i <10 ? '0'+i : i;
			
			str += '<input type="text" class="form_control" value="Chung" />';
			str += '<select id="cat_thongso_sub" name="cat_thongso_sub" class="aaa form_control">';
			for(ii=1;ii<=10;ii++){
				str += '<option value="'+ii+'">'+ii+'</option>';	
			} // End for(i=1;i<=10;i++)
			str += '</select>';
			str += '<br />';
			/*
			str += 'Thông số '+j+' : <input type="text" name="cat_form_'+i+'" id="cat_form_'+i+'" value="" class="form_control" style="width:400px;"><br />';
			*/
		}
		$('#catThongSo').html(str);	
	}
	/*
	function htmlThongSo1(){
		value 	= $('#cat_thongso_sub').val();
		str		= '';
		count = $('input.countTs').length;
		count = count > 0 ? count : 0;
		
		for(i=count+1;i<=value;i++){
			j = i <10 ? '0'+i : i;
			str += 'Thông số '+j+' : <input type="text" name="cat_form_'+i+'" id="cat_form_'+i+'" value="" class="form_control" style="width:400px;"><br />';
			str += '<div id="catThongSoSub"></div>';
		}
		$('#catThongSoSub').html(str);	
	}
	*/
	$('#cat_thongso').change(function(){
		htmlThongSo();	
	})
	$('.aaa').change(function(){
		alert(123);
	})
	htmlThongSo();
})
</script>