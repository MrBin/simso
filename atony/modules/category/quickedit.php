<?
require_once("config_security.php");

// Check quyen sua ban ghi
checkAddEdit("edit");	

// Khai bao bien
$returnurl 	= base64_decode(getValue("returnurl","str","GET",base64_encode("listing.php")));
$errorMsg 	= "";
$iQuick 		= getValue("iQuick","str","POST","");
$record_id 	= getValue("record_id", "arr", "POST", "");

if($iQuick == 'update'){

	if($record_id != ""){
		
		for($i=0; $i<count($record_id); $i++){
			
			// Kiem tra quyen user voi ban ghi
			checkRowUser($fs_table,$field_id,$record_id[$i],$returnurl);
			
			// Khai bao bien
			$cat_name_index 	= getValue("cat_name_index".$record_id[$i], "str", "POST", "");
			if($cat_name_index == '') $cat_name_index 	= removeTitle(getValue("cat_name".$record_id[$i], "str", "POST", ""),'/');

			// Goi class generate form
			$myform = new generate_form();
			$myform->removeHTML(0); // Loại bỏ chuc nang thay the Tag Html
			$myform->addTable($fs_table);
			$myform->add("cat_name","cat_name" . $record_id[$i],0,0,"",1,"dfsdfsdfds",0,"");
			$myform->add("cat_name_index","cat_name_index",0,1,"",0,"",0,"");
			$myform->add("cat_order","cat_order" . $record_id[$i],1,0,0,0,"",0,"");
			
			// Tao duong dan cho folder anh
			$fs_filepath = $fs_filepath.'/';
			if(!is_dir($fs_filepath)){
				$oldumask = umask(0);
				mkdir($fs_filepath,0777,true);
				umask($oldumask);	
			} // End if(!is_dir($fs_filepath))
		
			// Xu ly anh được up lên
			$upload_pic = new upload("picture" . $record_id[$i], $fs_filepath, $extension_list, $limit_size);
			if ($upload_pic->file_name != ""){
				$picture = $upload_pic->file_name;
				//resize anh
				resize_image($fs_filepath,$upload_pic->file_name,$small_width,$small_heght,$small_quantity,"s_");
				resize_image($fs_filepath,$upload_pic->file_name,$medium_width,$medium_heght,$medium_quantity,"m_");
				$myform->add("cat_picture","picture",0,1,"",0,"",0,"");
			} // End if ($upload_pic->file_name != ""){
				
			// Xoa anh khi ma da co file
			if ($upload_pic->file_name != "") delete_file($fs_table,"cat_id",$record_id[$i],"cat_picture",$fs_filepath);
			
			// Kiem tra loi
			$errorMsg .= $upload_pic->show_warning_error();
			$errorMsg .= $myform->checkdata();
			$errorMsg .= $myform->strErrorFeld ;
			
			// Khong co loi thi thuc  thien
			if($errorMsg == ""){
				$query = $myform->generate_update_SQL("cat_id",$record_id[$i]);
				$db_ex = new db_execute($query);
				//echo $query; exit();
			} // End if($errorMsg == ""){
				
		} // End for($i=0; $i<count($record_id); $i++){
			
	} // End if($record_id != ""){
	
	// Tra ve listing
	echo '<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">';
	echo tt("Đang cập nhật dữ liệu");
	redirect($returnurl);
	
} // End if ($iQuick == 'update')
?>