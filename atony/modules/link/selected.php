<?
include("config_security.php");

#+
#+ Khai bao bien
$fs_preview	= '';
$fs_title	= tt("Tạo link");
$fs_action	= getURL();
#+
#+ Object sẽ nhận link trả về khi create
$object		= getValue("object", "str", "GET", "");
$iCat		= getValue("iCat");

#+
#+ Lay toan bo categories
$class_menu	= new menu();
$menu->show_count = 1; //tính count sản phẩm
$listAll		= $class_menu->getAllChild("tbl_category INNER JOIN tbl_category_type ON (typ_name_index = cat_type)", "cat_id", "cat_parent_id", 0, "tbl_category.lang_id = ".$_SESSION["lang_id"], "cat_id,cat_name,cat_type,typ_order", "typ_order,cat_type ASC,cat_order ASC,cat_name ASC", "cat_has_child", 0);
unset($class_menu);

#+
#+ Tao link theo category
$db_category = new db_query("SELECT cat_id,cat_name,cat_name_index FROM tbl_category WHERE cat_id = " . $iCat);
$row=mysql_fetch_array($db_category->result);
$link_category	= ($iCat == 0) ? "" : createLink("cat",$row);

#+
#+ Xác định module khi user đã chọn category
$query = ' SELECT cat_type,typ_type'
		.' FROM tbl_category'
		.' INNER JOIN tbl_category_type ON (typ_name_index = cat_type)'
		.' WHERE tbl_category.lang_id = '.$_SESSION["lang_id"].' AND cat_id = '.$iCat
		;
$db_query	= new db_query($query);
if($row = mysql_fetch_array($db_query->result)){
	$module 	= $row["cat_type"];
	$typ_type	= $row["typ_type"];
} // End if($row = mysql_fetch_array($db_query->result))
$db_query->close();
unset($db_query);

#+
#+ Lay ra cac bai viet cua cac cat
$array_data = array();
#+
switch($typ_type){
	case "static":
		$array_data[$module] = array("tbl_statics", "sta_id", "sta_category", "sta_title", "sta_date", $module, "sta_id, sta_id AS dat_id,sta_category,sta_title AS dat_title,sta_title_index,sta_date AS dat_date");
		break;
	
	case "news":
		$array_data[$module] = array("tbl_news", "new_id", "new_category", "new_title", "new_date", $module, "new_id, new_id AS dat_id,new_category,new_title AS dat_title,new_title_index,new_date AS dat_date");
		break;
	
	case "danhba":
		$array_data[$module] = array("tbl_danhba", "dba_id", "dba_category", "dba_name", "dba_date", $module, "dba_id, dba_id AS dat_id,dba_category,dba_name AS dat_title,dba_name_index,dba_date AS dat_date");
		break;
} // End switch($typ_type)

#+
foreach($array_data as $key => $value){
	
	#+
	#+ Khi xac dinh duoc module thi lay bai viet theo module do
	if($module == $key){
		$sql_count	= " SELECT COUNT(*) AS count
						FROM tbl_category, ".$value[0]."
						WHERE cat_id = ".$value[2]." AND cat_type = '".$value[5]."' AND cat_id = ".$iCat;
		$sql_data	= " SELECT ".$value[6]."
						FROM tbl_category, ".$value[0]."
						WHERE cat_id = ".$value[2]." AND cat_type = '".$value[5]."' AND cat_id = ".$iCat;
		$arrayData	= array(0 => $value[1],1 => $value[3],2 => $value[4]);
		break;
	} // End if($module == $key)
	
} // End foreach($array_data as $key => $value)
?>


<html>
<head>
<title><?=$fs_title?></title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<?=$load_header?>
</head>

<body>
<?=template_top($fs_title . ' <a title="Đóng cửa sổ" href="javascript:window.close()"><img align="absmiddle" border="0" hspace="5" src="images/close.gif" />Đóng</a>',0)?>
<?
$form = new form();
?>

<?=$form->create_form("create_link", $fs_action, "get", "multipart/form-data");?>
<?=$form->create_table();?>
<?=$form->text_note('Những ô có dấu sao (<font class="form_asterisk">*</font>) là bắt buộc phải nhập.')?>
<? //*/?>
<tr>
	<td class="form_name">Trang chính</td>
	<td>
		<select class="form" onChange="change_file(this.value)">
			<option value="">Chọn trang</option>
			<option value="/">Trang chủ</option>
            <?
            $query = ' SELECT *' 
					.' FROM tbl_category_type'
					.' WHERE typ_active = 1 AND typ_type != "static"'
					.' ORDER BY typ_order'
					;
			$db_query = new db_query($query);
			while($typ = mysql_fetch_assoc($db_query->result)){
			?>
            	<option value="/<?=$typ['typ_name_index']?>.html"><?=$typ['typ_name']?></option>
            <?
			} // End while($tpy = mysql_fetch_assoc($db_query->result)
			?>
			<option value="/lien-he.html">Liên hệ</option>
		</select>
	</td>
</tr>
<tr>
	<td class="form_name">
		<font class="form_asterisk">* </font><?=tt("Thư mục cha")?> :
	</td>
	<td class="form_text">
		<select class="form" id="iCat" name="iCat" onChange="change_category()">
			<option value="0">--[Chọn category]--</option>
		<?
		$cat_type = "";
		for($i=0; $i<count($listAll); $i++){
		?>
			<?
			if($cat_type != $listAll[$i]["cat_type"]){
				$cat_type = $listAll[$i]["cat_type"];
			?>
				<optgroup label="<?=ucwords($cat_type)?>"></optgroup>
			<?
			}
			
			//
			$selected = ($iCat == $listAll[$i]["cat_id"]) ? ' selected="selected"' : '';
			echo '<option value="' . $listAll[$i]["cat_id"] . '"' . $selected . '>';
			for($j=0; $j<$listAll[$i]["level"]; $j++) echo ' |--';
			echo ' |-- ' . cut_string($listAll[$i]["cat_name"], 55) . '</option>';
			?>
		<?
		}
		?>
		</select>
	</td>
</tr>
<?=$form->text(1 ,"Link tới category", "link_category", "link_category", $link_category, "Link tới category", 250, "", "1000", "", 'disabled="disabled"', "")?>
<?=$form->button("button", "create_link_category", "create_link_category", "Tạo link", '" onClick="link_to_category()"', "");?>
<?=$form->hidden("object", "object", $object, "");?>
<?=$form->close_table();?>
<?=$form->close_form();?>
<? unset($form);?>

<?
//*/
//---------------------------- Create link to data -----------------------------------
if(isset($arrayData)){
	#+
	#+ Khai bao bien
	#+
	$id			= getValue("id");
	$keyword	= getValue("keyword", "str", "GET", "", 1);
	$sqlWhere	= "";
	$limit = getValue("limit");
	#+
	#+ Phan trang
	$normal_class    = "page";
	$selected_class  = "pageselect";
	$page_prefix     = "Trang : ";
	$current_page    = ( getValue("page") < 1 ) ? 1 : getValue("page");
	$page_size	     = ( $limit > 0 ) ? $limit : 30;
	$url				= getURL(0,0,1,1,"page");

	#+
	#+ Tìm theo ID
	if($id > 0)			
		$sqlWhere .= " AND " . $arrayData[0] . " = " . $id . " ";
	
	#+
	#+ Tìm theo keyword
	if($keyword != "")
		$sqlWhere .= " AND (" . $arrayData[1] . " LIKE '%" . $keyword . "%') ";
	
	#+
	#+ Sap xep du lieu
	$sort	= getValue("sort");
	switch($sort){
		case 1: $sqlOrderBy = $arrayData[0] . " ASC"; break;
		case 2: $sqlOrderBy = $arrayData[0] . " DESC"; break;
		case 3: $sqlOrderBy = $arrayData[1] . " ASC"; break;
		case 4: $sqlOrderBy = $arrayData[1] . " DESC"; break;
		case 5: $sqlOrderBy = $arrayData[2] . " ASC"; break;
		case 6: $sqlOrderBy = $arrayData[2] . " DESC"; break;
		default:$sqlOrderBy = $arrayData[0] . " DESC"; break;
	}

	#+
	#+ Dem so luong ban ghi
	$db_count		= new db_query($sql_count . $sqlWhere);
	$listing_count	= mysql_fetch_array($db_count->result);
	$total_record	= $listing_count["count"];
	$db_count->close();
	unset($db_count);
	
	#+
	#+ Lay ra du lieu
	$query = $sql_data . $sqlWhere . " ORDER BY " . $sqlOrderBy . " LIMIT " . ($current_page - 1) * $page_size . ", " . $page_size;
	# echo $query.'<br>';
	$db_listing	= new db_query($query);
	?>
	<? //Page break and search data?>
	<table width="98%" cellpadding="2" cellspacing="2">
		<tr>
			<? if($total_record > $page_size){ ?>
			<td align="left" class="pagi">
				<?=generatePageBar($page_prefix,$current_page,$page_size,$total_record,$url,$normal_class,$selected_class);?>
			</td>
			<? } ?>
			<td align="right">
				<table cellpadding="0" cellspacing="0">
				<form name="search" action="<?=getURL(0,0,1,0)?>" method="get">
					<tr>
                    	<td>
                        	<?
							$arrLimit = array(
								'5' => '5 items',
								'15' => '15 items',
								'30' => '30 items',
								'50' => '50 items',
								'80' => '80 items',
								'120' => '120 items'
							);
							?>
							<select name="limit" id="limit" class="form" onChange="this.form.submit();">
								<? foreach($arrLimit as $key => $row){?>
								<option value="<?=$key?>" <? if($key == $page_size){ echo 'selected';}?> ><?=$row?></option>
								<? }?>
							</select>
                        </td>
						<td class="textBold" nowrap="nowrap">
							ID:
							<input title="ID" type="text" class="form" id="id" name="id" value="<?=$id?>" maxlength="11" style="width:50px">&nbsp;
							Từ khóa:
							<input title="Từ khóa" type="text" class="form" id="keyword" name="keyword" value="<?=$keyword?>" maxlength="255" style="width:100px">&nbsp;
							<input type="hidden" name="sort" value="<?=$sort?>" />
							<input type="hidden" name="object" value="<?=$object?>" />
							<input type="hidden" name="iCat" value="<?=$iCat?>" />
						</td>
						<td class="textBold" style="padding-left:5px"><input title="Tìm kiếm" type="image" src="<?='images/'?>search.gif" border="0"></td>
					</tr>
				</form>
				</table>
			</td>
		</tr>
	</table>
	<? //End page break and search data?>
	<table border="1" style="border-collapse:collapse" bordercolor="<?=$bordercolor?>" cellpadding="3" cellspacing="0" width="98%">
		<tr class="textBold">
			<td>STT</td>
			<td nowrap="nowrap" align="center">
				<div>ID</div>
				<div>
					<?=generate_sort("asc", 1, $sort, 'images/')?>
					<?=generate_sort("desc", 2, $sort, 'images/')?>
				</div>
			</td>
			<td align="center">
				<div>Tên/ Tiêu đề</div>
				<div>
					<?=generate_sort("asc", 3, $sort, 'images/')?>
					<?=generate_sort("desc", 4, $sort, 'images/')?>
				</div>
			</td>
			<td nowrap="nowrap" align="center">
				<div>Ngày cập nhật</div>
				<div>
					<?=generate_sort("asc", 5, $sort, 'images/')?>
					<?=generate_sort("desc", 6, $sort, 'images/')?>
				</div>
			</td>
			<td align="center">Link</td>
			<td align="center">Tạo link</td>
		</tr>
	<?
	// Đếm số thứ tự
	$No = ($current_page - 1) * $page_size;
	while($listing = mysql_fetch_array($db_listing->result)){
		$No++;
		$link	=  createLink("detail_".$typ_type,$listing);
	?>
		<tr id="tr_<?=$No?>">
			<td class="No"><?=$No?></td>
			<td align="right" class="text_normal_bold">
				<?=$listing["dat_id"]?>
			</td>
			<td nowrap>
            	<a href="<?=$fs_preview . $link?>" target="_blank"><?=$listing["dat_title"]?></a>
            </td>
			<td align="center">
				<div><?=(isset($listing["dat_date"]) ? date("d/m/Y", $listing["dat_date"]) : "No update !")?></div>
				<div style="color:#666666; font-size:10px"><?=(isset($listing["dat_date"])) ? date("H:i A", $listing["dat_date"]) : '';?></div>
			</td>
			<td nowrap="nowrap" style="color:#0000FF">
				<a href="<?=$fs_preview . $link?>" target="_blank"><?=$link?></a>
            </td>
			<td align="center">
            	<img title="Tạo link" hspace="5" src="<?='images/'?>create_link.gif" style="cursor:pointer" onClick="link_to_data('<?=$link?>')" />
            </td>
		</tr>
	<? }?>
	</table>
	<? if($total_record > $page_size){ ?>
        <div class="pagi">
            <?=generatePageBar($page_prefix,$current_page,$page_size,$total_record,$url,$normal_class,$selected_class);?>
        </div>
    <? } ?>
	<?
	$db_listing->close();
	unset($db_listing);
	?>
<?
}// End if(isset($arrayData))
//*/
?>
<?=template_bottom()?>

</body>
</html>
<script language="javascript">
function change_category(){
	frm = document.create_link;
	frm.submit();
}
function change_file(filename){
	document.getElementById("link_category").value = '<?=$fs_preview?>'+filename;
}
function link_to_category(){
	ob = document.getElementById( "iCat" );
	/*if( ob.value == 0 ){
		alert( "Bạn phải chọn một category !" );
		ob.focus();
		return false;
	}*/
	window.opener.document.getElementById("<?=$object?>").value = document.getElementById("link_category").value;
	window.close();
}

function link_to_data(str_link){
	window.opener.document.getElementById("<?=$object?>").value = str_link;
	window.close();
}
</script>
<script language="javascript">self.moveTo((screen.width-document.body.clientWidth)/2, (screen.height-document.body.clientHeight)/2);</script>