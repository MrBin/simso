<?
require_once("config_security.php");	//check security	

include('Net/SSH2.php');

//
// Login to sever
$ssh = new Net_SSH2('sieuthisimthe.com');
$ssh->login('root', '') or die("Login failed");

//
// Get PID Memcached
$pid_memcached = $ssh->exec('ps -A|grep memcached');
if(isset($pid_memcached) && $pid_memcached != ''){
	$pid_memcached = explode('?', $pid_memcached);
	$pid_memcached = $pid_memcached[0];	
 	$pid_memcached = trim($pid_memcached);
 	echo 'Old PID MEM: ' . $pid_memcached . '<br>';
	
	if(is_numeric($pid_memcached)){
		// Kill and run Memcached
		$kill_memcached 	= $ssh->exec('kill ' . $pid_memcached);	
	} // End if(is_numeric($pid_memcached))
	
	$run_memcached 	= $ssh->exec('memcached -d -u nobody -m 2048 -p 11211 127.0.0.1');
		
	// Get new PID memcached
	$pid_memcached = $ssh->exec('ps -A|grep memcached');
	$pid_memcached = explode('?', $pid_memcached);
	$pid_memcached = $pid_memcached[0];	
 	$pid_memcached = trim($pid_memcached);
 	echo 'New PID MEM: ' . $pid_memcached . '<br>';
	
} // End if(isset($pid_memcached) && $pid_memcached != '')

//
// Get PID Searchd
$pid_searchd = $ssh->exec('ps -A|grep searchd');
$pid_searchd = trim($pid_searchd);
if(isset($pid_searchd) && $pid_searchd != ''){
	$pid_searchd = explode(' ', $pid_searchd);
	$pid_searchd = $pid_searchd[0];	
 	$pid_searchd = trim($pid_searchd);
 	echo 'Old PID SPX: ' . $pid_searchd . '<br>';
	
	if(is_numeric($pid_searchd)){
		// Kill and run Memcached
		$kill_searchd 	= $ssh->exec('kill ' . $pid_searchd);
	} // End if(is_numeric($pid_searchd))
	
	sleep(2);
	$run_searchd 	= $ssh->exec('searchd');
	$run_indexer 	= $ssh->exec('indexer --all --rotate');
	
	// Get new PID memcached
	$pid_searchd = $ssh->exec('ps -A|grep searchd');
	$pid_searchd = trim($pid_searchd);
	$pid_searchd = explode(' ', $pid_searchd);
	$pid_searchd = $pid_searchd[0];	
 	$pid_searchd = trim($pid_searchd);
 	echo 'New PID SPX: ' . $pid_searchd . '<br>';
 	
} // End if(isset($pid_searchd) && $pid_searchd != '')

//
// Redirect
echo '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<script type="text/javascript">alert("Thành công")</script>';
redirect('add.php');
exit();
?>