<?php
require_once("config_security.php");

// Khai bao bien
$sql 				= "";
$limit     	 	= getValue("limit", "int", "GET");
$show				= getValue("show");
$keyword 		= getValue("keyword", "str", "GET", "");
$iCat				= getValue("iCat");
$iType			= getValue("iType");
$iDaiLy			= getValue("iDaiLy");

$normal_class    	= "page";
$selected_class  	= "pageselect";
$page_prefix     	= "Trang : ";
$current_page    	= ( getValue("page") < 1 ) ? 1 : getValue("page");
$page_size	     	= ( $limit > 0 ) ? $limit : 15;
$url 					= "listing.php?keyword=" . $keyword . "&iCat=" . $iCat . "&iType=" . $iType . "&iDaiLy=" . $iDaiLy . "&limit=" . $limit . "&show=" . $show . "&page=";

if($keyword != ""){
	$keywords		= preg_replace("/[^0-9*xX]/si","",$keyword);
	if ($keywords != ""){
		$spot = strpos($keywords,"*");
		$slen = strlen($keywords);
	} // End if ($keywords!="")
	$keywords	 	= str_replace(array('x','X','*'),array('[0-9]','[0-9]','.*'),$keywords);

	if (stristr($keywords,"*") === false){
		if(strlen($keywords) >= 10){
			$keywords = ltrim($keywords,0);
			$sql 	.= " AND sim_sim2 = " . $keywords;
		}else $sql 	.= " AND sim_sim2 RLIKE '.*" . $keywords . ".*'";
	}else{
		$keywords = ltrim($keywords,0);
		if($spot == 0) $sql	.= " AND sim_sim2 RLIKE '" . $keywords . "$'";
		elseif($spot == ($slen -1)) $sql	.= " AND sim_sim2 RLIKE '^" . $keywords . "'";
		else $sql	.= " AND sim_sim2 RLIKE '^" . $keywords . "$'";
	} // End if (stristr($keywords,"*")=== false)
	
} // End if($keyword != "")

if($iCat != "") $sql .= " AND sim_category =" . $iCat;
if($iType != "") $sql .= " AND sim_typeid =" . $iType;
if($iDaiLy != "") $sql .= " AND sim_dailyid =" . $iDaiLy;

if($show > 0){
	switch($show){
		case 1:	
			$sql .= " AND sim_active = 1 "; 
			break;
		case 2:	
			$sql .= " AND sim_active = 0 "; 
			break;
		case 3:	
			$sql .= " AND sim_km = 1 "; 
			break;
		case 4:	
			$sql .= " AND sim_hot = 1 "; 
			break;
		case 5:	
			$sql .= " AND sim_new = 1 "; 
			break;
	}
}

$db_count  = new db_query("SELECT Count(*) AS count
									FROM " . $fs_table ." 
									WHERE lang_id=" . $_SESSION["lang_id"] . $sql
									);
$row_count 		= mysql_fetch_assoc($db_count->result);
$total_record 	= $row_count['count'];
unset($db_count);

$db_sim = new db_query("SELECT * FROM " . $fs_table ." 
								WHERE lang_id=" . $_SESSION["lang_id"] . $sql . " 
								ORDER BY sim_order DESC
								LIMIT " . ($current_page-1) * $page_size . "," . $page_size
								);
?>

<html>
<head>
<title>Channel Listing</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<?=$load_header?>
</head>

<body>
<!-- Load css và xử lý dữ liệu của jquery(kích hoạt, xóa bản ghi) -->
<div id="loadingBar">
    Đang xử lý ... Vui lòng đợi trong giây lát ... <br />
    <img align="absmiddle" src="../../resource/images/loading.gif" />
</div>
<input class="controller" type="hidden" value="controllers" />

<?=template_top(tt("Danh sách bản ghi") . "&nbsp;[" . number_format($total_record,0,".",",") . "]")?>

<div style="position: absolute; top: 3px; right: 5px;">
	<form action="listing.php" method="GET" name="formSearch">
	<table border="0" cellpadding="2" align="center">
		<tr>
			<td>
				<select title="Kiểu hiển thị tin" name="show" class="form">
					<option value="0"><?=tt("Chọn kiểu hiển thị")?></option>
					<? foreach($arrShow as $key => $value) echo '<option' . ($key == $show ? ' selected' : '') . ' value="' . $key . '">' . $value . '</option>';?>
				</select>
			</td>
	    	<td>
				<select name="limit" id="limit" class="form" onChange="this.form.submit();">
					<? foreach($arrLimit as $key => $value) echo '<option' . ($key == $page_size ? ' selected' : '') . ' value="' . $key . '">' . $value . '</option>';?>
				</select>
			</td>
			<td>
				<select name="iCat" id="iCat" class="form">
					<option value=""><?=tt("Chọn mạng")?></option>
					<? foreach($arrSimCat as $key => $value) echo '<option' . ($key == $iCat ? ' selected' : '') . ' value="' . $key . '">' . $value . '</option>';?>
				</select>
			</td>
			<td>
				<select name="iType" id="iType" class="form">
					<option value=""><?=tt("Chọn phân loại")?></option>
					<? foreach($arrSimType as $key => $value) echo '<option' . ($key == $iType ? ' selected' : '') . ' value="' . $key . '">' . $value . '</option>';?>
				</select>
			</td>  
			<td>
				<select name="iDaiLy" id="iDaiLy" class="form">
					<option value=""><?=tt("Chọn đại lý")?></option>
					<? foreach($arrSimDaily as $key => $value) echo '<option' . ($key == $iDaiLy ? ' selected' : '') . ' value="' . $key . '">' . $value . '</option>';?>
				</select>
			</td>
			<td><input type="text" class="form" name="keyword" value="<?=$keyword?>" style="width:120px" /></td>       
			<td><input type="submit" value="<?=tt("Tìm kiếm")?>" class="form_button" /></td>
		</tr>
	</table>
	</form>
</div>

<form action="quickedit.php?returnurl=<?=base64_encode(getURL())?>" method="post" name="form_listing" enctype="multipart/form-data">
	<input type="hidden" name="iQuick" value="update">
	<table border="1" cellpadding="2" cellspacing="0" width="100%" style="border-collapse:collapse" bordercolor="<?=$bordercolor?>">
		<tr align="center" height="40" class="textBold">
			<td width="5%">&nbsp;</td>
			<td class="textBold" width="50"><?=tt("Lưu")?></td>
			<td><?=tt("Số sim")?></td>
			<td><?=tt("Mạng")?></td>
			<td><?=tt("Phân loại")?></td>
			<td><?=tt("Cửa hàng")?></td>
			<td><?=tt("Giá bán")?></td>
			<td width="35"><?=tt("Hot")?></td>
			<td width="35"><?=tt("New")?></td>
			<td width="35"><?=tt("Active")?></td>
			<td><?=tt("Copy")?></td>
			<td><?=tt("Sửa")?></td>
			<td><?=tt("Xóa")?></td>
		</tr>
        
		<?
		$countno = 0;
		while( $row = mysql_fetch_assoc($db_sim->result) ){
			$countno++;
			$sim_km 		= ($row['sim_km'] == 1) ? 'active_1.png' : 'active_0.png';
			$sim_hot 	= ($row['sim_hot'] == 1) ? 'active_1.png' : 'active_0.png';
			$sim_new 	= ($row['sim_new'] == 1) ? 'active_1.png' : 'active_0.png';
			$sim_active = ($row['sim_active'] == 1) ? 'active_1.png' : 'active_0.png';
		?>
		<tr <?=$fs_change_bg?> class="row-<?=$row['sim_sim2']?> item-row" align="center">
			<td><input type="checkbox" name="record_id[]" id="record_id_<?=$countno?>" value="<?=$row['sim_sim2']?>"></td>
			<td><img src="<?=$fs_imagepath?>save.png" border="0" style="cursor:pointer" onClick=" document.getElementById('record_id_<?=$countno;?>').checked=true; document.form_listing.submit();" alt="Save"></td>
			<td><input name="sim_sim1<?=$row['sim_sim2']?>" value="<?=$row['sim_sim1']?>" onKeyUp="document.getElementById('record_id_<?=$countno?>').checked=true" type="text" class="form" /></td>
			<td>
				<select name="sim_category<?=$row['sim_sim2']?>" id="<?=$row['sim_sim2']?>" onChange="document.getElementById('record_id_<?=$countno?>').checked=true" class="form">
					<? foreach($arrSimCat as $key => $value) echo '<option' . ($key == $row['sim_category'] ? ' selected' : '') . ' value="' . $key . '">' . $value . '</option>';?>
				</select>
			</td>
			<td>
				<select name="sim_typeid<?=$row['sim_sim2']?>" id="<?=$row['sim_sim2']?>" onChange="document.getElementById('record_id_<?=$countno?>').checked=true" class="form">
					<? foreach($arrSimType as $key => $value) echo '<option' . ($key == $row['sim_typeid'] ? ' selected' : '') . ' value="' . $key . '">' . $value . '</option>';?>
				</select>
			</td>
			<td>
				<select name="sim_dailyid<?=$row['sim_sim2']?>" id="<?=$row['sim_sim2']?>" onChange="document.getElementById('record_id_<?=$countno?>').checked=true" class="form">
					<? foreach($arrSimDaily as $key => $value) echo '<option' . ($key == $row['sim_dailyid'] ? ' selected' : '') . ' value="' . $key . '">' . $value . '</option>';?>
				</select>
			</td>
			<td><input name="sim_price<?=$row['sim_sim2']?>" id="<?=$row['sim_sim2']?>" value="<?=$row['sim_price']?>" style="width:80px;" onKeyUp="document.getElementById('record_id_<?=$countno?>').checked=true" type="text" class="form" /></td>
			<td><img class="hot" name="<?=$row['sim_hot']?>" id="<?=$row['sim_sim2']?>" path="<?=$fs_imagepath?>" src="<?=$fs_imagepath.$sim_hot?>" alt="New" title="New" /></td>
			<td><img class="new" name="<?=$row['sim_new']?>" id="<?=$row['sim_sim2']?>" path="<?=$fs_imagepath?>" src="<?=$fs_imagepath.$sim_new?>" alt="Hot" title="Hot" /></td>
			<td><img class="active" name="<?=$row['sim_active']?>" id="<?=$row['sim_sim2']?>" path="<?=$fs_imagepath?>" src="<?=$fs_imagepath.$sim_active?>" alt="Kích hoạt" title="Kích hoạt" /></td>
			<td align="center"><img title="<?=tt("Sao chép bản ghi")?>" alt="<?=tt("Sao chép")?>" src="<?=$fs_imagepath?>copy.png" onClick="if (confirm('<?=tt("Bạn muốn sao chép bản ghi này?")?>')){ window.location.href='copy.php?record_id=<?=$row['sim_sim2']?>&returnurl=<?=base64_encode(getURL())?>'}" border="0" style="cursor:pointer"></td>
			<td><a href="edit.php?record_id=<?=$row['sim_sim2'];?>&url=<?=base64_encode(getURL())?>"><img src="<?=$fs_imagepath?>edit.png" alt="<?=tt("Edit")?>" border="0"></a></td>
			<td><img class="remove" id="<?=$row['sim_sim2']?>" src="<?=$fs_imagepath?>delete.png" /></td>
		</tr>       
		<?
		} // End while( $row = mysql_fetch_assoc($db_sim->result) )
		unset($db_sim);
		?>	
	</table>
</form>	

<? if($total_record > $page_size){ ?>     
<div class="pagi" align="right"><?=generatePageBar($page_prefix,$current_page,$page_size,$total_record,$url,$normal_class,$selected_class);?></div>
<? } ?>

<? template_bottom();?>
</body>
</html>