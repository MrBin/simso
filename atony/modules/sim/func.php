<?
// Loai sim
$db_sim_type = new db_query("SELECT simtp_id,simtp_name,simtp_index
									FROM tbl_simtype
									WHERE simtp_active = 1
									ORDER BY simtp_order
									");
while($row = mysql_fetch_assoc($db_sim_type->result)) $loai_sim_idx[$row["simtp_index"]] = $row["simtp_id"];
unset($db_sim_type);

// Gia sim
$db_sim_price = new db_query("SELECT pri_id,pri_min,pri_max
										FROM tbl_simprice
										");
while($row = mysql_fetch_assoc($db_sim_price->result)){
	$gia_sim_idx[$row['pri_id']] = array(
										'pri_min'=>$row['pri_min'],
										'pri_max'=>$row['pri_max']
										);
} // End while($row = mysql_fetch_assoc($db_sim_price->result))
unset($db_sim_price);

// Dau so
$db_sim_dauso = new db_query("SELECT sds_id,sds_category,sds_name,sds_name
										FROM tbl_simdauso
										WHERE sds_active = 1
										");
while($row = mysql_fetch_assoc($db_sim_dauso->result)){
	$index = intval($row['sds_name']);
	$dauso_sim_idx[$index] = array('sds_id'=>$row["sds_id"],
											'sds_category'=>$row["sds_category"]
											);
} // End while($ds = mysql_fetch_assoc($db_sim_dauso->result))
unset($db_sim_dauso);

// Dai ly
$db_sim_daily = new db_query("SELECT *
										FROM tbl_simdaily
										WHERE simch_active = 1
										ORDER BY simch_order
										");
$i = 0;
while($ch = mysql_fetch_assoc($db_sim_daily->result)){
	$i++;
	
	$dai_ly[$i] = $ch;
	$daily_sim_idx[$ch["simch_viettat"]] = $ch['simch_id'];
} // End while($ch = mysql_fetch_assoc($db_sim_daily->result))
unset($db_sim_daily);

function quanlysim( $data, $control ){
	global $daily_sim_idx;
	global $loai_sim_idx;
	global $gia_sim_idx;
	global $dauso_sim_idx;
	
	$db_sim_daily = new db_query("SELECT simch_viettat 
									FROM tbl_simdaily 
									WHERE simch_show = 1");
	$arrSimWeb = array();
	while($row = mysql_fetch_assoc($db_sim_daily->result)) $arrSimWeb[] = $row['simch_viettat'];
	unset($db_sim_daily);

	// Tạo ra array sim từ các dòng được copy từ excel
	$e = explode( "\r\n", $data );

	// Xử lý dữ liệu sim đưa vào
	$i = 0;
	for ( ; $i < count( $e ) - 1; $i++ ){
		
		list( $sosim, $giaban, $daily ) = split( "\t", $e[$i] );
		
		$sim1x = soreplace( $sosim );
		$sim2x = replace( $sosim );

		// Kiểm tra hợp lệ của số
		if ( checkso( $sim1x, $sim2x, $giaban, $i+1) ) $datapost['s'][$sim2x] = $sim1x . "-" . $giaban . "-" . $daily;
		
	} // End for ( ; $i < count( $e ) - 1; $i++ )

	$m = 0;
	$j = 0;
	foreach ( $datapost['s'] as $kg => $sg ){
		$m++;
		$j++;
		
		list( $sim1x, $giaban, $daily ) = split( "-", $sg );
		
		// Sim
		$sim_sim1 = $sim1x;
		$sim_sim2 = intval($kg);

		$sim_keyword = genTrigramsKeyword("0" . $sim_sim2);
		
		// Gia ban
		$sim_price = $giaban;
		
		// Ngay dang
		$sim_date = date("Y-m-d H:i:s");
		
		// Xac dinh sim cua cua hang
		$sim_web = 0;
		if(in_array($daily, $arrSimWeb)) $sim_web = 1;
		
		// Xac dinh loai sim
		$sim_typeid		= checkSimType($sim_sim2, $loai_sim_idx);
		
		// Xac dinh dai ly sim
		$sim_dailyid  	= isset($daily_sim_idx[$daily]) ? $daily_sim_idx[$daily] : '';
		
		// Xac dinh gia sim
		$sim_priceid	= 1;
		foreach($gia_sim_idx as $key => $row){
			if($giaban > $row['pri_min'] && $giaban <= $row['pri_max']) $sim_priceid = $key;	
		} // End foreach($price_idx as $key => $row)
		
		// Xac dinh mang sim va dau so
		$sim_category	= 1;
		$sim_dausoid	= 1;
		$dau1 = substr($sim_sim2,0,1);
		$dau2 = substr($sim_sim2,0,2);
		$dau3 = substr($sim_sim2,0,3);
		if(array_key_exists($dau3, $dauso_sim_idx)){
			$sim_dausoid	= $dauso_sim_idx[$dau3]['sds_id'];
			$sim_category	= $dauso_sim_idx[$dau3]['sds_category'];
		}elseif(array_key_exists($dau2, $dauso_sim_idx)){
			$sim_dausoid	= $dauso_sim_idx[$dau2]['sds_id'];
			$sim_category	= $dauso_sim_idx[$dau2]['sds_category'];
		}elseif(array_key_exists($dau1, $dauso_sim_idx)){
			$sim_dausoid	= $dauso_sim_idx[$dau1]['sds_id'];
			$sim_category	= $dauso_sim_idx[$dau1]['sds_category'];
		}  // End if(array_key_exists($dau, $dauso_sim_idx))

		// Tao chuoi string theo control
		if($control == 'xoa_sim' || $control == 'home_sim') $valuesx[] = $sim_sim2;
		else $valuesx[] = "('" . $sim_sim1 . "','" . $sim_sim2 . "','" . $sim_price . "','" . $sim_category . "','" . $sim_typeid . "','" . $sim_dailyid . "','" . $sim_priceid . "','" . $sim_dausoid . "','" . $sim_date . "','" . $sim_web . "', '" . $sim_keyword . "')";

		// Cứ đếm đủ 5k sim thì insert vào database 1 lần
		if($m%5000 == 0 ){
			quanlysim_control($control,$valuesx);
			
			// Chi truncate table 1 lan sau do la dang sim
			if($control == 'dang_xoa_sim') $control = 'dang_sim';
				
			$m = 0;
			unset( $valuesx );
		} // End if ( $m%5000 == 0 )
		
	} // End foreach ( $datapost['s'] as $kg => $sg )

	// Nếu không đủ 5k sim
	if ( 0 < $m && $m < 5000 ){
		quanlysim_control($control,$valuesx);
		unset( $valuesx );
	} // End if
	
	// Đưa ra thông báo đã insert thành công bao nhiêu sim
	echo thongbao( "Xử lý thành công ".$j." số !" );
    
} // End function quanlysim

function quanlysim_control($control,$valuesx){
	
	global $fs_table;

	if($control == 'dang_sim' || $control == 'dang_xoa_sim'){
		
		if($control == 'dang_xoa_sim'){
			$db_ex = new db_execute("TRUNCATE TABLE " . $fs_table);
			unset($db_ex);
		}

		$db_ex = new db_execute("INSERT IGNORE INTO " . $fs_table . " (sim_sim1,sim_sim2,sim_price,sim_category,sim_typeid,sim_dailyid,sim_priceid,sim_dausoid,sim_date,sim_web, sim_keyword) VALUES " . join( ",", $valuesx ));
		unset($db_ex);

	}elseif($control == 'xoa_sim'){
	
		$db_ex = new db_execute("DELETE FROM " . $fs_table . " 
										WHERE sim_sim2 IN (" . join( ",", $valuesx ) . ")
										");	
		unset($db_ex);
		
	}elseif($control == 'home_sim'){
		
		$db_ex = new db_execute("UPDATE " . $fs_table . "
										SET sim_hot = 0
										WHERE sim_hot = 1
										");
		unset($db_ex);
			
		$db_ex = new db_execute("UPDATE " . $fs_table . "
										SET sim_hot = 1
										WHERE sim_sim2 IN (" . join( ",", $valuesx ) . ")
										");	
		unset($db_ex);
		
	} // End if($control == 'dang_sim')	
	
} // End function quanlysim_control

function thongbao($msg){
	echo "<script>alert('".$msg."')</script>";
}
?>