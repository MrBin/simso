<?
require_once("config_security.php");
require_once("func.php");

checkAddEdit("add"); //check quyền thêm sửa xóa

// Khai bao bien
$tacvu 		= getValue("tacvu", "str", "POST", "dang_sim");
$action		= getValue("action", "str", "POST", "");
$upload_sim	= getValue("upload_sim", "str", "POST", "");

// Upload
if($action == 'submit') quanlysim($upload_sim, $tacvu);
?>

<html>
<head>
<title>Channel Add</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<?=$load_header?>
</head>

<body>
<? template_top(tt("Thêm mới bản ghi"))?>

<div class="textBlue">Đăng sim &nbsp;&nbsp; <img src="../../resource/images/line_red.gif"></div>

<form action="?" method="post" name="adminForm" id="adminForm" enctype="multipart/form-data">
	<table cellpadding="8" cellspacing="0">
		<tr>
			<td>Dữ liệu sim : </td>
			<td>
				<div class="marb10">Copy 3 cột Sim - Giá - Kho vào đây : </div>
				<textarea name="upload_sim" style="width:300px; height:150px; outline:none"></textarea>
			</td>
		</tr>
		<tr>
			<td>Chọn hành động : </td>
			<td valign="top">
				<div><input type="radio" name="tacvu" value="dang_sim" <? if($tacvu == 'dang_sim') echo 'checked'?>> Tạo sim mới trên hệ thống</div>
				<div><input type="radio" name="tacvu" value="dang_xoa_sim" <? if($tacvu == 'dang_xoa_sim') echo 'checked'?>> Tạo mới và xóa sim cũ trên hệ thống</div>
				<div><input type="radio" name="tacvu" value="xoa_sim" <? if($tacvu == 'xoa_sim') echo 'checked'?>> Xóa sim tương ứng trên hệ thống </div>
				<div><input type="radio" name="tacvu" value="home_sim" <? if($tacvu == 'home_sim') echo 'checked'?>> Đăng sim lên trang chủ </div>
			</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td><input type="submit" name="action" value="submit" class="form_button"></td>
		</tr>
	</table>
</form>

<ul style="line-height: 2em;">
	<li><a class="textBold textRed" href="delete_cache.php">Xóa cache</a></li>
	<li><a class="textBold textRed" href="simphongthuy.php" target="_blank">Cập nhật điểm phong thủy</a></li>
	<li><a class="textBold textRed" href="simphongthuy1.php" target="_blank">Lấy ra những sim có điểm cao</a></li>
	<li><a class="textBold textRed" href="repair.php" target="_blank">Tối ưu hóa bảng dữ liệu</a></li>
</ul>

<? template_bottom() ?>
</body>
</html>