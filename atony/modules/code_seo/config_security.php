<?
require_once("../../resource/security/security.php");

$module_id			 	= 24;

checkloggedin();
if (checkaccess($module_id) != 1){
	redirect("../../deny.htm");
	exit();
}

$fs_table				= "code_seo";
$field_id				= "cs_id";
$field_name				= "cs_name";

$arrPosition	 = array(0 => "-- Chọn vị trí --",
								1 => "Head",
								2 => "Body",
								3 => "Footer"
								);
?>