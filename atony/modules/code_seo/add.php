<?
require_once("config_security.php");

// kiem tra quyen them ban ghi
checkAddEdit("add"); 

// Khai bao bien
$errorMsg 			= "";
$action				= getValue("action", "str", "POST", "");

$add					= "add.php";
$listing				= "listing.php";
$edit					= "edit.php";
$after_save_data	= getValue("after_save_data", "str", "POST", $add);

$cs_strdate		= getValue("cs_strdate", "str", "POST", date("d/m/Y"));
$cs_strtime		= getValue("cs_strtime", "str", "POST", date("H:i:s"));
$cs_date			= convertDateTime($cs_strdate, $cs_strtime);

// class generate form
$myform = new generate_form();	//Call Class generate_form();
$myform->removeHTML(0);	//Loại bỏ chức năng không cho điền tag html trong form
$myform->addTable($fs_table);	// Add table
$myform->add("cs_position","cs_position",1,0,0,1,"Chọn mạng",0,"");
$myform->add("cs_name","cs_name",0,0,"",1,"Bạn chưa nhập tên code",1,"Tên code đã tồn tại");
$myform->add("cs_date","cs_date",1,1,0,0,"",0,"");
$myform->add("cs_description","cs_description",0,0,"",0,"",0,"");
$myform->evaluate(); // đổi tên trường thành biến và giá trị

// Nếu như có form được submit
if($action == "submitForm"){
	// Kiểm tra lỗi
	$errorMsg .= $myform->checkdata();
	$errorMsg .= $myform->strErrorFeld ;
	if($errorMsg == ""){	
	
		// Thuc hien query
		$db_ex	 		= new db_execute_return();
		$query			= $myform->generate_insert_SQL();
		$last_id 		= $db_ex->db_execute($query);
		$record_id 		= $last_id;
		//echo $query;exit();
		
		// Chuyen ve trang khac khi xu ly du lieu oki
		redirect($after_save_data . "?record_id=" . $record_id);
		exit();
		
	} // End if($errorMsg == ""){
	
} // End if($action == "submitForm")

// Khai bao ten form 
$myform->addFormname("submitForm"); //add  tên form để javacheck

// Javascript
$myform->addjavasrciptcode('');
$myform->checkjavascript();
?>

<html>
<head>
<title>Channel Add</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<?=$load_header?>
</head>

<body>
<? template_top(tt("Quản lý bản ghi"))?>

<table class="tab" border="0" cellpadding="0" cellspacing="0">
	<tr align="center">
		<td nowrap><div class="n" id="tabt_1" onClick="tabdiv(1,5);document.cookie='tab_select=1'"><?=tt("Chung")?></div></td>
		<td nowrap><div class="n" id="tabt_2" onClick="tabdiv(2,5);document.cookie='tab_select=2'"><?=tt("Mô tả")?></div></td>
		<td style="border-bottom:solid 1px #DADADA;" width="99%">&nbsp;</td>
	</tr>
</table>

<? $fs_action = $_SERVER['SCRIPT_NAME'] . "?" . @$_SERVER['QUERY_STRING'];?>
<?=$myform->create_form("submitForm", $fs_action, "post", "multipart/form-data","");?>
<?=$myform->errorMsg($errorMsg)?>

<div style="border:solid 1px #DADADA; border-top-width:0; padding:10px;">
	<div id="tabc_1" class="tabc">
		<?=$myform->create_table(8,3,"");?>
		<?=$myform->text_note()?>
		<tr>
			<td nowrap="nowrap" align="right"><font color="#FF0000">*</font> <?=tt("Chọn vị trí")?> :</td>
			<td>
				<select name="cs_position" id="cs_position" class="form_control">
					<?
					foreach($arrPosition as $kPos => $vPos){
					?>
					<option value="<?=$kPos?>" <? if($cs_position == $kPos){?>selected<? }?>>
						<?=$vPos?>
					</option>
					<?
					}
					?>
				</select>	
			</td>
		</tr>
        
		<?=$myform->text(0 , "Tên code", "cs_name", "cs_name", $cs_name, "Tên code", 150, "", 255, "", "", "")?>
		<script language="javascript">
		$(function() {
			$('#getDate').click(function() {
				$('#cs_strdate').val('<?=date("d/m/Y")?>');
				$('#cs_strtime').val('<?=date("H:i:s")?>');
			})
		})
		</script>
		<?=$myform->text(0, "Ngày cập nhật", "cs_strdate" . $myform->ec . "cs_strtime", "cs_strdate" . $myform->ec . "cs_strtime", $cs_strdate . $myform->ec . $cs_strtime, "Ngày (dd/mm/yyyy)" . $myform->ec . "Giờ (hh/mm/ss)", "70" . $myform->ec . "70", $myform->ec, "10" . $myform->ec . "10", " - ", $myform->ec, "&nbsp; <input id='getDate' type='button' value='Get date' class='form_button'>");?>
		<?=$myform->close_table();?>
	</div>   
    
	<div id="tabc_2" class="tabc">
		<?=$myform->wysiwyg("", "cs_description", $cs_description, "../../resource/ckeditor/", "99%", 300)?>
	</div>

    
	<?=$myform->create_table(8,3,"");?>
	<?=$myform->radio(0 , "Sau khi lưu dữ liệu", "add_new" . $myform->ec . "return_listing" . $myform->ec . "return_edit", "after_save_data", $add . $myform->ec . $listing . $myform->ec . $edit, $after_save_data, "Thêm mới" . $myform->ec . "Quay về danh sách" . $myform->ec . "Sửa bản ghi", "");?>
	<?=$myform->button("button" . $myform->ec . "reset", "button" . $myform->ec . "reset", "button" . $myform->ec . "reset", "Cập nhật"  . $myform->ec .  "Làm lại", "", "");?>
	<?=$myform->hidden("action", "action", "submitForm", "");?>
	<?=$myform->close_table();?>
</div>

<?=$myform->close_form();?>
<? unset($form);?>

<? $tab_select = getValue("tab_select","int","COOKIE",1);?>
<script>tabdiv(<?=$tab_select?>,5)</script>

<? template_bottom() ?>

</body>
</html>