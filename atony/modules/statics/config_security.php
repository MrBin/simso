<?
#+
require_once("../../resource/security/security.php");

#+
#+ Khai bao bien
$module_id = 9;		// module id trong field modules

#+
#+ kiem tra dang nhap
checkloggedin();
if (checkaccess($module_id) !=1){
	redirect("../deny.htm");
	exit();
}

#+
#+ Khai bao thong tin table, field id va name
$fs_table				= "tbl_statics"; 
$field_id				= "sta_id"; 
$field_name				= "sta_title";

#+
#+ Khai bao thong tin ve anh
$fs_filepath		= $root_path_admin.$path_static;
$extension_list 	= "jpg,gif,png,swf";
$limit_size			= 300000;
#+
$small_width		= 	150;
$small_heght		=	100;
$small_quantity		=	100;
#+
$medium_width		= 	300;
$medium_heght		=	200;
$medium_quantity	=	90;

#+
#+ Lay ra cac category theo type
$menu = new menu();
$sql = '';
$sql .= ' AND typ_type = "static"';
$listAll = $menu->getAllChild("tbl_category INNER JOIN tbl_category_type ON (typ_name_index = cat_type)", "cat_id", "cat_parent_id", "0", "tbl_category.lang_id = ".$_SESSION["lang_id"].$sql, "cat_id,cat_name,cat_type,typ_name", "typ_order,cat_type,cat_order ASC,cat_id ASC,cat_name ASC", "cat_has_child");
if($listAll=='') $listAll = array();

#+
#+ Limit record
$arrLimit = array(
	'5' => '5 items',
	'15' => '15 items',
	'30' => '30 items',
	'50' => '50 items',
	'80' => '80 items',
	'120' => '120 items'
	);
?>