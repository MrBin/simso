<?
#+
#+ Mr.LeToan - NTP.VnJp
require_once("config_security.php");

#+
#+ Khai bao bien
$sql			= "";
$sqlWhere		= "";

$limit     	 	= getValue("limit", "int", "GET");
$keyword		= getValue("keyword","str","GET","",1);
$iCat			= getValue("iCat");
$show			= getValue("show");

#+
#+ sap xep du lieu
$sort			= getValue("sort");
switch($sort){
	case 1: $sqlOrderBy = "sta_title ASC"; break;
	case 2: $sqlOrderBy = "sta_title DESC"; break;
	case 3: $sqlOrderBy = "sta_date ASC"; break;
	case 4: $sqlOrderBy = "sta_date DESC"; break;
	case 5: $sqlOrderBy = "sta_hits ASC"; break;
	case 6: $sqlOrderBy = "sta_hits DESC"; break;
	default:$sqlOrderBy = "sta_id DESC"; break;
}

#+
#+ Dua ra sql cho phu hop
if($keyword!="") 
	$sql.=" AND sta_title LIKE '%" . $keyword . "%'";
if($iCat!="") 
	$sql.=" AND sta_category =" . $iCat . "";
#+
if($show > 0){
	switch($show){
		case 1:	$sql .= " AND sta_picture <> '' "; break;
		case 2:	$sql .= " AND sta_picture IS NULL "; break;
		case 3:	$sql .= " AND sta_active = 1 "; break;
		case 4:	$sql .= " AND sta_active = 0 "; break;
		case 5:	$sql .= " AND sta_hot = 1 "; break;
		case 6:	$sql .= " AND sta_new = 1 "; break;
	}
}

#+
$normal_class    = "page";
$selected_class  = "pageselect";
$page_prefix     = "Trang : ";
$current_page    = ( getValue("page") < 1 ) ? 1 : getValue("page");
$page_size	     = ( $limit > 0 ) ? $limit : 15;
$url = "listing.php?keyword=" . $keyword . "&iCat=" . $iCat . "&limit=" . $limit . "&show=" . $show . "&sort=" . $sort."&page=";

#+
#+ Dem so luong ban ghi
$query = ' SELECT Count(*) AS count'
		.' FROM '.$fs_table
		.' INNER JOIN tbl_category ON (cat_id = sta_category)'
		.' WHERE '.$fs_table.'.lang_id = ' . $_SESSION["lang_id"] . $sql . $sqlWhere
		;
$db_query = new db_query($query);
$row_count 		= mysql_fetch_assoc($db_query->result);
$total_record 	= $row_count['count'];
$db_query->close();
unset($db_query);

$query = ' SELECT sta_id,sta_title,sta_title_index,sta_picture,sta_picture_web,sta_active,sta_hot,sta_new,sta_hits,sta_date,sta_auto'
		.' FROM '.$fs_table
		.' INNER JOIN tbl_category ON (cat_id = sta_category)'
		.' WHERE '.$fs_table.'.lang_id = ' . $_SESSION["lang_id"] . $sql . $sqlWhere
		.' ORDER BY ".$sqlOrderBy.", sta_date DESC, sta_id DESC'
		.' LIMIT '.($current_page-1) * $page_size . ',' . $page_size
		;
$db_query = new db_query($query);					 										
?>

<html>
<head>
<title>Channel Listing</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<?=$load_header?>
</head>

<body>
<!-- Load css và xử lý dữ liệu của jquery(kích hoạt, xóa bản ghi) -->
<div id="loadingBar">
    Đang xử lý ... Vui lòng đợi trong giây lát ... <br>
    <img align="absmiddle" src="../../resource/images/loading.gif">
</div>
<input class="controller" type="hidden" value="controllers" />
<? template_top(tt("Quản lý bản ghi") . "&nbsp;[" . number_format($total_record,0,".",",") . "]" )?>
<? $keyword=getValue("keyword","str","GET","");  ?>
<form action="listing.php" method="get" name="formSearch">
<table border="0" cellpadding="5" align="center">
	<tr>
    	<td>
            <select name="limit" id="limit" class="form" onChange="this.form.submit();">
            	<? foreach($arrLimit as $key => $row){?>
                <option value="<?=$key?>" <? if($key == $page_size){ echo 'selected';}?> ><?=$row?></option>
                <? }?>
            </select>
        </td>
        
        <td>
            <select title="Kiểu hiển thị video" name="show" class="form" onChange="this.form.submit()">
                <option value="0">Tùy chọn</option>
                <?
                $arrShow = array(1 => "Có ảnh", 2 => "Không có ảnh", 3 => "Kích hoạt", 4 => "Không kích hoạt", 5 => "Hot", 6 => "New");
                foreach($arrShow as $key => $value){
                ?>
                    <option title="<?=$value?>" value="<?=$key?>" <? if($key == $show){echo 'selected="selected"';}?>><?=$value?></option>
                <?
                }
                ?>
            </select>
        </td>

        <td>
            <select name="iCat" id="iCat" class="form" onChange="this.form.submit()">
                <option value=""><?=tt("Chọn danh mục")?></option>
                <?
				$cat_type = "";
				foreach($listAll as $i=>$cat){
					
					#+
					if($cat_type != $cat["cat_type"]){
						$cat_type = $cat["cat_type"];
					?>
						<optgroup label="---- <?=ucwords($cat["typ_name"])?> -----"></optgroup>
					<?
					}
					?>
						<option value="<?=$cat["cat_id"]?>" <? if($iCat == $cat["cat_id"]){?>selected<? }?>>
						<?
						for($j=0;$j<$cat["level"];$j++) echo '|--';
						echo $cat["cat_name"];
						?>
						</option>
					<?
				}
				?>
            </select>
        </td>
        <td><input type="text" class="form" name="keyword" value="<?=$keyword?>" style="width:120px" /></td>       
		<td><input type="submit" value="<?=tt("Tìm kiếm")?>" class="form_button" /></td>
	</tr>
</table>
</form>

<form action="quickedit.php?returnurl=<?=base64_encode(getURL())?>" method="post" name="form_listing" enctype="multipart/form-data">
	<input type="hidden" name="iQuick" value="update">
    <table border="1" cellpadding="5" cellspacing="0" width="100%" style="border-collapse:collapse" bordercolor="<?=$bordercolor?>">
        <tr class="textBold" align="center">
        	<td>&nbsp;</td>
            <td width="30"><?=translate_text("Lưu")?></td>
            <td><?=translate_text("Ảnh")?></td>
            <td>
                <div><?=translate_text("Tiêu đề")?></div>
                <div>
                    <?=generate_sort("asc", 1, $sort, $fs_imagepath)?>
                    <?=generate_sort("desc", 2, $sort, $fs_imagepath)?>
                </div>
            </td>
			<td>Link</td>
            <!--
            <td width="80">
                <div><?=translate_text("Cập nhật")?></div>
                <div>
                    <?=generate_sort("asc", 3, $sort, $fs_imagepath)?>
                    <?=generate_sort("desc", 4, $sort, $fs_imagepath)?>
                </div>
            </td>
            <td width="80">
                <div><?=translate_text("Lượt xem")?></div>
                <div>
                    <?=generate_sort("asc", 5, $sort, $fs_imagepath)?>
                    <?=generate_sort("desc", 6, $sort, $fs_imagepath)?>
                </div>
            </td>
            <td class="textBold" width="5%"><?=tt("Hot")?></td>
            <td class="textBold" width="5%"><?=tt("New")?></td>
            -->
            <td class="textBold" width="5%"><?=tt("Active")?></td>
            <td class="textBold" width="5%"><?=tt("Copy")?></td>
            <td class="textBold" width="5%"><?=tt("Sửa")?></td>
            <td class="textBold" width="5%"><?=tt("Xóa")?></td>
        </tr>
        
        <? if($total_record > $page_size){ ?>
        <tr>
            <td colspan="14" align="left" style="padding-right:5px">
                <div class="pagi"><?=generatePageBar($page_prefix,$current_page,$page_size,$total_record,$url,$normal_class,$selected_class);?></div>
            </td>
        </tr>
        <? } ?>
        
        <? 
        $countno = ($current_page-1) * $page_size;
        while($row=mysql_fetch_assoc($db_query->result)){
        $countno++;
		
			if($_SESSION["userlogin"] == 'banhang'){
				if($row['sta_id'] == 15){
				?>
				<tr <?=$fs_change_bg?> class="row-<?=$row["sta_id"]?> item-row" align="center">
					<td><input type="checkbox" name="record_id[]" id="record_id_<?=$countno?>" value="<?=$row["sta_id"]?>"></td>
					<td><img src="<?=$fs_imagepath?>save.png" border="0" style="cursor:pointer" onClick=" document.getElementById('record_id_<?=$countno;?>').checked=true ; document.form_listing.submit();" alt="Save"></td>
					<td>
					<? 
					$picture_path = $fs_filepath.$row['sta_title_index'].'/';
					if($row['sta_auto'] == 0){
						if( is_file($picture_path.$row['sta_picture'])){
							$picture 	= $row['sta_picture'];
							$picture_o 	= $picture_path.'m_'.$picture;
							$picture_s 	= $picture_path.'s_'.$picture;
						}else{
							$picture 	= $row['sta_picture_web'];
							$picture_o 	= $picture;
							$picture_s 	= $picture;	
						} // End if($row["sta_picture_web"] != '')	
					}else{
						
						if( is_file($picture_path.$row['sta_picture'])){
							$picture 	= $row['sta_picture'];
							$picture_o 	= $picture_path.'m_'.$picture;
							$picture_s 	= $picture_path.'s_'.$picture;
						}else{
							$picture 	= $row['sta_picture_web'];
							$picture_o 	= $picture;
							$picture_s 	= $picture;	
						} // End if($row["sta_picture_web"] != '')	
					}  // End if($row['sta_auto'] == 0)
					
					#+
					if($row["sta_picture"]!=''){
					?>
						<img width="35" height="25" src="<?=$picture_s?>" onMouseOver="showtip('<img src=\'<?=$picture_o?>\'>','<?=$bordercolor?>')" onMouseOut="hidetip()">
					<? 
					} // End if($row["sta_picture"]!='')
					?>
					</td>
					<td align="left" width="200"><input type="text" onKeyUp="document.getElementById('record_id_<?=$countno?>').checked=true" name="sta_title_<?=$row["sta_id"]?>" size="40" id="sta_title_<?=$row["sta_id"]?>" value="<?=replaceQ($row["sta_title"])?>" class="form"></td>
					
					<?
					$link = createLink('detail_static',$row);
					?>
					<td align="left">
						<?=$link?>
					</td>
					<td align="center"><img class="active" name="<?=$row["sta_active"]?>" id="<?=$row["sta_id"];?>" path="<?=$fs_imagepath?>" src="<?=$fs_imagepath?>active_<?=$row["sta_active"];?>.png" title="<?=tt("Kích hoạt bản ghi")?>" alt="<?=tt("Kích hoạt")?>" /></td>
					
					<td align="center"><img title="<?=tt("Sao chép bản ghi")?>" alt="<?=tt("Sao chép")?>" src="<?=$fs_imagepath?>copy.png" onClick="if (confirm('<?=tt("Bạn muốn sao chép bản ghi này?")?>')){ window.location.href='copy.php?record_id=<?=$row["sta_id"]?>&returnurl=<?=base64_encode(getURL())?>'}" border="0" style="cursor:pointer"></td>
					<td><a href="edit.php?record_id=<?=$row["sta_id"];?>&url=<?=base64_encode(getURL())?>"><img src="<?=$fs_imagepath?>edit.png" alt="<?=tt("Edit")?>" border="0"></a></td>
					<td align="center">
						<img class="remove" id="<?=$row["sta_id"];?>" src="<?=$fs_imagepath?>delete.png" title="<?=tt("Xóa bản ghi")?>" alt="<?=tt("Xóa")?>" />
					</td>
					
				</tr>
				<?	
				} // End  if($row['sta_id'] == 15)
				
			}else{
			?>
				<tr <?=$fs_change_bg?> class="row-<?=$row["sta_id"]?> item-row" align="center">
					<td><input type="checkbox" name="record_id[]" id="record_id_<?=$countno?>" value="<?=$row["sta_id"]?>"></td>
					<td><img src="<?=$fs_imagepath?>save.png" border="0" style="cursor:pointer" onClick=" document.getElementById('record_id_<?=$countno;?>').checked=true ; document.form_listing.submit();" alt="Save"></td>
					<td>
					<? 
					$picture_path = $fs_filepath.$row['sta_title_index'].'/';
					if($row['sta_auto'] == 0){
						if( is_file($picture_path.$row['sta_picture'])){
							$picture 	= $row['sta_picture'];
							$picture_o 	= $picture_path.'m_'.$picture;
							$picture_s 	= $picture_path.'s_'.$picture;
						}else{
							$picture 	= $row['sta_picture_web'];
							$picture_o 	= $picture;
							$picture_s 	= $picture;	
						} // End if($row["sta_picture_web"] != '')	
					}else{
						
						if( is_file($picture_path.$row['sta_picture'])){
							$picture 	= $row['sta_picture'];
							$picture_o 	= $picture_path.'m_'.$picture;
							$picture_s 	= $picture_path.'s_'.$picture;
						}else{
							$picture 	= $row['sta_picture_web'];
							$picture_o 	= $picture;
							$picture_s 	= $picture;	
						} // End if($row["sta_picture_web"] != '')	
					}  // End if($row['sta_auto'] == 0)
					
					#+
					if($row["sta_picture"]!=''){
					?>
						<img width="35" height="25" src="<?=$picture_s?>" onMouseOver="showtip('<img src=\'<?=$picture_o?>\'>','<?=$bordercolor?>')" onMouseOut="hidetip()">
					<? 
					} // End if($row["sta_picture"]!='')
					?>
					</td>
					<td align="left" width="200"><input type="text" onKeyUp="document.getElementById('record_id_<?=$countno?>').checked=true" name="sta_title_<?=$row["sta_id"]?>" size="40" id="sta_title_<?=$row["sta_id"]?>" value="<?=replaceQ($row["sta_title"])?>" class="form"></td>
					
					<?
					$link = createLink('detail_static',$row);
					?>
					<td align="left">
						<?=$link?>
					</td>
					<td align="center"><img class="active" name="<?=$row["sta_active"]?>" id="<?=$row["sta_id"];?>" path="<?=$fs_imagepath?>" src="<?=$fs_imagepath?>active_<?=$row["sta_active"];?>.png" title="<?=tt("Kích hoạt bản ghi")?>" alt="<?=tt("Kích hoạt")?>" /></td>
					
					<td align="center"><img title="<?=tt("Sao chép bản ghi")?>" alt="<?=tt("Sao chép")?>" src="<?=$fs_imagepath?>copy.png" onClick="if (confirm('<?=tt("Bạn muốn sao chép bản ghi này?")?>')){ window.location.href='copy.php?record_id=<?=$row["sta_id"]?>&returnurl=<?=base64_encode(getURL())?>'}" border="0" style="cursor:pointer"></td>
					<td><a href="edit.php?record_id=<?=$row["sta_id"];?>&url=<?=base64_encode(getURL())?>"><img src="<?=$fs_imagepath?>edit.png" alt="<?=tt("Edit")?>" border="0"></a></td>
					<td align="center">
						<img class="remove" id="<?=$row["sta_id"];?>" src="<?=$fs_imagepath?>delete.png" title="<?=tt("Xóa bản ghi")?>" alt="<?=tt("Xóa")?>" />
					</td>
					
				</tr>
			<?
			} // End if
		} // End while
		?>
        <? if($total_record > $page_size){ ?>
        <tr>
            <td colspan="14" align="right" style="padding-right:5px">
                <div class="pagi"><?=generatePageBar($page_prefix,$current_page,$page_size,$total_record,$url,$normal_class,$selected_class);?></div>
            </td>
        </tr>
        <? } ?>
    </table>
</form>
<? template_bottom() ?>
</body>
</html>
