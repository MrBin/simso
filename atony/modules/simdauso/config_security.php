<?
require_once("../../resource/security/security.php");

$module_id			 	= 18;

checkloggedin();
if (checkaccess($module_id) != 1){
	redirect("../../deny.htm");
	exit();
}

$fs_table				= "tbl_simdauso";
$field_id				= "sds_id";
$field_name				= "sds_name";

// Lay ra cac category theo type
$menu = new menu();
$sql = ' AND typ_type = "sim"';
$listAll = $menu->getAllChild("tbl_category INNER JOIN tbl_category_type ON (typ_name_index = cat_type)", "cat_id", "cat_parent_id", "0", "tbl_category.lang_id = ".$_SESSION["lang_id"].$sql, "cat_id,cat_name,cat_type,typ_name", "typ_order,cat_type,cat_order ASC,cat_id ASC,cat_name ASC", "cat_has_child");
if($listAll == '') $listAll = array();
?>