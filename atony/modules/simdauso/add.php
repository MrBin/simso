<?
require_once("config_security.php");

// kiem tra quyen them ban ghi
checkAddEdit("add"); 

// Khai bao bien
$errorMsg 			= "";
$action				= getValue("action", "str", "POST", "");

$add					= "add.php";
$listing				= "listing.php";
$edit					= "edit.php";
$after_save_data	= getValue("after_save_data", "str", "POST", $add);

$sds_strdate		= getValue("sds_strdate", "str", "POST", date("d/m/Y"));
$sds_strtime		= getValue("sds_strtime", "str", "POST", date("H:i:s"));
$sds_date			= convertDateTime($sds_strdate, $sds_strtime);

// class generate form
$myform = new generate_form();	//Call Class generate_form();
$myform->removeHTML(0);	//Loại bỏ chức năng không cho điền tag html trong form
$myform->addTable($fs_table);	// Add table
$myform->add("sds_category","sds_category",1,0,0,1,"Chọn mạng",0,"");
$myform->add("sds_name","sds_name",0,0,"",1,"Bạn chưa nhập đầu số",1,"Đầu số đã tồn tại");
$myform->add("sds_order","sds_order",1,1,0,0,"",0,"");
$myform->add("sds_date","sds_date",1,1,0,0,"",0,"");
$myform->add("sds_description","sds_description",0,0,"",0,"",0,"");
$myform->add("meta_title","meta_title",0,0,"",0,"",0,"");
$myform->add("meta_keyword","meta_keyword",0,0,"",0,"",0,"");
$myform->add("meta_description","meta_description",0,0,"",0,"",0,"");
$myform->evaluate(); // đổi tên trường thành biến và giá trị

// Nếu như có form được submit
if($action == "submitForm"){
	// Kiểm tra lỗi
	$errorMsg .= $myform->checkdata();
	$errorMsg .= $myform->strErrorFeld ;
	if($errorMsg == ""){	
	
		// Thuc hien query
		$db_ex	 		= new db_execute_return();
		$query			= $myform->generate_insert_SQL();
		$last_id 		= $db_ex->db_execute($query);
		$record_id 		= $last_id;
		//echo $query;exit();
		
		// Chuyen ve trang khac khi xu ly du lieu oki
		redirect($after_save_data . "?record_id=" . $record_id);
		exit();
		
	} // End if($errorMsg == ""){
	
} // End if($action == "submitForm")

// Khai bao ten form 
$myform->addFormname("submitForm"); //add  tên form để javacheck

// Javascript
$myform->addjavasrciptcode('');
$myform->checkjavascript();
?>

<html>
<head>
<title>Channel Add</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<?=$load_header?>
</head>

<body>
<? template_top(tt("Quản lý bản ghi"))?>

<table class="tab" border="0" cellpadding="0" cellspacing="0">
	<tr align="center">
		<td nowrap><div class="n" id="tabt_1" onClick="tabdiv(1,5);document.cookie='tab_select=1'"><?=tt("Chung")?></div></td>
		<td nowrap><div class="n" id="tabt_2" onClick="tabdiv(2,5);document.cookie='tab_select=2'"><?=tt("Mô tả")?></div></td>
		<td nowrap><div class="n" id="tabt_3" onClick="tabdiv(3,5);document.cookie='tab_select=3'"><?=tt("SEO")?></div></td>
		<td style="border-bottom:solid 1px #DADADA;" width="99%">&nbsp;</td>
	</tr>
</table>

<? $fs_action = $_SERVER['SCRIPT_NAME'] . "?" . @$_SERVER['QUERY_STRING'];?>
<?=$myform->create_form("submitForm", $fs_action, "post", "multipart/form-data","");?>
<?=$myform->errorMsg($errorMsg)?>

<div style="border:solid 1px #DADADA; border-top-width:0; padding:10px;">
	<div id="tabc_1" class="tabc">
		<?=$myform->create_table(8,3,"");?>
		<?=$myform->text_note()?>
		<tr>
			<td nowrap="nowrap" align="right"><font color="#FF0000">*</font> <?=tt("Chọn danh mục")?> :</td>
			<td>
				<select name="sds_category" id="sds_category" class="form_control">
					<option value=""><?=tt("Chọn danh mục")?></option>
					<?
					foreach($listAll as $i=>$cat){
					?>
					<option value="<?=$cat["cat_id"]?>" <? if($sds_category == $cat["cat_id"]){?>selected<? }?>>
						<?
						for($j=0;$j<$cat["level"];$j++) echo '|--';
						echo $cat["cat_name"];
						?>
					</option>
					<?
					}
					?>
				</select>	
			</td>
		</tr>
        
		<?=$myform->text(0 , "Tên", "sds_name", "sds_name", $sds_name, "Tên", 150, "", 255, "", "", "")?>
		<?=$myform->hidden("sds_name_check", "sds_name_check", $sds_name, "")?>
		<script language="javascript">
		$(function() {
			$('#getDate').click(function() {
				$('#sds_strdate').val('<?=date("d/m/Y")?>');
				$('#sds_strtime').val('<?=date("H:i:s")?>');
			})
		})
		</script>
		<?=$myform->text(0, "Ngày cập nhật", "sds_strdate" . $myform->ec . "sds_strtime", "sds_strdate" . $myform->ec . "sds_strtime", $sds_strdate . $myform->ec . $sds_strtime, "Ngày (dd/mm/yyyy)" . $myform->ec . "Giờ (hh/mm/ss)", "70" . $myform->ec . "70", $myform->ec, "10" . $myform->ec . "10", " - ", $myform->ec, "&nbsp; <input id='getDate' type='button' value='Get date' class='form_button'>");?>
		<?=$myform->close_table();?>
	</div>   
    
	<div id="tabc_2" class="tabc">
		<?=$myform->wysiwyg("", "sds_description", $sds_description, "../../resource/ckeditor/", "99%", 300)?>
	</div>
    
	<div id="tabc_3" class="tabc">
		<div class="mart10">Google Title :</div>
		<textarea name="meta_title" id="meta_title" class="form_control" style="width:90%; height:50px;"><?=$meta_title?></textarea>
		
		<div class="mart10">Google Description :</div>
		<textarea name="meta_description" id="meta_description" class="form_control" style="width:90%; height:100px;"><?=$meta_description?></textarea>
		
		<div class="mart10">Google Keyword : <span> - <i class="font11">(Các keyword cách nhau bằng dầu phẩy ',')</i></span></div>
		<textarea name="meta_keyword" id="meta_keyword" class="form_control" style="width:90%; height:100px;"><?=$meta_keyword?></textarea>
	</div>
    
	<?=$myform->create_table(8,3,"");?>
	<?=$myform->radio(0 , "Sau khi lưu dữ liệu", "add_new" . $myform->ec . "return_listing" . $myform->ec . "return_edit", "after_save_data", $add . $myform->ec . $listing . $myform->ec . $edit, $after_save_data, "Thêm mới" . $myform->ec . "Quay về danh sách" . $myform->ec . "Sửa bản ghi", "");?>
	<?=$myform->button("button" . $myform->ec . "reset", "button" . $myform->ec . "reset", "button" . $myform->ec . "reset", "Cập nhật"  . $myform->ec .  "Làm lại", "", "");?>
	<?=$myform->hidden("action", "action", "submitForm", "");?>
	<?=$myform->close_table();?>
</div>

<?=$myform->close_form();?>
<? unset($form);?>

<? $tab_select = getValue("tab_select","int","COOKIE",1);?>
<script>tabdiv(<?=$tab_select?>,5)</script>

<? template_bottom() ?>

</body>
</html>