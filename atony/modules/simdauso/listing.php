<?php
require_once("config_security.php");

//Keyword filter
$limit     	 	= getValue("limit", "int", "GET");
$keyword			= getValue("keyword", "str", "GET", "", 1);

$sql = "";
if($keyword != "") $sql .= " AND sds_name LIKE '%" . $keyword . "%' ";

$db_sim_dauso = new db_query("SELECT * 
										FROM " . $fs_table ." 
										WHERE lang_id=" . $_SESSION["lang_id"] . $sql ." 
										ORDER BY sds_category DESC, sds_order DESC,sds_id DESC"
										);
?>

<html>
<head>
<title>Channel Listing</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<?=$load_header?>
</head>

<body>
<!-- Load css và xử lý dữ liệu của jquery(kích hoạt, xóa bản ghi) -->
<div id="loadingBar">
    Đang xử lý ... Vui lòng đợi trong giây lát ... <br>
    <img align="absmiddle" src="../../resource/images/loading.gif">
</div>
<input class="controller" type="hidden" value="controllers" />

<?=template_top(tt("Danh sách bản ghi"));?>

<div style="position: absolute; top: 0; right: 5px;">
	<form action="listing.php" method="get" name="timkhohang">
	<table border="0" cellpadding="5" align="center">
		<tr>
			<td><input type="text" class="form" name="keyword" value="<?=$keyword?>" style="width:120px" /></td>       
			<td><input type="submit" value="<?=tt("Tìm kiếm")?>" class="form_button" /></td>
		</tr>
	</table>
	</form>
</div>

<form action="quickedit.php?returnurl=<?=base64_encode(getURL())?>" method="post" name="form_listing" enctype="multipart/form-data">
	<input type="hidden" name="iQuick" value="update">
		<table border="1" cellpadding="8" cellspacing="0" width="100%" style="border-collapse:collapse" bordercolor="<?=$bordercolor?>">
			<tr align="center" height="40" class="textBold">
			<td nowrap><input type="checkbox" id="check_all" onClick="check('1','1000')"></td>
			<td nowrap><?=tt("Lưu")?></td>
			<td nowrap><?=tt("Cat")?></td>
			<td nowrap><?=tt("Tên")?></td>
			<td nowrap><?=tt("Thứ tự")?></td>
			<td nowrap><?=tt("Home")?></td>
			<td nowrap><?=tt("Kích hoạt")?></td>
			<td nowrap><?=tt("Copy")?></td>
			<td nowrap><?=tt("Sửa")?></td>
			<td nowrap><?=tt("Xóa")?></td>
		</tr>
        
		<?
		$countno = 0;
		while($row = mysql_fetch_assoc($db_sim_dauso->result)){
			$countno++;
		?>
		<tr <?=$fs_change_bg?> class="row-<?=$row['sds_id']?> item-row" align="center">
			<td><input type="checkbox" name="record_id[]" id="record_id_<?=$countno?>" value="<?=$row['sds_id']?>"></td>
			<td><img src="<?=$fs_imagepath?>save.png" border="0" style="cursor:pointer" onClick=" document.getElementById('record_id_<?=$countno;?>').checked=true ; document.form_listing.submit();" alt="Save"></td>
			<td nowrap>
				<select name="sds_category_<?=$row['sds_id']?>" id="sds_category_<?=$row['sds_id']?>" class="form">
					<option value=""><?=tt("Chọn danh mục")?></option>
					<?
					foreach($listAll as $i=>$cat){
					?>
						<option value="<?=$cat["cat_id"]?>" <? if($row['sds_category'] == $cat["cat_id"]){?>selected<? }?>>
							<?
							for($j=0;$j<$cat["level"];$j++) echo '|--';
							echo $cat["cat_name"];
							?>
						</option>
					<?
					}
					?>
				</select>
			</td>
			<td nowrap>
				<input type="text" name="sds_name_<?=$row['sds_id']?>" size="30" id="sds_name_<?=$row['sds_id']?>" value="<?=$row['sds_name']?>" onKeyUp="document.getElementById('record_id_<?=$countno?>').checked=true" class="form">
				<input type="hidden" name="sds_name_check_<?=$row['sds_id']?>" size="30" id="sds_name_check_<?=$row['sds_id']?>" value="<?=$row['sds_name']?>" onKeyUp="document.getElementById('record_id_<?=$countno?>').checked=true" class="form">
			</td>
			<td nowrap><input type="text" name="sds_order_<?=$row['sds_id']?>" id="sds_order_<?=$row['sds_id']?>" value="<?=$row['sds_order']?>" onKeyUp="document.getElementById('record_id_<?=$countno?>').checked=true"  style=" width:40px;" class="form"></td>
			<td align="center"><img class="home" name="<?=$row['sds_home']?>" id="<?=$row['sds_id']?>" path="<?=$fs_imagepath?>" src="<?=$fs_imagepath?>active_<?=$row['sds_home']?>.png" title="<?=tt("Kích hoạt bản ghi")?>" alt="<?=tt("Kích hoạt")?>" /></td>
			<td align="center"><img class="active" name="<?=$row['sds_active']?>" id="<?=$row['sds_id']?>" path="<?=$fs_imagepath?>" src="<?=$fs_imagepath?>active_<?=$row['sds_active']?>.png" title="<?=tt("Kích hoạt bản ghi")?>" alt="<?=tt("Kích hoạt")?>" /></td>
			<td align="center"><img title="<?=tt("Sao chép bản ghi")?>" alt="<?=tt("Sao chép")?>" src="<?=$fs_imagepath?>copy.png" onClick="if (confirm('<?=tt("Bạn muốn sao chép bản ghi này?")?>')){ window.location.href='copy.php?record_id=<?=$row['sds_id']?>&returnurl=<?=base64_encode(getURL())?>'}" border="0" style="cursor:pointer"></td>
			<td><a href="edit.php?record_id=<?=$row['sds_id'];?>&url=<?=base64_encode(getURL())?>"><img src="<?=$fs_imagepath?>edit.png" alt="<?=tt("Edit")?>" border="0"></a></td>
			<td><img class="remove" id="<?=$row['sds_id']?>" src="<?=$fs_imagepath?>delete.png" /></td>
		</tr>
		<?
		} // End while($row = mysql_fetch_assoc($db_sim_dauso->result))
		unset($db_sim_dauso);
		?>	
	</table>
</form>
<? template_bottom(); ?>
</body>
</html>