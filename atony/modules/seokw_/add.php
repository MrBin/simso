<?
#+
#+ Mr.LeToan - NTP.VnJp
require_once("config_security.php");	//check security	 
checkAddEdit("add"); //check quyền thêm sửa xóa

#+
#+ Khai bao bien
$errorMsg 			= "";		//Warning Error!
$action				= getValue("action", "str", "POST", "");

$add				= "add.php";
$listing			= "listing.php";
$edit				= "edit.php";
$after_save_data	= getValue("after_save_data", "str", "POST", $edit);

$seo_strdate		= getValue("seo_strdate", "str", "POST", date("d/m/Y"));
$seo_strtime		= getValue("seo_strtime", "str", "POST", date("H:i:s"));
$seo_date			= convertDateTime($seo_strdate, $seo_strtime);
	
#+
#+ Goi class generate form
$myform = new generate_form();	//Call Class generate_form();
$myform->removeHTML(0);	//Loại bỏ chức năng không cho điền tag html trong form
#+
#+ Khai bao bang du lieu
$myform->addTable($fs_table);	// Add table
#+
#+ Khai bao thong tin cac truong
$myform->add("seo_keyword","seo_keyword",0,0,"",1,tt("Điền keyword SEO"),0,"");
$myform->add("seo_link","seo_link",0,0,"",1,tt("Điền link SEO"),0,"");
$myform->add("seo_date","seo_date",1,1,0,0,"",0,"");

#+
#+ Nếu như có form được submit
if($action == "submitForm"){
	
	#+
	#+ Kiểm tra lỗi
    $errorMsg .= $myform->checkdata();
	$errorMsg .= $myform->strErrorFeld ;	//Check Error!

	#+
	#+ Nếu như không có lỗi
    if($errorMsg == ""){

        #+
		#+ Thuc hien query
		$db_ex	 		= new db_execute_return();
		$query			= $myform->generate_insert_SQL();
		$last_id 		= $db_ex->db_execute($query);
		$record_id 		= $last_id;
		//echo $query;exit();
	
        redirect($after_save_data . "?record_id=" . $record_id);
		exit();
        //End
    } // End if($errorMsg == ""){
	
} // End if($action == "submitForm")


#+
#+ Khai bao ten form 
$myform->addFormname("submitForm"); //add  tên form để javacheck
#+
#+ đổi tên trường thành biến và giá trị
$myform->evaluate();
#+
#+ Xử lý javascript
$myform->addjavasrciptcode('');
$myform->checkjavascript();
?>

<html>
<head>
<title>Administrator Control panel</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<?=$load_header?>
</head>
<body>
<? template_top(tt("Quản lý bản ghi"))?>

<table class="tab" border="0" cellpadding="0" cellspacing="0">
    <tr align="center">
        <td nowrap>
            <div class="n" id="tabt_1" onClick="tabdiv(1,5);document.cookie='tab_select=1'"><?=tt("Chung")?></div>
        </td>
        <td style="border-bottom:solid 1px #DADADA;" width="99%">&nbsp;</td>
    </tr>
</table>

<?
$fs_action			= $_SERVER['SCRIPT_NAME'] . "?" . @$_SERVER['QUERY_STRING'];
?>
<?=$myform->create_form("submitForm", $fs_action, "post", "multipart/form-data","");?>


<div style="border:solid 1px #DADADA; border-top-width:0; padding:10px;">
	<div id="tabc_1" class="tabc">
		<?=$myform->create_table(8,0,"");?>
		<?=$myform->text_note()?>
        <?=$myform->errorMsg($errorMsg)?>							
        <div class="mart10">
			Keyword :
		</div>
		<input type="text" name="seo_keyword" id="seo_keyword" value="<?=$seo_keyword?>" class="form_control" style="width:50%">
		<div class="mart10">
			Đường dẫn thay thế :
		</div>
        <input type="text" name="seo_link" id="seo_link" value="<?=$seo_link?>" class="form_control" style="width:50%">
  
	
        <?=$myform->text(0, "Ngày cập nhật", "seo_strdate" . $myform->ec . "seo_strtime", "seo_strdate" . $myform->ec . "seo_strtime", $seo_strdate . $myform->ec . $seo_strtime, "Ngày (dd/mm/yyyy)" . $myform->ec . "Giờ (hh/mm/ss)", "70" . $myform->ec . "70", $myform->ec, "10" . $myform->ec . "10", " - ", $myform->ec, "");?>
        <?=$myform->close_table();?>
    </div>

	<?=$myform->create_table(8,3,"");?>
    <?=$myform->radio(0 , "Sau khi lưu dữ liệu", "add_new" . $myform->ec . "return_listing" . $myform->ec . "return_edit", "after_save_data", $add . $myform->ec . $listing . $myform->ec . $edit, $after_save_data, "Thêm mới" . $myform->ec . "Quay về danh sách" . $myform->ec . "Sửa bản ghi", "");?>
    <?=$myform->button("button" . $myform->ec . "reset", "button" . $myform->ec . "reset", "button" . $myform->ec . "reset", "Cập nhật"  . $myform->ec .  "Làm lại", "", "");?>
    <?=$myform->hidden("action", "action", "submitForm", "");?>
    <?=$myform->close_table();?>
</div>

<?=$myform->close_form();?>
<? unset($myform);?>
<? template_bottom() ?>
<? /*------------------------------------------------------------------------------------------------*/ ?>
</body>
</html>