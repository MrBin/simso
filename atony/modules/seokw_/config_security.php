<?
#+
require_once("../../resource/security/security.php");

#+
#+ Khai bao bien
$module_id = 8;		// module id trong field modules

#+
#+ kiem tra dang nhap
checkloggedin();
if (checkaccess($module_id) !=1){
	redirect("../deny.htm");
	exit();
}

#+
#+ Khai bao thong tin table, field id va name
$fs_table				= "tbl_seokw"; 
$field_id				= "seo_id"; 
$field_name				= "seo_keyword";

#+
#+ Limit record
$arrLimit = array(
	'5' => '5 items',
	'15' => '15 items',
	'30' => '30 items',
	'50' => '50 items',
	'80' => '80 items',
	'120' => '120 items'
	);
?>