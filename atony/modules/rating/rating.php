<?
include("config_security.php");

//Khai báo biến khi hiển thị danh sách
$fs_title		= $module_name . " | Sản phẩm";
$fs_action		= getURL(0,0,1,1,"record_id");
$fs_redirect	= getURL(0,0,1,1,"record_id|r") . "&r=" . random();
$fs_errorMsg	= "";
$fs_alertMsg	= "";

//Search data
$keyword		= getValue("keyword", "str", "GET", "", 1);
$filter		= getValue("filter", "int", "GET", -1);
$poster		= getValue("poster", "int", "GET", -1);
$status		= getValue("status", "int", "GET", -1);
$sqlWhere	= "";
if($keyword != ""){
	if(mb_substr($keyword, 0, 1, "UTF-8") == "#"){
		$id		= convert_list_to_list_id(str_replace("#", "", $keyword));
		$sqlWhere	.= " AND rat_id IN (" . $id . ")";
	}
	else $sqlWhere	.= " AND (rat_name LIKE '%" . $keyword . "%' OR rat_content LIKE '%" . $keyword . "%')";
}
switch($filter){
	case 0: $sqlWhere	.= " AND rat_count_reply = 0"; break;
	case 1: $sqlWhere	.= " AND rat_count_reply > 0"; break;
}
switch($poster){
	case 0: $sqlWhere	.= " AND rat_admin_id = 0"; break;
	case 1: $sqlWhere	.= " AND rat_admin_id > 0"; break;
}
if($status > -1)	$sqlWhere	.= " AND rat_status = " . $status;

//Sort data
$sort			= getValue("sort");
switch($sort){
	case 1: $sqlOrderBy = "rat_date ASC"; break;
	case 2: $sqlOrderBy = "rat_date DESC"; break;
	default:$sqlOrderBy = "rat_date DESC"; break;
}

//Get page break params
$page_size		= 100;
$page_prefix	= "Trang: ";
$normal_class	= "page";
$selected_class= "page_current";
$previous		= "<";
$next				= ">";
$first			= "<<";
$last				= ">>";
$break_type		= 1;//"1 => << < 1 2 [3] 4 5 > >>", "2 => < 1 2 [3] 4 5 >", "3 => 1 2 [3] 4 5", "4 => < >"
$url				= getURL(0,0,1,1,"page");
$db_count		= new db_query("SELECT COUNT(*) AS count
										 FROM " . $fs_table . "
										 WHERE rat_type = " . RATING_TYPE_PRODUCT . $sqlWhere);
$listing_count	= mysql_fetch_assoc($db_count->result);
$total_record	= $listing_count["count"];
$current_page	= getValue("page", "int", "GET", 1);
if($total_record % $page_size == 0) $num_of_page = $total_record / $page_size;
else $num_of_page = (int)($total_record / $page_size) + 1;
if($current_page > $num_of_page) $current_page = $num_of_page;
if($current_page < 1) $current_page = 1;
$db_count->close();
unset($db_count);
//End get page break params
$db_listing	= new db_query("SELECT " . $fs_table . ".*, IF(rat_count_reply > 0, 1, 0) AS has_reply
									 FROM " . $fs_table . "
									 WHERE rat_type = " . RATING_TYPE_PRODUCT . $sqlWhere . "
									 ORDER BY " . $sqlOrderBy . "
									 LIMIT " . ($current_page - 1) * $page_size . ", " . $page_size);
$list_id		= convert_result_set_2_list($db_listing->result, "rat_admin_id");
$db_admin	= new db_query("SELECT adm_id, adm_loginname FROM tbl_admin WHERE adm_id IN (" . $list_id . ")");
$arrAdmin	= convert_result_set_2_array($db_admin->result, "adm_id", "adm_loginname");
$db_admin->close();
unset($db_admin);
?>
<html>
<head>
<title>Channel Listing</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<?=$load_header?>
</head>

<body>
<!-- Load css và xử lý dữ liệu của jquery(kích hoạt, xóa bản ghi) -->
<div id="loadingBar">
    Đang xử lý ... Vui lòng đợi trong giây lát ... <br>
    <img align="absmiddle" src="../../resource/images/loading.gif">
</div>
<input class="controller" type="hidden" value="controllers" />

<? template_top(tt("Quản lý bản ghi") . "&nbsp;[" . number_format($total_record,0,".",",") . "]" )?>

<div class="pagi" style="position: absolute; top: 0; left: 50%; transform: translatex(-50%);">
	<?=generatePageBar($page_prefix,$current_page,$page_size,$total_record,$url,$normal_class,$selected_class);?>
</div>

<form action="rating.php" method="get" name="formSearch">
<table border="0" cellpadding="5" align="center">
	<tr>
		<td>
			<?=generate_filter(array(-1 => "- Trạng thái -", 0 => "Chưa duyệt", 1 => "Đã duyệt"), "status", $status)?>
		</td>
		<td><input type="text" class="form" name="keyword" value="<?=$keyword?>" style="width:120px" /></td>       
		<td><input type="submit" value="<?=tt("Tìm kiếm")?>" class="form_button" /></td>
	</tr>
</table>
</form>

<form action="quickedit.php?returnurl=<?=base64_encode(getURL())?>" method="post" name="form_listing" enctype="multipart/form-data">
	<input type="hidden" name="iQuick" value="update">
    <table border="1" cellpadding="5" cellspacing="0" width="100%" style="border-collapse:collapse" bordercolor="<?=$bordercolor?>">
		<tr class="textBold" align="center">
			<td width="30">STT</td>
			<td><?=translate_text("Trang bình luận")?></td>
			<td><?=translate_text("Nội dung")?></td>
			<td>Phản hồi</td>
			<td width="80">
				<div><?=translate_text("Ngày tạo")?></div>
				<div>
					<?=generate_sort("asc", 1, $sort, $fs_imagepath)?>
					<?=generate_sort("desc", 2, $sort, $fs_imagepath)?>
				</div>
			</td>
			<td class="textBold" width="5%"><?=tt("Duyệt")?></td>
			<td class="textBold" width="5%"><?=tt("Sửa")?></td>
			<td class="textBold" width="5%"><?=tt("Xóa")?></td>
		</tr>
        
		<? 
		$countno = ($current_page-1) * $page_size;
		while($row = mysql_fetch_assoc($db_listing->result)){
			$countno++;
		?>
		<tr <?=$fs_change_bg?> class="row-<?=$row["rat_id"]?> item-row" align="center">
			<td><?=$countno?></td>
			<td>
				<?=$arrPageRating[$row["rat_record_id"]]?>
			</td>
			<td align="left"><?=$row["rat_content"]?></td>
			<td align="left"><? if($row["rat_count_reply"] > 0) echo format_number($row["rat_count_reply"]);?></td>
			<td>
				<div><?=date("d/m/Y", $row["rat_date"])?></div>
				<div style="color:#666666; font-size:10px"><?=date("H:i A", $row["rat_date"])?></div>
			</td>
			<td align="center"><img class="active" name="<?=$row["rat_status"]?>" id="<?=$row["rat_id"];?>" path="<?=$fs_imagepath?>" src="<?=$fs_imagepath?>active_<?=$row["rat_status"];?>.png" title="<?=tt("Duyệt bản ghi")?>" alt="<?=tt("Duyệt")?>" /></td>
			<td><a href="edit.php?type=rating&record_id=<?=$row["rat_id"];?>&url=<?=base64_encode(getURL())?>"><img src="<?=$fs_imagepath?>edit.png" alt="<?=tt("Edit")?>" border="0"></a></td>
			<td align="center"><img class="remove" id="<?=$row["rat_id"];?>" src="<?=$fs_imagepath?>delete.png" title="<?=tt("Xóa bản ghi")?>" alt="<?=tt("Xóa")?>" /></td>  
		</tr>
	<? 
	} // End while($row = mysql_fetch_assoc($db_news->result))
	unset($db_listing);
	?>
	</table>
</form>

<? if($total_record > $page_size){ ?>     
<div class="pagi"><?=generatePageBar($page_prefix,$current_page,$page_size,$total_record,$url,$normal_class,$selected_class);?></div>
<? } ?>

<? template_bottom() ?>
</body>
</html>