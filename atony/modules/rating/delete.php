<?
include("config_security.php");

$rating		= new rating(RATING_TYPE_PRODUCT);
$ajax			= getValue("ajax");
$type			= getValue("type", "str", "GET", "");
$record_id	= getValue("record_id");
$redirect	= getValue("redirect", "str", "GET", base64_encode("rating.php"));
$redirect	= base64_decode($redirect);

switch($type){
	case "rating"			: $rating->delete_rating($record_id); break;
	case "rating_reply"	: $rating->delete_rating_reply($record_id); break;
}
if($ajax == 1) echo 1;
else redirect($redirect);
?>