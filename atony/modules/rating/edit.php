<?
include("config_security.php");

//Khai báo biến khi thêm mới
$redirect			= getValue("redirect", "str", "GET", base64_encode("rating.php"));
$fs_title			= $module_name . " | Sửa đổi";
$fs_action			= getURL();
$fs_redirect		= base64_decode($redirect);
$fs_errorMsg		= "";

$class_rating		= new rating(RATING_TYPE_PRODUCT);

//Get data edit
$popup				= getValue("popup");
$type					= getValue("type", "str", "GET", "rating");
$record_id			= getValue("record_id");
// Thông tin Rating Reply
if($type == "rating"){
	$fs_title		= "Sửa đánh giá";
	$prefix			= "rat";
	$fs_table		= "rating";
	$id_field		= "rat_id";
	$queryStr		= "SELECT rat_record_id, rat_category_id, rat_order_id, rat_rating, rat_content AS content, rat_name AS name, rat_mobile AS mobile, rat_date AS date, rat_status AS status
							FROM " . $fs_table . "
							WHERE " . $id_field . " = " . $record_id;
}
// Thông tin Rating
else{
	$fs_title		= "Sửa trả lời đánh giá";
	$prefix			= "rr";
	$fs_table		= "rating_reply";
	$id_field		= "rr_id";
	$queryStr		= "SELECT rr_content AS content, rr_name AS name, rr_mobile AS mobile, rr_date AS date, rr_status AS status
							FROM " . $fs_table . "
							WHERE " . $id_field . " = " . $record_id;
	$fs_redirect	= base64_decode(base64_encode("rating_reply.php"));
}
$db_edit				= new db_query($queryStr);
if(mysql_num_rows($db_edit->result) == 0) redirect($fs_error);
$edit					= mysql_fetch_assoc($db_edit->result);
unset($db_edit);

//Lấy dữ liệu đề giữ nguyên trạng thái khi submit error
$rat_order_id		= getValue("rat_order_id", "int", "POST", @$edit["rat_order_id"]);
$rat_rating			= getValue("rat_rating", "int", "POST", @$edit["rat_rating"]);
$content				= getValue("content", "str", "POST", $edit["content"]);
$name					= getValue("name", "str", "POST", $edit["name"]);
$mobile				= getValue("mobile", "str", "POST", $edit["mobile"]);
$strdate				= getValue("strdate", "str", "POST", date("d/m/Y", $edit["date"]));
$strtime				= getValue("strtime", "str", "POST", date("H:i:s", $edit["date"]));
$date					= convertDateTime($strdate, $strtime);
$last_update		= $time;
$status				= getValue("status", "int", "POST", $edit["status"]);

//Get action variable for add new data
$action				= getValue("action", "str", "POST", "");
$myform = new generate_form();
//Check $action for execute
if($action == "submitForm"){
	
	//Lấy dữ liệu kiểu checkbox
	$status			= getValue("status", "int", "POST", 0);
	
	/*
	Call class form:
	1). Ten truong
	2). Ten form
	3). Kieu du lieu , 0 : string , 1 : kieu int, 2 : kieu email, 3 : kieu double, 4 : kieu hash password
	4). Noi luu giu data  0 : post, 1 : variable
	5). Gia tri mac dinh, neu require thi phai lon hon hoac bang default
	6). Du lieu nay co can thiet hay khong
	7). Loi dua ra man hinh
	8). Chi co duy nhat trong database
	9). Loi dua ra man hinh neu co duplicate
	*/
	
	//Add table insert data
	$myform->addTable($fs_table);
	$myform->add($prefix . "_content", "content", 0, 1, " ", 1, "Bạn chưa nhập Nội dung.", 0, "");
	$myform->add($prefix . "_name", "name", 0, 1, " ", 1, "Bạn chưa nhập Họ và tên.", 0, "");
	$myform->add($prefix . "_mobile", "mobile", 0, 1, "", 0, "", 0, "");
	$myform->add($prefix . "_date", "date", 1, 1, 0, 0, "", 0, "");
	$myform->add($prefix . "_last_update", "last_update", 1, 1, 0, 0, "", 0, "");
	$myform->add($prefix . "_status", "status", 1, 1, 0, 0, "", 0, "");

	//Check form data
	$fs_errorMsg .= $myform->checkdata();
	
	if($fs_errorMsg == ""){
		
		//Insert to database
		$myform->removeHTML(0);
		$db_update = new db_execute($myform->generate_update_SQL($id_field, $record_id));
		unset($db_update);

		// Cập nhật lại data
		if($type == "rating") $class_rating->recount_rating_data($edit["rat_record_id"], $edit["rat_category_id"]);
		
		//Redirect after insert complate
		if($popup == 1) exit('<script type="text/javascript">parent.location.reload();</script>');
		else redirect($fs_redirect);
		
	}//End if($fs_errorMsg == "")
	
}//End if($action == "execute")
#+
#+ Khai bao ten form 
$myform->addFormname("submitForm"); //add  tên form để javacheck
#+
#+ Xử lý javascript
$myform->addjavasrciptcode('');
$myform->checkjavascript();
?>
<html>
<head>
<title>Administrator Control panel</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<?=$load_header?>
<style type="text/css">
.templateTop{
	max-width: calc(100% - 25px);
	text-align: center;
	margin: 0 auto;
	font-size: 18px;
}
</style>
</head>
<body>
<? template_top(tt($fs_title))?>

<?
$fs_action = $_SERVER['SCRIPT_NAME'] . "?" . @$_SERVER['QUERY_STRING'];
?>
<?=$myform->create_form("submitForm", $fs_action, "post", "multipart/form-data","");?>
<div>
	<?=$myform->errorMsg($errorMsg)?>

	<div id="tabc_1" class="tabc">
		<?=$myform->create_table(8,0,"");?>
		<?=$myform->text_note()?>					
		<tr> 
			<td align="right" nowrap><?=tt("Nội dung")?>:</td>
			<td><textarea name="content" id="content" class="form_control" style="width:90%; height:200px;"><?=$content?></textarea></td>
		</tr>
		<script language="javascript">
			$(document).ready(function(){
				$('#getDate').click(function(){
					$('#strdate').val('<?=date("d/m/Y")?>');
					$('#strtime').val('<?=date("H:i:s")?>');
				})
			})
		</script>
		<?=$myform->text(0, "Ngày cập nhật", "strdate" . $myform->ec . "strtime", "strdate" . $myform->ec . "strtime", $strdate . $myform->ec . $strtime, "Ngày (dd/mm/yyyy)" . $myform->ec . "Giờ (hh/mm/ss)", "70" . $myform->ec . "70", $myform->ec, "10" . $myform->ec . "10", " - ", $myform->ec, "&nbsp; <input id='getDate' type='button' value='Get date' class='form_button'>");?>
		<?=$myform->checkbox(0, "Duyệt bình luận", "status", "status", "1", $status, " Duyệt ", "");?>
		<?=$myform->text(0 , "Họ và tên", "name", "name", $name, "Họ và tên", 400, "", 255, "", "", '')?>
		<?=$myform->text(0 , "Số điện thoại", "mobile", "mobile", $mobile, "Số điện thoại", 400, "", 255, "", "", '')?>
		<?=$myform->close_table();?>
	</div>
	<?=$myform->create_table(8,3,"");?>
	<?=$myform->radio(0 , "Sau khi lưu dữ liệu", "add_new" . $myform->ec . "return_listing" . $myform->ec . "return_edit", "after_save_data", $add . $myform->ec . $listing . $myform->ec . $edit, $after_save_data, "Thêm mới" . $myform->ec . "Quay về danh sách" . $myform->ec . "Sửa bản ghi", "");?>
	<?=$myform->button("button" . $myform->ec . "reset", "button" . $myform->ec . "reset", "button" . $myform->ec . "reset", "Cập nhật"  . $myform->ec .  "Làm lại", "", "");?>
	<?=$myform->hidden("action", "action", "submitForm", "");?>
	<?=$myform->close_table();?>
</div>
<?
$tab_select = getValue("tab_select","int","COOKIE",1);
?>
<?=$myform->close_form();?>
<? unset($myform);?>
<? template_bottom() ?>
<? /*------------------------------------------------------------------------------------------------*/ ?>
</body>
</html>