<?
// Mr.LeToan - NTP.VnJp
require_once("config_security.php");

$type = getValue("type","int","GET",1);
//Khai bao Bien
$fs_table		= "tbl_config";
$fs_redirect	= "config.php?type=" . $type;

$myform = new generate_form();	//Call Class generate_form();
$myform->removeHTML(0);	//Loại bỏ chức năng không cho điền tag html trong form

//array chua cac truong ko update
$arrayFieldNotUpdate 	= "'con_id','con_lang_id','con_mod_rewrite'";
$db_config				= new db_query("SELECT * FROM tbl_config WHERE con_lang_id = " . $lang_id);
$row					= mysql_fetch_array($db_config->result);

//array list update
$arrayField = array(
	// 1 ten truong, 2 kieu, 3 gia tri mac dinh, 4 chieu rong, 5 tieu de
	array("con_admin_email",0,1000,90,"Email quản trị","text"),	
	array("con_site_title",0,1000,90,"Tiêu đề trang","text"),
	array("con_meta_keywords",0,180,90,tt("Google keywords"),"textarea"),	
	array("con_meta_description",0,180,90,tt("Google description"),"textarea"),
	array("con_sim_km",0,180,90,tt("Sim khuyến mại trang chủ"),"textarea"),
	array("con_sim_blog",0,180,90,tt("Sim đăng blog"),"textarea"),
	array("con_static_simphongthuy_checklike",0,180,90,tt("Xem sim phong thủy quảng cáo"),"textarea")
);

//Insert to database
for($i=0;$i<count($arrayField);$i++){
	$myform->add($arrayField[$i][0],$arrayField[$i][0],$arrayField[$i][1],0,"",0,"",0,"");
}

$myform->addTable($fs_table);	//Add table

$errorMsg = "";	//Warning Error!
//Get Action.
$action	= getValue("action", "str", "POST", "");
if($action == "update"){
	//Check Error!
	$errorMsg .= $myform->checkdata();
	if($errorMsg == ""){
		$db_ex = new db_execute($myform->generate_update_SQL("con_lang_id",$_SESSION["lang_id"]));
		//echo $myform->generate_update_SQL("con_lang_id",$_SESSION["lang_id"]);
		//Redirect to:
		redirect($fs_redirect);
		exit();
	}
}

$myform->addFormname("setting"); //add  tên form để javacheck
$myform->evaluate(); // đổi tên trường thành biến và giá trị
$myform->addjavasrciptcode("");
$myform->checkjavascript();	//check java

//Select data
$db_data = new db_query("SELECT * FROM tbl_config WHERE con_lang_id = " . $_SESSION["lang_id"]);
if (mysql_num_rows($db_data->result) > 0)
{
	$row = mysql_fetch_array($db_data->result);
	$db_data->close();
	unset($db_data);
}
else{
	echo "Cannot find data";
	exit();
}
?>

<html>
<head>
<title>Portal tbl_config</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<?=$load_header?>
</head>
<body topmargin="0" bottommargin="0" leftmargin="0" rightmargin="0">
<? template_top(tt("Cấu hinh website"))?>
<div><h1><?=$errorMsg?></h1></div>
<form action="<?=$_SERVER['SCRIPT_NAME'] . "?" . @$_SERVER['QUERY_STRING']?>" method="post" name="setting" enctype="multipart/form-data">
<table border="0" cellpadding="0" cellspacing="0" width="100%">
	<tr>
		<td>
			<table border="1" cellpadding="8" cellspacing="0" width="100%" style="border-collapse:collapse" bordercolor="<?=$bordercolor?>">
				<?
				for($i = 0; $i < count($arrayField); $i++){
				?>
				<tr <?=$fs_change_bg?>>
					<td width="30%" nowrap="nowrap" align="right">
						<?=$arrayField[$i][4]?> : 
                    </td>
					<td>
						<?
						if($arrayField[$i][5]=="text"){?>
						<input type="text" size="<?=$arrayField[$i][3]?>" class="form" name="<?=$arrayField[$i][0]?>" id="<?=$arrayField[$i][0]?>" value="<?=$row[$arrayField[$i][0]];?>">
						<?
						}
						?>
						<?
						if($arrayField[$i][5]=="textarea"){?>
						<textarea class="form" name="<?=$arrayField[$i][0]?>" id="<?=$arrayField[$i][0]?>" rows="7" cols="<?=$arrayField[$i][3]?>" style="display: inline-block; vertical-align: middle;"><?=$row[$arrayField[$i][0]];?></textarea>
						<?
						}
						
						if($arrayField[$i][0] == 'con_sim_blog'){
						?>
							<a href="blog_html_code.php" target="_blank" style="display: inline-block; vertical-align: middle; font-weight: bold;">Lấy mã đăng Blog</a>
						<?
						}
						?>

					</td>
				</tr>
				<?
				}
				?>
			</table>
		</td>
	</tr>
	<tr>
		<td align="center" style="padding:7px">
			<input type="button" class="form_button" value="<?=tt("Cập nhật")?>" onClick="validateForm();">&nbsp;
			<input type="reset" class="form_button" value="<?=tt("Làm lại")?>">
			<input type="hidden" name="action" value="update">
		</td>
	</tr>	
</table>
</form>
<? template_bottom() ?>
</body>
</html>