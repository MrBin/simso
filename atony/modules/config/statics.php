<?
//
require_once("config_security.php");
require_once("configcombobox.php");

//
$type = getValue("type","int","GET",1);

//
$arrayStatic	= array(
	"con_static_footer_ststcom"=>tt("Footer")
	, "con_static_dktratruoc"=>tt("Đăng ký trả trước")
	, "con_static_contact"=>tt("Liên hệ")
	, "con_static_bangsim"=>tt("Bảng sim")
	, "con_static_baogia"=>tt("Báo giá sim thẻ")
	, "con_static_muabansim"=>tt("Mua bán sim số đẹp")
	, "con_static_simphongthuy"=>tt("Sim phong thủy giới thiệu")
	, "con_static_simphongthuy1"=>tt("Sim phong thủy tra cứu")
	, "con_static_choose"=>tt("Chọn sim phong thủy")
	, "con_static_simkm"=>tt("Sim khuyến mãi trang chủ")
	);
	
//Khai bao Bien
$fs_redirect	= "statics.php?type=" . $type;
//Call Class generate_form();
$myform = new generate_form();
//Loại bỏ chuc nang thay the Tag Html
$myform->removeHTML(0);
/*
1. data_field : Ten truong
2. data_value : Ten form
3. data_type : Kieu du lieu , 0 : string , 1 : kieu int, 2 : kieu email, 3 : kieu double
4. data_store : Noi luu giu data  0 : post, 1 : variable
5. data_default_value : gia tri mac dinh, neu require thi` phai lon hon hoac bang default
6. data_require : du lieu nay co can thiet hay khong
7. data_error_message : Loi dua ra man hinh
8. data_unique : Chỉ có duy nhất trong database
9. data_error_message2 : Loi dua ra man hinh neu co duplicate
*/
//Get all input name
$db_checkField = new db_query("SHOW COLUMNS FROM tbl_config");
$arrayField = array();
while($row=mysql_fetch_assoc($db_checkField->result)){
	$arrayField[$row["Field"]] = '';
}

foreach($arrayStatic as $key=>$value){
	if(!isset($arrayField[$key])){
		$db_ex = new db_execute("ALTER TABLE `tbl_config` ADD `" . $key . "` INT( 11 ) NULL DEFAULT '0'");
	}
	$$key = getValue("$key","int","POST",0);
}
//Insert to database
foreach($arrayStatic as $key=>$value){
	$myform->add("$key","$key",1,0,1,0,"",0,"");
}
//Add table
$myform->addTable($fs_table);
//Warning Error!
$errorMsg = "";
//Get Action.
$action	= getValue("action", "str", "POST", "");
if($action == "update"){
	//Check Error!
	$errorMsg .= $myform->checkdata();
	if($errorMsg == ""){
		$db_ex = new db_execute($myform->generate_update_SQL("con_lang_id",$_SESSION["lang_id"]));
		//echo $myform->generate_update_SQL("con_id",1);
		//Redirect to:
		redirect($fs_redirect);
		exit();
	}
}
//add form for javacheck
$myform->addFormname("setting");
//add more javacode
$myform->addjavasrciptcode("
						    		");
$myform->checkjavascript();
//Select data
$db_data = new db_query("SELECT * FROM tbl_config WHERE con_lang_id = " . $_SESSION["lang_id"]);
if (mysql_num_rows($db_data->result) > 0)
{
	$row = mysql_fetch_array($db_data->result);
	foreach($arrayStatic as $key=>$value){
		$$key = $row["$key"];
	}
	$db_data->close();
	unset($db_data);
}
else{
	echo "Cannot find data";
	exit();
}
?>
<html>
<head>
<title>Portal tbl_config</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<?=$load_header?>
</head>
<body topmargin="0" bottommargin="0" leftmargin="0" rightmargin="0">
<? template_top(tt("Cấu hình trang tĩnh"))?>
<div><h1><?=$errorMsg?></h1></div>
<form action="<?=getURL()?>" method="post" name="setting">
<table border="1" cellpadding="8" cellspacing="0" width="100%" style="border-collapse:collapse" bordercolor="<?=$bordercolor?>">
	<?
	//config static module
	$db_static = new db_query("SELECT sta_id,sta_title FROM tbl_statics,tbl_category WHERE sta_category=cat_id AND tbl_statics.lang_id = " . $_SESSION["lang_id"]  . $sqlcategory );
	if (mysql_num_rows($db_static->result) > 0) mysql_data_seek($db_static->result,0);
	//loop all static config
	$i=0;
	foreach($arrayStatic as $key=>$value){
	$i++;
	?>
	<tr <?=$fs_change_bg?>>
		<td width="30%" nowrap="nowrap">&nbsp;-&nbsp;<?=$value;?> : </td>
		<td>
			<?=get_config_combo($db_static->result,$key,$$key);?>
		</td>
	</tr>
	<?
	}
	$db_static->close();
	unset($db_static);
	?>
	<tr>
		<td>&nbsp;</td>
		<td height="30">
			<input type="button" class="form_button" value="<?=tt("Cập nhật")?>" onClick="validateForm();">&nbsp;
			<input type="reset" class="form_button" value="<?=tt("Làm lại")?>" >
			<input type="hidden" name="action" value="update">
		</td>
	</tr>
<? /*---------------------------------*/ ?>
</form>
</table>
<? template_bottom() ?>
</body>
</html>