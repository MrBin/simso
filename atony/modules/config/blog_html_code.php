<?
// Mr.LeToan - NTP.VnJp
require_once("config_security.php");
require_once("../../../kernel/classes/cls_template_desktop.php");

// Loai sim
$db_sim_type = new db_query("SELECT simtp_id,simtp_name,simtp_index
									FROM tbl_simtype
									WHERE simtp_active = 1
									ORDER BY simtp_order
									");
while($row = mysql_fetch_assoc($db_sim_type->result)) $loai_sim_idx[$row["simtp_index"]] = $row["simtp_id"];
unset($db_sim_type);

// Dau so
$db_sim_dauso = new db_query("SELECT sds_id,sds_category,sds_name,sds_name
										FROM tbl_simdauso
										WHERE sds_active = 1
										");
while($row = mysql_fetch_assoc($db_sim_dauso->result)){
	$index = intval($row['sds_name']);
	$dauso_sim_idx[$index] = array('sds_id'=>$row["sds_id"],
											'sds_category'=>$row["sds_category"]
											);
} // End while($ds = mysql_fetch_assoc($db_sim_dauso->result))
unset($db_sim_dauso);

ob_start('callback');
?>
<table cellpadding="4" cellspacing="1" border="1" border-color="#E5E5E5" style="width: 100%; border-collapse: collapse; border-color: #E5E5E5; border: none;">
	<thead style="background: #F2F2F2;">
		<tr>
			<th style="text-align: center;">Sim</th>
			<th style="text-align: center;">Giá</th>
			<th style="text-align: center;">Mạng</th>
			<th style="text-align: center;">Loại</th>
		</tr>
	</thead>
	<tbody>
		<?
		//$str = getStatic($con_static_simkm);
		$arrSimSale = array();
		if(trim($con_sim_blog) != ""){
			$e = explode( "\r\n", $con_sim_blog );

			// Xử lý dữ liệu sim đưa vào
			$i = 0;
			for ( ; $i < count( $e ) - 1; $i++ ){
				
				list( $sosim, $price, $price_sale ) = split( "\t", $e[$i] );
				
				if(trim($sosim) != ""){
				
					$sim1x = preg_replace('/[^0-9. ]/','',$sosim);
					$sim2x = intval(preg_replace('/[^0-9]/','',$sosim));
					
					// Xac dinh loai sim
					$sim_typeid		= checkSimType($sim2x, $loai_sim_idx);
					$query  = " SELECT simtp_id,simtp_name,simtp_index"
			   				." FROM tbl_simtype"
			   				." ORDER BY simtp_order, simtp_id DESC"
			   				;
		   		$arrSimType = getArray($query,'simtp_id');
		   		
		   		// Xac dinh mang sim va dau so
					$sim_category	= 1;
					$sim_dausoid	= 1;
					$dau1 = substr($sim2x,0,1);
					$dau2 = substr($sim2x,0,2);
					$dau3 = substr($sim2x,0,3);
					if(array_key_exists($dau3, $dauso_sim_idx)){
						$sim_dausoid	= $dauso_sim_idx[$dau3]['sds_id'];
						$sim_category	= $dauso_sim_idx[$dau3]['sds_category'];
					}elseif(array_key_exists($dau2, $dauso_sim_idx)){
						$sim_dausoid	= $dauso_sim_idx[$dau2]['sds_id'];
						$sim_category	= $dauso_sim_idx[$dau2]['sds_category'];
					}elseif(array_key_exists($dau1, $dauso_sim_idx)){
						$sim_dausoid	= $dauso_sim_idx[$dau1]['sds_id'];
						$sim_category	= $dauso_sim_idx[$dau1]['sds_category'];
					}  // End if(array_key_exists($dau, $dauso_sim_idx))
		   		
		   		// Category
		   		$query  = " SELECT cat_id,cat_name,cat_name_index,cat_picture"
			   				." FROM tbl_category"
			   				." WHERE cat_active = 1 AND cat_type = 'sim'"
			   				." ORDER BY cat_order, cat_id DESC"
				   			;
		   		$arrSimCat = getArray($query,'cat_id');
			 		
					// Kiểm tra hợp lệ của số
					$arrSimSale[$sim2x] 	 = array("sim_sim1" => $sim1x,
															"sim_sim2" => $sim2x, 
															"sim_price" => $price, 
															"sim_price_sale" => $price_sale,
															"sim_type" => $arrSimType[$sim_typeid]['simtp_name'],
															"sim_category" => $arrSimCat[$sim_category]["cat_picture"]
															);
														
				}// End if(trim($sosim) != "")
				
			}// End for ( ; $i < count( $e ) - 1; $i++ )
		}// End if(trim($con_sim_blog) != "")
		//echo '<xmp>'; print_r($con_sim_blog); echo '</xmp>';
		
		
		foreach($arrSimSale as $key => $row){
			$temp_type 				= checkSimType($key,$arrSimTypeIndex);
			$row["simtp_index"]	= @$arrSimType[$temp_type]["simtp_index"];
			$link_detail 			= createLink("detail_simtype",$row);
		?>
		<tr style="text-align: center;">
			<td><a style="color: #3276B1; font-size: 15px;" href="<?=$link_detail?>" title=""><b><?=@$row["sim_sim1"]?></b></a></td>
			<td style="color: #E04435; font-size: 15px;"><?=format_number(@$row["sim_price"])?></td>
			<td><img src="http://sieuthisimthe.com/store/media/category/<?=@$row["sim_category"]?>" /></td>
			<td><?=@$row["sim_type"]?></td>
		</tr>
		<?
		}
		?>
	</tbody>
</table>

<?
$ob_content = ob_get_contents();
?>
<style>
*{
	font-family: Arial;
	font-size: 14px;
}
body{
	margin: 0 auto;
	padding: 10px;
	width: 700px;
}
</style>
<br />
<b>HTML CODE:</b>
<textarea rows="20" style="width: 100%;"><?=$ob_content?></textarea>
<?
ob_end_flush();
?>