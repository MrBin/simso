<?
require_once("config_security.php");	//check security
$record_id	= getValue("record_id");
$iType		= getValue("type", "str", "GET", "");
$iParent 	= getValue("iParent","str","GET","");


$menu	= new menu();
$listAll		= $menu->getAllChild("tbl_menus", "mnu_id", "mnu_parent_id", 0, "mnu_type = '" . $iType . "' AND mnu_id <> " . $record_id . " AND lang_id = " . $_SESSION["lang_id"], "mnu_id,mnu_name,mnu_type", "mnu_order ASC,mnu_name ASC", "mnu_has_child", 0);
unset($class_menu);
?>
<select title="<?=tt("Menu cấp trên")?>" id="mnu_parent_id" name="mnu_parent_id" class="form_control">
	<option value="">--[<?=tt("Menu cấp trên")?>]--</option>
	<?
	for($i=0; $i<count($listAll); $i++){
		$selected = ($iParent == $listAll[$i]["mnu_id"]) ? ' selected="selected"' : '';
		echo '<option title="' . htmlspecialbo($listAll[$i]["mnu_name"]) . '" value="' . $listAll[$i]["mnu_id"] . '"' . $selected . '>';
		for($j=0; $j<$listAll[$i]["level"]; $j++) echo '---';
		echo '+ ' . cut_string($listAll[$i]["mnu_name"], 55) . '</option>';
	}
	?>
</select>



