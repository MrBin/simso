<?
#+
#+ Mr.LeToan - NTP.VnJp
require_once("config_security.php");	//check security	 
checkAddEdit("add"); //check quyền thêm sửa xóa

#+
#+ Khai bao bien
$add				= "add.php";
$listing			= "listing.php";
$edit				= "edit.php";
$after_save_data	= getValue("after_save_data", "str", "POST", $add);

$mnu_type		= getValue("mnu_type","str","GET", 0);
$mnu_parent_id 	= getValue("mnu_parent_id","str","GET","");

#+
#+ Tùy chỉnh biến
#+
if($mnu_name_index == ''){
	$mnu_name_index 	= removeTitle(getValue("mnu_name", "str", "POST", ""),'/');
	$mnu_name_index 	= strtolower($mnu_name_index);
} // End if($mnu_name_index == ''){

#+
#+ Lay ra menu cap con
$menu = new menu();
$listAll = $menu->getAllChild("tbl_menus","mnu_id","mnu_parent_id","0","mnu_type = " . $mnu_type . " AND lang_id = " . $_SESSION["lang_id"],"mnu_id,mnu_name,mnu_link,mnu_target,mnu_order,mnu_type,mnu_parent_id,mnu_has_child","mnu_order ASC, mnu_name ASC","mnu_has_child");

#+
#+ Goi class generate form
$myform = new generate_form();	//Call Class generate_form();
$myform->removeHTML(0);	//Loại bỏ chức năng không cho điền tag html trong form
#+
#+ Khai bao bang du lieu
$myform->addTable($fs_table);	// Add table
#+
#+ Khai bao thong tin cac truong
$myform->add("mnu_type","mnu_type",1,0,0,1,tt("Bạn chưa chọn loại trình đơn"),0,tt("Bạn chưa chọn loại trình đơn"));
$myform->add("mnu_name","mnu_name",0,0,"",1,tt("Bạn chưa nhập tên trình đơn"),0,tt("Bạn chưa nhập tên trình đơn"));
$myform->add("mnu_link","mnu_link",0,0,"",0,"",0,"");
$myform->add("mnu_check","mnu_check",0,0,"",0,"",0,"");
$myform->add("mnu_parent_id","mnu_parent_id",1,0,0,0,"",0,"");
$myform->add("mnu_target","mnu_target",0,0,"_self",1,tt("Bạn chưa nhập thực thi khi ấn vào trình đơn"),0,tt("Bạn chưa nhập thực thi khi ấn vào trình đơn"));
$myform->add("mnu_order","mnu_order",1,0,0,1,tt("Bạn chưa thiết lập vị trí ưu tiên"),0,tt("Bạn chưa thiết lập vị trí ưu tiên"));
	//$myform->add("mnu_teaser","mnu_teaser",0,0,"",0,"",0,"");

$errorMsg = "";	//Warning Error!
//
$action	= getValue("action", "str", "POST", "");	//Get Action.
if($action == "insert"){
	//
	$upload_pic = new upload("picture", $fs_filepath, $extension_list, $limit_size);
	if ($upload_pic->file_name != ""){
		$picture = $upload_pic->file_name;
		$myform->add("mnu_picture","picture",0,1,"",0,"",0,"");
	}
	
	//
	$errorMsg .= $upload_pic->show_warning_error();	//Check Error!
	$errorMsg .= $myform->checkdata();
	$errorMsg .= $myform->strErrorFeld ;	//Check Error!
	if($errorMsg == ""){
		//echo $myform->generate_insert_SQL();exit();
        $db_ex	 		= new db_execute_return();
		$last_id 		= $db_ex->db_execute($myform->generate_insert_SQL());
		$record_id 		= $last_id;
		
		$mnu_type	= getValue("mnu_type","int","POST","");	
		$mnu_parent_id	= getValue("mnu_parent_id","str","POST","");		
		$fs_redirect = $after_save_data. "?record_id=" . $record_id . "&mnu_type=" . $mnu_type . "&mnu_parent_id=" . $mnu_parent_id;
		redirect($fs_redirect);
		exit();
	}
}
//
$myform->addFormname("add_new"); //add  tên form để javacheck
$myform->evaluate(); // đổi tên trường thành biến và giá trị
$myform->addjavasrciptcode("");
$myform->checkjavascript();	//check java
?>


<html>
<head>
<title>Channel Add</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<?=$load_header?>
</head>

<body>
<? /*------------------------------------------------------------------------------------------------*/ ?>
<? template_top(tt("Thêm mới bản ghi"))?>
<?
$fs_action			= $_SERVER['SCRIPT_NAME'] . "?" . @$_SERVER['QUERY_STRING'];
$onChange			= "load_data('load.php?type=' + this.value, 'content_loader')";
$form = new form();
?>
<?=$form->create_form("add_new", $fs_action, "post", "multipart/form-data");?>
<?=$form->create_table(8,3,"");?>
<?=$form->text_note('Những ô có dấu sao (<font class="form_asterisk">*</font>) là bắt buộc phải nhập.')?>
<?=$form->errorMsg($errorMsg)?>
<?=$form->select(1 , "Loại trình đơn", "mnu_type", "mnu_type", $arrType, $mnu_type, "Loại trình đơn", 1, 0, 'onChange="' . $onChange . '"', "")?>
<?=$form->text(1 , "Tên trình đơn", "mnu_name", "mnu_name", $mnu_name, "Tên trình đơn", 250, "", 255, "", "", "")?>
<?=$form->text(1 , "Tiêu đề trình đơn", "mnu_name_index", "mnu_name_index", $mnu_name_index, "Tiêu đề trình đơn", 250, "", 255, "", "", "")?>
<?=$form->text(1 , "Liên kết", "mnu_link", "mnu_link", $mnu_link, "Liên kết", 350, "", 255, "", "", '<img title="Tạo link cho menu" align="absmiddle" src="' . $fs_imagepath . 'edit.png" style="cursor:pointer; margin-left:5px" onClick="creat_link(\'mnu_link\')">')?>
<?=$myform->getFile(0,"Ảnh minh họa", "picture", "picture" , 32, "", '<br />(<i>Dung lượng tối đa <font color="#993300">' . number_format($limit_size,0,'','.') . ' Kb</font>)</i>')?>
<? if($checkme){?>
<?=$form->text(0, "Menu defined", "mnu_check", "mnu_check", $mnu_check, "Menu defined", 250, "", 255, "", "", "")?>
<? }?>
<tr>
	<td class="form_name"><?=tt("Menu cấp trên")?> : </td>
	<td class="form_text">
		<div id="content_loader">
			<select title="<?=tt("Menu cấp trên")?>" id="mnu_parent_id" name="mnu_parent_id" class="form_control">
				<option value="">--[<?=tt("Menu cấp trên")?>]--</option>
				<?
				for($i=0; $i<count($listAll); $i++){
					$selected = ($mnu_parent_id == $listAll[$i]["mnu_id"]) ? ' selected="selected"' : '';
					echo '<option title="' . htmlspecialbo($listAll[$i]["mnu_name"]) . '" value="' . $listAll[$i]["mnu_id"] . '"' . $selected . '>';
					for($j=0; $j<$listAll[$i]["level"]; $j++) echo '---';
					echo '+ ' . cut_string($listAll[$i]["mnu_name"], 55) . '</option>';
				}
				?>
			</select>
		</div>
	</td>
</tr>
<?=$form->select(0, "Mở ra", "mnu_target", "mnu_target", $arrTarget, $mnu_target, "Mở ra", 1, 0, "", "")?>
<?=$form->text(0 , "Thứ tự", "mnu_order", "mnu_order", $mnu_order, "Thứ tự", 50, "", 5, "", "", "")?>
<?=$myform->radio(0 , "Sau khi lưu dữ liệu", "add_new" . $myform->ec . "return_listing" . $myform->ec . "return_edit", "after_save_data", $add . $myform->ec . $listing . $myform->ec . $edit, $after_save_data, "Thêm mới" . $myform->ec . "Quay về danh sách" . $myform->ec . "Sửa bản ghi", "");?>
<?=$form->button("button" . $form->ec . "reset", "button" . $form->ec . "reset", "button" . $form->ec . "reset", "Cập nhật"  . $form->ec .  "Làm lại", $after_save_data, "");?>
<?=$form->hidden("action", "action", "insert", "");?>
<?=$form->close_table();?>
<?=$form->close_form();?>
<? unset($form);?>
<? template_bottom() ?>
<? /*------------------------------------------------------------------------------------------------*/ ?>
</body>
</html>