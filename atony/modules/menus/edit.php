<?
// Mr.LeToan - NTP.VnJp
require_once("config_security.php");
checkAddEdit("edit");	// Check quyền thêm sửa xóa
//
$record_id		= getValue("record_id","int","GET");
//
$position 			= getValue("position");
$redirect			= getValue("returnurl", "str", "GET", base64_encode("listing.php?position=" . $position));
$add				= base64_encode("add.php");
$listing			= $redirect;
$after_save_data	= getValue("after_save_data", "str", "POST", $redirect);
$fs_redirect		= base64_decode($after_save_data);

//
checkRowUser($fs_table,$field_id,$record_id,$fs_redirect);
//
$myform = new generate_form();	//Call Class generate_form();
$myform->removeHTML(0);	//Loại bỏ chức năng không cho điền tag html trong form
//
$myform->add("mnu_type","mnu_type",1,0,0,1,tt("Bạn chưa chọn loại trình đơn"),0,tt("Bạn chưa chọn loại trình đơn"));
$myform->add("mnu_name","mnu_name",0,0,"",1,tt("Bạn chưa nhập tên trình đơn"),0,tt("Bạn chưa nhập tên trình đơn"));
$myform->add("mnu_link","mnu_link",0,0,"",0,"Bạn chưa nhập địa chỉ liên kết !",0,"Bạn chưa nhập địa chỉ liên kết");
if($checkme){
$myform->add("mnu_check","mnu_check",0,0,"",0,"",0,"");
}
$myform->add("mnu_parent_id","mnu_parent_id",1,0,0,0,"",0,"");
$myform->add("mnu_target","mnu_target",0,0,"_self",1,tt("Bạn chưa nhập thực thi khi ấn vào trình đơn"),0,tt("Bạn chưa nhập thực thi khi ấn vào trình đơn"));
$myform->add("mnu_order","mnu_order",1,0,0,1,tt("Bạn chưa thiết lập vị trí ưu tiên"),0,tt("Bạn chưa thiết lập vị trí ưu tiên"));
$myform->add("mnu_padding_left","mnu_padding_left",1,0,0,0,"",0,"");
//
$myform->addTable($fs_table);
//
$errorMsg = "";	//Warning Error!
$action	= getValue("action", "str", "POST", "");	//Get Action.
if($action == "update"){
	$upload_pic = new upload("picture", $fs_filepath, $extension_list, $limit_size);
	if ($upload_pic->file_name != ""){
		$picture = $upload_pic->file_name;
		$myform->add("mnu_picture","picture",0,1,"",0,"",0,"");
	}
	//Delete picture
	if ($upload_pic->file_name != ""){
		//Delete file
		delete_file($fs_table,"mnu_id",$record_id,"mnu_picture",$fs_filepath);
		//Permision file
	}
	
	//Check Error!
	$errorMsg .= $upload_pic->show_warning_error();	//Check Error!
	$errorMsg .= $myform->checkdata();
	$errorMsg .= $myform->strErrorFeld ;	//Check Error!
	if($errorMsg == ""){
		$db_ex = new db_execute($myform->generate_update_SQL("mnu_id", $record_id));
		//echo $myform->generate_update_SQL("new_id", $record_id);exit();
		redirect($fs_redirect);
		exit();
	}
}
//
$myform->addFormname("update"); //add  tên form để javacheck
$myform->evaluate(); // đổi tên trường thành biến và giá trị
$myform->addjavasrciptcode("");
$myform->checkjavascript();	//check java

//
$iType = getValue("position", "int", "GET", 0);
//
$menu = new menu();
$listAll = $menu->getAllChild("tbl_menus","mnu_id","mnu_parent_id","0","mnu_id <> " . $record_id . " AND mnu_type = " . $iType . " AND lang_id = " . $_SESSION["lang_id"],"mnu_id,mnu_name,mnu_link,mnu_target,mnu_order,mnu_type,mnu_parent_id,mnu_has_child,mnu_check","mnu_order ASC, mnu_name ASC","mnu_has_child",0);
//
$db_data = new db_query("SELECT *
						 FROM tbl_menus
						 WHERE mnu_id = " . $record_id);
if (mysql_num_rows($db_data->result) > 0)
{
	$row = mysql_fetch_array($db_data->result);
	$db_data->close();
	unset($db_data);
}
else{
	echo "Cannot find data";
	exit();
}
?>
<html>
<head>
<title>Channel Edit</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<?=$load_header?>
<script type="text/javascript">
$(document).ready(function(){
	$('#content_loader').empty();
	$('#content_loader').load('load.php?type=<?=$row['mnu_type']?>');	
})
</script>
</head>
<body>
<? /*------------------------------------------------------------------------------------------------*/ ?>
<? template_top(tt("Chỉnh sửa trình đơn"))?>
<?
$fs_action			= $_SERVER['SCRIPT_NAME'] . "?" . @$_SERVER['QUERY_STRING'];
$onChange			= "load_data('load.php?type=' + this.value, 'content_loader')";
$form = new form();
?>
<?=$form->create_form("update", $fs_action, "post", "multipart/form-data");?>
<?=$form->create_table(8,3,"");?>
<?=$form->text_note('Những ô có dấu sao (<font class="form_asterisk">*</font>) là bắt buộc phải nhập.')?>
<?=$form->errorMsg($errorMsg)?>
<?=$form->select(1 , "Loại trình đơn", "mnu_type", "mnu_type", $arrType, $row["mnu_type"], "Loại trình đơn", 1, 0, 'onChange="' . $onChange . '"', "")?>
<?=$form->text(1 , "Tên trình đơn", "mnu_name", "mnu_name", $row["mnu_name"], "Tên trình đơn", 250, "", 255, "", "", "")?>
<?=$form->text(1 , "Tiêu đề trình đơn", "mnu_name_title", "mnu_name_title", $row["mnu_name_title"], "Tiêu đề trình đơn", 250, "", 255, "", "", "")?>
<?=$form->text(1 , "Liên kết", "mnu_link", "mnu_link", $row["mnu_link"], "Liên kết", 350, "", 255, "", "", '<img title="Tạo link cho menu" align="absmiddle" src="' . $fs_imagepath . 'edit.png" style="cursor:pointer; margin-left:5px" onClick="creat_link(\'mnu_link\')">')?>
<?=$myform->getFile(0,"Ảnh minh họa", "picture", "picture" , 32, "", '<br />(<i>Dung lượng tối đa <font color="#993300">' . number_format($limit_size,0,'','.') . ' Kb</font>)</i>')?>
<? if($checkme){?>
<?=$form->text(0, "Menu defined", "mnu_check", "mnu_check", $row["mnu_check"], "Menu defined", 250, "", 255, "", "", "")?>
<? }?>
<tr>
	<td class="form_name"><?=tt("Menu cấp trên")?> : </td>
	<td class="form_text">
		<div id="content_loader"></div>
	</td>
</tr>
<?=$form->select(0, "Mở ra", "mnu_target", "mnu_target", $arrTarget, $row["mnu_target"], "Mở ra", 1, 0, "", "")?>
<?=$form->text(0 , "Căn trái", "mnu_padding_left", "mnu_padding_left", $row["mnu_padding_left"], "Căn trái", 50, "", 5, "", "", " pixel")?>
<?=$form->text(0 , "Thứ tự", "mnu_order", "mnu_order", $row["mnu_order"], "Thứ tự", 50, "", 5, "", "", "")?>
<?=$form->radio(0 , "Sau khi lưu dữ liệu", "add_new" . $form->ec . "return_listing", "after_save_data", $add . $form->ec . $listing, $after_save_data, "Thêm mới" . $form->ec . "Quay về danh sách", "");?>
<?=$form->button("button" . $form->ec . "reset", "button" . $form->ec . "reset", "button" . $form->ec . "reset", "Cập nhật"  . $form->ec .  "Làm lại", $after_save_data, "");?>
<?=$form->hidden("action", "action", "update", "");?>
<?=$form->close_table();?>
<?=$form->close_form();?>
<? unset($form);?>
<? template_bottom() ?>
<? /*------------------------------------------------------------------------------------------------*/ ?>
</body>
</html>