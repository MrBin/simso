<?
#+
require_once("../../resource/security/security.php");

#+
#+ Khai bao bien
$module_id = 6;		// module id trong field modules

#+
#+ kiem tra dang nhap
checkloggedin();
if (checkaccess($module_id) !=1){
	redirect("../deny.htm");
	exit();
}

#+
#+ Khai bao thong tin table, field id va name
$fs_table                = "tbl_menus"; 
$field_id                = "mnu_id"; 
$field_name				 = "mnu_name";

#+
$fs_filepath		= $root_path_admin.$path_menu;
$extension_list 	= "jpg,gif,png,swf";
$limit_size			= 300000;
#+
$small_width		= 	125;
$small_heght		=	97;
$small_quantity		=	100;
#+
$medium_width		= 	250;
$medium_heght		=	100;
$medium_quantity	=	90;

#+
$arrType = array(
	1 => tt("Menu Top")			
	,2 => tt("Menu Dịch vụ")	
	,3 => tt("Menu Liên kết")		
	);

#+
$arrTarget = array(
	"_self" => tt("Hiện hành")
	, "_blank" => tt("Trang mới"));							
?>