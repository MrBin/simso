<?
require_once("config_security.php");
//
$sort			= getValue("sort");
switch($sort){
	case 1: $sqlOrderBy = "mnu_type , mnu_order ASC"; break;
	case 2: $sqlOrderBy = "mnu_type , mnu_order DESC"; break;
	case 3: $sqlOrderBy = "mnu_type , mnu_name ASC"; break;
	case 4: $sqlOrderBy = "mnu_type , mnu_name DESC"; break;
	default:$sqlOrderBy = "mnu_type , mnu_order ASC"; break;
}
//
$iType	= getValue("position","int","GET",0);
$sql 		= '';
if($iType != 0){ $sql = " AND mnu_type = " . $iType ;}

$menu		= new menu();
$listAll	= $menu->getAllChild("tbl_menus","mnu_id","mnu_parent_id","0"," lang_id = " . $_SESSION["lang_id"] . $sql," mnu_id,mnu_name,mnu_link,mnu_target,mnu_check,mnu_order,mnu_type,mnu_active,mnu_parent_id,mnu_has_child,mnu_picture, mnu_follow",$sqlOrderBy,"mnu_has_child");
$total_record	= count($listAll);
?>

<html>
<head>
<title>Channel Listing</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<?=$load_header?>
</head>

<body>

<!-- Load css và xử lý dữ liệu của jquery(kích hoạt, xóa bản ghi) -->
<div id="loadingBar">
    Đang xử lý ... Vui lòng đợi trong giây lát ... <br>
    <img align="absmiddle" src="../../resource/images/loading.gif">
</div>
<input class="controller" type="hidden" value="controllers" />

<? template_top(tt("Danh sách bản ghi") . "&nbsp;[" . number_format($total_record,0,".",",") . "]" )?>
<? //*/?>
<div align="center">
	<table cellpadding="8">
		<tr>
			<td class="textBold"><?=tt("Loại trình đơn")?></td>
			<td>
			<select name="mnu_type" class="form" onChange="window.location.href='listing.php?position='+this.value">
				<option value=""><?=tt("Loại trình đơn")?></option>
				<?

				foreach($arrType as $key => $value){
					if($key == $iType){
						echo "<option value='" . $key . "' selected>" . $value . "</option>";
					}
					else{
						echo "<option value='" . $key . "'>" . $value ."</option>";
					}
				?>
				<? } ?>
			</select>
			</td>
		</tr>
	</table>			
</div>
<? //*/?>

<form action="quickedit.php?returnurl=<?=base64_encode($_SERVER['REQUEST_URI'])?>" method="post" name="form_listing" id="form_listing" enctype="multipart/form-data">
<table border="1" cellpadding="0" cellspacing="0" width="100%" style="border-collapse:collapse" bordercolor="<?=$bordercolor?>">
	<tr align="center" class="textBold"> 
		<td nowrap="nowrap" width="25"><input type="checkbox" id="check_all" onClick="check('1','1000')"></td>
		<td nowrap="nowrap" width="40"><?=tt("Lưu")?></td>
		<td nowrap="nowrap"><?=tt("Loại menu")?></td>
		<td>
			<div><?=tt("Thứ tự")?> - <?=tt("Tên menu")?></div>
			<div>
				<?=generate_sort("asc", 1, $sort, $fs_imagepath)?>
				<?=generate_sort("desc", 2, $sort, $fs_imagepath)?>
				&nbsp;
				<?=generate_sort("asc", 3, $sort, $fs_imagepath)?>
				<?=generate_sort("desc", 4, $sort, $fs_imagepath)?>
				&nbsp; &nbsp;
			</div>
		</td>
		<td nowrap="nowrap"><?=tt("Liên kết")?></td>
		<td nowrap="nowrap"><?=tt("Mở ra")?></td>
        <? if($checkme){?>
		<td nowrap="nowrap"><?=tt("Menu defined")?></td>
        <? }?>
        <td nowrap="nowrap" width="40"><?=tt("Follow")?></td>
		<td nowrap="nowrap" width="40"><?=tt("Kích hoạt")?></td>
		<td nowrap="nowrap" width="40"><?=tt("Sao chép")?></td>
		<td nowrap="nowrap" width="40"><?=tt("Sửa")?></td>
		<td nowrap="nowrap" width="40"><?=tt("Xóa")?></td>
	</tr>
	<? 
	
	$iType ='';
	for($i=0;$i<count($listAll);$i++){ $countno = $i;?>
	<?
	/*/
	if($iType != $listAll[$i]["mnu_type"]){
		$iType = $listAll[$i]["mnu_type"];
	?>
		<tr>
			<td colspan="11" bgcolor="<?=$bgcolor?>" align="center" class="textBold">
			<?=$arrType[$listAll[$i]["mnu_type"]]?>
			<? //isset($category_type) ? $category_type : $category_type?>
		</td>
		</tr>
	<?
	}
	//*/
	?>
	<tr <?=$fs_change_bg?> class="row-<?=$listAll[$i]["mnu_id"]?> item-row" align="center">
		<td>
			<input title="<?=tt("Tích vào giá trị muốn thay đổi")?>" type="checkbox" id="record_<?=$listAll[$i]["mnu_id"]?>_<?=$countno?>" name="record_id[]" value="<?=$listAll[$i]["mnu_id"]?>">
			<input type="hidden" name="iQuick" value="update">
		</td>
		<td>
			<img title="<?=tt("Lưu bản ghi")?>" alt="<?=tt("Lưu")?>" src="<?=$fs_imagepath?>save.png" border="0" style="cursor:pointer" onClick="submitAll('form_listing',<?=$listAll[$i]["mnu_id"]?>,<?=$countno?>)">
		</td>	
		<td>
			<select name="mnu_type<?=$listAll[$i]['mnu_id'];?>" id="mnu_type<?=$listAll[$i]['mnu_id'];?>" class="form" onChange="changeCheck(<?=$listAll[$i]["mnu_id"]?>,<?=$countno?>)">
			<?
			$pos = $listAll[$i]["mnu_type"];
			foreach($arrType as $key => $value){
				if($key == $pos){
					echo "<option value='" . $key . "' selected>" . $value . "</option>";
				}
				else{
					echo "<option value='" . $key . "'>" . $value . "</option>";
				}
			}
			?>
			</select>
		</td>
		
		<td nowrap="nowrap" align="left" style="padding-left:10px;">
			<? for($j=0;$j<$listAll[$i]["level"];$j++) echo '&nbsp;---&nbsp;';?>	
			<input title="<?=tt("Thứ tự")?>" type="text" id="mnu_order<?=$listAll[$i]['mnu_id'];?>" name="mnu_order<?=$listAll[$i]['mnu_id'];?>" value="<?=$listAll[$i]['mnu_order']?>" class="form" style="width:20px;<? if($listAll[$i]['level'] == 0) echo 'font-weight:bold';?>" onKeyUp="changeCheck(<?=$listAll[$i]["mnu_id"]?>,<?=$countno?>)">
			<input title="<?=tt("Tên danh mục")?>" type="text" id="mnu_name<?=$listAll[$i]['mnu_id'];?>"  name="mnu_name<?=$listAll[$i]['mnu_id'];?>" value="<?=$listAll[$i]['mnu_name'];?>" class="form" style="width:150px;<? if($listAll[$i]['level'] == 0) echo 'font-weight:bold';?>" onKeyUp="changeCheck(<?=$listAll[$i]["mnu_id"]?>,<?=$countno?>)">
		</td>
		<td nowrap="nowrap">
			<input title="Liên kết" type="text" id="mnu_link_<?=$countno?>" name="mnu_link<?=$listAll[$i]["mnu_id"]?>" value="<?=htmlspecialbo($listAll[$i]["mnu_link"])?>" class="form_control" style="width:150px<? if($listAll[$i]["level"] == 0) echo '; font-weight:bold';?>" onKeyUp="changeCheck(<?=$listAll[$i]["mnu_id"]?>,<?=$countno?>)">
			<img title="Tạo link cho menu" align="absmiddle" src="<?=$fs_imagepath?>edit.png" style="cursor:pointer" onClick="creat_link('mnu_link_<?=$countno?>')">
		</td>
		<td>	
			<select id="mnu_target_<?=$countno?>" name="mnu_target<?=$listAll[$i]["mnu_id"]?>" class="form_control" onChange="changeCheck(<?=$listAll[$i]["mnu_id"]?>,<?=$countno?>)">
				<?
				reset($arrTarget);
				foreach($arrTarget as $key => $value){
				?>
					<option value="<?=$key?>"<? if($key == $listAll[$i]["mnu_target"]){echo ' selected="selected"';}?>><?=$value?></option>
				<?
				}
				?>
			</select>
		</td>
        <? if($checkme){?>
		<td align="center">
			<input title="<?=tt("Menu defined")?>" type="text" id="mnu_check_<?=$countno?>" name="mnu_check<?=$listAll[$i]["mnu_id"]?>" value="<?=htmlspecialbo($listAll[$i]["mnu_check"])?>" class="form_control" style="width:120px<? if($listAll[$i]["level"] == 0) echo '; font-weight:bold';?>" onKeyUp="changeCheck(<?=$listAll[$i]["mnu_id"]?>,<?=$countno?>)">
		</td>
        <? }?>
        <td align="center"><img class="follow" name="<?=$listAll[$i]["mnu_follow"];?>" id="<?=$listAll[$i]["mnu_id"];?>" path="<?=$fs_imagepath?>" src="<?=$fs_imagepath?>active_<?=$listAll[$i]["mnu_follow"];?>.png" title="<?=tt("Follow bản ghi")?>" alt="<?=tt("Kích hoạt")?>" /></td>
		<td align="center"><img class="active" name="<?=$listAll[$i]["mnu_active"];?>" id="<?=$listAll[$i]["mnu_id"];?>" path="<?=$fs_imagepath?>" src="<?=$fs_imagepath?>active_<?=$listAll[$i]["mnu_active"];?>.png" title="<?=tt("Kích hoạt bản ghi")?>" alt="<?=tt("Kích hoạt")?>" /></td>
		<td align="center"><img title="<?=tt("Sao chép bản ghi")?>" alt="<?=tt("Sao chép")?>" src="<?=$fs_imagepath?>copy.png" onClick="if (confirm('<?=tt("Bạn muốn sao chép bản ghi này?")?>')){ window.location.href='copy.php?record_id=<?=$listAll[$i]["mnu_id"]?>&returnurl=<?=base64_encode(getURL())?>'}" border="0" style="cursor:pointer"></td>
		<td align="center"><img title="<?=tt("Sửa bản ghi")?>" alt="<?=tt("Sửa")?>" src="<?=$fs_imagepath?>edit.png" onClick="window.location.href='edit.php?record_id=<?=$listAll[$i]["mnu_id"]?>&returnurl=<?=base64_encode(getURL())?>'" border="0" style="cursor:pointer"></td>
        <td align="center">
            <img class="remove" id="<?=$listAll[$i]["mnu_id"];?>" src="<?=$fs_imagepath?>delete.png" title="<?=tt("Xóa bản ghi")?>" alt="<?=tt("Xóa")?>" />
        </td>
	</tr>
	<? } ?>
</table>
</form>
<? template_bottom()?>	
</body>
</html>