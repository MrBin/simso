<?
require_once("config_security.php");	//check security	 

//Khai bao Bien
$errorMsg 		= "";
$iQuick 			= getValue("iQuick","str","POST","");
$returnurl 		= base64_decode(getValue("returnurl","str","GET",base64_encode("listing.php")));
$record_id		= getValue("record_id", "arr", "POST", "");

//check quyền them sua xoa
checkAddEdit("edit");

//
if ($iQuick == 'update'){
	if($record_id != ""){
		for($i=0; $i<count($record_id); $i++){
			
			// Kiem tra quyen user voi ban ghi
			checkRowUser($fs_table,$field_id,$record_id[$i],$returnurl);

			$pri_name					= getValue("pri_name_" . $record_id[$i], "str", "POST", "");
			$pri_name_check			= getValue("pri_name_check_" . $record_id[$i], "str", "POST", "");
			$pri_name_index 			= getValue("pri_name_index_" . $record_id[$i], "str", "POST", "");
			$pri_name_index_check 	= getValue("pri_name_index_check_" . $record_id[$i], "str", "POST", "");
			$require = ($pri_name_index != "" && $pri_name_index != $pri_name_index_check) ? 1 : 0;
			if($pri_name_index == '') $pri_name_index = removeTitle($pri_name,'/');

			// Call Class generate_form();
			$myform = new generate_form();
			$myform->removeHTML(0);
			$myform->addTable($fs_table);
			$myform->add("pri_name","pri_name",0,1,"",0,"",0,"");
			$myform->add("pri_name_index","pri_name_index",0,1,"",0,"",$require,"Khoảng giá nhập đã tồn tại");
			$myform->add("pri_min","pri_min_" . $record_id[$i],3,0,0,0,"",0,"");
			$myform->add("pri_max","pri_max_" . $record_id[$i],3,0,0,0,"",0,"");
			$myform->add("pri_order","pri_order_" . $record_id[$i],1,0,0,0,"",0,"");
			$myform->add("pri_currency","pri_currency_" . $record_id[$i],1,0,0,0,"",0,"");

			$errorMsg .= $myform->checkdata();
			if($errorMsg == ""){
				$db_ex = new db_execute($myform->generate_update_SQL("pri_id",$record_id[$i]));
				//echo $myform->generate_update_SQL("con_id",$record_id[$i]);exit();
			}
			
		} // End for($i=0; $i<count($record_id); $i++)
		
	} // End if($record_id != "")
	
	redirect($returnurl);
} // End if ($iQuick == 'update')
?>