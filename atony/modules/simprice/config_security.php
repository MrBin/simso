<?
require_once("../../resource/security/security.php");

$module_id			 	= 16;

checkloggedin();
if (checkaccess($module_id) != 1){
	redirect("../../deny.htm");
	exit();
}

$fs_table				= "tbl_simprice";
$field_id				= "pri_id";
$field_name				= "pri_name";
?>