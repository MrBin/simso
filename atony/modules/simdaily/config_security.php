<?
require_once("../../resource/security/security.php");

$module_id			 	= 15;

checkloggedin();
if (checkaccess($module_id) != 1){
	redirect("../deny.htm");
	exit();
}

$fs_table			= "tbl_simdaily";
$field_id			= "simch_id";
$field_name			= "simch_name";

$fs_filepath		= "";
?>