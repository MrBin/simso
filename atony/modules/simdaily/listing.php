<?php
require_once("config_security.php");

// Khai bao bien
$limit     	 	= getValue("limit", "int", "GET");
$keyword			= getValue("keyword", "str", "GET", "", 1);

$sql = "";
if($keyword != "") $sql .= " AND (simch_name LIKE '%" . $keyword . "%')";

$db_sim_daily = new db_query("SELECT * 
			FROM " . $fs_table ." 
			WHERE lang_id=" . $_SESSION["lang_id"] . $sql ." 
			ORDER BY simch_order DESC");
?>

<html>
<head>
<title>Channel Listing</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<?=$load_header?>
</head>

<body>
<!-- Load css và xử lý dữ liệu của jquery(kích hoạt, xóa bản ghi) -->
<div id="loadingBar">
    Đang xử lý ... Vui lòng đợi trong giây lát ... <br>
    <img align="absmiddle" src="../../resource/images/loading.gif">
</div>
<input class="controller" type="hidden" value="controllers" />

<? template_top(tt("Danh sách bản ghi"));?>

<div style="position: absolute; top: 0; right: 5px;">
	<form action="listing.php" method="get" name="timkhohang">
	<table border="0" cellpadding="5" align="center">
		<tr>
	        <td><input type="text" class="form" name="keyword" value="<?=$keyword?>" style="width:120px" /></td>       
			<td><input type="submit" value="<?=tt("Tìm kiếm")?>" class="form_button" /></td>
		</tr>
	</table>
	</form>
</div>

<form action="quickedit.php?returnurl=<?=base64_encode(getURL())?>" method="post" name="form_listing" enctype="multipart/form-data">
	<input type="hidden" name="iQuick" value="update">
	<table border="1" cellpadding="8" cellspacing="0" width="100%" style="border-collapse:collapse" bordercolor="<?=$bordercolor?>">
		<tr align="center" height="40" class="textBold">
			<td width="2%">&nbsp;</td>
			<td class="textBold" width="50"><?=tt("Lưu")?></td>
			<td><?=tt("Thứ tự")?> - <?=tt("Tên")?></td>
			<td><?=tt("Tên Viết tắt")?> - <?=tt("Tên hiển thị")?></td>
			<td width="5%"><?=tt("Xóa sim")?></td>
			<td width="5%"><?=tt("Show")?></td>
			<td width="5%"><?=tt("Active")?></td>
			<td width="5%"><?=tt("Copy")?></td>
			<td width="5%"><?=tt("Sửa")?></td>
			<td width="5%"><?=tt("Xóa")?></td>
		</tr>
        
		<?
		$countno = 0;
		while($row = mysql_fetch_assoc($db_sim_daily->result)){
			$countno++;
			$simch_active 	= ($row['simch_active'] == 1) ? 'active_1.png' : 'active_0.png';
			$simch_show 	= ($row['simch_show'] == 1) ? 'active_1.png' : 'active_0.png';
		?>
		<tr <?=$fs_change_bg?> class="row-<?=$row['simch_id']?> item-row" align="center">
			<td><input type="checkbox" name="record_id[]" id="record_id_<?=$countno?>" value="<?=$row['simch_id']?>"></td>
			<td><img src="<?=$fs_imagepath?>save.png" border="0" style="cursor:pointer" onClick=" document.getElementById('record_id_<?=$countno;?>').checked=true ; document.form_listing.submit();" alt="Save"></td>
			<td align="left">
				<input name="simch_order<?=$row['simch_id']?>" value="<?=$row['simch_order']?>" onKeyUp="document.getElementById('record_id_<?=$countno?>').checked=true" style="width:30px;" type="text" class="form_control" />
				<input name="simch_name<?=$row['simch_id']?>" value="<?=$row['simch_name']?>" onKeyUp="document.getElementById('record_id_<?=$countno?>').checked=true" type="text" style="width:150px;" class="form_control" />
			</td>
			<td align="left">
				<input name="simch_viettat<?=$row['simch_id']?>" value="<?=$row['simch_viettat']?>" onKeyUp="document.getElementById('record_id_<?=$countno?>').checked=true" type="text" style="width:150px;" class="form_control" />
				<input name="simch_hienthi<?=$row['simch_id']?>" value="<?=$row['simch_hienthi']?>" onKeyUp="document.getElementById('record_id_<?=$countno?>').checked=true" type="text" style="width:150px;" class="form_control" />
			</td>
			<td><a href="deletesim.php?record_id=<?=$row['simch_id'];?>&url=<?=base64_encode(getURL())?>"><img src="<?=$fs_imagepath?>delete.png" /></a></td>
			<td><img class="show" name="<?=$row['simch_show']?>" id="<?=$row['simch_id']?>" path="<?=$fs_imagepath?>" src="<?=$fs_imagepath.$simch_show?>" /></td>
			<td><img class="active" name="<?=$row['simch_active']?>" id="<?=$row['simch_id']?>" path="<?=$fs_imagepath?>" src="<?=$fs_imagepath.$simch_active?>" /></td>
			<td align="center"><img title="<?=tt("Sao chép bản ghi")?>" alt="<?=tt("Sao chép")?>" src="<?=$fs_imagepath?>copy.png" onClick="if (confirm('<?=tt("Bạn muốn sao chép bản ghi này?")?>')){ window.location.href='copy.php?record_id=<?=$row['simch_id']?>&returnurl=<?=base64_encode(getURL())?>'}" border="0" style="cursor:pointer"></td>
			<td><a href="edit.php?record_id=<?=$row['simch_id'];?>&url=<?=base64_encode(getURL())?>"><img src="<?=$fs_imagepath?>edit.png" alt="<?=tt("Edit")?>" border="0"></a></td>
			<td><img class="remove" id="<?=$row['simch_id']?>" src="<?=$fs_imagepath?>delete.png" /></td>
		</tr>       
		<?
		}
		?>	
	</table>
</form>

<? template_bottom(); ?>
</body>
</html>