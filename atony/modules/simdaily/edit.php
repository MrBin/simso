<?
require_once("config_security.php");

$record_id			= getValue("record_id","int","GET");

// Kiem tra quyen voi ban ghi
checkAddEdit("edit");	
checkRowUser($fs_table, $field_id, $record_id, base64_encode('listing.php'));

// Lay ra ban ghi dang sua
$db_data   = new db_query("SELECT * 
									FROM " . $fs_table . " 
									WHERE " . $field_id . " = " . $record_id
									);
if($row = mysql_fetch_assoc($db_data->result)){
	foreach($row as $key => $value) if($key != 'lang_id' && $key != 'admin_id') $$key = $value;
}else{
	exit();
} // End if($row = mysql_fetch_assoc($db_data->result))
unset($db_data);

// Khai bao bien
$errorMsg 			= "";		//Warning Error!
$action				= getValue("action", "str", "POST", "");

$add					= "add.php";
$listing				= "listing.php";
$edit					= "edit.php";
$after_save_data	= getValue("after_save_data", "str", "POST", $listing);

// generate_form
$myform = new generate_form();	//Call Class generate_form();
$myform->removeHTML(0);	//Loại bỏ chức năng không cho điền tag html trong form
$myform->addTable($fs_table);
$myform->add("simch_name","simch_name",0,0,"",1,"Điền tên đại lý",0,"");
$myform->add("simch_viettat","simch_viettat",0,0,"",1,"Điền tên viết tắt",0,"");
$myform->add("simch_hienthi","simch_hienthi",0,0,"",1,"Điền tên hiển thị trên web",0,"");
$myform->add("simch_teaser","simch_teaser",0,0,"",0,"",0,"");
$myform->evaluate(); // đổi tên trường thành biến và giá trị

// Nếu như có form được submit
if($action == "submitForm"){
	// Kiểm tra lỗi
	$errorMsg .= $myform->checkdata();
	$errorMsg .= $myform->strErrorFeld ;	//Check Error!
	if($errorMsg == ""){	
	
		// Thuc hien query
		$query = $myform->generate_update_SQL($field_id,$record_id);
		$db_ex = new db_execute($query);
		//echo $query;exit();

		// Chuyen ve trang khac khi xu ly du lieu oki
		redirect($after_save_data . "?record_id=" . $record_id);
		exit();
		
    } // End if($errorMsg == ""){
	
} // End if($action == "submitForm")

// Khai bao ten form 
$myform->addFormname("submitForm"); //add  tên form để javacheck

// Xử lý javascript
$myform->addjavasrciptcode('');
$myform->checkjavascript();

// Chuyen ten truong thanh bien va gia tri
foreach($row as $key=>$value) if($key != 'lang_id' && $key != 'admin_id') $$key = $value;
?>

<html>
<head>
<title>Channel Add</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<?=$load_header?>
</head>

<body>
<? template_top(tt("Sửa bản ghi"))?>
<? $fs_action = $_SERVER['SCRIPT_NAME'] . "?" . @$_SERVER['QUERY_STRING']; ?>
<?=$myform->create_form("submitForm", $fs_action, "post", "multipart/form-data","");?>
<?=$myform->create_table(8,3,"");?>
<?=$myform->text_note()?>
<?=$myform->errorMsg($errorMsg)?>

<?=$myform->text(1 , "Tên đại lý", "simch_name", "simch_name", $simch_name, "Tên đại lý", 250, "", 255, "", "", "")?>
<?=$myform->text(1 , "Tên viết tắt", "simch_viettat", "simch_viettat", $simch_viettat, "Tên viết tắt", 250, "", 255, "", "", "")?>
<?=$myform->text(1 , "Tên hiển thị", "simch_hienthi", "simch_hienthi", $simch_hienthi, "Tên hiển thị trên web", 250, "", 255, "", "", "")?>
<?=$myform->textarea(0,"Mô tả", "simch_teaser", "simch_teaser", $simch_teaser, 400, 120, "", "")?>

<?=$myform->radio(0 , "Sau khi lưu dữ liệu", "add_new" . $myform->ec . "return_listing" . $myform->ec . "return_edit", "after_save_data", $add . $myform->ec . $listing . $myform->ec . $edit, $after_save_data, "Thêm mới" . $myform->ec . "Quay về danh sách" . $myform->ec . "Sửa bản ghi", "");?>
<?=$myform->button("button" . $myform->ec . "reset", "button" . $myform->ec . "reset", "button" . $myform->ec . "reset", "Cập nhật"  . $myform->ec .  "Làm lại", "", "");?>
<?=$myform->hidden("action", "action", "submitForm", "");?>
<?=$myform->close_table();?>
<?=$myform->close_form();?>
<? unset($form);?>
<? template_bottom() ?>
</body>
</html>