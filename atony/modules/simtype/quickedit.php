<?
require_once("config_security.php");	//check security	 

//Khai bao Bien
$errorMsg 		= "";
$iQuick 			= getValue("iQuick","str","POST","");
$returnurl 		= base64_decode(getValue("returnurl","str","GET",base64_encode("listing.php")));
$record_id		= getValue("record_id", "arr", "POST", "");

//check quyền them sua xoa
checkAddEdit("edit");

//
if ($iQuick == 'update'){
	if($record_id != ""){
		for($i=0; $i<count($record_id); $i++){
			
			// Kiem tra quyen user voi ban ghi
			checkRowUser($fs_table,$field_id,$record_id[$i],$returnurl);
			
			$simtp_name				= getValue("simtp_name" . $record_id[$i], "str", "POST", "");
			$simtp_name_check		= getValue("simtp_name_check" . $record_id[$i], "str", "POST", "");
			$simtp_index 			= getValue("simtp_index" . $record_id[$i], "str", "POST", "");
			$simtp_index_check 	= getValue("simtp_index_check" . $record_id[$i], "str", "POST", "");
			$require = ($simtp_index != "" && $simtp_index != $simtp_index_check) ? 1 : 0;
			if($simtp_index == '') $simtp_index = removeTitle($simtp_name,'/');

			// Call Class generate_form();
			$myform = new generate_form();
			$myform->removeHTML(0);
			$myform->addTable($fs_table);
			$myform->add("simtp_name","simtp_name",0,1,"",0,"",0,"");
			$myform->add("simtp_index","simtp_index",0,1,"",0,"",$require,"Tên loại sim đã tồn tại");
			$myform->add("simtp_order","simtp_order" . $record_id[$i],1,0,"",0,"",0,"");

			$errorMsg .= $myform->checkdata();
			if($errorMsg == ""){
				$db_ex = new db_execute($myform->generate_update_SQL("simtp_id",$record_id[$i]));
				//echo $myform->generate_update_SQL("con_id",$record_id[$i]);exit();
			}
			
		} // End for($i=0; $i<count($record_id); $i++)
		
	} // End if($record_id != "")
	
	redirect($returnurl);
} // End if ($iQuick == 'update')
?>