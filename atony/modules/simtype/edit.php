<?
require_once("config_security.php");

$record_id			= getValue("record_id","int","GET");

// Kiem tra quyen voi ban ghi
checkAddEdit("edit");	
checkRowUser($fs_table, $field_id, $record_id, base64_encode('listing.php'));

// Lay ra ban ghi dang sua
$db_data   = new db_query("SELECT * 
									FROM " . $fs_table . " 
									WHERE " . $field_id . " = " . $record_id
									);
if($row = mysql_fetch_assoc($db_data->result)){
	foreach($row as $key => $value) if($key != 'lang_id' && $key != 'admin_id') $$key = $value;
}else{
	exit();
} // End if($row = mysql_fetch_assoc($db_data->result))
unset($db_data);

// Khai bao bien
$errorMsg 			= "";		//Warning Error!
$action				= getValue("action", "str", "POST", "");

$add					= "add.php";
$listing				= "listing.php";
$edit					= "edit.php";
$after_save_data	= getValue("after_save_data", "str", "POST", $listing);

$simtp_strdate		= getValue("simtp_strdate", "str", "POST", date("d/m/Y", $simtp_date));
$simtp_strtime		= getValue("simtp_strtime", "str", "POST", date("H:i:s", $simtp_date));
$simtp_date			= convertDateTime($simtp_strdate, $simtp_strtime);

$simtp_name_req	= getValue("simtp_name", "str", "POST", "");
$simtp_index_req	= getValue("simtp_index", "str", "POST", "");

$require = ($simtp_index_req != "" && $simtp_index_req != $simtp_index) ? 1 : 0;
if($simtp_index_req == "") $simtp_index = removeTitle($simtp_name_req, '/');
else $simtp_index = $simtp_index_req;
	
// Goi class generate form
$myform = new generate_form();	//Call Class generate_form();
$myform->removeHTML(0);	//Loại bỏ chức năng không cho điền tag html trong form
$myform->addTable($fs_table);	// Add table
$myform->add("simtp_name","simtp_name",0,0,"",1,"Điền tên loại sim",0,"");
$myform->add("simtp_index","simtp_index",0,1,"",0,"",$require,"Tên loại sim đã tồn tại");
$myform->add("simtp_date","simtp_date",1,1,0,0,"",0,"");
$myform->add("simtp_order","simtp_order",1,1,0,0,"",0,"");
$myform->add("simtp_description","simtp_description",0,0,"",0,"",0,"");
$myform->add("meta_title","meta_title",0,0,"",0,"",0,"");
$myform->add("meta_keyword","meta_keyword",0,0,"",0,"",0,"");
$myform->add("meta_description","meta_description",0,0,"",0,"",0,"");
$myform->evaluate(); // đổi tên trường thành biến và giá trị

// Nếu như có form được submit
if($action == "submitForm"){
	// Kiểm tra lỗi
	$errorMsg .= $myform->checkdata();
	$errorMsg .= $myform->strErrorFeld ;	//Check Error!
	if($errorMsg == ""){	
	
		// Thuc hien query
		$query = $myform->generate_update_SQL($field_id,$record_id);
		$db_ex = new db_execute($query);
		//echo $query;exit();

		// Chuyen ve trang khac khi xu ly du lieu oki
		redirect($after_save_data . "?record_id=" . $record_id);
		exit();
		
    } // End if($errorMsg == ""){
	
} // End if($action == "submitForm")

// Khai bao ten form 
$myform->addFormname("submitForm"); //add  tên form để javacheck

// Xử lý javascript
$myform->addjavasrciptcode('');
$myform->checkjavascript();

// Chuyen ten truong thanh bien va gia tri
foreach($row as $key=>$value) if($key != 'lang_id' && $key != 'admin_id') $$key = $value;
?>

<html>
<head>
<title>Channel Add</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<?=$load_header?>
</head>

<body>
<? template_top(tt("Sửa bản ghi"))?>

<table class="tab" border="0" cellpadding="0" cellspacing="0">
	<tr align="center">
		<td nowrap><div class="n" id="tabt_1" onClick="tabdiv(1,5);document.cookie='tab_select=1'"><?=tt("Chung")?></div></td>
		<td nowrap><div class="n" id="tabt_2" onClick="tabdiv(2,5);document.cookie='tab_select=2'"><?=tt("Mô tả")?></div></td>
		<td nowrap><div class="n" id="tabt_3" onClick="tabdiv(3,5);document.cookie='tab_select=3'"><?=tt("SEO")?></div></td>
		<td style="border-bottom:solid 1px #DADADA;" width="99%">&nbsp;</td>
	</tr>
</table>

<? $fs_action			= $_SERVER['SCRIPT_NAME'] . "?" . @$_SERVER['QUERY_STRING'];?>
<?=$myform->create_form("submitForm", $fs_action, "post", "multipart/form-data","");?>

<div style="border:solid 1px #DADADA; border-top-width:0; padding:10px;">
	<?=$myform->errorMsg($errorMsg)?>
    
	<div id="tabc_1" class="tabc">
		<?=$myform->create_table(8,3,"");?>
		<?=$myform->text_note()?>
		<?=$myform->text(0 , "Tên loại sim", "simtp_name", "simtp_name", $simtp_name, "Tên loại sim", 250, "", 255, "", "", "")?>
		<script language="javascript">
		$(function() {
			$('#getDate').click(function() {
				$('#simtp_strdate').val('<?=date("d/m/Y")?>');
				$('#simtp_strtime').val('<?=date("H:i:s")?>');
			})
		})
		</script>
		<?=$myform->text(0, "Ngày cập nhật", "simtp_strdate" . $myform->ec . "simtp_strtime", "simtp_strdate" . $myform->ec . "simtp_strtime", $simtp_strdate . $myform->ec . $simtp_strtime, "Ngày (dd/mm/yyyy)" . $myform->ec . "Giờ (hh/mm/ss)", "70" . $myform->ec . "70", $myform->ec, "10" . $myform->ec . "10", " - ", $myform->ec, "&nbsp; <input id='getDate' type='button' value='Get date' class='form_button'>");?>
		<?=$myform->close_table();?>
	</div>
    
	<div id="tabc_2" class="tabc">
		<?=$myform->wysiwyg("", "simtp_description", $simtp_description, "../../resource/ckeditor/", "99%", 300)?>
	</div>
    
	<div id="tabc_3" class="tabc">
		<div class="mart10">Đường dẫn :</div>
		<input type="text" name="simtp_index" id="simtp_index" value="<?=$simtp_index?>" class="form_control" style="width:50%">

		<div class="mart10">Google Title :</div>
		<textarea name="meta_title" id="meta_title" class="form_control" style="width:90%; height:50px;"><?=$meta_title?></textarea>
		
		<div class="mart10">Google Description :</div>
		<textarea name="meta_description" id="meta_description" class="form_control" style="width:90%; height:100px;"><?=$meta_description?></textarea>
		
		<div class="mart10">Google Keyword : <span> - <i class="font11">(Các keyword cách nhau bằng dầu phẩy ',')</i></span></div>
		<textarea name="meta_keyword" id="meta_keyword" class="form_control" style="width:90%; height:100px;"><?=$meta_keyword?></textarea>
	</div>
    
	<?=$myform->create_table(8,3,"");?>
	<?=$myform->radio(0 , "Sau khi lưu dữ liệu", "add_new" . $myform->ec . "return_listing" . $myform->ec . "return_edit", "after_save_data", $add . $myform->ec . $listing . $myform->ec . $edit, $after_save_data, "Thêm mới" . $myform->ec . "Quay về danh sách" . $myform->ec . "Sửa bản ghi", "");?>
	<?=$myform->button("button" . $myform->ec . "reset", "button" . $myform->ec . "reset", "button" . $myform->ec . "reset", "Cập nhật"  . $myform->ec .  "Làm lại", "", "");?>
	<?=$myform->hidden("action", "action", "submitForm", "");?>
	<?=$myform->close_table();?>
</div>

<?=$myform->close_form();?>
<? unset($form);?>

<? $tab_select = getValue("tab_select","int","COOKIE",1);?>
<script>tabdiv(<?=$tab_select?>,5)</script>

<? template_bottom() ?>

</body>
</html>