<?
require_once("../../resource/security/security.php");

$module_id			 	= 14;

//check security...
checkloggedin();
if (checkaccess($module_id) != 1){
	redirect("../deny.htm");
	exit();
}

$fs_table			= "tbl_simtype";
$field_id			= "simtp_id";
$field_name			= "simtp_name";

$fs_filepath		= "";
?>