self.AMP_CONFIG = {
	"canary": false,
	"amp-ios-overflow-x": 1,
	"amp-experiment": 1,
	"pan-y": 1,
	"expAdsenseA4A": 0.1,
	"expDoubleclickA4A": 0.1,
	"a4aProfilingRate": 1,
	"amp-form": 1,
	"form-submit": 1,
	"amp-scrollable-carousel": 1,
	"amp-fx-flying-carpet": 1,
	"amp-app-banner": 1,
	"v": "041478801557976"
};
try {
	(function() {
		function mixin(obj, source) {
			function parent() {}
			parent.prototype = source.prototype;
			obj.prototype = new parent;
			obj.prototype.constructor = obj;
			var prop;
			for (prop in source) {
				if (Object.defineProperties) {
					var desc = Object.getOwnPropertyDescriptor(source, prop);
					if (desc) {
						Object.defineProperty(obj, prop, desc);
					}
				} else {
					obj[prop] = source[prop];
				}
			}
		}

		function fail(opt_attributes) {
			var scope = opt_attributes || self;
			var options;
			if (scope.AMP_MODE) {
				options = scope.AMP_MODE;
			} else {
				options = scope;
				if (options.context && options.context.mode) {
					options = options.context.mode;
				} else {
					var $ = parseQueryString(options.location.originalHash || options.location.hash);
					var q = parseQueryString(options.location.search);
					if (!optsData) {
						optsData = options.AMP_CONFIG && options.AMP_CONFIG.v ? options.AMP_CONFIG.v : "011478801557976";
					}
					options = {
						localDev: false,
						development: !("1" != $.development && !options.AMP_DEV_MODE),
						filter: $.filter,
						minified: true,
						lite: void 0 != q.amp_lite,
						test: false,
						log: $.log,
						version: "1478801557976",
						rtvVersion: optsData
					};
				}
				options = scope.AMP_MODE = options;
			}
			return options;
		}

		function parseQueryString(baseName) {
			var object = Object.create(null);
			if (!baseName) {
				return object;
			}
			if (0 == baseName.indexOf("?") || 0 == baseName.indexOf("#")) {
				baseName = baseName.substr(1);
			}
			var codeSegments = baseName.split("&");
			var i = 0;
			for (; i < codeSegments.length; i++) {
				var part = codeSegments[i];
				var index = part.indexOf("=");
				var key;
				var val;
				if (-1 != index) {
					key = decodeURIComponent(part.substring(0, index)).trim();
					val = decodeURIComponent(part.substring(index + 1)).trim();
				} else {
					key = decodeURIComponent(part).trim();
					val = "";
				}
				if (key) {
					object[key] = val;
				}
			}
			return object;
		}

		function test(successCallback, component, reason) {
			this.win = successCallback;
			this.se = component;
			this.Ea = this.win.console && (this.win.console.log && "0" != fail().log) ? this.se({
				localDev: false,
				development: fail(void 0).development,
				filter: fail(void 0).filter,
				minified: true,
				lite: fail(void 0).lite,
				test: false,
				log: fail(void 0).log,
				version: fail(void 0).version,
				rtvVersion: fail(void 0).rtvVersion
			}) : 0;
			this.Jb = reason || "";
		}

		function log(settings, keepData, type, arg) {
			if (0 != settings.Ea) {
				var fn = settings.win.console.log;
				if ("ERROR" == type) {
					fn = settings.win.console.error || fn;
				} else {
					if ("INFO" == type) {
						fn = settings.win.console.info || fn;
					} else {
						if ("WARN" == type) {
							fn = settings.win.console.warn || fn;
						}
					}
				}
				arg.unshift(Date.now() - ctrlx, "[" + keepData + "]");
				fn.apply(settings.win.console, arg);
			}
		}

		function onError(message, result) {
			if (message.Jb) {
				if (result.message) {
					if (-1 == result.message.indexOf(message.Jb)) {
						result.message += message.Jb;
					}
				} else {
					result.message = message.Jb;
				}
			}
		}

		function constructor(config) {
			var error = null;
			var message = "";
			var i = 0;
			for (; i < arguments.length; i++) {
				var err = arguments[i];
				if (err instanceof Error && !error) {
					error = err;
				} else {
					if (message) {
						message += " ";
					}
					message += err;
				}
			}
			if (error) {
				if (message) {
					error.message = message + ": " + error.message;
				}
			} else {
				error = Error(message);
			}
			return error;
		}

		function then(callback) {
			var props = constructor.apply(null, arguments);
			setTimeout(function() {
				throw props;
			});
		}

		function error() {
			if (req.user) {
				return req.user;
			}
			if (!User) {
				throw Error("failed to call initLogConstructor");
			}
			return req.user = new User(self, function(options) {
				var charCodeToReplace = parseInt(options.log, 10);
				return options.development || 1 <= charCodeToReplace ? 4 : 0;
			}, "\u200b\u200b\u200b");
		}

		function call() {
			if (req.dev) {
				return req.dev;
			}
			if (!User) {
				throw Error("failed to call initLogConstructor");
			}
			return req.dev = new User(self, function(color) {
				color = parseInt(color.log, 10);
				return 3 <= color ? 4 : 2 <= color ? 3 : 0;
			});
		}

		function eachEvent(ev) {
			return Array.isArray(ev);
		}

		function makeArray(ar) {
			if (!ar) {
				return [];
			}
			var a = Array(ar.length);
			var i = 0;
			for (; i < ar.length; i++) {
				a[i] = ar[i];
			}
			return a;
		}

		function defaults() {
			var map;
			var obj = Object.create(null);
			if (map) {
				Object.assign(obj, map);
			}
			return obj;
		}

		function isString(obj) {
			return "[object Object]" === core_toString.call(obj);
		}

		function isArrayLike(val) {
			return "number" === typeof val && isFinite(val);
		}

		function contains(target) {
			return target == this || this.documentElement.contains(target);
		}

		function inspect(obj) {
			if (!obj.HTMLDocument.prototype.contains) {
				obj.HTMLDocument.prototype.contains = contains;
			}
		}

		function sign(val) {
			return (val = Number(val)) ? 0 < val ? 1 : -1 : val;
		}

		function every(arr) {
			if (null == arr) {
				throw new TypeError("Cannot convert undefined or null to object");
			}
			var object = Object(arr);
			var i = 1;
			for (; i < arguments.length; i++) {
				var iterable = arguments[i];
				if (null != iterable) {
					var key;
					for (key in iterable) {
						if (hasOwnProperty.call(iterable, key)) {
							object[key] = iterable[key];
						}
					}
				}
			}
			return object;
		}

		function def(value) {
			if (!(this instanceof def)) {
				throw new TypeError("Constructor Promise requires `new`");
			}
			if (!isFunction(value)) {
				throw new TypeError("Must pass resolver function");
			}
			this._state = result;
			this._value = [];
			this._isChainEnd = true;
			__(this, transition(this, fallback), transition(this, Model), {
				then: value
			});
		}

		function ok(value) {
			var ok = this;
			var av;
			return av = value === Object(value) && value instanceof this ? value : new ok(function($sanitize) {
				$sanitize(value);
			});
		}

		function reject(reason) {
			return new this(function(dataAndEvents, reject) {
				reject(reason);
			});
		}

		function getGame(elems) {
			var require = this;
			return new require(function(behavior, callback) {
				var length = elems.length;
				var result = Array(length);
				if (0 === length) {
					return behavior(result);
				}
				arrayForEach(elems, function(from, key) {
					require.resolve(from).then(function(value) {
						result[key] = value;
						if (0 === --length) {
							behavior(result);
						}
					}, callback);
				});
			});
		}

		function race(results) {
			var dfd = this;
			return new dfd(function(sqlt, callback) {
				var i = 0;
				var l = results.length;
				for (; i < l; i++) {
					dfd.resolve(results[i]).then(sqlt, callback);
				}
			});
		}

		function fallback(test, result, op, res) {
			if (!result) {
				return this;
			}
			if (!res) {
				res = new Deferred(this.constructor);
			}
			expect(defer(res, result, test));
			return res.promise;
		}

		function Model(name, texturePath, data, def) {
			if (!data) {
				return this;
			}
			if (!def) {
				def = new Deferred(this.constructor);
			}
			expect(defer(def, data, name));
			return def.promise;
		}

		function result(property, err, object, result) {
			if (!err && !object) {
				return this;
			}
			if (!result) {
				result = new Deferred(this.constructor);
			}
			property.push({
				deferred: result,
				onFulfilled: err || result.resolve,
				onRejected: object || result.reject
			});
			return result.promise;
		}

		function Deferred(promise) {
			var deferred = this;
			this.promise = new promise(function(resolve, reject) {
				deferred.resolve = resolve;
				deferred.reject = reject;
			});
			return deferred;
		}

		function X(m, n, o) {
			var codeSegments = m._value;
			m._state = n;
			m._value = o;
			var i = 0;
			for (; i < codeSegments.length; i++) {
				var me = codeSegments[i];
				m._state(o, me.onFulfilled, me.onRejected, me.deferred);
			}
			if (n === Model) {
				if (m._isChainEnd) {
					setTimeout(function() {
						if (m._isChainEnd) {
							throw o;
						}
					}, 0);
				}
			}
		}

		function transition(method, callback) {
			return function(b) {
				X(method, callback, b);
			};
		}

		function _server() {}

		function isFunction(fn) {
			return "function" === typeof fn;
		}

		function arrayForEach(array, action) {
			var i = 0;
			for (; i < array.length; i++) {
				action(array[i], i);
			}
		}

		function defer(obj, next, name) {
			var c = obj.promise;
			var device = obj.resolve;
			var reject = obj.reject;
			return function() {
				try {
					var actual = next(name);
					if (device !== next) {
						if (reject !== next) {
							__(c, device, reject, actual, actual);
						}
					}
				} catch (exception) {
					reject(exception);
				}
			};
		}

		function __(t, type, fn, value, val) {
			var server = fn;
			var then;
			var checkevalblock;
			try {
				if (value === t) {
					throw new TypeError("Cannot fulfill promise with itself");
				}
				var argsAreObjects = value === Object(value);
				if (argsAreObjects && value instanceof t.constructor) {
					X(t, value._state, value._value);
				} else {
					if (argsAreObjects && ((then = value.then) && isFunction(then))) {
						checkevalblock = function(block) {
							checkevalblock = server = _server;
							__(t, type, fn, block, block);
						};
						server = function(deepDataAndEvents) {
							checkevalblock = server = _server;
							fn(deepDataAndEvents);
						};
						then.call(val, function(block) {
							checkevalblock(block);
						}, function(deepDataAndEvents) {
							server(deepDataAndEvents);
						});
					} else {
						type(value);
					}
				}
			} catch (deepDataAndEvents) {
				server(deepDataAndEvents);
			}
		}

		function init(global) {
			function next() {
				var pathConfig = pieces.splice(0, pieces.length);
				animationTimer = 0;
				for (; pathConfig.length;) {
					pathConfig.shift().call(null, pathConfig.shift());
				}
			}

			function debug(results, x) {
				var i = 0;
				var l = results.length;
				for (; i < l; i++) {
					cb(results[i], x);
				}
			}

			function forEach(values) {
				var i = 0;
				var valuesLen = values.length;
				var name;
				for (; i < valuesLen; i++) {
					name = values[i];
					fun(name, obj[filter(name)]);
				}
			}

			function f(n) {
				return function(el) {
					if (classes(el)) {
						cb(el, n);
						debug(el.querySelectorAll(query), n);
					}
				};
			}

			function filter(node) {
				var c = options.call(node, "is");
				var out = node.nodeName.toUpperCase();
				node = console.call(arg, c ? start + c.toUpperCase() : i + out);
				return c && (-1 < node && !$(out, c)) ? -1 : node;
			}

			function $(a, b) {
				return -1 < query.indexOf(a + '[is="' + b + '"]');
			}

			function emit(e) {
				var obj = e.currentTarget;
				var direction = e.attrChange;
				var val = e.attrName;
				var item = e.target;
				var LEFT = e[n] || 2;
				var RIGHT = e[methodName] || 3;
				if (xb && ((!item || item === obj) && (obj[fnName] && ("style" !== val && (e.prevValue !== e.newValue || "" === e.newValue && (direction === LEFT || direction === RIGHT)))))) {
					obj[fnName](val, direction === LEFT ? null : e.prevValue, direction === RIGHT ? null : e.newValue);
				}
			}

			function loop(n) {
				var attributes = f(n);
				return function(pi) {
					pieces.push(attributes, pi.target);
					if (animationTimer) {
						clearTimeout(animationTimer);
					}
					animationTimer = setTimeout(next, 1);
				};
			}

			function onclick(event) {
				if (Td) {
					Td = false;
					event.currentTarget.removeEventListener(domContentLoaded, onclick);
				}
				debug((event.target || doc).querySelectorAll(query), event.detail === id ? id : len);
				if (trimmedClasses) {
					toggle();
				}
			}

			function complete(name, value) {
				iterator.call(this, name, value);
				select.call(this, {
					target: this
				});
			}

			function add(doc, obj) {
				h(doc, obj);
				if (jQuery) {
					jQuery.observe(doc, restoreScript);
				} else {
					if (zb) {
						doc.setAttribute = complete;
						doc[type] = log(doc);
						doc[key](test_text, select);
					}
					doc[key](name, emit);
				}
				if (doc[k]) {
					if (xb) {
						doc.created = true;
						doc[k]();
						doc.created = false;
					}
				}
			}

			function toggle() {
				var target;
				var idx = 0;
				var len = ary.length;
				for (; idx < len; idx++) {
					target = ary[idx];
					if (!element.contains(target)) {
						len--;
						ary.splice(idx--, 1);
						cb(target, id);
					}
				}
			}

			function success(object) {
				throw Error("A " + object + " type is already registered");
			}

			function cb(item, i) {
				var func;
				var name = filter(item);
				if (-1 < name) {
					buildParams(item, obj[name]);
					name = 0;
					if (i !== len || item[len]) {
						if (!(i !== id)) {
							if (!item[id]) {
								item[len] = false;
								item[id] = true;
								name = 1;
							}
						}
					} else {
						item[id] = false;
						item[len] = true;
						name = 1;
						if (trimmedClasses) {
							if (0 > console.call(ary, item)) {
								ary.push(item);
							}
						}
					}
					if (name) {
						if (func = item[i + j]) {
							func.call(item);
						}
					}
				}
			}

			function constructor() {}

			function init(name, klass, response) {
				response = response && response[index] || "";
				var data = klass.prototype;
				var instance = getter(data);
				var ret = klass.observedAttributes || common;
				var ans = {
					prototype: instance
				};
				defineProperty(instance, k, {
					value: function() {
						if (Bb) {
							Bb = false;
						} else {
							if (!this[param]) {
								this[param] = true;
								new klass(this);
								if (data[k]) {
									data[k].call(this);
								}
								var factory = item[t.get(klass)];
								if (!html || 1 < factory.create.length) {
									update(this);
								}
							}
						}
					}
				});
				defineProperty(instance, fnName, {
					value: function(callback) {
						if (-1 < console.call(ret, callback)) {
							data[fnName].apply(this, arguments);
						}
					}
				});
				if (data[tempI]) {
					defineProperty(instance, frequency, {
						value: data[tempI]
					});
				}
				if (data[idProperty]) {
					defineProperty(instance, rvar, {
						value: data[idProperty]
					});
				}
				if (response) {
					ans[index] = response;
				}
				name = name.toUpperCase();
				item[name] = {
					constructor: klass,
					create: response ? [response, fn(name)] : [name]
				};
				t.set(klass, name);
				doc[ADDEVENTLISTENER](name.toLowerCase(), ans);
				render(name);
				timeMap[name].r();
			}

			function hasClass(name) {
				return (name = item[name.toUpperCase()]) && name.constructor;
			}

			function append(node) {
				return "string" === typeof node ? node : node && node.is || "";
			}

			function update(obj) {
				var fn = obj[fnName];
				var tokenized = fn ? obj.attributes : common;
				var index = tokenized.length;
				var attr;
				for (; index--;) {
					attr = tokenized[index];
					fn.call(obj, attr.name || attr.nodeName, null, attr.value || attr.nodeValue);
				}
			}

			function render(name) {
				name = name.toUpperCase();
				if (!(name in timeMap)) {
					timeMap[name] = {};
					timeMap[name].p = new Promise(function(r) {
						timeMap[name].r = r;
					});
				}
				return timeMap[name].p;
			}

			function create() {
				function create(i) {
					var klass = global[i];
					if (klass) {
						global[i] = function construct(data) {
							var isRTL;
							if (!data) {
								data = this;
							}
							if (!data[param]) {
								Bb = true;
								data = item[t.get(data.constructor)];
								data = (isRTL = html && 1 === data.create.length) ? Reflect.construct(klass, common, data.constructor) : doc.createElement.apply(doc, data.create);
								data[param] = true;
								Bb = false;
								if (!isRTL) {
									update(data);
								}
							}
							return data;
						};
						global[i].prototype = klass.prototype;
						try {
							klass.prototype.constructor = global[i];
						} catch (Fg) {
							Lg = true;
							get(klass, param, {
								value: global[i]
							});
						}
					}
				}
				if (self) {
					delete global.customElements;
				}
				get(global, "customElements", {
					configurable: true,
					value: new constructor
				});
				get(global, "CustomElementRegistry", {
					configurable: true,
					value: constructor
				});
				var tokenized = cl.get(/^HTML[A-Z]*[a-z]/);
				var index = tokenized.length;
				for (; index--; create(tokenized[index])) {}
				doc.createElement = function(nodeName, value) {
					return (value = append(value)) ? parse.call(this, nodeName, fn(value)) : parse.call(this, nodeName);
				};
			}
			var doc = global.document;
			var OBJECT = global.Object;
			var cl = function(rules) {
				function normalize(prop, name) {
					name = name.toLowerCase();
					if (!(name in obj)) {
						obj[prop] = (obj[prop] || []).concat(name);
						obj[name] = obj[name.toUpperCase()] = prop;
					}
				}

				function callback(user) {
					var contents = [];
					var attributes;
					for (attributes in obj) {
						if (user.test(attributes)) {
							contents.push(attributes);
						}
					}
					return contents;
				}
				var rparentsprev = /^[A-Z]+[a-z]/;
				var obj = (OBJECT.create || OBJECT)(null);
				var transition = {};
				var i;
				var selector;
				var data;
				var prop;
				for (selector in rules) {
					for (prop in rules[selector]) {
						data = rules[selector][prop];
						obj[prop] = data;
						i = 0;
						for (; i < data.length; i++) {
							obj[data[i].toLowerCase()] = obj[data[i].toUpperCase()] = prop;
						}
					}
				}
				transition.get = function(name) {
					return "string" === typeof name ? obj[name] || (rparentsprev.test(name) ? [] : "") : callback(name);
				};
				transition.set = function e(name, index) {
					return rparentsprev.test(name) ? normalize(name, index) : normalize(index, name), transition;
				};
				return transition;
			}({
				collections: {
					HTMLAllCollection: ["all"],
					HTMLCollection: ["forms"],
					HTMLFormControlsCollection: ["elements"],
					HTMLOptionsCollection: ["options"]
				},
				elements: {
					Element: ["element"],
					HTMLAnchorElement: ["a"],
					HTMLAppletElement: ["applet"],
					HTMLAreaElement: ["area"],
					HTMLAttachmentElement: ["attachment"],
					HTMLAudioElement: ["audio"],
					HTMLBRElement: ["br"],
					HTMLBaseElement: ["base"],
					HTMLBodyElement: ["body"],
					HTMLButtonElement: ["button"],
					HTMLCanvasElement: ["canvas"],
					HTMLContentElement: ["content"],
					HTMLDListElement: ["dl"],
					HTMLDataElement: ["data"],
					HTMLDataListElement: ["datalist"],
					HTMLDetailsElement: ["details"],
					HTMLDialogElement: ["dialog"],
					HTMLDirectoryElement: ["dir"],
					HTMLDivElement: ["div"],
					HTMLDocument: ["document"],
					HTMLElement: "element abbr address article aside b bdi bdo cite code command dd dfn dt em figcaption figure footer header i kbd mark nav noscript rp rt ruby s samp section small strong sub summary sup u var wbr".split(" "),
					HTMLEmbedElement: ["embed"],
					HTMLFieldSetElement: ["fieldset"],
					HTMLFontElement: ["font"],
					HTMLFormElement: ["form"],
					HTMLFrameElement: ["frame"],
					HTMLFrameSetElement: ["frameset"],
					HTMLHRElement: ["hr"],
					HTMLHeadElement: ["head"],
					HTMLHeadingElement: "h1 h2 h3 h4 h5 h6".split(" "),
					HTMLHtmlElement: ["html"],
					HTMLIFrameElement: ["iframe"],
					HTMLImageElement: ["img"],
					HTMLInputElement: ["input"],
					HTMLKeygenElement: ["keygen"],
					HTMLLIElement: ["li"],
					HTMLLabelElement: ["label"],
					HTMLLegendElement: ["legend"],
					HTMLLinkElement: ["link"],
					HTMLMapElement: ["map"],
					HTMLMarqueeElement: ["marquee"],
					HTMLMediaElement: ["media"],
					HTMLMenuElement: ["menu"],
					HTMLMenuItemElement: ["menuitem"],
					HTMLMetaElement: ["meta"],
					HTMLMeterElement: ["meter"],
					HTMLModElement: ["del", "ins"],
					HTMLOListElement: ["ol"],
					HTMLObjectElement: ["object"],
					HTMLOptGroupElement: ["optgroup"],
					HTMLOptionElement: ["option"],
					HTMLOutputElement: ["output"],
					HTMLParagraphElement: ["p"],
					HTMLParamElement: ["param"],
					HTMLPictureElement: ["picture"],
					HTMLPreElement: ["pre"],
					HTMLProgressElement: ["progress"],
					HTMLQuoteElement: ["blockquote", "q", "quote"],
					HTMLScriptElement: ["script"],
					HTMLSelectElement: ["select"],
					HTMLShadowElement: ["shadow"],
					HTMLSlotElement: ["slot"],
					HTMLSourceElement: ["source"],
					HTMLSpanElement: ["span"],
					HTMLStyleElement: ["style"],
					HTMLTableCaptionElement: ["caption"],
					HTMLTableCellElement: ["td", "th"],
					HTMLTableColElement: ["col", "colgroup"],
					HTMLTableElement: ["table"],
					HTMLTableRowElement: ["tr"],
					HTMLTableSectionElement: ["thead", "tbody", "tfoot"],
					HTMLTemplateElement: ["template"],
					HTMLTextAreaElement: ["textarea"],
					HTMLTimeElement: ["time"],
					HTMLTitleElement: ["title"],
					HTMLTrackElement: ["track"],
					HTMLUListElement: ["ul"],
					HTMLUnknownElement: ["unknown", "vhgroupv", "vkeygen"],
					HTMLVideoElement: ["video"]
				},
				nodes: {
					Attr: ["node"],
					Audio: ["audio"],
					CDATASection: ["node"],
					CharacterData: ["node"],
					Comment: ["#comment"],
					Document: ["#document"],
					DocumentFragment: ["#document-fragment"],
					DocumentType: ["node"],
					HTMLDocument: ["#document"],
					Image: ["img"],
					Option: ["option"],
					ProcessingInstruction: ["node"],
					ShadowRoot: ["#shadow-root"],
					Text: ["#text"],
					XMLDocument: ["xml"]
				}
			});
			var ADDEVENTLISTENER = "registerElement";
			var type = "__" + ADDEVENTLISTENER + (1E5 * global.Math.random() >> 0);
			var key = "addEventListener";
			var len = "attached";
			var j = "Callback";
			var id = "detached";
			var index = "extends";
			var fnName = "attributeChanged" + j;
			var frequency = len + j;
			var tempI = "connected" + j;
			var idProperty = "disconnected" + j;
			var k = "created" + j;
			var rvar = id + j;
			var n = "ADDITION";
			var v = "MODIFICATION";
			var methodName = "REMOVAL";
			var name = "DOMAttrModified";
			var domContentLoaded = "DOMContentLoaded";
			var test_text = "DOMSubtreeModified";
			var i = "<";
			var start = "=";
			var cx = /^[A-Z][A-Z0-9]*(?:-[A-Z0-9]+)+$/;
			var _this = "ANNOTATION-XML COLOR-PROFILE FONT-FACE FONT-FACE-SRC FONT-FACE-URI FONT-FACE-FORMAT FONT-FACE-NAME MISSING-GLYPH".split(" ");
			var arg = [];
			var obj = [];
			var query = "";
			var element = doc.documentElement;
			var console = arg.indexOf || function(string) {
				var i = this.length;
				for (; i-- && this[i] !== string;) {}
				return i;
			};
			var ObjProto = OBJECT.prototype;
			var hasOwnProperty = ObjProto.hasOwnProperty;
			var matches = ObjProto.isPrototypeOf;
			var get = OBJECT.defineProperty;
			var common = [];
			var callback = OBJECT.getOwnPropertyDescriptor;
			var _keys = OBJECT.getOwnPropertyNames;
			var getParent = OBJECT.getPrototypeOf;
			var program = OBJECT.setPrototypeOf;
			var inverse = !!OBJECT.__proto__;
			var Lg = false;
			var param = "__dreCEv1";
			var self = global.customElements;
			var html = !!(self && (self.define && (self.get && self.whenDefined)));
			var make = OBJECT.create || OBJECT;
			var Node = global.Map || function() {
				var el = [];
				var cache = [];
				var data;
				return {
					get: function(name) {
						return cache[console.call(el, name)];
					},
					set: function(attributes, value) {
						data = console.call(el, attributes);
						if (0 > data) {
							cache[el.push(attributes) - 1] = value;
						} else {
							cache[data] = value;
						}
					}
				};
			};
			var Promise = global.Promise || function(finish) {
				function done(total) {
					d = true;
					for (; immediateQueue.length;) {
						immediateQueue.shift()(total);
					}
				}
				var immediateQueue = [];
				var d = false;
				var isBuggy = {
					"catch": function() {
						return isBuggy;
					},
					then: function(callback) {
						immediateQueue.push(callback);
						if (d) {
							setTimeout(done, 1);
						}
						return isBuggy;
					}
				};
				finish(done);
				return isBuggy;
			};
			var Bb = false;
			var item = make(null);
			var timeMap = make(null);
			var t = new Node;
			var fn = String;
			var getter = OBJECT.create || function create(recurring) {
				return recurring ? (create.prototype = recurring, new create) : this;
			};
			var h = program || (inverse ? function(response, obj) {
				response.__proto__ = obj;
				return response;
			} : _keys && callback ? function() {
				function contains(object, b) {
					var key;
					var keys = _keys(b);
					var i = 0;
					var il = keys.length;
					for (; i < il; i++) {
						key = keys[i];
						if (!hasOwnProperty.call(object, key)) {
							get(object, key, callback(b, key));
						}
					}
				}
				return function(expr, elem) {
					do {
						contains(expr, elem);
					} while ((elem = getParent(elem)) && !matches.call(elem, expr));
					return expr;
				};
			}() : function(object, iterable) {
				var key;
				for (key in iterable) {
					object[key] = iterable[key];
				}
				return object;
			});
			var doubleQuotedValue = global.MutationObserver || global.WebKitMutationObserver;
			var elem = (global.HTMLElement || (global.Element || global.Node)).prototype;
			var trimmedClasses = !matches.call(elem, element);
			var defineProperty = trimmedClasses ? function(element, name, opt_attributes) {
				element[name] = opt_attributes.value;
				return element;
			} : get;
			var classes = trimmedClasses ? function(el) {
				return 1 === el.nodeType;
			} : function(expr) {
				return matches.call(elem, expr);
			};
			var ary = trimmedClasses && [];
			var cloneNode = elem.cloneNode;
			var handler = elem.dispatchEvent;
			var options = elem.getAttribute;
			var parent = elem.hasAttribute;
			var eventRemove = elem.removeAttribute;
			var iterator = elem.setAttribute;
			var fs = doc.createElement;
			var parse = fs;
			var restoreScript = doubleQuotedValue && {
				attributes: true,
				characterData: true,
				attributeOldValue: true
			};
			var value = doubleQuotedValue || function() {
				zb = false;
				element.removeEventListener(name, value);
			};
			var pieces;
			var animationTimer = 0;
			var de = false;
			var zb = true;
			var Td = true;
			var xb = true;
			var select;
			var onchange;
			var log;
			var jQuery;
			var buildParams;
			var fun;
			if (!(ADDEVENTLISTENER in doc)) {
				if (program || inverse) {
					buildParams = function(prefix, obj) {
						if (!matches.call(obj, prefix)) {
							add(prefix, obj);
						}
					};
					fun = add;
				} else {
					fun = buildParams = function(s, obj) {
						if (!s[type]) {
							s[type] = OBJECT(true);
							add(s, obj);
						}
					};
				}
				if (trimmedClasses) {
					zb = false;
					(function() {
						function handler(event) {
							var self = event.currentTarget;
							var node = self[type];
							event = event.propertyName;
							var e;
							if (node.hasOwnProperty(event)) {
								node = node[event];
								e = new CustomEvent(name, {
									bubbles: true
								});
								e.attrName = node.name;
								e.prevValue = node.value || null;
								e.newValue = node.value = self[event] || null;
								if (null == e.prevValue) {
									e[n] = e.attrChange = 0;
								} else {
									e[v] = e.attrChange = 1;
								}
								handler.call(self, e);
							}
						}

						function _trigger(value, val) {
							var result = parent.call(this, value);
							var okay = result && options.call(this, value);
							var e = new CustomEvent(name, {
								bubbles: true
							});
							iterator.call(this, value, val);
							e.attrName = value;
							e.prevValue = result ? okay : null;
							e.newValue = val;
							if (result) {
								e[v] = e.attrChange = 1;
							} else {
								e[n] = e.attrChange = 0;
							}
							handler.call(this, e);
						}

						function MutationEvent(elem) {
							var event = new CustomEvent(name, {
								bubbles: true
							});
							event.attrName = elem;
							event.prevValue = options.call(this, elem);
							event.newValue = null;
							event[methodName] = event.attrChange = 2;
							eventRemove.call(this, elem);
							handler.call(this, event);
						}
						var token = callback(elem, key);
						var fn = token.value;
						token.value = function(args, callback, right) {
							if (args === name) {
								if (this[fnName]) {
									if (this.setAttribute !== _trigger) {
										this[type] = {
											className: {
												name: "class",
												value: this.className
											}
										};
										this.setAttribute = _trigger;
										this.removeAttribute = MutationEvent;
										fn.call(this, "propertychange", handler);
									}
								}
							}
							fn.call(this, args, callback, right);
						};
						get(elem, key, token);
					})();
				} else {
					if (!doubleQuotedValue) {
						element[key](name, value);
						element.setAttribute(type, 1);
						element.removeAttribute(type);
						if (zb) {
							select = function(event) {
								var value;
								var attrs;
								var key;
								if (this === event.target) {
									value = this[type];
									this[type] = attrs = log(this);
									for (key in attrs) {
										if (!(key in value)) {
											return onchange(0, this, key, value[key], attrs[key], n);
										}
										if (attrs[key] !== value[key]) {
											return onchange(1, this, key, value[key], attrs[key], v);
										}
									}
									for (key in value) {
										if (!(key in attrs)) {
											return onchange(2, this, key, value[key], attrs[key], methodName);
										}
									}
								}
							};
							onchange = function(val, dataAndEvents, name, newStatus, newValue, prop) {
								name = {
									attrChange: val,
									currentTarget: dataAndEvents,
									attrName: name,
									prevValue: newStatus,
									newValue: newValue
								};
								name[prop] = val;
								emit(name);
							};
							log = function(id) {
								var obj;
								var ret = {};
								var a = id.attributes;
								var i = 0;
								var aLength = a.length;
								for (; i < aLength; i++) {
									obj = a[i];
									id = obj.name;
									if ("setAttribute" !== id) {
										ret[id] = obj.value;
									}
								}
								return ret;
							};
						}
					}
				}
				doc[ADDEVENTLISTENER] = function init(m, color) {
					function noop() {
						return prefix ? doc.createElement(name, text) : doc.createElement(name);
					}
					text = m.toUpperCase();
					if (!de) {
						de = true;
						if (doubleQuotedValue) {
							jQuery = function(opt_obj2, obj) {
								function forEach(arr, f) {
									var i = 0;
									var e = arr.length;
									for (; i < e; f(arr[i++])) {}
								}
								return new doubleQuotedValue(function(values) {
									var data;
									var context;
									var page;
									var i = 0;
									var valuesLen = values.length;
									for (; i < valuesLen; i++) {
										if (data = values[i], "childList" === data.type) {
											forEach(data.addedNodes, opt_obj2);
											forEach(data.removedNodes, obj);
										} else {
											if (context = data.target, xb && (context[fnName] && ("style" !== data.attributeName && (page = options.call(context, data.attributeName), page !== data.oldValue)))) {
												context[fnName](data.attributeName, data.oldValue, page);
											}
										}
									}
								});
							}(f(len), f(id));
							jQuery.observe(doc, {
								childList: true,
								subtree: true
							});
						} else {
							pieces = [];
							doc[key]("DOMNodeInserted", loop(len));
							doc[key]("DOMNodeRemoved", loop(id));
						}
						doc[key](domContentLoaded, onclick);
						doc[key]("readystatechange", onclick);
						elem.cloneNode = function(deep) {
							var doc = cloneNode.call(this, !!deep);
							var val = filter(doc);
							if (-1 < val) {
								fun(doc, obj[val]);
							}
							if (deep) {
								forEach(doc.querySelectorAll(query));
							}
							return doc;
						};
					}
					if (-2 < console.call(arg, start + text) + console.call(arg, i + text)) {
						success(m);
					}
					if (!cx.test(text) || -1 < console.call(_this, text)) {
						throw Error("The type " + m + " is invalid");
					}
					var a = color || ObjProto;
					var prefix = hasOwnProperty.call(a, index);
					var name = prefix ? color[index].toUpperCase() : text;
					var text;
					if (prefix) {
						if (-1 < console.call(arg, i + name)) {
							success(name);
						}
					}
					color = arg.push((prefix ? start : i) + text) - 1;
					query = query.concat(query.length ? "," : "", prefix ? name + '[is="' + m.toLowerCase() + '"]' : name);
					noop.prototype = obj[color] = hasOwnProperty.call(a, "prototype") ? a.prototype : getter(elem);
					debug(doc.querySelectorAll(query), len);
					return noop;
				};
				doc.createElement = parse = function(str, text) {
					var item = append(text);
					var data = item ? fs.call(doc, str, fn(item)) : fs.call(doc, str);
					str = "" + str;
					var keyName = console.call(arg, (item ? start : i) + (item || str).toUpperCase());
					var name = -1 < keyName;
					if (item) {
						data.setAttribute("is", item = item.toLowerCase());
						if (name) {
							name = $(str.toUpperCase(), item);
						}
					}
					xb = !doc.createElement.innerHTMLHelper;
					if (name) {
						fun(data, obj[keyName]);
					}
					return data;
				};
			}
			constructor.prototype = {
				constructor: constructor,
				define: html ? function(name, value, response) {
					if (response) {
						init(name, value, response);
					} else {
						var id = name.toUpperCase();
						item[id] = {
							constructor: value,
							create: [id]
						};
						t.set(value, id);
						self.define(name, value);
					}
				} : init,
				get: html ? function(name) {
					return self.get(name) || hasClass(name);
				} : hasClass,
				whenDefined: html ? function(name) {
					return Promise.race([self.whenDefined(name), render(name)]);
				} : render
			};
			if (self) {
				try {
					(function(klass, response, optgroup) {
						response[index] = "a";
						klass.prototype = getter(HTMLAnchorElement.prototype);
						klass.prototype.constructor = klass;
						global.customElements.define(optgroup, klass, response);
						if (options.call(doc.createElement("a", {
								is: optgroup
							}), "is") !== optgroup || html && options.call(new klass, "is") !== optgroup) {
							throw response;
						}
					})(function options() {
						return Reflect.construct(HTMLAnchorElement, [], options);
					}, {}, "document-register-element-a");
				} catch (ub) {
					create();
				}
			} else {
				create();
			}
			try {
				fs.call(doc, "a", "a");
			} catch (ub) {
				fn = function(arg) {
					return {
						is: arg
					};
				};
			}
		}

		function createElement(obj, name) {
			obj = forEach(obj);
			var tag = obj.services && (obj.services[name] && obj.services[name].obj);
			return tag;
		}

		function data(key, id) {
			var app = indexOf(key);
			return app && (app.services && (app.services[id] && app.services[id].obj));
		}

		function emit(element) {
			if (element.nodeType) {
				var res = (element.ownerDocument || element).defaultView;
				var err = forEach(res);
				if (res = res != err && (res.services && (res.services["url-replace"] && res.services["url-replace"].obj)) ? res.services["url-replace"].obj : null) {
					return res;
				}
			}
			return data(element, "url-replace");
		}

		function setData(object, name, value) {
			object = forEach(object);
			return eventsApi(object, object, name, value ? value : void 0);
		}

		function extend(object, name, obj) {
			object = forEach(object);
			return eventsApi(object, object, name, void 0, obj);
		}

		function triggerEvent(target, name, args) {
			var actual = Event(target);
			return eventsApi(indexOf(actual), actual, name, args);
		}

		function parseInt(type, name, child) {
			type = Event(type);
			return eventsApi(indexOf(type), type, name, void 0, child);
		}

		function increment(a, e) {
			a.__AMP_PARENT = e;
			a.__AMP_TOP = forEach(e);
		}

		function forEach(obj) {
			return obj.__AMP_TOP || obj;
		}

		function getComputedStyle(elem, name) {
			var c = (elem.ownerDocument || elem).defaultView;
			if (c && (c != name && forEach(c) == name)) {
				try {
					return c.frameElement;
				} catch (d) {}
			}
			return null;
		}

		function Event(elem) {
			return elem.nodeType ? setData((elem.ownerDocument || elem).defaultView, "ampdoc").getAmpDoc(elem) : elem;
		}

		function indexOf(type) {
			type = Event(type);
			return type.isSingleDoc() ? type.win : type;
		}

		function eventsApi(obj, methodName, name, rest, parent) {
			var old = hasKey(obj);
			var d = old[name];
			if (!d) {
				d = old[name] = {
					obj: null,
					promise: null,
					resolve: null
				};
			}
			if (!d.obj) {
				d.obj = parent ? new parent(methodName) : rest(methodName);
				if (d.resolve) {
					d.resolve(d.obj);
				}
			}
			return d.obj;
		}

		function when(result, id) {
			var div = successCallback(result, id);
			if (div) {
				return div;
			}
			result = hasKey(result);
			var text;
			var promise = new Promise(function(textAlt) {
				text = textAlt;
			});
			result[id] = {
				obj: null,
				promise: promise,
				resolve: text
			};
			return promise;
		}

		function successCallback(result, value) {
			if (result = hasKey(result)[value]) {
				if (value = result.promise) {
					return value;
				}
				if (result.obj) {
					return result.promise = Promise.resolve(result.obj);
				}
			}
			return null;
		}

		function hasKey(obj) {
			var terse = obj.services;
			if (!terse) {
				terse = obj.services = {};
			}
			return terse;
		}

		function format(number) {
			if (0 == arguments.length) {
				throw new TypeError("`CSS.escape` requires an argument.");
			}
			var r = String(number);
			var len = r.length;
			var i = -1;
			var ch;
			var str = "";
			var h = r.charCodeAt(0);
			for (; ++i < len;) {
				ch = r.charCodeAt(i);
				str = 0 == ch ? str + "\ufffd" : 1 <= ch && 31 >= ch || (127 == ch || (0 == i && (48 <= ch && 57 >= ch) || 1 == i && (48 <= ch && (57 >= ch && 45 == h)))) ? str + ("\\" + ch.toString(16) + " ") : 0 == i && (1 == len && 45 == ch) ? str + ("\\" + r.charAt(i)) : 128 <= ch || (45 == ch || (95 == ch || (48 <= ch && 57 >= ch || (65 <= ch && 90 >= ch || 97 <= ch && 122 >= ch)))) ? str + r.charAt(i) : str + ("\\" + r.charAt(i));
			}
			return str;
		}

		function addMutationObserver(elem, matcher, allow) {
			if (matcher(elem)) {
				allow();
			} else {
				var win = elem.ownerDocument.defaultView;
				if (win.MutationObserver) {
					var target = new win.MutationObserver(function() {
						if (matcher(elem)) {
							target.disconnect();
							allow();
						}
					});
					target.observe(elem, {
						childList: true
					});
				} else {
					var readyStateTimer = win.setInterval(function() {
						if (matcher(elem)) {
							win.clearInterval(readyStateTimer);
							allow();
						}
					}, 5)
				}
			}
		}

		function attach(context, element) {
			addMutationObserver(context.documentElement, function() {
				return !!context.body;
			}, element);
		}

		function findAll(type) {
			return new Promise(function(classesToRemove) {
				attach(type, classesToRemove);
			});
		}

		function sibling(elem, next) {
			var cur = elem;
			for (; cur; cur = cur.parentElement) {
				if (next(cur)) {
					return cur;
				}
			}
			return null;
		}

		function isDescendant(parent, child) {
			for (; parent; parent = parent.parentNode) {
				if (child(parent)) {
					return parent;
				}
			}
			return null;
		}

		function trigger(elem, name) {
			if (elem.closest) {
				return elem.closest(name);
			}
			name = name.toUpperCase();
			return sibling(elem, function(child) {
				return child.tagName == name;
			});
		}

		function traverseNode(dataAndEvents, fun) {
			var node = dataAndEvents.firstElementChild;
			for (; node; node = node.nextElementSibling) {
				if (fun(node)) {
					return node;
				}
			}
			return null;
		}

		function Element(el, next) {
			var excludes = [];
			el = el.firstElementChild;
			for (; el; el = el.nextElementSibling) {
				if (next(el)) {
					excludes.push(el);
				}
			}
			return excludes;
		}

		function fire(element, f) {
			element = element.lastElementChild;
			for (; element; element = element.previousElementSibling) {
				if (f(element)) {
					return element;
				}
			}
			return null;
		}

		function clean(el, s) {
			var ret = [];
			el = el.firstChild;
			for (; el; el = el.nextSibling) {
				if (s(el)) {
					ret.push(el);
				}
			}
			return ret;
		}

		function find(context) {
			try {
				return context.ownerDocument.querySelector(":scope"), true;
			} catch (b) {
				return false;
			}
		}

		function highlight(node, attribute) {
			if (null == doc) {
				doc = find(node);
			}
			return doc ? node.querySelector(":scope > [" + attribute + "]") : traverseNode(node, function(el) {
				return el.hasAttribute(attribute);
			});
		}

		function query(root) {
			if (null == doc) {
				doc = find(root);
			}
			return doc ? makeArray(root.querySelectorAll(":scope > [placeholder]")) : Element(root, function(elem) {
				return elem.hasAttribute("placeholder");
			});
		}

		function select(node) {
			var name = "template";
			if (null == doc) {
				doc = find(node);
			}
			if (doc) {
				return node.querySelector(":scope > " + name);
			}
			name = name.toUpperCase();
			return traverseNode(node, function(child) {
				return child.tagName == name;
			});
		}

		function send(data, url, name) {
			var key;
			var result;
			try {
				result = data.open(url, name, key);
			} catch (e) {
				call().error("DOM", "Failed to open url on target: ", name, e);
			}
			if (!result) {
				if (!("_top" == name)) {
					result = data.open(url, "_top");
				}
			}
		}

		function compile(api, val) {
			return api.CSS && api.CSS.escape ? api.CSS.escape(val) : format(val);
		}

		function sequence(tasks, parent, assert) {
			return all(tasks, parent, assert).then(function(express) {
				return error().assert(express, "Service %s was requested to be provided through %s, but %s is not loaded in the current page. To fix this problem load the JavaScript file for %s in this page.", parent, assert, assert, assert);
			});
		}

		function all(obj, data, className) {
			var buf = successCallback(obj, data);
			return buf ? buf : Promise.resolve().then(function() {
				return obj.ampExtendedElements && obj.ampExtendedElements[className] ? when(obj, data) : findAll(obj.document).then(function() {
					return obj.ampExtendedElements && obj.ampExtendedElements[className] ? when(obj, data) : null;
				});
			});
		}

		function equal(obj) {
			return createElement(obj, "timer");
		}

		function confirm(spec) {
			return data(spec, "documentInfo").get();
		}

		function getData(array) {
			var val = o.urls.cdn;
			return val.length > array.length ? false : 0 == array.lastIndexOf(val, 0);
		}

		function parseURL(url, dataAndEvents) {
			if (!config) {
				config = self.document.createElement("a");
				params = self.UrlCache || (self.UrlCache = Object.create(null));
			}
			var param = params[url];
			if (param) {
				return param;
			}
			config.href = url;
			if (!config.protocol) {
				config.href = config.href;
			}
			var location = {
				href: config.href,
				protocol: config.protocol,
				host: config.host,
				hostname: config.hostname,
				port: "0" == config.port ? "" : config.port,
				pathname: config.pathname,
				search: config.search,
				hash: config.hash,
				origin: null
			};
			if ("/" !== location.pathname[0]) {
				location.pathname = "/" + location.pathname;
			}
			if ("http:" == location.protocol && 80 == location.port || "https:" == location.protocol && 443 == location.port) {
				location.port = "";
				location.host = location.hostname;
			}
			location.origin = config.origin && "null" != config.origin ? config.origin : "data:" != location.protocol && location.host ? location.protocol + "//" + location.host : location.href;
			var name = location;
			return dataAndEvents ? name : params[url] = name;
		}

		function decode(s, path, opt_attributes) {
			if (!path) {
				return s;
			}
			var parts = s.split("#", 2);
			var e = parts[0].split("?", 2);
			var str = e[0] + (e[1] ? opt_attributes ? "?" + path + "&" + e[1] : "?" + e[1] + "&" + path : "?" + path);
			return str += parts[1] ? "#" + parts[1] : "";
		}

		function urlParse(url) {
			if ("string" == typeof url) {
				url = parseURL(url);
			}
			var id;
			if (!(id = "https:" == url.protocol || "localhost" == url.hostname)) {
				url = url.hostname;
				id = url.length - 10;
				id = 0 <= id && url.indexOf(".localhost", id) == id;
			}
			return id;
		}

		function read(callback, id, string) {
			string = void 0 === string ? "source" : string;
			error().assert(null != callback, "%s %s must be available", id, string);
			var action = callback;
			error().assert(urlParse(action) || /^(\/\/)/.test(action), '%s %s must start with "https://" or "//" or be relative and served from either https or from localhost. Invalid value: %s', id, string, action);
		}

		function normalize(b) {
			var out = Object.create(null);
			if (!b) {
				return out;
			}
			if (0 == b.indexOf("?") || 0 == b.indexOf("#")) {
				b = b.substr(1);
			}
			b = b.split("&");
			var bi = 0;
			for (; bi < b.length; bi++) {
				var data = b[bi];
				var headDelim = data.indexOf("=");
				var unlock;
				if (-1 != headDelim) {
					unlock = decodeURIComponent(data.substring(0, headDelim)).trim();
					data = decodeURIComponent(data.substring(headDelim + 1)).trim();
				} else {
					unlock = decodeURIComponent(data).trim();
					data = "";
				}
				if (unlock) {
					out[unlock] = data;
				}
			}
			return out;
		}

		function merge(r) {
			var x = r.indexOf("#");
			return -1 == x ? r : r.substring(0, x);
		}

		function generate(el) {
			if ("string" == typeof el) {
				el = parseURL(el);
			}
			var x = el.pathname.split("/")[1];
			return el.origin == o.urls.cdn || 0 == el.origin.indexOf("http://localhost:") && ("c" == x || "v" == x);
		}

		function next(el) {
			if ("string" == typeof el) {
				el = parseURL(el);
			}
			if (!generate(el)) {
				return el.href;
			}
			var p = el.pathname.split("/");
			var s = p[1];
			error().assert("c" == s || "v" == s, "Unknown path prefix in url %s", el.href);
			var v = p[2];
			var base = "s" == v ? "https://" + decodeURIComponent(p[3]) : "http://" + decodeURIComponent(v);
			error().assert(0 < base.indexOf("."), "Expected a . in origin %s", base);
			p.splice(1, "s" == v ? 3 : 2);
			p = base + p.join("/");
			s = (s = el.search) && "?" != s ? (s = s.replace(rSlash, "").replace(/^[?&]/, "")) ? "?" + s : "" : "";
			return p + s + (el.hash || "");
		}

		function makeModuleMap(url) {
			var file = parseURL(url);
			var target = normalize(file.search);
			error().assert(!("__amp_source_origin" in target), "Source origin is not allowed in %s", url);
		}

		function split(key) {
			return data(key, "viewer");
		}

		function func(result, key) {
			var o = root || (root = Object.create(null));
			if (key in o) {
				result = o[key];
			} else {
				var k = key;
				result = -1 != compare(result).indexOf(k) ? true : result.AMP_CONFIG && result.AMP_CONFIG.hasOwnProperty(k) ? Math.random() < result.AMP_CONFIG[k] : false;
				result = o[key] = result;
			}
			return result;
		}

		function reset(doc) {
			var oldconfig = doc.location.originalHash || doc.location.hash;
			if (oldconfig) {
				var e_chunked_amp = normalize(oldconfig)["e-chunked-amp"];
				if ("1" == e_chunked_amp) {
					return true;
				}
				if ("0" == e_chunked_amp) {
					return false;
				}
			}
			return func(doc, "chunked-amp");
		}

		function write(req, key, text, pluginName) {
			var obj = root || (root = Object.create(null));
			var buffer = compare(req);
			var check = -1 != buffer.indexOf(key) || key in obj && obj[key];
			var value = void 0 !== text ? text : !check;
			if (value != check && (value ? (buffer.push(key), obj[key] = true) : (buffer.splice(buffer.indexOf(key), 1), obj[key] = false), !pluginName)) {
				req._experimentCookie = null;
				key = buffer.join(",");
				obj = Date.now() + 15552E6;
				key = encodeURIComponent("AMP_EXP") + "=" + encodeURIComponent(key) + "; path=/; expires=" + (new Date(obj)).toUTCString();
				try {
					req.document.cookie = key;
				} catch (l) {}
			}
			return value;
		}

		function compare(req) {
			if (req._experimentCookie) {
				return req._experimentCookie;
			}
			var value;
			a: {
				b: {
					try {
						value = req.document.cookie;
						break b;
					} catch (h) {}
					value = void 0;
				}
				if (value) {
					value = value.split(";");
					var j = 0;
					for (; j < value.length; j++) {
						var part = value[j].trim();
						var index = part.indexOf("=");
						if (-1 != index && "AMP_EXP" == decodeURIComponent(part.substring(0, index).trim())) {
							value = decodeURIComponent(part.substring(index + 1).trim());
							break a;
						}
					}
				}
				value = null;
			}
			var data = value;
			return req._experimentCookie = data ? data.split(/\s*,\s*/g) : [];
		}

		function some() {
			var iterator = createTrigger();
			return function(fnc) {
				return setTimeout(fnc, iterator());
			};
		}

		function createTrigger() {
			var pow = 0;
			return function() {
				var d = Math.pow(1.5, pow++);
				var chunk;
				chunk = d * (chunk || 0.3) * Math.random();
				if (0.5 < Math.random()) {
					chunk *= -1;
				}
				d += chunk;
				return 1E3 * d;
			};
		}

		function addEvent(el, eventName, ev) {
			var d;
			var bubble = d || false;
			el.addEventListener(eventName, ev, bubble);
			return function() {
				if (el) {
					el.removeEventListener(eventName, ev, bubble);
				}
				el = ev = null;
			};
		}

		function addListener(el, type, listener, eventName) {
			function handler(event) {
				listener(event);
				fn();
			}
			var bubble = eventName || false;
			var fn;
			fn = function() {
				if (el) {
					el.removeEventListener(type, handler, bubble);
				}
				listener = handler = el = null;
			};
			el.addEventListener(type, handler, bubble);
			return fn;
		}

		function removeEvent(element, type, callback, id) {
			var res;
			var udataCur = new Promise(function(listener) {
				res = addListener(element, type, listener, callback);
			});
			return index(udataCur, res, void 0, id);
		}

		function complete(result, id) {
			var name;
			var restoreScript;
			if (result.complete || ("complete" == result.readyState || result.document && "complete" == result.document.readyState)) {
				return Promise.resolve(result);
			}
			var ret = new Promise(function(eventHandler, listener) {
				var tag = result.tagName;
				name = "AUDIO" === tag || "VIDEO" === tag ? addListener(result, "loadstart", eventHandler) : addListener(result, "load", eventHandler);
				if (tag) {
					restoreScript = addListener(result, "error", listener);
				}
			});
			ret = ret.then(function() {
				return result;
			}, args);
			return index(ret, name, restoreScript, id);
		}

		function args(image) {
			if (image = image.target) {
				if (image.src) {
					image = image.src;
				}
			}
			throw error().createError("Failed to load:", image);
		}

		function index(value, next, callback, id) {
			var promise;
			promise = void 0 === id ? value : equal(self).timeoutPromise(id || 0, value);
			if (next) {
				promise.then(next, next);
			}
			if (callback) {
				promise.then(callback, callback);
			}
			return promise;
		}

		function Node() {
			this.Ca = [];
		}

		function match(other, property, expectedNumberOfNonCommentArgs) {
			if (!object) {
				object = Object.create(null);
			}
			var method = object[property];
			if (!method || expectedNumberOfNonCommentArgs) {
				method = property;
				if (void 0 === other[property]) {
					var error = property.charAt(0).toUpperCase() + property.slice(1);
					a: {
						var i = 0;
						for (; i < codeSegments.length; i++) {
							var err = codeSegments[i] + error;
							if (void 0 !== other[err]) {
								error = err;
								break a;
							}
						}
						error = "";
					}
					var name = error;
					if (void 0 !== other[name]) {
						method = name;
					}
				}
				if (!expectedNumberOfNonCommentArgs) {
					object[property] = method;
				}
			}
			return method;
		}

		function setStyle(el, prop, value, unit) {
			if (prop = match(el.style, prop, void 0)) {
				el.style[prop] = unit ? value + unit : value;
			}
		}

		function _getStyle(el, prop) {
			if (prop = match(el.style, prop, void 0)) {
				return el.style[prop];
			}
		}

		function css(el, opt_attributes) {
			var p;
			for (p in opt_attributes) {
				setStyle(el, p, opt_attributes[p]);
			}
		}

		function win(e) {
			var value = false;
			if (void 0 === value) {
				value = "none" == _getStyle(e, "display");
			}
			setStyle(e, "display", value ? "" : "none");
		}

		function handle(win) {
			this.win = win;
			this.G = win.document;
			this.pa = match(this.G, "hidden", true);
			if (void 0 === this.G[this.pa]) {
				this.pa = null;
			}
			this.Nb = match(this.G, "visibilityState", true);
			if (void 0 === this.G[this.Nb]) {
				this.Nb = null;
			}
			this.hb = new Node;
			this.La = null;
			if (this.pa) {
				this.La = "visibilitychange";
				var indexOfEquals = this.pa.indexOf("Hidden");
				if (-1 != indexOfEquals) {
					this.La = this.pa.substring(0, indexOfEquals) + "Visibilitychange";
				}
			}
			this.Wc = this.Bc.bind(this);
			if (this.La) {
				this.G.addEventListener(this.La, this.Wc);
			}
			this.ya = null;
		}

		function getContext(name) {
			return extend(name, "documentState", handle);
		}

		function i(obj) {
			var ret = getKeys(obj).map(function(done) {
				return equal(obj).timeoutPromise(3E3, when(obj, done), "Render timeout waiting for service " + done + " to be ready.");
			});
			return Promise.all(ret);
		}

		function getKeys(obj) {
			var l = obj.document;
			return Object.keys(conditions).filter(function(i) {
				return l.querySelector(conditions[i]);
			});
		}

		function toJSON(key) {
			return data(key, "resources");
		}

		function loadStyleSheet(parent, type, callback, recurring, fn) {
			var nodes = insertAfter(parent, parent.head, type, recurring || false, fn || null);
			if (cleanup(parent, nodes)) {
				callback();
			} else {
				var poll = setInterval(function() {
					if (cleanup(parent, nodes)) {
						clearInterval(poll);
						callback();
					}
				}, 4)
			}
		}

		function insertAfter(node, el, t, precedingNode, style) {
			node = node.createElement("style");
			node.textContent = t;
			var archor = null;
			if (precedingNode) {
				node.setAttribute("amp-runtime", "");
				el.runtimeStyleElement = node;
			} else {
				if ("amp-custom" == style) {
					node.setAttribute("amp-custom", "");
					archor = el.lastChild;
				} else {
					node.setAttribute("amp-extension", style || "");
					archor = el.runtimeStyleElement;
				}
			}
			t = el;
			var elt = archor;
			if (elt) {
				if (elt.nextSibling) {
					t.insertBefore(node, elt.nextSibling);
				} else {
					t.appendChild(node);
				}
			} else {
				t.insertBefore(node, t.firstChild);
			}
			return node;
		}

		function drag(doc, dataAndEvents) {
			function drag() {
				css(doc.body, {
					opacity: 1,
					visibility: "visible",
					animation: "none"
				});
			}
			try {
				var key = doc.defaultView;
				getContext(key).onBodyAvailable(function() {
					if (!key.__AMP_BODY_VISIBLE) {
						key.__AMP_BODY_VISIBLE = true;
						if (dataAndEvents) {
							i(key).catch(function(results) {
								then(results);
								return [];
							}).then(function(newlines) {
								drag();
								if (0 < newlines.length) {
									toJSON(doc).schedulePass(1, true);
								}
								try {
									var node = createElement(key, "performance");
									node.tick("mbv");
									node.flush();
								} catch (h) {}
							});
						} else {
							drag();
						}
					}
				});
			} catch (restoreScript) {
				drag();
				then(restoreScript);
			}
		}

		function cleanup(i, event) {
			var resultItems = i.styleSheets;
			i = 0;
			for (; i < resultItems.length; i++) {
				var result = resultItems[i];
				if (result.ownerNode == event) {
					return true;
				}
			}
			return false;
		}

		function unfoldSoak(o) {
			unfoldSoak = some();
			return unfoldSoak(o);
		}

		function done(e, t) {
			if (self.console && (e || (e = Error("no error supplied")), !e.reported)) {
				e.reported = true;
				var p = t || e.associatedElement;
				if (p) {
					if (p.classList) {
						p.classList.add("-amp-error");
						if (fail().development) {
							p.classList.add("-amp-element-error");
							p.setAttribute("error-message", e.message);
						}
					}
				}
				if (e.messageArray) {
					(console.error || console.log).apply(console, e.messageArray);
				} else {
					if (p) {
						(console.error || console.log).call(console, p.tagName + "#" + p.id, e.message);
					} else {
						(console.error || console.log).call(console, e.message);
					}
				}
				if (p) {
					if (p.Qa) {
						p.Qa();
					}
				}
				template.call(void 0, void 0, void 0, void 0, void 0, e);
			}
		}

		function onComplete() {
			var that = self;
			that.onerror = template;
			that.addEventListener("unhandledrejection", function(error) {
				done(error.reason || Error("rejected promise " + error));
			});
		}

		function template(data, view, partials, helpers, args) {
			if (this) {
				if (this.document) {
					drag(this.document);
				}
			}
			if (!fail().development) {
				var restoreScript = false;
				try {
					restoreScript = getFile();
				} catch (k) {}
				if (!(restoreScript && 0.01 < Math.random())) {
					var result = render(data, view, partials, helpers, args, restoreScript);
					unfoldSoak(function() {
						if (result) {
							(new Image).src = result;
						}
					});
				}
			}
		}

		function render(attributes, url, data, parent, e, callback) {
			attributes = e && e.message ? e.message : attributes;
			if (!/_reported_/.test(attributes) && ("CANCELLED" != attributes && (attributes || (attributes = "Unknown error"), -1 == attributes.indexOf("Failed to load:")))) {
				callback = o.urls.errorReporting + "?v=" + encodeURIComponent("1478801557976") + "&noAmp=" + (callback ? 1 : 0) + "&m=" + encodeURIComponent(attributes.replace("\u200b\u200b\u200b", "")) + "&a=" + (0 <= attributes.indexOf("\u200b\u200b\u200b") ? 1 : 0);
				if (self.context) {
					if (self.context.location) {
						callback += "&3p=1";
					}
				}
				if (self.AMP_CONFIG) {
					if (self.AMP_CONFIG.canary) {
						callback += "&ca=1";
					}
				}
				if (self.location.ancestorOrigins) {
					if (self.location.ancestorOrigins[0]) {
						callback += "&or=" + encodeURIComponent(self.location.ancestorOrigins[0]);
					}
				}
				if (self.viewerState) {
					callback += "&vs=" + encodeURIComponent(self.viewerState);
				}
				if (self.parent) {
					if (self.parent != self) {
						callback += "&iem=1";
					}
				}
				if (self.AMP.viewer) {
					var encodedValue = self.AMP.viewer.getResolvedViewerUrl();
					var unmd = self.AMP.viewer.maybeGetMessagingOrigin();
					if (encodedValue) {
						callback += "&rvu=" + encodeURIComponent(encodedValue);
					}
					if (unmd) {
						callback += "&mso=" + encodeURIComponent(unmd);
					}
				}
				if (e) {
					callback += "&el=" + encodeURIComponent(e && e.associatedElement ? e.associatedElement.tagName : "u") + "&s=" + encodeURIComponent(e.stack || "");
					e.message += " _reported_";
				} else {
					callback += "&f=" + encodeURIComponent(url || "") + "&l=" + encodeURIComponent(data || "") + "&c=" + encodeURIComponent(parent || "");
				}
				callback += "&r=" + encodeURIComponent(self.document.referrer);
				callback += "&ae=" + encodeURIComponent(m.join(","));
				m.push(attributes);
				return callback += "&fr=" + encodeURIComponent(self.location.originalHash || self.location.hash);
			}
		}

		function getFile() {
			var codeSegments = self.document.querySelectorAll("script[src]");
			var i = 0;
			for (; i < codeSegments.length; i++) {
				if (!getData(codeSegments[i].src.toLowerCase())) {
					return true;
				}
			}
			return false;
		}

		function checkState(state) {
			return "loading" != state.readyState && "uninitialized" != state.readyState;
		}

		function restoreScript(elem) {
			return "complete" == elem.readyState;
		}

		function after(deepDataAndEvents, obj) {
			fadeIn(deepDataAndEvents, checkState, obj);
		}

		function fadeIn(el, callback, fn) {
			var value = callback(el);
			if (value) {
				fn(el);
			} else {
				var done = function() {
					if (callback(el)) {
						if (!value) {
							value = true;
							fn(el);
						}
						el.removeEventListener("readystatechange", done);
					}
				};
				el.addEventListener("readystatechange", done);
			}
		}

		function copyFiles(deepDataAndEvents) {
			return new Promise(function(walkers) {
				after(deepDataAndEvents, walkers);
			});
		}

		function wait(duration) {
			return new Promise(function(sqlt) {
				fadeIn(duration, restoreScript, sqlt);
			});
		}

		function SwapArrayValues(ordered) {
			return "fixed" == ordered || ("fixed-height" == ordered || ("responsive" == ordered || ("fill" == ordered || "flex-item" == ordered)));
		}

		function isNumber(val) {
			if ("number" == typeof val) {
				return val + "px";
			}
			if (val && /^\d+(\.\d+)?(px|em|rem|vh|vw|vmin|vmax|cm|mm|q|in|pc|pt)?$/.test(val)) {
				return /^\d+(\.\d+)?$/.test(val) ? val + "px" : val;
			}
		}

		function getValue(value) {
			error().assert(/^\d+(\.\d+)?(px|em|rem|vh|vw|vmin|vmax|cm|mm|q|in|pc|pt)$/.test(value), "Invalid length value: %s", value);
			return value;
		}

		function store(value) {
			error().assert(/^\d+(\.\d+)?(px|em|rem|vh|vw|vmin|vmax|%)$/.test(value), "Invalid length or percent value: %s", value);
			return value;
		}

		function iterator(value) {
			getValue(value);
			var text = error().assert(value.match(/[a-z]+/i), "Failed to read units from %s", value);
			return text[0];
		}

		function lookupIterator(value) {
			value = parseFloat(value);
			return isArrayLike(value) ? value : void 0;
		}

		function width(x, value, w, recurring) {
			return {
				left: x,
				top: value,
				width: w,
				height: recurring,
				bottom: value + recurring,
				right: x + w
			};
		}

		function onMove(obj, offset) {
			var left = Math.max(obj.left, offset.left);
			var right = Math.min(obj.left + obj.width, offset.left + offset.width);
			if (left <= right) {
				var currentOffset = Math.max(obj.top, offset.top);
				offset = Math.min(obj.top + obj.height, offset.top + offset.height);
				if (currentOffset <= offset) {
					return width(left, currentOffset, right - left, offset - currentOffset);
				}
			}
			return null;
		}

		function link(self, width, i) {
			return {
				top: self.top - self.height * i,
				bottom: self.bottom + self.height * i,
				left: self.left - self.width * width,
				right: self.right + self.width * width,
				width: self.width * (1 + 2 * width),
				height: self.height * (1 + 2 * i)
			};
		}

		function position(obj, x, y) {
			return 0 == x && 0 == y || 0 == obj.width && 0 == obj.height ? obj : width(obj.left + x, obj.top + y, obj.width, obj.height);
		}

		function nextTick(xs, fn) {
			var arrayOutput = [];
			var i = 0;
			for (; i < xs.length; i++) {
				var attributes = xs[i];
				if (!fn(attributes, i, xs)) {
					arrayOutput.push(attributes);
					xs.splice(i, 1);
					i--;
				}
			}
		}

		function getArea(obj) {
			return {
				left: obj.left,
				top: obj.top,
				width: obj.width,
				height: obj.height,
				bottom: obj.bottom,
				right: obj.right,
				x: obj.left,
				y: obj.top
			};
		}

		function keys(obj) {
			var ret = Object.create(null);
			var key;
			for (key in obj) {
				if (obj.hasOwnProperty(key)) {
					var val = obj[key];
					ret[key] = isString(val) ? keys(val) : val;
				}
			}
			return ret;
		}

		function each(fn) {
			return data(fn, "viewport");
		}

		function with_walkers(walkers) {
			return all(walkers, "access", "amp-access");
		}

		function validate(index, callback) {
			if (Pd) {
				promiseOrValue.then(callback);
			} else {
				var self = parseInt(index, "chunk", close);
				self.A.push(callback);
				self.ua();
			}
		}

		function close(result) {
			var p = this;
			this.hf = result;
			this.U = result.win;
			this.A = [];
			this.a = null;
			this.Sb = function() {
				return tick(p);
			};
			if (this.Rc = reset(this.U)) {
				if (!this.U.requestIdleCallback) {
					this.U.addEventListener("message", this.Sb);
				}
				when(indexOf(result), "viewer").then(function(v) {
					p.a = v;
					v.onVisibilityChanged(function() {
						if (v.isVisible()) {
							tick(p);
						}
					});
					if (v.isVisible()) {
						tick(p);
					}
				});
			}
		}

		function tick(options) {
			var cancelAnimationFrame = options.A.shift();
			if (!cancelAnimationFrame) {
				return false;
			}
			var c = Date.now();
			try {
				cancelAnimationFrame();
			} catch (d) {
				throw drag(self.document), d;
			} finally {
				if (options.A.length) {
					options.ua();
				}
			}
			return true;
		}

		function process() {
			var el = self;
			var index = 0;
			var p = el.performance;
			if (p) {
				if (p.timing) {
					if (p.timing.responseStart) {
						index = Date.now() - p.timing.responseStart;
					}
				}
			}
			var content = Math.max(1, 1E3 - index);
			equal(el).delay(function() {
				if (!checkState(el.document)) {
					var codeSegments = el.document.querySelectorAll('link[rel~="stylesheet"]');
					var self = {};
					var i = 0;
					for (; i < codeSegments.length; self = {
							newLink: self.newLink,
							media: self.media
						}, i++) {
						var node = codeSegments[i];
						self.newLink = node.cloneNode(false);
						self.media = node.media || "all";
						self.newLink.media = "not-matching";
						self.newLink.onload = function(self) {
							return function() {
								self.newLink.media = self.media;
							};
						}(self);
						self.newLink.setAttribute("i-amp-timeout", content);
						var parent = node.parentElement;
						parent.insertBefore(self.newLink, node);
						parent.removeChild(node);
					}
				}
			}, content);
		}

		function message(win) {
			var _this = this;
			this.win = win;
			this.fc = Date.now();
			this.Ta = [];
			this.c = this.a = null;
			this.nc = false;
			this.df = copyFiles(this.win.document).then(function() {});
			wait(win.document).then(function() {
				_this.tick("ol");
				_this.flush();
			});
		}

		function extract(self) {
			var collection = !self.a.hasBeenVisible();
			var length = collection ? -1 : self.fc;
			if (collection) {
				self.a.whenFirstVisible().then(function() {
					length = Date.now();
				});
			}
			apply(self).then(function() {
				if (collection) {
					var x = -1 < length ? Date.now() - length : 1;
					self.tickDelta("pc", x);
					if (self.a) {
						self.a.prerenderComplete({
							value: x
						});
					}
				} else {
					self.tick("pc");
					var j = Date.now() - length;
					if (self.a) {
						self.a.prerenderComplete({
							value: j
						});
					}
				}
				self.flush();
			});
		}

		function apply(args) {
			return args.df.then(function() {
				return Promise.all(args.c.getResourcesInViewport().map(function(dataAndEvents) {
					return dataAndEvents.loadedOnce();
				}));
			});
		}

		function _reset(options) {
			if (options.a) {
				if (options.a.isPerformanceTrackingOn()) {
					options.Ta.forEach(function(isXML) {
						options.a.tick(isXML);
					});
				}
				options.Ta.length = 0;
			}
		}

		function save(options) {
			apply(options).then(function() {
				var result = Object.create(null);
				var url = confirm(options.win.document).sourceUrl.replace(/#.*/, "");
				result.sourceUrl = url;
				options.c.get().forEach(function(el) {
					el = el.element;
					var partName = el.tagName.toLowerCase();
					if (partName) {
						if (result[partName]) {
							result[partName]++;
						} else {
							result[partName] = 1;
						}
					}
					if ("amp-ad" == partName) {
						if (el = "ad-" + el.getAttribute("type")) {
							if (result[el]) {
								result[el]++;
							} else {
								result[el] = 1;
							}
						}
					}
				});
				options.a.setFlushParams(result);
				options.flush();
			});
		}

		function on(el) {
			return createElement(el, "platform");
		}

		function Editor(Pos, inS) {
			this.da = Pos;
			this.s = inS;
			this.Kb = false;
			this.Ic = 0;
			this.cd = this.Fe.bind(this);
			this.bd = this.Ee.bind(this);
			this.ad = this.De.bind(this);
			this.$c = this.Ce.bind(this);
			this.da.addEventListener("touchstart", this.cd, true);
		}

		function cancel(event) {
			event.Kb = false;
			event.Ic = 0;
			event.da.removeEventListener("touchmove", event.bd, true);
			event.da.removeEventListener("touchend", event.ad, true);
			event.da.removeEventListener("touchcancel", event.$c, true);
		}

		function Tabs(opt_renderer) {
			this.ampdoc = opt_renderer;
			this.s = each(this.ampdoc);
			this.a = split(this.ampdoc);
			this.B = data(this.ampdoc, "history");
			var dojo = on(this.ampdoc.win);
			this.re = dojo.isIos() && dojo.isSafari();
			if (this.a.isIframed()) {
				if (this.a.isOvertakeHistory()) {
					this.Tb = this.ne.bind(this);
					this.ampdoc.getRootNode().addEventListener("click", this.Tb);
				}
			}
		}

		function fn(event, response, range, scopes, sendFn) {
			if (!event.defaultPrevented) {
				var target = trigger(event.target, "A");
				if (target && target.href) {
					emit(response).maybeExpandLink(target);
					var message = response.win;
					var a = parseURL(target.href);
					var l = "ftp:" == a.protocol;
					if (l) {
						send(message, target.href, "_blank");
						event.preventDefault();
					}
					var n = /^(https?|mailto):$/.test(a.protocol);
					if (sendFn) {
						if (!n) {
							send(message, target.href, "_top");
							event.preventDefault();
						}
					}
					if (a.hash) {
						var file = parseURL(message.location.href);
						var i = "" + a.origin + a.pathname + a.search;
						var last = "" + file.origin + file.pathname + file.search;
						if (i == last) {
							event.preventDefault();
							event = a.hash.slice(1);
							var newblock = null;
							if (event) {
								var result = compile(response.win, event);
								newblock = response.getRootNode().getElementById(event) || response.getRootNode().querySelector('a[name="' + result + '"]');
							}
							if (a.hash != file.hash) {
								message.location.replace("#" + event);
							}
							if (newblock) {
								range.scrollIntoView(newblock);
								equal(message).delay(function() {
									return range.scrollIntoView(newblock);
								}, 1);
							} else {
								call().warn("HTML", "failed to find element with id=" + event + " or a[name=" + event + "]");
							}
							if (a.hash != file.hash) {
								scopes.push(function() {
									message.location.replace("" + (file.hash || "#"));
								});
							}
						}
					}
				}
			}
		}

		function findListItems(parent) {
			if (void 0 === before) {
				before = !!Element.prototype.createShadowRoot;
			}
			return before && Node.prototype.getRootNode ? parent.getRootNode(element) : isDescendant(parent, function(qualifier) {
				return qualifier ? "I-AMP-SHADOW-ROOT" == qualifier.tagName ? true : 11 == qualifier.nodeType && "[object ShadowRoot]" === Object.prototype.toString.call(qualifier) : false;
			});
		}

		function Selection(win, dragHandles) {
			this.win = win;
			this.Eb = null;
			if (dragHandles) {
				this.Eb = new Player(win);
			}
		}

		function value(win) {
			this.win = win;
		}

		function Player(win) {
			var response = this;
			this.win = win;
			this.ae = this.win.document.body ? Promise.resolve(this.win.document.body) : findAll(this.win.document).then(function() {
				return response.getBody();
			});
			this.Ke = checkState(this.win.document) ? Promise.resolve() : copyFiles(this.win.document);
		}

		function promote() {
			var which = self;
			return setData(which, "ampdoc", function() {
				return new Selection(which, true);
			});
		}

		function $(win) {
			this.G = win.document;
			this.cc = win.document.head;
			this.Xa = {};
			this.Td = {};
			this.zb = on(win);
			this.Xa[parseURL(win.location.href).origin] = true;
			var node;
			a: {
				if (!fragment) {
					node = win.document.createElement("link").relList;
					if (!node || !node.supports) {
						node = {};
						break a;
					}
					fragment = {
						preconnect: node.supports("preconnect"),
						preload: node.supports("preload")
					};
				}
				node = fragment;
			}
			this.Xb = node;
			this.L = equal(win);
		}

		function getUrl(url) {
			return 0 == url.indexOf("https:") || 0 == url.indexOf("http:") ? true : false;
		}

		function search(elements, local, num) {
			if (!elements.Xb.preconnect) {
				if (elements.zb.isSafari()) {
					local.whenFirstVisible().then(function() {
						elements.Xa[num] = Date.now() + 18E4;
						var client = new XMLHttpRequest;
						client.open("HEAD", num + "/amp_preconnect_polyfill_404_or_other_error_expected._Do_not_worry_about_it?" + Math.random(), true);
						client.send();
					});
				}
			}
		}

		function group(behavior, context) {
			this.Dd = behavior;
			this.ke = context;
			this.a = null;
		}

		function lookup(me) {
			if (!me.a) {
				me.a = split(me.ke);
			}
			return me.a;
		}

		function once(element) {
			return createElement(element, "vsync");
		}

		function Item(element) {
			this.element = element;
			this.layout_ = "nodisplay";
			this.layoutWidth_ = -1;
			this.inViewport_ = false;
			this.win = element.ownerDocument.defaultView;
			this.actionMap_ = null;
			element = this.element;
			var oldconfig = extend(element.ownerDocument.defaultView, "preconnect", $);
			this.preconnect = new group(oldconfig, element);
			this.config = null;
		}

		function clone(dataAndEvents) {
			if (!dataAndEvents.actionMap_) {
				dataAndEvents.actionMap_ = dataAndEvents.win.Object.create(null);
			}
		}

		function tag(data) {
			Item.call(this, data);
			data = data.tagName.toLowerCase();
			if (!cache[data]) {
				cache[data] = true;
				createElement(this.win, "extensions").loadExtension(data, false);
			}
			tokens.push(this);
		}

		function load(id, prefix) {
			var assertions = id.split(",");
			error().assert(0 < assertions.length, "sizes has to have at least one size");
			var title = [];
			assertions.forEach(function(token) {
				token = token.replace(/\s+/g, " ").trim();
				if (0 != token.length) {
					var mediaQuery;
					var key;
					var k = token.charAt(token.length - 1);
					var i;
					var opt_keys = false;
					if (")" == k) {
						opt_keys = true;
						var m = 1;
						i = token.length - 2;
						for (; 0 <= i; i--) {
							var ch = token.charAt(i);
							if ("(" == ch) {
								m--;
							} else {
								if (")" == ch) {
									m++;
								}
							}
							if (0 == m) {
								break;
							}
						}
						var j = i - 1;
						if (0 < i) {
							i--;
							for (; 0 <= i && (ch = token.charAt(i), "%" == ch || ("-" == ch || ("_" == ch || ("a" <= ch && "z" >= ch || ("A" <= ch && "Z" >= ch || "0" <= ch && "9" >= ch))))); i--) {}
						}
						error().assert(i < j, 'Invalid CSS function in "%s"', token);
					} else {
						i = token.length - 2;
						for (; 0 <= i && (ch = token.charAt(i), "%" == ch || ("." == ch || ("a" <= ch && "z" >= ch || ("A" <= ch && "Z" >= ch || "0" <= ch && "9" >= ch)))); i--) {}
					}
					if (0 <= i) {
						mediaQuery = token.substring(0, i + 1).trim();
						key = token.substring(i + 1).trim();
					} else {
						key = token;
						mediaQuery = void 0;
					}
					title.push({
						mediaQuery: mediaQuery,
						size: opt_keys ? key : prefix ? store(key) : getValue(key)
					});
				}
			});
			return new Test(title);
		}

		function Test(ca) {
			error().assert(0 < ca.length, "SizeList must have at least one option");
			this.Gb = ca;
			var i = 0;
			for (; i < ca.length; i++) {
				var c = ca[i];
				if (i < ca.length - 1) {
					error().assert(c.mediaQuery, "All options except for the last must have a media condition");
				} else {
					error().assert(!c.mediaQuery, "The last option must not have a media condition");
				}
			}
		}

		function main(self) {
			if (!self.ampExtendedElements) {
				self.ampExtendedElements = {};
				if (!wrapped["amp-ad"]) {
					if (!wrapped["amp-embed"]) {
						require(self);
					}
				}
			}
			var codeSegments = self.document.querySelectorAll("[custom-element]");
			var i = 0;
			for (; i < codeSegments.length; i++) {
				var methodName = codeSegments[i].getAttribute("custom-element");
				self.ampExtendedElements[methodName] = true;
				if (!wrapped[methodName]) {
					define(self, methodName, tag);
				}
			}
			if (!self.document.body) {
				var user = getContext(self);
				user.onBodyAvailable(function() {
					return main(self);
				});
			}
		}

		function require(self) {
			self.ampExtendedElements["amp-ad"] = true;
			define(self, "amp-ad", tag);
			self.ampExtendedElements["amp-embed"] = true;
			define(self, "amp-embed", tag);
		}

		function method(self, methodName) {
			if (!wrapped[methodName]) {
				if (!self.ampExtendedElements) {
					self.ampExtendedElements = {};
				}
				self.ampExtendedElements[methodName] = true;
				define(self, methodName, tag);
			}
		}

		function invoke(type, methodName) {
			if (!type.ampExtendedElements) {
				type.ampExtendedElements = {};
				require(type);
			}
			type.ampExtendedElements[methodName] = true;
			define(type, methodName, wrapped[methodName] || tag);
		}

		function getElement(el) {
			var query = "string" == typeof el ? el : el.tagName;
			return query && 0 == query.toLowerCase().indexOf("i-") || el.tagName && (el.hasAttribute("placeholder") || (el.hasAttribute("fallback") || el.hasAttribute("overflow"))) ? true : false;
		}

		function concat(name, var_args) {
			function result(j) {
				return value.call(this, j);
			}
			var value = build(name);
			mixin(result, value);
			result.prototype.elementName = function() {
				return var_args;
			};
			return result;
		}

		function build(res) {
			function req(next) {
				next = value.call(this, next);
				next.createdCallback();
				return next;
			}
			if (res.BaseCustomElementClass) {
				return res.BaseCustomElementClass;
			}
			var value = res.HTMLElement;
			mixin(req, value);
			req.prototype.createdCallback = function() {
				this.Ub = false;
				this.readyState = "loading";
				this.everAttached = false;
				this.c = this.D = null;
				this.layout_ = "nodisplay";
				this.layoutWidth_ = -1;
				this.X = 0;
				this.jc = true;
				this.fa = false;
				this.mb = this.Fb = this.sb = void 0;
				this.bb = null;
				this.uc = this.sc = void 0;
				this.tc = this.ra = null;
				this.P = void 0;
				var val = wrapped[this.elementName()];
				this.implementation_ = new val(this);
				this.Ka = 1;
				this.oa = [];
				this.Ua = void 0;
			};
			req.prototype.elementName = function() {};
			req.prototype.getAmpDoc = function() {
				return this.D;
			};
			req.prototype.getResources = function() {
				return this.c;
			};
			req.prototype.isUpgraded = function() {
				return 2 == this.Ka;
			};
			req.prototype.upgrade = function(obj) {
				if (!this.Ua) {
					if (!(1 != this.Ka)) {
						this.implementation_ = new obj(this);
						if (this.everAttached) {
							this.Qd();
						}
					}
				}
			};
			req.prototype.Wb = function(regex) {
				this.Ka = 2;
				this.implementation_ = regex;
				this.classList.remove("amp-unresolved");
				this.classList.remove("-amp-unresolved");
				this.implementation_.createdCallback();
				if ("nodisplay" != this.layout_ && !this.implementation_.isLayoutSupported(this.layout_)) {
					throw error().createError("Layout not supported: " + this.layout_);
				}
				this.implementation_.layout_ = this.layout_;
				this.implementation_.layoutWidth_ = this.layoutWidth_;
				if (this.everAttached) {
					this.implementation_.firstAttachedCallback();
					this.Qa();
					this.getResources().upgraded(this);
				}
			};
			req.prototype.isBuilt = function() {
				return this.Ub;
			};
			req.prototype.getPriority = function() {
				this.isUpgraded();
				return this.implementation_.getPriority();
			};
			req.prototype.build = function() {
				if (!this.isBuilt()) {
					this.isUpgraded();
					try {
						this.implementation_.buildCallback();
						this.preconnect(false);
						this.Ub = true;
						this.classList.remove("-amp-notbuilt");
						this.classList.remove("amp-notbuilt");
					} catch (error) {
						throw done(error, this), error;
					}
					if (this.Ub) {
						if (this.fa) {
							this.Rd(true);
						}
					}
					if (this.oa) {
						if (0 < this.oa.length) {
							equal(this.ownerDocument.defaultView).delay(this.he.bind(this), 1);
						} else {
							this.oa = null;
						}
					}
					if (!this.getPlaceholder()) {
						var elem = this.createPlaceholder();
						if (elem) {
							this.appendChild(elem);
						}
					}
				}
			};
			req.prototype.preconnect = function(recurring) {
				var implementation_ = this;
				if (recurring) {
					this.implementation_.preconnectCallback(recurring);
				} else {
					equal(this.ownerDocument.defaultView).delay(function() {
						implementation_.implementation_.preconnectCallback(recurring);
					}, 1);
				}
			};
			req.prototype.isAlwaysFixed = function() {
				return this.implementation_.isAlwaysFixed();
			};
			req.prototype.updateLayoutBox = function(c) {
				var x = this;
				this.layoutWidth_ = c.width;
				if (this.isUpgraded()) {
					this.implementation_.layoutWidth_ = this.layoutWidth_;
				}
				this.implementation_.onLayoutMeasure();
				if (this.ob()) {
					if (this.fa) {
						this.fb(true);
					} else {
						if (1E3 > c.top) {
							if (0 <= c.top) {
								once(this.ownerDocument.defaultView).mutate(function() {
									if (x.ob()) {
										x.Ed();
									}
								});
							}
						}
					}
				}
			};
			req.prototype.applySizesAndMediaQuery = function() {
				if (void 0 === this.sb) {
					this.sb = this.getAttribute("media") || null;
				}
				if (this.sb) {
					var win = this.ownerDocument.defaultView;
					this.classList.toggle("-amp-hidden-by-media-query", !win.matchMedia(this.sb).matches);
				}
				if (void 0 === this.Fb) {
					var id = this.getAttribute("sizes");
					this.Fb = id ? load(id) : null;
				}
				if (this.Fb) {
					setStyle(this, "width", this.Fb.select(this.ownerDocument.defaultView));
				}
				if (void 0 === this.mb) {
					this.mb = (id = this.getAttribute("heights")) ? load(id, true) : null;
				}
				if (this.mb) {
					if ("responsive" === this.layout_) {
						if (this.bb) {
							setStyle(this.bb, "paddingTop", this.mb.select(this.ownerDocument.defaultView));
						}
					}
				}
			};
			req.prototype.changeSize = function(type, isXML) {
				if (this.bb) {
					setStyle(this.bb, "paddingTop", "0");
				}
				if (void 0 !== type) {
					setStyle(this, "height", type, "px");
				}
				if (void 0 !== isXML) {
					setStyle(this, "width", isXML, "px");
				}
			};
			req.prototype.connectedCallback = function() {
				if (!this.everAttached) {
					this.classList.add("-amp-element");
					this.classList.add("-amp-notbuilt");
					this.classList.add("amp-notbuilt");
				}
				if (void 0 === Je) {
					Je = "content" in self.document.createElement("template");
				}
				if (!Je) {
					if (!(void 0 !== this.Ua)) {
						this.Ua = !!trigger(this, "template");
					}
				}
				if (!this.Ua) {
					if (!this.D) {
						this.D = createElement(this.ownerDocument.defaultView, "ampdoc").getAmpDoc(this);
					}
					if (!this.c) {
						this.c = toJSON(this.D);
					}
					if (!this.everAttached) {
						if (!(this.implementation_ instanceof tag)) {
							this.Qd();
						}
						if (!this.isUpgraded()) {
							this.classList.add("amp-unresolved");
							this.classList.add("-amp-unresolved");
						}
						try {
							var className = this.getAttribute("layout");
							var val = this.getAttribute("width");
							var id = this.getAttribute("height");
							var orig = this.getAttribute("sizes");
							var path = this.getAttribute("heights");
							var key;
							if (className) {
								a: {
									var j;
									for (j in opts) {
										if (opts[j] == className) {
											key = opts[j];
											break a;
										}
									}
									key = void 0;
								}
							}
							else {
								key = null;
							}
							error().assert(void 0 !== key, "Unknown layout: %s", className);
							var FnConstructor = val && "auto" != val ? isNumber(val) : val;
							error().assert(void 0 !== FnConstructor, "Invalid width value: %s", val);
							var newValue = id ? isNumber(id) : null;
							error().assert(void 0 !== newValue, "Invalid height value: %s", id);
							var pdataOld;
							var value;
							var ordered;
							var A;
							if (!(A = key && ("fixed" != key && "fixed-height" != key) || FnConstructor && newValue)) {
								var tagName = this.tagName;
								tagName = tagName.toUpperCase();
								A = void 0 === styleCache[tagName];
							}
							if (A) {
								pdataOld = FnConstructor;
								value = newValue;
							} else {
								var text = this.tagName.toUpperCase();
								if (!styleCache[text]) {
									var doc = this.ownerDocument;
									var name = text.replace(/^AMP\-/, "");
									var p = doc.createElement(name);
									p.controls = true;
									css(p, {
										position: "absolute",
										visibility: "hidden"
									});
									doc.body.appendChild(p);
									styleCache[text] = {
										width: (p.offsetWidth || 1) + "px",
										height: (p.offsetHeight || 1) + "px"
									};
									doc.body.removeChild(p);
								}
								var style = styleCache[text];
								pdataOld = FnConstructor || "fixed-height" == key ? FnConstructor : style.width;
								value = newValue || style.height;
							}
							ordered = key ? key : pdataOld || value ? !value || pdataOld && "auto" != pdataOld ? value && (pdataOld && (orig || path)) ? "responsive" : "fixed" : "fixed-height" : "container";
							if (!("fixed" != ordered && ("fixed-height" != ordered && "responsive" != ordered))) {
								error().assert(value, "Expected height to be available: %s", id);
							}
							if ("fixed-height" == ordered) {
								error().assert(!pdataOld || "auto" == pdataOld, 'Expected width to be either absent or equal "auto" for fixed-height layout: %s', val);
							}
							if (!("fixed" != ordered && "responsive" != ordered)) {
								error().assert(pdataOld && "auto" != pdataOld, 'Expected width to be available and not equal to "auto": %s', val);
							}
							if ("responsive" == ordered) {
								error().assert(iterator(pdataOld) == iterator(value), "Length units should be the same for width and height: %s, %s", val, id);
							} else {
								error().assert(null === path, 'Unexpected "heights" attribute for none-responsive layout');
							}
							this.classList.add("-amp-layout-" + ordered);
							if (SwapArrayValues(ordered)) {
								this.classList.add("-amp-layout-size-defined");
							}
							if ("nodisplay" == ordered) {
								setStyle(this, "display", "none");
							} else {
								if ("fixed" == ordered) {
									css(this, {
										width: pdataOld,
										height: value
									});
								} else {
									if ("fixed-height" == ordered) {
										setStyle(this, "height", value);
									} else {
										if ("responsive" == ordered) {
											var clone = this.ownerDocument.createElement("i-amp-sizer");
											css(clone, {
												display: "block",
												paddingTop: lookupIterator(value) / lookupIterator(pdataOld) * 100 + "%"
											});
											this.insertBefore(clone, this.firstChild);
											this.bb = clone;
										} else {
											if ("fill" != ordered) {
												if ("container" != ordered) {
													if ("flex-item" == ordered) {
														if (pdataOld) {
															setStyle(this, "width", pdataOld);
														}
														if (value) {
															setStyle(this, "height", value);
														}
													}
												}
											}
										}
									}
								}
							}
							this.layout_ = ordered;
							if ("nodisplay" != this.layout_ && !this.implementation_.isLayoutSupported(this.layout_)) {
								throw error().createError("Layout not supported: " + this.layout_);
							}
							this.implementation_.layout_ = this.layout_;
							this.implementation_.firstAttachedCallback();
							this.isUpgraded();
							this.Qa();
						} catch (e) {
							done(e, this);
						}
						this.everAttached = true;
					}
					this.getResources().add(this);
				}
			};
			req.prototype.attachedCallback = function() {
				this.connectedCallback();
			};
			req.prototype.Qd = function() {
				var val = this;
				var r20 = this.implementation_;
				if (1 == this.Ka) {
					this.Ka = 4;
					var rreturn = r20.upgradeCallback();
					if (rreturn) {
						if ("function" == typeof rreturn.then) {
							rreturn.then(function(cx) {
								return val.Wb(cx);
							}).catch(function(results) {
								val.Ka = 3;
								then(results);
							});
						} else {
							this.Wb(rreturn);
						}
					} else {
						this.Wb(r20);
					}
				}
			};
			req.prototype.disconnectedCallback = function() {
				if (!this.Ua) {
					this.getResources().remove(this);
					this.implementation_.detachedCallback();
				}
			};
			req.prototype.detachedCallback = function() {
				this.disconnectedCallback();
			};
			req.prototype.dispatchCustomEvent = function(name, args) {
				args = args || {};
				var e = this.ownerDocument.defaultView.document.createEvent("Event");
				e.data = args;
				e.initEvent(name, true, true);
				this.dispatchEvent(e);
			};
			req.prototype.Qa = function() {};
			req.prototype.prerenderAllowed = function() {
				return this.implementation_.prerenderAllowed();
			};
			req.prototype.createPlaceholder = function() {
				return this.implementation_.createPlaceholderCallback();
			};
			req.prototype.renderOutsideViewport = function() {
				return this.implementation_.renderOutsideViewport();
			};
			req.prototype.getLayoutBox = function() {
				return this.getResources().getResourceForElement(this).getLayoutBox();
			};
			req.prototype.getIntersectionChangeEntry = function() {
				var length2 = this.implementation_.getIntersectionElementLayoutBox();
				var j = this.getResources().getResourceForElement(this).getOwner();
				var param = this.implementation_.getViewport().getRect();
				var m3 = j && j.getLayoutBox();
				j = length2;
				var suiteView = m3;
				var initial = param;
				var image = j;
				if (suiteView) {
					image = onMove(suiteView, j) || width(0, 0, 0, 0);
				}
				image = onMove(initial, image) || width(0, 0, 0, 0);
				suiteView = position(j, -initial.left, -initial.top);
				image = position(image, -initial.left, -initial.top);
				initial = position(initial, -initial.left, -initial.top);
				return {
					time: Date.now(),
					rootBounds: getArea(initial),
					boundingClientRect: getArea(suiteView),
					intersectionRect: getArea(image),
					intersectionRatio: image.width * image.height / (j.width * j.height)
				};
			};
			req.prototype.getResourceId = function() {
				return this.getResources().getResourceForElement(this).getId();
			};
			req.prototype.isRelayoutNeeded = function() {
				return this.implementation_.isRelayoutNeeded();
			};
			req.prototype.layoutCallback = function() {
				var obj = this;
				this.isBuilt();
				this.Qa();
				var promise = this.implementation_.layoutCallback();
				this.preconnect(true);
				this.classList.add("-amp-layout");
				return promise.then(function() {
					obj.readyState = "complete";
					obj.X++;
					obj.fb(false, true);
					if (obj.jc) {
						obj.implementation_.firstLayoutCompleted();
						obj.jc = false;
						obj.dispatchCustomEvent("amp:load:end");
					}
				}, function(dataAndEvents) {
					obj.X++;
					obj.fb(false, true);
					throw dataAndEvents;
				});
			};
			req.prototype.viewportCallback = function(deepDataAndEvents) {
				var pos = this;
				this.fa = deepDataAndEvents;
				if (0 == this.X) {
					if (deepDataAndEvents) {
						equal(this.ownerDocument.defaultView).delay(function() {
							if (0 == pos.X) {
								if (pos.fa) {
									pos.fb(true);
								}
							}
						}, 100);
					} else {
						this.fb(false);
					}
				}
				if (this.isBuilt()) {
					this.Rd(deepDataAndEvents);
				}
			};
			req.prototype.Rd = function(deepDataAndEvents) {
				this.implementation_.inViewport_ = deepDataAndEvents;
				this.implementation_.viewportCallback(deepDataAndEvents);
			};
			req.prototype.pauseCallback = function() {
				if (this.isBuilt()) {
					this.implementation_.pauseCallback();
				}
			};
			req.prototype.resumeCallback = function() {
				if (this.isBuilt()) {
					this.implementation_.resumeCallback();
				}
			};
			req.prototype.unlayoutCallback = function() {
				if (!this.isBuilt()) {
					return false;
				}
				var unlayoutCallback = this.implementation_.unlayoutCallback();
				if (unlayoutCallback) {
					this.X = 0;
					this.jc = true;
				}
				return unlayoutCallback;
			};
			req.prototype.unlayoutOnPause = function() {
				return this.implementation_.unlayoutOnPause();
			};
			req.prototype.collapse = function() {
				this.implementation_.collapse();
			};
			req.prototype.collapsedCallback = function(deepDataAndEvents) {
				this.implementation_.collapsedCallback(deepDataAndEvents);
			};
			req.prototype.enqueAction = function(attributes) {
				if (this.isBuilt()) {
					this.kd(attributes, false);
				} else {
					this.oa.push(attributes);
				}
			};
			req.prototype.he = function() {
				var core_indexOf = this;
				if (this.oa) {
					var asserterNames = this.oa;
					this.oa = null;
					asserterNames.forEach(function(qualifier) {
						core_indexOf.kd(qualifier, true);
					});
				}
			};
			req.prototype.kd = function(obj, recurring) {
				try {
					this.implementation_.executeAction(obj, recurring);
				} catch (rejected) {
					then("Action execution failed:", rejected, obj.target.tagName, obj.method);
				}
			};
			req.prototype.getRealChildNodes = function() {
				return clean(this, function(el) {
					return !getElement(el);
				});
			};
			req.prototype.getRealChildren = function() {
				return Element(this, function(el) {
					return !getElement(el);
				});
			};
			req.prototype.getPlaceholder = function() {
				return fire(this, function(element) {
					return element.hasAttribute("placeholder") && !("placeholder" in element);
				});
			};
			req.prototype.togglePlaceholder = function(recurring) {
				if (recurring) {
					var i = this.getPlaceholder();
					if (i) {
						i.classList.remove("amp-hidden");
					}
				} else {
					var codeSegments = query(this);
					i = 0;
					for (; i < codeSegments.length; i++) {
						codeSegments[i].classList.add("amp-hidden");
					}
				}
			};
			req.prototype.getFallback = function() {
				return highlight(this, "fallback");
			};
			req.prototype.toggleFallback = function(recurring) {
				this.classList.toggle("amp-notsupported", recurring);
				if (1 == recurring) {
					var dataAndEvents = this.getFallback();
					if (dataAndEvents) {
						this.getResources().scheduleLayout(this, dataAndEvents);
					}
				}
			};
			req.prototype.ob = function() {
				if (void 0 === this.sc) {
					this.sc = this.hasAttribute("noloading");
				}
				var key;
				if (!(key = this.sc)) {
					key = this.tagName.toUpperCase();
					key = !("AMP-AD" != key && "AMP-EMBED" != key || !func(this.ownerDocument.defaultView, "amp-ad-loading-ux") ? $cookies[key] : 1);
				}
				return key || (100 > this.layoutWidth_ || (0 < this.X || (getElement(this) || !SwapArrayValues(this.layout_)))) ? false : true;
			};
			req.prototype.Ed = function() {
				if (!this.ra) {
					var ownerDocument = this.ownerDocument;
					var dialog = ownerDocument.createElement("div");
					dialog.classList.add("-amp-loading-container");
					dialog.classList.add("-amp-fill-content");
					dialog.classList.add("amp-hidden");
					var title = ownerDocument.createElement("div");
					title.classList.add("-amp-loader");
					var h = 0;
					for (; 3 > h; h++) {
						var column = ownerDocument.createElement("div");
						column.classList.add("-amp-loader-dot");
						title.appendChild(column);
					}
					dialog.appendChild(title);
					this.appendChild(dialog);
					this.ra = dialog;
					this.tc = title;
				}
			};
			req.prototype.fb = function(recurring, dataAndEvents) {
				var item = this;
				if ((this.uc = recurring) || this.ra) {
					if (recurring && !this.ob()) {
						this.uc = false;
					} else {
						once(this.ownerDocument.defaultView).mutate(function() {
							var wrapper = item.uc;
							if (wrapper) {
								if (!item.ob()) {
									wrapper = false;
								}
							}
							if (wrapper) {
								item.Ed();
							}
							if (item.ra && (item.ra.classList.toggle("amp-hidden", !wrapper), item.tc.classList.toggle("amp-active", wrapper), !wrapper && dataAndEvents)) {
								var type = item.ra;
								item.ra = null;
								item.tc = null;
								item.getResources().deferMutate(item, function() {
									var t = type;
									if (t.parentElement) {
										t.parentElement.removeChild(t);
									}
								});
							}
						});
					}
				}
			};
			req.prototype.getOverflowElement = function() {
				if (void 0 === this.P) {
					if (this.P = highlight(this, "overflow")) {
						if (!this.P.hasAttribute("tabindex")) {
							this.P.setAttribute("tabindex", "0");
						}
						if (!this.P.hasAttribute("role")) {
							this.P.setAttribute("role", "button");
						}
					}
				}
				return this.P;
			};
			req.prototype.overflowCallback = function(recurring, isXML, deepDataAndEvents) {
				var elem = this;
				this.getOverflowElement();
				if (this.P) {
					this.P.classList.toggle("amp-visible", recurring);
					this.P.onclick = recurring ? function() {
						elem.getResources().changeSize(elem, isXML, deepDataAndEvents);
						once(elem.ownerDocument.defaultView).mutate(function() {
							elem.overflowCallback(false, isXML, deepDataAndEvents);
						});
					} : null;
				} else {
					if (recurring) {
						error().warn("CustomElement", "Cannot resize element and overflow is not available", this);
					}
				}
			};
			res.BaseCustomElementClass = req;
			return res.BaseCustomElementClass;
		}

		function define(self, name, str) {
			wrapped[name] = str;
			var value = concat(self, name);
			var recent = "customElements" in self;
			if (recent) {
				self.customElements.define(name, value);
			} else {
				self.document.registerElement(name, {
					prototype: value.prototype
				});
			}
		}

		function e(a) {
			error().assert(0 < a.length, "Srcset must have at least one source");
			this.I = a;
			var thisp = false;
			var details = false;
			a = 0;
			for (; a < this.I.length; a++) {
				var f = this.I[a];
				error().assert((f.width || f.dpr) && (!f.width || !f.dpr), "Either dpr or width must be specified");
				thisp = thisp || !!f.width;
				details = details || !!f.dpr;
			}
			error().assert(!thisp || !details, "Srcset cannot have both width and dpr sources");
			if (thisp) {
				this.I.sort(sortOrder);
			} else {
				this.I.sort(comp);
			}
			this.ef = thisp;
			this.ie = details;
		}

		function sortOrder(a, b) {
			error().assert(a.width != b.width, "Duplicate width: %s", a.width);
			return b.width - a.width;
		}

		function comp(a, b) {
			error().assert(a.dpr != b.dpr, "Duplicate dpr: %s", a.dpr);
			return b.dpr - a.dpr;
		}

		function c(options) {
			Item.call(this, options);
			this.rd = this.Ma = true;
			this.Md = this.J = null;
		}

		function success(m) {
			if (0 >= m.getLayoutWidth()) {
				return Promise.resolve();
			}
			var requestUrl = m.Md.select(m.getLayoutWidth(), m.getDpr()).url;
			if (requestUrl == m.J.getAttribute("src")) {
				return Promise.resolve();
			}
			m.J.setAttribute("src", requestUrl);
			return m.loadPromise(m.J).then(function() {
				if (!m.Ma) {
					if (m.J.classList.contains("-amp-ghost")) {
						m.getVsync().mutate(function() {
							m.J.classList.remove("-amp-ghost");
							m.toggleFallback(false);
						});
					}
				}
			});
		}

		function flip(args) {
			args.getVsync().mutate(function() {
				args.J.classList.add("-amp-ghost");
				args.toggleFallback(true);
			});
		}

		function proxy(self) {
			function c(m3) {
				Item.apply(this, arguments);
			}
			mixin(c, Item);
			c.prototype.getPriority = function() {
				return 1;
			};
			c.prototype.isLayoutSupported = function(dataAndEvents) {
				return "fixed" == dataAndEvents;
			};
			c.prototype.buildCallback = function() {
				this.element.setAttribute("aria-hidden", "true");
			};
			c.prototype.layoutCallback = function() {
				var d = this;
				win(this.element);
				var camelKey = this.element.getAttribute("src");
				return emit(this.getAmpDoc()).expandAsync(this.assertSource(camelKey)).then(function(src) {
					var im = new Image;
					im.src = src;
					d.element.appendChild(im);
				});
			};
			c.prototype.assertSource = function(key) {
				error().assert(/^(https\:\/\/|\/\/)/i.test(key), 'The <amp-pixel> src attribute must start with "https://" or "//". Invalid value: ' + key);
				return key;
			};
			define(self, "amp-pixel", c);
		}

		function SMSound(exports) {
			function self(data) {
				Item.call(this, data);
				this.o = null;
			}
			mixin(self, Item);
			self.prototype.isLayoutSupported = function(ordered) {
				return SwapArrayValues(ordered);
			};
			self.prototype.buildCallback = function() {
				this.o = this.element.ownerDocument.createElement("video");
				var poster = this.element.getAttribute("poster");
				if (!poster) {
					if (fail().development) {
						console.error('No "poster" attribute has been provided for amp-video.');
					}
				}
				this.o.setAttribute("playsinline", "");
				this.o.setAttribute("webkit-playsinline", "");
				this.o.setAttribute("preload", "none");
				this.propagateAttributes(["poster", "controls", "aria-label", "aria-describedby", "aria-labelledby"], this.o);
				this.forwardEvents(["play", "pause"], this.o);
				this.applyFillContent(this.o, true);
				this.element.appendChild(this.o);
				data(this.win.document, "video-manager").register(this);
			};
			self.prototype.viewportCallback = function(deepDataAndEvents) {
				this.element.dispatchCustomEvent("amp:video:visibility", {
					visible: deepDataAndEvents
				});
			};
			self.prototype.layoutCallback = function() {
				var self = this;
				this.o = this.o;
				if (!this.sd()) {
					return this.toggleFallback(true), Promise.resolve();
				}
				if (this.element.getAttribute("src")) {
					read(this.element.getAttribute("src"), this.element);
				}
				this.propagateAttributes(["src", "loop"], this.o);
				if (this.element.hasAttribute("preload")) {
					this.o.setAttribute("preload", this.element.getAttribute("preload"));
				} else {
					this.o.removeAttribute("preload");
				}
				this.getRealChildNodes().forEach(function(element) {
					if (self.o !== element) {
						if (element.getAttribute) {
							if (element.getAttribute("src")) {
								read(element.getAttribute("src"), element);
							}
						}
						self.o.appendChild(element);
					}
				});
				return this.loadPromise(this.o).then(function() {
					self.element.dispatchCustomEvent("load");
				});
			};
			self.prototype.pauseCallback = function() {
				if (this.o) {
					this.o.pause();
				}
			};
			self.prototype.sd = function() {
				return !!this.o.play;
			};
			self.prototype.supportsPlatform = function() {
				return this.sd();
			};
			self.prototype.isInteractive = function() {
				return this.element.hasAttribute("controls");
			};
			self.prototype.play = function() {
				this.o.play();
			};
			self.prototype.pause = function() {
				this.o.pause();
			};
			self.prototype.mute = function() {
				this.o.muted = true;
			};
			self.prototype.unmute = function() {
				this.o.muted = false;
			};
			self.prototype.showControls = function() {
				this.o.controls = true;
			};
			self.prototype.hideControls = function() {
				this.o.controls = false;
			};
			define(exports, "amp-video", self);
		}

		function base(var_args) {
			this.win = var_args;
			this.ld = {};
			this.ib = null;
		}

		function stringify(value, j, keepData, obj) {
			isArray(value, j).extension.elements[j] = {
				implementationClass: keepData,
				css: obj
			};
		}

		function debug(object, name) {
			var p = object.ld[name];
			if (!p) {
				p = {
					extension: {
						elements: {}
					},
					docFactories: [],
					shadowRootFactories: [],
					promise: void 0,
					resolve: void 0,
					reject: void 0,
					loaded: void 0,
					error: void 0,
					scriptPresent: void 0
				};
				object.ld[name] = p;
			}
			return p;
		}

		function isArray(obj, t) {
			if (!obj.ib) {
				call().error("extensions", "unknown extension for ", t);
			}
			return debug(obj, obj.ib || "_UNKNOWN_");
		}

		function promise(result) {
			if (!result.promise) {
				result.promise = result.loaded ? Promise.resolve(result.extension) : result.error ? Promise.reject(result.error) : new Promise(function(resolve, reject) {
					result.resolve = resolve;
					result.reject = reject;
				});
			}
			return result.promise;
		}

		function getModelPrefix() {
			var a = false;
			return o.urls.cdn;
		}

		function withinElement(event) {
			invoke(event, "amp-img");
			invoke(event, "amp-pixel");
			invoke(event, "amp-video");
		}

		function exports(opts) {
			inspect(opts);
			init(opts);
		}

		function request() {
			var res = self;
			equal(res).delay(function() {
				if (func(res, "cache-service-worker") && ("serviceWorker" in navigator && res.location.hostname === parseURL(o.urls.cdn).hostname)) {
					var prefix = getModelPrefix();
					var r = func(res, "cache-service-worker-kill");
					navigator.serviceWorker.register(prefix + "/sw" + (r ? "-kill" : "") + ".js").then(function(oldUrl) {
						call().info("cache-service-worker", "ServiceWorker registration successful: ", oldUrl);
					}, function(e) {
						call().error("cache-service-worker", "ServiceWorker registration failed: ", e);
					});
				}
			});
		}

		function loadScript() {
			var win = self;
			if (fail().development) {
				var myUrl = win.location.href;
				if (0 != myUrl.indexOf("about:")) {
					var script = win.document.createElement("script");
					script.src = o.urls.cdn + "/v0/validator.js";
					script.onload = function() {
						win.document.head.removeChild(script);
						amp.validator.validateUrlAndLog(myUrl, win.document, fail().filter);
					};
					win.document.head.appendChild(script);
				}
			}
		}

		function create() {
			var win = self;
			var fn;
			p = new Promise(function(tmp) {
				fn = tmp;
			});
			if (func(win, "alp")) {
				var evt = split(win.document);
				var e = evt.getParam("click");
				if (e) {
					if (0 != e.indexOf("https://")) {
						error().warn("IMPRESSION", "click fragment param should start with https://. Found ", e);
						fn();
					} else {
						if (win.location.hash) {
							win.location.hash = "";
						}
						evt.whenFirstVisible().then(function() {
							var oldconfig = createElement(win, "xhr").fetchJson(e, {
								credentials: "include",
								requireAmpResponseSourceOrigin: true
							}).then(function(data) {
								var url = data.location;
								if (data = data.tracking_url || url) {
									if (!generate(data)) {
										(new Image).src = data;
									}
								}
								if (url && win.history.replaceState) {
									data = win.location.href;
									url = parseURL(url);
									var path;
									url = normalize(url.search);
									var tagNameArr = [];
									for (path in url) {
										var events = url[path];
										if (null != events) {
											if (eachEvent(events)) {
												var i = 0;
												for (; i < events.length; i++) {
													var v = events[i];
													tagNameArr.push(encodeURIComponent(path) + "=" + encodeURIComponent(v));
												}
											} else {
												tagNameArr.push(encodeURIComponent(path) + "=" + encodeURIComponent(events));
											}
										}
									}
									path = tagNameArr.join("&");
									path = decode(data, path);
									win.history.replaceState(null, "", path);
								}
							});
							fn(equal(win).timeoutPromise(8E3, oldconfig, "timeout waiting for ad server response").catch(function() {}));
						});
					}
				} else {
					fn();
				}
			} else {
				fn();
			}
		}

		function Text(element) {
			this.element = element;
			this.win = element.ownerDocument.defaultView;
			this.compileCallback();
		}

		function copy(from_instance) {
			this.U = from_instance;
			this.eb = {};
			this.Lc = {};
			this.jb = void 0;
		}

		function byId(node) {
			var id;
			var element = node.getAttribute("template");
			id = element ? node.ownerDocument.getElementById(element) : select(node);
			error().assert(id, "Template not found for %s", node);
			error().assert("TEMPLATE" == id.tagName, 'Template element must be a "template" tag %s', id);
			return id;
		}

		function check(properties, options) {
			var value = options.__AMP_IMPL_;
			if (value) {
				return Promise.resolve(value);
			}
			value = error().assert(options.getAttribute("type"), "Type must be specified: %s", options);
			var match = options.__AMP_WAIT_;
			if (match) {
				return match;
			}
			match = enter(properties, options, value).then(function(ItemViewType) {
				var check = options.__AMP_IMPL_ = new ItemViewType(options);
				delete options.__AMP_WAIT_;
				return check;
			});
			return options.__AMP_WAIT_ = match;
		}

		function enter(value, name, url) {
			if (value.eb[url]) {
				return value.eb[url];
			}
			post(value, name, url);
			var text;
			name = new Promise(function(textAlt) {
				text = textAlt;
			});
			value.eb[url] = name;
			value.Lc[url] = text;
			return name;
		}

		function post(node, name, id) {
			if (!node.jb) {
				node.jb = node.U.Object.create(null);
				var codeSegments = node.U.document.querySelectorAll("script[custom-template]");
				var i = 0;
				for (; i < codeSegments.length; i++) {
					node.jb[codeSegments[i].getAttribute("custom-template")] = true;
				}
			}
			error().assert(node.jb[id], "Template must be declared for %s as <script custom-template=%s>", name, id);
		}

		function fireEvent(target, method, args, data, event) {
			this.target = target;
			this.method = method;
			this.args = args;
			this.source = data;
			this.event = event;
		}

		function Window(h) {
			this.ampdoc = h;
			this.Yb = defaults();
			this.h = once(h.win);
			this.addEvent("tap");
			this.addEvent("submit");
			this.addEvent("change");
		}

		function set(v, date, sel) {
			error().assert(false, "Action Error: " + v + (date ? " in [" + date.str + "]" : "") + (sel ? " on [" + sel + "]" : ""));
		}

		function callback(self, obj, text, attributes, index, setting, scope) {
			attributes = new fireEvent(obj, text, attributes, index, setting);
			if (self.Yb[attributes.method]) {
				self.Yb[attributes.method](attributes);
			} else {
				var j = obj.tagName.toLowerCase();
				if ("amp-" == j.substring(0, 4)) {
					if (obj.enqueAction) {
						obj.enqueAction(attributes);
					} else {
						set('Unrecognized AMP element "' + j + '". Did you forget to include it via <script custom-element>?', scope, obj);
					}
				} else {
					var handler = handlers[j];
					if (obj.id && "amp-" == obj.id.substring(0, 4) || handler && -1 != handler.indexOf(text)) {
						if (!obj.__AMP_ACTION_QUEUE__) {
							obj.__AMP_ACTION_QUEUE__ = [];
						}
						obj.__AMP_ACTION_QUEUE__.push(attributes);
					} else {
						set("Target element does not support provided action", scope, obj);
					}
				}
			}
		}

		function mkdir(i, path, express, prefix) {
			return error().assert(express, "Invalid action definition in %s: [%s] %s", path, i, prefix || "");
		}

		function action(i, cb, e, name, value) {
			if (void 0 !== value) {
				mkdir(i, cb, e.type == name && e.value == value, "; expected [" + value + "]");
			} else {
				mkdir(i, cb, e.type == name);
			}
			return e;
		}

		function Queue(concurrency) {
			this.C = concurrency;
			this.dc = -1;
		}

		function onSuccess(value, res) {
			var j = value.dc + 1;
			if (j >= value.C.length) {
				return {
					type: eventType,
					index: value.dc
				};
			}
			var c = value.C.charAt(j);
			if (-1 != " \t\n\r\f\v\u00a0\u2028\u2029".indexOf(c)) {
				j++;
				for (; j < value.C.length && -1 != " \t\n\r\f\v\u00a0\u2028\u2029".indexOf(value.C.charAt(j)); j++) {}
				if (j >= value.C.length) {
					return {
						type: eventType,
						index: j
					};
				}
				c = value.C.charAt(j);
			}
			if (res && (onLoad(c) || "." == c && (j + 1 < value.C.length && onLoad(value.C[j + 1])))) {
				var stateVal = "." == c;
				var i = j + 1;
				for (; i < value.C.length; i++) {
					var img = value.C.charAt(i);
					if ("." == img) {
						stateVal = true;
					} else {
						if (!onLoad(img)) {
							break;
						}
					}
				}
				value = value.C.substring(j, i);
				value = stateVal ? parseFloat(value) : parseInt(value, 10);
				j = i - 1;
				return {
					type: key,
					value: value,
					index: j
				};
			}
			if (-1 != ";:.()=,|!".indexOf(c)) {
				return {
					type: type,
					value: c,
					index: j
				};
			}
			if (-1 != "\"'".indexOf(c)) {
				i = -1;
				var k = j + 1;
				for (; k < value.C.length; k++) {
					if (value.C.charAt(k) == c) {
						i = k;
						break;
					}
				}
				if (-1 == i) {
					return {
						type: 0,
						index: j
					};
				}
				value = value.C.substring(j + 1, i);
				j = i;
				return {
					type: key,
					value: value,
					index: j
				};
			}
			i = j + 1;
			for (; i < value.C.length && -1 == " \t\n\r\f\x0B\u00a0\u2028\u2029;:.()=,|!\"'".indexOf(value.C.charAt(i)); i++) {}
			value = value.C.substring(j, i);
			j = i - 1;
			return {
				type: key,
				value: !res || "true" != value && "false" != value ? value : "true" == value,
				index: j
			};
		}

		function onLoad(col) {
			return "0" <= col && "9" >= col;
		}

		function me(e) {
			this.D = e;
			this.ec = null;
		}

		function createProxy(target) {
			triggerEvent(target, "submit", function(oUiArea) {
				oUiArea.getRootNode().addEventListener("submit", listener, true);
				return {};
			});
		}

		function listener(event) {
			if (!event.defaultPrevented) {
				var node = event.target;
				if (node && "FORM" == node.tagName) {
					var fields = node.elements;
					var id = 0;
					for (; id < fields.length; id++) {
						error().assert(!fields[id].name || "__amp_source_origin" != fields[id].name, "Illegal input name, %s found: %s", "__amp_source_origin", fields[id]);
					}
					id = node.getAttribute("action");
					var requestString = node.getAttribute("action-xhr");
					var method = (node.getAttribute("method") || "GET").toUpperCase();
					if ("GET" == method) {
						error().assert(id, "form action attribute is required for method=GET: %s", node);
						read(id, node, "action");
						error().assert(!getData(id), "form action should not be on AMP CDN: %s", node);
						makeModuleMap(id);
					} else {
						if ("POST" == method) {
							if (id) {
								event.preventDefault();
								error().assert(false, "form action attribute is invalid for method=POST: %s", node);
							}
							if (!requestString) {
								event.preventDefault();
								error().assert(false, "Only XHR based (via action-xhr attribute) submissions are support for POST requests. %s", node);
							}
						}
					}
					id = node.getAttribute("target");
					error().assert(id, "form target attribute is required: %s", node);
					error().assert("_blank" == id || "_top" == id, "form target=%s is invalid can only be _blank or _top: %s", id, node);
					if (requestString) {
						makeModuleMap(requestString);
					}
					var name = node.classList.contains("-amp-form");
					var names;
					if (names = name ? !node.hasAttribute("amp-novalidate") : !node.hasAttribute("novalidate")) {
						if (node.checkValidity) {
							if (!node.checkValidity()) {
								event.preventDefault();
							}
						}
					}
				}
			}
		}

		function Server(handlers, options, client) {
			var Ha = this;
			this.L = equal(handlers);
			this.oe = options;
			this.ge = client || 0;
			this.Z = -1;
			this.Ac = 0;
			this.ka = false;
			this.de = function() {
				return Ha.Ha();
			};
		}

		function initialize(options, map) {
			var self = this;
			this.ampdoc = options;
			this.win = options.win;
			this.kc = this.win.parent && this.win.parent != this.win;
			this.Ra = getContext(this.win);
			this.W = true;
			this.Dc = false;
			this.Wd = this.Ob = "visible";
			this.ia = 1;
			this.m = 0;
			this.Hd = new Node;
			this.hb = new Node;
			this.Xd = new Node;
			this.od = new Node;
			this.dd = new Node;
			this.ub = this.Fa = null;
			this.sa = [];
			this.u = {};
			this.Nc = this.Oc = this.wc = this.ea = this.Zd = null;
			this.cf = new Promise(function(theTitle) {
				self.Zd = theTitle;
			});
			if (map) {
				Object.assign(this.u, map);
			} else {
				if (this.win.name) {
					if (0 == this.win.name.indexOf("__AMP__")) {
						triggerHandler(this.win.name.substring(7), this.u);
					}
				}
				if (this.win.location.hash) {
					triggerHandler(this.win.location.hash, this.u);
				}
			}
			this.W = !parseInt(this.u.off, 10);
			this.Dc = !(!parseInt(this.u.history, 10) && !this.Dc);
			stop(this, this.u.visibilityState);
			this.ia = parseInt(this.u.prerenderSize, 10) || this.ia;
			this.m = parseInt(this.u.paddingTop, 10) || this.m;
			this.Ge = "1" === this.u.csi;
			this.pc = !this.kc && "1" == this.u.webview;
			this.qa = !(!(this.kc && (!this.win.AMP_TEST_IFRAME && (this.u.origin || (this.u.visibilityState || -1 != this.win.location.search.indexOf("amp_js_v")))) || this.pc) && options.isSingleDoc());
			this.$b = this.isVisible();
			this.Ra.onVisibilityChanged(this.Gd.bind(this));
			this.vc = this.qa ? equal(this.win).timeoutPromise(2E4, new Promise(function(theTitle) {
				self.wc = theTitle;
			})).catch(function(ok) {
				throw assert(ok);
			}) : null;
			this.tb = this.qa ? this.vc.catch(function(res) {
				done(assert(res));
			}) : null;
			var value;
			var isFunction;
			if (this.qa) {
				if (this.win.location.ancestorOrigins && !this.pc) {
					value = 0 < this.win.location.ancestorOrigins.length && hasClass(this, this.win.location.ancestorOrigins[0]);
					isFunction = Promise.resolve(value);
				} else {
					value = void 0;
					isFunction = new Promise(function(theTitle) {
						self.Nc = theTitle;
					});
				}
			} else {
				value = false;
				isFunction = Promise.resolve(false);
			}
			this.oc = isFunction;
			this.af = new Promise(function(callback) {
				if (self.isEmbedded()) {
					if (self.win.location.ancestorOrigins && 0 < self.win.location.ancestorOrigins.length) {
						callback(self.win.location.ancestorOrigins[0]);
					} else {
						equal(self.win).delay(function() {
							return callback("");
						}, 1E3);
						self.Oc = callback;
					}
				} else {
					callback("");
				}
			});
			this.Mb = this.isEmbedded() && ("referrer" in this.u && false !== value) ? this.u.referrer : this.win.document.referrer;
			this.Le = new Promise(function($timeout) {
				if (self.isEmbedded() && "referrer" in self.u) {
					self.oc.then(function(dataAndEvents) {
						if (dataAndEvents) {
							$timeout(self.u.referrer);
						} else {
							$timeout(self.win.document.referrer);
							if (self.Mb != self.win.document.referrer) {
								call().error("Viewer", "Untrusted viewer referrer override: " + self.Mb + " at " + self.ub);
								self.Mb = self.win.document.referrer;
							}
						}
					});
				} else {
					$timeout(self.win.document.referrer);
				}
			});
			this.Bb = merge(this.win.location.href || "");
			this.bf = new Promise(function(fn) {
				var compassResult = self.u.viewerUrl;
				if (self.isEmbedded() && compassResult) {
					self.oc.then(function(dataAndEvents) {
						if (dataAndEvents) {
							self.Bb = compassResult;
						} else {
							call().error("Viewer", "Untrusted viewer url override: " + compassResult + " at " + self.ub);
						}
						fn(self.Bb);
					});
				} else {
					fn(self.Bb);
				}
			});
			if (this.qa || this.u.click) {
				options = merge(this.win.location.href);
				if (options != this.win.location.href) {
					if (this.win.history.replaceState) {
						this.win.location.originalHash = this.win.location.hash;
						this.win.history.replaceState({}, "", options + "#-");
					}
				}
			}
			this.Gd();
			hide(this);
		}

		function hide(self) {
			if (self.isVisible()) {
				if (!self.ea) {
					self.ea = Date.now();
				}
				self.$b = true;
				self.Zd();
			}
			self.hb.fire();
		}

		function stop(event, target) {
			if (target) {
				var related = event.Ob;
				target = call().assertEnumValue(suiteView, target, "VisibilityState");
				if ("hidden" === target) {
					target = event.$b ? "inactive" : "prerender";
				}
				event.Wd = target;
				if (!!event.Ra.isHidden()) {
					if (!("visible" !== target && "paused" !== target)) {
						target = "hidden";
					}
				}
				event.Ob = target;
				if (related !== target) {
					hide(event);
				}
			}
		}

		function hasClass(self, className) {
			if (self.pc && /^www\.[.a-z]+$/.test(className)) {
				return trace.some(function(classNameFilter) {
					return classNameFilter.test(className);
				});
			}
			var e = parseURL(className);
			return "https:" != e.protocol ? false : trace.some(function(client) {
				return client.test(e.hostname);
			});
		}

		function resolve(obj, type, opt_attributes, recurring) {
			if (obj.Fa) {
				return obj.Fa(type, opt_attributes, recurring);
			}
			var part = null;
			var i = 0;
			for (; i < obj.sa.length; i++) {
				if (obj.sa[i].eventType == type) {
					part = obj.sa[i];
					break;
				}
			}
			if (part) {
				part.data = opt_attributes;
			} else {
				obj.sa.push({
					eventType: type,
					data: opt_attributes
				});
			}
			if (recurring) {
				return Promise.resolve();
			}
		}

		function resolveOne(results, opt_attributes) {
			if (results.tb) {
				results.tb.then(function() {
					if (results.Fa) {
						resolve(results, "broadcast", opt_attributes, false);
					}
				});
			}
		}

		function triggerHandler(data, cache) {
			data = normalize(data);
			var prop;
			for (prop in data) {
				cache[prop] = data[prop];
			}
		}

		function assert(result) {
			return result instanceof Error ? (result.message = "No messaging channel: " + result.message, result) : Error("No messaging channel: " + result);
		}

		function sendMessage(target, opt_attributes) {
			return triggerEvent(target, "viewer", function() {
				return new initialize(target, opt_attributes);
			});
		}

		function reduce(key) {
			var res = {};
			res["AMP.History"] = key;
			return res;
		}

		function Klass(a, b) {
			this.L = equal(a.win);
			this.b = b;
			this.j = 0;
			this.wa = [];
			this.F = [];
			this.b.setOnStackIndexUpdated(this.O.bind(this));
		}

		function exec(key, callback) {
			var success;
			var text;
			var promise = new Promise(function(a3, textAlt) {
				success = a3;
				text = textAlt;
			});
			key.F.push({
				callback: callback,
				resolve: success,
				reject: text
			});
			if (1 == key.F.length) {
				execute(key);
			}
			return promise;
		}

		function execute(event) {
			if (0 != event.F.length) {
				var deferred = event.F[0];
				var ret;
				try {
					ret = deferred.callback();
				} catch (error) {
					ret = Promise.reject(error);
				}
				ret.then(function(translation) {
					deferred.resolve(translation);
				}, function(e) {
					call().error("History", "failed to execute a task:", e);
					deferred.reject(e);
				}).then(function() {
					event.F.splice(0, 1);
					execute(event);
				});
			}
		}

		function update(options) {
			var jQuery = this;
			this.win = options;
			this.L = equal(options);
			options = this.win.history;
			this.xa = options.length - 1;
			if (options.state) {
				if (void 0 !== options.state["AMP.History"]) {
					this.xa = Math.min(options.state["AMP.History"], this.xa);
				}
			}
			this.j = this.xa;
			this.O = null;
			this.We = "state" in options;
			this.Ja = reduce(this.j);
			var prop;
			var require;
			if (options.pushState && options.replaceState) {
				this.xb = options.originalPushState || options.pushState.bind(options);
				this.Wa = options.originalReplaceState || options.replaceState.bind(options);
				prop = function(prop, callback, value) {
					jQuery.Ja = prop;
					jQuery.xb(prop, callback, value);
				};
				require = function(arg, callback, value) {
					jQuery.Ja = arg;
					if (void 0 !== value) {
						jQuery.Wa(arg, callback, value);
					} else {
						jQuery.Wa(arg, callback);
					}
				};
				if (!options.originalPushState) {
					options.originalPushState = this.xb;
				}
				if (!options.originalReplaceState) {
					options.originalReplaceState = this.Wa;
				}
			} else {
				prop = function(value) {
					jQuery.Ja = value;
				};
				require = function(arg) {
					jQuery.Ja = arg;
				};
			}
			this.Ie = prop;
			this.Ab = require;
			try {
				this.Ab(reduce(this.j));
			} catch (ex) {
				call().error("History", "Initial replaceState failed: " + ex.message);
			}
			options.pushState = this.pd.bind(this);
			options.replaceState = this.pe.bind(this);
			var srv = new Server(this.win, this.xe.bind(this), 50);
			this.Cd = function() {
				srv.schedule();
			};
			this.nd = function() {
				srv.schedule();
			};
			this.win.addEventListener("popstate", this.Cd);
			this.win.addEventListener("hashchange", this.nd);
		}

		function respond(event, next) {
			return event.Pb ? event.Pb.promise.then(next, next) : next();
		}

		function getAll(Deferred) {
			var success;
			var text;
			var promise = Deferred.L.timeoutPromise(500, new Promise(function(a3, textAlt) {
				success = a3;
				text = textAlt;
			}));
			Deferred.Pb = {
				promise: promise,
				resolve: success,
				reject: text
			};
			return promise;
		}

		function updateSegment(node, index) {
			if (0 >= index) {
				return Promise.resolve(node.j);
			}
			node.Ja = reduce(node.j - index);
			var allDataForNode = getAll(node);
			node.win.history.go(-index);
			return allDataForNode.then(function() {
				return Promise.resolve(node.j);
			});
		}

		function A(a) {
			this.a = a;
			this.j = 0;
			this.O = null;
			this.Ze = this.a.onHistoryPoppedEvent(this.ye.bind(this));
		}

		function change(el) {
			triggerEvent(el, "history", function(data) {
				var newState = sendMessage(data);
				newState = newState.isOvertakeHistory() || data.win.AMP_TEST_IFRAME ? new A(newState) : extend(data.win, "global-history-binding", update);
				return new Klass(data, newState);
			});
		}

		function events(elem) {
			this.N = elem.navigator;
		}

		function layout(options, path) {
			if (!options.N.userAgent) {
				return 0;
			}
			options = options.N.userAgent.match(path);
			return !options || 1 >= options.length ? 0 : parseInt(options[1], 10);
		}

		function Color(g) {
			this.g = g;
			this.Pd = Object.create(null);
		}

		function listen(el, opt_capt) {
			var self = this;
			this.win = el;
			this.He = opt_capt;
			this.B = [];
			this.Bd = new Node;
			this.fd = function(e) {
				if (e.target) {
					dispatch(self, e.target);
				}
			};
			this.ed = function() {
				equal(el).delay(function() {
					dispatch(self, self.win.document.activeElement);
				}, 500);
			};
			this.win.document.addEventListener("focus", this.fd, true);
			this.win.addEventListener("blur", this.ed);
		}

		function dispatch(self, el) {
			var t = Date.now();
			if (0 == self.B.length || self.B[self.B.length - 1].el != el) {
				self.B.push({
					el: el,
					time: t
				});
			} else {
				self.B[self.B.length - 1].time = t;
			}
			self.purgeBefore(t - self.He);
			self.Bd.fire(el);
		}

		function s(u, el, c) {
			var o = this;
			el.__AMP__RESOURCE = this;
			this.qe = u;
			this.element = el;
			this.debugid = el.tagName.toLowerCase() + "#" + u;
			this.hostWin = el.ownerDocument.defaultView;
			this.c = c;
			this.Rb = false;
			this.Ga = void 0;
			this.g = el.isBuilt() ? 1 : 0;
			this.X = 0;
			this.nb = false;
			this.K = width(-1E4, -1E4, 0, 0);
			this.gc = null;
			this.fa = this.mc = false;
			this.Da = null;
			this.Ec = void 0;
			this.vd = false;
			this.rb = null;
			this.te = new Promise(function(lvl) {
				o.rb = lvl;
			});
			this.Ia = false;
		}

		function get_mangled(keepData) {
			return keepData.__AMP__RESOURCE;
		}

		function handleError(result, recurring, error) {
			if (result.rb) {
				result.rb();
				result.rb = null;
			}
			result.Da = null;
			result.vd = true;
			result.g = recurring ? 4 : 5;
			if (!recurring) {
				return Promise.reject(error);
			}
		}

		function EventEmitter() {
			this.A = [];
			this.cb = {};
			this.td = this.ud = 0;
		}

		function connect(win) {
			var isIe;
			return !(isIe || on(win)).isIe() || bootstrap(win) ? null : new Promise(function($sanitize) {
				var b = Date.now() + 2E3;
				var readyStateTimer = win.setInterval(function() {
					var a = Date.now();
					var selectionRange = bootstrap(win);
					if (selectionRange || a > b) {
						win.clearInterval(readyStateTimer);
						$sanitize();
						if (!selectionRange) {
							call().error("ie-media-bug", "IE media never resolved");
						}
					}
				}, 10);
			});
		}

		function bootstrap(win) {
			var query = "(min-width: " + win.innerWidth + "px)" + (" AND (max-width: " + win.innerWidth + "px)");
			try {
				return win.matchMedia(query).matches;
			} catch (e) {
				return call().error("ie-media-bug", "IE matchMedia failed: ", e), true;
			}
		}

		function _init(win) {
			this.win = win;
			this.Tc = this.ze.bind(this);
			this.Uc = this.Ae.bind(this);
			this.Vc = this.Be.bind(this);
			this.be = this.yd.bind(this);
			this.ce = this.ve.bind(this);
			this.bc = "ontouchstart" in win || (void 0 !== win.navigator.maxTouchPoints && 0 < win.navigator.maxTouchPoints || void 0 !== win.DocumentTouch);
			this.Va = false;
			this.win.document.addEventListener("keydown", this.Tc);
			this.win.document.addEventListener("mousedown", this.Uc);
			this.ac = true;
			this.zd = 0;
			this.Ye = new Node;
			this.Ad = new Node;
			this.qc = new Node;
			if (this.bc) {
				this.ac = !this.bc;
				addListener(win.document, "mousemove", this.Vc);
			}
		}

		function spy(outstandingDataSize, endRow, expectedNumberOfNonCommentArgs, dataAndEvents) {
			var r = new Range(0, 0, outstandingDataSize, endRow, expectedNumberOfNonCommentArgs, dataAndEvents, 1, 1);
			return r.solveYValueFromXValue.bind(r);
		}

		function Range(x0, y0, x1, y1, x2, y2, x3, y3) {
			this.x0 = x0;
			this.y0 = y0;
			this.x1 = x1;
			this.y1 = y1;
			this.x2 = x2;
			this.y2 = y2;
			this.x3 = x3;
			this.y3 = y3;
		}

		function remove(selector) {
			if (!selector) {
				return null;
			}
			if ("string" == typeof selector) {
				if (-1 != selector.indexOf("cubic-bezier")) {
					var args = selector.match(/cubic-bezier\((.+)\)/);
					if (args && (args = args[1].split(",").map(parseFloat), 4 == args.length)) {
						var i = 0;
						for (; 4 > i; i++) {
							if (isNaN(args[i])) {
								return null;
							}
						}
						return spy(args[0], args[1], args[2], args[3]);
					}
					return null;
				}
				return easing_functions[selector];
			}
			return selector;
		}

		function defaultCompare() {}

		function Game(initialState, height) {
			this.Pa = initialState;
			this.h = height || once(self);
			this.jd = null;
			this.H = [];
		}

		function isValid(options, classNames, date, attr) {
			return (new Game(options)).setCurve(attr).add(0, classNames, 1).start(date);
		}

		function start(h, p, list, e, ui) {
			var o = this;
			this.h = h;
			this.Pa = p;
			this.H = [];
			p = 0;
			for (; p < list.length; p++) {
				var opts = list[p];
				this.H.push({
					delay: opts.delay,
					func: opts.func,
					duration: opts.duration,
					curve: opts.curve || e,
					started: false,
					completed: false
				});
			}
			this.je = ui;
			this.nf = this.mf = this.Hb = 0;
			this.ka = false;
			this.g = {};
			this.Fd = new Promise(function(lvl, i) {
				o.Pe = lvl;
				o.Me = i;
			});
			this.Od = this.h.createAnimTask(this.Pa, {
				mutate: this.Ve.bind(this)
			});
		}

		function end(node, recurring, mayParseLabeledStatementInstead) {
			if (node.ka) {
				node.ka = false;
				if (0 != mayParseLabeledStatementInstead) {
					if (1 < node.H.length) {
						node.H.sort(function(a, opts) {
							return a.delay + a.duration - (opts.delay + opts.duration);
						});
					}
					try {
						if (0 < mayParseLabeledStatementInstead) {
							var i = 0;
							for (; i < node.H.length; i++) {
								node.H[i].func(1, true);
							}
						} else {
							i = node.H.length - 1;
							for (; 0 <= i; i--) {
								node.H[i].func(0, false);
							}
						}
					} catch (e) {
						call().error("Animation", "completion failed: " + e, e);
						recurring = false;
					}
				}
				if (recurring) {
					node.Pe();
				} else {
					node.Me();
				}
			}
		}

		function Rect(a4, h, a3, y) {
			this.ampdoc = a4;
			this.h = h;
			this.hd = this.m = a3;
			this.Lb = y && a4.isSingleDoc();
			this.l = null;
			this.fe = 0;
			this.w = [];
		}

		function move(self, element, opt_attributes) {
			var attributes = null;
			var i = 0;
			for (; i < self.w.length; i++) {
				if (self.w[i].element == element) {
					attributes = self.w[i];
					break;
				}
			}
			if (attributes) {
				attributes.selectors.push(opt_attributes);
			} else {
				var value = "F" + self.fe++;
				element.setAttribute("i-amp-fixedid", value);
				element.__AMP_DECLFIXED = true;
				attributes = {
					id: value,
					element: element,
					selectors: [opt_attributes]
				};
				self.w.push(attributes);
			}
		}

		function play(self, element) {
			var k = 0;
			for (; k < self.w.length; k++) {
				if (self.w[k].element == element) {
					self.h.mutate(function() {
						setStyle(element, "top", "");
					});
					var id = self.w[k];
					self.w.splice(k, 1);
					return id;
				}
			}
		}

		function register(exports) {
			exports.w.sort(function(e, nodes) {
				return e.element.compareDocumentPosition(nodes.element) & 1 ? 1 : -1;
			});
		}

		function setup(self, node, deepDataAndEvents, obj) {
			var el = node.element;
			if (el.parentElement != self.l) {
				error().warn("FixedLayer", "In order to improve scrolling performance in Safari, we now move the element to a fixed positioning layer:", node.element);
				if (!node.placeholder) {
					setStyle(el, "pointer-events", "initial");
					node.placeholder = self.ampdoc.win.document.createElement("i-amp-fp");
					node.placeholder.setAttribute("i-amp-fixedid", node.id);
					setStyle(node.placeholder, "display", "none");
				}
				setStyle(el, "zIndex", "calc(" + (1E4 + deepDataAndEvents) + " + " + (obj.zIndex || 0) + ")");
				el.parentElement.replaceChild(node.placeholder, el);
				t(self).appendChild(el);
				if (!node.selectors.some(function(expr) {
						var url;
						a: {
							try {
								var matches = el.matches || (el.webkitMatchesSelector || (el.mozMatchesSelector || (el.msMatchesSelector || el.oMatchesSelector)));
								if (matches) {
									url = matches.call(el, expr);
									break a;
								}
							} catch (e) {
								call().error("FixedLayer", "Failed to test query match:", e);
							}
							url = false;
						}
						return url;
					})) {
					error().warn("FixedLayer", "Failed to move the element to the fixed position layer. This is most likely due to the compound CSS selector:", node.element);
					draw(self, node);
				}
			}
		}

		function draw(obj, node) {
			if (node.placeholder) {
				if (obj.ampdoc.contains(node.placeholder)) {
					if (obj.ampdoc.contains(node.element)) {
						if (_getStyle(node.element, "zIndex")) {
							setStyle(node.element, "zIndex", "");
						}
						node.placeholder.parentElement.replaceChild(node.element, node.placeholder);
					} else {
						node.placeholder.parentElement.removeChild(node.placeholder);
					}
				}
			}
		}

		function t(self) {
			if (!self.Lb || self.l) {
				return self.l;
			}
			self.l = self.ampdoc.win.document.createElement("body");
			self.l.id = "-amp-fixedlayer";
			css(self.l, {
				position: "absolute",
				top: 0,
				left: 0,
				height: 0,
				width: 0,
				pointerEvents: "none",
				overflow: "hidden",
				animation: "none",
				background: "none",
				border: "none",
				borderImage: "none",
				boxSizing: "border-box",
				boxShadow: "none",
				display: "block",
				float: "none",
				margin: 0,
				opacity: 1,
				outline: "none",
				padding: "none",
				transform: "none",
				transition: "none",
				visibility: "visible"
			});
			self.ampdoc.win.document.documentElement.appendChild(self.l);
			if (void 0 !== self.l.style.webkitAnimation) {
				self.l.style.webkitAnimation = "none";
			} else {
				if (void 0 !== self.l.style.WebkitAnimation) {
					self.l.style.WebkitAnimation = "none";
				}
			}
			return self.l;
		}

		function assign(lvalue, ca, value) {
			var i = 0;
			for (; i < ca.length; i++) {
				var c = ca[i];
				if (1 == c.type) {
					if ("*" != c.selectorText) {
						if ("fixed" == c.style.position) {
							value.push(c.selectorText);
						}
					}
				} else {
					if (4 == c.type) {
						assign(lvalue, c.cssRules, value);
					} else {
						if (12 == c.type) {
							assign(lvalue, c.cssRules, value);
						}
					}
				}
			}
		}

		function flatten(source, target) {
			return function(pos) {
				return source + (target - source) * pos;
			};
		}

		function errorHandler(next) {
			this.win = next;
			this.Qe = Promise.resolve();
			this.Xe = 0;
			this.Vb = {};
			this.Hb = Date.now();
		}

		function Class(var_args) {
			var eventProps = this;
			this.win = var_args;
			this.Qb = createElement(this.win, "ampdoc");
			this.Ra = getContext(this.win);
			this.Je = requestAnimationFrame(this);
			this.A = [];
			this.zc = [];
			this.Kc = [];
			this.yc = [];
			this.Z = false;
			this.xc = this.vb = null;
			this.Xc = this.Se.bind(this);
			this.Ha = new Server(this.win, this.Xc, 16);
			this.Hc = null;
			var cycle = this.Bc.bind(this);
			if (this.Qb.isSingleDoc()) {
				when(indexOf(this.Qb.getAmpDoc()), "viewer").then(function(bench) {
					eventProps.Hc = bench;
					bench.onVisibilityChanged(cycle);
				});
			} else {
				this.Ra.onVisibilityChanged(cycle);
			}
		}

		function changeClass(value, elem) {
			return value.Ra.isHidden() ? false : value.Hc ? value.Hc.isVisible() : elem ? (value = value.Qb.getAmpDoc(elem), split(value).isVisible()) : true;
		}

		function array_to_hash(a) {
			if (changeClass(a)) {
				a.Je(a.Xc);
			} else {
				a.Ha.schedule();
			}
		}

		function requestAnimationFrame(self) {
			var fn = self.win.requestAnimationFrame || self.win.webkitRequestAnimationFrame;
			if (fn) {
				return fn.bind(self.win);
			}
			var pos = 0;
			return function(funcToCall) {
				var start = Date.now();
				var d = Math.max(0, 16 - (start - pos));
				pos = start + d;
				self.win.setTimeout(funcToCall, d);
			};
		}

		function getNext(callback, basis) {
			try {
				callback(basis);
			} catch (restoreScript) {
				return then(restoreScript), false;
			}
			return true;
		}

		function step(config) {
			return setData(config, "vsync", function() {
				extend(config, "timer", errorHandler);
				return new Class(config);
			});
		}

		function go(name) {
			var ret;
			var win = name.win;
			var dojo = on(win);
			name = ret || split(name);
			var selectionRange = once(win);
			if (!!dojo.isIos()) {
				if (!!dojo.isSafari()) {
					if (!(8 < dojo.getMajorVersion())) {
						if (!("1" != name.getParam("b29185497"))) {
							new Promise(function($sanitize) {
								selectionRange.mutate(function() {
									setStyle(win.document.body, "bottom", "");
									selectionRange.mutate(function() {
										setStyle(win.document.body, "bottom", "0px");
										$sanitize();
									});
								});
							});
						}
					}
				}
			}
		}

		function f(n, p2, a) {
			var d = this;
			this.ampdoc = n;
			this.lb = this.ampdoc.win.document;
			this.b = p2;
			this.a = a;
			this.lf = this.va = this.ab = this.Ya = null;
			this.Fc = false;
			this.Cb = null;
			this.m = a.getPaddingTop();
			this.pf = this.pb = 0;
			this.L = equal(this.ampdoc.win);
			this.h = step(this.ampdoc.win);
			this.Gc = false;
			this.Id = 0;
			this.gd = new Node;
			this.S = new Node;
			this.Cc = this.gb = void 0;
			this.l = new Rect(this.ampdoc, this.h, this.m, this.b.requiresFixedLayerTransfer());
			this.ampdoc.whenReady().then(function() {
				return d.l.setup();
			});
			this.kf = this.Mc.bind(this);
			this.a.onViewportEvent(this.$e.bind(this));
			this.b.updatePaddingTop(this.m);
			this.b.onScroll(this.Te.bind(this));
			this.b.onResize(this.Oe.bind(this));
			this.onScroll(this.Ue.bind(this));
			this.T = false;
			this.a.onVisibilityChanged(this.Sd.bind(this));
			this.Sd();
			if (this.ampdoc.isSingleDoc()) {
				if (a.isEmbedded()) {
					if (func(this.ampdoc.win, "pan-y")) {
						setStyle(this.lb.documentElement, "touch-action", "pan-y");
					}
				}
			}
			if (this.ampdoc.isSingleDoc()) {
				if (func(this.ampdoc.win, "make-body-block")) {
					this.lb.documentElement.classList.add("-amp-make-body-block");
				}
			}
		}

		function setContent(element, content) {
			return (element = proceed(element)) && element.content != content ? (element.content = content, true) : false;
		}

		function proceed(e) {
			if (e.a.isIframed()) {
				return null;
			}
			if (void 0 === e.gb) {
				e.gb = e.lb.querySelector("meta[name=viewport]");
				if (e.gb) {
					e.Cc = e.gb.content;
				}
			}
			return e.gb;
		}

		function cast(value, scope, model, deepDataAndEvents) {
			value.l.updatePaddingTop(value.m, deepDataAndEvents);
			if (0 >= scope) {
				return Promise.resolve();
			}
			var callback = flatten(value.pb - value.m, 0);
			return isValid(value.ampdoc.getRootNode(), function(basis) {
				basis = callback(basis);
				value.l.transformMutate("translateY(" + basis + "px)");
			}, scope, model).thenAlways(function() {
				value.l.transformMutate(null);
			});
		}

		function up(self, a, x) {
			var opts = self.getSize();
			var pickWinTop = self.getScrollTop();
			var pickWinLeft = self.getScrollLeft();
			self.gd.fire({
				relayoutAll: a,
				top: pickWinTop,
				left: pickWinLeft,
				width: opts.width,
				height: opts.height,
				velocity: x
			});
		}

		function attachEvents(h2, a) {
			var self = this;
			this.win = h2;
			this.zb = on(h2);
			this.a = a;
			this.S = new Node;
			this.ja = new Node;
			this.Oa = function() {
				return self.S.fire();
			};
			this.Na = function() {
				return self.ja.fire();
			};
			if (this.win.document.defaultView) {
				attach(this.win.document, function() {
					var body = self.win.document.body;
					setStyle(body, "overflow", "visible");
					if (self.zb.isIos()) {
						if ("1" === self.a.getParam("webview")) {
							css(body, {
								overflowX: "hidden",
								overflowY: "visible"
							});
						}
					}
					if (func(self.win, "make-body-relative")) {
						css(body, {
							display: "block",
							position: "relative",
							overflowX: "hidden",
							overflowY: "visible"
						});
					}
				});
			}
		}

		function jQuery(context) {
			var doc = context.win.document;
			return doc.scrollingElement ? doc.scrollingElement : doc.body && context.zb.isWebKit() ? doc.body : doc.documentElement;
		}

		function resize(event, height) {
			var self = this;
			this.win = event;
			this.ampdoc = height;
			this.Ba = this.la = this.ba = null;
			this.ha = {
				x: 0,
				y: 0
			};
			this.S = new Node;
			this.ja = new Node;
			this.m = 0;
			copyFiles(this.win.document).then(function() {
				return self.Db();
			});
			this.win.addEventListener("resize", function() {
				return self.ja.fire();
			});
		}

		function _detect3DTransforms($scope, dataAndEvents) {
			if ($scope.la) {
				setStyle($scope.la, "transform", "translateY(" + (dataAndEvents - $scope.m) + "px)");
				$scope.la.scrollIntoView(true);
			}
		}

		function parse(execResult) {
			var hooks = this;
			this.win = execResult;
			this.win.document.documentElement.classList.add("-amp-ios-embed");
			this.M = this.win.document.createElement("i-amp-html-wrapper");
			this.S = new Node;
			this.ja = new Node;
			this.Oa = this.wb.bind(this);
			this.Na = function() {
				return hooks.ja.fire();
			};
			this.Kd = false;
			attach(this.win.document, this.Db.bind(this));
		}

		function onload(el) {
			return triggerEvent(el, "viewport", function(self) {
				var that = sendMessage(self);
				var failuresLink;
				if (failuresLink = self.isSingleDoc()) {
					failuresLink = self.win;
					var natural = that.getParam("viewportType") || "natural";
					if (on(failuresLink).isIos()) {
						if ("natural" == natural && that.isIframed() || fail(failuresLink).development) {
							natural = "natural-ios-embed";
						}
					}
					failuresLink = "natural-ios-embed" == natural;
				}
				failuresLink = failuresLink ? func(self.win, "ios-embed-wrapper") && 7 < on(self.win).getMajorVersion() ? new parse(self.win) : new resize(self.win, self) : new attachEvents(self.win, that);
				return new f(self, failuresLink, that);
			});
		}

		function run(args) {
			var self = this;
			this.ampdoc = args;
			this.win = args.win;
			this.a = sendMessage(args);
			this.W = this.a.isRuntimeOn();
			this.xd = this.win.devicePixelRatio || 1;
			this.Re = 0;
			this.c = [];
			this.Sc = 0;
			this.T = this.a.isVisible();
			this.ia = this.a.getPrerenderSize();
			this.kb = false;
			this.md = true;
			this.ea = -1;
			this.ta = true;
			this.Za = -1;
			this.rc = this.qb = 0;
			this.Ha = new Server(this.win, function() {
				return toggle(self);
			});
			this.$ = new EventEmitter;
			this.F = new EventEmitter;
			this.Zc = function(options) {
				var m = self.s.getRect();
				var child = options.resource.getLayoutBox();
				m = Math.floor((child.top - m.top) / m.height);
				if (Math.sign(m) != self.getScrollDirection()) {
					m *= 2;
				}
				m = Math.abs(m);
				return 10 * options.priority + m;
			};
			this.R = [];
			this.Aa = [];
			this.aa = [];
			this.ic = false;
			this.s = onload(this.ampdoc);
			this.h = step(this.win);
			this.Qc = new listen(this.win, 6E4);
			this.Pc = false;
			this.Yd = new Color(this.a.getVisibilityState());
			open(this, this.Yd);
			this.s.onChanged(function(p) {
				self.qb = Date.now();
				self.rc = p.velocity;
				self.ta = self.ta || p.relayoutAll;
				self.schedulePass();
			});
			this.s.onScroll(function() {
				self.qb = Date.now();
			});
			this.a.onVisibilityChanged(function() {
				if (-1 == self.ea) {
					if (self.a.isVisible()) {
						self.ea = Date.now();
					}
				}
				self.schedulePass();
			});
			this.a.onRuntimeState(function(W) {
				self.W = W;
				self.schedulePass(1);
			});
			this.Qc.onFocus(function(source) {
				runTest(self, source);
			});
			this.schedulePass();
			this.ampdoc.whenReady().then(function() {
				self.kb = true;
				removeClass(self);
				self.aa = null;
				var promise = connect(self.win);
				if (promise) {
					promise.then(function() {
						self.ta = true;
						self.schedulePass();
					});
				} else {
					self.ta = true;
				}
				self.schedulePass();
				finish(self);
			});
		}

		function finish(req) {
			var oldconfig = extend(req.win, "input", _init);
			oldconfig.onTouchDetected(function(container) {
				put(req, "amp-mode-touch", container);
			}, true);
			oldconfig.onMouseDetected(function(container) {
				put(req, "amp-mode-mouse", container);
			}, true);
			oldconfig.onKeyboardStateChanged(function(container) {
				put(req, "amp-mode-keyboard-active", container);
			}, true);
		}

		function put(num, name, event) {
			num.ampdoc.whenBodyAvailable().then(function(item) {
				num.h.mutate(function() {
					item.classList.toggle(name, event);
				});
			});
		}

		function addClass(obj, attributes, deepDataAndEvents, value) {
			deepDataAndEvents = void 0 === deepDataAndEvents ? false : deepDataAndEvents;
			value = void 0 === value ? true : value;
			if (obj.W) {
				if (obj.kb) {
					attributes.build();
					if (value) {
						if (!attributes.isBlacklisted()) {
							obj.schedulePass();
						}
					}
				} else {
					if (!attributes.element.isBuilt()) {
						if (!(deepDataAndEvents && -1 != obj.aa.indexOf(attributes))) {
							obj.aa.push(attributes);
							removeClass(obj, value);
						}
					}
				}
			}
		}

		function removeClass(s, value) {
			if (!s.ic) {
				try {
					s.ic = true;
					value = void 0 === value ? true : value;
					value = void 0 === value ? true : value;
					var c = 0;
					var n = 0;
					for (; n < s.aa.length; n++) {
						var o = s.aa[n];
						var index;
						if (!(index = s.kb)) {
							a: {
								var target = o.element;
								do {
									if (target.nextSibling) {
										index = true;
										break a;
									}
								} while (target = target.parentNode);
								index = false;
							}
						}
						if (index) {
							s.aa.splice(n--, 1);
							o.build();
							if (!o.isBlacklisted()) {
								c++;
							}
						}
					}
					if (value) {
						if (0 < c) {
							s.schedulePass();
						}
					}
				} finally {
					s.ic = false;
				}
			}
		}

		function removeListener(target, type) {
			var r2 = target.c.indexOf(type);
			if (-1 != r2) {
				target.c.splice(r2, 1);
			}
			type.pauseOnRemove();
			setState(target, type, true);
		}

		function toggle(options) {
			if (options.W) {
				options.T = options.a.isVisible();
				options.ia = options.a.getPrerenderSize();
				if (options.kb) {
					if (options.md) {
						options.md = false;
						options.a.postDocumentReady();
					}
				}
				var b = options.s.getSize();
				Date.now();
				options.Ha.cancel();
				options.Pc = false;
				options.Yd.setState(options.a.getVisibilityState());
			}
		}

		function animate(item) {
			var i = Date.now();
			var box = item.s.getRect();
			var d = item.s.getScrollHeight();
			var scrollTop = box.height / 10;
			var winH = box.height / 10;
			var contentBottom = 0.85 * d;
			var k = 0.01 > Math.abs(item.rc) && 500 < i - item.qb || 1E3 < i - item.qb;
			if (0 < item.Aa.length) {
				var resultItems = item.Aa;
				item.Aa = [];
				i = 0;
				for (; i < resultItems.length; i++) {
					resultItems[i]();
				}
			}
			if (0 < item.R.length) {
				var codeSegments = item.R;
				item.R = [];
				var top = -1;
				var assertions = [];
				i = 0;
				for (; i < codeSegments.length; i++) {
					var attributes = codeSegments[i];
					var n = attributes.resource;
					var rect = n.getLayoutBox();
					var scrollableRect = n.getInitialLayoutBox();
					var one_vw = attributes.newHeight - rect.height;
					var actionFound = false;
					if (0 != one_vw) {
						if (attributes.force || !item.T) {
							actionFound = true;
						} else {
							if (item.Qc.hasDescendantsOf(n.element)) {
								actionFound = true;
							} else {
								if (rect.bottom + Math.min(one_vw, 0) >= box.bottom - winH) {
									actionFound = true;
								} else {
									if (rect.bottom <= box.top + scrollTop) {
										if (k) {
											assertions.push(attributes);
										} else {
											item.R.push(attributes);
										}
										continue;
									} else {
										if (scrollableRect.bottom >= contentBottom || rect.bottom >= contentBottom) {
											actionFound = true;
										} else {
											if (!(0 > one_vw)) {
												attributes.resource.overflowCallback(true, attributes.newHeight, attributes.newWidth);
											}
										}
									}
								}
							}
						}
					}
					if (actionFound) {
						if (0 <= rect.top) {
							top = -1 == top ? rect.top : Math.min(top, rect.top);
						}
						attributes.resource.changeSize(attributes.newHeight, attributes.newWidth);
						attributes.resource.overflowCallback(false, attributes.newHeight, attributes.newWidth);
					}
					if (attributes.callback) {
						attributes.callback(actionFound);
					}
				}
				if (-1 != top) {
					walk(item, top);
				}
				if (0 < assertions.length) {
					item.h.run({
						measure: function(results) {
							results.scrollHeight = item.s.getScrollHeight();
							results.scrollTop = item.s.getScrollTop();
						},
						mutate: function(obj) {
							var top = -1;
							assertions.forEach(function(e) {
								var box = e.resource.getLayoutBox();
								top = -1 == top ? box.top : Math.min(top, box.top);
								e.resource.changeSize(e.newHeight, e.newWidth);
								if (e.callback) {
									e.callback(true);
								}
							});
							if (-1 != top) {
								walk(item, top);
							}
							var tarY = item.s.getScrollHeight();
							if (tarY > obj.scrollHeight) {
								item.s.setScrollTop(obj.scrollTop + (tarY - obj.scrollHeight));
							}
						}
					}, {});
				}
			}
		}

		function walk(object, b) {
			object.Za = -1 == object.Za ? b : Math.min(b, object.Za);
		}

		function runTest(a, name) {
			var key = sibling(name, function(name) {
				return !!get_mangled(name);
			});
			if (key) {
				name = get_mangled(key);
				var $cont = name.getPendingChangeSize();
				if (void 0 !== $cont) {
					getter(a, name, $cont.height, $cont.width, true);
				}
			}
		}

		function handler(item) {
			var k = Date.now();
			var i = item.ta;
			item.ta = false;
			var me = item.Za;
			item.Za = -1;
			var e = 0;
			var g = 0;
			var key = 0;
			for (; key < item.c.length; key++) {
				var attributes = item.c[key];
				if (!(0 != attributes.getState())) {
					if (!attributes.isBlacklisted()) {
						addClass(item, attributes, true, false);
					}
				}
				if (i || 1 == attributes.getState()) {
					attributes.applySizesAndMediaQuery();
					e++;
				}
				if (attributes.isMeasureRequested()) {
					g++;
				}
			}
			var failures;
			if (0 < e || (0 < g || (i || -1 != me))) {
				key = 0;
				for (; key < item.c.length; key++) {
					if (attributes = item.c[key], 0 != attributes.getState() && (!attributes.hasOwner() && (i || (1 == attributes.getState() || (attributes.isMeasureRequested() || -1 != me && attributes.getLayoutBox().bottom >= me))))) {
						var n = attributes.isDisplayed();
						attributes.measure();
						if (n) {
							if (!attributes.isDisplayed()) {
								if (!failures) {
									failures = [];
								}
								failures.push(attributes);
							}
						}
					}
				}
			}
			if (failures) {
				item.h.mutate(function() {
					failures.forEach(function(prev) {
						prev.unload();
						setState(item, prev);
					});
				});
			}
			i = item.s.getRect();
			var defs;
			defs = item.T ? link(i, 0.25, 2) : 0 < item.ia ? link(i, 0, item.ia - 1) : null;
			var tail = item.T ? link(i, 0.25, 0.25) : i;
			i = 0;
			for (; i < item.c.length; i++) {
				if (me = item.c[i], 0 != me.getState() && !me.hasOwner()) {
					var recurring = item.T && (me.isDisplayed() && me.overlaps(tail));
					me.setInViewport(recurring);
				}
			}
			if (defs) {
				i = 0;
				for (; i < item.c.length; i++) {
					me = item.c[i];
					if (!(2 != me.getState())) {
						if (!me.hasOwner()) {
							if (me.isDisplayed()) {
								if (me.overlaps(defs)) {
									add(item, me, true);
								}
							}
						}
					}
				}
			}
			if (item.T && (0 == item.$.getSize() && (0 == item.F.getSize() && k > item.$.getLastDequeueTime() + 5E3))) {
				var w = 0;
				k = 0;
				for (; k < item.c.length && !(i = item.c[k], 2 == i.getState() && (!i.hasOwner() && (i.isDisplayed() && (add(item, i, false), w++, 4 <= w)))); k++) {}
			}
		}

		function loop(self, b) {
			var now = Date.now();
			if (0 == self.$.getSize()) {
				if (-1 === self.ea) {
					return 0;
				}
				var RETRY_DELAY = 1E3 * b.priority;
				return Math.max(RETRY_DELAY - (now - self.ea), 0);
			}
			var closingAnimationTime = 0;
			self.$.forEach(function(a) {
				closingAnimationTime = Math.max(closingAnimationTime, Math.max(1E3 * (b.priority - a.priority), 0) - (now - a.startTime));
			});
			return closingAnimationTime;
		}

		function getter(obj, name, isXML, attributes, recurring, deepDataAndEvents) {
			if (name.hasBeenMeasured()) {
				clear(obj, name, isXML, attributes, recurring, deepDataAndEvents);
			} else {
				obj.h.measure(function() {
					name.measure();
					clear(obj, name, isXML, attributes, recurring, deepDataAndEvents);
				});
			}
		}

		function clear(node, res, value, arr, force, callback) {
			res.resetPendingChangeSize();
			var options = res.getLayoutBox();
			if (void 0 !== value && value != options.height || void 0 !== arr && arr != options.width) {
				options = null;
				var i = 0;
				for (; i < node.R.length; i++) {
					if (node.R[i].resource == res) {
						options = node.R[i];
						break;
					}
				}
				if (options) {
					options.newHeight = value;
					options.newWidth = arr;
					options.force = force || options.force;
					options.callback = callback;
				} else {
					node.R.push({
						resource: res,
						newHeight: value,
						newWidth: arr,
						force: force,
						callback: callback
					});
				}
				node.schedulePassVsync();
			} else {
				if (void 0 === value) {
					if (void 0 === arr) {
						call().error("Resources", "attempting to change size with undefined dimensions", res.debugid);
					}
				}
				if (callback) {
					callback(false);
				}
			}
		}

		function add(test, method, recurring, vec0) {
			if (0 != method.getState()) {
				method.isDisplayed();
			}
			if (test.T || "prerender" == test.a.getVisibilityState() && method.prerenderAllowed()) {
				if (method.isInViewport() || method.renderOutsideViewport()) {
					if (recurring) {
						test.ua(method, "L", 0, vec0 || 0, method.startLayout.bind(method));
					} else {
						test.ua(method, "P", 2, vec0 || 0, method.startLayout.bind(method));
					}
				}
			}
		}

		function parallel(item, tasks, recurring, clone) {
			var assertions = [];
			rename(item, tasks, clone, function(attributes) {
				if (0 != attributes.getState()) {
					assertions.push(attributes);
				}
			});
			if (0 < assertions.length) {
				assertions.forEach(function(event) {
					event.measure();
					if (2 == event.getState()) {
						if (event.isDisplayed()) {
							add(item, event, recurring, tasks.getPriority());
						}
					}
				});
			}
		}

		function removeNode(name, walkers, node, deepDataAndEvents) {
			var recurring = walkers.isInViewport() && deepDataAndEvents;
			rename(name, walkers, node, function(content) {
				content.setInViewport(recurring);
			});
		}

		function rename(node, obj, dest, callback) {
			dest.forEach(function(keys) {
				obj.element.contains(keys);
				report(node, keys, callback);
			});
		}

		function report(node, name, callback) {
			if (name.classList.contains("-amp-element")) {
				callback(get_mangled(name));
				if (name = name.getPlaceholder()) {
					report(node, name, callback);
				}
			} else {
				node = name.getElementsByClassName("-amp-element");
				var codeSegments = [];
				name = 0;
				for (; name < node.length; name++) {
					var key = node[name];
					var g = false;
					var i = 0;
					for (; i < codeSegments.length; i++) {
						if (codeSegments[i].contains(key)) {
							g = true;
							break;
						}
					}
					if (!g) {
						codeSegments.push(key);
						callback(get_mangled(key));
					}
				}
			}
		}

		function open(self, target) {
			function get() {
				self.c.forEach(function(res) {
					return res.resume();
				});
				update();
			}

			function complete() {
				self.c.forEach(function(prev) {
					prev.unload();
					setState(self, prev);
				});
				self.unselectText();
			}

			function onSuccess() {
				self.c.forEach(function(gridStore) {
					return gridStore.pause();
				});
			}

			function udataCur() {}

			function update() {
				var g = self.s.getSize();
				if (0 < g.height && 0 < g.width) {
					if (0 < self.Aa.length || 0 < self.R.length) {
						animate(self);
					}
					handler(self);
					g = Date.now();
					var visible = self.a.getVisibilityState();
					var result = -1;
					var callback = self.F.peek(self.Zc);
					for (; callback;) {
						result = loop(self, callback);
						if (16 < result) {
							break;
						}
						self.F.dequeue(callback);
						if (result = self.$.getTaskById(callback.id)) {
							callback = self.Ne.bind(self, callback);
							result.promise.then(callback, callback);
						} else {
							callback.promise = callback.callback("visible" == visible);
							callback.startTime = g;
							self.$.enqueue(callback);
							callback.promise.then(self.Nd.bind(self, callback, true), self.Nd.bind(self, callback, false)).catch(done);
						}
						callback = self.F.peek(self.Zc);
						result = -1;
					}
					if (0 <= result) {
						g = result;
					} else {
						g = 2 * (g - self.$.getLastDequeueTime());
						g = Math.max(Math.min(3E4, g), 5E3);
					}
					if (0 < self.Aa.length || 0 < self.R.length) {
						g = Math.min(g, 500);
					}
					if (self.T) {
						self.schedulePass(g);
					}
				}
			}
			var customEvent = "prerender";
			var one = "hidden";
			var that = "paused";
			var source = "inactive";
			target.addTransition(customEvent, customEvent, update);
			target.addTransition(customEvent, "visible", update);
			target.addTransition(customEvent, one, update);
			target.addTransition(customEvent, source, update);
			target.addTransition(customEvent, that, update);
			target.addTransition("visible", "visible", update);
			target.addTransition("visible", one, update);
			target.addTransition("visible", source, complete);
			target.addTransition("visible", that, onSuccess);
			target.addTransition(one, "visible", update);
			target.addTransition(one, one, update);
			target.addTransition(one, source, complete);
			target.addTransition(one, that, onSuccess);
			target.addTransition(source, "visible", get);
			target.addTransition(source, one, get);
			target.addTransition(source, source, udataCur);
			target.addTransition(source, that, update);
			target.addTransition(that, "visible", get);
			target.addTransition(that, one, update);
			target.addTransition(that, source, complete);
			target.addTransition(that, that, udataCur);
		}

		function setState(item, event, deep) {
			if (1 == event.getState()) {
				item.F.purge(function(req) {
					return req.resource == event;
				});
				item.$.purge(function(req) {
					return req.resource == event;
				});
				nextTick(item.R, function(response) {
					return response.resource != event;
				});
			}
			if (0 == event.getState() && (deep && item.aa)) {
				var which = item.aa.indexOf(event);
				if (-1 != which) {
					item.aa.splice(which, 1);
				}
			}
		}

		function check_kb(events) {
			return eachEvent(events) ? events : [events];
		}

		function Map(c) {
			this.$d = parseInt(c, "action", Window);
			this.c = parseInt(c, "resources", run);
			this.$d.addGlobalMethodHandler("hide", this.handleHide.bind(this));
		}

		function val(value, a, s) {
			this.ampdoc = value;
			this.a = a;
			this.b = s;
			this.yb = parseURL(next(this.ampdoc.win.location)).origin;
			this.Ib = null;
		}

		function poll(event) {
			if (!event.Ib) {
				event.Ib = event.b.loadBlob(event.yb).then(function(base64) {
					return base64 ? JSON.parse(atob(base64)) : {};
				}).catch(function(e) {
					call().error("Storage", "Failed to load store: ", e);
					return {};
				}).then(function(val) {
					return new Assertion(val);
				});
			}
			return event.Ib;
		}

		function destroy(self, fn) {
			return poll(self).then(function(s) {
				fn(s);
				s = btoa(JSON.stringify(s.obj));
				return self.b.saveBlob(self.yb, s);
			}).then(self.ee.bind(self));
		}

		function filterWithQueryAndMatcher(b) {
			b.a.onBroadcast(function(a) {
				if ("amp-storage-reset" == a.type) {
					if (a.origin == b.yb) {
						b.Ib = null;
					}
				}
			});
		}

		function Assertion(obj, negate) {
			this.obj = keys(obj);
			this.ue = negate || 8;
			this.ca = this.obj.vv || Object.create(null);
			if (!this.obj.vv) {
				this.obj.vv = this.ca;
			}
		}

		function Table(win) {
			this.win = win;
			if (!(this.lc = "localStorage" in this.win)) {
				call().error("Storage", "localStorage not supported.");
			}
		}

		function x(a) {
			this.a = a;
		}

		function _track(item) {
			triggerEvent(item, "storage", function() {
				var iq = split(item);
				var index = parseInt(iq.getParam("storage"), 10);
				var actual = index ? new x(iq) : new Table(item.win);
				return (new val(item, iq, actual)).Jc();
			});
		}

		function Session(data) {
			this.D = data;
			this.Sa = null;
			this.Jd = false;
		}

		function show(e, self) {
			addEvent(self.video.element, "amp:video:visibility", function() {
				self.updateVisibility();
			});
			if (!e.Jd) {
				var cycle = function() {
					var r = 0;
					for (; r < e.Sa.length; r++) {
						e.Sa[r].updateVisibility();
					}
				};
				var opts = each(e.D);
				opts.onScroll(cycle);
				opts.onChanged(cycle);
				e.Jd = true;
			}
		}

		function completed(element, e) {
			var suiteView = this;
			this.D = element;
			this.video = e;
			this.jf = null;
			this.Ud = this.ga = this.wd = false;
			this.h = once(element.win);
			this.Yc = install.bind(null, element.win, fail(element.win).lite);
			element = e.element;
			this.Zb = element.hasAttribute("autoplay");
			removeEvent(element, "load").then(function() {
				suiteView.updateVisibility();
				suiteView.wd = true;
				if (suiteView.ga) {
					if (suiteView.Zb) {
						isUndefined(suiteView);
					}
				}
			});
			this.updateVisibility();
			if (this.Zb) {
				filter(this);
			}
		}

		function filter(_this) {
			if (_this.video.isInteractive()) {
				_this.video.hideControls();
			}
			_this.Yc().then(function(dataAndEvents) {
				if (!dataAndEvents && _this.video.isInteractive()) {
					_this.video.showControls();
				} else {
					_this.video.mute();
					if (_this.video.isInteractive()) {
						initUI(_this);
					}
				}
			});
		}

		function initUI(_this) {
			function update(event) {
				_this.h.mutate(function() {
					childElement.classList.toggle("amp-video-eq-play", event);
				});
			}

			function success() {
				this.Ud = true;
				this.video.showControls();
				this.video.unmute();
				response();
				compiled();
				hideLoadMask();
				childElement.remove();
				b.remove();
			}
			_this.video.hideControls();
			var childElement = loaded(_this);
			var b = createFrame(_this);
			_this.h.mutate(function() {
				_this.video.element.appendChild(childElement);
				_this.video.element.appendChild(b);
			});
			var response = addEvent(b, "click", success.bind(_this));
			var compiled = addEvent(_this.video.element, "pause", update.bind(_this, false));
			var hideLoadMask = addEvent(_this.video.element, "play", update.bind(_this, true));
		}

		function isUndefined(obj) {
			if (!obj.Ud) {
				if (split(obj.D).isVisible()) {
					obj.Yc().then(function(dataAndEvents) {
						if (dataAndEvents) {
							if (obj.ga) {
								obj.video.play(true);
							} else {
								obj.video.pause();
							}
						}
					});
				}
			}
		}

		function loaded(browser) {
			var doc = browser.D.win.document;
			var fragment = doc.createElement("i-amp-video-eq");
			fragment.classList.add("amp-video-eq");
			var tp = 1;
			for (; 4 >= tp; tp++) {
				var el = doc.createElement("div");
				el.classList.add("amp-video-eq-col");
				var j = 1;
				for (; 2 >= j; j++) {
					var elWrapper = doc.createElement("div");
					elWrapper.classList.add("amp-video-eq-" + tp + "-" + j);
					el.appendChild(elWrapper);
				}
				fragment.appendChild(el);
			}
			browser = on(browser.D.win);
			if (browser.isSafari()) {
				if (browser.isIos()) {
					fragment.setAttribute("unpausable", "");
				}
			}
			return fragment;
		}

		function createFrame(obj) {
			obj = obj.D.win.document.createElement("i-amp-video-mask");
			obj.classList.add("-amp-fill-content");
			return obj;
		}

		function install(win, decEndpoints) {
			if (instance) {
				return instance;
			}
			if (decEndpoints) {
				return instance = Promise.resolve(false);
			}
			var p = win.document.createElement("video");
			p.setAttribute("muted", "");
			p.setAttribute("playsinline", "");
			p.setAttribute("webkit-playsinline", "");
			p.muted = true;
			p.playsinline = true;
			p.webkitPlaysinline = true;
			p.setAttribute("height", "0");
			p.setAttribute("width", "0");
			css(p, {
				position: "fixed",
				top: "0",
				width: "0",
				height: "0",
				opacity: "0"
			});
			try {
				var promise = p.play();
				if (promise) {
					if (promise.catch) {
						promise.catch(function() {});
					}
				}
			} catch (e) {}
			return instance = Promise.resolve(!p.paused);
		}

		function source(request) {
			this.win = request;
		}

		function get(context, url, obj) {
			var options = obj || {};
			url = context.getCorsUrl(context.win, url);
			var o = parseURL(context.win.location.href).origin;
			obj = parseURL(url).origin;
			if (o == obj) {
				options.headers = options.headers || {};
				options.headers["AMP-Same-Origin"] = "true";
			}
			return context.le(url, options).then(function(result) {
				var actual = result.headers.get("AMP-Access-Control-Allow-Source-Origin");
				if (actual) {
					var expected = parseURL(next(context.win.location.href)).origin;
					error().assert(actual == expected, "Returned AMP-Access-Control-Allow-Source-Origin is not equal to the current: " + actual + (" vs " + expected));
				} else {
					if (options.requireAmpResponseSourceOrigin) {
						error().assert(false, "Response must contain the AMP-Access-Control-Allow-Source-Origin header");
					}
				}
				return result;
			}, function(err) {
				error().assert(false, "Fetch failed %s: %s", url, err && err.message);
			});
		}

		function String(method) {
			if (void 0 === method) {
				return "GET";
			}
			method = method.toUpperCase();
			methods.indexOf(method);
			return method;
		}

		function sendRequest(req) {
			req.headers = req.headers || {};
			req.headers.Accept = "application/json";
			if ("POST" == req.method) {
				if ("[object FormData]" !== core_toString.call(req.body)) {
					array.some(function(callback) {
						return callback(req.body);
					});
					req.headers["Content-Type"] = "application/json;charset=utf-8";
					req.body = JSON.stringify(req.body);
				}
			}
		}

		function ajax(url, options) {
			var opts = options || {};
			return new Promise(function(fetchOnlyFunction, done) {
				var xhr = createCORSRequest(opts.method || "GET", url);
				if ("include" == opts.credentials) {
					xhr.withCredentials = true;
				}
				if (opts.responseType in defaultMock) {
					xhr.responseType = opts.responseType;
				}
				if (opts.headers) {
					Object.keys(opts.headers).forEach(function(name) {
						xhr.setRequestHeader(name, opts.headers[name]);
					});
				}
				xhr.onreadystatechange = function() {
					if (!(2 > xhr.readyState)) {
						if (100 > xhr.status || 599 < xhr.status) {
							xhr.onreadystatechange = null;
							done(Error("Unknown HTTP status " + xhr.status));
						} else {
							if (4 == xhr.readyState) {
								fetchOnlyFunction(new module(xhr));
							}
						}
					}
				};
				xhr.onerror = function() {
					done(Error("Network failure"));
				};
				xhr.onabort = function() {
					done(Error("Request aborted"));
				};
				if ("POST" == opts.method) {
					xhr.send(opts.body);
				} else {
					xhr.send();
				}
			});
		}

		function createCORSRequest(method, url) {
			var xhr = new XMLHttpRequest;
			if ("withCredentials" in xhr) {
				xhr.open(method, url, true);
			} else {
				if ("undefined" != typeof XDomainRequest) {
					xhr = new XDomainRequest;
					xhr.open(method, url);
				} else {
					throw Error("CORS is not supported");
				}
			}
			return xhr;
		}

		function cb(stats) {
			return 415 == stats || 500 <= stats && 600 > stats;
		}

		function fetch(res) {
			return new Promise(function(fn, done) {
				if (200 > res.status || 300 <= res.status) {
					var err = error().createError("HTTP error " + res.status);
					if (cb(res.status)) {
						err.retriable = true;
					}
					if ("application/json" == res.headers.get("Content-Type")) {
						res.json().then(function(buf) {
							err.responseJson = buf;
							done(err);
						}, function() {
							done(err);
						});
					} else {
						done(err);
					}
				} else {
					fn(res);
				}
			});
		}

		function module(name) {
			this.na = name;
			this.status = this.na.status;
			this.headers = new Store(name);
			this.bodyUsed = false;
		}

		function interpolate(item) {
			item.bodyUsed = true;
			return Promise.resolve(item.na.responseText);
		}

		function Store(options) {
			this.na = options;
		}

		function Component(book) {
			this.ampdoc = book;
			this.$a = void 0;
			this.Y = this.ampdoc.win.Object.create(null);
			this.me = with_walkers;
			this.Vd = all(this.ampdoc.win, "variant", "amp-experiment");
			this.Ld = all(this.ampdoc.win, "share-tracking", "amp-share-tracking");
			this.qd = false;
		}

		function activate(container, callback, e) {
			return container.me(container.ampdoc.win).then(function(err) {
				return err ? callback(err) : (error().error("UrlReplacements", "Access service is not installed to access: ", e), null);
			});
		}

		function map(str, list, callback) {
			var val = evaluate(str, list, callback);
			return "" === val ? wait(str.ampdoc.win.document).then(function() {
				return evaluate(str, list, callback);
			}) : Promise.resolve(val);
		}

		function evaluate(w, name, object) {
			w = w.ampdoc.win;
			var t = w.performance && w.performance.timing;
			if (t && (0 != t.navigationStart && (name = void 0 === object ? t[name] : t[object] - t[name], isArrayLike(name)))) {
				return 0 > name ? "" : name;
			}
		}

		function simulateKeyEvent(target, type) {
			target = target.ampdoc.win;
			var special = target.performance && target.performance.navigation;
			if (special && void 0 !== special[type]) {
				return special[type];
			}
		}

		function insert(target, selector, rest_items) {
			error().assert(selector, "The first argument to QUERY_PARAM, the query string param is required");
			error().assert("string" == typeof selector, "param should be a string");
			target = parseURL(target.ampdoc.win.location.href);
			target = normalize(target.search);
			return "undefined" !== typeof target[selector] ? target[selector] : rest_items;
		}

		function valueOf(item, type, sync) {
			type.indexOf("RETURN");
			item.Y[type] = item.Y[type] || {
				sync: void 0,
				async: void 0
			};
			item.Y[type].sync = sync;
			item.$a = void 0;
			return item;
		}

		function is(obj, type, value) {
			type.indexOf("RETURN");
			obj.Y[type] = obj.Y[type] || {
				sync: void 0,
				async: void 0
			};
			obj.Y[type].async = value;
			obj.$a = void 0;
		}

		function text(str, item, p, elems) {
			is(valueOf(str, item, function() {
				return evaluate(str, p, elems);
			}), item, function() {
				return map(str, p, elems);
			});
		}

		function bind(obj, name, object, flags, fn, handler) {
			if (!obj.qd) {
				obj.hc();
			}
			var index = values(obj, object);
			var result;
			var val = name.replace(index, function(key, i, line) {
				var args = [];
				if ("string" == typeof line) {
					args = line.split(",");
				}
				if (handler && !handler[i]) {
					return key;
				}
				var options;
				if (object && i in object) {
					options = object[i];
				} else {
					if (options = obj.Y[i]) {
						if (fn) {
							if (options = options.sync, !options) {
								return error().error("UrlReplacements", "ignoring async replacement key: ", i), "";
							}
						} else {
							options = options.async || options.sync;
						}
					}
				}
				var value;
				try {
					value = "function" == typeof options ? options.apply(null, args) : options;
				} catch (restoreScript) {
					if (fn) {
						value = "";
					}
					then(restoreScript);
				}
				if (value && value.then) {
					if (fn) {
						return error().error("UrlReplacements", "ignoring promise value for key: ", i), "";
					}
					var __ = value.catch(function(results) {
						then(results);
					}).then(function(value) {
						val = val.replace(key, null == value ? "" : encodeURIComponent(value));
						if (flags) {
							flags[key] = value;
						}
					});
					result = result ? result.then(function() {
						return __;
					}) : __;
					return key;
				}
				if (flags) {
					flags[key] = value;
				}
				return null == value ? "" : encodeURIComponent(value);
			});
			if (result) {
				result = result.then(function() {
					return val;
				});
			}
			return fn ? ondata(name, val) : (result || Promise.resolve(val)).then(function(val) {
				return ondata(name, val);
			});
		}

		function values(obj, object) {
			var assertions = object ? Object.keys(object) : null;
			if (assertions && 0 < assertions.length) {
				var output = Object.keys(obj.Y);
				assertions.forEach(function(attributes) {
					if (void 0 === obj.Y[attributes]) {
						output.push(attributes);
					}
				});
				return list(output);
			}
			if (!obj.$a) {
				obj.$a = list(Object.keys(obj.Y));
			}
			return obj.$a;
		}

		function list(buf) {
			buf.sort(function(check, value) {
				return value.length - check.length;
			});
			var b = buf.join("|");
			return new RegExp("\\$?(" + b + ")(?:\\(([0-9a-zA-Z-_.,]+)\\))?", "g");
		}

		function ondata(name, url) {
			var expected = parseURL(url, true).protocol;
			var actual = parseURL(name, true).protocol;
			if (expected != actual) {
				return error().error("UrlReplacements", "Illegal replacement of the protocol: ", name), name;
			}
			error().assert("javascript:" !== expected, "Illegal javascript link protocol: %s", name);
			return url;
		}

		function setter(el) {
			extend(el, "platform", events);
			extend(el, "timer", errorHandler);
			step(el);
			extend(el, "xhr", source);
			extend(el, "templates", copy);
		}

		function push(done) {
			function fn(f) {
				if ("function" == typeof f) {
					var loaded = f;
					validate(win.document, function() {
						return loaded(win.AMP);
					});
				} else {
					var check = function() {
						var name = f.n;
						var data = f.f;
						var innerHeight = win.AMP;
						var result = debug(doc, name);
						try {
							doc.ib = name;
							data(innerHeight);
							result.loaded = true;
							if (result.resolve) {
								result.resolve(result.extension);
							}
						} catch (e) {
							throw result.error = e, result.reject && result.reject(e), e;
						} finally {
							doc.ib = null;
						}
					};
					check.displayName = f.n;
					validate(win.document, check);
				}
			}

			function callback() {
				if (func(win, "amp-lightbox-viewer-auto")) {
					createElement(win, "extensions").loadExtension("amp-lightbox-viewer");
				}
			}
			var win = self;
			var that = {
				registerElement: access,
				registerServiceForDoc: createDom
			};
			if (!win.AMP_TAG) {
				win.AMP_TAG = true;
				var codeSegments = win.AMP || [];
				var doc = extend(win, "extensions", base);
				setter(win);
				win.AMP = {
					win: win
				};
				win.AMP.config = o;
				win.AMP.BaseElement = Item;
				win.AMP.BaseTemplate = Text;
				win.AMP.registerElement = that.registerElement.bind(null, win, doc);
				win.AMP.registerTemplate = function(name, value) {
					var context = extend(win, "templates", copy);
					if (context.eb[name]) {
						var func = context.Lc[name];
						error().assert(func, "Duplicate template type: %s", name);
						delete context.Lc[name];
						func(value);
					} else {
						context.eb[name] = Promise.resolve(value);
					}
				};
				win.AMP.registerServiceForDoc = that.registerServiceForDoc.bind(null, win, doc);
				win.AMP.isExperimentOn = func.bind(null, win);
				win.AMP.toggleExperiment = write.bind(null, win);
				win.AMP.setTickFunction = function() {};
				done(win, doc);
				win.AMP.push = function(opt_attributes) {
					function push() {
						attach(win.document, function() {
							fn(opt_attributes);
						});
					}
					push.displayName = opt_attributes.n;
					validate(win.document, push);
				};
				attach(win.document, function() {
					var i = 0;
					for (; i < codeSegments.length; i++) {
						var item = codeSegments[i];
						try {
							fn(item);
						} catch (e) {
							call().error("runtime", "Extension failed: ", e, item.n);
						}
					}
					callback();
					codeSegments.length = 0;
				});
				if (on(win).isIos()) {
					setStyle(win.document.documentElement, "cursor", "pointer");
				}
			}
		}

		function ready() {
			push(function(options) {
				var that = split(options.document);
				options.AMP.viewer = that;
				if (fail().development) {
					options.AMP.toggleRuntime = that.toggleRuntime.bind(that);
					options.AMP.resources = toJSON(options.document);
				}
				that = each(options.document);
				options.AMP.viewport = {};
				options.AMP.viewport.getScrollLeft = that.getScrollLeft.bind(that);
				options.AMP.viewport.getScrollWidth = that.getScrollWidth.bind(that);
				options.AMP.viewport.getWidth = that.getWidth.bind(that);
			});
		}

		function access(res, raw, value, key, owner) {
			stringify(raw, value, key, owner);
			if (owner) {
				loadStyleSheet(res.document, owner, function() {
					append(res, value, key);
				}, false, value);
			} else {
				append(res, value, key);
			}
		}

		function append(name, key, value) {
			if (wrapped[key]) {
				error().assert(wrapped[key] == tag, "%s is already registered. The script tag for %s is likely included twice in the page.", key, key);
				wrapped[key] = value;
				var i = 0;
				for (; i < tokens.length; i++) {
					var el = tokens[i].element;
					if (el.tagName.toLowerCase() == key) {
						try {
							el.upgrade(value);
						} catch (e) {
							done(e, el);
						}
						tokens.splice(i--, 1);
					}
				}
			} else {
				define(name, key, value);
			}
			setData(name, key, udataCur);
		}

		function createDom(type, opt_attributes, tagName, var_args, attrs) {
			type = createElement(type, "ampdoc").getAmpDoc();
			if (opt_attributes = var_args) {
				parseInt(type, tagName, opt_attributes);
			} else {
				triggerEvent(type, tagName, attrs);
			}
		}

		function udataCur() {
			return {};
		}
		var that;
		(function(dataAndEvents) {
			return "undefined" != typeof window && window === dataAndEvents ? dataAndEvents : "undefined" != typeof global ? global : dataAndEvents;
		})(this);
		var optsData = "";
		var ctrlx = Date.now();
		that = test.prototype;
		that.isEnabled = function() {
			return 0 != this.Ea;
		};
		that.fine = function(key, msg) {
			if (4 <= this.Ea) {
				log(this, key, "FINE", Array.prototype.slice.call(arguments, 1));
			}
		};
		that.info = function(name, var_args) {
			if (3 <= this.Ea) {
				log(this, name, "INFO", Array.prototype.slice.call(arguments, 1));
			}
		};
		that.warn = function(name, message) {
			if (2 <= this.Ea) {
				log(this, name, "WARN", Array.prototype.slice.call(arguments, 1));
			}
		};
		that.error = function(name, textStatus) {
			if (1 <= this.Ea) {
				log(this, name, "ERROR", Array.prototype.slice.call(arguments, 1));
			} else {
				var expectationResult = constructor.apply(null, Array.prototype.slice.call(arguments, 1));
				onError(this, expectationResult);
				this.win.setTimeout(function() {
					throw expectationResult;
				});
			}
		};
		that.createError = function(msg) {
			var res = constructor.apply(null, arguments);
			onError(this, res);
			return res;
		};
		that.assert = function(expr, expression, value) {
			var data;
			if (!expr) {
				var pathConfig = (expression || "Assertion failed").split("%s");
				var i = pathConfig.shift();
				var pointer = i;
				var Q = [];
				if ("" != i) {
					Q.push(i);
				}
				i = 2;
				for (; i < arguments.length; i++) {
					var attributes = arguments[i];
					if (attributes) {
						if (attributes.tagName) {
							data = attributes;
						}
					}
					var clss = pathConfig.shift();
					Q.push(attributes);
					var val = clss.trim();
					if ("" != val) {
						Q.push(val);
					}
					val = pointer;
					var item;
					item = attributes;
					item = item instanceof Element ? item.tagName.toLowerCase() + (item.id ? "#" + item.id : "") : item;
					pointer = val + (item + clss);
				}
				i = Error(pointer);
				i.fromAssert = true;
				i.associatedElement = data;
				i.messageArray = Q;
				onError(this, i);
				throw i;
			}
			return expr;
		};
		that.assertElement = function(value, var_args) {
			this.assert(value && 1 == value.nodeType, (var_args || "Element expected") + ": %s", value);
			return value;
		};
		that.assertString = function(value, var_args) {
			this.assert("string" == typeof value, (var_args || "String expected") + ": %s", value);
			return value;
		};
		that.assertNumber = function(value, var_args) {
			this.assert("number" == typeof value, (var_args || "Number expected") + ": %s", value);
			return value;
		};
		that.assertEnumValue = function(obj, val, dataAndEvents) {
			var key;
			for (key in obj) {
				if (obj[key] == val) {
					return obj[key];
				}
			}
			this.assert(false, 'Unknown %s value: "%s"', dataAndEvents || "enum", val);
		};
		self.log = self.log || {
			user: null,
			dev: null
		};
		var req = self.log;
		var User = null;
		var core_toString = Object.prototype.toString;
		var hasOwnProperty = Object.prototype.hasOwnProperty;
		def.prototype.then = function(fn, callback) {
			fn = isFunction(fn) ? fn : void 0;
			callback = isFunction(callback) ? callback : void 0;
			if (fn || callback) {
				this._isChainEnd = false;
			}
			return this._state(this._value, fn, callback);
		};
		def.prototype.catch = function(callback) {
			return this.then(void 0, callback);
		};
		var expect = function() {
			function focus() {
				var i = 0;
				for (; i < j; i++) {
					var parent = args[i];
					args[i] = null;
					parent();
				}
				j = 0;
			}

			function completed(code) {
				if (0 === j) {
					ready();
				}
				args[j++] = code;
			}
			var ready;
			if ("undefined" !== typeof window && window.postMessage) {
				window.addEventListener("message", focus);
				ready = function() {
					window.postMessage("macro-task", "*");
				};
			} else {
				ready = function() {
					setTimeout(focus, 0);
				};
			}
			var args = Array(16);
			var j = 0;
			return completed;
		}();
		init(self);
		(function(context) {
			if (!context.Math.sign) {
				context.Math.sign = sign;
			}
		})(self);
		(function(obj) {
			if (!obj.Object.assign) {
				obj.Object.assign = every;
			}
		})(self);
		(function(self) {
			if (!self.Promise) {
				self.Promise = def;
				if (def.default) {
					self.Promise = def.default;
				}
				self.Promise.resolve = ok;
				self.Promise.reject = reject;
				self.Promise.all = getGame;
				self.Promise.race = race;
			}
		})(self);
		inspect(self);
		var doc;
		var o = {};
		var options = self.AMP_CONFIG || {};
		o.urls = {
			thirdParty: options.thirdPartyUrl || "https://3p.ampproject.net",
			thirdPartyFrameHost: options.thirdPartyFrameHost || "ampproject.net",
			thirdPartyFrameRegex: ("string" == typeof options.thirdPartyFrameRegex ? new RegExp(options.thirdPartyFrameRegex) : options.thirdPartyFrameRegex) || /^d-\d+\.ampproject\.net$/,
			cdn: options.cdnUrl || "https://cdn.ampproject.org",
			errorReporting: options.errorReportingUrl || "https://amp-error-reporting.appspot.com/r",
			localDev: options.localDev || false
		};
		var config;
		var params;
		var rSlash = /[?&]amp_js[^&]*/;
		var root;
		that = Node.prototype;
		that.add = function(type) {
			var $div = this;
			this.Ca.push(type);
			return function() {
				$div.remove(type);
			};
		};
		that.remove = function(object) {
			object = this.Ca.indexOf(object);
			if (-1 < object) {
				this.Ca.splice(object, 1);
			}
		};
		that.removeAll = function() {
			this.Ca.length = 0;
		};
		that.fire = function(data) {
			var codeSegments = this.Ca;
			var i = 0;
			for (; i < codeSegments.length; i++) {
				(0, codeSegments[i])(data);
			}
		};
		that.getHandlerCount = function() {
			return this.Ca.length;
		};
		var object;
		var codeSegments = "Webkit webkit Moz moz ms O o".split(" ");
		that = handle.prototype;
		that.za = function() {
			if (this.La) {
				this.G.removeEventListener(this.La, this.Wc);
			}
		};
		that.isHidden = function() {
			return this.pa ? this.G[this.pa] : false;
		};
		that.getVisibilityState = function() {
			return this.Nb ? this.G[this.Nb] : this.isHidden() ? "hidden" : "visible";
		};
		that.onVisibilityChanged = function(cycle) {
			return this.hb.add(cycle);
		};
		that.Bc = function() {
			this.hb.fire();
		};
		that.onBodyAvailable = function(ready) {
			var iframeDoc = this.G;
			if (iframeDoc.body) {
				return ready(), null;
			}
			if (!this.ya) {
				this.ya = new Node;
				addMutationObserver(iframeDoc.documentElement, function() {
					return !!iframeDoc.body;
				}, this.we.bind(this));
			}
			return this.ya.add(ready);
		};
		that.we = function() {
			this.ya.fire();
			this.ya.removeAll();
			this.ya = null;
		};
		var conditions = {
			"amp-accordion": "[custom-element=amp-accordion]",
			"amp-dynamic-css-classes": "[custom-element=amp-dynamic-css-classes]",
			variant: "amp-experiment"
		};
		var m = self.AMPErrors || [];
		self.AMPErrors = m;
		var opts = {
			NODISPLAY: "nodisplay",
			FIXED: "fixed",
			FIXED_HEIGHT: "fixed-height",
			RESPONSIVE: "responsive",
			CONTAINER: "container",
			FILL: "fill",
			FLEX_ITEM: "flex-item"
		};
		var styleCache = {
			"AMP-PIXEL": {
				width: "1px",
				height: "1px"
			},
			"AMP-ANALYTICS": {
				width: "1px",
				height: "1px"
			},
			"AMP-AUDIO": null,
			"AMP-SOCIAL-SHARE": {
				width: "60px",
				height: "44px"
			}
		};
		var $cookies = {
			"AMP-ANIM": true,
			"AMP-BRIGHTCOVE": true,
			"AMP-EMBED": true,
			"AMP-IFRAME": true,
			"AMP-IMG": true,
			"AMP-INSTAGRAM": true,
			"AMP-LIST": true,
			"AMP-PINTEREST": true,
			"AMP-VIDEO": true,
			"AMP-YOUTUBE": true
		};
		var suiteView = {
			PRERENDER: "prerender",
			VISIBLE: "visible",
			HIDDEN: "hidden",
			PAUSED: "paused",
			INACTIVE: "inactive"
		};
		var Pd = /nochunking=1/.test(self.location.hash);
		var promiseOrValue = Promise.resolve();
		close.prototype.ua = function() {
			if (!this.Rc || this.ga()) {
				promiseOrValue.then(this.Sb);
			} else {
				if (this.U.requestIdleCallback) {
					this.U.requestIdleCallback(this.Sb, {
						timeout: 1E3
					});
				} else {
					this.U.postMessage("amp-macro-task", "*");
				}
			}
		};
		close.prototype.ga = function() {
			return this.a ? this.a.isVisible() : this.U.document.hidden ? false : !/visibilityState=(hidden|prerender)/.test(this.U.location.hash);
		};
		that = message.prototype;
		that.coreServicesAvailable = function() {
			var _this = this;
			this.a = split(this.win.document);
			this.c = toJSON(this.win.document);
			this.a.onVisibilityChanged(this.flush.bind(this));
			extract(this);
			var promise = this.a.whenMessagingReady();
			this.a.whenFirstVisible().then(function() {
				_this.tick("ofv");
				_this.flush();
			});
			return promise ? promise.then(function() {
				_this.nc = true;
				save(_this);
				_reset(_this);
				_this.flush();
			}) : Promise.resolve();
		};
		that.tick = function(value, top, event) {
			top = void 0 == top ? null : top;
			event = void 0 == event ? Date.now() : event;
			if (this.nc && this.a.isPerformanceTrackingOn()) {
				this.a.tick({
					label: value,
					from: top,
					value: event
				});
			} else {
				var key = top;
				var originalEvent = event;
				if (50 <= this.Ta.length) {
					this.Ta.shift();
				}
				this.Ta.push({
					label: value,
					from: key,
					value: originalEvent
				});
			}
			if (this.win.performance) {
				if (this.win.performance.mark) {
					if (1 == arguments.length) {
						this.win.performance.mark(value);
					}
				}
			}
		};
		that.tickDelta = function(name, x1) {
			this.tick("_" + name, void 0, this.fc);
			this.tick(name, "_" + name, Math.round(x1 + this.fc));
		};
		that.tickSinceVisible = function(optgroup) {
			var right = Date.now();
			var left = this.a ? this.a.getFirstVisibleTime() : 0;
			this.tickDelta(optgroup, left ? Math.max(right - left, 0) : 0);
		};
		that.flush = function() {
			if (this.nc) {
				if (this.a.isPerformanceTrackingOn()) {
					this.a.flushTicks();
				}
			}
		};
		that = Editor.prototype;
		that.cleanup = function() {
			cancel(this);
			this.da.removeEventListener("touchstart", this.cd, true);
		};
		that.Fe = function(orig) {
			if (!this.Kb) {
				if (!!orig.touches) {
					if (!(1 != orig.touches.length)) {
						if (!(0 < this.s.getScrollTop())) {
							orig = orig.touches[0].clientY;
							this.Kb = true;
							this.Ic = orig;
							this.da.addEventListener("touchmove", this.bd, true);
							this.da.addEventListener("touchend", this.ad, true);
							this.da.addEventListener("touchcancel", this.$c, true);
						}
					}
				}
			}
		};
		that.Ee = function(event) {
			if (this.Kb) {
				var Ic = event.touches[0].clientY - this.Ic;
				if (0 < Ic) {
					event.preventDefault();
				}
				if (0 != Ic) {
					cancel(this);
				}
			}
		};
		that.De = function() {
			cancel(this);
		};
		that.Ce = function() {
			cancel(this);
		};
		Tabs.prototype.cleanup = function() {
			if (this.Tb) {
				this.ampdoc.getRootNode().removeEventListener("click", this.Tb);
			}
		};
		Tabs.prototype.ne = function(context) {
			fn(context, this.ampdoc, this.s, this.B, this.re);
		};
		var element = {
			composed: false
		};
		var before;
		Selection.prototype.isSingleDoc = function() {
			return !!this.Eb;
		};
		Selection.prototype.getAmpDoc = function(elems) {
			if (this.Eb) {
				return this.Eb;
			}
			var source = elems;
			for (; source;) {
				if (source.D) {
					return source.D;
				}
				var code = getComputedStyle(source, this.win);
				if (code) {
					source = code;
				} else {
					source = findListItems(source);
					if (!source) {
						break;
					}
					var il = source.__AMPDOC;
					if (il) {
						return il;
					}
					source = source.host;
				}
			}
			throw call().createError("No ampdoc found for", elems);
		};
		that = value.prototype;
		that.isSingleDoc = function() {
			return null;
		};
		that.getWin = function() {
			return this.win;
		};
		that.getRootNode = function() {
			return null;
		};
		that.isBodyAvailable = function() {
			return false;
		};
		that.getBody = function() {
			return null;
		};
		that.whenBodyAvailable = function() {
			return null;
		};
		that.isReady = function() {
			return null;
		};
		that.whenReady = function() {
			return null;
		};
		that.getUrl = function() {
			return null;
		};
		that.getElementById = function(id) {
			return this.getRootNode().getElementById(id);
		};
		that.contains = function(target) {
			return this.getRootNode().contains(target);
		};
		mixin(Player, value);
		that = Player.prototype;
		that.isSingleDoc = function() {
			return true;
		};
		that.getRootNode = function() {
			return this.win.document;
		};
		that.getUrl = function() {
			return this.win.location.href;
		};
		that.isBodyAvailable = function() {
			return !!this.win.document.body;
		};
		that.getBody = function() {
			return this.win.document.body;
		};
		that.whenBodyAvailable = function() {
			return this.ae;
		};
		that.isReady = function() {
			return checkState(this.win.document);
		};
		that.whenReady = function() {
			return this.Ke;
		};
		var fragment = null;
		$.prototype.url = function(callback, url, value) {
			if (getUrl(url)) {
				url = parseURL(url).origin;
				var curveY = Date.now();
				var lowBound = this.Xa[url];
				if (lowBound && curveY < lowBound) {
					if (value) {
						this.Xa[url] = curveY + 18E4;
					}
				} else {
					this.Xa[url] = curveY + (value ? 18E4 : 1E4);
					var a;
					if (!this.Xb.preconnect) {
						a = this.G.createElement("link");
						a.setAttribute("rel", "dns-prefetch");
						a.setAttribute("href", url);
						this.cc.appendChild(a);
					}
					var link = this.G.createElement("link");
					link.setAttribute("rel", "preconnect");
					link.setAttribute("href", url);
					link.setAttribute("referrerpolicy", "origin");
					this.cc.appendChild(link);
					this.L.delay(function() {
						if (a) {
							if (a.parentNode) {
								a.parentNode.removeChild(a);
							}
						}
						if (link.parentNode) {
							link.parentNode.removeChild(link);
						}
					}, 1E4);
					search(this, callback, url);
				}
			}
		};
		$.prototype.preload = function(sqlt, method) {
			var data = this;
			if (getUrl(method) && !this.Td[method]) {
				var val = this.Xb.preload ? "preload" : "prefetch";
				this.Td[method] = true;
				this.url(sqlt, method, true);
				sqlt.whenFirstVisible().then(function() {
					var el = data.G.createElement("link");
					el.setAttribute("rel", val);
					el.setAttribute("href", method);
					el.setAttribute("referrerpolicy", "origin");
					data.cc.appendChild(el);
				});
			}
		};
		group.prototype.url = function(callback, args) {
			this.Dd.url(lookup(this), callback, args);
		};
		group.prototype.preload = function(el, url) {
			this.Dd.preload(lookup(this), el, url);
		};
		that = Item.prototype;
		that.getPriority = function() {
			return 0;
		};
		that.getLayout = function() {
			return this.layout_;
		};
		that.getLayoutBox = function() {
			return this.element.getLayoutBox();
		};
		that.getWin = function() {
			return this.win;
		};
		that.getAmpDoc = function() {
			return this.element.getAmpDoc();
		};
		that.getVsync = function() {
			return once(this.win);
		};
		that.getLayoutWidth = function() {
			return this.layoutWidth_;
		};
		that.isLayoutSupported = function(dataAndEvents) {
			return "nodisplay" == dataAndEvents;
		};
		that.isAlwaysFixed = function() {
			return false;
		};
		that.isInViewport = function() {
			return this.inViewport_;
		};
		that.upgradeCallback = function() {
			return null;
		};
		that.createdCallback = function() {};
		that.firstAttachedCallback = function() {};
		that.buildCallback = function() {};
		that.preconnectCallback = function() {};
		that.detachedCallback = function() {};
		that.setAsOwner = function(plugin) {
			this.element.getResources().setOwner(plugin, this.element);
		};
		that.prerenderAllowed = function() {
			return false;
		};
		that.createPlaceholderCallback = function() {
			return null;
		};
		that.renderOutsideViewport = function() {
			return 3;
		};
		that.isRelayoutNeeded = function() {
			return false;
		};
		that.layoutCallback = function() {
			return Promise.resolve();
		};
		that.firstLayoutCompleted = function() {
			this.togglePlaceholder(false);
		};
		that.viewportCallback = function() {};
		that.pauseCallback = function() {};
		that.resumeCallback = function() {};
		that.unlayoutCallback = function() {
			return false;
		};
		that.unlayoutOnPause = function() {
			return false;
		};
		that.activate = function() {};
		that.loadPromise = function(success, target) {
			return complete(success, target);
		};
		that.registerAction = function(name, method) {
			clone(this);
			this.actionMap_[name] = method;
		};
		that.executeAction = function(event) {
			if ("activate" == event.method) {
				this.activate(event);
			} else {
				clone(this);
				var func = this.actionMap_[event.method];
				error().assert(func, "Method not found: " + event.method + " in %s", this);
				func(event);
			}
		};
		that.getMaxDpr = function() {
			return this.element.getResources().getMaxDpr();
		};
		that.getDpr = function() {
			return this.element.getResources().getDpr();
		};
		that.propagateAttributes = function(events, h) {
			events = eachEvent(events) ? events : [events];
			var i = 0;
			for (; i < events.length; i++) {
				var node = events[i];
				if (this.element.hasAttribute(node)) {
					h.setAttribute(node, this.element.getAttribute(node));
				}
			}
		};
		that.forwardEvents = function(events, socket) {
			var validator = this;
			events = eachEvent(events) ? events : [events];
			var pos = {
				i: 0
			};
			for (; pos.i < events.length; pos = {
					i: pos.i
				}, pos.i++) {
				socket.addEventListener(events[pos.i], function(e) {
					return function(messageEvent) {
						validator.element.dispatchCustomEvent(events[e.i], messageEvent.data || {});
					};
				}(pos));
			}
		};
		that.getPlaceholder = function() {
			return this.element.getPlaceholder();
		};
		that.togglePlaceholder = function(recurring) {
			this.element.togglePlaceholder(recurring);
		};
		that.getFallback = function() {
			return this.element.getFallback();
		};
		that.toggleFallback = function(recurring) {
			this.element.toggleFallback(recurring);
		};
		that.getOverflowElement = function() {
			return this.element.getOverflowElement();
		};
		that.getRealChildNodes = function() {
			return this.element.getRealChildNodes();
		};
		that.getRealChildren = function() {
			return this.element.getRealChildren();
		};
		that.applyFillContent = function(targetBody, dataAndEvents) {
			targetBody.classList.add("-amp-fill-content");
			if (dataAndEvents) {
				targetBody.classList.add("-amp-replaced-content");
			}
		};
		that.getViewport = function() {
			return each(this.getAmpDoc());
		};
		that.getIntersectionElementLayoutBox = function() {
			return this.getLayoutBox();
		};
		that.scheduleLayout = function(dataAndEvents) {
			this.element.getResources().scheduleLayout(this.element, dataAndEvents);
		};
		that.schedulePause = function(events) {
			this.element.getResources().schedulePause(this.element, events);
		};
		that.scheduleResume = function(events) {
			this.element.getResources().scheduleResume(this.element, events);
		};
		that.schedulePreload = function(deepDataAndEvents) {
			this.element.getResources().schedulePreload(this.element, deepDataAndEvents);
		};
		that.scheduleUnlayout = function(events) {
			this.element.getResources().scheduleUnlayout(this.element, events);
		};
		that.updateInViewport = function(dataAndEvents, deepDataAndEvents) {
			this.element.getResources().updateInViewport(this.element, dataAndEvents, deepDataAndEvents);
		};
		that.changeHeight = function(isXML) {
			this.element.getResources().changeSize(this.element, isXML, void 0);
		};
		that.attemptChangeHeight = function(deepDataAndEvents) {
			return this.element.getResources().attemptChangeSize(this.element, deepDataAndEvents, void 0);
		};
		that.attemptChangeSize = function(deepDataAndEvents, opt_attributes) {
			return this.element.getResources().attemptChangeSize(this.element, deepDataAndEvents, opt_attributes);
		};
		that.mutateElement = function(key, keepData) {
			return this.element.getResources().mutateElement(keepData || this.element, key);
		};
		that.deferMutate = function(attributes) {
			this.element.getResources().deferMutate(this.element, attributes);
		};
		that.collapse = function() {
			this.element.getResources().collapseElement(this.element);
		};
		that.collapsedCallback = function() {};
		that.onLayoutMeasure = function() {};
		var tokens = [];
		var cache = {};
		mixin(tag, Item);
		tag.prototype.getPriority = function() {
			return 0;
		};
		tag.prototype.isLayoutSupported = function() {
			return true;
		};
		Test.prototype.select = function(context) {
			var dataName = 0;
			for (; dataName < this.Gb.length - 1; dataName++) {
				var data = this.Gb[dataName];
				if (data.mediaQuery && context.matchMedia(data.mediaQuery).matches) {
					return data.size;
				}
			}
			return this.getLast();
		};
		Test.prototype.getLast = function() {
			return this.Gb[this.Gb.length - 1].size;
		};
		var wrapped = {};
		var Je;
		e.prototype.select = function(idx, target) {
			var prevIdx = -1;
			if (this.ef) {
				prevIdx = -1;
				var len = 1E6;
				var endOffset = 1E6;
				var i = 0;
				for (; i < this.I.length; i++) {
					var offset = this.I[i];
					offset = offset.width ? offset.width / target : len / 2;
					len = Math.min(len, offset);
					offset = Math.abs((offset - idx) / idx - 0.2);
					if (offset < endOffset) {
						endOffset = offset;
						prevIdx = i;
					}
				}
			} else {
				if (this.ie) {
					idx = -1;
					prevIdx = 1E6;
					len = 0;
					for (; len < this.I.length; len++) {
						endOffset = Math.abs((this.I[len].dpr || 1) - target);
						if (endOffset < prevIdx) {
							prevIdx = endOffset;
							idx = len;
						}
					}
					prevIdx = idx;
				}
			}
			return -1 != prevIdx ? this.I[prevIdx] : this.getLast();
		};
		e.prototype.getLast = function() {
			return this.I[this.I.length - 1];
		};
		e.prototype.getSources = function() {
			return this.I;
		};
		e.prototype.stringify = function() {
			var ctx = [];
			var i = 0;
			for (; i < this.I.length; i++) {
				var param = this.I[i];
				if (param.width) {
					ctx.push(param.url + " " + param.width + "w");
				} else {
					if (param.dpr) {
						ctx.push(param.url + " " + param.dpr + "x");
					} else {
						ctx.push("" + param.url);
					}
				}
			}
			return ctx.join(", ");
		};
		mixin(c, Item);
		that = c.prototype;
		that.buildCallback = function() {
			this.rd = !this.element.hasAttribute("noprerender");
			var n;
			n = this.element;
			var m = n.getAttribute("srcset");
			if (m) {
				n = m.match(/\s*(?:[\S]*)(?:\s+(?:-?(?:\d+(?:\.(?:\d+)?)?|\.\d+)[a-zA-Z]))?(?:\s*,)?/g);
				error().assert(0 < n.length, "srcset has to have at least one source: %s", void 0);
				m = [];
				var i = 0;
				for (; i < n.length; i++) {
					var part = n[i].trim();
					if ("," == part.substr(-1)) {
						part = part.substr(0, part.length - 1).trim();
					}
					var parts = part.split(/\s+/, 2);
					if (0 != parts.length && ((1 != parts.length || parts[0]) && (2 != parts.length || (parts[0] || parts[1])))) {
						if (part = parts[0], 1 == parts.length || 2 == parts.length && !parts[1]) {
							m.push({
								url: part,
								width: void 0,
								dpr: 1
							});
						} else {
							parts = parts[1].toLowerCase();
							var erlang = parts.substring(parts.length - 1);
							if ("w" == erlang) {
								m.push({
									url: part,
									width: parseFloat(parts),
									dpr: void 0
								});
							} else {
								if ("x" == erlang) {
									m.push({
										url: part,
										width: void 0,
										dpr: parseFloat(parts)
									});
								}
							}
						}
					}
				}
				n = new e(m);
			} else {
				n = error().assert(n.getAttribute("src"), 'Either non-empty "srcset" or "src" attribute must be specified: %s', n);
				n = new e([{
					url: n,
					width: void 0,
					dpr: 1
				}]);
			}
			this.Md = n;
		};
		that.isLayoutSupported = function(ordered) {
			return SwapArrayValues(ordered);
		};
		that.hc = function() {
			if (!this.J) {
				this.Ma = true;
				if (this.element.hasAttribute("fallback")) {
					this.Ma = false;
				}
				this.J = new Image;
				if (this.element.id) {
					this.J.setAttribute("amp-img-id", this.element.id);
				}
				if ("img" == this.element.getAttribute("role")) {
					this.element.removeAttribute("role");
					error().error("AMP-IMG", "Setting role=img on amp-img elements breaks screen readers please just set alt or ARIA attributes, they will be correctly propagated for the underlying <img> element.");
				}
				this.propagateAttributes(["alt", "referrerpolicy", "aria-label", "aria-describedby", "aria-labelledby"], this.J);
				this.applyFillContent(this.J, true);
				this.element.appendChild(this.J);
			}
		};
		that.prerenderAllowed = function() {
			return this.rd;
		};
		that.isRelayoutNeeded = function() {
			return true;
		};
		that.layoutCallback = function() {
			var typePattern = this;
			this.hc();
			var success1 = success(this);
			if (this.Ma) {
				success1 = success1.catch(function(dataAndEvents) {
					flip(typePattern);
					throw dataAndEvents;
				});
				this.Ma = false;
			}
			return success1;
		};
		that = base.prototype;
		that.waitForExtension = function(template) {
			return promise(debug(this, template));
		};
		that.loadExtension = function(name, object) {
			object = void 0 === object ? true : object;
			if ("amp-embed" == name) {
				name = "amp-ad";
			}
			var result = debug(this, name);
			var iteratee = object;
			var el;
			if (result.loaded || result.error) {
				el = false;
			} else {
				if (void 0 === result.scriptPresent) {
					el = this.win.document.head.querySelector('[custom-element="' + name + '"]');
					result.scriptPresent = !!el;
				}
				el = !result.scriptPresent;
			}
			if (el) {
				el = this.win.document.createElement("script");
				el.async = true;
				el.setAttribute("custom-element", name);
				el.setAttribute("data-script", name);
				var path = getModelPrefix() + "/rtv/" + fail().rtvVersion + "/v0/" + name + "-0.1.js";
				el.src = path;
				this.win.document.head.appendChild(el);
				result.scriptPresent = true;
				if (iteratee) {
					method(this.win, name);
				}
			}
			return promise(result);
		};
		that.loadElementClass = function(name) {
			return this.loadExtension(name).then(function(item) {
				return item.elements[name].implementationClass;
			});
		};
		that.ff = function(attributes, d) {
			isArray(this, d).docFactories.push(attributes);
		};
		that.gf = function(attributes, sqlt) {
			isArray(this, sqlt).shadowRootFactories.push(attributes);
		};
		that.installFactoriesInShadowRoot = function(outErr, failures) {
			var data = this;
			var ret = [];
			failures.forEach(function(path) {
				var info = debug(data, path);
				ret.push(promise(info).then(function() {
					info.shadowRootFactories.forEach(function(cb) {
						try {
							cb(outErr);
						} catch (rejected) {
							then("ShadowRoot factory failed: ", rejected, path);
						}
					});
				}));
			});
			return Promise.all(ret);
		};
		that.installExtensionsInChildWindow = function(event, failures, proceed) {
			var _self = this;
			var win = this.win;
			increment(event, event.frameElement.ownerDocument.defaultView);
			exports(event);
			loadStyleSheet(event.document, "html{overflow-x:hidden!important}body,html{height:auto!important}body{margin:0!important;-webkit-text-size-adjust:100%;-moz-text-size-adjust:100%;-ms-text-size-adjust:100%;text-size-adjust:100%}html.-amp-ios-embed{position:static}html.-amp-ios-embed,i-amp-html-wrapper{overflow-y:auto!important;-webkit-overflow-scrolling:touch!important}i-amp-html-wrapper{overflow-x:hidden!important;position:absolute!important;top:0!important;left:0!important;right:0!important;bottom:0!important;margin:0!important;display:block!important}i-amp-html-wrapper>body{position:relative!important;display:block!important;border-top:1px solid transparent!important}.-amp-make-body-block body{display:block!important}.-amp-element{display:inline-block}.-amp-layout-fixed{display:inline-block;position:relative}.-amp-layout-container,.-amp-layout-fixed-height,.-amp-layout-responsive{display:block;position:relative}.-amp-layout-fill{display:block;overflow:hidden!important;position:absolute;top:0;left:0;bottom:0;right:0}.-amp-layout-flex-item{display:block;position:relative;-webkit-box-flex:1;-webkit-flex:1 1 auto;-ms-flex:1 1 auto;flex:1 1 auto}.-amp-layout-size-defined{overflow:hidden!important}i-amp-sizer{display:block!important}.-amp-fill-content{display:block;width:1px;min-width:100%;height:100%;margin:auto}.-amp-layout-size-defined .-amp-fill-content{position:absolute;top:0;left:0;bottom:0;right:0}.-amp-replaced-content,.-amp-screen-reader{padding:0!important;border:none!important}.-amp-screen-reader{position:fixed!important;top:0px!important;left:0px!important;width:2px!important;height:2px!important;opacity:0!important;overflow:hidden!important;margin:0!important;display:block!important;visibility:visible!important}.-amp-unresolved{position:relative;overflow:hidden!important}.-amp-notbuilt{position:relative;overflow:hidden!important;color:transparent!important}.-amp-notbuilt>*{display:none}.-amp-ghost{visibility:hidden!important}.-amp-element>[placeholder]{display:block}.-amp-element>[placeholder].amp-hidden,.-amp-element>[placeholder].hidden{visibility:hidden}.-amp-element:not(.amp-notsupported)>[fallback]{display:none}.-amp-layout-size-defined>[fallback],.-amp-layout-size-defined>[placeholder]{position:absolute!important;top:0!important;left:0!important;right:0!important;bottom:0!important;z-index:1!important}.-amp-notbuilt>[placeholder]{display:block!important}.-amp-hidden-by-media-query{display:none}.-amp-element-error{background:red!important;color:#fff!important;position:relative!important}.-amp-element-error:before{content:attr(error-message)}i-amp-scroll-container{position:absolute;top:0;left:0;right:0;bottom:0;display:block}i-amp-scroll-container.amp-active{overflow:auto}.-amp-loading-container{display:block!important;z-index:1}.-amp-notbuilt>.-amp-loading-container{display:block!important}.-amp-loading-container.amp-hidden{visibility:hidden}.-amp-loader{position:absolute;display:block;height:10px;top:50%;left:50%;-webkit-transform:translateX(-50%) translateY(-50%);transform:translateX(-50%) translateY(-50%);-webkit-transform-origin:50% 50%;transform-origin:50% 50%;white-space:nowrap}.-amp-loader.amp-active .-amp-loader-dot{-webkit-animation:a 2s infinite;animation:a 2s infinite}.-amp-loader-dot{position:relative;display:inline-block;height:10px;width:10px;margin:2px;border-radius:100%;background-color:rgba(0,0,0,.3);box-shadow:2px 2px 2px 1px rgba(0,0,0,.2);will-change:transform}.-amp-loader .-amp-loader-dot:nth-child(1){-webkit-animation-delay:0s;animation-delay:0s}.-amp-loader .-amp-loader-dot:nth-child(2){-webkit-animation-delay:.1s;animation-delay:.1s}.-amp-loader .-amp-loader-dot:nth-child(3){-webkit-animation-delay:.2s;animation-delay:.2s}@-webkit-keyframes a{0%,to{-webkit-transform:scale(.7);transform:scale(.7);background-color:rgba(0,0,0,.3)}50%{-webkit-transform:scale(.8);transform:scale(.8);background-color:rgba(0,0,0,.5)}}@keyframes a{0%,to{-webkit-transform:scale(.7);transform:scale(.7);background-color:rgba(0,0,0,.3)}50%{-webkit-transform:scale(.8);transform:scale(.8);background-color:rgba(0,0,0,.5)}}.-amp-element>[overflow]{cursor:pointer;z-index:2;visibility:hidden}.-amp-element>[overflow].amp-visible{visibility:visible}template{display:none!important}.amp-border-box,.amp-border-box *,.amp-border-box :after,.amp-border-box :before{box-sizing:border-box}amp-pixel{position:fixed!important;top:0!important;width:1px!important;height:1px!important;overflow:hidden!important;visibility:hidden}amp-instagram{padding:48px 8px!important;background-color:#fff}amp-analytics{position:fixed!important;top:0!important;width:1px!important;height:1px!important;overflow:hidden!important;visibility:hidden}amp-iframe iframe{box-sizing:border-box!important}[amp-access][amp-access-hide],amp-experiment,amp-live-list>[update],amp-share-tracking,form [submit-error],form [submit-success]{display:none}amp-fresh{visibility:hidden}.amp-video-eq{-webkit-box-align:end;-webkit-align-items:flex-end;-ms-flex-align:end;align-items:flex-end;bottom:7px;display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;height:12px;opacity:0.8;overflow:hidden;position:absolute;right:7px;width:20px}.amp-video-eq .amp-video-eq-col{-webkit-box-flex:1;-webkit-flex:1;-ms-flex:1;flex:1;height:100%;margin-right:1px;position:relative}.amp-video-eq .amp-video-eq-col div{-webkit-animation-name:b;animation-name:b;-webkit-animation-timing-function:linear;animation-timing-function:linear;-webkit-animation-iteration-count:infinite;animation-iteration-count:infinite;-webkit-animation-direction:alternate;animation-direction:alternate;background-color:#fafafa;height:100%;position:absolute;width:100%;will-change:transform;-webkit-animation-play-state:paused;animation-play-state:paused}.amp-video-eq[unpausable] .amp-video-eq-col div{-webkit-animation-name:none;animation-name:none}.amp-video-eq[unpausable].amp-video-eq-play .amp-video-eq-col div{-webkit-animation-name:b;animation-name:b}.amp-video-eq.amp-video-eq-play .amp-video-eq-col div{-webkit-animation-play-state:running;animation-play-state:running}.amp-video-eq-1-1{-webkit-animation-duration:0.3s;animation-duration:0.3s}.amp-video-eq-1-1,.amp-video-eq-1-2{-webkit-transform:translateY(60%);transform:translateY(60%)}.amp-video-eq-1-2{-webkit-animation-duration:0.45s;animation-duration:0.45s}.amp-video-eq-2-1{-webkit-animation-duration:0.5s;animation-duration:0.5s}.amp-video-eq-2-1,.amp-video-eq-2-2{-webkit-transform:translateY(30%);transform:translateY(30%)}.amp-video-eq-2-2{-webkit-animation-duration:0.4s;animation-duration:0.4s}.amp-video-eq-3-1{-webkit-animation-duration:0.3s;animation-duration:0.3s}.amp-video-eq-3-1,.amp-video-eq-3-2{-webkit-transform:translateY(70%);transform:translateY(70%)}.amp-video-eq-3-2{-webkit-animation-duration:0.35s;animation-duration:0.35s}.amp-video-eq-4-1{-webkit-animation-duration:0.4s;animation-duration:0.4s}.amp-video-eq-4-1,.amp-video-eq-4-2{-webkit-transform:translateY(50%);transform:translateY(50%)}.amp-video-eq-4-2{-webkit-animation-duration:0.25s;animation-duration:0.25s}@-webkit-keyframes b{0%{-webkit-transform:translateY(100%);transform:translateY(100%)}to{-webkit-transform:translateY(0);transform:translateY(0)}}@keyframes b{0%{-webkit-transform:translateY(100%);transform:translateY(100%)}to{-webkit-transform:translateY(0);transform:translateY(0)}}\n/*# sourceURL=/css/amp.css*/",
				function() {}, true, "amp-runtime");
			if (proceed) {
				proceed(event);
			}
			withinElement(event);
			var ret = [];
			failures.forEach(function(name) {
				method(win, name);
				invoke(event, name);
				var attributes = _self.loadExtension(name).then(function(item) {
					var definition = item.elements[name];
					if (definition) {
						if (definition.css) {
							loadStyleSheet(event.document, definition.css, function() {}, false, name);
						}
					}
				});
				ret.push(attributes);
			});
			return Promise.all(ret);
		};
		var p = null;
		Text.prototype.compileCallback = function() {};
		Text.prototype.render = function() {
			throw Error("Not implemented");
		};
		Text.prototype.unwrap = function(parent) {
			var refNode = null;
			var node = parent.firstChild;
			for (; null != node; node = node.nextSibling) {
				if (3 == node.nodeType) {
					if (node.textContent.trim()) {
						refNode = null;
						break;
					}
				} else {
					if (8 != node.nodeType) {
						if (1 == node.nodeType) {
							if (refNode) {
								refNode = null;
								break;
							} else {
								refNode = node;
							}
						} else {
							refNode = null;
						}
					}
				}
			}
			return refNode || parent;
		};
		copy.prototype.renderTemplate = function(callback, data) {
			return check(this, callback).then(function(template) {
				return template.render(data);
			});
		};
		copy.prototype.renderTemplateArray = function(done, deepDataAndEvents) {
			return 0 == deepDataAndEvents.length ? Promise.resolve([]) : check(this, done).then(function(sass) {
				return deepDataAndEvents.map(function(str) {
					return sass.render(str);
				});
			});
		};
		copy.prototype.findAndRenderTemplate = function(refNode, data) {
			return this.renderTemplate(byId(refNode), data);
		};
		copy.prototype.findAndRenderTemplateArray = function(refNode, deepDataAndEvents) {
			return this.renderTemplateArray(byId(refNode), deepDataAndEvents);
		};
		var $name = "__AMP_ACTION_MAP__" + Math.random();
		var handlers = {
			form: ["submit"]
		};
		that = Window.prototype;
		that.addEvent = function(event) {
			var _self = this;
			if ("tap" == event) {
				this.ampdoc.getRootNode().addEventListener("click", function(e) {
					if (!e.defaultPrevented) {
						_self.trigger(e.target, "tap", e);
					}
				});
			} else {
				if (!("submit" != event && "change" != event)) {
					this.ampdoc.getRootNode().addEventListener(event, function(e) {
						_self.trigger(e.target, event, e);
					});
				}
			}
		};
		that.addGlobalMethodHandler = function(option, value) {
			this.Yb[option] = value;
		};
		that.trigger = function(obj, data, path) {
			a: {
				var root;
				for (; obj;) {
					root = data;
					var item;
					item = obj;
					var name = item[$name];
					if (void 0 === name) {
						name = null;
						if (item.hasAttribute("on")) {
							name = item.getAttribute("on");
							var h = mkdir.bind(null, name, item);
							var fn = action.bind(null, name, item);
							var template = null;
							var stream = new Queue(name);
							var e;
							var node;
							do {
								if (e = stream.next(), e.type != eventType && (e.type != type || ";" != e.value)) {
									if (e.type == key) {
										var args = e.value;
										fn(stream.next(), type, ":");
										var list = fn(stream.next(), key).value;
										var x = "activate";
										var options = null;
										node = stream.peek();
										if (node.type == type && ("." == node.value && (stream.next(), x = fn(stream.next(), key).value || x, node = stream.peek(), node.type == type && "(" == node.value))) {
											stream.next();
											do {
												if (e = stream.next(), e.type != type || "," != e.value && ")" != e.value) {
													if (e.type == key) {
														node = e.value;
														fn(stream.next(), type, "=");
														var raw = fn(stream.next(true), key).value;
														if (!options) {
															options = defaults();
														}
														options[node] = raw;
														node = stream.peek();
														h(node.type == type && ("," == node.value || ")" == node.value), "Expected either [,] or [)]");
													} else {
														h(false, "; unexpected token [" + (e.value || "") + "]");
													}
												}
											} while (e.type != type || ")" != e.value);
										}
										args = {
											event: args,
											target: list,
											method: x,
											args: options,
											str: name
										};
										if (!template) {
											template = defaults();
										}
										template[args.event] = args;
									} else {
										h(false, "; unexpected token [" + (e.value || "") + "]");
									}
								}
							} while (e.type != eventType);
							name = template;
						}
						item[$name] = name;
					}
					if (root = (item = name) ? item[root] || null : null) {
						data = {
							node: obj,
							actionInfo: root
						};
						break a;
					}
					obj = obj.parentElement;
				}
				data = null;
			}
			if (data) {
				if (obj = this.ampdoc.getElementById(data.actionInfo.target)) {
					callback(this, obj, data.actionInfo.method, data.actionInfo.args, data.node, path, data.actionInfo);
				} else {
					set("target not found", data.actionInfo, obj);
				}
			}
		};
		that.execute = function(context, arg, options, time, path) {
			callback(this, context, arg, options, time, path, null);
		};
		that.installActionHandler = function(node, forOwn) {
			var c = node.tagName + "#" + node.id;
			if (!(node.id && "amp-" == node.id.substring(0, 4))) {
				node.tagName.toLowerCase();
			}
			var events = node.__AMP_ACTION_QUEUE__;
			if (events) {
				eachEvent(events);
			}
			node.__AMP_ACTION_QUEUE__ = {
				push: forOwn
			};
			if (events) {
				equal(node.ownerDocument.defaultView).delay(function() {
					events.forEach(function(attributes) {
						try {
							forOwn(attributes);
						} catch (e) {
							call().error("Action", "Action execution failed:", attributes, e);
						}
					});
				}, 1);
			}
		};
		var eventType = 1;
		var type = 2;
		var key = 3;
		Queue.prototype.next = function(signal_eof) {
			var result = onSuccess(this, signal_eof || false);
			this.dc = result.index;
			return result;
		};
		Queue.prototype.peek = function(array) {
			return onSuccess(this, array || false);
		};
		me.prototype.get = function() {
			if (this.ec) {
				return this.ec;
			}
			var res = this.D;
			var rel = res.getUrl();
			rel = next(rel);
			var parentElement = res.getRootNode();
			var fqstate = parentElement && (parentElement.AMP && parentElement.AMP.canonicalUrl);
			if (!fqstate) {
				var elem = parentElement.querySelector("link[rel=canonical]");
				fqstate = elem ? parseURL(elem.href).href : rel;
			}
			var t = String(Math.floor(1E4 * res.win.Math.random()));
			return this.ec = {
				get sourceUrl() {
					return Ic(a.getUrl())
				},
				canonicalUrl: fqstate,
				pageViewId: t
			};
		};
		Server.prototype.isPending = function() {
			return -1 != this.Z;
		};
		Server.prototype.schedule = function(expectedNumberOfNonCommentArgs) {
			var cosAngle = expectedNumberOfNonCommentArgs || this.ge;
			if (this.ka) {
				if (10 > cosAngle) {
					cosAngle = 10;
				}
			}
			var r11 = Date.now() + cosAngle;
			return !this.isPending() || -10 > r11 - this.Ac ? (this.cancel(), this.Ac = r11, this.Z = this.L.delay(this.de, cosAngle), true) : false;
		};
		Server.prototype.Ha = function() {
			this.Z = -1;
			this.Ac = 0;
			this.ka = true;
			this.oe();
			this.ka = false;
		};
		Server.prototype.cancel = function() {
			if (this.isPending()) {
				this.L.cancel(this.Z);
				this.Z = -1;
			}
		};
		var trace = [/(^|\.)google\.(com?|[a-z]{2}|com?\.[a-z]{2}|cat)$/];
		that = initialize.prototype;
		that.getParam = function(name) {
			return this.u[name];
		};
		that.hasCapability = function(text) {
			var cap = this.u.cap;
			return cap ? -1 != cap.split(",").indexOf(text) : false;
		};
		that.navigateTo = function(url, errorFn) {
			url.indexOf(o.urls.cdn);
			if (this.hasCapability("a2a")) {
				this.sendMessage("a2a", {
					url: url,
					requestedBy: errorFn
				}, false);
			} else {
				this.win.top.location.href = url;
			}
		};
		that.isIframed = function() {
			return this.kc;
		};
		that.isEmbedded = function() {
			return this.qa;
		};
		that.isRuntimeOn = function() {
			return this.W;
		};
		that.isPerformanceTrackingOn = function() {
			return this.Ge && !!this.tb;
		};
		that.toggleRuntime = function() {
			this.W = !this.W;
			this.Hd.fire(this.W);
		};
		that.onRuntimeState = function(ready) {
			return this.Hd.add(ready);
		};
		that.isOvertakeHistory = function() {
			return this.Dc;
		};
		that.getVisibilityState = function() {
			return this.Ob;
		};
		that.Gd = function() {
			stop(this, this.Wd);
		};
		that.isVisible = function() {
			return "visible" == this.getVisibilityState();
		};
		that.hasBeenVisible = function() {
			return this.$b;
		};
		that.whenFirstVisible = function() {
			return this.cf;
		};
		that.getFirstVisibleTime = function() {
			return this.ea;
		};
		that.getPrerenderSize = function() {
			return this.ia;
		};
		that.getPaddingTop = function() {
			return this.m;
		};
		that.getResolvedViewerUrl = function() {
			return this.Bb;
		};
		that.getViewerUrl = function() {
			return this.bf;
		};
		that.maybeGetMessagingOrigin = function() {
			return this.ub;
		};
		that.getUnconfirmedReferrerUrl = function() {
			return this.Mb;
		};
		that.getReferrerUrl = function() {
			return this.Le;
		};
		that.isTrustedViewer = function() {
			return this.oc;
		};
		that.getViewerOrigin = function() {
			return this.af;
		};
		that.onVisibilityChanged = function(cycle) {
			return this.hb.add(cycle);
		};
		that.onViewportEvent = function(ready) {
			return this.Xd.add(ready);
		};
		that.onHistoryPoppedEvent = function(ready) {
			return this.od.add(ready);
		};
		that.postDocumentReady = function() {
			resolve(this, "documentLoaded", {
				title: this.win.document.title,
				sourceUrl: next(this.ampdoc.getUrl())
			}, false);
		};
		that.postScroll = function(x) {
			resolve(this, "scroll", {
				scrollTop: x
			}, false);
		};
		that.requestFullOverlay = function() {
			return resolve(this, "requestFullOverlay", {}, true);
		};
		that.cancelFullOverlay = function() {
			return resolve(this, "cancelFullOverlay", {}, true);
		};
		that.postPushHistory = function(stackIndex) {
			return resolve(this, "pushHistory", {
				stackIndex: stackIndex
			}, true);
		};
		that.postPopHistory = function(dataAndEvents) {
			return resolve(this, "popHistory", {
				stackIndex: dataAndEvents
			}, true);
		};
		that.baseCid = function(url) {
			var self = this;
			return this.isTrustedViewer().then(function(dataAndEvents) {
				if (dataAndEvents) {
					var oldconfig = self.sendMessage("cid", url, true).then(function(data) {
						var callback;
						if (callback = data) {
							var xml;
							a: {
								try {
									xml = JSON.parse(data);
									break a;
								} catch (k) {}
								xml = void 0;
							}
							callback = !xml;
						}
						return callback ? JSON.stringify({
							time: Date.now(),
							cid: data
						}) : data;
					});
					return equal(self.win).timeoutPromise(1E4, oldconfig, "base cid").catch(function(textStatus) {
						call().error("Viewer", textStatus);
					});
				}
			});
		};
		that.getFragment = function() {
			if (!this.qa) {
				var values = this.win.location.hash;
				values = values.substr(1);
				return Promise.resolve(values);
			}
			return this.hasCapability("fragment") ? resolve(this, "fragment", void 0, true).then(function(rgb) {
				return rgb ? rgb.substr(1) : "";
			}) : Promise.resolve("");
		};
		that.updateFragment = function(path) {
			return this.qa ? this.hasCapability("fragment") ? resolve(this, "fragment", {
				fragment: path
			}, true) : Promise.resolve() : (this.win.history.replaceState && this.win.history.replaceState({}, "", path), Promise.resolve());
		};
		that.tick = function(value) {
			resolve(this, "tick", value, false);
		};
		that.flushTicks = function() {
			resolve(this, "sendCsi", void 0, false);
		};
		that.setFlushParams = function(value) {
			resolve(this, "setFlushParams", value, false);
		};
		that.prerenderComplete = function(opt_attributes) {
			resolve(this, "prerenderComplete", opt_attributes, false);
		};
		that.receiveMessage = function(el, data) {
			if ("viewport" == el) {
				if (void 0 !== data.paddingTop) {
					return this.m = data.paddingTop, this.Xd.fire(data), Promise.resolve();
				}
			} else {
				if ("historyPopped" == el) {
					return this.od.fire({
						newStackIndex: data.newStackIndex
					}), Promise.resolve();
				}
				if ("visibilitychange" == el) {
					return void 0 !== data.prerenderSize && (this.ia = data.prerenderSize), stop(this, data.state), Promise.resolve();
				}
				if ("broadcast" == el) {
					return this.dd.fire(data), Promise.resolve();
				}
			}
		};
		that.setMessageDeliverer = function(dataAndEvents, classNames) {
			var element = this;
			if (this.Fa) {
				throw Error("message channel can only be initialized once");
			}
			if (!classNames) {
				throw Error("message channel must have an origin");
			}
			this.Fa = dataAndEvents;
			this.ub = classNames;
			if (this.wc) {
				this.wc();
			}
			if (this.Nc) {
				this.Nc(classNames ? hasClass(this, classNames) : false);
			}
			if (this.Oc) {
				this.Oc(classNames || "");
			}
			if (0 < this.sa.length) {
				classNames = this.sa.slice(0);
				this.sa = [];
				classNames.forEach(function(handler) {
					element.Fa(handler.eventType, handler.data, false);
				});
			}
		};
		that.sendMessage = function(e, opt_attributes, recurring) {
			var suiteView = this;
			return this.vc ? this.vc.then(function() {
				return resolve(suiteView, e, opt_attributes, recurring);
			}) : Promise.reject(assert());
		};
		that.broadcast = function(opt_attributes) {
			resolveOne(this, opt_attributes);
		};
		that.onBroadcast = function(ready) {
			return this.dd.add(ready);
		};
		that.whenMessagingReady = function() {
			return this.tb;
		};
		Klass.prototype.za = function() {
			this.b.za();
		};
		Klass.prototype.push = function(opt_attributes) {
			var item = this;
			return exec(this, function() {
				return item.b.push().then(function(k) {
					item.O(k);
					if (opt_attributes) {
						item.wa[k] = opt_attributes;
					}
					return k;
				});
			});
		};
		Klass.prototype.pop = function(value) {
			var current = this;
			return exec(this, function() {
				return current.b.pop(value).then(function(k) {
					current.O(k);
				});
			});
		};
		Klass.prototype.O = function(data) {
			this.j = data;
			if (!(this.j >= this.wa.length - 1)) {
				data = [];
				var j = this.wa.length - 1;
				for (; j > this.j; j--) {
					if (this.wa[j]) {
						data.push(this.wa[j]);
						this.wa[j] = void 0;
					}
				}
				this.wa.splice(this.j + 1);
				if (0 < data.length) {
					j = 0;
					for (; j < data.length; j++) {
						this.L.delay(data[j], 1);
					}
				}
			}
		};
		that = update.prototype;
		that.za = function() {
			if (this.xb) {
				this.win.history.pushState = this.xb;
			}
			if (this.Wa) {
				this.win.history.replaceState = this.Wa;
			}
			this.win.removeEventListener("popstate", this.Cd);
			this.win.removeEventListener("hashchange", this.nd);
		};
		that.setOnStackIndexUpdated = function(dataAndEvents) {
			this.O = dataAndEvents;
		};
		that.push = function() {
			var me = this;
			return respond(this, function() {
				me.pd();
				return Promise.resolve(me.j);
			});
		};
		that.pop = function(segments) {
			var j = this;
			segments = Math.max(segments, this.xa);
			return respond(this, function() {
				return updateSegment(j, j.j - segments + 1);
			});
		};
		that.backTo = function(maxRows) {
			var j = this;
			maxRows = Math.max(maxRows, this.xa);
			return respond(this, function() {
				return updateSegment(j, j.j - maxRows);
			});
		};
		that.xe = function() {
			var until = this.We ? this.win.history.state : this.Ja;
			var n = until ? until["AMP.History"] : void 0;
			var pdataCur = this.j;
			var dfd = this.Pb;
			this.Pb = void 0;
			if (pdataCur > this.win.history.length - 2) {
				pdataCur = this.win.history.length - 2;
				this.ma(pdataCur);
			}
			pdataCur = void 0 == n ? pdataCur + 1 : n < this.win.history.length ? n : this.win.history.length - 1;
			if (!until) {
				until = {};
			}
			until["AMP.History"] = pdataCur;
			this.Ab(until, void 0, void 0);
			if (pdataCur != this.j) {
				this.ma(pdataCur);
			}
			if (pdataCur < this.xa) {
				this.xa = pdataCur;
			}
			if (dfd) {
				dfd.resolve();
			}
		};
		that.pd = function(e, next_callback, isXML) {
			if (!e) {
				e = {};
			}
			var pdataCur = this.j + 1;
			e["AMP.History"] = pdataCur;
			this.Ie(e, next_callback, isXML);
			if (pdataCur != this.win.history.length - 1) {
				pdataCur = this.win.history.length - 1;
				e["AMP.History"] = pdataCur;
				this.Ab(e);
			}
			this.ma(pdataCur);
		};
		that.pe = function(until, next_callback, isXML) {
			if (!until) {
				until = {};
			}
			var pdataCur = Math.min(this.j, this.win.history.length - 1);
			until["AMP.History"] = pdataCur;
			this.Ab(until, next_callback, isXML);
			this.ma(pdataCur);
		};
		that.ma = function(value) {
			value = Math.min(value, this.win.history.length - 1);
			if (this.j != value) {
				this.j = value;
				if (this.O) {
					this.O(value);
				}
			}
		};
		that = A.prototype;
		that.za = function() {
			this.Ze();
		};
		that.setOnStackIndexUpdated = function(dataAndEvents) {
			this.O = dataAndEvents;
		};
		that.push = function() {
			this.ma(this.j + 1);
			this.a.postPushHistory(this.j);
			return Promise.resolve(this.j);
		};
		that.pop = function(node) {
			if (node > this.j) {
				return Promise.resolve(this.j);
			}
			this.a.postPopHistory(node);
			this.ma(node - 1);
			return Promise.resolve(this.j);
		};
		that.ye = function(dataAndEvents) {
			this.ma(dataAndEvents.newStackIndex);
		};
		that.ma = function(data) {
			if (this.j != data) {
				this.j = data;
				if (this.O) {
					this.O(data);
				}
			}
		};
		that = events.prototype;
		that.isAndroid = function() {
			return /Android/i.test(this.N.userAgent);
		};
		that.isIos = function() {
			return /iPhone|iPad|iPod/i.test(this.N.userAgent);
		};
		that.isSafari = function() {
			return /Safari/i.test(this.N.userAgent) && (!this.isChrome() && !this.isEdge());
		};
		that.isChrome = function() {
			return /Chrome|CriOS/i.test(this.N.userAgent) && !this.isEdge();
		};
		that.isFirefox = function() {
			return /Firefox/i.test(this.N.userAgent) && !this.isEdge();
		};
		that.isIe = function() {
			return /MSIE/i.test(this.N.userAgent) || /IEMobile/i.test(this.N.userAgent);
		};
		that.isEdge = function() {
			return /Edge/i.test(this.N.userAgent);
		};
		that.isWebKit = function() {
			return /WebKit/i.test(this.N.userAgent) && !this.isEdge();
		};
		that.getMajorVersion = function() {
			return this.isSafari() ? layout(this, /\sVersion\/(\d+)/) : this.isChrome() ? layout(this, /\Chrome\/(\d+)/) : this.isFirefox() ? layout(this, /\Firefox\/(\d+)/) : this.isIe() ? layout(this, /\MSIE\s(\d+)/) : this.isEdge() ? layout(this, /\Edge\/(\d+)/) : 0;
		};
		Color.prototype.addTransition = function(fn, src, value) {
			var leaf = fn + "|" + src;
			this.Pd[leaf] = value;
		};
		Color.prototype.setState = function(control) {
			var g = this.g;
			this.g = control;
			if (control = this.Pd[g + "|" + control]) {
				control();
			}
		};
		that = listen.prototype;
		that.za = function() {
			this.win.document.removeEventListener("focus", this.fd, true);
			this.win.removeEventListener("blur", this.ed);
		};
		that.onFocus = function(ready) {
			return this.Bd.add(ready);
		};
		that.getLast = function() {
			return 0 == this.B.length ? null : this.B[this.B.length - 1].el;
		};
		that.purgeBefore = function(time) {
			var index = this.B.length - 1;
			var j = 0;
			for (; j < this.B.length; j++) {
				if (this.B[j].time >= time) {
					index = j - 1;
					break;
				}
			}
			if (-1 != index) {
				this.B.splice(0, index + 1);
			}
		};
		that.hasDescendantsOf = function(a) {
			if (this.win.document.activeElement) {
				dispatch(this, this.win.document.activeElement);
			}
			var i = 0;
			for (; i < this.B.length; i++) {
				if (a.contains(this.B[i].el)) {
					return true;
				}
			}
			return false;
		};
		that = s.prototype;
		that.getId = function() {
			return this.qe;
		};
		that.updateOwner = function(deepDataAndEvents) {
			this.Ga = deepDataAndEvents;
		};
		that.getOwner = function() {
			if (void 0 === this.Ga) {
				var element = this.element;
				for (; element; element = element.parentElement) {
					if (element.__AMP__OWNER) {
						this.Ga = element.__AMP__OWNER;
						break;
					}
				}
				if (void 0 === this.Ga) {
					this.Ga = null;
				}
			}
			return this.Ga;
		};
		that.hasOwner = function() {
			return !!this.getOwner();
		};
		that.getPriority = function() {
			return this.element.getPriority();
		};
		that.getState = function() {
			return this.g;
		};
		that.isBlacklisted = function() {
			return this.Rb;
		};
		that.build = function() {
			if (!this.Rb && this.element.isUpgraded()) {
				try {
					this.element.build();
				} catch (e) {
					call().error("Resource", "failed to build:", this.debugid, e);
					this.Rb = true;
					return;
				}
				if (this.hasBeenMeasured()) {
					this.g = 2;
				} else {
					this.g = 1;
				}
				this.element.dispatchCustomEvent("amp:built");
			}
		};
		that.applySizesAndMediaQuery = function() {
			this.element.applySizesAndMediaQuery();
		};
		that.changeSize = function(type, isXML) {
			this.element.changeSize(type, isXML);
			if (0 != this.g) {
				this.g = 1;
			}
		};
		that.overflowCallback = function(recurring, isXML, deepDataAndEvents) {
			if (recurring) {
				this.Ec = {
					height: isXML,
					width: deepDataAndEvents
				};
			}
			this.element.overflowCallback(recurring, isXML, deepDataAndEvents);
		};
		that.resetPendingChangeSize = function() {
			this.Ec = void 0;
		};
		that.getPendingChangeSize = function() {
			return this.Ec;
		};
		that.measure = function() {
			this.mc = false;
			var l = this.c.getViewport().getLayoutRect(this.element);
			var last = this.K;
			var that = this.c.getViewport();
			this.K = l;
			var nb = false;
			if (this.isDisplayed()) {
				var win = this.c.win;
				var parent = win.document.body;
				var elem = this.element;
				for (; elem && elem != parent; elem = elem.offsetParent) {
					if (elem.isAlwaysFixed && elem.isAlwaysFixed()) {
						nb = true;
						break;
					}
					if (that.isDeclaredFixed(elem) && "fixed" == win.getComputedStyle(elem).position) {
						nb = true;
						break;
					}
				}
			}
			if (this.nb = nb) {
				l = this.K = position(l, -that.getScrollLeft(), -that.getScrollTop());
			}
			if (!(1 != this.g && (last.top == l.top && (last.width == l.width && last.height == l.height)))) {
				if (!!this.element.isUpgraded()) {
					if (!(0 == this.g)) {
						if (!(1 != this.g && !this.element.isRelayoutNeeded())) {
							this.g = 2;
						}
					}
				}
			}
			if (!this.hasBeenMeasured()) {
				this.gc = l;
			}
			this.element.updateLayoutBox(l);
		};
		that.completeCollapse = function() {
			win(this.element);
			this.K = width(this.K.left, this.K.top, 0, 0);
			this.nb = false;
			this.element.updateLayoutBox(this.K);
		};
		that.isMeasureRequested = function() {
			return this.mc;
		};
		that.hasBeenMeasured = function() {
			return !!this.gc;
		};
		that.requestMeasure = function() {
			if (0 != this.g) {
				this.mc = true;
			}
		};
		that.getLayoutBox = function() {
			if (!this.nb) {
				return this.K;
			}
			var cveSession = this.c.getViewport();
			return position(this.K, cveSession.getScrollLeft(), cveSession.getScrollTop());
		};
		that.getInitialLayoutBox = function() {
			return this.gc || this.K;
		};
		that.isDisplayed = function() {
			return 0 < this.K.height && 0 < this.K.width;
		};
		that.isFixed = function() {
			return this.nb;
		};
		that.overlaps = function(a) {
			var b = this.getLayoutBox();
			return b.top <= a.bottom && (a.top <= b.bottom && (b.left <= a.right && a.left <= b.right));
		};
		that.prerenderAllowed = function() {
			return this.element.prerenderAllowed();
		};
		that.renderOutsideViewport = function() {
			if (this.hasOwner()) {
				return true;
			}
			var val = this.element.renderOutsideViewport();
			if (true === val || false === val) {
				return val;
			}
			var a = this.c.getViewport().getRect();
			var b = this.getLayoutBox();
			var d = this.c.getScrollDirection();
			var s = Math.max(val, 0);
			var CONSTRAINTREPORT_ITEMSIZE = 1;
			var i;
			if (a.right < b.left || a.left > b.right) {
				return false;
			}
			if (a.bottom < b.top) {
				i = b.top - a.bottom;
				if (-1 == d) {
					CONSTRAINTREPORT_ITEMSIZE = 2;
				}
			} else {
				if (a.top > b.bottom) {
					i = a.top - b.bottom;
					if (1 == d) {
						CONSTRAINTREPORT_ITEMSIZE = 2;
					}
				} else {
					return true;
				}
			}
			return i < a.height * s / CONSTRAINTREPORT_ITEMSIZE;
		};
		that.layoutScheduled = function() {
			this.g = 3;
		};
		that.startLayout = function(dataAndEvents) {
			var expectationResult = this;
			if (this.Da) {
				return this.Da;
			}
			if (4 == this.g) {
				return Promise.resolve();
			}
			if (5 == this.g) {
				return Promise.reject("already failed");
			}
			if (!dataAndEvents && !this.prerenderAllowed() || !this.isInViewport() && !this.renderOutsideViewport()) {
				return this.g = 2, Promise.resolve();
			}
			this.measure();
			if (!this.isDisplayed()) {
				return Promise.resolve();
			}
			if (0 < this.X && !this.element.isRelayoutNeeded()) {
				return this.g = 4, Promise.resolve();
			}
			this.X++;
			this.g = 3;
			var promise;
			try {
				promise = this.element.layoutCallback();
			} catch (error) {
				return Promise.reject(error);
			}
			return this.Da = promise.then(function() {
				return handleError(expectationResult, true);
			}, function(error) {
				return handleError(expectationResult, false, error);
			});
		};
		that.isLayoutPending = function() {
			return 4 != this.g && 5 != this.g;
		};
		that.loadedOnce = function() {
			return this.te;
		};
		that.hasLoadedOnce = function() {
			return this.vd;
		};
		that.isInViewport = function() {
			return this.fa;
		};
		that.setInViewport = function(recurring) {
			if (recurring != this.fa) {
				this.fa = recurring;
				this.element.viewportCallback(recurring);
			}
		};
		that.unlayout = function() {
			if (0 != this.g) {
				if (1 != this.g) {
					this.setInViewport(false);
					if (this.element.unlayoutCallback()) {
						this.element.togglePlaceholder(true);
						this.g = 1;
						this.X = 0;
						this.Da = null;
					}
				}
			}
		};
		that.getTaskId = function(keepData) {
			return this.debugid + "#" + keepData;
		};
		that.pause = function() {
			if (!(0 == this.g)) {
				if (!this.Ia) {
					this.Ia = true;
					this.setInViewport(false);
					this.element.pauseCallback();
					if (this.element.unlayoutOnPause()) {
						this.unlayout();
					}
				}
			}
		};
		that.pauseOnRemove = function() {
			if (0 != this.g) {
				this.setInViewport(false);
				if (!this.Ia) {
					this.Ia = true;
					this.element.pauseCallback();
				}
			}
		};
		that.resume = function() {
			if (0 != this.g) {
				if (this.Ia) {
					this.Ia = false;
					this.element.resumeCallback();
				}
			}
		};
		that.unload = function() {
			this.pause();
			this.unlayout();
		};
		that = EventEmitter.prototype;
		that.getSize = function() {
			return this.A.length;
		};
		that.getLastEnqueueTime = function() {
			return this.ud;
		};
		that.getLastDequeueTime = function() {
			return this.td;
		};
		that.getTaskById = function(key) {
			return this.cb[key] || null;
		};
		that.enqueue = function(cb) {
			this.A.push(cb);
			this.cb[cb.id] = cb;
			this.ud = Date.now();
		};
		that.dequeue = function(type) {
			var local = this.cb[type.id];
			var charset = this.removeAtIndex(type, this.A.indexOf(local));
			if (!charset) {
				return false;
			}
			this.td = Date.now();
			return true;
		};
		that.peek = function(get) {
			var right = 1E6;
			var result = null;
			var n = 0;
			for (; n < this.A.length; n++) {
				var item = this.A[n];
				var left = get(item);
				if (left < right) {
					right = left;
					result = item;
				}
			}
			return result;
		};
		that.forEach = function(arr) {
			this.A.forEach(arr);
		};
		that.removeAtIndex = function(keepData, idx) {
			var item = this.cb[keepData.id];
			if (!item || this.A[idx] != item) {
				return false;
			}
			this.A.splice(idx, 1);
			delete this.cb[keepData.id];
			return true;
		};
		that.purge = function(test) {
			var i = this.A.length;
			for (; i--;) {
				if (test(this.A[i])) {
					this.removeAtIndex(this.A[i], i);
				}
			}
		};
		that = _init.prototype;
		that.za = function() {
			this.win.document.removeEventListener("keydown", this.Tc);
			this.win.document.removeEventListener("mousedown", this.Uc);
		};
		that.isTouchDetected = function() {
			return this.bc;
		};
		that.onTouchDetected = function(ready, dataAndEvents) {
			if (dataAndEvents) {
				ready(this.isTouchDetected());
			}
			return this.Ye.add(ready);
		};
		that.isMouseDetected = function() {
			return this.ac;
		};
		that.onMouseDetected = function(ready, dataAndEvents) {
			if (dataAndEvents) {
				ready(this.isMouseDetected());
			}
			return this.Ad.add(ready);
		};
		that.isKeyboardActive = function() {
			return this.Va;
		};
		that.onKeyboardStateChanged = function(ready, dataAndEvents) {
			if (dataAndEvents) {
				ready(this.isKeyboardActive());
			}
			return this.qc.add(ready);
		};
		that.ze = function(el) {
			if (!this.Va) {
				if (!el.defaultPrevented) {
					if (!(el = el.target, el && ("INPUT" == el.tagName || ("TEXTAREA" == el.tagName || ("SELECT" == el.tagName || ("OPTION" == el.tagName || el.hasAttribute("contenteditable"))))))) {
						this.Va = true;
						this.qc.fire(true);
					}
				}
			}
		};
		that.Ae = function() {
			if (this.Va) {
				this.Va = false;
				this.qc.fire(false);
			}
		};
		that.Be = function(dataAndEvents) {
			if (dataAndEvents.sourceCapabilities && dataAndEvents.sourceCapabilities.firesTouchEvents) {
				this.yd();
			} else {
				return removeEvent(this.win.document, "click", false, 300).then(this.be, this.ce);
			}
		};
		that.ve = function() {
			this.ac = true;
			this.Ad.fire(true);
		};
		that.yd = function() {
			this.zd++;
			if (3 >= this.zd) {
				addListener(this.win.document, "mousemove", this.Vc);
			}
		};
		that = Range.prototype;
		that.solveYValueFromXValue = function(xVal) {
			return this.getPointY(this.solvePositionFromXValue(xVal));
		};
		that.solvePositionFromXValue = function(xVal) {
			var epsilon = 1E-6;
			var t = (xVal - this.x0) / (this.x3 - this.x0);
			if (0 >= t) {
				return 0;
			}
			if (1 <= t) {
				return 1;
			}
			var a = 0;
			var s = 1;
			var value = 0;
			var h = 0;
			for (; 8 > h; h++) {
				value = this.getPointX(t);
				var derivative = (this.getPointX(t + epsilon) - value) / epsilon;
				if (Math.abs(value - xVal) < epsilon) {
					return t;
				}
				if (Math.abs(derivative) < epsilon) {
					break;
				} else {
					if (value < xVal) {
						a = t;
					} else {
						s = t;
					}
					t -= (value - xVal) / derivative;
				}
			}
			h = 0;
			for (; Math.abs(value - xVal) > epsilon && 8 > h; h++) {
				if (value < xVal) {
					a = t;
					t = (t + s) / 2;
				} else {
					s = t;
					t = (t + a) / 2;
				}
				value = this.getPointX(t);
			}
			return t;
		};
		that.getPointX = function(t) {
			if (0 == t) {
				return this.x0;
			}
			if (1 == t) {
				return this.x3;
			}
			var ix0 = this.lerp(this.x0, this.x1, t);
			var ix1 = this.lerp(this.x1, this.x2, t);
			var ix2 = this.lerp(this.x2, this.x3, t);
			ix0 = this.lerp(ix0, ix1, t);
			ix1 = this.lerp(ix1, ix2, t);
			return this.lerp(ix0, ix1, t);
		};
		that.getPointY = function(t) {
			if (0 == t) {
				return this.y0;
			}
			if (1 == t) {
				return this.y3;
			}
			var iy0 = this.lerp(this.y0, this.y1, t);
			var iy1 = this.lerp(this.y1, this.y2, t);
			var iy2 = this.lerp(this.y2, this.y3, t);
			iy0 = this.lerp(iy0, iy1, t);
			iy1 = this.lerp(iy1, iy2, t);
			return this.lerp(iy0, iy1, t);
		};
		that.lerp = function(a, b, x) {
			return a + x * (b - a);
		};
		var ease = spy(0.25, 0.1, 0.25, 1);
		var actualReturn = spy(0.42, 0, 1, 1);
		var ease_out = spy(0, 0, 0.58, 1);
		var ease_in_out = spy(0.42, 0, 0.58, 1);
		var easing_functions = {
			linear: function(t) {
				return t;
			},
			ease: ease,
			"ease-in": actualReturn,
			"ease-out": ease_out,
			"ease-in-out": ease_in_out
		};
		Game.prototype.setCurve = function(val) {
			if (val) {
				this.jd = remove(val);
			}
			return this;
		};
		Game.prototype.add = function(type, i, animation, node) {
			this.H.push({
				delay: type,
				func: i,
				duration: animation,
				curve: remove(node)
			});
			return this;
		};
		Game.prototype.start = function(d) {
			var started = new start(this.h, this.Pa, this.H, this.jd, d);
			started.Jc();
			return started;
		};
		that = start.prototype;
		that.then = function(fn, callback) {
			return fn || callback ? this.Fd.then(fn, callback) : this.Fd;
		};
		that.thenAlways = function(callback) {
			callback = callback || defaultCompare;
			return this.then(callback, callback);
		};
		that.halt = function(immediate) {
			end(this, false, immediate || 0);
		};
		that.Jc = function() {
			this.Hb = Date.now();
			this.ka = true;
			if (this.h.canAnimate(this.Pa)) {
				this.Od(this.g);
			} else {
				call().warn("Animation", "cannot animate");
				end(this, false, 0);
			}
		};
		that.Ve = function() {
			if (this.ka) {
				var Hb = Date.now();
				var clock = Math.min((Hb - this.Hb) / this.je, 1);
				var dataIndex = 0;
				for (; dataIndex < this.H.length; dataIndex++) {
					var data = this.H[dataIndex];
					if (!data.started) {
						if (clock >= data.delay) {
							data.started = true;
						}
					}
				}
				dataIndex = 0;
				for (; dataIndex < this.H.length; dataIndex++) {
					if (data = this.H[dataIndex], data.started && !data.completed) {
						a: {
							var radixToPower;
							var result;
							if (0 < data.duration) {
								if (result = radixToPower = Math.min((clock - data.delay) / data.duration, 1), data.curve && 1 != result) {
									try {
										result = data.curve(radixToPower);
									} catch (e) {
										call().error("Animation", "step curve failed: " + e, e);
										end(this, false, 0);
										break a;
									}
								}
							} else {
								result = radixToPower = 1;
							}
							if (1 == radixToPower) {
								data.completed = true;
							}
							try {
								data.func(result, data.completed);
							} catch (line) {
								call().error("Animation", "step mutate failed: " + line, line);
								end(this, false, 0);
							}
						}
					}
				}
				if (1 == clock) {
					end(this, true, 0);
				} else {
					if (this.h.canAnimate(this.Pa)) {
						this.Od(this.g);
					} else {
						call().warn("Animation", "cancel animation");
						end(this, false, 0);
					}
				}
			}
		};
		that = Rect.prototype;
		that.setVisible = function(isVisible) {
			var node = this;
			if (this.l) {
				this.h.mutate(function() {
					setStyle(node.l, "visibility", isVisible ? "visible" : "hidden");
				});
			}
		};
		that.setup = function() {
			var codeSegments = this.ampdoc.getRootNode().styleSheets;
			if (codeSegments) {
				var pdataCur = [];
				var i = 0;
				for (; i < codeSegments.length; i++) {
					var s = codeSegments[i];
					if (!s.disabled) {
						if (!!s.ownerNode) {
							if (!("STYLE" != s.ownerNode.tagName)) {
								if (!s.ownerNode.hasAttribute("amp-boilerplate")) {
									if (!s.ownerNode.hasAttribute("amp-runtime")) {
										if (!s.ownerNode.hasAttribute("amp-extension")) {
											assign(this, s.cssRules, pdataCur);
										}
									}
								}
							}
						}
					}
				}
				this.trySetupFixedSelectorsNoInline(pdataCur);
				register(this);
				i = on(this.ampdoc.win);
				if (0 < this.w.length) {
					if (!this.Lb) {
						if (i.isIos()) {
							error().warn("FixedLayer", "Please test this page inside of an AMP Viewer such as Google's because the fixed positioning might have slightly different layout.");
						}
					}
				}
				this.update();
			}
		};
		that.updatePaddingTop = function(recurring, deepDataAndEvents) {
			this.m = recurring;
			if (!deepDataAndEvents) {
				this.hd = recurring;
			}
			this.update();
		};
		that.transformMutate = function(isXML) {
			if (isXML) {
				this.w.forEach(function(el) {
					if (el.fixedNow) {
						if (el.top) {
							setStyle(el.element, "transition", "none");
							if (el.transform && "none" != el.transform) {
								setStyle(el.element, "transform", el.transform + " " + isXML);
							} else {
								setStyle(el.element, "transform", isXML);
							}
						}
					}
				});
			} else {
				this.w.forEach(function(_) {
					if (_.fixedNow) {
						if (_.top) {
							css(_.element, {
								transform: "",
								transition: ""
							});
						}
					}
				});
			}
		};
		that.addElement = function(data) {
			move(this, data, "*");
			register(this);
			this.update();
		};
		that.removeElement = function(element) {
			var suiteView = this;
			var container = play(this, element);
			if (container) {
				if (this.l) {
					this.h.mutate(function() {
						draw(suiteView, container);
					});
				}
			}
		};
		that.isDeclaredFixed = function(second) {
			return !!second.__AMP_DECLFIXED;
		};
		that.update = function() {
			var self = this;
			if (0 == this.w.length) {
				return Promise.resolve();
			}
			var asserterNames = this.w.filter(function(elem) {
				return !self.ampdoc.contains(elem.element);
			});
			asserterNames.forEach(function(item) {
				return play(self, item.element);
			});
			var Lb = false;
			return this.h.runPromise({
				measure: function(runs) {
					var propertiesJson = {};
					self.w.forEach(function(node) {
						setStyle(node.element, "top", "auto");
					});
					self.w.forEach(function(p) {
						propertiesJson[p.id] = p.element.offsetTop;
					});
					self.w.forEach(function(node) {
						setStyle(node.element, "top", "");
					});
					self.w.forEach(function(p) {
						var div = p.element;
						var style = self.ampdoc.win.getComputedStyle(div, null);
						if (style) {
							var oldPosition = style.getPropertyValue("position");
							var value = "fixed" == oldPosition && (0 < div.offsetWidth && 0 < div.offsetHeight);
							if (value) {
								var position = style.getPropertyValue("top");
								var i = div.offsetTop;
								var isDefault = i == propertiesJson[p.id];
								if (!("auto" != position && !isDefault)) {
									if (!("0px" == position)) {
										position = "";
										if (i == self.hd) {
											position = "0px";
										}
									}
								}
								var cDigit = style.getPropertyValue("bottom");
								var version = parseFloat(style.getPropertyValue("opacity"));
								var attrNames = value && (0 < version && (300 > div.offsetHeight && (!!position && 0 == parseInt(position, 10) || !!cDigit && 0 == parseInt(cDigit, 10))));
								if (attrNames) {
									Lb = true;
								}
								runs[p.id] = {
									fixed: value,
									transferrable: attrNames,
									top: position,
									zIndex: style.getPropertyValue("z-index"),
									transform: style.getPropertyValue("transform")
								};
							} else {
								runs[p.id] = {
									fixed: false,
									transferrable: false,
									top: "",
									zIndex: ""
								};
							}
						} else {
							runs[p.id] = {
								fixed: false,
								transferrable: false,
								top: "",
								zIndex: ""
							};
						}
					});
				},
				mutate: function(value) {
					if (Lb && self.Lb) {
						var el = t(self);
						if (el.className != self.ampdoc.getBody().className) {
							el.className = self.ampdoc.getBody().className;
						}
					}
					self.w.forEach(function(options, deepDataAndEvents) {
						var flags = value[options.id];
						if (flags) {
							var args = flags;
							var el = options.element;
							var async = options.fixedNow;
							options.fixedNow = args.fixed;
							options.top = args.fixed ? args.top : "";
							options.transform = args.transform;
							if (args.fixed) {
								setStyle(el, "top", args.top ? "calc(" + args.top + " + " + self.m + "px)" : "");
								if (!async) {
									if (self.Lb) {
										if (args.transferrable) {
											setup(self, options, deepDataAndEvents, args);
										} else {
											draw(self, options);
										}
									}
								}
							} else {
								if (async) {
									if (_getStyle(el, "top")) {
										setStyle(el, "top", "");
									}
									draw(self, options);
								}
							}
						}
					});
				}
			}, {}).catch(function(e) {
				call().error("FixedLayer", "Failed to mutate fixed elements:", e);
			});
		};
		that.trySetupFixedSelectorsNoInline = function(data) {
			try {
				var j = 0;
				for (; j < data.length; j++) {
					var selector = data[j];
					var codeSegments = this.ampdoc.getRootNode().querySelectorAll(selector);
					var i = 0;
					for (; i < codeSegments.length && !(10 < i); i++) {
						move(this, codeSegments[i], selector);
					}
				}
			} catch (e) {
				call().error("FixedLayer", "Failed to setup fixed elements:", e);
			}
		};
		that = errorHandler.prototype;
		that.timeSinceStart = function() {
			return Date.now() - this.Hb;
		};
		that.delay = function(callback, expectedNumberOfNonCommentArgs) {
			var jQuery = this;
			if (!expectedNumberOfNonCommentArgs) {
				var id = "p" + this.Xe++;
				this.Qe.then(function() {
					if (jQuery.Vb[id]) {
						delete jQuery.Vb[id];
					} else {
						callback();
					}
				});
				return id;
			}
			return this.win.setTimeout(callback, expectedNumberOfNonCommentArgs);
		};
		that.cancel = function(name) {
			if ("string" == typeof name) {
				this.Vb[name] = true;
			} else {
				this.win.clearTimeout(name);
			}
		};
		that.promise = function(obj, type) {
			var result = this;
			return new Promise(function(Event) {
				var i = result.delay(function() {
					Event(type);
				}, obj);
				if (-1 == i) {
					throw Error("Failed to schedule timer.");
				}
			});
		};
		that.timeoutPromise = function(expectedNumberOfNonCommentArgs, b, dataAndEvents) {
			var fn = this;
			var a = new Promise(function(dataAndEvents, $sanitize) {
				if (-1 == fn.delay(function() {
						$sanitize(error().createError(dataAndEvents || "timeout"));
					}, expectedNumberOfNonCommentArgs)) {
					throw Error("Failed to schedule timer.");
				}
			});
			return b ? Promise.race([a, b]) : a;
		};
		that = Class.prototype;
		that.Bc = function() {
			if (this.Z) {
				array_to_hash(this);
			}
		};
		that.run = function(opt_attributes, args) {
			this.A.push(opt_attributes);
			this.Kc.push(args || void 0);
			this.ua();
		};
		that.runPromise = function(opt_attributes, args) {
			var piece = this;
			this.run(opt_attributes, args);
			return this.vb ? this.vb : this.vb = new Promise(function(x) {
				piece.xc = x;
			});
		};
		that.createTask = function(ex) {
			var p = this;
			return function(done) {
				p.run(ex, done);
			};
		};
		that.mutate = function(mutation) {
			this.run({
				measure: void 0,
				mutate: mutation
			});
		};
		that.mutatePromise = function(dataAndEvents) {
			return this.runPromise({
				measure: void 0,
				mutate: dataAndEvents
			});
		};
		that.measure = function(measure) {
			this.run({
				measure: measure,
				mutate: void 0
			});
		};
		that.measurePromise = function(values) {
			var parent = this;
			return new Promise(function(arrMin) {
				parent.measure(function() {
					arrMin(values());
				});
			});
		};
		that.canAnimate = function(classNames) {
			return changeClass(this, classNames);
		};
		that.runAnim = function(classNames, opt_attributes, args) {
			if (!changeClass(this, classNames)) {
				return call().warn("VSYNC", "Did not schedule a vsync request, because document was invisible"), false;
			}
			this.run(opt_attributes, args);
			return true;
		};
		that.createAnimTask = function(i, opt_attributes) {
			var me = this;
			return function(argv) {
				return me.runAnim(i, opt_attributes, argv);
			};
		};
		that.runAnimMutateSeries = function(classNames, max, maxHeight) {
			var context = this;
			return changeClass(this, classNames) ? new Promise(function(forEach, done) {
				var clientY = Date.now();
				var delta = 0;
				var onMove = context.createAnimTask(classNames, {
					mutate: function(ev) {
						var y = Date.now() - clientY;
						if (max(y, y - delta, ev)) {
							if (maxHeight && y > maxHeight) {
								done(Error("timeout"));
							} else {
								delta = y;
								onMove(ev);
							}
						} else {
							forEach();
						}
					}
				});
				onMove({});
			}) : Promise.reject(Error("CANCELLED"));
		};
		that.ua = function() {
			if (!this.Z) {
				this.Z = true;
				array_to_hash(this);
			}
		};
		that.Se = function() {
			this.Z = false;
			var codeSegments = this.A;
			var parts = this.Kc;
			var throttledUpdate = this.xc;
			this.vb = this.xc = null;
			this.A = this.zc;
			this.Kc = this.yc;
			var i = 0;
			for (; i < codeSegments.length; i++) {
				if (codeSegments[i].measure) {
					if (!getNext(codeSegments[i].measure, parts[i])) {
						codeSegments[i].mutate = void 0;
					}
				}
			}
			i = 0;
			for (; i < codeSegments.length; i++) {
				if (codeSegments[i].mutate) {
					getNext(codeSegments[i].mutate, parts[i]);
				}
			}
			this.zc = codeSegments;
			this.yc = parts;
			this.zc.length = 0;
			this.yc.length = 0;
			if (throttledUpdate) {
				throttledUpdate();
			}
		};
		that = f.prototype;
		that.dispose = function() {
			this.b.disconnect();
		};
		that.ensureReadyForElements = function() {
			this.b.ensureReadyForElements();
		};
		that.Sd = function() {
			var T = this.a.isVisible();
			if (T != this.T) {
				if (this.T = T) {
					this.b.connect();
				} else {
					this.b.disconnect();
				}
			}
		};
		that.getPaddingTop = function() {
			return this.m;
		};
		that.getTop = function() {
			return this.getScrollTop();
		};
		that.getScrollTop = function() {
			if (null == this.va) {
				this.va = this.b.getScrollTop();
			}
			return this.va;
		};
		that.getScrollLeft = function() {
			if (null == this.Cb) {
				this.Cb = this.b.getScrollLeft();
			}
			return this.Cb;
		};
		that.setScrollTop = function(inTop) {
			this.va = null;
			this.b.setScrollTop(inTop);
		};
		that.updatePaddingBottom = function(val) {
			this.ampdoc.whenBodyAvailable().then(function(el) {
				setStyle(el, "borderBottom", val + "px solid transparent");
			});
		};
		that.getSize = function() {
			return this.ab ? this.ab : this.ab = this.b.getSize();
		};
		that.getHeight = function() {
			return this.getSize().height;
		};
		that.getWidth = function() {
			return this.getSize().width;
		};
		that.getScrollWidth = function() {
			return this.b.getScrollWidth();
		};
		that.getScrollHeight = function() {
			return this.b.getScrollHeight();
		};
		that.getRect = function() {
			if (null == this.Ya) {
				var udataCur = this.getScrollTop();
				var x = this.getScrollLeft();
				var size = this.getSize();
				this.Ya = width(x, udataCur, size.width, size.height);
			}
			return this.Ya;
		};
		that.getLayoutRect = function(bb) {
			var c = this.getScrollLeft();
			var y = this.getScrollTop();
			var bbox = getComputedStyle(bb, this.ampdoc.win);
			return bbox ? (bb = this.b.getLayoutRect(bb, 0, 0), c = this.b.getLayoutRect(bbox, c, y), width(Math.round(bb.left + c.left), Math.round(bb.top + c.top), Math.round(bb.width), Math.round(bb.height))) : this.b.getLayoutRect(bb, c, y);
		};
		that.isDeclaredFixed = function(elems) {
			return this.l.isDeclaredFixed(elems);
		};
		that.scrollIntoView = function(bb) {
			var rectTop = this.b.getLayoutRect(bb).top;
			var inTop = Math.max(0, rectTop - this.m);
			this.b.setScrollTop(inTop);
		};
		that.animateScrollIntoView = function(bb, object, v) {
			object = void 0 === object ? 500 : object;
			v = void 0 === v ? "ease-in" : v;
			var updated_doc = this;
			bb = this.b.getLayoutRect(bb).top;
			bb = Math.max(0, bb - this.m);
			var value = this.getScrollTop();
			if (bb == value) {
				return Promise.resolve();
			}
			var HOP = flatten(value, bb);
			return isValid(this.ampdoc.getRootNode(), function(walkers) {
				updated_doc.b.setScrollTop(HOP(walkers));
			}, object, v).then();
		};
		that.onChanged = function(cycle) {
			return this.gd.add(cycle);
		};
		that.onScroll = function(cycle) {
			return this.S.add(cycle);
		};
		that.enterLightboxMode = function() {
			var updated_doc = this;
			this.a.requestFullOverlay();
			this.disableTouchZoom();
			this.hideFixedLayer();
			this.h.mutate(function() {
				return updated_doc.b.updateLightboxMode(true);
			});
		};
		that.leaveLightboxMode = function() {
			var updated_doc = this;
			this.a.cancelFullOverlay();
			this.showFixedLayer();
			this.restoreOriginalTouchZoom();
			this.h.mutate(function() {
				return updated_doc.b.updateLightboxMode(false);
			});
		};
		that.resetTouchZoom = function() {
			var restoreOriginalTouchZoom = this;
			var a = this.ampdoc.win.innerHeight;
			var b = this.lb.documentElement.clientHeight;
			if (!(a && (b && a === b))) {
				if (this.disableTouchZoom()) {
					this.L.delay(function() {
						restoreOriginalTouchZoom.restoreOriginalTouchZoom();
					}, 50);
				}
			}
		};
		that.disableTouchZoom = function() {
			var value = proceed(this);
			if (!value) {
				return false;
			}
			var style = value.content;
			var props = {
				"maximum-scale": "1",
				"user-scalable": "no"
			};
			var blocks = Object.create(null);
			if (style) {
				var codeSegments = style.split(/,|;/);
				var i = 0;
				for (; i < codeSegments.length; i++) {
					var variable = codeSegments[i].split("=");
					var key = variable[0].trim();
					variable = variable[1];
					variable = (variable || "").trim();
					if (key) {
						blocks[key] = variable;
					}
				}
			}
			codeSegments = false;
			var k;
			for (k in props) {
				if (blocks[k] !== props[k]) {
					codeSegments = true;
					if (void 0 !== props[k]) {
						blocks[k] = props[k];
					} else {
						delete blocks[k];
					}
				}
			}
			if (codeSegments) {
				style = "";
				var name;
				for (name in blocks) {
					if (0 < style.length) {
						style += ",";
					}
					style = blocks[name] ? style + (name + "=" + blocks[name]) : style + name;
				}
			}
			blocks = style;
			return setContent(this, blocks);
		};
		that.restoreOriginalTouchZoom = function() {
			return void 0 !== this.Cc ? setContent(this, this.Cc) : false;
		};
		that.hasScrolled = function() {
			return 0 < this.Id;
		};
		that.hideFixedLayer = function() {
			this.l.setVisible(false);
		};
		that.showFixedLayer = function() {
			this.l.setVisible(true);
		};
		that.updateFixedLayer = function() {
			this.l.update();
		};
		that.addToFixedLayer = function(inplace) {
			this.l.addElement(inplace);
		};
		that.removeFromFixedLayer = function(element) {
			this.l.removeElement(element);
		};
		that.$e = function(opts) {
			var matrix = this;
			var m = opts.paddingTop;
			var instance = opts.duration || 0;
			var model = opts.curve;
			var origContext = opts["transient"];
			if (m != this.m) {
				this.pb = this.m;
				this.m = m;
				if (this.m < this.pb) {
					this.b.hideViewerHeader(origContext, this.pb);
					cast(this, instance, model, origContext);
				} else {
					cast(this, instance, model, origContext).then(function() {
						matrix.b.showViewerHeader(origContext, matrix.m);
					});
				}
			}
		};
		that.Te = function() {
			var data_priv = this;
			this.Ya = null;
			this.Id++;
			this.Cb = this.b.getScrollLeft();
			var pdataOld = this.b.getScrollTop();
			if (!(0 > pdataOld)) {
				this.va = pdataOld;
				if (!this.Gc) {
					this.Gc = true;
					var oldconfig = Date.now();
					this.L.delay(function() {
						data_priv.h.measure(function() {
							data_priv.Mc(oldconfig, pdataOld);
						});
					}, 36);
				}
				this.S.fire();
			}
		};
		that.Mc = function(b, value) {
			var that = this;
			var d = this.va = this.b.getScrollTop();
			var a = Date.now();
			var distY = 0;
			if (a != b) {
				distY = (d - value) / (a - b);
			}
			if (0.03 > Math.abs(distY)) {
				up(this, false, distY);
				this.Gc = false;
			} else {
				this.L.delay(function() {
					return that.h.measure(that.Mc.bind(that, a, d));
				}, 20);
			}
		};
		that.Ue = function() {
			var doc = this;
			if (!this.Fc) {
				this.Fc = true;
				this.h.measure(function() {
					doc.Fc = false;
					doc.a.postScroll(doc.getScrollTop());
				});
			}
		};
		that.Oe = function() {
			var SELF = this;
			this.Ya = null;
			var innerSize = this.ab;
			this.ab = null;
			var outerSize = this.getSize();
			this.l.update().then(function() {
				up(SELF, !innerSize || innerSize.width != outerSize.width, 0);
			});
		};
		that = attachEvents.prototype;
		that.connect = function() {
			this.win.addEventListener("scroll", this.Oa);
			this.win.addEventListener("resize", this.Na);
		};
		that.disconnect = function() {
			this.win.removeEventListener("scroll", this.Oa);
			this.win.removeEventListener("resize", this.Na);
		};
		that.ensureReadyForElements = function() {};
		that.requiresFixedLayerTransfer = function() {
			return false;
		};
		that.onScroll = function(cycle) {
			this.S.add(cycle);
		};
		that.onResize = function(ready) {
			this.ja.add(ready);
		};
		that.updatePaddingTop = function(recurring) {
			setStyle(this.win.document.documentElement, "paddingTop", recurring + "px");
		};
		that.hideViewerHeader = function(context) {
			if (!context) {
				this.updatePaddingTop(0);
			}
		};
		that.showViewerHeader = function(context, recurring) {
			if (!context) {
				this.updatePaddingTop(recurring);
			}
		};
		that.updateLightboxMode = function() {};
		that.getSize = function() {
			var w = this.win.innerWidth;
			var h = this.win.innerHeight;
			if (w && h) {
				return {
					width: w,
					height: h
				};
			}
			var D = this.win.document.documentElement;
			return {
				width: D.clientWidth,
				height: D.clientHeight
			};
		};
		that.getScrollTop = function() {
			return jQuery(this).scrollTop || this.win.pageYOffset;
		};
		that.getScrollLeft = function() {
			return jQuery(this).scrollLeft || this.win.pageXOffset;
		};
		that.getScrollWidth = function() {
			return jQuery(this).scrollWidth;
		};
		that.getScrollHeight = function() {
			return jQuery(this).scrollHeight;
		};
		that.getLayoutRect = function(bb, v23, v1) {
			var y0 = void 0 != v1 ? v1 : this.getScrollTop();
			var x0 = void 0 != v23 ? v23 : this.getScrollLeft();
			bb = bb.getBoundingClientRect();
			return width(Math.round(bb.left + x0), Math.round(bb.top + y0), Math.round(bb.width), Math.round(bb.height));
		};
		that.setScrollTop = function(inTop) {
			jQuery(this).scrollTop = inTop;
		};
		that = resize.prototype;
		that.ensureReadyForElements = function() {};
		that.requiresFixedLayerTransfer = function() {
			return true;
		};
		that.Db = function() {
			var el = this.win.document.body;
			css(this.win.document.documentElement, {
				overflowY: "auto",
				webkitOverflowScrolling: "touch"
			});
			css(el, {
				overflowX: "hidden",
				overflowY: "auto",
				webkitOverflowScrolling: "touch",
				position: "absolute",
				top: 0,
				left: 0,
				right: 0,
				bottom: 0
			});
			this.ba = this.win.document.createElement("div");
			this.ba.id = "-amp-scrollpos";
			css(this.ba, {
				position: "absolute",
				top: 0,
				left: 0,
				width: 0,
				height: 0,
				visibility: "hidden"
			});
			el.appendChild(this.ba);
			this.la = this.win.document.createElement("div");
			this.la.id = "-amp-scrollmove";
			css(this.la, {
				position: "absolute",
				top: 0,
				left: 0,
				width: 0,
				height: 0,
				visibility: "hidden"
			});
			el.appendChild(this.la);
			this.Ba = this.win.document.createElement("div");
			this.Ba.id = "-amp-endpos";
			css(this.Ba, {
				width: 0,
				height: 0,
				visibility: "hidden"
			});
			el.appendChild(this.Ba);
			el.addEventListener("scroll", this.wb.bind(this));
			go(this.ampdoc);
		};
		that.connect = function() {};
		that.disconnect = function() {};
		that.hideViewerHeader = function(context, dataAndEvents) {
			var self = this;
			if (context) {
				after(this.win.document, function(sprite) {
					var d = self.win.getComputedStyle(sprite.body)["padding-top"] || "0";
					css(sprite.body, {
						paddingTop: "calc(" + d + " + " + dataAndEvents + "px)",
						borderTop: ""
					});
				});
			} else {
				this.updatePaddingTop(0);
			}
		};
		that.showViewerHeader = function(context, recurring) {
			if (!context) {
				this.updatePaddingTop(recurring);
			}
		};
		that.updatePaddingTop = function(recurring) {
			var matrix = this;
			after(this.win.document, function(args) {
				matrix.m = recurring;
				css(args.body, {
					borderTop: recurring + "px solid transparent",
					paddingTop: ""
				});
			});
		};
		that.updateLightboxMode = function(recurring) {
			after(this.win.document, function(element) {
				setStyle(element.body, "borderTopStyle", recurring ? "none" : "solid");
			});
		};
		that.onScroll = function(cycle) {
			this.S.add(cycle);
		};
		that.onResize = function(ready) {
			this.ja.add(ready);
		};
		that.getSize = function() {
			return {
				width: this.win.innerWidth,
				height: this.win.innerHeight
			};
		};
		that.getScrollTop = function() {
			return Math.round(this.ha.y);
		};
		that.getScrollLeft = function() {
			return Math.round(this.ha.x);
		};
		that.getScrollWidth = function() {
			return this.win.innerWidth;
		};
		that.getScrollHeight = function() {
			return this.Ba ? Math.round(this.Ba.getBoundingClientRect().top - this.ba.getBoundingClientRect().top) : 0;
		};
		that.getLayoutRect = function(bb) {
			bb = bb.getBoundingClientRect();
			return width(Math.round(bb.left + this.ha.x), Math.round(bb.top + this.ha.y), Math.round(bb.width), Math.round(bb.height));
		};
		that.setScrollTop = function(inTop) {
			_detect3DTransforms(this, inTop || 1);
		};
		that.wb = function(cp) {
			if (this.ba) {
				if (this.ba && (this.la && (0 == -this.ba.getBoundingClientRect().top + this.m && (_detect3DTransforms(this, 1), cp && cp.preventDefault()))), cp = this.ba.getBoundingClientRect(), this.ha.x != -cp.left || this.ha.y != -cp.top) {
					this.ha.x = -cp.left;
					this.ha.y = -cp.top + this.m;
					this.S.fire();
				}
			}
		};
		that = parse.prototype;
		that.ensureReadyForElements = function() {
			this.Db();
		};
		that.Db = function() {
			if (!this.Kd) {
				this.Kd = true;
				var doc = this.win.document;
				var b = doc.body;
				doc.documentElement.appendChild(this.M);
				this.M.appendChild(b);
				Object.defineProperty(doc, "body", {
					get: function() {
						return b;
					}
				});
				this.wb();
			}
		};
		that.connect = function() {
			this.win.addEventListener("resize", this.Na);
			this.M.addEventListener("scroll", this.Oa);
		};
		that.disconnect = function() {
			this.win.removeEventListener("resize", this.Na);
			this.M.removeEventListener("scroll", this.Oa);
		};
		that.requiresFixedLayerTransfer = function() {
			return true;
		};
		that.onScroll = function(cycle) {
			this.S.add(cycle);
		};
		that.onResize = function(ready) {
			this.ja.add(ready);
		};
		that.updatePaddingTop = function(recurring) {
			setStyle(this.M, "paddingTop", recurring + "px");
		};
		that.hideViewerHeader = function(context) {
			if (!context) {
				this.updatePaddingTop(0);
			}
		};
		that.showViewerHeader = function(context, recurring) {
			if (!context) {
				this.updatePaddingTop(recurring);
			}
		};
		that.updateLightboxMode = function() {};
		that.getSize = function() {
			return {
				width: this.win.innerWidth,
				height: this.win.innerHeight
			};
		};
		that.getScrollTop = function() {
			return this.M.scrollTop;
		};
		that.getScrollLeft = function() {
			return this.M.scrollLeft;
		};
		that.getScrollWidth = function() {
			return this.M.scrollWidth;
		};
		that.getScrollHeight = function() {
			return this.M.scrollHeight;
		};
		that.getLayoutRect = function(bb, left, dy) {
			dy = void 0 != dy ? dy : this.getScrollTop();
			left = void 0 != left ? left : this.getScrollLeft();
			bb = bb.getBoundingClientRect();
			return width(Math.round(bb.left + left), Math.round(bb.top + dy), Math.round(bb.width), Math.round(bb.height));
		};
		that.setScrollTop = function(inTop) {
			this.M.scrollTop = inTop || 1;
		};
		that.wb = function(types) {
			if (0 == this.M.scrollTop) {
				this.M.scrollTop = 1;
				if (types) {
					types.preventDefault();
				}
			}
			if (types) {
				this.S.fire();
			}
		};
		that = run.prototype;
		that.get = function() {
			return this.c.slice(0);
		};
		that.isRuntimeOn = function() {
			return this.W;
		};
		that.getResourcesInViewport = function(dataAndEvents) {
			dataAndEvents = dataAndEvents || false;
			var tail = this.s.getRect();
			return this.c.filter(function(me) {
				return me.hasOwner() || (!me.isDisplayed() || (!me.overlaps(tail) || dataAndEvents && !me.prerenderAllowed())) ? false : true;
			});
		};
		that.getMaxDpr = function() {
			return this.xd;
		};
		that.getDpr = function() {
			return this.xd;
		};
		that.getResourceForElement = function(name) {
			return get_mangled(name);
		};
		that.getViewport = function() {
			return this.s;
		};
		that.getScrollDirection = function() {
			return Math.sign(this.rc) || 1;
		};
		that.add = function(type) {
			this.Sc++;
			if (1 == this.Sc) {
				this.s.ensureReadyForElements();
			}
			var attributes = new s(++this.Re, type, this);
			if (!type.id) {
				type.id = "AMP_" + attributes.getId();
			}
			this.c.push(attributes);
			addClass(this, attributes);
		};
		that.remove = function(name) {
			if (name = get_mangled(name)) {
				removeListener(this, name);
			}
		};
		that.removeForChildWindow = function(dataAndEvents) {
			var basePrototype = this;
			this.c.filter(function(deepDataAndEvents) {
				return deepDataAndEvents.hostWin == dataAndEvents;
			}).forEach(function(eventType) {
				return removeListener(basePrototype, eventType);
			});
		};
		that.upgraded = function(name) {
			name = get_mangled(name);
			addClass(this, name);
		};
		that.setOwner = function(name, deepDataAndEvents) {
			deepDataAndEvents.contains(name);
			if (get_mangled(name)) {
				get_mangled(name).updateOwner(deepDataAndEvents);
			}
			name.__AMP__OWNER = deepDataAndEvents;
		};
		that.scheduleLayout = function(name, dataAndEvents) {
			parallel(this, get_mangled(name), true, check_kb(dataAndEvents));
		};
		that.schedulePause = function(name, ev) {
			var suiteView = get_mangled(name);
			ev = check_kb(ev);
			rename(this, suiteView, ev, function(gridStore) {
				gridStore.pause();
			});
		};
		that.scheduleResume = function(name, ev) {
			name = get_mangled(name);
			ev = check_kb(ev);
			rename(this, name, ev, function(res) {
				res.resume();
			});
		};
		that.scheduleUnlayout = function(name, ev) {
			name = get_mangled(name);
			ev = check_kb(ev);
			rename(this, name, ev, function(dataAndEvents) {
				dataAndEvents.unlayout();
			});
		};
		that.schedulePreload = function(name, deepDataAndEvents) {
			parallel(this, get_mangled(name), false, check_kb(deepDataAndEvents));
		};
		that.updateInViewport = function(name, dataAndEvents, deepDataAndEvents) {
			removeNode(this, get_mangled(name), check_kb(dataAndEvents), deepDataAndEvents);
		};
		that.changeSize = function(name, isXML, opt_attributes, deepDataAndEvents) {
			getter(this, get_mangled(name), isXML, opt_attributes, true, deepDataAndEvents);
		};
		that.attemptChangeSize = function(name, deepDataAndEvents, opt_attributes) {
			var suiteView = this;
			return new Promise(function($sanitize, done) {
				getter(suiteView, get_mangled(name), deepDataAndEvents, opt_attributes, false, function(dataAndEvents) {
					if (dataAndEvents) {
						$sanitize();
					} else {
						done(Error("changeSize attempt denied"));
					}
				});
			});
		};
		that.deferMutate = function(name, attributes) {
			get_mangled(name);
			this.Aa.push(attributes);
			this.schedulePassVsync();
		};
		that.mutateElement = function(bb, compareFn) {
			function position() {
				var alignToBox = tree.s.getLayoutRect(bb);
				return 0 != alignToBox.width && 0 != alignToBox.height ? alignToBox.top : -1;
			}
			var tree = this;
			var k = -1;
			return this.h.runPromise({
				measure: function() {
					k = position();
				},
				mutate: function() {
					compareFn();
					if (bb.classList.contains("-amp-element")) {
						get_mangled(bb).requestMeasure();
					}
					var codeSegments = bb.getElementsByClassName("-amp-element");
					var i = 0;
					for (; i < codeSegments.length; i++) {
						get_mangled(codeSegments[i]).requestMeasure();
					}
					if (-1 != k) {
						walk(tree, k);
					}
					tree.schedulePass(70);
					tree.h.measure(function() {
						var oldconfig = position();
						if (-1 != oldconfig) {
							if (oldconfig != k) {
								walk(tree, oldconfig);
								tree.schedulePass(70);
							}
						}
					});
				}
			});
		};
		that.collapseElement = function(bb) {
			var boundingBox = this.s.getLayoutRect(bb);
			var f_b = get_mangled(bb);
			if (0 != boundingBox.width) {
				if (0 != boundingBox.height) {
					walk(this, boundingBox.top);
				}
			}
			f_b.completeCollapse();
			if (boundingBox = f_b.getOwner()) {
				boundingBox.collapsedCallback(bb);
			}
			this.schedulePass(70);
		};
		that.schedulePass = function(expectedNumberOfNonCommentArgs, dataAndEvents) {
			if (dataAndEvents) {
				this.ta = true;
			}
			return this.Ha.schedule(expectedNumberOfNonCommentArgs);
		};
		that.schedulePassVsync = function() {
			var memory = this;
			if (!this.Pc) {
				this.Pc = true;
				this.h.mutate(function() {
					return toggle(memory);
				});
			}
		};
		that.Ne = function(cb) {
			if (!this.F.getTaskById(cb.id)) {
				this.F.enqueue(cb);
			}
		};
		that.Nd = function(node, dataAndEvents, reason) {
			this.$.dequeue(node);
			this.schedulePass(1E3);
			if (!dataAndEvents) {
				return call().info("Resources", "task failed:", node.id, node.resource.debugid, reason), Promise.reject(reason);
			}
		};
		that.ua = function(options, key, outstandingDataSize, rh, callback) {
			key = options.getTaskId(key);
			options = {
				id: key,
				resource: options,
				priority: Math.max(options.getPriority(), rh) + outstandingDataSize,
				callback: callback,
				scheduleTime: Date.now(),
				startTime: 0,
				promise: null
			};
			var e = this.F.getTaskById(key);
			if (!e || options.priority < e.priority) {
				if (e) {
					this.F.dequeue(e);
				}
				this.F.enqueue(options);
				this.schedulePass(loop(this, options));
			}
			options.resource.layoutScheduled();
		};
		that.unselectText = function() {
			try {
				this.win.getSelection().removeAllRanges();
			} catch (a) {}
		};
		Map.prototype.handleHide = function(ev) {
			var result = ev.target;
			this.c.mutateElement(result, function() {
				if (result.classList.contains("-amp-element")) {
					result.collapse();
				} else {
					win(result);
				}
			});
		};
		that = val.prototype;
		that.Jc = function() {
			filterWithQueryAndMatcher(this);
			return this;
		};
		that.get = function(name) {
			return poll(this).then(function($templateCache) {
				return $templateCache.get(name);
			});
		};
		that.set = function(obj, value) {
			return destroy(this, function(results) {
				return results.set(obj, value);
			});
		};
		that.remove = function(key) {
			return destroy(this, function(selfObj) {
				return selfObj.remove(key);
			});
		};
		that.ee = function() {
			this.a.broadcast({
				type: "amp-storage-reset",
				origin: this.yb
			});
		};
		Assertion.prototype.get = function(name) {
			return (name = this.ca[name]) ? name.v : void 0;
		};
		Assertion.prototype.set = function(item, value) {
			if (void 0 !== this.ca[item]) {
				item = this.ca[item];
				item.v = value;
				item.t = Date.now();
			} else {
				this.ca[item] = {
					v: value,
					t: Date.now()
				};
			}
			value = Object.keys(this.ca);
			if (value.length > this.ue) {
				var n = Infinity;
				var target = null;
				item = 0;
				for (; item < value.length; item++) {
					var result = this.ca[value[item]];
					if (result.t < n) {
						target = value[item];
						n = result.t;
					}
				}
				if (target) {
					delete this.ca[target];
				}
			}
		};
		Assertion.prototype.remove = function(selector) {
			delete this.ca[selector];
		};
		Table.prototype.loadBlob = function(oid) {
			var $ = this;
			return new Promise(function($sanitize) {
				if ($.lc) {
					$sanitize($.win.localStorage.getItem("amp-store:" + oid));
				} else {
					$sanitize(null);
				}
			});
		};
		Table.prototype.saveBlob = function(oid, jsonString) {
			var $ = this;
			return new Promise(function($sanitize) {
				if ($.lc) {
					$.win.localStorage.setItem("amp-store:" + oid, jsonString);
				}
				$sanitize();
			});
		};
		x.prototype.loadBlob = function(origin) {
			return this.a.sendMessage("loadStore", {
				origin: origin
			}, true).then(function(fileOrBlobData) {
				return fileOrBlobData.blob;
			});
		};
		x.prototype.saveBlob = function(origin, v02) {
			return this.a.sendMessage("saveStore", {
				origin: origin,
				blob: v02
			}, true);
		};
		Session.prototype.register = function(event) {
			if (event.element.hasAttribute("autoplay") && event.supportsPlatform()) {
				this.Sa = this.Sa || [];
				var attributes = new completed(this.D, event);
				show(this, attributes);
				this.Sa.push(attributes);
			}
		};
		completed.prototype.updateVisibility = function() {
			function push() {
				if (suiteView.ga != ga) {
					if (suiteView.wd) {
						if (suiteView.Zb) {
							isUndefined(suiteView);
						}
					}
				}
			}

			function ll_LogoLoader() {
				if (suiteView.video.isInViewport()) {
					var form = suiteView.video.element.getIntersectionChangeEntry();
					var b = isArrayLike(form.intersectionRatio) ? 100 * form.intersectionRatio : 0;
					suiteView.ga = 75 <= b;
				} else {
					suiteView.ga = false;
				}
			}
			var suiteView = this;
			var ga = this.ga;
			this.h.run({
				measure: ll_LogoLoader,
				mutate: push
			});
		};
		var instance = null;
		var methods = ["GET", "POST"];
		var array = [eachEvent, isString];
		var defaultMock = {
			document: 1,
			text: 2,
			arraybuffer: 3
		};
		that = source.prototype;
		that.le = function(value, options) {
			return options && "document" == options.responseType ? ajax(value, options) : (this.win.fetch || ajax).apply(null, arguments);
		};
		that.fetchJson = function(path, options) {
			options = options || {};
			options.method = String(options.method);
			sendRequest(options);
			return get(this, path, options).then(function(err) {
				return fetch(err);
			}).then(function(res) {
				return res.json();
			});
		};
		that.fetchText = function(url, req) {
			req = req || {};
			req.method = String(req.method);
			req.headers = req.headers || {};
			req.headers.Accept = "text/plain";
			return get(this, url, req).then(function(err) {
				return fetch(err);
			}).then(function(script) {
				return script.text();
			});
		};
		that.fetchDocument = function(baseUrl, req) {
			req = req || {};
			req.responseType = "document";
			req.method = String(req.method);
			req.headers = req.headers || {};
			req.headers.Accept = "text/html";
			return get(this, baseUrl, req).then(function(err) {
				return fetch(err);
			}).then(function(neighbor) {
				return neighbor.G();
			});
		};
		that.fetch = function(target, req) {
			req = req || {};
			req.responseType = "arraybuffer";
			req.method = String(req.method);
			req.headers = req.headers || {};
			return get(this, target, req).then(function(err) {
				return fetch(err);
			});
		};
		that.sendSignal = function(baseUrl, walkers) {
			return get(this, baseUrl, walkers).then(function(err) {
				return fetch(err);
			});
		};
		that.getCorsUrl = function(c, id) {
			makeModuleMap(id);
			c = parseURL(next(c.location.href)).origin;
			c = encodeURIComponent("__amp_source_origin") + "=" + encodeURIComponent(c);
			return decode(id, c, void 0);
		};
		module.prototype.text = function() {
			return interpolate(this);
		};
		module.prototype.json = function() {
			return interpolate(this).then(JSON.parse.bind(JSON));
		};
		module.prototype.G = function() {
			this.bodyUsed = true;
			error().assert(this.na.responseXML, "responseXML should exist. Make sure to return Content-Type: text/html header.");
			return Promise.resolve(this.na.responseXML);
		};
		module.prototype.arrayBuffer = function() {
			error().assert(this.na.response, "arrayBuffer response should exist.");
			this.bodyUsed = true;
			return Promise.resolve(this.na.response);
		};
		Store.prototype.get = function(name) {
			return this.na.getResponseHeader(name);
		};
		that = Component.prototype;
		that.hc = function() {
			var me = this;
			this.qd = true;
			var that = each(this.ampdoc);
			valueOf(this, "RANDOM", function() {
				return Math.random();
			});
			var buf = null;
			valueOf(this, "COUNTER", function(off) {
				if (!buf) {
					buf = Object.create(null);
				}
				if (!buf[off]) {
					buf[off] = 0;
				}
				return ++buf[off];
			});
			valueOf(this, "CANONICAL_URL", this.V.bind(this, function(dataAndEvents) {
				return dataAndEvents.canonicalUrl;
			}));
			valueOf(this, "CANONICAL_HOST", this.V.bind(this, function(settings) {
				return (settings = parseURL(settings.canonicalUrl)) && settings.host;
			}));
			valueOf(this, "CANONICAL_HOSTNAME", this.V.bind(this, function(settings) {
				return (settings = parseURL(settings.canonicalUrl)) && settings.hostname;
			}));
			valueOf(this, "CANONICAL_PATH", this.V.bind(this, function(pieces) {
				return (pieces = parseURL(pieces.canonicalUrl)) && pieces.pathname;
			}));
			is(this, "DOCUMENT_REFERRER", function() {
				return split(me.ampdoc).getReferrerUrl();
			});
			valueOf(this, "TITLE", function() {
				return me.ampdoc.win.document.title;
			});
			valueOf(this, "AMPDOC_URL", function() {
				return merge(me.ampdoc.win.location.href);
			});
			valueOf(this, "AMPDOC_HOST", function() {
				var config = parseURL(me.ampdoc.win.location.href);
				return config && config.host;
			});
			valueOf(this, "AMPDOC_HOSTNAME", function() {
				var url = parseURL(me.ampdoc.win.location.href);
				return url && url.hostname;
			});
			valueOf(this, "SOURCE_URL", this.V.bind(this, function(context) {
				return merge(context.sourceUrl);
			}));
			is(this, "SOURCE_URL", function() {
				return p.then(function() {
					return me.V(function(context) {
						return merge(context.sourceUrl);
					});
				});
			});
			valueOf(this, "SOURCE_HOST", this.V.bind(this, function(options) {
				return parseURL(options.sourceUrl).host;
			}));
			valueOf(this, "SOURCE_HOSTNAME", this.V.bind(this, function(options) {
				return parseURL(options.sourceUrl).hostname;
			}));
			valueOf(this, "SOURCE_PATH", this.V.bind(this, function(options) {
				return parseURL(options.sourceUrl).pathname;
			}));
			valueOf(this, "PAGE_VIEW_ID", this.V.bind(this, function(dataAndEvents) {
				return dataAndEvents.pageViewId;
			}));
			valueOf(this, "QUERY_PARAM", function(target, value) {
				value = void 0 === value ? "" : value;
				return insert(me, target, value);
			});
			is(this, "QUERY_PARAM", function(target, value) {
				value = void 0 === value ? "" : value;
				return p.then(function() {
					return insert(me, target, value);
				});
			});
			var object = null;
			is(this, "CLIENT_ID", function(key, optgroup) {
				error().assertString(key, "The first argument to CLIENT_ID, the fallback cookie name, is required");
				var camelKey = Promise.resolve();
				if (optgroup) {
					camelKey = sequence(me.ampdoc.win, "userNotificationManager", "amp-user-notification").then(function(doc) {
						return doc.get(optgroup);
					});
				}
				return sequence(me.ampdoc.win, "cid", "amp-analytics").then(function(data_user) {
					return data_user.get({
						scope: key,
						createCookieIfNotPresent: true
					}, camelKey);
				}).then(function(val) {
					if (!object) {
						object = Object.create(null);
					}
					return object[key] = val;
				});
			});
			valueOf(this, "CLIENT_ID", function(property) {
				return object ? object[property] : null;
			});
			is(this, "VARIANT", function(line) {
				return me.Vd.then(function(data) {
					error().assert(data, "To use variable VARIANT, amp-experiment should be configured");
					var value = data[line];
					error().assert(void 0 !== value, "The value passed to VARIANT() is not a valid experiment name:" + line);
					return null === value ? "none" : value;
				});
			});
			is(this, "VARIANTS", function() {
				return me.Vd.then(function(obj) {
					error().assert(obj, "To use variable VARIANTS, amp-experiment should be configured");
					var tagNameArr = [];
					var prop;
					for (prop in obj) {
						tagNameArr.push(prop + "." + (obj[prop] || "none"));
					}
					return tagNameArr.join("!");
				});
			});
			is(this, "SHARE_TRACKING_INCOMING", function() {
				return me.Ld.then(function(express) {
					error().assert(express, "To use variable SHARE_TRACKING_INCOMING, amp-share-tracking should be configured");
					return express.incomingFragment;
				});
			});
			is(this, "SHARE_TRACKING_OUTGOING", function() {
				return me.Ld.then(function(express) {
					error().assert(express, "To use variable SHARE_TRACKING_OUTGOING, amp-share-tracking should be configured");
					return express.outgoingFragment;
				});
			});
			valueOf(this, "TIMESTAMP", function() {
				return Date.now();
			});
			valueOf(this, "TIMEZONE", function() {
				return (new Date).getTimezoneOffset();
			});
			valueOf(this, "SCROLL_TOP", function() {
				return that.getScrollTop();
			});
			valueOf(this, "SCROLL_LEFT", function() {
				return that.getScrollLeft();
			});
			valueOf(this, "SCROLL_HEIGHT", function() {
				return that.getScrollHeight();
			});
			valueOf(this, "SCROLL_WIDTH", function() {
				return that.getScrollWidth();
			});
			valueOf(this, "VIEWPORT_HEIGHT", function() {
				return that.getSize().height;
			});
			valueOf(this, "VIEWPORT_WIDTH", function() {
				return that.getSize().width;
			});
			valueOf(this, "SCREEN_WIDTH", function() {
				return me.ampdoc.win.screen.width;
			});
			valueOf(this, "SCREEN_HEIGHT", function() {
				return me.ampdoc.win.screen.height;
			});
			valueOf(this, "AVAILABLE_SCREEN_HEIGHT", function() {
				return me.ampdoc.win.screen.availHeight;
			});
			valueOf(this, "AVAILABLE_SCREEN_WIDTH", function() {
				return me.ampdoc.win.screen.availWidth;
			});
			valueOf(this, "SCREEN_COLOR_DEPTH", function() {
				return me.ampdoc.win.screen.colorDepth;
			});
			valueOf(this, "DOCUMENT_CHARSET", function() {
				var doc = me.ampdoc.win.document;
				return doc.characterSet || doc.charset;
			});
			valueOf(this, "BROWSER_LANGUAGE", function() {
				var nav = me.ampdoc.win.navigator;
				return (nav.language || (nav.userLanguage || (nav.browserLanguage || ""))).toLowerCase();
			});
			text(this, "PAGE_LOAD_TIME", "navigationStart", "loadEventStart");
			text(this, "DOMAIN_LOOKUP_TIME", "domainLookupStart", "domainLookupEnd");
			text(this, "TCP_CONNECT_TIME", "connectStart", "connectEnd");
			text(this, "SERVER_RESPONSE_TIME", "requestStart", "responseStart");
			text(this, "PAGE_DOWNLOAD_TIME", "responseStart", "responseEnd");
			text(this, "REDIRECT_TIME", "navigationStart", "fetchStart");
			text(this, "DOM_INTERACTIVE_TIME", "navigationStart", "domInteractive");
			text(this, "CONTENT_LOAD_TIME", "navigationStart", "domContentLoadedEventStart");
			is(this, "ACCESS_READER_ID", function() {
				return activate(me, function(dataAndEvents) {
					return dataAndEvents.getAccessReaderId();
				}, "ACCESS_READER_ID");
			});
			is(this, "AUTHDATA", function(express) {
				error().assert(express, "The first argument to AUTHDATA, the field, is required");
				return activate(me, function(test) {
					return test.getAuthdataField(express);
				}, "AUTHDATA");
			});
			is(this, "VIEWER", function() {
				return split(me.ampdoc).getViewerOrigin().then(function(mount) {
					return void 0 == mount ? "" : mount;
				});
			});
			is(this, "TOTAL_ENGAGED_TIME", function() {
				return sequence(me.ampdoc.win, "activity", "amp-analytics").then(function(dataAndEvents) {
					return dataAndEvents.getTotalEngagedTime();
				});
			});
			valueOf(this, "NAV_TIMING", function(express, variables) {
				error().assert(express, "The first argument to NAV_TIMING, the start attribute name, is required");
				return evaluate(me, express, variables);
			});
			is(this, "NAV_TIMING", function(obj, next_callback) {
				error().assert(obj, "The first argument to NAV_TIMING, the start attribute name, is required");
				return map(me, obj, next_callback);
			});
			valueOf(this, "NAV_TYPE", function() {
				return simulateKeyEvent(me, "type");
			});
			valueOf(this, "NAV_REDIRECT_COUNT", function() {
				return simulateKeyEvent(me, "redirectCount");
			});
			valueOf(this, "AMP_VERSION", function() {
				return "1478801557976";
			});
			valueOf(this, "BACKGROUND_STATE", function() {
				return split(me.ampdoc.win.document).isVisible() ? "0" : "1";
			});
		};
		that.V = function(callback) {
			return callback(confirm(this.ampdoc));
		};
		that.expandSync = function(data, replacementHash, flags, result) {
			return bind(this, data, replacementHash, flags, true, result);
		};
		that.expandAsync = function(d, index) {
			return bind(this, d, index);
		};
		that.maybeExpandLink = function(a) {
			if (func(this.ampdoc.win, "link-url-replace")) {
				var buf = a.getAttribute("data-amp-replace");
				if (buf) {
					var result = confirm(this.ampdoc);
					var url = a["amp-original-href"] || a.getAttribute("href");
					var file = parseURL(url);
					if (file.origin != parseURL(result.canonicalUrl).origin && file.origin != parseURL(result.sourceUrl).origin) {
						error().warn("URL", "Ignoring link replacement", url, " because the link does not go to the document's source or canonical origin.");
					} else {
						if (null == a["amp-original-href"]) {
							a["amp-original-href"] = url;
						}
						var actual = {
							CLIENT_ID: true,
							QUERY_PARAM: true
						};
						var expectationResult = {};
						buf.trim().split(/\s*,\s*/).forEach(function(key) {
							if (actual.hasOwnProperty(key)) {
								expectationResult[key] = true;
							} else {
								error().warn("URL", "Ignoring unsupported link replacement", key);
							}
						});
						return a.href = this.expandSync(url, void 0, void 0, expectationResult);
					}
				}
			}
		};
		that.collectVars = function(data, handler) {
			var count = Object.create(null);
			return bind(this, data, handler, count).then(function() {
				return count;
			});
		};
		(function() {
			User = test;
			call();
			error();
		})();
		var obj;
		try {
			onComplete();
			obj = promote();
		} catch (a) {
			throw drag(self.document), a;
		}
		validate(self.document, function init() {
			var e = obj.getAmpDoc(self.document);
			var api = extend(self, "performance", message);
			api.tick("is");
			loadStyleSheet(self.document, "html{overflow-x:hidden!important}body,html{height:auto!important}body{margin:0!important;-webkit-text-size-adjust:100%;-moz-text-size-adjust:100%;-ms-text-size-adjust:100%;text-size-adjust:100%}html.-amp-ios-embed{position:static}html.-amp-ios-embed,i-amp-html-wrapper{overflow-y:auto!important;-webkit-overflow-scrolling:touch!important}i-amp-html-wrapper{overflow-x:hidden!important;position:absolute!important;top:0!important;left:0!important;right:0!important;bottom:0!important;margin:0!important;display:block!important}i-amp-html-wrapper>body{position:relative!important;display:block!important;border-top:1px solid transparent!important}.-amp-make-body-block body{display:block!important}.-amp-element{display:inline-block}.-amp-layout-fixed{display:inline-block;position:relative}.-amp-layout-container,.-amp-layout-fixed-height,.-amp-layout-responsive{display:block;position:relative}.-amp-layout-fill{display:block;overflow:hidden!important;position:absolute;top:0;left:0;bottom:0;right:0}.-amp-layout-flex-item{display:block;position:relative;-webkit-box-flex:1;-webkit-flex:1 1 auto;-ms-flex:1 1 auto;flex:1 1 auto}.-amp-layout-size-defined{overflow:hidden!important}i-amp-sizer{display:block!important}.-amp-fill-content{display:block;width:1px;min-width:100%;height:100%;margin:auto}.-amp-layout-size-defined .-amp-fill-content{position:absolute;top:0;left:0;bottom:0;right:0}.-amp-replaced-content,.-amp-screen-reader{padding:0!important;border:none!important}.-amp-screen-reader{position:fixed!important;top:0px!important;left:0px!important;width:2px!important;height:2px!important;opacity:0!important;overflow:hidden!important;margin:0!important;display:block!important;visibility:visible!important}.-amp-unresolved{position:relative;overflow:hidden!important}.-amp-notbuilt{position:relative;overflow:hidden!important;color:transparent!important}.-amp-notbuilt>*{display:none}.-amp-ghost{visibility:hidden!important}.-amp-element>[placeholder]{display:block}.-amp-element>[placeholder].amp-hidden,.-amp-element>[placeholder].hidden{visibility:hidden}.-amp-element:not(.amp-notsupported)>[fallback]{display:none}.-amp-layout-size-defined>[fallback],.-amp-layout-size-defined>[placeholder]{position:absolute!important;top:0!important;left:0!important;right:0!important;bottom:0!important;z-index:1!important}.-amp-notbuilt>[placeholder]{display:block!important}.-amp-hidden-by-media-query{display:none}.-amp-element-error{background:red!important;color:#fff!important;position:relative!important}.-amp-element-error:before{content:attr(error-message)}i-amp-scroll-container{position:absolute;top:0;left:0;right:0;bottom:0;display:block}i-amp-scroll-container.amp-active{overflow:auto}.-amp-loading-container{display:block!important;z-index:1}.-amp-notbuilt>.-amp-loading-container{display:block!important}.-amp-loading-container.amp-hidden{visibility:hidden}.-amp-loader{position:absolute;display:block;height:10px;top:50%;left:50%;-webkit-transform:translateX(-50%) translateY(-50%);transform:translateX(-50%) translateY(-50%);-webkit-transform-origin:50% 50%;transform-origin:50% 50%;white-space:nowrap}.-amp-loader.amp-active .-amp-loader-dot{-webkit-animation:a 2s infinite;animation:a 2s infinite}.-amp-loader-dot{position:relative;display:inline-block;height:10px;width:10px;margin:2px;border-radius:100%;background-color:rgba(0,0,0,.3);box-shadow:2px 2px 2px 1px rgba(0,0,0,.2);will-change:transform}.-amp-loader .-amp-loader-dot:nth-child(1){-webkit-animation-delay:0s;animation-delay:0s}.-amp-loader .-amp-loader-dot:nth-child(2){-webkit-animation-delay:.1s;animation-delay:.1s}.-amp-loader .-amp-loader-dot:nth-child(3){-webkit-animation-delay:.2s;animation-delay:.2s}@-webkit-keyframes a{0%,to{-webkit-transform:scale(.7);transform:scale(.7);background-color:rgba(0,0,0,.3)}50%{-webkit-transform:scale(.8);transform:scale(.8);background-color:rgba(0,0,0,.5)}}@keyframes a{0%,to{-webkit-transform:scale(.7);transform:scale(.7);background-color:rgba(0,0,0,.3)}50%{-webkit-transform:scale(.8);transform:scale(.8);background-color:rgba(0,0,0,.5)}}.-amp-element>[overflow]{cursor:pointer;z-index:2;visibility:hidden}.-amp-element>[overflow].amp-visible{visibility:visible}template{display:none!important}.amp-border-box,.amp-border-box *,.amp-border-box :after,.amp-border-box :before{box-sizing:border-box}amp-pixel{position:fixed!important;top:0!important;width:1px!important;height:1px!important;overflow:hidden!important;visibility:hidden}amp-instagram{padding:48px 8px!important;background-color:#fff}amp-analytics{position:fixed!important;top:0!important;width:1px!important;height:1px!important;overflow:hidden!important;visibility:hidden}amp-iframe iframe{box-sizing:border-box!important}[amp-access][amp-access-hide],amp-experiment,amp-live-list>[update],amp-share-tracking,form [submit-error],form [submit-success]{display:none}amp-fresh{visibility:hidden}.amp-video-eq{-webkit-box-align:end;-webkit-align-items:flex-end;-ms-flex-align:end;align-items:flex-end;bottom:7px;display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;height:12px;opacity:0.8;overflow:hidden;position:absolute;right:7px;width:20px}.amp-video-eq .amp-video-eq-col{-webkit-box-flex:1;-webkit-flex:1;-ms-flex:1;flex:1;height:100%;margin-right:1px;position:relative}.amp-video-eq .amp-video-eq-col div{-webkit-animation-name:b;animation-name:b;-webkit-animation-timing-function:linear;animation-timing-function:linear;-webkit-animation-iteration-count:infinite;animation-iteration-count:infinite;-webkit-animation-direction:alternate;animation-direction:alternate;background-color:#fafafa;height:100%;position:absolute;width:100%;will-change:transform;-webkit-animation-play-state:paused;animation-play-state:paused}.amp-video-eq[unpausable] .amp-video-eq-col div{-webkit-animation-name:none;animation-name:none}.amp-video-eq[unpausable].amp-video-eq-play .amp-video-eq-col div{-webkit-animation-name:b;animation-name:b}.amp-video-eq.amp-video-eq-play .amp-video-eq-col div{-webkit-animation-play-state:running;animation-play-state:running}.amp-video-eq-1-1{-webkit-animation-duration:0.3s;animation-duration:0.3s}.amp-video-eq-1-1,.amp-video-eq-1-2{-webkit-transform:translateY(60%);transform:translateY(60%)}.amp-video-eq-1-2{-webkit-animation-duration:0.45s;animation-duration:0.45s}.amp-video-eq-2-1{-webkit-animation-duration:0.5s;animation-duration:0.5s}.amp-video-eq-2-1,.amp-video-eq-2-2{-webkit-transform:translateY(30%);transform:translateY(30%)}.amp-video-eq-2-2{-webkit-animation-duration:0.4s;animation-duration:0.4s}.amp-video-eq-3-1{-webkit-animation-duration:0.3s;animation-duration:0.3s}.amp-video-eq-3-1,.amp-video-eq-3-2{-webkit-transform:translateY(70%);transform:translateY(70%)}.amp-video-eq-3-2{-webkit-animation-duration:0.35s;animation-duration:0.35s}.amp-video-eq-4-1{-webkit-animation-duration:0.4s;animation-duration:0.4s}.amp-video-eq-4-1,.amp-video-eq-4-2{-webkit-transform:translateY(50%);transform:translateY(50%)}.amp-video-eq-4-2{-webkit-animation-duration:0.25s;animation-duration:0.25s}@-webkit-keyframes b{0%{-webkit-transform:translateY(100%);transform:translateY(100%)}to{-webkit-transform:translateY(0);transform:translateY(0)}}@keyframes b{0%{-webkit-transform:translateY(100%);transform:translateY(100%)}to{-webkit-transform:translateY(0);transform:translateY(0)}}\n/*# sourceURL=/css/amp.css*/",
				function() {
					validate(self.document, function() {
						setter(self);
						process();
						parseInt(e, "documentInfo", me);
						sendMessage(e, void 0);
						onload(e);
						change(e);
						parseInt(e, "resources", run);
						parseInt(e, "url-replace", Component);
						parseInt(e, "action", Window);
						parseInt(e, "standard-actions", Map);
						_track(e);
						parseInt(e, "video-manager", Session);
						if (func(e.win, "form-submit")) {
							createProxy(e);
						}
						api.coreServicesAvailable();
						create();
					});
					validate(self.document, function init() {
						var SELF = self;
						define(SELF, "amp-img", c);
						proxy(SELF);
						SMSound(SELF);
					});
					validate(self.document, function completed() {
						ready();
					});
					validate(self.document, function() {
						main(self);
					});
					validate(self.document, function setup() {
						var failuresLink = self;
						if ("0" == split(failuresLink.document).getParam("p2r")) {
							if (on(failuresLink).isChrome()) {
								new Editor(failuresLink.document, each(failuresLink.document));
							}
						}
						parseInt(e, "clickhandler", Tabs);
						loadScript();
						drag(self.document, true);
						request();
					});
					validate(self.document, function init() {
						api.tick("e_is");
						api.flush();
					});
				}, true, "amp-runtime");
		});
		if (self.console) {
			(console.info || console.log).call(console, "Powered by AMP \u26a1 HTML \u2013 Version 1478801557976", self.location.href);
		}
		self.document.documentElement.setAttribute("amp-version", "1478801557976");
	})();
} catch (e$$154) {
	setTimeout(function() {
		var style = document.body.style;
		style.opacity = 1;
		style.visibility = "visible";
		style.animation = "none";
		style.WebkitAnimation = "none;";
	}, 1E3);
	throw e$$154;
};