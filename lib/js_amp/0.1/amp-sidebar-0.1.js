(self.AMP = self.AMP || []).push({
	n: "amp-sidebar",
	f: (function(AMP) {
		function g(a, b) {
			function c() {}
			c.prototype = b.prototype;
			a.prototype = new c;
			a.prototype.constructor = a;
			for (var d in b)
				if (Object.defineProperties) {
					var e = Object.getOwnPropertyDescriptor(b, d);
					e && Object.defineProperty(a, d, e)
				} else a[d] = b[d]
		};
		Date.now();
		self.log = self.log || {
			user: null,
			dev: null
		};

		function h(a, b) {
			a = a.__AMP_TOP || a;
			var c = a.services && a.services[b] && a.services[b].obj;
			return c
		}

		function k(a) {
			var b = a;
			if (b.nodeType) {
				var c = (b.ownerDocument || b).defaultView,
					d = c = c.__AMP_TOP || c,
					e = c.services;
				e || (e = c.services = {});
				c = e;
				(e = c.ampdoc) || (e = c.ampdoc = {
					obj: null,
					promise: null,
					resolve: null
				});
				e.obj || (e.obj = (void 0)(d), e.resolve && e.resolve(e.obj));
				b = e.obj.getAmpDoc(b)
			}
			var f = b.isSingleDoc() ? b.win : b;
			return f && f.services && f.services.history && f.services.history.obj
		};
		var l, m = "Webkit webkit Moz moz ms O o".split(" ");

		function n(a, b) {
			var c = void 0;
			l || (l = Object.create(null));
			var d = l[b];
			if (!d || c) {
				d = b;
				if (void 0 === a[b]) {
					var e = b.charAt(0).toUpperCase() + b.slice(1);
					a: {
						for (var f = 0; f < m.length; f++) {
							var r = m[f] + e;
							if (void 0 !== a[r]) {
								e = r;
								break a
							}
						}
						e = ""
					}
					var t = e;
					void 0 !== a[t] && (d = t)
				}
				c || (l[b] = d)
			}
			return d
		}

		function p(a, b, c) {
			var d;
			(b = n(a.style, b)) && (a.style[b] = d ? c + d : c)
		}

		function q(a, b) {
			if (void 0 === b) {
				var c;
				c = (c = n(a.style, "display")) ? a.style[c] : void 0;
				b = "none" == c
			}
			p(a, "display", b ? "" : "none")
		};

		function u(a) {
			AMP.BaseElement.call(this, a);
			this.h = null;
			this.m = h(this.win, "vsync");
			this.f = null;
			this.c = this.win.document;
			this.s = this.c.documentElement;
			this.g = null;
			var b = h(this.win, "platform");
			this.u = b.isIos() && b.isSafari();
			this.i = -1;
			this.o = !1;
			this.l = h(this.win, "timer");
			this.b = null
		}
		g(u, AMP.BaseElement);
		u.prototype.isLayoutSupported = function(a) {
			return "nodisplay" == a
		};
		u.prototype.buildCallback = function() {
			var a = this;
			this.g = this.element.getAttribute("side");
			this.h = this.getViewport();
			if ("left" != this.g && "right" != this.g) {
				var b = this.c.body.getAttribute("dir") || this.s.getAttribute("dir") || "ltr";
				this.g = "rtl" == b ? "right" : "left";
				this.element.setAttribute("side", this.g)
			}
			this.u && v(this);
			w(this) ? this.j() : this.element.setAttribute("aria-hidden", "true");
			this.element.hasAttribute("role") || this.element.setAttribute("role", "menu");
			this.element.tabIndex = -1;
			this.s.addEventListener("keydown",
				function(b) {
					27 == b.keyCode && a.a()
				});
			var c = this.c.createElement("button");
			c.textContent = "Close the sidebar";
			c.classList.add("-amp-screen-reader");
			c.tabIndex = -1;
			c.addEventListener("click", function() {
				a.a()
			});
			this.element.appendChild(c);
			this.registerAction("toggle", this.v.bind(this));
			this.registerAction("open", this.j.bind(this));
			this.registerAction("close", this.a.bind(this))
		};

		function w(a) {
			return a.element.hasAttribute("open")
		}
		u.prototype.activate = function() {
			this.j()
		};
		u.prototype.v = function() {
			w(this) ? this.a() : this.j()
		};
		u.prototype.j = function() {
			var a = this;
			w(this) || (this.h.disableTouchZoom(), this.m.mutate(function() {
				q(a.element, !0);
				a.h.addToFixedLayer(a.element);
				x(a);
				a.u && y(a);
				a.element.scrollTop = 1;
				a.m.mutate(function() {
					a.element.setAttribute("open", "");
					a.element.setAttribute("aria-hidden", "false");
					try {
						a.element.focus()
					} catch (b) {}
					a.b && a.l.cancel(a.b);
					a.b = a.l.delay(function() {
						var b = a.getRealChildren();
						a.scheduleLayout(b);
						a.scheduleResume(b)
					}, 550)
				})
			}), k(this.getAmpDoc()).push(this.a.bind(this)).then(function(b) {
				a.i = b
			}))
		};
		u.prototype.a = function() {
			var a = this;
			w(this) && (this.h.restoreOriginalTouchZoom(), this.m.mutate(function() {
				a.f && q(a.f, !1);
				a.element.removeAttribute("open");
				a.element.setAttribute("aria-hidden", "true");
				a.b && a.l.cancel(a.b);
				a.b = a.l.delay(function() {
					w(a) || (a.h.removeFromFixedLayer(a.element), a.m.mutate(function() {
						q(a.element, !1);
						a.schedulePause(a.getRealChildren())
					}))
				}, 550)
			}), -1 != this.i && (k(this.getAmpDoc()).pop(this.i), this.i = -1))
		};

		function x(a) {
			if (!a.f) {
				var b = a.c.createElement("div");
				b.classList.add("-amp-sidebar-mask");
				b.addEventListener("click", function() {
					a.a()
				});
				a.element.parentNode.appendChild(b);
				b.addEventListener("touchmove", function(a) {
					a.preventDefault()
				});
				a.f = b
			}
			q(a.f, !0)
		}

		function v(a) {
			a.element.addEventListener("scroll", function(b) {
				w(a) && (1 > a.element.scrollTop ? (a.element.scrollTop = 1, b.preventDefault()) : a.element.scrollHeight == a.element.scrollTop + a.element.offsetHeight && (--a.element.scrollTop, b.preventDefault()))
			})
		}

		function y(a) {
			if (!a.o) {
				var b = a.c.createElement("div"),
					c = {
						height: "10vh",
						width: "100%",
						"background-color": "transparent"
					},
					d;
				for (d in c) p(b, d, c[d]);
				a.element.appendChild(b);
				a.o = !0
			}
		}
		AMP.registerElement("amp-sidebar", u, "amp-sidebar{position:fixed!important;top:0;max-height:100vh!important;height:100vh;max-width:80vw!important;background-color:#efefef;min-width:45px!important;outline:none;overflow-x:hidden!important;overflow-y:auto!important;z-index:9999!important;-webkit-overflow-scrolling:touch;will-change:transform}amp-sidebar[side=left]{left:0!important;-webkit-transform:translateX(-100%)!important;transform:translateX(-100%)!important}amp-sidebar[side=right]{right:0!important;-webkit-transform:translateX(100%)!important;transform:translateX(100%)!important}amp-sidebar[side][open]{-webkit-transform:translateX(0)!important;transform:translateX(0)!important}amp-sidebar[side]{-webkit-transition:-webkit-transform 233ms cubic-bezier(0,0,.21,1);transition:-webkit-transform 233ms cubic-bezier(0,0,.21,1);transition:transform 233ms cubic-bezier(0,0,.21,1);transition:transform 233ms cubic-bezier(0,0,.21,1),-webkit-transform 233ms cubic-bezier(0,0,.21,1)}.-amp-sidebar-mask{position:fixed!important;top:0!important;left:0!important;width:100vw!important;height:100vh!important;opacity:0.2;background-image:none!important;background-color:#000;z-index:9998!important}\n/*# sourceURL=/extensions/amp-sidebar/0.1/amp-sidebar.css*/");
	})
});
//# sourceMappingURL=amp-sidebar-0.1.js.map