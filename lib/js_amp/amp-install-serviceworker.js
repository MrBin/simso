(self.AMP = self.AMP || []).push({
	n: "amp-install-serviceworker",
	f: (function(AMP) {
		function g(a, b) {
			function c() {}
			c.prototype = b.prototype;
			a.prototype = new c;
			a.prototype.constructor = a;
			for (var d in b)
				if (Object.defineProperties) {
					var e = Object.getOwnPropertyDescriptor(b, d);
					e && Object.defineProperty(a, d, e)
				} else a[d] = b[d]
		}
		var k = "";

		function l(a) {
			var b = Object.create(null);
			if (!a) return b;
			if (0 == a.indexOf("?") || 0 == a.indexOf("#")) a = a.substr(1);
			for (var c = a.split("&"), d = 0; d < c.length; d++) {
				var e = c[d],
					f = e.indexOf("="),
					h, r; - 1 != f ? (h = decodeURIComponent(e.substring(0, f)).trim(), r = decodeURIComponent(e.substring(f + 1)).trim()) : (h = decodeURIComponent(e).trim(), r = "");
				h && (b[h] = r)
			}
			return b
		};
		Date.now();
		self.log = self.log || {
			user: null,
			dev: null
		};
		var m = self.log;

		function n() {
			if (m.user) return m.user;
			throw Error("failed to call initLogConstructor");
		};

		function p(a) {
			a = a.__AMP_TOP || a;
			var b = a.services && a.services.timer && a.services.timer.obj;
			return b
		}

		function q(a, b) {
			var c = a;
			if (c.nodeType) {
				var d = (c.ownerDocument || c).defaultView,
					e = d = d.__AMP_TOP || d,
					f = d.services;
				f || (f = d.services = {});
				d = f;
				(f = d.ampdoc) || (f = d.ampdoc = {
					obj: null,
					promise: null,
					resolve: null
				});
				f.obj || (f.obj = (void 0)(e), f.resolve && f.resolve(f.obj));
				c = f.obj.getAmpDoc(c)
			}
			var h = c.isSingleDoc() ? c.win : c;
			return h && h.services && h.services[b] && h.services[b].obj
		};

		function t(a, b) {
			for (var c = a; c; c = c.parentElement)
				if (b(c)) return c;
			return null
		}

		function u(a) {
			var b = "A";
			if (a.closest) return a.closest(b);
			b = b.toUpperCase();
			return t(a, function(a) {
				return a.tagName == b
			})
		};
		var v = (self.AMP_CONFIG || {}).cdnUrl || "https://cdn.ampproject.org";
		var w, x;

		function y(a) {
			var b;
			w || (w = self.document.createElement("a"), x = self.UrlCache || (self.UrlCache = Object.create(null)));
			var c = x[a];
			if (c) return c;
			w.href = a;
			w.protocol || (w.href = w.href);
			var d = {
				href: w.href,
				protocol: w.protocol,
				host: w.host,
				hostname: w.hostname,
				port: "0" == w.port ? "" : w.port,
				pathname: w.pathname,
				search: w.search,
				hash: w.hash,
				origin: null
			};
			"/" !== d.pathname[0] && (d.pathname = "/" + d.pathname);
			if ("http:" == d.protocol && 80 == d.port || "https:" == d.protocol && 443 == d.port) d.port = "", d.host = d.hostname;
			d.origin = w.origin && "null" !=
				w.origin ? w.origin : "data:" != d.protocol && d.host ? d.protocol + "//" + d.host : d.href;
			var e = d;
			return b ? e : x[a] = e
		}

		function z(a) {
			"string" == typeof a && (a = y(a));
			var b;
			(b = "https:" == a.protocol || "localhost" == a.hostname) || (a = a.hostname, b = a.length - 10, b = 0 <= b && a.indexOf(".localhost", b) == b);
			return b
		}

		function A(a, b) {
			var c;
			c = void 0 === c ? "source" : c;
			n().assert(null != a, "%s %s must be available", b, c);
			var d = a;
			n().assert(z(d) || /^(\/\/)/.test(d), '%s %s must start with "https://" or "//" or be relative and served from either https or from localhost. Invalid value: %s', b, c, d)
		}

		function B(a) {
			var b = a.indexOf("#");
			return -1 == b ? a : a.substring(0, b)
		}

		function C(a) {
			"string" == typeof a && (a = y(a));
			var b = a.pathname.split("/")[1];
			return a.origin == v || 0 == a.origin.indexOf("http://localhost:") && ("c" == b || "v" == b)
		};
		var D, E = "Webkit webkit Moz moz ms O o".split(" ");

		function F(a) {
			var b = void 0;
			D || (D = Object.create(null));
			var c = D.display;
			if (!c || b) {
				c = "display";
				if (void 0 === a.display) {
					var d = "D" + "display".slice(1);
					a: {
						for (var e = 0; e < E.length; e++) {
							var f = E[e] + d;
							if (void 0 !== a[f]) {
								d = f;
								break a
							}
						}
						d = ""
					}
					var h = d;
					void 0 !== a[h] && (c = h)
				}
				b || (D.display = c)
			}
			return c
		}

		function G(a, b) {
			var c, d = F(a.style);
			d && (a.style[d] = c ? b + c : b)
		}

		function H(a) {
			var b = !1;
			if (void 0 === b) {
				var c;
				c = (c = F(a.style)) ? a.style[c] : void 0;
				b = "none" == c
			}
			G(a, b ? "" : "none")
		};

		function I(a) {
			AMP.BaseElement.call(this, a);
			this.i = this.a = null
		}
		g(I, AMP.BaseElement);
		I.prototype.buildCallback = function() {
			var a = this,
				b = this.win;
			if ("serviceWorker" in b.navigator) {
				var c = this.element.getAttribute("src");
				A(c, this.element);
				if (C(c) || C(b.location.href)) {
					var d = this.element.getAttribute("data-iframe-src");
					if (d) {
						A(d, this.element);
						var b = y(d).origin,
							e = q(this.element, "documentInfo").get(),
							f = y(e.sourceUrl),
							h = y(e.canonicalUrl);
						n().assert(b == f.origin || b == h.origin, "data-iframe-src (%s) should be a URL on the same origin as the source (%s) or canonical URL (%s) of the AMP-document.", b,
							f.origin, h.origin);
						this.a = d;
						J(this)
					}
				} else y(b.location.href).origin == y(c).origin ? this.loadPromise(this.win).then(function() {
					return K(a.win, c)
				}) : n().error("amp-install-serviceworker", "Did not install ServiceWorker because it does not match the current origin: " + c)
			} else L(this)
		};

		function J(a) {
			q(a.getAmpDoc(), "viewer").whenFirstVisible().then(function() {
				p(a.win).delay(function() {
					a.deferMutate(a.f.bind(a))
				}, 2E4)
			})
		}
		I.prototype.f = function() {
			if (q(this.getAmpDoc(), "viewer").isVisible()) {
				G(this.element, "none");
				var a = this.win.document.createElement("iframe");
				a.setAttribute("sandbox", "allow-same-origin allow-scripts");
				a.src = this.a;
				this.element.appendChild(a)
			}
		};

		function L(a) {
			if (a.getAmpDoc().isSingleDoc()) {
				var b = a.getAmpDoc(),
					c = y(a.win.location.href);
				if (!C(c)) {
					var d = a.element.getAttribute("data-no-service-worker-fallback-url-match"),
						e = a.element.getAttribute("data-no-service-worker-fallback-shell-url");
					if (d || e) {
						n().assert(d && e, 'Both, "%s" and "%s" must be specified for url-rewrite', "data-no-service-worker-fallback-url-match", "data-no-service-worker-fallback-shell-url");
						var e = B(e),
							f;
						try {
							f = new RegExp(d)
						} catch (h) {
							throw n().createError('Invalid "data-no-service-worker-fallback-url-match" expression',
								h);
						}
						n().assert(c.origin == y(e).origin, 'Shell origin "%s" must be the same as source origin "%s"', e, c.href);
						a.i = new M(b, f, e);
						z(e) && N(a, e)
					}
				}
			}
		}

		function N(a, b) {
			var c = a.loadPromise(a.win),
				d = q(a.getAmpDoc(), "viewer").whenFirstVisible();
			Promise.all([c, d]).then(function() {
				a.deferMutate(function() {
					return O(a, b)
				})
			})
		}

		function O(a, b) {
			var c = a.win.document.createElement("iframe");
			c.id = "i-amp-shell-preload";
			c.setAttribute("src", b + "#preload");
			H(c);
			c.setAttribute("sandbox", "allow-scripts allow-same-origin");
			var d = a.loadPromise(c);
			d.then(function() {
				c.parentElement && c.parentElement.removeChild(c)
			});
			a.element.appendChild(c)
		}

		function M(a, b, c) {
			this.win = a.win;
			this.h = b;
			this.g = c;
			this.b = y(c);
			a = a.getRootNode();
			b = this.c.bind(this);
			a.addEventListener("click", b, !1)
		}
		M.prototype.c = function(a) {
			if (!a.defaultPrevented && (a = u(a.target)) && a.href) {
				var b = y(a.href);
				if (b.origin == this.b.origin && b.pathname != this.b.pathname && this.h.test(b.href) && !a.getAttribute("i-amp-orig-href")) {
					var c = this.win;
					B(b.href) != B(c.location.href) && (a.setAttribute("i-amp-orig-href", a.href), a.href = this.g + "#href=" + encodeURIComponent("" + b.pathname + b.search + b.hash))
				}
			}
		};

		function K(a, b) {
			return a.navigator.serviceWorker.register(b).then(function(a) {
				var b = self;
				if (b.AMP_MODE) b = b.AMP_MODE;
				else {
					var c;
					if (b.context && b.context.mode) c = b.context.mode;
					else {
						c = l(b.location.originalHash || b.location.hash);
						var f = l(b.location.search);
						k || (k = b.AMP_CONFIG && b.AMP_CONFIG.v ? b.AMP_CONFIG.v : "011480633190770");
						c = {
							localDev: !1,
							development: !("1" != c.development && !b.AMP_DEV_MODE),
							filter: c.filter,
							minified: !0,
							lite: void 0 != f.amp_lite,
							test: !1,
							log: c.log,
							version: "1480633190770",
							rtvVersion: k
						}
					}
					b = b.AMP_MODE = c
				}
				b.development && n().info("amp-install-serviceworker", "ServiceWorker registration successful with scope: ", a.scope);
				return a
			}, function(a) {
				n().error("amp-install-serviceworker", "ServiceWorker registration failed:", a)
			})
		}
		AMP.registerElement("amp-install-serviceworker", I);
	})
});
//# sourceMappingURL=amp-install-serviceworker-0.1.js.map