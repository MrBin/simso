(self.AMP = self.AMP || []).push({
	n: "amp-sidebar",
	f: function(e) {
		function extend(a, b) {
			function __() {}
			__.prototype = b.prototype;
			a.prototype = new __;
			a.prototype.constructor = a;
			var prop;
			for (prop in b) {
				if (Object.defineProperties) {
					var desc = Object.getOwnPropertyDescriptor(b, prop);
					if (desc) {
						Object.defineProperty(a, prop, desc);
					}
				} else {
					a[prop] = b[prop];
				}
			}
		}

		function C(m, name) {
			m = m.__AMP_TOP || m;
			var obj = m.services && (m.services[name] && m.services[name].obj);
			return obj;
		}

		function fn(range) {
			var elem = range;
			if (elem.nodeType) {
				var e = (elem.ownerDocument || elem).defaultView;
				var maybeHex = e = e.__AMP_TOP || e;
				var result = e.services;
				if (!result) {
					result = e.services = {};
				}
				e = result;
				if (!(result = e.ampdoc)) {
					result = e.ampdoc = {
						obj: null,
						promise: null,
						resolve: null
					};
				}
				if (!result.obj) {
					result.obj = (void 0)(maybeHex);
					if (result.resolve) {
						result.resolve(result.obj);
					}
				}
				elem = result.obj.getAmpDoc(elem);
			}
			var obj = elem.isSingleDoc() ? elem.win : elem;
			return obj && (obj.services && (obj.services.history && obj.services.history.obj));
		}

		function add(models, prop) {
			var c = void 0;
			if (!obj) {
				obj = Object.create(null);
			}
			var val = obj[prop];
			if (!val || c) {
				val = prop;
				if (void 0 === models[prop]) {
					var id = prop.charAt(0).toUpperCase() + prop.slice(1);
					a: {
						var i = 0;
						for (; i < codeSegments.length; i++) {
							var name = codeSegments[i] + id;
							if (void 0 !== models[name]) {
								id = name;
								break a;
							}
						}
						id = "";
					}
					var model = id;
					if (void 0 !== models[model]) {
						val = model;
					}
				}
				if (!c) {
					obj[prop] = val;
				}
			}
			return val;
		}

		function setStyle(el, prop, value) {
			var unit;
			if (prop = add(el.style, prop)) {
				el.style[prop] = unit ? value + unit : value;
			}
		}

		function callback(el, recurring) {
			if (void 0 === recurring) {
				var cssprop;
				cssprop = (cssprop = add(el.style, "display")) ? el.style[cssprop] : void 0;
				recurring = "none" == cssprop;
			}
			setStyle(el, "display", recurring ? "" : "none");
		}

		function s(p) {
			e.BaseElement.call(this, p);
			this.h = null;
			this.m = C(this.win, "vsync");
			this.f = null;
			this.c = this.win.document;
			this.s = this.c.documentElement;
			this.g = null;
			var dojo = C(this.win, "platform");
			this.u = dojo.isIos() && dojo.isSafari();
			this.i = -1;
			this.o = false;
			this.l = C(this.win, "timer");
			this.b = null;
		}

		function push(node) {
			return node.element.hasAttribute("open");
		}

		function initialize(node) {
			if (!node.f) {
				var el = node.c.createElement("div");
				el.classList.add("-amp-sidebar-mask");
				el.addEventListener("click", function() {
					node.a();
				});
				node.element.parentNode.appendChild(el);
				el.addEventListener("touchmove", function(types) {
					types.preventDefault();
				});
				node.f = el;
			}
			callback(node.f, true);
		}

		function postLink(view) {
			view.element.addEventListener("scroll", function(types) {
				if (push(view)) {
					if (1 > view.element.scrollTop) {
						view.element.scrollTop = 1;
						types.preventDefault();
					} else {
						if (view.element.scrollHeight == view.element.scrollTop + view.element.offsetHeight) {
							--view.element.scrollTop;
							types.preventDefault();
						}
					}
				}
			});
		}

		function init(target) {
			if (!target.o) {
				var el = target.c.createElement("div");
				var properties = {
					height: "10vh",
					width: "100%",
					"background-color": "transparent"
				};
				var p;
				for (p in properties) {
					setStyle(el, p, properties[p]);
				}
				target.element.appendChild(el);
				target.o = true;
			}
		}
		Date.now();
		self.log = self.log || {
			user: null,
			dev: null
		};
		var obj;
		var codeSegments = "Webkit webkit Moz moz ms O o".split(" ");
		extend(s, e.BaseElement);
		s.prototype.isLayoutSupported = function(dataAndEvents) {
			return "nodisplay" == dataAndEvents;
		};
		s.prototype.buildCallback = function() {
			var object = this;
			this.g = this.element.getAttribute("side");
			this.h = this.getViewport();
			if ("left" != this.g && "right" != this.g) {
				var left = this.c.body.getAttribute("dir") || (this.s.getAttribute("dir") || "ltr");
				this.g = "rtl" == left ? "right" : "left";
				this.element.setAttribute("side", this.g);
			}
			if (this.u) {
				postLink(this);
			}
			if (push(this)) {
				this.j();
			} else {
				this.element.setAttribute("aria-hidden", "true");
			}
			if (!this.element.hasAttribute("role")) {
				this.element.setAttribute("role", "menu");
			}
			this.element.tabIndex = -1;
			this.s.addEventListener("keydown", function(event) {
				if (27 == event.keyCode) {
					object.a();
				}
			});
			var element = this.c.createElement("button");
			element.textContent = "Close the sidebar";
			element.classList.add("-amp-screen-reader");
			element.tabIndex = -1;
			element.addEventListener("click", function() {
				object.a();
			});
			this.element.appendChild(element);
			this.registerAction("toggle", this.v.bind(this));
			this.registerAction("open", this.j.bind(this));
			this.registerAction("close", this.a.bind(this));
		};
		s.prototype.activate = function() {
			this.j();
		};
		s.prototype.v = function() {
			if (push(this)) {
				this.a();
			} else {
				this.j();
			}
		};
		s.prototype.j = function() {
			var me = this;
			if (!push(this)) {
				this.h.disableTouchZoom();
				this.m.mutate(function() {
					callback(me.element, true);
					me.h.addToFixedLayer(me.element);
					initialize(me);
					if (me.u) {
						init(me);
					}
					me.element.scrollTop = 1;
					me.m.mutate(function() {
						me.element.setAttribute("open", "");
						me.element.setAttribute("aria-hidden", "false");
						try {
							me.element.focus();
						} catch (b) {}
						if (me.b) {
							me.l.cancel(me.b);
						}
						me.b = me.l.delay(function() {
							var er = me.getRealChildren();
							me.scheduleLayout(er);
							me.scheduleResume(er);
						}, 550);
					});
				});
				fn(this.getAmpDoc()).push(this.a.bind(this)).then(function(x) {
					me.i = x;
				});
			}
		};
		s.prototype.a = function() {
			var item = this;
			if (push(this)) {
				this.h.restoreOriginalTouchZoom();
				this.m.mutate(function() {
					if (item.f) {
						callback(item.f, false);
					}
					item.element.removeAttribute("open");
					item.element.setAttribute("aria-hidden", "true");
					if (item.b) {
						item.l.cancel(item.b);
					}
					item.b = item.l.delay(function() {
						if (!push(item)) {
							item.h.removeFromFixedLayer(item.element);
							item.m.mutate(function() {
								callback(item.element, false);
								item.schedulePause(item.getRealChildren());
							});
						}
					}, 550);
				});
				if (-1 != this.i) {
					fn(this.getAmpDoc()).pop(this.i);
					this.i = -1;
				}
			}
		};
		e.registerElement("amp-sidebar", s, "amp-sidebar{position:fixed!important;top:0;max-height:100vh!important;height:100vh;max-width:80vw!important;background-color:#efefef;min-width:45px!important;outline:none;overflow-x:hidden!important;overflow-y:auto!important;z-index:9999!important;-webkit-overflow-scrolling:touch;will-change:transform}amp-sidebar[side=left]{left:0!important;-webkit-transform:translateX(-100%)!important;transform:translateX(-100%)!important}amp-sidebar[side=right]{right:0!important;-webkit-transform:translateX(100%)!important;transform:translateX(100%)!important}amp-sidebar[side][open]{-webkit-transform:translateX(0)!important;transform:translateX(0)!important}amp-sidebar[side]{-webkit-transition:-webkit-transform 233ms cubic-bezier(0,0,.21,1);transition:-webkit-transform 233ms cubic-bezier(0,0,.21,1);transition:transform 233ms cubic-bezier(0,0,.21,1);transition:transform 233ms cubic-bezier(0,0,.21,1),-webkit-transform 233ms cubic-bezier(0,0,.21,1)}.-amp-sidebar-mask{position:fixed!important;top:0!important;left:0!important;width:100vw!important;height:100vh!important;opacity:0.2;background-image:none!important;background-color:#000;z-index:9998!important}\n/*# sourceURL=/extensions/amp-sidebar/0.1/amp-sidebar.css*/");
	}
});