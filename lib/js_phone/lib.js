function toggleMenu() {
    if (isShowMenu === false) {
		showMenu();
	} else {
		closeAllMenu();   
	}
}

function toggleRightMenu() {
    if (isShowMenu === false) {
		showRightMenu();
	} else {
		closeAllMenu();   
	}
}

function showMenu() {
	isShowMenu = true;
    $('#main_container').animate({left: "270px"}, { duration: 300, queue: false });
	$('#home_content').removeClass('show_right_menu');
    $('#home_content').addClass('show_left_menu');
    $('.overlay').show();
}

function showRightMenu() {
	isShowMenu = true;
    $('#main_container').animate({left: "-270px"}, { duration: 300, queue: false });
	$('#home_content').removeClass('show_left_menu');
	$('#home_content').addClass('show_right_menu');
	$('.overlay').show();
}
function showCenterMenu(domEle){
	var action = domEle.attr("data-action");

	if($("#menu_mobile").html() == ""){
		$.ajax({
			url: '/ajax/load_menu_mb.php',
			type: 'GET',
			data: {'action':action},
			success: function(data){
				if(data != ""){
					$("#menu_mobile").html(data);
					domEle.attr("data-action", "hide");
				}
				else{
					domEle.attr("data-action", "show");
				}
			}
		}).done(function() {
		  $("#menu_mobile").slideToggle(300);
		});
	}
	else $("#menu_mobile").slideToggle(300);
}

function closeAllMenu() {
	isShowMenu = false;
    $('#main_container').animate({left: "0px"}, { duration: 300, queue: false });
	setTimeout(function () {
		$('#home_content').removeClass('show_left_menu').removeClass('show_right_menu');
		$('.overlay').hide();
	}, 300);
}

function calculateAngle(x, y) {
    var r = Math.atan2(y, x); //radians
    var angle = Math.round(r * 180 / Math.PI); //degrees
    //ensure value is positive
    if(angle < 0) {
        angle = 360 - Math.abs(angle);
    }
    if(angle > 180) {
        angle = 360 - angle;
    }
    return angle;
}

var isShowMenu = false;
var allowMenu = true;

window.addEventListener('load', function () {
	var homeTouch = document.getElementById('home_content'),
		sX = 0,
		sY = 0,
		lX = 0,
		lY = 0,
		isTouch = false,
		ang = 0;
	homeTouch.addEventListener('touchstart', function (event) {
		if(allowMenu == true) {
			isTouch = true;
			var touchobj = event.changedTouches[0];
			sX = parseInt(touchobj.clientX);
			sY = parseInt(touchobj.clientY);
		}
	}, false)

	homeTouch.addEventListener('touchmove', function (event) {
		if(isTouch == true) {
			var touchobj = event.changedTouches[0];
			lX = parseInt(touchobj.clientX);
			lY = parseInt(touchobj.clientY);
			var dX = lX - sX;
			var dY = lY - sY;
			ang = calculateAngle(dX, dY);
			if((Math.abs(dX) > 100) && ((ang < 30) || (ang > 150))) {
				event.preventDefault();
			}
		}

	}, false)

	homeTouch.addEventListener('touchend', function (event) {
		if(isTouch == true) {
			var touchobj = event.changedTouches[0];
			lX = parseInt(touchobj.clientX);
			lY = parseInt(touchobj.clientY);
			var dX = lX - sX;
			var dY = lY - sY;
			ang = calculateAngle(dX, dY);
			if(isShowMenu == true) {
				if((ang < 30 || ang > 150)  && Math.abs(dX) > 50) {
                    event.preventDefault();
					closeAllMenu();
                }
			} else {
				if((ang < 30) && (Math.abs(dX) > 50)) {
					event.preventDefault();
					showMenu();
				} else if((ang > 150) && (Math.abs(dX) > 50)) {
					event.preventDefault();
					showRightMenu();
				}
			}

		}
		isTouch = false;
	}, false)
}, false);





/*-----------------------------------------------------------------------------------*/
/* Search sim ajax
/*-----------------------------------------------------------------------------------*/
function changeRadioChecked(name, value) {
	$("input[name=" + name + "]:checked").prop("checked", false);
	$("input[name=" + name + "][value=" + value + "]").prop("checked", true);
}
function search10so(check_module){
	var check_module = (check_module != "" ? check_module : "");

	changeRadioChecked('digit',1);

	if(check_module == 'searchsim') submit_form_v2();
	else submit_form('');
} 
function search11so(check_module){
	var check_module = (check_module != "" ? check_module : "");

	changeRadioChecked('digit',2);

	if(check_module == 'searchsim') submit_form_v2();
	else submit_form('');
}
function searchMang(last_url, check_module){

	var check_module = (check_module != "" ? check_module : "");

	$('#searchDauSo').val(0);

	if(check_module == 'searchsim') submit_form_v2();
	else submit_form(last_url);
}
function searchMenh(last_url, check_module){

	var check_module = (check_module != "" ? check_module : "");

	$('#searchNamSinh').val(0);	

	if(check_module == 'searchsim') submit_form_v2();
	else submit_form(last_url);
}
function submit_form(last_url) {
	var keyword = $('#searchText').val();
	var digit	= $('input[name=digit]:checked').val();
	var network	= $('#searchMang').val();
	var dauso	= $('#searchDauSo').val();
	var type	= $('#searchType').val();
	var price	= $('#searchPrice').val();
	var namsinh	= $('#searchNamSinh').val();
	var nguhanh	= $('#searchMenh').val();
	
	$('.searchText').popover('destroy');

	if(last_url == '&submit=submit'){
		var namsinh	= 0;
		var nguhanh	= 0;
		var digit 	= 0;	
		var network = 0;
		var type 	= 0;
		var price	= 0;
		var dauso	= 0;
	}
	
	if(keyword != '')
		var module = 'searchsim';
	else
		var module = 'simajax';

	last_url += '&module='+module+'&keyword='+keyword+'&digit='+digit+'&iCat='+network+'&iType='+type+'&iPrice='+price+'&iDauSo='+dauso+'&iNamSinh='+namsinh+'&iNguHanh='+nguhanh;
	
	url = "/load/?type=sim"+last_url
	
    $.pjax({
		url: url,
		container: '#main',
		container_ajax: '#main',
		ajax_allow: true
	})
}
function submit_form_v2(search_type) {
	var search_type = (search_type != "" ? search_type : "");

	var keyword    = $('#searchText').val();
	var digit      = parseInt($('input[name=digit]:checked').val());
	var network    = parseInt($('#searchMang').val());
	var dauso      = parseInt($('#searchDauSo').val());
	var type       = parseInt($('#searchType').val());
	var price      = parseInt($('#searchPrice').val());
	var namsinh    = parseInt($('#searchNamSinh').val());
	var nguhanh    = parseInt($('#searchMenh').val());
	var keywordLen = 0;
	var strNetwork = "";
	var url 			= "";

	if(search_type == "all"){
		var namsinh	= 0;
		var nguhanh	= 0;
		var digit 	= 0;	
		var network = 0;
		var type 	= 0;
		var price	= 0;
		var dauso	= 0;
	}

	/* Nếu có keyword thì earch */
	if(keyword != ''){
		if(keyword.indexOf("*") == -1 && keyword.indexOf("x") == -1){
			keyword = keyword.replace(/[^0-9]/g, "");
			/* Check keyword nếu 10 số thì redirect về trang chi tiết */
			var keywordLen = keyword.length;

			if(keywordLen == 10 && jsonNetwork != ""){

				var objNetwork  = JSON.parse(jsonNetwork);
				var firstNumber = parseInt(keyword.substr(0, 3));

				for (var key in objNetwork) {
					if (objNetwork.hasOwnProperty(key)) {
						if(key == firstNumber) strNetwork = objNetwork[key];
					}
				}
			}
		}

		if(strNetwork != ""){
			url = '/sim-' + strNetwork + '-' + keyword + '/';
		}
		else{
			url          = '/tim-sim/' + keyword + '.html';
			var urlParam = "";

			if(digit > 0) 		urlParam   	+= '&digit=' + digit;
			if(network > 0) 	urlParam 	+= '&iCat=' + network;
			if(type > 0) 		urlParam   	+= '&iType=' + type;
			if(price > 0) 		urlParam   	+= '&iPrice=' + price;
			if(dauso > 0) 		urlParam   	+= '&iDauSo=' + dauso;
			if(namsinh > 0) 	urlParam 	+= '&iNamSinh=' + namsinh;
			if(nguhanh > 0) 	urlParam		+= '&iNguHanh=' + nguhanh;

			if(urlParam != ""){
				url = url + '?' + urlParam;
				url = url.replace("?&", "?");
			}
		}

		$.pjax({
			url: url,
			container: '#main',
			container_ajax: '#main',
			ajax_allow: false
		});
	}

	else alert("Bạn vui lòng nhập từ khóa");
}
function getSimBirthDay() {
	var iBirthday   = $('#frm_birth_day').val();
	var iBirthMonth = $('#frm_birth_month').val();
	var iBirthYear  = $('#frm_birth_year').val();

	var keyword 	 = iBirthday + iBirthMonth + iBirthYear;
	var url       	 = "/load/?type=sim&submit=submit&module=searchsim&keyword=*" + keyword;
	
    $.pjax({
		url: url,
		container: '#main',
		container_ajax: '#main',
		ajax_allow: false
	})
}
function getSimHopMenh() {
	
	var giosinh   = parseInt($('#pt_birth_hour').val());
	var namsinh   = parseInt($('#bss_birth_year').val());
	var ngaysinh  = parseInt($('#bss_birth_day').val());
	var thangsinh = parseInt($('#bss_birth_month').val());
	var gioitinh  = $('.bml_sex ul li.active span').html();

	if(namsinh > 0 && ngaysinh > 0 && thangsinh > 0 && typeof(gioitinh) != 'undefined'){
		var url = '/home/vn/type.php?type=static&module=xem-sim-hop-tuoi&module_check=sim-hop-tuoi&giosinh=' + giosinh + '&ngaysinh=' + ngaysinh + '&thangsinh=' + thangsinh + '&namsinh=' + namsinh + '&gioitinh=' + gioitinh + '&action=Xem';
	
	    $.pjax({
			url: url,
			container: '#main',
			container_ajax: '#main',
			ajax_allow: false
		});
	}
	else{
		alert("Vui lòng điền đầy đủ thông tin!!!");
	}
}
function getSimCouple() {
	var couple_number = $('#couple_number').val();
	var couple_type   = $('#couple_type').val();
	var couple_price  = $('#couple_price').val();

	var keyword 	 = couple_number + couple_type + couple_price;
	var url       	 = "/load/?type=sim&submit=submit&module=searchsim&keyword=" + couple_number + "&digit=0&iCat=" + couple_type + "&iType=0&iPrice=" + couple_price + "&iDauSo=0&iNamSinh=0&iNguHanh=0";
	
    $.pjax({
		url: url,
		container: '#main',
		container_ajax: '#main',
		ajax_allow: false
	})
}
function loadPagePjax() {
    if(($("#endOfLoadPage").length <= 0) && allowLoadPage) {
        $(window).scroll(function () {
            if(($(window).scrollTop() + $(window).height() >= $(document).height() - 300) && allowLoadPage) {
                allowLoadPage = false;
                $.pjax({
                    url: urlLoadPage,
                    container: "#main",
                    appendData: true
                })
            }
        })
    }
}
/* Phong Thuy */
function tra_sim_phong_thuy(){
	$gioitinh 		= $("#gioitinh").val();
	$sodienthoai 	= $("#sodienthoai").val();
	$giosinh 		= $("#giosinh").val();
	$ngaysinh 		= $("#ngaysinh").val();
	$thangsinh 		= $("#thangsinh").val();
	$namsinh 		= $("#namsinh").val();
	$module_check 	= $("#module_check").val();

	last_url = "&module_check="+$module_check+"&gioitinh="+$gioitinh+"&sodienthoai="+$sodienthoai+"&giosinh="+$giosinh+"&ngaysinh="+$ngaysinh+"&thangsinh="+$thangsinh+"&namsinh="+$namsinh+"&iNamSinh="+$namsinh;
	
	url = "/load/?type=static&module=tra-phong-thuy"+last_url
	
    $.pjax({
		url: url,
		container: '#main-phong-thuy',
		container_ajax: '#main-phong-thuy',
		ajax_allow: false
	})
	
}


function initLoad(){
	
	$('#go-to-top').click(function(e) {
		showMenu();
	});

	// tooltip demo
    $('[data-toggle=tooltip]').tooltip({	
		animation : false,
		html : true,
		trigger : 'click'
    })

	// popover
    $('[data-toggle=popover]').popover({	
		html : true,
		trigger : 'click',
		content: function() {
			id = $(this).attr('id')
			return $('.popover_'+id).html();
		}	
    })
	
	// popover search
    $(".searchText").popover({
	  	html : true,
		trigger : 'focus',
		content: function() {
			return $('#search-popover').html();
		}	  
	})
	
	// Enter search sim
	$('.searchText').bind('keypress', function(e) {
		if(e.keyCode==13){
			submit_form_v2('all');
		}
	}); 

}


$(function(){
	$("#frm_checkout").validate({
		rules: {
			billing_name: {
				required: true,
				minlength: 3
			},
			billing_phone: {
				required: true,
				number: true
			}
		},
		messages: {
			billing_name: {
				required: "Vui lòng nhập họ và tên của bạn!",
				minlength: "Họ tên bạn tối thiểu 3 kí tự!"
			},
			billing_phone: {
				required: "Vui lòng nhập số điện thoại của bạn!",
				number: "Số điện thoại không đúng!"
			}

		},
		submitHandler: function(form) {

			var hoten     = $("#billing_name").val();
			var dienthoai = $("#billing_phone").val();
			var diachi    = $("#billing_address").val();
			var giaban    = $("#giaban").val();
			var simcard   = $("#simcard").val();
			var action    = $("#action").val();

			$.ajax({
				url: '/ajax/sim_checkout.php',
				type: 'POST',
				dataType: "json",
				data: {'hoten':hoten, 'dienthoai':dienthoai, 'diachi':diachi, 'giaban':giaban, 'simcard':simcard, 'action':action},
				success: function(data){
					console.log(data);
					if(data.status == 1){
						$("#box_contact").hide();
						$("#box_contact_success").show();
					}
					else{
						$(".errorMsg").html(data.msg).show();
					}
				}
			});

			return false;
		}
	});

	$(".bs_more_hot a").click(function(){
		$(".list_simhot").toggleClass("show_full");
		var textBtn = $.trim($(this).text());
		if(textBtn == "Xem thêm"){
			$(this).text("Thu gọn");
		}
		else{
			$(this).text("Xem thêm");
		}
	});

	var bLazy = new Blazy();
});
