/*-----------------------------------------------------------------------------------*/
/* Search sim ajax
/*-----------------------------------------------------------------------------------*/
function changeRadioChecked(name, value) {
	$("input[name=" + name + "]:checked").prop("checked", false);
	$("input[name=" + name + "][value=" + value + "]").prop("checked", true);
}
function search10so(check_module){
	var check_module = (check_module != "" ? check_module : "");

	changeRadioChecked('digit',1);

	if(check_module == 'searchsim') submit_form_v2();
	else submit_form('');
} 
function search11so(check_module){
	var check_module = (check_module != "" ? check_module : "");

	changeRadioChecked('digit',2);

	if(check_module == 'searchsim') submit_form_v2();
	else submit_form('');
}
function searchMang(last_url, check_module){

	var check_module = (check_module != "" ? check_module : "");

	$('#searchDauSo').val(0);

	if(check_module == 'searchsim') submit_form_v2();
	else submit_form(last_url);
}
function searchMenh(last_url, check_module){

	var check_module = (check_module != "" ? check_module : "");

	$('#searchNamSinh').val(0);	

	if(check_module == 'searchsim') submit_form_v2();
	else submit_form(last_url);
}
function submit_form(last_url) {
	var keyword = $('#searchText').val();
	var digit	= $('input[name=digit]:checked').val();
	var network	= $('#searchMang').val();
	var dauso	= $('#searchDauSo').val();
	var type	= $('#searchType').val();
	var price	= $('#searchPrice').val();
	var namsinh	= $('#searchNamSinh').val();
	var nguhanh	= $('#searchMenh').val();

	if(last_url == '&submit=submit'){
		var namsinh	= 0;
		var nguhanh	= 0;
		var digit 	= 0;	
		var network = 0;
		var type 	= 0;
		var price	= 0;
		var dauso	= 0;
	}
	
	if(keyword != '')
		var module = 'searchsim';
	else
		var module = 'simajax';

	last_url += '&module='+module+'&keyword='+keyword+'&digit='+digit+'&iCat='+network+'&iType='+type+'&iPrice='+price+'&iDauSo='+dauso+'&iNamSinh='+namsinh+'&iNguHanh='+nguhanh;
	
	url = "/load/?type=sim"+last_url;
	
    $.pjax({
		url: url,
		container: '#main_left',
		container_ajax: '#main_left',
		ajax_allow: false
	});
}

function submit_form_v2(search_type) {
	var search_type = (search_type != "" ? search_type : "");

	var keyword    = $('#searchText').val();
	// var digit      = parseInt($('input[name=digit]:checked').val());
	var network    = parseInt($('#searchMang').val());
	var dauso      = parseInt($('#searchDauSo').val());
	var type       = parseInt($('#searchType').val());
	var price      = parseInt($('#searchPrice').val());
	var namsinh    = parseInt($('#searchNamSinh').val());
	var nguhanh    = parseInt($('#searchMenh').val());
	var keywordLen = 0;
	var strNetwork = "";
	var url 			= "";

	if(search_type == "all"){
		var namsinh	= 0;
		var nguhanh	= 0;
		// var digit 	= 0;	
		var network = 0;
		var type 	= 0;
		var price	= 0;
		var dauso	= 0;
	}

	/* Nếu có keyword thì earch */
	if(keyword != ''){
		if(keyword.indexOf("*") == -1 && keyword.indexOf("x") == -1){
			keyword = keyword.replace(/[^0-9]/g, "");

			/* Check keyword nếu 10 số thì redirect về trang chi tiết */
			var keywordLen = keyword.length;

			if(keywordLen == 10 && jsonNetwork != ""){

				var objNetwork  = JSON.parse(jsonNetwork);
				var firstNumber = parseInt(keyword.substr(0, 3));

				for (var key in objNetwork) {
					if (objNetwork.hasOwnProperty(key)) {
						if(key == firstNumber) strNetwork = objNetwork[key];
					}
				}
			}
		}

		if(strNetwork != ""){
			url = '/sim-' + strNetwork + '-' + keyword + '/';
		}
		else{
			url          = '/tim-sim/' + keyword + '.html';
			var urlParam = "";

			// if(digit > 0) 		urlParam   	+= '&digit=' + digit;
			if(network > 0) 	urlParam 	+= '&iCat=' + network;
			if(type > 0) 		urlParam   	+= '&iType=' + type;
			if(price > 0) 		urlParam   	+= '&iPrice=' + price;
			if(dauso > 0) 		urlParam   	+= '&iDauSo=' + dauso;
			if(namsinh > 0) 	urlParam 	+= '&iNamSinh=' + namsinh;
			if(nguhanh > 0) 	urlParam		+= '&iNguHanh=' + nguhanh;

			if(urlParam != ""){
				url = url + '?' + urlParam;
				url = url.replace("?&", "?");
			}
		}

		$.pjax({
			url: url,
			container: '#main_left',
			container_ajax: '#main_left',
			ajax_allow: false
		});
	}

	else alert("Bạn vui lòng nhập từ khóa");
}

function getSimBirthDay() {
	var iBirthday   = $('#frm_birth_day').val();
	var iBirthMonth = $('#frm_birth_month').val();
	var iBirthYear  = $('#frm_birth_year').val();

	var keyword 	 = iBirthday + iBirthMonth + iBirthYear;
	var url       	 = "/load/?type=sim&submit=submit&module=searchsim&keyword=" + keyword;
	
    $.pjax({
		url: url,
		container: '#main_left',
		container_ajax: '#main_left',
		ajax_allow: false
	});
}
function getSimHopMenh() {
	
	var giosinh   = parseInt($('#pt_birth_hour').val());
	var namsinh   = parseInt($('#bss_birth_year').val());
	var ngaysinh  = parseInt($('#bss_birth_day').val());
	var thangsinh = parseInt($('#bss_birth_month').val());
	var gioitinh  = $('.bml_sex ul li.active span').html();

	if(namsinh > 0 && ngaysinh > 0 && thangsinh > 0 && typeof(gioitinh) != 'undefined'){
		var url = '/home/vn/type.php?type=static&module=xem-sim-hop-tuoi&module_check=sim-hop-tuoi&giosinh=' + giosinh + '&ngaysinh=' + ngaysinh + '&thangsinh=' + thangsinh + '&namsinh=' + namsinh + '&gioitinh=' + gioitinh + '&action=Xem';
	
	    $.pjax({
			url: url,
			container: '#main_left',
			container_ajax: '#main_left',
			ajax_allow: false
		});
	}
	else{
		alert("Vui lòng điền đầy đủ thông tin!!!");
	}
}
function getSimCouple() {
	var couple_number = $('#couple_number').val();
	var couple_type   = $('#couple_type').val();
	var couple_price  = $('#couple_price').val();

	var keyword 	 = couple_number + couple_type + couple_price;
	var url       	 = "/load/?type=sim&submit=submit&module=searchsim&keyword=" + couple_number + "&digit=0&iCat=" + couple_type + "&iType=0&iPrice=" + couple_price + "&iDauSo=0&iNamSinh=0&iNguHanh=0";
	
    $.pjax({
		url: url,
		container: '#main_left',
		container_ajax: '#main_left',
		ajax_allow: false
	});
}
function loadPagePjax() {
    if(($("#endOfLoadPage").length <= 0) && allowLoadPage) {
        $(window).scroll(function () {
            if(($(window).scrollTop() + $(window).height() >= $(document).height() - 600) && allowLoadPage) {
                allowLoadPage = false;
                $.pjax({
                    url: urlLoadPage,
                    container: "#main-page",
                    appendData: true
                });
            }
        });
    }
}
/* Phong Thuy */
function tra_sim_phong_thuy(){
	$gioitinh 		= $("#gioitinh").val();
	$sodienthoai 	= $("#sodienthoai").val();
	$giosinh 		= $("#giosinh").val();
	$ngaysinh 		= $("#ngaysinh").val();
	$thangsinh 		= $("#thangsinh").val();
	$namsinh 		= $("#namsinh").val();
	$module_check 	= $("#module_check").val();

	last_url = "&module_check="+$module_check+"&gioitinh="+$gioitinh+"&sodienthoai="+$sodienthoai+"&giosinh="+$giosinh+"&ngaysinh="+$ngaysinh+"&thangsinh="+$thangsinh+"&namsinh="+$namsinh+"&iNamSinh="+$namsinh;
	
	url = "/load/?type=static&module=sim-phong-thuy"+last_url;
	
    $.pjax({
		url: url,
		container: 'body',
		container_ajax: '#main-phong-thuy',
		ajax_allow: false
	});
	
}
/*
function changePage($id,$page,$url){

	url = $url+'&'+$page;

	$.pjax({
		url: url,
		container: '#main',
		ajax_allow: true
	})
}
*/
function initLoad(){
	
	$(window).scroll(function() {
		var height = $(window).scrollTop();
		if(height  > 120) {
			$('.panel-search').appendTo('.search-bar');
		}else{
			$('.panel-search').appendTo('.col-search');
		}
	});
	
	$('#go-to-top').click(function(e) {
		$('html, body').animate({scrollTop:0}, 'slow');	 
	});
	
	$('.modal-click-load').click(function(e) {
		var url = $(this).attr('data-href');
		
		$('#myModal').modal('show');
		$.pjax({
			url: url,
			container: '.modal-body',
			container_ajax: '.modal-body',
			ajax_allow: false
		});
		
	});

	// tooltip demo
    $('[data-toggle=tooltip]').tooltip({	
		animation : false,
		html : true
    });
	
	// Clone sim nam sinh toi header bar
	$('.topbar-sim-nam-sinh').mouseover(function(e) {
		if($('#main-sim-nam-sinh').html() == '')
    		$('#sim-nam-sinh').clone().appendTo("#main-sim-nam-sinh");	    
    });

	// popover
    $('[data-toggle=popover]').popover({	
		html : true,
		trigger : 'hover',
		content: function() {
			id = $(this).attr('id');
			return $('.popover_'+id).html();
		}	
    });
	
	// popover search
    $(".searchText").popover({
	  	html : true,
		trigger : 'focus',
		content: function() {
			return $('#search-popover').html();
		}	  
	});
	
	// Enter search sim
	$('.searchText').bind('keypress', function(e) {
		if(e.keyCode==13){
			 $(this).popover('hide');
			submit_form_v2('all');
		}
	}); 

	// Get Code
	$('#getIfCode').click(function(){
		width = $('#ifWidth').val();
		height = $('#ifHeight').val();	
		code = '<iframe frameborder="0" src="https://sieuthisimthe.com/sim-phong-thuy.html" width="'+width+'" height="'+height+'" style="border:solid 1px #DADADA; padding:2px;"></iframe>';
		$('#ifCode').val(code);
	});

}
function loadAjaxContent(url){
	var cache_data = [];
	var key	= (typeof(arguments[1]) != "undefined" ? arguments[1] : "");
	if(key != ""){
		if(typeof(cache_data[key]) == "undefined") cache_data[key]	= $.ajax({ url: url, cache: false, async: false }).responseText;
		return cache_data[key];
	}
	else return $.ajax({ url: url, cache: false, async: false }).responseText;
}

$(document).ready(function(){
	
	// pjax
	$(document).pjax("a[data-pjax]");

	$(document).on('pjax:complete', function() {

		simpleTip();
	})

});
$(function(){
	$("#frm_checkout").validate({
		rules: {
			billing_name: {
				required: true,
				minlength: 3
			},
			billing_phone: {
				required: true,
				number: true
			}
		},
		messages: {
			billing_name: {
				required: "Vui lòng nhập họ và tên của bạn!",
				minlength: "Họ tên bạn tối thiểu 3 kí tự!"
			},
			billing_phone: {
				required: "Vui lòng nhập số điện thoại của bạn!",
				number: "Số điện thoại không đúng!"
			}

		},
		submitHandler: function(form) {

			var hoten     = $("#billing_name").val();
			var dienthoai = $("#billing_phone").val();
			var diachi    = $("#billing_address").val();
			var giaban    = $("#giaban").val();
			var simcard   = $("#simcard").val();
			var action    = $("#action").val();

			$.ajax({
				url: '/ajax/sim_checkout.php',
				type: 'POST',
				dataType: "json",
				data: {'hoten':hoten, 'dienthoai':dienthoai, 'diachi':diachi, 'giaban':giaban, 'simcard':simcard, 'action':action},
				success: function(data){
					/*console.log(data); */
					if(data.status == 1){
						$("#box_contact").hide();
						$("#box_contact_success").show();
						$("#detail_sim").addClass("detail_order_success");
					}
					else{
						$(".errorMsg").html(data.msg).show();
					}
				}
			});

			return false;
		}
	});

	$(".bs_more_hot a").click(function(){
		$(".list_simhot").toggleClass("show_full");
		var textBtn = $.trim($(this).text());
		if(textBtn == "Xem thêm"){
			$(this).text("Thu gọn");
		}
		else{
			$(this).text("Xem thêm");
		}
	});

	var bLazy = new Blazy();

	simpleTip();
});