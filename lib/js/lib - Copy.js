/*-----------------------------------------------------------------------------------*/
/* Search sim ajax
/*-----------------------------------------------------------------------------------*/
function changeRadioChecked(name, value) {
	$("input[name=" + name + "]:checked").prop("checked", false);;
	$("input[name=" + name + "][value=" + value + "]").prop("checked", true);
}
function search10so(){
	changeRadioChecked('digit',1)
	submit_form('');
} 
function search11so(){
	changeRadioChecked('digit',2)
	submit_form('');
}
function searchMang(last_url){
	$('#searchDauSo').val(0);	
	submit_form(last_url);
}
function searchMenh(last_url){
	$('#searchNamSinh').val(0);	
	submit_form(last_url);
}
function submit_form(last_url) {
	var keyword = $('#searchText').val();
	var digit	= $('input[name=digit]:checked').val();
	var network	= $('#searchMang').val();
	var dauso	= $('#searchDauSo').val();
	var type	= $('#searchType').val();
	var price	= $('#searchPrice').val();
	var namsinh	= $('#searchNamSinh').val();
	var nguhanh	= $('#searchMenh').val();

	if(last_url == '&submit=submit'){
		var namsinh	= 0;
		var nguhanh	= 0;
		var digit 	= 0;	
		var network = 0;
		var type 	= 0;
		var price	= 0;
		var dauso	= 0;
	}
	
	if(keyword != '')
		var module = 'searchsim';
	else
		var module = 'simajax';

	last_url += '&module='+module+'&keyword='+keyword+'&digit='+digit+'&iCat='+network+'&iType='+type+'&iPrice='+price+'&iDauSo='+dauso+'&iNamSinh='+namsinh+'&iNguHanh='+nguhanh;
	
	url = "/load/?type=sim"+last_url
	
    $.pjax({
		url: url,
		container: 'body',
		container_ajax: '#main',
		ajax_allow: false
	})
}
/* Phong Thuy */
function tra_sim_phong_thuy(){
	$gioitinh 		= $("#gioitinh").val();
	$sodienthoai 	= $("#sodienthoai").val();
	$giosinh 		= $("#giosinh").val();
	$ngaysinh 		= $("#ngaysinh").val();
	$thangsinh 		= $("#thangsinh").val();
	$namsinh 		= $("#namsinh").val();
	$module_check 	= $("#module_check").val();

	last_url = "module_check="+$module_check+"&gioitinh="+$gioitinh+"&sodienthoai="+$sodienthoai+"&giosinh="+$giosinh+"&ngaysinh="+$ngaysinh+"&thangsinh="+$thangsinh+"&namsinh="+$namsinh+"&iNamSinh="+$namsinh;
	
	last_url = $.base64.encode(last_url);

	url = "/load/"+last_url
	
    $.pjax({
		url: url,
		container: '#main-phong-thuy',
		container_ajax: '#main-phong-thuy',
		ajax_allow: false
	})
	
}
/*
function changePage($id,$page,$url){

	url = $url+'&'+$page;

	$.pjax({
		url: url,
		container: '#main',
		ajax_allow: true
	})
}
*/
function initLoad(){

	// tooltip demo
    $('[data-toggle=tooltip]').tooltip({	
		animation : false,
		html : true
    })
	
	// Clone sim nam sinh toi header bar
	$('.topbar-sim-nam-sinh').mouseover(function(e) {
		if($('#main-sim-nam-sinh').html() == '')
    		$('#sim-nam-sinh').clone().appendTo("#main-sim-nam-sinh");	    
    });

	// popover
    $('[data-toggle=popover]').popover({	
		html : true,
		trigger : 'hover',
		content: function() {
			id = $(this).attr('id')
			return $('.popover_'+id).html();
		}	
    })
	
	// popover search
    $(".searchText").popover({
	  	html : true,
		trigger : 'focus',
		content: function() {
			return $('#search-popover').html();
		}	  
	})
	
	// Enter search sim
	$('.searchText').bind('keypress', function(e) {
		if(e.keyCode==13){
			 $(this).popover('hide')
			submit_form('&submit=submit');
		}
	}); 

	// Get Code
	$('#getIfCode').click(function(){
		width = $('#ifWidth').val();
		height = $('#ifHeight').val();	
		code = '<iframe frameborder="0" src="http://www.sieuthisimthe.com/xem-sim-phong-thuy.html" width="'+width+'" height="'+height+'" style="border:solid 1px #DADADA; padding:2px;"></iframe>'
		$('#ifCode').val(code)
	})

}

$(document).ready(function(){
	
	// pjax
	$(document).pjax("a[data-pjax]");

})