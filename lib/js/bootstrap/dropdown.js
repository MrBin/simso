/* ========================================================================
 * Bootstrap: dropdown.js v3.0.0
 * http://twbs.github.com/bootstrap/javascript.html#dropdowns
 * ========================================================================
 * Copyright 2012 Twitter, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ======================================================================== */
+ function ($) {
    "use strict";

    // DROPDOWN CLASS DEFINITION
    // =========================
    var backdrop = '.dropdown-backdrop';
    var hover = '[data-hover=dropdown]';
    var toggle = '[data-toggle=dropdown]';
    var Dropdown = function (element) {
        var $el = $(element).on('click.bs.dropdown', this.toggle);
    }

    Dropdown.prototype.toggle = function (e) {
        var $this = $(this);
        // Nếu như có .disabled thì dừng lại
        if($this.is('.disabled, :disabled')) return;

        if(e.type == 'mouseenter' || e.type == 'mouseleave')
            var $this = $this;
        else
            var $this = getParent($this);

        var isActive = $this.hasClass('open');

        clearMenus();

        if(!isActive) {

            $this.trigger(e = $.Event('show.bs.dropdown'));

            if(e.isDefaultPrevented()) return;

            $this
                .toggleClass('open')
                .trigger('shown.bs.dropdown');
        } else {
            $this
                .removeClass('open')
                .trigger('shown.bs.dropdown');
        }

        return false;
    };

    function clearMenus() {
        $(backdrop).remove();

        $(toggle).each(function (e) {
            var $parent = getParent($(this));
            if(!$parent.hasClass('open')) return;
            $parent.trigger(e = $.Event('hide.bs.dropdown'));
            if(e.isDefaultPrevented()) return;
            $parent.removeClass('open').trigger('hidden.bs.dropdown');
        });
    }

    function getParent($this) {
        var selector = $this.attr('data-target');

        if(!selector) {
            selector = $this.attr('href');
            selector = selector && /#/.test(selector) && selector.replace(/.*(?=#[^\s]*$)/, ''); //strip for ie7
        }

        var $parent = selector && $(selector);

        return $parent && $parent.length ? $parent : $this.parent();
    }


    // DROPDOWN PLUGIN DEFINITION
    // ==========================

    var old = $.fn.dropdown;

    $.fn.dropdown = function (option) {
        return this.each(function () {
            var $this = $(this);
            var data = $this.data('dropdown');

            if(!data) $this.data('dropdown', (data = new Dropdown(this)));
            if(typeof option == 'string') data[option].call($this);
        })
    };

    $.fn.dropdown.Constructor = Dropdown;


    // DROPDOWN NO CONFLICT
    // ====================

    $.fn.dropdown.noConflict = function () {
        $.fn.dropdown = old;
        return this;
    };


    // APPLY TO STANDARD DROPDOWN ELEMENTS
    // ===================================

    $(document)
        .on('mouseenter mouseleave', hover, Dropdown.prototype.toggle)
        .on('click.bs.dropdown.data-api', clearMenus)
        .on('click.bs.dropdown.data-api', '.dropdown form', function (e) {
            e.stopPropagation()
        })
        .on('click.bs.dropdown.data-api', toggle, Dropdown.prototype.toggle);

}(jQuery);