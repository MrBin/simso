<?
$arrayCssInline = array(
	/* Template */
	"/lib/css/css_first.css",
	"/lib/css/temp_grid.css",
	"/lib/css/temp_glyphicons.css",	
	"/lib/css/temp_tables.css",
	"/lib/css/temp_forms.css",
	"/lib/css/temp_buttons.css",
	"/lib/css/temp_breadcrumbs.css",    
	"/lib/css/temp_pagination.css",
	"/lib/css/temp_alerts.css",
	"/lib/css/temp_panels.css",	
	"/lib/css/temp_tooltip.css",
	"/lib/css/temp_popovers.css",
	"/lib/css/temp_close.css",
	"/lib/css/temp_modals.css",
	"/lib/css/temp_theme.css",
	
	"/lib/css/temp_navs.css",
	"/lib/css/temp_navbar.css",
	"/lib/css/temp_dropdowns.css",
	"/lib/css/type_phongthuy.css",
	"/lib/css/type_sim.css",
	/* Type */
	"/lib/css/type_news.css",
	
	/* Detail */
	"/lib/css/detail_news.css",
	);
$arrayCssFile = array(
	"/lib/simpleTip/simpleTip.css",
	"/lib/windowPrompt/windowPrompt.css",
	/* Template */
	"/lib/css/temp_abc.css",
	/* Top + Bot + Sidebar */
	"/lib/css/bot.css",
	
	/* Index */
	
	/* IE */
	# "/lib/css/ie_grid.css",
	# "/lib/css/ie_icon.css",
	);
$arrayJsFile = array(
	"/lib/js/jquery.js",
	"/lib/js/jquery.cookie.js",
	// "/lib/js/auto.like.js",
	"/lib/js/jquery.pjax.js",
	"/lib/js/bootstrap/transition.js",
	"/lib/js/bootstrap/util.js",
	"/lib/js/bootstrap/collapse.js",
	"/lib/js/bootstrap/dropdown.js", 
	"/lib/js/bootstrap/tab.js",
	"/lib/js/bootstrap/tooltip.js",
	"/lib/js/bootstrap/popover.js",
	"/lib/js/bootstrap/modal.js",
	"/lib/js/jquery.validate.min.js",
	"/lib/js/blazy.min.js",
	"/lib/simpleTip/simpleTip.js",
	"/lib/windowPrompt/windowPrompt.js",
	"/lib/js/jquery.hoverIntent.js",
	"/lib/js/lib.js",	
	);
	
$arrayCssInlineMb = array(
	/* Template */
   "/lib/css_phone/css_first.css",
   "/lib/css_phone/temp_grid.css",
   "/lib/css/temp_glyphicons.css",
   "/lib/css_phone/temp_pagination.css",
   "/lib/css_phone/temp_breadcrumbs.css",
   "/lib/css_phone/temp_tables.css",   
   "/lib/css_phone/temp_forms.css",
   "/lib/css_phone/temp_buttons.css",
   "/lib/css_phone/temp_tooltip.css",
   "/lib/css_phone/temp_popovers.css",
   
   /* Top + Bot + Sidebar */
   "/lib/css_phone/temp_navs.css",
   "/lib/css_phone/temp_navbar.css",
   
   /* Type */
   "/lib/css_phone/type_sim.css",
   "/lib/css_phone/type_news.css",
   "/lib/css_phone/type_phongthuy.css",
   
   /* Detail */
   "/lib/css_phone/detail_news.css",
	);

$arrayCssPhoneFile = array(
	/* Template */
   "/lib/css_phone/temp_abc.css",
   "/lib/css_phone/temp_dropdowns.css",
   "/lib/windowPrompt/windowPrompt.css",
   
   "/lib/css_phone/temp_left.css",
   "/lib/css_phone/temp_right.css",
   
   "/lib/css_phone/bot.css"
	);
$arrayJsPhoneFile = array(
   "/lib/js_phone/jquery.js", 
   "/lib/js_phone/jquery.pjax.js", 

   "/lib/js_phone/bootstrap/dropdown.js",
   "/lib/js/bootstrap/util.js", 
   "/lib/js_phone/bootstrap/collapse.js", 
   "/lib/js_phone/bootstrap/tab.js", 
   "/lib/js_phone/bootstrap/tooltip.js",
   "/lib/js_phone/bootstrap/popover.js",
   "/lib/js/jquery.validate.min.js",
   "/lib/windowPrompt/windowPrompt.js",
   "/lib/js/blazy.min.js",
   "/lib/js_phone/lib.js",	
	);
?>