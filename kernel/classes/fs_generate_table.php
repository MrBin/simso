<?
class generateTable{ 
	var $fieldRelate = "classified_id";
	var $arrayFieldDefault = array();
	function generateTable($arrayStructure = array(),$arrayFieldDefault = array()){
		$this->arrayFieldDefault = $arrayFieldDefault;
		$this->arrayFieldDefault["lang_id"] = 1;
		foreach($arrayStructure as $table=>$tvalue){
			//neu chua co bang thi tao bang moi
			if(!isset($arrayTableList[$table])){
				$db_ex = new db_execute("CREATE TABLE IF NOT EXISTS `" . $table . "` (
												  `" . $table . "_id` int(11) NOT NULL auto_increment,
												  `" . $this->fieldRelate . "` int(11) default '0',
												  `lang_id` int(11) default '0',
												  PRIMARY KEY  (`" . $table . "_id`),
												  KEY `" . $this->fieldRelate . "` (`" . $this->fieldRelate . "`)
												) ENGINE=MyISAM DEFAULT CHARSET=utf8");
				unset($db_ex);
			}
		}
		//dua ra các table đã có trong database
		$db_listTable = new db_query("SHOW TABLES");
		
		//gán các bảng vào một array
		$arrayTableList = array();
		while($row = mysql_fetch_array($db_listTable->result)){
			$arrayTableList[$row[0]] = '';
		}
		
		unset($db_listTable);
		$listFieldDatabase 		= array();
		foreach($arrayStructure as $table=>$tvalue){
			
			//nếu bảng đã tồn tại trong database thì kiểm trang các Field đã có hết chưa ko thì thêm vào
			if(isset($arrayTableList[$table])){
				//dua ra danh sach cac truong xem truong nao thua thi xoa thieu thi them vao
				$db_listColumn = new db_query("SHOW COLUMNS FROM " . $table);
				while($row = mysql_fetch_array($db_listColumn->result)){
					//các field có sẵn trong database				
					if(!isset($this->arrayFieldDefault[$row[0]]) && $row[0]!=$table . "_id"){
						if(!isset($tvalue[$row[0]])){
							$db_ex = new db_execute("ALTER TABLE " . $table . " DROP " . $row[0]);
							unset($db_ex);
						}else{
							$listFieldDatabase[$row[0]] = 	$row[0];
						}
					}
				}
				unset($db_listColumn);
				//các trường trong can phai co
				foreach($tvalue as $key1=>$row1){
					//kiểm tra kiểu dữ liệu
					if(!isset($listFieldDatabase[$key1])){
						switch($row1["fie_type"]){
							case 0:
								//kiểu string
								$this->createField($table,$key1," VARCHAR( 255 ) NULL");
							break;
							default:
								$this->createField($table,$key1," INT( 11 ) NULL");
							break;
						}
					}
				}
				//print_r($listFieldDatabase);
			}
			/*
			//nếu bảng đã tồn tại trong database thì kiểm trang các Field đã có hết chưa ko thì thêm vào
			if(isset($arrayTableList[$table])){
				//dua ra danh sach cac truong xem truong nao thua thi xoa thieu thi them vao
				$db_listColumn = new db_query("SHOW COLUMNS FROM " . $table);
				while($row = mysql_fetch_array($db_listColumn->result)){
					//các field có sẵn trong database				
					$listFieldSelect[$row[0]] = 	$row[0];
				}
				unset($db_listColumn);
				
				//các trường trong array
				foreach($arrayStructure[$table] as $key1=>$fvalue){	
					
					//kiểm tra kiểu dữ liệu
					switch($fvalue[1]){
						case 0:
							$listFieldArray[$key1 . '_text'] = $key1 . '_text';
						break;
						default:
							$listFieldArray[$key1] = $key1;
						break;
					}
				}
				
				//them cac truong mac dinh ko co trong array
				foreach($this->arrayFieldDefault as $key=>$value){
					$listFieldArray[$key] = $key;
					$listFieldArray[$table . "_id"] = $table . "_id";
				}
				$arrayAdd = array_diff($listFieldArray, $listFieldSelect);
				$arrayDrop = array_diff($listFieldSelect, $listFieldArray);
				
				//them cac truong chua co vao
				foreach($arrayAdd as $field=>$value){
					if(strpos($field,"_text")!==false){
						$db_ex = new db_execute("ALTER TABLE " . $table . " ADD " . $field . " VARCHAR( 255 ) NULL");
						unset($db_ex);
					}else{
						$db_ex = new db_execute("ALTER TABLE " . $table . " ADD " . $field . " INT( 11 ) NULL");
						unset($db_ex);
					}
				}
				
				//xoa cac truong khong co trong database
				foreach($arrayDrop as $field=>$value){
					$db_ex = new db_execute("ALTER TABLE " . $table . " DROP " . $field);
					unset($db_ex);
				}
			}else{
				$db_ex = new db_execute("CREATE TABLE " . $table . " (" . $table . "_id INT( 11 ) NULL AUTO_INCREMENT PRIMARY KEY)  ENGINE = MYISAM");
				unset($db_ex);
				foreach($arrayStructure[$table] as $key1=>$fvalue){
					//kiểm tra kiểu dữ liệu
					switch($fvalue[1]){
						case 0:
							$db_ex = new db_execute("ALTER TABLE " . $table . " ADD " . $key1 . "_text VARCHAR( 255 ) NULL");
							unset($db_ex);
						break;
						default:
							$db_ex = new db_execute("ALTER TABLE " . $table . " ADD " . $key1 . " INT( 11 ) NULL default 0");
							unset($db_ex);
						break;
					}
				}
				
				//them cac truong mac dinh ko co trong array
				foreach($this->arrayFieldDefault as $key=>$value){
					$db_ex = new db_execute("ALTER TABLE " . $table . " ADD " . $key . " INT( 11 ) NULL default 0");
					unset($db_ex);
				}

				//đánh index cho truong product_id
				$db_ex = new db_execute("ALTER TABLE `" . $table . "` ADD INDEX ( `" . $this->fieldRelate . "` )");
				unset($db_ex);
				
			}//end if
			
			
			//*/
		}//end foreach
		
	}//end function
	//hàm tạo trường
	function createField($table,$field,$type){
		$db_ex = new db_execute("ALTER TABLE " . $table . " ADD " . $field . " " . $type);
		unset($db_ex);
	}
}

?>