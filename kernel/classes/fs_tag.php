<?
class tag{
	
	var $store_term	= array();
	var $array_term	= array();
	var $id				= 0;
	var $text			= "";
	
	function tag($text, $text1 = ''){
		$this->text 	= $text;
		if($text1 != '')
			$this->text1 	= $text1;
	}
	
	/*
	Function insert tag to database
	*/
	function insert_tag(){
		
		#+
		#+ Xu ly du lieu dua vao - Remove hết các ký tự xuống dòng, Tab và thẻ HTML
		$text 	= removeHTML($this->text);
		$str	= array(chr(9), chr(10), chr(13), "-", '"', ".", "?", ":", "*", "%", "#", "|", "/", "\\", ",", "‘", "’", '“', '”', "&nbsp;", "(", ")", "{", "}", "  ", "   ", "    ","     ");
		$text	= str_replace($str, "", $text);
		$text	= mb_strtolower($text, "UTF-8");
		$text	= trim($text);

		#+
		#+ Be chuoi text voi cac khoang trang de tao ra array
		$array_text	= explode(" ", $text);
		for($i=0; $i<count($array_text); $i++){
			
			if(mb_strlen($array_text[$i]) >= 3){
				$this->store_term[] = $array_text[$i];
				$this->array_term[] = $array_text[$i];
			} // End if(mb_strlen($array_text[$i]) >= 3)
			
		} // End for($i=0; $i<count($array_text); $i++)
		
		#+
		#+ Voi nhung tu co duoc tao array them chu co 2 tu, 3 tu, 4 tu
		$num_arr = count($this->array_term);
		for($i=0; $i<$num_arr; $i++){
			#+
			#+ Chu co 2 tu
			if($i < $num_arr-1){
				$this->store_term[] = $this->array_term[$i] . " " . $this->array_term[$i+1];
			} // End if($i < $num_arr-1)
			
			#+
			#+ Chu co 3 tu
			if($i < $num_arr-2){
				$this->store_term[] = $this->array_term[$i] . " " . $this->array_term[$i+1] . " " . $this->array_term[$i+2];
			} // End if($i < $num_arr-2)

			#+
			#+ Chu co 4 tu
			if($i < $num_arr-3){
				$this->store_term[] = $this->array_term[$i] . " " . $this->array_term[$i+1] . " " . $this->array_term[$i+2] . " " . $this->array_term[$i+3];
			} // End if($i < $num_arr-3)
			
		} // End for($i=0; $i<$num_arr; $i++)
		
		#+
		#+ Dem so luong chu co 1 tu -> 4 tu xem co bao nhieu chu trung nhau
		$array_count	= array_count_values($this->store_term);
		$array_remove	= array(1);

		#+
		#+ Loai bo nhung chu chi xuat hien 1 lan
		$array_temp	= array_diff($array_count, $array_remove);

		#+
		#+ Tao ra array moi - Voi nhung chu > 1 tu thi dem so luong chu do xuat hien = so luong chu xuat hien * so luong tu trong chu * 1.5
		$new_array = array();
		foreach($array_temp as $key => $value){
			$arr_exp = explode(" ", $key);
			$num		= (count($arr_exp) > 1) ? $value*count($arr_exp)*1.5 : $value;
			$new_array[$key] = $num;
		} // End foreach($array_temp as $key => $value)
		
		#+
		#+ Sap xep theo so luong xuat hien cua chu va giu lai 10 chu dau tien
		arsort($new_array);
		array_splice($new_array, 10);
		
		#+
		#+ Tra lai 1 chuoi text meta_keyword de luu vao database
		$meta_keyword= '';
		$i = 0;
		foreach($new_array as $key => $value){
			$i ++;
			#+
			#+ Insert vao bang meta_keyword cua bang tbl_news
			$meta_keyword .= $i != 1 ? ', '.$key : $key;
		} // End foreach($new_array as $key => $value)
		
		
		#+
		#+ Neu co meta_keyword nhap bang tay
		if(is_array($this->text1)){
			foreach($this->text1 as $key => $value){
				if($value != '')	
					$new_array[trim($value)] = 0;
			} // End foreach($this->text1 as $key => $value)
		} // End if(is_array($this->text1))
		arsort($new_array);

 		/*/
		echo '<xmp>';
		print_r($new_array);
		echo '</xmp>';
		exit();
		//*/
		
		//*/
		$i = 0;
		foreach($new_array as $key => $value){
			$i ++;

			$tag_name 			= replaceMQ($key);
			$tag_name_index 	= removeTitle($tag_name,'/');
			$tag_name_index 	= strtolower($tag_name_index);
			
			#+
			#+ Kiem tra xem trong bang da co tu khoa nay chua
			$query = ' SELECT tag_id FROM tbl_tags WHERE tag_name_index = "'.$tag_name_index.'" LIMIT 1';
			$db_check = new db_query($query);
			$row = mysql_fetch_assoc($db_check->result);
			
			#+
			#+ Neu nhu da co roi
			if(mysql_num_rows($db_check->result) > 0){
				$query	= "UPDATE tbl_tags SET `tag_count` = `tag_count` + 1 WHERE tag_id = ".$row['tag_id'];
				$db_exe	= new db_execute($query);	
			#+
			#+ Neu chua co
			}else{
				#+
				#+ Them tag vao bang tags va lay ra id cua tag vua duoc them vao
				$query = "INSERT IGNORE INTO tbl_tags(`tag_name`,`tag_name_index`) VALUES('".$tag_name."', '".$tag_name_index."')";
				$db_ex = new db_execute($query);
			} // End if(mysql_num_rows($db_check->result) > 0)
 
		} // End foreach($new_array as $key => $value)
		//*/
		return $meta_keyword;
 
	} // End function insert_tag()
	
} // End class tag
?>