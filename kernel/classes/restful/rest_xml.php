<?
/*
Xử lý XML
*/
class rest_xml{
	/*
	XML đầu vào
	*/
	var $input_xml;
	var $xml_object = array();
	
	function rest_xml($input_xml){
		$this->input_xml = $input_xml;
	}
	
	function simplexml_load_string(){
		$this->xml_object = @simplexml_load_string($this->input_xml);
	}
	
	/*
	Lấy value của 1 key
	*/
	function getValue($key, $data_type = "int"){
		if (isset($this->xml_object->$key)){
			switch ($data_type){
				case "dbl": return doubleval($this->xml_object->$key); break;
				case "str": return strval($this->xml_object->$key); break;
				default: return intval($this->xml_object->$key); break;
			}
		}
		else return "";
	}
	
}
?>