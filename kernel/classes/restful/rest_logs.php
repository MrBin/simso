<?
class Rest_Logs{
	//Log file
	var $var_log_file				= "rest_logs.cfn";

	var $log_date					= 0;
	var $log_source				= 0;
	var $log_type					= 0;
	var $log_item					= 0;
	var $log_checksum				= "";
	var $log_from_url				= "";
	var $log_to_url				= "";
	var $log_request_header		= "";
	var $log_respond_header		= "";
	var $log_keyword				= "";
	var $log_from_ip				= "";
	var $log_to_ip					= "";
	var $log_backtrace			= "";
	var $log_status 				= 0;

	function AddStatus($status = 0){
		$this->log_status	= $status;
	}

	// 1. Nhanh, 2.Baokim
	function AddSource($log_source){
		$this->log_source = $log_source;
	}

	// Phân loại log theo source Log số tồn, log thông tin sản phẩm ...
	function AddType($log_type){
		$this->log_type = $log_type;
	}

	// ID Unique Vd. product_id
	function AddItem($log_item){
		$this->log_item = $log_item;
	}

	// ID Unique Vd. product_id
	function AddChecksum($log_checksum){
		$this->log_checksum = $log_checksum;
	}

	// Thời gian lưu log
	function AddDate($log_date){
		$this->log_date = time();
	}

	function AddFromUrl($log_from_url){
		$this->log_from_url = $log_from_url;
	}

	function AddToUrl($log_to_url){
		$this->log_to_url = $log_to_url;
	}

	function AddRequestHeader($log_request_header){
		$this->log_request_header = $log_request_header;
	}

	function AddRespondHeader($log_respond_header){
		$this->log_respond_header = $log_respond_header;
	}

	function AddKeyword($log_keyword){
		$this->log_keyword = $log_keyword;
	}

	function AddFromIp($log_from_ip){
		$this->log_from_ip = $log_from_ip;
	}

	function AddToIp($log_to_ip){
		$this->log_to_ip = $log_to_ip;
	}


	function SaveLogs(){

		// Ghi log file chan le theo khoang thoi gian 5ph
		if(intval(date('i')/5)%2 == 0){
			$var_log_file = "rest_logs_0.cfn";
		}else{
			$var_log_file = "rest_logs_1.cfn";
		}

		$dirname 	= dirname(__FILE__);
		$dirname 	= str_replace(DIRECTORY_SEPARATOR . "classes" . DIRECTORY_SEPARATOR . "restful", DIRECTORY_SEPARATOR . "ipstore" . DIRECTORY_SEPARATOR, $dirname);
		$filename 	= $dirname . $var_log_file;

		// Nếu file lớn hơn 1GB thì bỏ qua
		if(is_file($filename) && filesize($filename) > pow(1024, 3)) return;

		$back_track 	= debug_backtrace();
	   $caller     	= array_shift($back_track);
	   $arrBacktrace 				= array();
	   $arrBacktrace['file'] 	= isset($caller['file']) ? $caller['file'] : '';
	   $arrBacktrace['line'] 	= isset($caller['line']) ? $caller['line'] : '';
	   $this->log_backtrace = json_encode($arrBacktrace);
	   //echo '<xmp>'; print_r($arrBacktrace); echo '</xmp>'; exit();

		$arrDataLogs 	= array( 'log_source' 			=> $this->log_source,
										'log_type' 				=> $this->log_type,
										'log_item' 				=> $this->log_item,
										'log_checksum' 		=> $this->log_checksum,
										'log_date' 				=> $this->log_date,
										'log_from_url' 		=> $this->log_from_url,
										'log_to_url' 			=> $this->log_to_url,
										'log_request_header' => $this->log_request_header,
										'log_respond_header' => $this->log_respond_header,
										'log_from_ip' 			=> $this->log_from_ip,
										'log_to_ip'	 			=> $this->log_to_ip,
										'log_keyword'			=> $this->log_keyword,
										'log_backtrace'	 	=> $this->log_backtrace,
										'log_active'			=> $this->log_status
										);

		$handle 		= @fopen($filename, 'a');
		//Nếu handle chưa có mở thêm ../
		if (!$handle) $handle = @fopen($filename, 'a');
		//Nếu ko mở đc lần 2 thì exit luôn
		if (!$handle) return;
		//Ghi file
		fwrite($handle, json_encode($arrDataLogs) . "\n");
		fclose($handle);
	}
}
?>