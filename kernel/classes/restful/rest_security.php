<?
class Request_Security{
	
	
	var $basic_authentication_username = "";
	var $basic_authentication_password = "";
	
	var $rest_url = "http://api.vatgia.com/user/subzero";
	var $error_msg = "";
	
	var $rest_access_key_id = "";
	var $rest_payload			= "";
	var $rest_share_key		= "";
	var $rest_checksum		= "";
	 
	var $user_agent = "";

	//CURL Handle
	var $ch;
	
	/**
	Khởi tạo class
	$rest_access_key_id : Khóa giành riêng cho user
	$rest_payload : Dữ liệu đc post lên
	*/
	
	function Request_Security($rest_access_key_id, $rest_payload, $rest_share_key, $rest_url){
	
		$this->rest_access_key_id	= $rest_access_key_id;
		$this->rest_payload			= $rest_payload;
		$this->rest_share_key		= $rest_share_key;
		$this->rest_url 				= $rest_url;
		
	}
	
	//Thiết lập user_name và password cho Basic_Authentication
	function Set_Basic_Authentication($user_name, $password){
		
		$this->basic_authentication_username = $user_name;
		$this->basic_authentication_password = $password;
		
		return $this;
	}
	
	/*
	Thiết lập user agent
	*/
	function Set_User_Agent($user_agent){
	
		$this->user_agent = $user_agent;
		return $this;
		
	}
	
	/*
	Khởi tạo CURL
	*/
	function CURL_init(){
		//Khởi tao CURL
		$this->ch = curl_init();
		//Set URL
		curl_setopt($this->ch, CURLOPT_URL, $this->rest_url);
		
		//SSL
		curl_setopt($this->ch, CURLOPT_SSL_VERIFYPEER, FALSE); 
		curl_setopt($this->ch, CURLOPT_SSL_VERIFYHOST, FALSE); 
		
		//Set Fail on Error khi gặp http code > 400 (401: Unauthorized ...)
		curl_setopt($this->ch, CURLOPT_FAILONERROR, true);
		
		//Thiết lập user và password cho Basic_Authentication
		if ($this->basic_authentication_username != "" && $this->basic_authentication_password != ""){
			curl_setopt($this->ch, CURLOPT_USERPWD, $this->basic_authentication_username . ":" . $this->basic_authentication_password);
			curl_setopt($this->ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
		}
		
		/*
		Thiết lập user agent
		*/
		if ($this->user_agent != ""){
			curl_setopt($this->ch, CURLOPT_USERAGENT, $this->user_agent);
		}
		
		//Set return
		curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, true);
		
	} // End CURL_init()
	
	/*
	Kiểm tra lỗi và đóng CURL
	*/
	function CURL_error(){
		
		//Nếu có lỗi thì return lại lỗi
		if(curl_errno($this->ch)){

			//Set error
			$this->Set_Request_Error("Error Number = " . curl_errno($this->ch) . ". Error Message = " . curl_error($this->ch));
			curl_close($this->ch);
			
			return false;
			
		} 
		
		// close cURL resource, and free up system resources
		curl_close($this->ch);
		return true;
	
	} // End CURL_error()
	
	/*
	Post dữ liệu
	*/
	function Post_Data(){
		
		$this->CURL_init();
		/*
		Add dữ liệu vào để POST
		*/
		$post_array = array("rest_access_key_id" 	=> $this->rest_access_key_id, 
								  "rest_payload" 			=> $this->rest_payload,
								  "rest_checksum" 		=> $this->Generate_Checksum(),
								  );
										  
		curl_setopt($this->ch, CURLOPT_POST, 1);
		curl_setopt($this->ch, CURLOPT_POSTFIELDS, $post_array);

		$data = curl_exec($this->ch);
		
		//echo $data;
		//Check xem có lỗi gì ko?
		if (!$this->CURL_error()) return false;
		
		return $data;

	} // End Post_Data()
	
	/*
	Get_Data: Lấy dữ liệu theo method GET
	*/
	function Get_Data(){
		
		$this->CURL_init();

		$data = curl_exec($this->ch);
		
		//echo $data;
		//echo $this->CURL_error();
		//Check xem có lỗi gì ko?
		if (!$this->CURL_error()) return false;
		
		return $data;
		
	} //End Get_Data()
	
	
	/*
	Lấy error
	*/
	function Get_Request_Error(){
		
		return $this->error_msg;
		
	}
	
	/*
	Set error
	$error_msg 	: Thông báo lỗi
	$clear 		: Clear các thông báo lỗi ở trước
	*/
	function Set_Request_Error($error_msg, $clear=0){
		if ($clear != 0) $this->error_msg = "";
		$this->error_msg .= $error_msg;
	}
	
	/*
	Tạo checksum cho POST
	*/
	function Generate_Checksum(){
		return hash("sha256", hash("sha256", $this->rest_payload) . $this->rest_share_key);
	}	
	
}
?>