<?
class Rest_Request{
	
	var $response_type = "XML";
	
	
	/*
	Khởi tạo class
	*/
	function Rest_Request(){
		
	}
	
	/**
	Lấy method GET, POST, DELETE
	*/
	function getRequestMethod(){
		switch (@$_SERVER['REQUEST_METHOD']){
			case "POST": 	return "POST"; break;
			case "DELETE": return "DELETE"; break;
			default:	 		return "GET"; break;
		}
	}
	
	/**
	Trả lại kết quả, có thể kiểu XML, có thể kiêu JSON
	$result_set: Lấy từ database
	$arrayTemplate: Định nghĩa tên các trường
	*/
	function Response($arrayResult, $arrayTemplate){
		switch ($this->response_type){
			default: 
				return $this->ResponseXML($arrayResult, $arrayTemplate); 
			break;
		}
	}
	
	/**
	Trả về chuỗi XML
	*/
	function ResponseXML($arrayResult, $arrayTemplate){
		$str_output =  '<?xml version="1.0" encoding="utf-8" ?> ' . "\n";
		$str_output .= '<message>' . "\n";
		
		//Loop toàn bộ column của $arrayResult
		foreach($arrayResult as $m_key => $m_value){
			//Nếu tồn tại định nghĩa thì mới in Tag XML đó ra
			if (isset($arrayTemplate[$m_key])){
				//Show ra XML TAG + Value
				if (strval($m_value) != "") $str_output .= '<' . $arrayTemplate[$m_key] . '>' . htmlspecialchars($m_value) . '</' . $arrayTemplate[$m_key] . '>' . "\n";
			}
		} //End foreach
		
		//Send kèm Agent
		$str_output .= "<User_Agent>" . @$_SERVER['HTTP_USER_AGENT'] . "</User_Agent>\n";
		
		$str_output .= '</message>';
		
		return $str_output;
	}
	
	/**
	ResponseError: Trả về lỗi
	errorCode: mã lỗi
	errorDescription: mô tả lỗi
	*/
	function ResponseError($errorCode, $errorDescription){
		switch ($this->response_type){
			default: 
				return $this->ResponseErrorXML($errorCode, $errorDescription); 
			break;
		}
	}
	
	/**
	Trả lại lỗi dạng XML
	*/
	function ResponseErrorXML($errorCode, $errorDescription){
		$str_output =  '<?xml version="1.0" encoding="utf-8" ?> ' . "\n";
		$str_output .= '<message>' . "\n";
		$str_output .= '<ErrorCode>' . htmlspecialchars($errorCode) . '</ErrorCode>' . "\n";
		$str_output .= '<ErrorDescription>' . htmlspecialchars($errorDescription) . '</ErrorDescription>' . "\n";
		$str_output .= '</message>';
		
		return $str_output;
	}
	
	/**
	Check Sum
	*/
	function CheckSumPostData($rest_access_key_id, $rest_payload, $rest_checksum){
		global $VG_user_security;

		//Kiểm tra xem key có tồn tại hay ko
		if (!isset($VG_user_security[$rest_access_key_id])) return "Invalid Accesss Key ID";
		
		$new_rest_checksum = hash("sha256", hash("sha256", $rest_payload) . $VG_user_security[$rest_access_key_id]);
		
		//CheckSum
		if (strval($rest_checksum) != strval($new_rest_checksum)) return "Invalid CheckSum";
		
		//No error	
		return "1";
		
	}
}

?>