<?php
require_once('fs_json.php');

class site_info
{
    protected $url      = null;
    protected $http_url = null;
    protected $ch       = null;
    protected $json     = null;
    
    function __construct($url)
    {
        $this->url      = $url;
        $this->http_url = 'http://'.$url;
        $this->json     = new Services_JSON;
		$this->_curl_init();
    }
	
	
	/*
     *
     * Get site title
    */
	public function get_title()
    {
        $curl    = $this->_curl_get('http://'.$this->url);
        
        if( isset($curl->content) )
        {
            if (preg_match("|<[\s]*title[\s]*>([^<]+)<[\s]*/[\s]*title[\s]*>|Ui", $curl->content, $match))
            {
                return $match[1];
            }
        }
        
		return FALSE;
    }
	
	/*
     *
     * Get network information
    */
    public function get_network_info()
    {
        $curl      = $this->_curl_post('http://www.yougetsignal.com/tools/network-location/php/get-network-location-json.php', array('remoteAddress'=>$this->url));
        
        if( isset($curl->content) )
        {
            return $this->json->decode($curl->content);
        }
        
        return FALSE;
    }
    
    /*
     *
     * Get meta tags
    */
    public function get_meta_tags( $keys=array() )
    {
		@$tags = get_meta_tags('http://'.$this->url);
        
        if( !empty($keys) )
        {
            foreach( $keys as $k ){
                $out[$k] = ( isset($tags[$k]) ) ? $tags[$k] : NULL;
            }
            
            return $out;
        }
        
        return $tags;
		/*/
        $tags = get_meta_tags('http://'.$this->url);
        $out  = new stdClass;
        
        if( !empty($keys) )
        {
            foreach( $keys as $k ){
                $out->$k = ( isset($tags[$k]) ) ? $tags[$k] : NULL;
            }
        }
        else
        {
            if( !empty($tags) )
            {
                foreach( $tags as $k => $v ){
                    $out->$k = $v;
                }
            }
        }
        
        return $out;
		//*/
    }
	
	
	/*/
    public function get_title()
    {
        // we can't treat it as an XML document because some sites aren't valid XHTML
		// so, we have to use the classic file reading functions and parse the page manually
		$fh = @fopen($this->http_url,"r");
		$str = fread($fh, 7500);  // read the first 7500 characters, it's gonna be near the top
		fclose($fh);
	
		if (preg_match('/<title>(.*?)<\/title>/is',$str,$found)) {
			return $title = $found[1];
		} else {
			return $title = "";
		}
    }
	//*/

	
    /*
     *
     * Get age of site
    */
    public function get_age()
    {
        $curl = $this->_curl_get('http://www.who.is/whois-com/ip-address/'.$this->url);
        
		if (preg_match('#Creation Date: ([a-z0-9-]+)#si', $curl->content, $p))
        {
			$time = time() - strtotime($p[1]);
            
            $age        = new stdClass;
			$age->years = round($time / 31556926);
            $age->days  = round(($time % 31556926) / 86400);
            
            return $age;
        }
        
        return FALSE;
    }
    
    /*
     *
     * Get another site(s) on same ip
    */
    public function get_neighbours()
    {
        $curl      = $this->_curl_post('http://www.yougetsignal.com/tools/web-sites-on-web-server/php/get-web-sites-on-web-server-json-data.php', array('remoteAddress'=>$this->url));
        $neighbour = array();
        
        if( isset($curl->content) )
        {
            $data = $this->json->decode( $curl->content );
            
            if( isset($data->domainArray) && !empty($data->domainArray) )
            {
                foreach( $data->domainArray as $v )
                {
                    if( $v[0] ){
                        $neighbour[] = $v[0];
                    }
                }
            }
        }
        
        return $neighbour;
    }
    
    /*
     *
     * Get whois information
    */
    public function get_whois()
    {
        $curl      = $this->_curl_post('http://www.yougetsignal.com/tools/whois-lookup/php/get-whois-lookup-json-data.php', array('remoteAddress'=>$this->url));
        
        if( isset($curl->content) )
        {
            $data = $this->json->decode($curl->content);
            
            if( isset($data->whoisData) ){
                return $data->whoisData;
            }
        }
        
        return FALSE;
    }
    
    
    
    
	
	/*
     *
     * Get information about site on Alexa.com
    */
    public function get_alexa_info()
    {
        $curl    			= $this->_curl_get('http://data.alexa.com/data?cli=10&dat=s&url='.$this->url);
		$info    			= new stdClass;
		$info->rank 		= 0;
		$info->backlink 	= 0;
		$info->review_avg 	= 0;
		$info->review_num 	= 0;
        
        if( isset($curl->content) )
        {
			#Rank
			if ( preg_match('#<POPULARITY URL="(.*?)" TEXT="([0-9]+){1,}"/>#si', $curl->content, $match) ){
				$info->rank = isset($match[2]) ? $match[2] : 0;
			}
			
			#Backlink
			if ( preg_match('/\<linksin num\=\"([0-9]+)\"\/\>/i', $curl->content, $match) ){
				$info->backlink = isset($match[1]) ? $match[1] : 0;
			}
			
			#Review
			if ( preg_match('/\<reviews avg\=\"([0-9\.]+)\" num\=\"([0-9]+)\"\/\>/i', $curl->content, $match) ) {
				$info->review_avg = isset($match[1]) ? $match[1] : 0;
				$info->review_num = isset($match[2]) ? $match[2] : 0;
			}
        }
        
		return $info;
    }
	
	/*
     *
     * Get Yahoo backlinks
    */
	public function get_yahoo_backlink()
	{
		/*
		$curl    			= $this->_curl_get('http://search.yahooapis.com/SiteExplorerService/V1/inlinkData?appid=YahooDemo&output=json&omit_inlinks=domain&entire_site=userinputoptions&results='.$limit.'&start='.$start.'&query=http://'.$this->url);
		$info				= new stdClass;
		$info->total        = 0;
		$info->links        = array();
		
		if( isset($curl->content) )
		{
			$data        = $this->json->decode($curl->content);
			$info->total = isset($data->ResultSet->totalResultsAvailable) ? $data->ResultSet->totalResultsAvailable : 0;
			$info->links = isset($data->ResultSet->Result) ? $data->ResultSet->Result : array();
		}
		
		return $info;
		*/
		$curl = $this->_curl_get('http://siteexplorer.search.yahoo.com/advsearch?bwm=i&bwmo=d&bwmf=s&p=http://'.$this->url);
		if( isset($curl->content) )
		{
			if (preg_match('/Inlinks \((.*)\)/Us', $curl->content, $match)) {
				return $match[1];
			}
		}
		
		return 0;
	}
	
	/*
     *
     * Get Google backlinks
    */
	public function get_google_backlink()
	{
		$curl    			= $this->_curl_get('http://ajax.googleapis.com/ajax/services/search/web?v=1.0&q=link:'.$this->url);
		
		if( isset($curl->content) )
		{
			$data        = $this->json->decode($curl->content);
			return $data->responseData->cursor->estimatedResultCount;
		}
		
		return 0;
	}
	
	/*
     *
     * Get Google backlinks
    */
	public function get_google_pr()
	{
		require_once("google_pagerank.class.php");
		@$rank_obj = new GooglePageRank($this->url);
		//echo (int)$rank_obj->pagerank;
		
		return (int)$rank_obj->pagerank;
	}
	
	/*
     *
     * Is site on DMOZ.ORG ?
     * @return bool TRUE if is listing, FALSE if else
    */
	public function is_dmoz()
	{
		$curl  = $this->_curl_get('http://search.dmoz.org/search/search?q='.$this->url);
		
		if( isset($curl->content) )
        {
			if( preg_match('/Open Directory Categories/', $curl->content) ){
				return TRUE;
			}
		}
		
		return FALSE;
	}
	
	/*
     *
     * Is site on Yahoo Directory?
     * @return bool TRUE if is listing, FALSE if else
    */
	public function is_yahoo_dir()
	{
		$curl  = $this->_curl_get('http://search.yahoo.com/search/dir?p='.$this->url);
		
		if( isset($curl->content) )
        {
			if( !preg_match('/No Directory Search results were found/', $curl->content) ){
				return TRUE;
			}
		}
		
		return FALSE;
	}
	
	/*
     *
     * Get loading time
     * @return float time in second(s)
    */
	public function get_loading_time()
	{
		$start = microtime(true);
		$curl  = $this->_curl_get('http://'.$this->url);
		$end   = microtime(true);
		
		return ($end - $start);
	}
	
	/*
     *
     * Get Alexa.com thumb
     * @return string thumbnail link
    */
	public function get_alexa_thumb($width=250, $height=150)
	{
		return 'http://traffic.alexa.com/graph?c=1&u='.$this->url.'&r=6m&y=r&z=3&h='.$height.'&w='.$width.'&b=FFFFFF';
	}
	
	/*
     *
     * Get Compete.com thumb
     * @return string thumbnail link
    */
	public function get_compete_thumb()
	{
		return 'http://origin.grapher.compete.com/site_media/snapshot/'.$this->url.'_uv_310.png';
	}
	
	/*
     *
     * Get inbound & outbound link
    */
	public function get_inout_link()
	{
		$curl     = $this->_curl_get('http://'.$this->url);
		$inbound  = array();
		$outbound = array();
		
		if( isset($curl->content) )
        {
			preg_match_all("/a[\s]+[^>]*?href[\s]?=[\s\"\']+(.*?)[\"\']+.*?>"."([^<]+|.*?)?<\/a>/", $curl->content, $matches);
			
			if( !empty($matches[1]) )
			{
				foreach( $matches[1] as $match )
				{
					if( !preg_match('/^(http|https|ftp):\/\/([A-Z0-9][A-Z0-9_-]*(?:\.[A-Z0-9][A-Z0-9_-]*)+):?(\d+)?\/?/i', $match) || preg_match('/^(http|https|ftp):\/\/'.$this->url.':?(\d+)?\/?/i', $match) )
					{
						if( !preg_match('/javascript:/', $match) )
						{
							$inbound[] = $match;
						}
					}
					else
					{
						$outbound[] = $match;
					}
				}
			}
		}
		
		$data 			= new stdClass;
		$data->inbound  = $inbound;
		$data->outbound = $outbound;
		
		return $data;
	}
	
    
    /*
     *
     * cURL functions
    */
    protected function _curl_init()
    {
		$this->ch = curl_init();
		@curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, true);
		@curl_setopt($this->ch, CURLOPT_AUTOREFERER, true);
		@curl_setopt($this->ch, CURLOPT_FOLLOWLOCATION, true);
		@curl_setopt($this->ch, CURLOPT_MAXREDIRS, 2);
		@curl_setopt($this->ch, CURLOPT_CONNECTTIMEOUT, 15);
		@curl_setopt($this->ch, CURLOPT_TIMEOUT, 35);
		@curl_setopt($this->ch, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
	}
    
    protected function _curl_get($url)
    {
        #Set extra cURL option
		if (preg_match('/^https/i', $url)){
            curl_setopt($this->ch, CURLOPT_SSL_VERIFYPEER, false);
		}
		curl_setopt($this->ch, CURLOPT_URL, $url);
		curl_setopt($this->ch, CURLOPT_REFERER, $url);
        
        #Get response
        $curl          = new stdClass;
		$curl->content = curl_exec($this->ch);
        $curl->info    = curl_getinfo($this->ch);
        
        #Return data
        return $curl;
	}

	protected function _curl_post($url, $post_data=array())
    {
        #Set extra cURL option
		if (preg_match('/^https/i', $url)){
            curl_setopt($this->ch, CURLOPT_SSL_VERIFYPEER, false);
		}
		curl_setopt($this->ch, CURLOPT_URL, $url);
		curl_setopt($this->ch, CURLOPT_REFERER, $url);
		curl_setopt($this->ch, CURLOPT_POST, 1);
		curl_setopt($this->ch, CURLOPT_POSTFIELDS, $post_data);
        
        
        #Get response
        $curl          = new stdClass;
		$curl->content = curl_exec($this->ch);
        $curl->info    = curl_getinfo($this->ch);
        
        #Return data
        return $curl;
	}
}