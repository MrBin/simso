<?
class sphinx_search{

	const KEYWORD_FIRST   = 1;
	const KEYWORD_BETWEEN = 2;
	const KEYWORD_END     = 3;

	public $sphinx_keyword   =	"";
	public $sphinx_keyword_2 =	"";

	function __construct($sphinx_keyword){

		if($sphinx_keyword != ""){
			$this->sphinx_keyword	= self::validateKeywords($sphinx_keyword);
			$this->sphinx_keyword_2	= self::validateKeywords2($sphinx_keyword);
		}

	}

	static function generateX($string, $position){

		$arrReturn = array();
		$countX    = substr_count($string, "x");
		$keyLen    = strlen($string);

		$arrBetweenFirst = array();
		$arrBetweenLast  = array();

		if($countX > 0){
			for ($i=0; $i <= 9; $i++) { 
				$arrReturn[] = str_replace("x", $i, $string);
			}
		}
		else{
			
			if($keyLen == 1){

				switch ($position) {
					// FIRST
					case 1:

						for ($i=0; $i <= 9; $i++) { 
							$arrReturn[] = str_replace($string, $string . $i, $string);
						}
						break;

					// BETWEEN
					case 2:

						for ($i=0; $i <= 9; $i++) { 
							$arrBetweenFirst[] = str_replace($string, $string . $i, $string);
							$arrBetweenLast[]  = str_replace($string, $i . $string, $string);
						}

						$arrReturn = array_merge($arrBetweenFirst, $arrBetweenLast);

						break;
					// LAST
					case 3:

						for ($i=0; $i <= 9; $i++) { 
							$arrReturn[] = str_replace($string, $i . $string, $string);
						}

						break;
				}
			}
			else $arrReturn = array(0 => $string);
		}

		return $arrReturn;
	}

	static function getBetweenKeyword($keyword, $start = "*", $end = "*"){

		$arrReturn = array();
		$strReturn = "";

		$countStar = substr_count($keyword, "*");

		if($countStar > 1){
			$r = explode($start, $keyword);
			if (isset($r[1])){
				$r = explode($end, $r[1]);
				$strReturn = $r[0];
			}

			if($strReturn != "") $arrReturn = self::generateX($strReturn, self::KEYWORD_BETWEEN);
		}

		return $arrReturn;

	}

	static function getFirstKeyword($keyword, $char = "*"){

		$strReturn = "";
		$arrReturn = array();

		$spot      = strpos($keyword, $char);

		$countStar = substr_count($keyword, $char);

		if($countStar > 0) $strReturn = substr($keyword, 0, $spot);

		if($strReturn != "") $arrReturn = self::generateX($strReturn, self::KEYWORD_FIRST);

		return $arrReturn;
	}

	static function getLastKeyword($keyword, $char = "*"){
		
		$strReturn = "";
		$arrReturn = array();

		$strReturn	= substr($keyword, strrpos($keyword, '*' )+1);

		if($strReturn != "") $arrReturn = self::generateX($strReturn, self::KEYWORD_END);

		return $arrReturn;

	}

	static function validateKeywords($keyword){

		$keyword        	= trim($keyword);
		$sphinx_keyword 	= "";
		$keywordBetween	= "";
		if($keyword != ""){
			$keywords = preg_replace("/[^0-9*xX]/si", "", $keyword);

			$keywords = str_replace(array("X", "**"), array("x", "*"), $keywords);

	   	if ($keywords != ""){

				$spot  = strpos($keywords, "*");
				$slen  = strlen($keywords);

				$countStar = substr_count($keywords, "*");

				if($countStar > 0){

					$sphinx_keyword  = "";

					$arrKFirst   = self::getFirstKeyword($keywords);
					$arrKBetween = self::getBetweenKeyword($keywords);
					$arrKLast    = self::getLastKeyword($keywords);

					// Nếu chỉ có đầu
					if(!empty($arrKFirst) && empty($arrKBetween) && empty($arrKLast)){

						foreach ($arrKFirst as $key => $value) {
							if($key == 0) $sphinx_keyword .= '@(sim_keyword) "^' . genTrigramsKeyword($value) . '"';
							else $sphinx_keyword .= ' | "^' . genTrigramsKeyword($value) . '"';
						}

					}
					// Nếu chỉ có giữa
					else if(empty($arrKFirst) && !empty($arrKBetween) && empty($arrKLast)){

						foreach ($arrKBetween as $key => $value) {
							if($key == 0) $sphinx_keyword .= '@(sim_keyword)  "' . genTrigramsKeyword($value) . '"';
							else $sphinx_keyword .= ' | "' . genTrigramsKeyword($value) . '"';
						}

					}
					// Nếu chỉ có cuối
					else if(empty($arrKFirst) && empty($arrKBetween) && !empty($arrKLast)){

						foreach ($arrKLast as $key => $value) {

							if($key == 0) $sphinx_keyword .= '@(sim_keyword)  "^ ' . genTrigramsKeyword($value) . '$"';
							else $sphinx_keyword .= ' | "^ ' . genTrigramsKeyword($value) . '$"';
						}

					}
					// Nếu có đâu và giữa
					else if(!empty($arrKFirst) && !empty($arrKBetween) && empty($arrKLast)){

						foreach ($arrKFirst as $key => $value) {

							if($key == 0) $sphinx_keyword .= '(@(sim_keyword) "^' . genTrigramsKeyword($value) . '"';
							else $sphinx_keyword .= ' | (@(sim_keyword) "^' . genTrigramsKeyword($value) . '"';

							foreach ($arrKBetween as $kCenter => $vCenter) {
								if($kCenter == 0) $sphinx_keyword .= ' " * ' . genTrigramsKeyword($vCenter) . ' * ")';
								else $sphinx_keyword .= ' | (@(sim_keyword) "^' . genTrigramsKeyword($value) . '" " * ' . genTrigramsKeyword($vCenter) . ' * ")';
							}

						}

					}
					// Nếu chỉ có đầu và cuối
					else if(!empty($arrKFirst) && empty($arrKBetween) && !empty($arrKLast)){

						foreach ($arrKFirst as $key => $value) {

							if($key == 0) $sphinx_keyword .= '(@(sim_keyword) "^' . genTrigramsKeyword($value) . '"';
							else $sphinx_keyword .= ' | (@(sim_keyword) "^' . genTrigramsKeyword($value) . '"';

							foreach ($arrKLast as $kLast => $vLast) {
								if($kLast == 0) $sphinx_keyword .= ' "' . genTrigramsKeyword($vLast) . '$")';
								else $sphinx_keyword .= ' | (@(sim_keyword) "^' . genTrigramsKeyword($value) . '" "' . genTrigramsKeyword($vLast) . '$")';
							}

						}

					}
					// Nếu chỉ có giữa và cuối
					else if(empty($arrKFirst) && !empty($arrKBetween) && !empty($arrKLast)){

						foreach ($arrKBetween as $key => $value) {

							if($key == 0) $sphinx_keyword .= '(@(sim_keyword) " * ' . genTrigramsKeyword($value) . ' * "';
							else $sphinx_keyword .= ' | (@(sim_keyword) " * ' . genTrigramsKeyword($value) . ' * "';

							foreach ($arrKLast as $kLast => $vLast) {
								if($kLast == 0) $sphinx_keyword .= ' "' . genTrigramsKeyword($vLast) . '$")';
								else $sphinx_keyword .= ' | (@(sim_keyword) " * ' . genTrigramsKeyword($value) . '" "' . genTrigramsKeyword($vLast) . '$")';
							}

						}

					}

				}
				else{

					if(strlen($keywords) > 7){
						$keywords = genTrigramsKeyword($keywords);

						$sphinx_keyword = '@(sim_keyword) "' . $keywords . '" /5';
					}
					else{

						$arrNotStar = self::generateX($keywords);

						if(!empty($arrNotStar)){
							foreach ($arrNotStar as $key => $value) {
								if($key == 0) $sphinx_keyword .= '@(sim_keyword)  "^ ' . genTrigramsKeyword($value) . '$"';
								else $sphinx_keyword .= ' | "^ ' . genTrigramsKeyword($value) . '$"';
							}
						}
					}
				}

	   	}
		}
		// var_dump($sphinx_keyword);

		return $sphinx_keyword;
	}

	static function validateKeywords2($keyword){

		$keyword        	= trim($keyword);
		$sphinx_keyword 	= "";
		$keywordBetween	= "";
		if($keyword != ""){
			$keywords = preg_replace("/[^0-9*xX]/si", "", $keyword);

			$keywords = str_replace(array("X", "**"), array("x", "*"), $keywords);

	   	if ($keywords != ""){

				$spot  = strpos($keywords, "*");
				$slen  = strlen($keywords);

				$countStar = substr_count($keywords, "*");

				if($countStar > 0){

					$sphinx_keyword  = "";

					$arrKLast    = self::getLastKeyword($keywords);

					if(!empty($arrKLast)){

						foreach ($arrKLast as $key => $value) {

							if($key == 0) $sphinx_keyword .= '@(sim_keyword)  "^ ' . genTrigramsKeyword($value) . '$"';
							else $sphinx_keyword .= ' | "^ ' . genTrigramsKeyword($value) . '$"';
						}

					}

				}

	   	}
		}
		// var_dump($sphinx_keyword);

		return $sphinx_keyword;
	}

	public function search($option=array()){

		$arrData = array(
								"data" 			=> array(),
								"total_record" => 0
						);

		$opts	= array ("page"		=> 1,
							"page_size"	=> 100,
							"iCat"		=> 0,
							"iType"		=> 0,
							"iPrice"		=> 0,
							"iDauSo"		=> 0,
							"iDaiLy"		=> 0,
							"digit"		=> 0,
							"iNguHanh"	=> 0,
							"module"		=> "",
							"sort"		=> 0,
							);
		// Gán lại option
		if(is_array($option) && count($option) > 0) $opts	= $option + $opts;

		// Sửa lại page
		if($opts["page"] < 1)	$opts["page"] = 1;
		if($opts["page"] > 100)	$opts["page"] = 100;

		// Thêm $sqlSelect để sort
		$sqlSelect				= "";
		$sqlWhere				= " sim_price >= " . doubleval(10000) . " AND sim_price <= " . doubleval(900000000000) . " AND sim_active = 1";
		$sqlOrderBy				= " ORDER BY sim_web DESC";

		// Tìm theo category
		if($opts["iCat"] > 0)	$sqlWhere	.= " AND sim_category = " . $opts["iCat"];
		// Tìm theo city
		if($opts["iType"] > 0)	$sqlWhere	.= " AND sim_typeid = " . $opts["iType"];
		// Tìm theo giá
		if($opts["iPrice"] > 0)	$sqlWhere	.= " AND sim_priceid = " . $opts["iPrice"];

		if($opts["iDauSo"] > 0)	$sqlWhere	.= " AND sim_dausoid = " . $opts["iDauSo"];

		if($opts["iDaiLy"] > 0) $sqlWhere	.= " AND sim_dailyid = " . $opts["iDaiLy"];

		// Dua ra sim 10 so hay 11 so
      // if($opts["digit"] == 1) $sqlWhere	.= " AND sim_digit_1 = 1";
      // elseif($opts["digit"] == 2) $sqlWhere	.= " AND sim_digit_2 = 1";

      // Dua ra nhung sim hop ngu hanh sinh khac
      if($opts["iNguHanh"] != 0){

      	$sqlWhere .= " AND sim_nguhanh = " . $opts["iNguHanh"];

      	$sqlWhere .= " AND sim_diem_stst >= 7 AND sim_diem_stst <= 10";
      	$sqlWhere .= " AND sim_diem_vietaa >= 6 AND sim_diem_vietaa <= 10";
      	$sqlWhere .= " AND sim_sonut >= 6 AND sim_sonut <= 10";
      
      } // End if($iNguHanh != 0)

      // Xử lý keyword 2
      $sqlWhereKey = $sqlWhere;

      if($this->sphinx_keyword_2 != ""){

   		$sqlWhereKey =  $sqlWhere . " AND MATCH('" . $this->sphinx_keyword_2 . "')";
   	}

      // Xử lý keyword
      if($this->sphinx_keyword != ""){

   		$sqlWhere .= " AND MATCH('" . $this->sphinx_keyword . "')";
   	}

   	// Ngu hanh
      if($opts["iNguHanh"] != 0) $sqlOrderBy .= ", sim_diem_stst DESC";

      #echo $opts["sort"];
      if($opts["sort"] == 1 || $opts["module"] == "simsodep") $sqlOrderBy .= ", sim_price DESC";
      else $sqlOrderBy .= ", sim_price ASC";

      $sqlOrderBy	.= ", WEIGHT() DESC";

      // Lấy count kết quả
      $query_count = "SELECT COUNT(*) AS total
							 FROM tbl_sim_boisim
							 WHERE " . $sqlWhere;

		$db_count	= new db_query_sphinx($query_count);

		if($row_count = mysql_fetch_assoc($db_count->result)){
			$arrData["total_record"] = $row_count["total"];
		}
		unset($db_count);

		// Lấy data
		$query		= "SELECT *
							FROM tbl_sim_boisim
							WHERE " . $sqlWhere . "
							" . $sqlOrderBy . "
							LIMIT " . (($opts["page"] - 1) * $opts["page_size"]) . ", " . $opts["page_size"] ."
							OPTION max_matches = " . ($opts["page"] * $opts["page_size"] + 1);
							// var_dump($query);
		$db_data	= new db_query_sphinx($query);
		while($row = mysql_fetch_assoc($db_data->result)){
			$row["sim_sim2"]   = $row["id"];
			$arrData["data"][] = $row;
		}
		unset($db_data);

		// Nếu không có kq thì lấy TH2
		if(empty($arrData["data"])){
			
			$query_count = "SELECT COUNT(*) AS total
								 FROM tbl_sim_boisim
								 WHERE " . $sqlWhereKey;

			$db_count	= new db_query_sphinx($query_count);

			if($row_count = mysql_fetch_assoc($db_count->result)){
				$arrData["total_record"] = $row_count["total"];
			}
			unset($db_count);

			// Lấy data
			$query2		= "SELECT *
								FROM tbl_sim_boisim
								WHERE " . $sqlWhereKey . "
								" . $sqlOrderBy . "
								LIMIT " . (($opts["page"] - 1) * $opts["page_size"]) . ", " . $opts["page_size"] ."
								OPTION max_matches = " . ($opts["page"] * $opts["page_size"] + 1);
								// var_dump($query2);
			$db_data_2	= new db_query_sphinx($query2);
			while($row = mysql_fetch_assoc($db_data_2->result)){
				$row["sim_sim2"]   = $row["id"];
				$arrData["data"][] = $row;
			}
			unset($db_data2);
		}

		return $arrData;
	}
}

?>