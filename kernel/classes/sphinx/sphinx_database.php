<?
// Khai báo các hằng số sử dụng trong class
define("SPHINX_DATABASE_ACTIVE", true);

class db_init_sphinx{

	var $server			= "127.0.0.1";
	var $port			= "9306";
	var $username		= "";
	var $password		= "";
	var $database		= "";
	
	function __construct($server_key=0){
		
		// Cấu hình sphinx database tại localhost
		$this->server 	= "127.0.0.1";
		
	}
	
}

class db_query_sphinx{
	
	var $link	= false;
	var $result	= false;
	var $error	= true;
	
	function __construct($query, $server_key=0){
		
		$query	= trim($query);
		if(!SPHINX_DATABASE_ACTIVE || $query == "") return false;
		
		$log		= "";

		global $query_analyze_list;
		global $query_analyze_array;

		if (!isset($query_analyze_list)) $query_analyze_list = "";
		if (!isset($query_analyze_array)) $query_analyze_array = array();

		//echo $query . "<br>";
		$time_start = microtime_float();

		//hàm này để bắt log xem từ file nào dòng nào gọi đến xử lý 
		$arrTemp = @debug_backtrace();
		
		//lấy ra file gọi class xử lý
		$file_call = isset($arrTemp[0]["file"]) ? $arrTemp[0]["file"] : '';
		//lấy ra dòng gọi class
		$file_line = isset($arrTemp[0]["line"]) ? $arrTemp[0]["line"] : '';
		
		$db_init	= new db_init_sphinx($server_key);
		// var_dump($db_init);
		if($this->link = @mysql_connect($db_init->server . ":" . $db_init->port, $db_init->username, $db_init->password)){
			@mysql_select_db($db_init->database, $this->link);
			@mysql_query("SET NAMES 'utf8'", $this->link);
			$this->result	= @mysql_query($query, $this->link);
			if(!$this->result) $log	.= "Error in query string";
			else $this->error	= false;
		}
		else $log	.= "Cannot connect sphinx database";
		// var_dump($log);
		// exit();
		// Nếu có log lỗi thì saveErrorLog
		if($log != ""){
			saveErrorLog($log . " (" . $db_init->server . ":" . $db_init->port . "): " . @mysql_error($this->link) . "\n" . @$_SERVER["SERVER_NAME"] . @$_SERVER["REQUEST_URI"] . "\n" . $query, "SPHINX_LOG");
			@mysql_close($this->link);
		}
		
		$time_end = microtime_float();
		$time = $time_end - $time_start;

		foreach($arrTemp as $key => $val){
			$arrTemp[0]["listfile"][] = $val["file"] . " (Line: " . $val["line"] . ")";
		}
		if(isset($arrTemp[0]) && is_array($arrTemp[0])){
			$arrTemp[0]["time"] 		= $time;
			$arrTemp[0]["query"] 	= $query;
			$arrQueryInfo				= $arrTemp[0];
			if(isset($arrQueryInfo["object"])) unset($arrQueryInfo["object"]);
			// Dump thêm server gây chậm để có thể xử lý
			$arrQueryInfo["host"]	=	$db_init->server;
			$arrQueryInfo["type"]	=	"SPHINX";
	
			$query_analyze_array[] = $arrQueryInfo;
			unset($arrQueryInfo);
			
		}
		unset($arrTemp);

	}
	
	function getInfoQuery(){
		
		if(!$this->link) return false;
		
		$metainfo	= @mysql_query("SHOW META", $this->link);
		$srchmeta	= array();
		while($meta = @mysql_fetch_assoc($metainfo)) $srchmeta[$meta["Variable_name"]]	= $meta["Value"];
		
		return $srchmeta;
		
	}
	
	function close(){
		
		@mysql_free_result($this->result);
		if($this->link) @mysql_close($this->link);
		
	}
	
}

class db_query_sphinx_mysqli{
	
	var $link	= false;
	var $db_init= false;
	var $mysqli	= false;
	var $result	= false;
	var $query	= "";
	var $error	= true;
	var $i		= 0;
	
	function __construct($query, $server_key=0){
		
		$query	= trim($query);
		if(!SPHINX_DATABASE_ACTIVE || $query == "") return false;
		
		global $query_analyze_list;
		global $query_analyze_array;

		if (!isset($query_analyze_list)) $query_analyze_list = "";
		if (!isset($query_analyze_array)) $query_analyze_array = array();

		//echo $query . "<br>";
		$time_start = microtime_float();

		//hàm này để bắt log xem từ file nào dòng nào gọi đến xử lý 
		$arrTemp = @debug_backtrace();

		$db_init			= new db_init_sphinx($server_key);
		$this->db_init	= $db_init;
		$this->mysqli	= @new mysqli($db_init->server, $db_init->username, $db_init->password, $db_init->database, $db_init->port);
		if($this->mysqli->connect_errno){
			saveErrorLog("Cannot connect sphinx database (" . $db_init->server . ":" . $db_init->port . "): " . $this->mysqli->connect_error . "\n" . @$_SERVER["SERVER_NAME"] . @$_SERVER["REQUEST_URI"] . "\n" . $this->query, "SPHINX_LOG");
			return false;
		}
		
		$this->i++;
		$this->query	= $query;
		$this->mysqli->query("SET NAMES 'utf8'");
		if($this->mysqli->multi_query($this->query)){
			$this->result	= $this->mysqli->store_result();
			$this->error	= false;
		}
		else{
			saveErrorLog("Error in query string result " . $this->i . " (" . $db_init->server . ":" . $db_init->port . "): " . $this->mysqli->error . "\n" . @$_SERVER["SERVER_NAME"] . @$_SERVER["REQUEST_URI"] . "\n" . $this->query, "SPHINX_LOG");
			@$this->mysqli->close();
		}

		$time_end = microtime_float();
		$time = $time_end - $time_start;

		foreach($arrTemp as $key => $val){
			$arrTemp[0]["listfile"][] = $val["file"] . " (Line: " . $val["line"] . ")";
		}
		if(isset($arrTemp[0]) && is_array($arrTemp[0])){
			$arrTemp[0]["time"] 		= $time;
			$arrTemp[0]["query"] 	= $query;
			$arrQueryInfo				= $arrTemp[0];
			if(isset($arrQueryInfo["object"])) unset($arrQueryInfo["object"]);
			// Dump thêm server gây chậm để có thể xử lý
			$arrQueryInfo["host"]	=	$db_init->server;
			$arrQueryInfo["type"]	=	"SPHINX";
	
			$query_analyze_array[] = $arrQueryInfo;
			unset($arrQueryInfo);
			
		}
		unset($arrTemp);
		
	}
	
	function more_results(){
		
		return ($this->mysqli->more_results() ? true : false);
		
	}
	
	function next_result(){
		
		$this->i++;
		if($this->mysqli->next_result()) $this->result = $this->mysqli->store_result();
		else saveErrorLog("Error in query string result " . $this->i . " (" . $this->db_init->server . ":" . $this->db_init->port . ")\n" . @$_SERVER["SERVER_NAME"] . @$_SERVER["REQUEST_URI"] . "\n" . $this->query, "SPHINX_LOG");
		
	}
	
	function close(){
		
		@mysqli_free_result($this->result);
		@$this->mysqli->close();
		
	}
	
}

class db_execute_sphinx{
	
	var $link	= false;
	var $result	= false;
	var $error	= true;

	function __construct($query, $server_key=0){
		
		$query	= trim($query);
		if(!SPHINX_DATABASE_ACTIVE || $query == "") return false;
		
		$log		= "";
		$db_init	= new db_init_sphinx($server_key);
		if($this->link = @mysql_connect($db_init->server . ":" . $db_init->port, $db_init->username, $db_init->password)){
			@mysql_select_db($db_init->database, $this->link);
			@mysql_query("SET NAMES 'utf8'", $this->link);
			$this->result	= @mysql_query($query, $this->link);
			if(!$this->result) $log	.= "Error in query string";
			else $this->error	= false;
		}
		else $log	.= "Cannot connect sphinx database";
		// Nếu có log lỗi thì saveErrorLog
		if($log != "") saveErrorLog($log . " (" . $db_init->server . ":" . $db_init->port . "): " . @mysql_error($this->link) . "\n" . @$_SERVER["SERVER_NAME"] . @$_SERVER["REQUEST_URI"] . "\n" . $query, "SPHINX_LOG");
		@mysql_close($this->link);
		unset($db_init);
		
	}
	
}
?>