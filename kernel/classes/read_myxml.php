<?
//code by dinhtoan1905@yahoo.co.uk
//reader xml and rss
class rssRead{

	var $arrayItem	= array();				// Mảng chứa các Record
	var $recordimg;							// Anh chính của tin
	var $recordimgname;						// Tên ảnh chính
	var $recordid;								// ID của tin
	var $category;								// Category ID của tin
	var $title;									// Tiêu đề
	var $pubDate;								// Ngày đăng
	var $teaser;								// Tóm tắt
	var $description;							// Mô tả chi tiết
	var $css;									// File css
	var $source_url;							// Link nguồn
	var $image = array();					// Ảnh trong chi tiết (đã đc base64_encode)
	var $imagename = array();				// Tên ảnh trong chi tiết
	var $pathSaveImage = "";				// Đường dẫn lưu ảnh trong file type.php
	var $pathSaveImageDescription = "";	// Đường dẫn lưu ảnh trong chi tiết tin
	var $php_version = 5;
	
	// Đọc file RSS và gán giá trị vào mảng, đồng thời kiểm tra phiên bản của PHP
	function rssRead($filename){
		
		if(function_exists("simplexml_load_file")){
			$feed	= @simplexml_load_file($filename);
			$this->php_version = 5;
		}
		else{
			$sx	= new simplexml;
			$feed	= $sx->xml_load_file($filename);
			$this->php_version = 4;
		}
		
	   //print_r($feed);
		if(isset($feed->channel->item)){
			$this->arrayItem 		= $feed->channel->item;
			$this->title 			= $this->str_decode($this->arrayItem->title);
			$this->recordimg 		= @$this->arrayItem->recordimg;
			$this->recordimgname 	= @$this->arrayItem->recordimgname;
			$this->recordid 		= $this->arrayItem->recordid;
			$this->category 		= $this->arrayItem->category;
			$this->pubDate 			= $this->arrayItem->pubDate;
			$this->teaser 			= $this->arrayItem->teaser;
			$this->description 		= $this->arrayItem->description;
			$this->css 				= $this->arrayItem->css;
			$this->source_url 		= $this->arrayItem->source_url;
			$this->image 			= $this->arrayItem->image;
			$this->imagename 		= $this->arrayItem->imagename;
		}
		else{
			return;
		}
		
	}
	
	/*
	Function lưu ảnh vào thư mục theo đúng name đã generate
	*/
	function saveImage(){
		
		// Lưu ảnh chính của tin
		if($this->recordimg != "" && $this->recordimgname != ""){
			$this->create_file($this->pathSaveImage . "small_" . $this->recordimgname, $this->str_decode($this->recordimg));
		}
		
		// Kiểm tra nếu trong chi tiết có ảnh thì mới save
		if(isset($this->image[0])){
			
			// Kiểm tra nếu php sử dụng phiên bản 4 thì check is_array trước rồi mới loop
			if($this->php_version < 5){
				
				// Kiểm tra xem ảnh có nhiều hay chỉ có 1 ảnh để lưu
				if(is_array($this->imagename)){
					for($i=0; $i<count($this->image); $i++){
						if(isset($this->imagename[$i])){
							$this->create_file($this->pathSaveImageDescription . $this->imagename[$i], $this->str_decode($this->image[$i]));
						}
					}
				}
				else{
					$this->create_file($this->pathSaveImageDescription . $this->imagename, $this->str_decode($this->image));
				}
				
			}
			// Ngược lại nếu PHP là bản >= 5 thì loop luôn
			else{
				for($i=0; $i<count($this->image); $i++){
					if(isset($this->imagename[$i])){
						$this->create_file($this->pathSaveImageDescription . $this->imagename[$i], $this->str_decode($this->image[$i]));
					}
				}
			}
			
		}// End if(isset($this->image[0]))
		
	}
	
	/*
	Function giải mã hóa
	*/
	function str_decode($encodedStr="", $length=0){
	   $returnStr = "";
		if(!empty($encodedStr)) {
			 $dec = str_rot13($encodedStr);
			 $dec = base64_decode($dec);
			$returnStr = $dec;
		}
		return $returnStr;
	
	}
	
	/*
	Function lưu file
	*/
	function create_file($filename,$contents){
		
		if(is_file($filename)){
			$fd = fopen($filename, "r"); 
			if($fd){
				fclose($fd); 
				$contents++; 
				$fp = fopen ($filename, "w"); 
				if($fp){
					fwrite($fp,$contents); 
					fclose($fp); 
				}
				return $contents;
			}
		}
		else{
			$fp = fopen ($filename, "w"); 
			if($fp){
				fwrite ($fp,$contents); 
				fclose ($fp); 
			}
			return $contents;
		}
		return create_file($filename,$contents);
		
	}
	
	/*
	Function generate file name
	*/
	function generate_name($filename){
		
		$name = "";
		for($i=0; $i<3; $i++){
			$name .= chr(rand(97, 122));
		}
		$today= getdate();
		$name.= $today[0];
		$ext	= substr($filename, (strrpos($filename, ".") + 1));
		return $name . "." . $ext;
		
	}
	
}
?>