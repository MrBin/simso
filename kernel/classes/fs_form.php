<?
//Code by: Mr. Tran - Yahoo: txnc2002, boy_infotech
class form{
	var $class_table					= 'form_table';		// Style của table
	var $class_form						= 'form_main';			// Style của form
	var $class_form_button				= 'form_button';	// Style của button control
	var $class_form_name				= 'form_name';		// Style của tên control
	var $class_form_control				= 'form_control';	// Style của control
	var $class_form_errorMsg			= 'form_errorMsg';	// Style của chuỗi text thông báo submit error
	var $class_form_text				= 'form_text';		// Style của chuỗi text bên cạnh control
	var $class_form_text_note			= 'form_text_note';	// Style của chuỗi text thông báo yêu cầu nhập dữ liệu
	var $class_form_require				= 'form_asterisk';	// Style của chuỗi text biểu thị dữ liệu bắt buộc nhập
	var $ec								= '{-----}';		// Explode character
	var $tr_html						= '';
	
	// Create <table>
	function create_table($cellpadding='3', $cellspacing='3', $add_html=''){
		return $create_table = '<table class="' . $this->class_table . '" cellpadding="' . $cellpadding . '" cellspacing="' . $cellspacing . '" ' . $add_html . '>';
	}
	
	// Close </table>
	function close_table($close_table='</table>'){
		return $close_table;
	}
	
	// Create <form>
	function create_form($name, $action, $method='get', $enctype='multipart/form-data', $add_html=''){
		return $create_form = '<form class="' . $this->class_form . '" name="' . $name . '" action="' . $action . '" method="' . $method . '" enctype="' . $enctype . '" ' . $add_html . '>';
	}
	
	// Close </form>
	function close_form($close_form='</form>'){
		return $close_form;
	}
	
	// Check data require
	function data_require($require, $title){
		if($require != 0){
			$control_name = '<font class="' . $this->class_form_require . '">* </font>' . $title;
		}
		else $control_name = $title;
		return $control_name;
	}
	
	// Generate control in table
	function create_control($name, $control){
		if($name != "") $name = $name . ' :';
		else $name = "&nbsp;";
		return '<tr' . $this->tr_html . '><td nowrap="nowrap" class="' . $this->class_form_name . '">' . $name . '</td><td class="' . $this->class_form_text . '">' . $control . '</td></tr>';
	}
	
	// Add file javascript
	function add_javascript($filepath, $java_code_add_on=''){
		if($filepath != '') $src = 'src="' . $filepath . '"';
		else $src = '';
		return '<script type="text/javascript" ' . $src . '>' . $java_code_add_on . '</script>';
	}

	// Show error
	function errorMsg($errorMsg){
		if($errorMsg != ""){
			$control = '<font class="' . $this->class_form_errorMsg . '">' . $errorMsg . '</font>';
			return $this->create_control('', $control);
		}
	}
	
	//Replace " -> &quot; (chống phá ngoặc)
	function replaceQ($string){
		$string = str_replace('\"', '"', $string);
		$string = str_replace("\'", "'", $string);
		$string = str_replace("\&quot;", "&quot;", $string);
		$string = str_replace("\\\\", "\\", $string);
		return str_replace('"', "&quot;", $string);
	}

	function text_note($text_note='Những ô có dấu sao (<font class="form_asterisk">*</font>) là bắt buộc phải nhập.'){
		if($text_note != ""){
			$control = '<font class="form_text_note">' . $text_note . '</font>';
			return $this->create_control('', $control);
		}
	}

	function button($typeControl, $id, $name, $value, $add_html='', $add_text=''){
		$arrTypeControl		= explode($this->ec, $typeControl);
		$arrID				= explode($this->ec, $id);
		$arrName			= explode($this->ec, $name);
		$arrValue			= explode($this->ec, $value);
		$control				= '';
		for($i=0; $i<count($arrID); $i++){
			if($arrTypeControl[$i] == 'button') $check_javascript = 'onClick="validateForm()"';
			else $check_javascript = '';
			$control .= '<input class="' . $this->class_form_button . '" type="' . $arrTypeControl[$i] . '" id="' . $arrID[$i] . '" name="' . $arrName[$i] . '" value="' . tt($arrValue[$i]) . '"' . $add_html . ' title="' . tt($arrValue[$i]) . '"  ' . $check_javascript . '/>';
			if($i < count($arrID)-1) $control .= '&nbsp;';
		}
		$control .= $add_text;		
		return $this->create_control('', $control);
	}

	function checkbox($require='0' ,$titleControl, $id, $name, $value, $currentValue, $add_html='', $add_text=''){
		$arrID				= explode($this->ec, $id);
		$arrName			= explode($this->ec, $name);
		$arrValue			= explode($this->ec, $value);
		$arrCurrentValue	= explode($this->ec, $currentValue);
		//$arrTitle			= explode($this->ec, $title);
		$arrAdd_html		= explode($this->ec, $add_html);
		$control				= '';
		for($i=0; $i<count($arrID); $i++){
			$checked = '';
			if($arrValue[$i] == $arrCurrentValue[$i]) $checked = ' checked="checked"';
			$control .= '<input type="checkbox" id="' . $arrID[$i] . '" name="' . $arrName[$i] . '" value="' . tt($arrValue[$i]) . '"' . $checked . '/>' . $arrAdd_html[$i];
			//if($arrTitle[$i] != '') $control .= '<label for="' . $arrID[$i] . '">' . $arrTitle[$i] . '</label>';
			if($i < count($arrID)-1) $control .= ' &nbsp; ';
		}
		$control .= $add_text;
		return $this->create_control($this->data_require($require, tt($titleControl)), $control);
	}

	function radio($require='0' ,$titleControl, $id, $name, $value, $currentValue, $add_html='', $add_text=''){
		$arrID			= explode($this->ec, $id);
		$arrValue		= explode($this->ec, $value);
		//$arrTitle		= explode($this->ec, $title);
		$arrAdd_html	= explode($this->ec, $add_html);
		$control			= '';
		for($i=0; $i<count($arrID); $i++){
			$checked = '';
			if($arrValue[$i] == $currentValue) $checked = ' checked="checked"';
			$control .= '<input type="radio" id="' . $arrID[$i] . '" name="' . $name . '" value="' . $arrValue[$i] . '"' . $checked . '/>' .  tt($arrAdd_html[$i]);
			//if($arrTitle[$i] != '') $control .= '<label for="' . $arrID[$i] . '">' . $arrTitle[$i] . '</label>';
			if($i < count($arrID)-1) $control .= ' &nbsp; ';
		}
		$control .= $add_text;
		return $this->create_control($this->data_require($require, tt($titleControl)), $control);
	}

	function getFile($require='0' ,$titleControl, $id, $name, $size='30', $add_html='', $add_text=''){
		$control = '<input class="' . $this->class_form_control . '" type="file" id="' . $id . '" name="' . $name . '" title="' . tt($titleControl) . '" size="' . $size . '" ' . $add_html . '/>';
		$control .= $add_text;
		return $this->create_control($this->data_require($require, tt($titleControl)), $control);
	}
	
	function text($require='0' ,$titleControl, $id, $name, $value, $title='', $width='', $height='', $maxlength='', $breakControl=' - ', $add_html='', $add_text=''){
		$arrID			= explode($this->ec, $id);
		$arrName			= explode($this->ec, $name);
		$arrValue		= explode($this->ec, $value);
		$arrTitle		= explode($this->ec, $title);
		$arrWidth		= explode($this->ec, $width);
		$arrHeight		= explode($this->ec, $height);
		$arrMaxlength	= explode($this->ec, $maxlength);
		$arrAdd_html	= explode($this->ec, $add_html);
		$control			= '';
		for($i=0; $i<count($arrID); $i++){
			if($breakControl == "title"){
				$control .= $arrTitle[$i] . '&nbsp;';
				$control .= '<input class="' . $this->class_form_control . '" type="text" title="' . tt($arrTitle[$i]) . '" id="' . $arrID[$i] . '" name="' . $arrName[$i] . '" value="' . $this->replaceQ($arrValue[$i]) . '" style="width:' . $arrWidth[$i] . 'px; height:' . $arrHeight[$i] . 'px" maxlength="' . $arrMaxlength[$i].'" '. $arrAdd_html[$i] . ' "/>';
				$control .= " &nbsp; ";
			}
			else{
				$control .= '<input class="' . $this->class_form_control . '" type="text" title="' . tt($arrTitle[$i]) . '" id="' . $arrID[$i] . '" name="' . $arrName[$i] . '" value="' . $this->replaceQ($arrValue[$i]) . '" style="width:' . $arrWidth[$i] . 'px; height:' . $arrHeight[$i] . 'px" maxlength="' . $arrMaxlength[$i].'" '. $arrAdd_html[$i] . ' "/>';
				if($i < count($arrID)-1) $control .= $breakControl;
			}
		}
		$control .= $add_text;
		return $this->create_control($this->data_require($require, tt($titleControl)), $control);
	}

	function password($require='0' ,$titleControl, $id, $name, $value, $width='', $height='', $maxlength='', $add_html='', $add_text=''){
		$control = '<input class="' . $this->class_form_control . '" type="password" id="' . $id . '" name="' . $name . '" value="' . $value . '" title="' . tt($titleControl) . '" style="width:' . $width . 'px; height:' . $height . 'px" maxlength="' . $maxlength . '" ' . $add_html . '/>';
		$control .= $add_text;
		return $this->create_control($this->data_require($require, tt($titleControl)), $control);
	}

	function textarea($require='0' ,$titleControl, $id, $name, $value, $width='', $height='', $add_html='', $add_text='', $wysiwyg=''){
		$control = $wysiwyg;
		$control .= '<textarea class="' . $this->class_form_control . '" id="' . $id . '" name="' . $name . '" title="' . tt($titleControl) . '" style="width:' . $width . 'px; height:' . $height . 'px" ' . $add_html . '>' . $this->replaceQ($value) . '</textarea>';
		$control .= $add_text;
		return $this->create_control($this->data_require($require, tt($titleControl)), $control);
	}

	function wysiwygarea($titleControl, $id, $value, $basePath='../../resource/wysiwyg_editor/', $width='100%', $height='450'){
		require_once($basePath . "fckeditor.php");
		$oFCKeditor				= new FCKeditor($id);
		$oFCKeditor->Value		= $value;
		$oFCKeditor->BasePath	= $basePath;
		$oFCKeditor->Width		= $width;
		$oFCKeditor->Height		= $height;
		echo '<div class="' . $this->class_form_name . '" style="text-align:left; padding:5px">' . $titleControl . '</div>';
		$oFCKeditor->Create();
	}
	
	function wysiwyg($titleControl, $id, $value, $basePath='../../resource/ckeditor/', $width='100%', $height='450'){
		//*/
		require_once($basePath . "ckfinder/ckfinder.php");
		$ckfinder = new CKFinder();
		$ckfinder->BasePath = $basePath . 'ckfinder/'; // Note: BasePath property in CKFinder class starts with capital letter
		
		require_once($basePath . "ckeditor.php");
		$CKEditor 						= new CKEditor();
		$CKEditor->returnOutput 		= true;
		$CKEditor->basePath				= $basePath;
		$CKEditor->config['width'] 		= $width;
		$CKEditor->config['height'] 	= $height;
		$CKEditor->config['filebrowserBrowseUrl'] 		= $basePath . 'ckfinder/ckfinder.html';
		//$CKEditor->config['filebrowserImageBrowseUrl'] 	= $basePath . 'ckfinder/ckfinder.html?type=Images';
		//$CKEditor->config['filebrowserFlashBrowseUrl'] 	= $basePath . 'ckfinder/ckfinder.html?type=Flash';
		
		$CKEditor->config['filebrowserUploadUrl'] 		= $basePath . 'ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files';
		$CKEditor->config['filebrowserImageUploadUrl']	= $basePath . 'ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images';
		$CKEditor->config['filebrowserFlashUploadUrl'] 	= $basePath . 'ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash';
		
		$CKEditor->config['sharedSpaces'] = array('top'=>'topSpace');
		
		#+
		$str = '';
		$str .= '<div style="padding:0 0 5px 0">' . $titleControl . '</div>';	
		$str .= $CKEditor->editor($id, $value);
		
		#+
		return $str;
		

		
		//$ckfinder->Create() ;
		//*/
		
		/*/
		require_once($basePath . "fckeditor.php");
		$oFCKeditor				= new FCKeditor($id);
		$oFCKeditor->Value		= $value;
		$oFCKeditor->BasePath	= $basePath;
		$oFCKeditor->Width		= $width;
		$oFCKeditor->Height		= $height;
		echo '<div class="' . $this->class_form_name . '" style="text-align:left; padding:5px">' . $titleControl . '</div>';
		$oFCKeditor->Create();
		//*/
	}

	function hidden($id, $name, $value, $add_html=''){
		$control = '<input type="hidden" id="' . $id . '" name="' . $name . '" value="' . $this->replaceQ($value) . '" ' . $add_html . '/>';
		return $control;
	}

	function select($require='0' ,$titleControl, $id, $name, $array_option, $currentValue, $width='', $size='', $multiple='0', $add_html='', $add_text=''){
		$select_multi = '';
		if($multiple != 0) $select_multi = 'multiple="multiple"';
		$control 	= '<select class="' . $this->class_form_control . '" id="' . $id . '" name="' . $name . '" title="' . tt($titleControl) . '" style="width:' . $width . 'px" size="' . $size . '" ' . $select_multi . ' ' . $add_html . '>';
		$control   .= '<option value="">--[' . tt($titleControl) . ']--</option>';
		if(is_array($array_option)){
			foreach($array_option as $arrKey => $arrValue){
				$selected = '';
				if($multiple != 0){
					if(strpos($currentValue, "[" . $arrKey . "]") !== false) $selected = ' selected="selected"';
				}
				else{
					if($arrKey == $currentValue) $selected = ' selected="selected"';
				}
				$control .= '<option title="' . $this->replaceQ($arrValue) . '" value="' . $arrKey . '"' . $selected . '>' . $arrValue . '</option>';
			}
		}//End if(is_array($array_option))
		$control .= '</select>';
		$control .= $add_text;
		return $this->create_control($this->data_require($require, tt($titleControl)), $control);
	}

	function select_db($require='0' ,$titleControl, $id, $name, $db_query, $value_field, $name_field, $currentValue, $title='', $require='0', $width='', $size='', $multiple='0', $add_html='', $add_text=''){
		$select_multi = '';
		if($multiple != 0) $select_multi = 'multiple="multiple"';
		$control = '<select class="' . $this->class_form_control . '" title="' . tt($title) . '" id="' . $id . '" name="' . $name . '" style="width:' . $width . 'px" size="' . $size . '" ' . $select_multi . ' ' . $add_html . '>';
		if($title != "" && $multiple == 0) $control.= '<option value="">-- ' . tt($title) . ' --</option>';
		while($row = mysql_fetch_array($db_query->result)){
			$selected = '';
			if($multiple != 0){
				if(strpos($currentValue, "[" . $row[$value_field] . "]") !== false) $selected = ' selected="selected"';
			}
			else{
				if($currentValue == $row[$value_field]) $selected = ' selected="selected"';
			}
			$control .= '<option title="' . $this->replaceQ($row[$name_field]) . '" value="' . $row[$value_field] . '"' . $selected . '>' . $row[$name_field] . '</option>';
		}
		$control .= '</select>';
		$control .= $add_text;
		return $this->create_control($this->data_require($require, tt($titleControl)), $control);
	}

	function select_db_2_level($require='0' ,$titleControl, $id, $name, $db_query, $value_field_lv1, $name_field_lv1, $value_field_lv2, $name_field_lv2, $currentValue, $title='', $width='', $size='', $multiple='0', $add_html='', $add_text=''){
		$select_multi = '';
		if($multiple != 0) $select_multi = 'multiple="multiple"';
		$control = '<select class="' . $this->class_form_control . '" title="' . tt($title) . '" id="' . $id . '" name="' . $name . '" style="width:' . $width . 'px" size="' . $size . '" ' . $select_multi . ' ' . $add_html . '>';
		if($title != "" && $multiple == 0) $control.= '<option value="">--[ ' . tt($title) . ' ]--</option>';
		$root_id	= 0;
		while($row = mysql_fetch_array($db_query->result)){
			if($root_id != $row[$value_field_lv1]){
				$root_id = $row[$value_field_lv1];
				$control .= '<optgroup label="' . $this->replaceQ($row["root_name"]) . '"></optgroup>';
			}
			$selected = '';
			if($multiple != 0){
				if(strpos($currentValue, "[" . $row[$value_field_lv2] . "]") !== false) $selected = ' selected="selected"';
			}
			else{
				if($currentValue == $row[$value_field_lv2]) $selected = ' selected="selected"';
			}
			$control .= '<option title="' . $this->replaceQ($row[$name_field_lv2]) . '" value="' . $row[$value_field_lv2] . '"' . $selected . '>&nbsp; |-- ' . $row[$name_field_lv2] . '</option>';

		}
		$control .= '</select>';
		$control .= $add_text;
		return $this->create_control($this->data_require($require, tt($titleControl)), $control);
	}

	function select_db_multi($require='0' ,$titleControl, $id, $name, $array_multi, $value_field, $name_field, $currentValue, $title='', $width='', $size='', $multiple='0', $add_html='', $add_text=''){
		$select_multi = '';
		if($multiple != 0) $select_multi = 'multiple="multiple"';
		$control = '<select class="' . $this->class_form_control . '" title="' . tt($title) . '" id="' . $id . '" name="' . $name . '" style="width:' . $width . 'px" size="' . $size . '" ' . $select_multi . ' ' . $add_html . '>';
		if($title != "" && $multiple == 0) $control.= '<option value="">--[ ' . tt($title) . ' ]--</option>';
		for($i=0; $i<count($array_multi); $i++){
			$selected = '';
			if($multiple != 0){
				if(strpos($currentValue, "[" . $array_multi[$i][$value_field] . "]") !== false) $selected = ' selected="selected"';
			}
			else{
				if($currentValue == $array_multi[$i][$value_field]) $selected = ' selected="selected"';
			}
			$control .= '<option title="' . $this->replaceQ($array_multi[$i][$name_field]) . '" value="' . $array_multi[$i][$value_field] . '"' . $selected . '>';
			for($j=0; $j<$array_multi[$i]['level']; $j++) $control .= '---';
			$control .= '+' . $array_multi[$i][$name_field] . '</option>';
		}
		$control .= '</select>';
		$control .= $add_text;
		return $this->create_control($this->data_require($require, tt($titleControl)), $control);
	}
}
?>