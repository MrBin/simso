<?
// Khai báo các hằng số sử dụng trong class
define("RATING_TYPE_PRODUCT", 1);

class rating{

	var $table				= "rating";
	var $table_data		= "rating_data";
	var $table_reply		= "rating_reply";
	var $array_replace	= array ("{#CON_SITE_NAME#}",
											"{#CON_ADDRESS#}",
											"{#CON_MOBILE#}",
											"{#CART_BUTTON#}",
											);
	var $array_admin		= array ();
	var $type				= 0;
	var $last_rating_id	= 0;
	var $last_reply_id	= 0;
	var $ip2long			= 0;
	var $time				= 0;
	var $error				= false;

	/**
	Function khởi tạo
	*/
	function __construct($type){

		$this->type		= $type;
		$this->ip2long	= ip2long(getUserIpAddr()) == false ? 0 : ip2long(getUserIpAddr());
		$this->time		= time();
		$db_admin		= new db_query("SELECT adm_id, adm_name FROM tbl_admin");
		$this->array_admin	= convert_result_set_2_array($db_admin->result, "adm_id");
		$db_admin->close();
		unset($db_admin);
		
	}

	/**
	Function add rating
	*/
	function add_rating($option=array()){

		$opts	= array ("admin_id"		=> 0,
							"record_id"		=> 0,
							"category_id"	=> 0,
							"order_id"		=> 0,
							"rating"			=> 0,
							"name"			=> "",
							"mobile"			=> "",
							"content"		=> "",
							"date"			=> $this->time,
							"status"			=> 0,
							);
		// Gán lại option
		if(is_array($option) && count($option) > 0) $opts	= $option + $opts;
		
		if($opts["rating"] < 1) $opts["rating"]	= 1;
		if($opts["rating"] > 5) $opts["rating"]	= 5;
		if($opts["admin_id"] > 0) $opts["status"]	= 1;
		else{
			$opts["name"]			= htmlspecialbo($opts["name"]);
			$opts["mobile"]		= htmlspecialbo($opts["mobile"]);
			$opts["content"]		= htmlspecialbo($opts["content"]);
		}

		// Insert data
		$db_execute					= new db_execute_return();
		$this->last_rating_id	= $db_execute->db_execute("INSERT INTO " . $this->table . " (rat_type, rat_admin_id, rat_record_id, rat_category_id, rat_order_id, rat_rating,
																														 rat_name, rat_mobile, rat_content, rat_count_reply, rat_like, rat_ip, rat_date, rat_last_update, rat_status)
																			VALUES  (" . $this->type . ", " . $opts["admin_id"] . ", " . $opts["record_id"] . ", " . $opts["category_id"] . ", " . $opts["order_id"] . ", " . $opts["rating"] . ",
																						'" . replaceMQ($opts["name"]) . "', '" . replaceMQ($opts["mobile"]) . "', '" . replaceMQ($opts["content"]) . "', 0, 0, " . $this->ip2long . ", " . $opts["date"] . ", " . $this->time . ", " . $opts["status"] . ")");
		unset($db_execute);

		// Đếm lại để lưu vào bảng data
		$this->recount_rating_data($opts["record_id"], $opts["category_id"]);

	}

	/**
	Function add rating reply
	*/
	function add_rating_reply($rating_id, $option=array()){

		$opts		= array ("admin_id"	=> 0,
								"name"		=> "",
								"mobile"		=> "",
								"content"	=> "",
								"date"		=> $this->time,
								"status"		=> 0,
								);
		// Gán lại option
		if(is_array($option) && count($option) > 0) $opts	= $option + $opts;

		if($opts["admin_id"] > 0) $opts["status"]	= 1;
		else{
			$opts["name"]		= htmlspecialbo($opts["name"]);
			$opts["mobile"]	= htmlspecialbo($opts["mobile"]);
			$opts["content"]	= htmlspecialbo($opts["content"]);
		}

		// Check xem rating có tồn tại hay ko
		$db_rating				= new db_query("SELECT rat_id, rat_record_id, rat_category_id, rat_status FROM " . $this->table . " WHERE rat_type = " . $this->type . " AND rat_id = " . $rating_id);
		if(mysql_num_rows($db_rating->result) == 0){
			$this->error[]		= "Không tìm thấy đánh giá, bạn hãy ấn F5 và thử lại.";
			return false;
		}
		$rowRating	= mysql_fetch_assoc($db_rating->result);
		$db_rating->close();
		unset($db_rating);

		// Insert data
		$db_execute				= new db_execute_return();
		$this->last_reply_id	= $db_execute->db_execute("INSERT INTO " . $this->table_reply . "(rr_type, rr_admin_id, rr_rating_id, rr_record_id, rr_category_id,
																															rr_name, rr_mobile, rr_content, rr_like, rr_ip, rr_date, rr_last_update, rr_status)
																		VALUES  (" . $this->type . ", " . $opts["admin_id"] . ", " . $rating_id . ", " . $rowRating["rat_record_id"] . ", " . $rowRating["rat_category_id"] . ",
																					'" . replaceMQ($opts["name"]) . "', '" . replaceMQ($opts["mobile"]) . "', '" . replaceMQ($opts["content"]) . "', 0, " . $this->ip2long . ", " . $opts["date"] . ", " . $this->time . ", " . $opts["status"] . ")");
		unset($db_execute);

		// Nếu là admin trả lời thì duyệt luôn rating
		if($opts["admin_id"] > 0 && $rowRating["rat_status"] == 0){
			$db_execute	= new db_execute("UPDATE " . $this->table . " SET rat_status = 1 WHERE rat_id = " . $rowRating["rat_id"]);
			unset($db_execute);
		}

		// Đếm lại để lưu vào bảng rating
		$this->recount_rating_reply($rating_id);

		// Đếm lại để lưu vào bảng data
		$this->recount_rating_data($rowRating["rat_record_id"], $rowRating["rat_category_id"]);

	}

	/**
	Function đếm lại số bình luận theo $rating_id
	*/
	function recount_rating_reply($rating_id){

		$db_count	= new db_query("SELECT COUNT(*) AS count FROM " . $this->table_reply . " WHERE rr_type = " . $this->type . " AND rr_rating_id = " . $rating_id);
		$row			= mysql_fetch_assoc($db_count->result);
		$db_count->close();
		unset($db_count);
		
		// Update count
		$db_execute	= new db_execute("UPDATE " . $this->table . " SET rat_count_reply = " . $row["count"] . " WHERE rat_id = " . $rating_id);
		unset($db_execute);

	}

	/**
	Function đếm lại số đánh giá theo $record_id
	*/
	function recount_rating_data($record_id, $category_id){

		// Đánh giá trung bình, tổng số đánh giá
		$db_count	= new db_query("SELECT AVG(rat_rating) AS rating, COUNT(*) AS count
											 FROM " . $this->table . "
											 WHERE rat_type = " . $this->type . " AND rat_record_id = " . $record_id);
		$row			= mysql_fetch_assoc($db_count->result);
		$db_count->close();
		unset($db_count);

		// Tổng số reply
		$db_count	= new db_query("SELECT COUNT(*) AS count
											 FROM " . $this->table_reply . "
											 WHERE rr_type = " . $this->type . " AND rr_record_id = " . $record_id);
		$rowReply	= mysql_fetch_assoc($db_count->result);
		$db_count->close();
		unset($db_count);

		// Kiểm tra nếu chưa có thì INSERT, có rồi thì UPDATE
		$db_check	= new db_query("SELECT rd_record_id FROM " . $this->table_data . " WHERE rd_type = " . $this->type . " AND rd_record_id = " . $record_id);
		if(mysql_num_rows($db_check->result) > 0){
			$queryStr	= "UPDATE " . $this->table_data . "
								SET rd_rating = " . doubleval($row["rating"]) . ", rd_count_rating = " . intval($row["count"]) . ", rd_count_reply = " . intval($rowReply["count"]) . ", rd_last_update = " . $this->time . "
								WHERE rd_type = " . $this->type . " AND rd_record_id = " . $record_id;
		}
		else{
			$queryStr	= "INSERT INTO " . $this->table_data . " (rd_type, rd_record_id, rd_category_id, rd_rating, rd_count_rating, rd_count_reply, rd_last_update)
								VALUES (" . $this->type . ", " . $record_id . ", " . $category_id . ", " . doubleval($row["rating"]) . ", " . intval($row["count"]) . ", " . intval($rowReply["count"]) . ", " . $this->time . ")";
		}
		
		// Update count
		$db_execute	= new db_execute($queryStr);
		unset($db_execute);

	}

	/**
	Function xóa đánh giá
	*/
	function delete_rating($list_id){

		$record_id	= "";
		$arrRecount	= array();
		$db_rating	= new db_query("SELECT rat_id, rat_record_id, rat_category_id FROM " . $this->table . " WHERE rat_type = " . $this->type . " AND rat_id IN (" . convert_list_to_list_id($list_id) . ")");
		while($row = mysql_fetch_assoc($db_rating->result)){
			$arrRecount[$row["rat_record_id"]]	= $row["rat_category_id"];
			if($record_id != "") $record_id .= ",";
			$record_id	.= $row["rat_id"];
		}
		$db_rating->close();
		unset($db_rating);

		// Nếu có dữ liệu thì xóa
		if($record_id != ""){
			// Xóa trong bảng đánh giá
			$db_execute	= new db_execute("DELETE FROM " . $this->table . " WHERE rat_type = " . $this->type . " AND rat_id IN (" . convert_list_to_list_id($record_id) . ")");
			unset($db_execute);
			// Xóa trong bảng bình luận
			$db_execute	= new db_execute("DELETE FROM " . $this->table_reply . " WHERE rr_type = " . $this->type . " AND rr_rating_id IN (" . convert_list_to_list_id($record_id) . ")");
			unset($db_execute);
			// Recount lại rating data
			foreach($arrRecount as $key => $value) $this->recount_rating_data($key, $value);
		}
		
	}

	/**
	Function xóa bình luận
	*/
	function delete_rating_reply($list_id){

		$record_id	= "";
		$arrRecount	= array();
		$db_rating	= new db_query("SELECT rr_id, rr_rating_id, rr_record_id, rr_category_id FROM " . $this->table_reply . " WHERE rr_type = " . $this->type . " AND rr_id IN (" . convert_list_to_list_id($list_id) . ")");

		while($row = mysql_fetch_assoc($db_rating->result)){
			$arrRecount[$row["rr_rating_id"]]	= $row["rr_record_id"];
			if($record_id != "") $record_id .= ",";
			$record_id	.= $row["rr_id"];
		}
		$db_rating->close();
		unset($db_rating);

		// Nếu có dữ liệu thì xóa
		if($record_id != ""){

			// Xóa trong bảng bình luận
			$db_execute	= new db_execute("DELETE FROM " . $this->table_reply . " WHERE rr_type = " . $this->type . " AND rr_id IN (" . convert_list_to_list_id($record_id) . ")");
			unset($db_execute);
			// Recount lại rating data
			foreach($arrRecount as $key => $value) $this->recount_rating_reply($key);
		}

	}

	/**
	Function generate rating
	*/
	function generate_rating($arrData, $option=array()){

		// global $con_admin_id;

		$opts			= array ("ajax" => false);
		// Gán lại option
		if(is_array($option) && count($option) > 0) $opts	= $option + $opts;

		$strReturn	= '';

		foreach($arrData as $row){

			$order		= ($row["rat_order_id"] > 0 ? '<span class="ordered"><i class="icm icm_checkmark-circle"></i></span>' : '');
			// $time			= ($con_admin_id > 0 ? ' (<span class="time">' . date("d/m/Y - H:i", $row["rat_date"]) . '</span>)' : '');
			$time 		= ' (<span class="time">' . date("d/m/Y - H:i", $row["rat_date"]) . '</span>)';
			$linkReply	= '<a href="javascript:;" onclick="generateRatingReplyForm(' . $row["rat_id"] . ', \'' . $row["rat_name"] . '\')">Trả lời</a> &nbsp;•&nbsp; ' . generateDurationShort($this->time - $row["rat_date"]) . ' trước' . $time;
			$strReturn	.= '<li id="rating_' . $row["rat_id"] . '" class="wrapper">
									<div class="user"><abbr title="' . $row["rat_name"] . '">' . $this->abbr_name($row["rat_name"]) . '</abbr>' . $row["rat_name"] . $order . '</div>
									<div class="content">' . $row["rat_content"] . '</div>
									<div class="text">' . $linkReply . '</div>
								</li>';
			
			// Nếu có reply thì hiển thị
			if($row["rat_count_reply"] > 0){

				$db_reply	= new db_query("SELECT rr_id, rr_admin_id, rr_rating_id, rr_name, rr_mobile, rr_content, rr_picture_json, rr_like, rr_date
													 FROM rating_reply
													 WHERE rr_type = " . $this->type . " AND rr_date < " . $this->time . " AND rr_rating_id = " . $row["rat_id"] . "
													 ORDER BY rr_date ASC
													 LIMIT 30");
				$arrReply	= convert_result_set_2_array($db_reply->result, "rr_id");
				$db_reply->close();
				unset($db_reply);

				// Nếu có reply thì hiển thị
				if(count($arrReply) > 0) $strReturn	.= '<li id="reply_list_' . $row["rat_id"] . '" class="reply">' . $this->generate_rating_reply($arrReply, $opts) . '</li>';

			}// End if($row["rat_count_reply"] > 0)

		}// End foreach($arrData as $row)

		return $strReturn;

	}

	/**
	Function generate rating reply
	*/
	function generate_rating_reply($arrData, $option=array()){

		global $con_admin_id;
		global $con_css_path;

		$opts			= array ("ajax" => false);
		// Gán lại option
		if(is_array($option) && count($option) > 0) $opts	= $option + $opts;

		$strReturn	= '';
		foreach($arrData as $row){

			$avatar	= '';
			$name		= $row["rr_name"];
			$admin	= '';
			if($row["rr_admin_id"] > 0 && isset($this->array_admin[$row["rr_admin_id"]])){
				$temp		= $this->array_admin[$row["rr_admin_id"]];
				$avatar	= "";
				$name		= $temp["adm_name"];
				$admin	= '<span class="admin">Quản trị viên</span>';
			}
			$abbr			= '<abbr title="' . $name . '">' . $this->abbr_name($name) . '</abbr>';
			// $time			= ($con_admin_id > 0 ? ' (<span class="time">' . date("d/m/Y - H:i", $row["rr_date"]) . '</span>)' : '');
			$time			= ' (<span class="time">' . date("d/m/Y - H:i", $row["rr_date"]) . '</span>)';
			$linkReply	= '<a href="javascript:;" onclick="generateRatingReplyForm(' . $row["rr_rating_id"] . ', \'' . $name . '\')">Trả lời</a> &nbsp;•&nbsp; ' . generateDurationShort($this->time - $row["rr_date"]) . ' trước' . $time;
			$strReturn	.= '<div id="reply_' . $row["rr_id"] . '" class="wrapper">
									<div class="user">' . $abbr . $name . $admin . '</div>
									<div class="content">' . $row["rr_content"] . '</div>
									<div class="text">' . $linkReply . '</div>
								 </div>';
		}
		return $strReturn;

	}

	/**
	Function short name
	*/
	function abbr_name($name){

		$strReturn	= "";
		$name			= replace_double_space($name);
		if(mb_strlen($name, "UTF-8") <= 2) return $name;
		$arrName		= array_slice(explode(" ", $name), -2, 2);
		foreach($arrName as $value) $strReturn	.= mb_substr($value, 0, 1, "UTF-8");
		return $strReturn;

	}

	/**
	Function replace content
	*/
	function replace_content($content){

		global $con_server_name;
		// global $con_site_name;
		// global $con_address;
		// global $arrConMobile;

		$content	= preg_replace('@(' . $con_server_name . '?(/([\w/_+\.#-]*(\?\S+)?[^\.\s])?)?)@', '<a href="$1" target="_blank">$1</a>', $content);
		// $arrText	= array ('<a href="' . $con_server_name . '/" target="_blank">' . $con_site_name . '</a>',
		// 						$con_address,
		// 						'<a href="tel:' . str_replace(" ", "", $arrConMobile[0]) . '" rel="nofollow">' . $arrConMobile[0] . '</a>',
		// 						'<button class="form_button_3 rating_button" onclick="$(\'#cart_button\').click()"><i class="icm icm_cart-add2"></i>Mua ngay</button>',
		// 						);
		// $content	= str_replace($this->array_replace, $arrText, $content);
		$content	= nl2br($content);

		return $content;

	}

}
?>