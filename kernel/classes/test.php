<?php
//
// $Id$
//

require ( "sphinxapi.php" );

$cl = new SphinxClient();
$cl->SetServer('127.0.0.1', 33120);
$sphinx_status = $cl->status();

if ($sphinx_status[0][1] > 1) {
echo "HTTP/1.1 200 OK\r\n";
echo "Content-Type: Content-Type: text/plain\r\n";
echo "\r\n";
echo "Sphinx is running.\r\n";
echo "\r\n";
} else {
echo "HTTP/1.1 503 Service Unavailable\r\n";
echo "Content-Type: Content-Type: text/plain\r\n";
echo "\r\n";
echo "Sphinx is *down*.\r\n";
echo "\r\n";
}
