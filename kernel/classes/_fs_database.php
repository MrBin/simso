<?php
if( strpos($_SERVER['SERVER_NAME'], 'sieuthisimthe.com') === false && strpos($_SERVER['SERVER_NAME'], 'localhost') === false )
	exit();
	
class db_init
{
	function db_init()
	{
		#+
		global $dbHost;
		global $dbUser;
		global $dbPass;
		global $dbName;
		
		#+
		$this->server	= $dbHost;
		$this->username = $dbUser;
		$this->password = $dbPass;
		$this->database = $dbName;
	}
	function __destruct()
	{
		unset($this->server);
		unset($this->username);
		unset($this->password);
		unset($this->database);
	}
}

class dbQuery
{
	var $_resource =  '';
	var $_result   =  '';
	var $_errorNum =  0;
	var $_errorMsg =  '';
	var $_sqlStr   =  '';
	var $_offset   =  0;
	var $_limit    =  0;
	var $_debug    =  0;
	
	function __construct($sqlStr='', $offset=0, $limit=0)
	{
		$dbInfo = new db_init();
		
		// Kiểm tra hàm kết nối cơ bản có tồn tại hay không
		if ( !function_exists( 'mysql_pconnect' ) ){
			$this->_errorNum = 1;
			$this->_errorMsg = 'The MySQL adapter "mysql" is not available.';
			return;
		}
		
		//Kết nối tới máy chủ MySQL
		if ( !($this->_resource = @mysql_pconnect( $dbInfo->server, $dbInfo->username, $dbInfo->password, true )) ){
			$this->_errorNum = 2;
			$this->_errorMsg = 'Could not connect to MySQL';
			return;
		}
		
		//Chọn database
		if(  $dbInfo->database ){
			$this->select( $dbInfo->database );
		}
		
		//Thiết lập chế độ kết nối UTF8
		$this->setUTF();
		
		//Thiết lập câu lệnh nếu có
		if( $sqlStr )
		{
			$this->setQuery($sqlStr, $offset, $limit);
		}
	}
	
	/**
	 * Hủy bỏ đối tượng database
	 *
	 * @return boolean
	 */
	function __destruct()
	{
		$return = false;
		if ( is_resource($this->_resource) ){
			$return = mysql_close($this->_resource);
		}
		return $return;
	}
	
	/**
	 * Thiết lập câu lệnh
	 *
	 */
	public function setQuery($sqlStr, $offset=0, $limit=0)
	{
		//Thiết lập thông số
		$this->_sqlStr = $sqlStr;
		$this->_offset = $offset;
		$this->_limit  = $limit;
	}
	
	/**
	 * Thực thi một câu lệnh
	 *
	 *@access công khai
	 *@param $sqlStr: câu lệnh cần thực thi
	 *@return TRUE nếu thực thi thành công, FALSE nếu lỗi
	 */
	public function Execute($sqlStr)
	{
		if( !$sqlStr ){
			return false;
		}
		
		$this->_sqlStr = $sqlStr;
		$this->_debug  = 0;
		
		if( !$this->query() ){
			return false;
		}
		
		return true;
	}
	
	/**
	 * Lấy câu lệnh
	 *
	 *@access công khai
	 */
	public function getSQL()
	{
		return $this->_sqlStr;	
	}
	
	/**
	 * Kiểm tra trạng thái kết nối tới máy chủ MySQL
	 *
	 * @return boolean true: kết nối - false: không kết nối
	 */
	public function connected()
	{
		if( is_resource($this->_resource) ) {
			return mysql_ping($this->_resource);
		}
		return false;
	}
	
	/**
	 * Chọn database để sử dụng
	 *
	 * @access	công khai
	 * @param	thông số: $database = tên database
	 * @return	boolean trả về True nếu như thành công
	 */
	public function select($database)
	{
		if( !$database ){
			return false;
		}

		if ( !mysql_select_db( $database, $this->_resource ) ){
			$this->_errorNum = 3;
			$this->_errorMsg = 'Could not connect to database';
			return false;
		}
		
		/*
		// Nếu như chạy trên MySQL 5, thiết lập sql-mode là mysql40
		if ( strpos( $this->getVersion(), '5' ) === 0 ) {
			$this->setQuery( "SET sql_mode = 'MYSQL40'" );
			$this->query();
		}
		*/
		
		return true;
	}
	
	/**
	 * Kiểm tra xem có hỗ trợ UTF hay không
	 *
	 * @access	công khai
	 * @return 	boolean True nếu như có hỗ trợ UTF
	 */
	public function hasUTF()
	{
		$verParts = explode( '.', $this->getVersion() );
		return ($verParts[0] == 5 || ($verParts[0] == 4 && $verParts[1] == 1 && (int)$verParts[2] >= 2));
	}
	
	/**
	 * Thiết lập chế độ UTF cho kết nối
	 *
	 * @access	công khai
	 */
	public function setUTF()
	{
		mysql_query( "SET NAMES 'utf8'", $this->_resource );
	}
	
	/**
	 * Lấy chuỗi đã được escape
	 *
	 * @access	công khai
	 * @param	thông số: chuỗi cần escape
	 * @return	chuỗi
	 * @abstract
	 */
	public function getEscaped($text)
	{
		return mysql_real_escape_string( $text, $this->_resource );
	}

	
	/**
	 * Thực thi câu lệnh
	 *
	 * @access	public
	 * @param	$query: câu lệnh cần thực thi
	 * @param       $offset: điểm bắt đầu
	 * @param       $limit: số bản ghi cần lấy
	 * @return 	database resource cố định nếu thành công, FALSE nếu không thành công
	 */
	public function query()
	{
		if ( !is_resource($this->_resource) ) {
			return false;
		}
		
		$sql = $this->_sqlStr;
		
		//Thiết lập giới hạn
		if ( $this->_offset > 0 || $this->_limit > 0) {
			$sql .= ' LIMIT '.$this->_offset.', '.$this->_limit;
		}
		
		if ($this->_debug){
			$this->_ticker++;
			$this->_log[] = $sql;
		}
		$this->_errorNum = 0;
		$this->_errorMsg = '';
		$this->_result   = mysql_query( $sql, $this->_resource  );

		if ( !$this->_result )
		{
			$this->_errorNum = mysql_errno( $this->_resource );
			$this->_errorMsg = mysql_error( $this->_resource )." SQL=$sql";

			if ($this->_debug){
				die( 'MySQL query error: '.$this->_errorNum.' - '.$this->_errorMsg );
			}
			return false;
		}
		
		return $this->_result;
	}
	
	/**
	 * Lấy tổng số hàng thực hiện câu lệnh
	 *
	 * @access	công khai
	 * @return      tổng số hàng
	 */
	public function getAffectedRows()
	{
		return mysql_affected_rows( $this->_resource );
	}
	
	/**
	 * Lấy tổng số hàng
	 *
	 * @access	công khai
	 * @return      tổng số hàng của câu lệnh mới nhất
	 */
	public function getNumRows()
	{
		if ( !($cur = $this->query()) ){
			return null;
		}
		return mysql_num_rows( $this->_result );
	}
	
	/**
	 * Lấy trường của dòng đầu tiên
	 *
	 * @access	Công khai
	 * @param       $key = vị trí của trường
	 * @return      Giá trị của trường hoặc rỗng nếu câu lệnh lỗi
	 */
	public function loadResult($key=0)
	{
		if ( !($cur = $this->query()) ){
			return null;
		}
		
		$ret = null;
		if ( $row = mysql_fetch_row($cur) ) {
			$ret = $row[$key];
		}
		mysql_free_result($cur);
		
		return $ret;
	}
	
	/**
	 * Load an array of single field results into an array
	 *
	 * @access	public
	 */
	public function loadResultArray($numinarray = 0)
	{
		if (!($cur = $this->query())) {
			return null;
		}
		$array = array();
		while ($row = mysql_fetch_row( $cur )) {
			$array[] = $row[$numinarray];
		}
		mysql_free_result( $cur );
		return $array;
	}

	/**
	* Fetch a result row as an associative array
	*
	* @access	public
	* @return array
	*/
	public function loadAssoc()
	{
		if (!($cur = $this->query())) {
			return null;
		}
		$ret = null;
		if ($array = mysql_fetch_assoc( $cur )) {
			$ret = $array;
		}
		mysql_free_result( $cur );
		return $ret;
	}

	/**
	* Load a assoc list of database rows
	*
	* @access	public
	* @param string The field name of a primary key
	* @return array If <var>key</var> is empty as sequential list of returned records.
	*/
	public function loadAssocList( $key='' )
	{
		if (!($cur = $this->query())) {
			return null;
		}
		$array = array();
		while ($row = mysql_fetch_assoc( $cur )) {
			if ($key) {
				$array[$row[$key]] = $row;
			} else {
				$array[] = $row;
			}
		}
		mysql_free_result( $cur );
		return $array;
	}

	/**
	* This global function loads the first row of a query into an object
	*
	* @access	public
	* @return 	object
	*/
	public function loadObject()
	{
		if (!($cur = $this->query())) {
			return null;
		}
		$ret = null;
		if ($object = mysql_fetch_object( $cur )) {
			$ret = $object;
		}
		mysql_free_result( $cur );
		return $ret;
	}

	/**
	* Load a list of database objects
	*
	* If <var>key</var> is not empty then the returned array is indexed by the value
	* the database key.  Returns <var>null</var> if the query fails.
	*
	* @access	public
	* @param string The field name of a primary key
	* @return array If <var>key</var> is empty as sequential list of returned records.
	*/
	public function loadObjectList( $key='' )
	{
		if (!($cur = $this->query())) {
			return null;
		}
		$array = array();
		while ($row = mysql_fetch_object( $cur )) {
			if ($key) {
				$array[$row->$key] = $row;
			} else {
				$array[] = $row;
			}
		}
		mysql_free_result( $cur );
		return $array;
	}

	/**
	 * Description
	 *
	 * @access	public
	 * @return The first row of the query.
	 */
	public function loadRow()
	{
		if (!($cur = $this->query())) {
			return null;
		}
		$ret = null;
		if ($row = mysql_fetch_row( $cur )) {
			$ret = $row;
		}
		mysql_free_result( $cur );
		return $ret;
	}

	/**
	* Load a list of database rows (numeric column indexing)
	*
	* @access public
	* @param string The field name of a primary key
	* @return array If <var>key</var> is empty as sequential list of returned records.
	* If <var>key</var> is not empty then the returned array is indexed by the value
	* the database key.  Returns <var>null</var> if the query fails.
	*/
	public function loadRowList( $key=null )
	{
		if (!($cur = $this->query())) {
			return null;
		}
		$array = array();
		while ($row = mysql_fetch_row( $cur )) {
			if ($key !== null) {
				$array[$row[$key]] = $row;
			} else {
				$array[] = $row;
			}
		}
		mysql_free_result( $cur );
		return $array;
	}
	
	/**
	 * Inserts a row into a table based on an objects properties
	 *
	 * @access	public
	 * @param	string	The name of the table
	 * @param	object	An object whose properties match table fields
	 * @param	string	The name of the primary key. If provided the object property is updated.
	 */
	public function insertObject( $table, &$object, $keyName = NULL )
	{
		$fmtsql = 'INSERT INTO '.$this->nameQuote($table).' ( %s ) VALUES ( %s ) ';
		$fields = array();
		foreach (get_object_vars( $object ) as $k => $v) {
			if (is_array($v) or is_object($v) or $v === NULL) {
				continue;
			}
			if ($k[0] == '_') { // internal field
				continue;
			}
			$fields[] = $this->nameQuote( $k );
			$values[] = $this->isQuoted( $k ) ? $this->Quote( $v ) : (int) $v;
		}
		$this->setQuery( sprintf( $fmtsql, implode( ",", $fields ) ,  implode( ",", $values ) ) );
		if (!$this->query()) {
			return false;
		}
		$id = $this->insertid();
		if ($keyName && $id) {
			$object->$keyName = $id;
		}
		return true;
	}

	/**
	 * Description
	 *
	 * @access public
	 * @param [type] $updateNulls
	 */
	public function updateObject( $table, &$object, $keyName, $updateNulls=true )
	{
		$fmtsql = 'UPDATE '.$this->nameQuote($table).' SET %s WHERE %s';
		$tmp = array();
		foreach (get_object_vars( $object ) as $k => $v)
		{
			if( is_array($v) or is_object($v) or $k[0] == '_' ) { // internal or NA field
				continue;
			}
			if( $k == $keyName ) { // PK not to be updated
				$where = $keyName . '=' . $this->Quote( $v );
				continue;
			}
			if ($v === null)
			{
				if ($updateNulls) {
					$val = 'NULL';
				} else {
					continue;
				}
			} else {
				$val = $this->isQuoted( $k ) ? $this->Quote( $v ) : (int) $v;
			}
			$tmp[] = $this->nameQuote( $k ) . '=' . $val;
		}
		$this->setQuery( sprintf( $fmtsql, implode( ",", $tmp ) , $where ) );
		return $this->query();
	}

	/**
	 * Lấy ID tự tăng của câu lênh mới nhất
	 *
	 * @access công khai
	 */
	public function insertid()
	{
		return mysql_insert_id( $this->_resource );
	}

	/**
	 * Lấy phiên bản hiện tại của máy chủ MySQL
	 *
	 * @access công khai
	 */
	public function getVersion()
	{
		return mysql_get_server_info( $this->_resource );
	}

	/**
	 * Assumes database collation in use by sampling one text field in one table
	 *
	 * @access	public
	 * @return string Collation in use
	 */
	public function getCollation ()
	{
		if ( $this->hasUTF() ) {
			$this->setQuery( 'SHOW FULL COLUMNS FROM #__content' );
			$array = $this->loadAssocList();
			return $array['4']['Collation'];
		} else {
			return "N/A (mySQL < 4.1.2)";
		}
	}

	/**
	 * Description
	 *
	 * @access	public
	 * @return array A list of all the tables in the database
	 */
	public function getTableList()
	{
		$this->setQuery( 'SHOW TABLES' );
		return $this->loadResultArray();
	}

	/**
	 * Shows the CREATE TABLE statement that creates the given tables
	 *
	 * @access	public
	 * @param 	array|string 	A table name or a list of table names
	 * @return 	array A list the create SQL for the tables
	 */
	public function getTableCreate( $tables )
	{
		settype($tables, 'array'); //force to array
		$result = array();

		foreach ($tables as $tblval) {
			$this->setQuery( 'SHOW CREATE table ' . $this->getEscaped( $tblval ) );
			$rows = $this->loadRowList();
			foreach ($rows as $row) {
				$result[$tblval] = $row[1];
			}
		}

		return $result;
	}

	/**
	 * Retrieves information about the given tables
	 *
	 * @access	public
	 * @param 	array|string 	A table name or a list of table names
	 * @param	boolean			Only return field types, default true
	 * @return	array An array of fields by table
	 */
	public function getTableFields( $tables, $typeonly = true )
	{
		settype($tables, 'array'); //force to array
		$result = array();

		foreach ($tables as $tblval)
		{
			$this->setQuery( 'SHOW FIELDS FROM ' . $tblval );
			$fields = $this->loadObjectList();

			if($typeonly)
			{
				foreach ($fields as $field) {
					$result[$tblval][$field->Field] = preg_replace("/[(0-9)]/",'', $field->Type );
				}
			}
			else
			{
				foreach ($fields as $field) {
					$result[$tblval][$field->Field] = $field;
				}
			}
		}

		return $result;
	}
	
	/**
	 * Lấy lỗi khi thực thi câu lênh
	 *
	 * @access công khai
	 * @return chuỗi thông điệp báo lỗi từ máy chủ MySQL
	 */
	public function getErrorMsg()
	{
		return $this->_errorMsg;
	}
}

class db_query 
{
	var $result;
	var $links;
	
	function db_query($query)
	{
		//echo $query;
		$dbinit = new db_init();
		
		//Khai bao connect
		$this->links = mysql_pconnect($dbinit->server, $dbinit->username, $dbinit->password);
		$db_select = mysql_select_db($dbinit->database,$this->links);
		
		//echo $query;
		$time_start = $this->microtime_float();
		
		mysql_query("SET NAMES 'utf8'");
		$this->result = mysql_query($query,$this->links);
		
		/*
		$time_end = $this->microtime_float();
		$time = $time_end - $time_start;
		if ($time >= 0.05){
			 //Ghi log o file
			 $path = $_SERVER['DOCUMENT_ROOT'] . "/store/log/";
			 $filename = date("Y_m_d_H") . "h.txt";
			 $timetong =0;
			 //Ghi log o file
			 $url = $_SERVER['SERVER_NAME'] . @$_SERVER['SCRIPT_NAME'] . @$_SERVER['QUERY_STRING'];
			 if(file_exists($path . $filename)){
			 	$str = file_get_contents($path . $filename);
				$str = number_format($time,10,".",",") . " :  " . $query . chr(13) . chr(13) . $str;
				file_put_contents($path . $filename,"Thoi gian : " . date("H:i") . " : " . $url . "--------------------------------------------->" . chr(13) . number_format($time,10,".",",") . " :  " . $str);
			 }else{
			 	file_put_contents($path . $filename,"Thoi gian : " . date("H:i") . " : " . $url . "--------------------------------------------->" . chr(13) . number_format($time,10,".",",") . " :  " . $query);
				@chmod($path . $filename,0644);
			 }
		}
		// echo " <font color='red'><b>" . number_format($time,10,".",",") . "</b></font> ";	
		//*/
		if(isset($_SESSION["numberQuery"])) $_SESSION["numberQuery"]++;
		unset($dbinit);
		if (!$this->result)
		{
			$error = mysql_error($this->links);
			mysql_close($this->links);	
			die("Error in query string " . $error);
		}
		
	}
	
	function close()
	{
		mysql_free_result($this->result); 
		if ($this->links) 
		{  
			mysql_close($this->links);		
		}
	}
	//Hàm tính time
	function microtime_float()
	{
	   list($usec, $sec) = explode(" ", microtime());
	   return ((float)$usec + (float)$sec);
	}	
}

class db_execute 
{
	var $links;
	function db_execute($query, $offset=0, $limit=0)
	{
		$dbinit = new db_init();
		
		$this->links = mysql_pconnect($dbinit->server, $dbinit->username, $dbinit->password);
		mysql_select_db($dbinit->database,$this->links);
		
		unset($dbinit);
		mysql_query("SET NAMES 'utf8'");
		$result = mysql_query($query,$this->links);
		mysql_close($this->links);	
		
		return $result;
	}
}

class db_execute_return 
{
	var $links;
	var $result;
	
	function db_execute($query)
	{
		$dbinit = new db_init();
		$this->links = mysql_pconnect($dbinit->server, $dbinit->username, $dbinit->password);
		mysql_select_db($dbinit->database);
		
		unset($dbinit);
		mysql_query("SET NAMES 'utf8'");
		mysql_query($query);
		
		$last_id = 0;
		$this->result = mysql_query("select LAST_INSERT_ID() as last_id",$this->links);
		
		if($row=mysql_fetch_array($this->result)){
			$last_id = $row["last_id"];
		}
		
		mysql_close($this->links); 
		return $last_id;
	}
}
?>