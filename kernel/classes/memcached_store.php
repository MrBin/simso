<?php
define('MEMCACHED_SERVER', '127.0.0.1');
define('MEMCACHED_PORT', '11211');

//Class memcached
class memcached_store {
	public $memcache;
	public $connect_successful = false;
	// Biến giữ thể hiện duy nhất của class
	public static $instance = NULL;
	// Danh sách domain bỏ qua k dùng memcache
	private $deny_domain = array(
		'localhost',
		'www.sieuthisimthe.com.vn',
		'boisim.com.vn'
	);

	/**
	 * Singleton method to get class instance
	 * @return instance memcached_store
	 */
	public static function getInstance() {
		
		if (is_null(self::$instance)) {
			self::$instance = new memcached_store();
		}
		return self::$instance;
	}

	/**
	 * Constructor
	 */
	private function __construct(){

		//Nếu MEMCACHED_SERVER là none thì return luôn
		if (MEMCACHED_SERVER == "none" || in_array($_SERVER['SERVER_NAME'], $this->deny_domain)){
			return;
		}

		$this->memcache = new Memcache;

		//Bẻ ra nhiều server
		$array_memcached = explode(",", MEMCACHED_SERVER);

		//Loop để add memcached server
		foreach ($array_memcached as $m_key => $m_value){
			$link_connect = @$this->memcache->addServer(trim($m_value), MEMCACHED_PORT, true);
		}

		//Nếu kết nối thành công thì cho biến connect_successful là true
		if ($link_connect){
			$this->connect_successful = true;
		}
	}

	/**
	 * Get value by key from memcache
	 * @param  string $key Tên key
	 * @return data
	 */
	public function get($key){
		global $dbName;
		if ($this->connect_successful) return @$this->memcache->get($dbName . '_' . $key);
		else return NULL;
	}

	/**
	 * Set value & key to memcache
	 * Default timeout is 3600s (1h)
	 * @param string  $key     Tên key
	 * @param mixed   $value   Data
	 * @param integer $timeout Thời gian lưu data trong memcache
	 */
	public function set($key, $value, $timeout = 3600){
		global $dbName;
		if ($this->connect_successful) return @$this->memcache->set($dbName . '_' . $key, $value, 0, $timeout);
		else return NULL;
	}
	
	public function flush(){
		if ($this->connect_successful) return @$this->memcache->flush();
		else return NULL;
	}

	/**
	 * Get stats method
	 */
	public function getExtendedStats(){
		if ($this->connect_successful) return $this->memcache->getExtendedStats();
		else return NULL;
	}

	/**
	 * Overide clone method.
	 * Chặn việc tạo thể hiện mới bằng magic method clone
	 */
	private function __clone() {}
}