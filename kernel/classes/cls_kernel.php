<?
/*
Created by: mr.Trieu
Call me if you want: 090 22 989 77
Y!m: final.style
---------------------------------
Class tong hop cac public static function co ban can dung trong luc lap trinh.
*/
class myKernel{

	public static function _include($_temp='',$includ_file){
		if(!$includ_file) return false;
		if(file_exists($_temp.'/'.$includ_file)) require($_temp.'/'.$includ_file);
		else return 'File could not exists';
	}

	// Random code
	public function generateCode($characters){
      /* list all possible characters,similar looking characters and vowels have been removed */
      $possible = '23456789bcdfghjkmnpqrstvwxyz';
      $code = '';
      $i = 0;
      while($i < $characters){ 
         $code .= substr($possible,mt_rand(0,strlen($possible)-1),1);
         $i++;
      }
      return $code;
   }

	// Generate Start && End Data
	public static function _gDate($date='',$stop=0){
		if($date=='') $current_date = strtotime('now');

		$seperant = array('|',' ',':',',','.','-','&nbsp;','\'','"');
		$strDate = str_replace($seperant,'/',$date);
		unset($seperant);

		$arrDate = explode('/',$strDate);
		$day = $arrDate[0];
		$month = $arrDate[1];
		$year = $arrDate[2];
		
		if($stop==0) $current_date = mktime('00','00','00',$month,$day,$year);
		else $current_date = mktime('23','59','59',$month,$day,$year);

		return $current_date;
	}

	// Encode value
	public static function vEncode($encodeStr=''){
		$str = '';
		if($encodeStr == '') return $encodeStr;
		if(!empty($encodeStr)){
			$enc = base64_encode($encodeStr);
			$enc = str_replace('=','',$enc);
			$enc = str_rot13($enc);
			$str = $enc;
		}
		return $str;
	}
	// Decode value
	public static function vDecode($encodedStr='',$type=0){
		$str = '';
		$encodedStr = str_replace(' ','+',$encodedStr);
		if(!empty($encodedStr)){
			 $dec = str_rot13($encodedStr);
			 $dec = base64_decode($dec);
			$str = $dec;
		}
		return $str;
	}

	// Gallery Images
	public static function _gallery($DAT_ID=0){
		$db_data = new db_query("SELECT img_name,img_image
								 FROM imgproducts
								 WHERE img_active = 1 AND img_pro_id=".$DAT_ID."
								 ORDER BY img_order ASC");
		if(mysql_num_rows($db_data->result)>0){
			while($row=mysql_fetch_array($db_data->result)){
				$path = "pictures/thumb_".$row['img_image'];
				if($row['img_image'] != '' && @file_exists($path)){
					list($width,$height,$type,$attr) = @getimagesize($path);
					echo '<a href="/pictures/'.$row['img_image'].'" title="'.$row['img_name'].'">';
					echo '<img src="/pictures/thumb_'.$row['img_image'].'" border="0" width="'.$width.'" height="'.$height.'" class="pGallery" title="'.$row['img_name'].'" alt="'.$row['img_name'].'" />';
					echo '</a>';
				}
			}
		}
		$db_data->close();
		unset($db_data);
	}

	/* GetValue:
	Lay gia tri cua bien tu method Post or Get
	*/
	public static function getValue($value_name, $data_type = "int", $method = "GET", $default_value = 0, $advance = 0){
		$value = $default_value;
		switch($method){
			case "GET": if(isset($_GET[$value_name])) $value = $_GET[$value_name]; break;
			case "POST": if(isset($_POST[$value_name])) $value = $_POST[$value_name]; break;
			case "COOKIE": if(isset($_COOKIE[$value_name])) $value = $_COOKIE[$value_name]; break;
			case "SESSION": if(isset($_SESSION[$value_name])) $value = $_SESSION[$value_name]; break;
			default: if(isset($_GET[$value_name])) $value = $_GET[$value_name]; break;
		}
		$valueArray	= array ("int" => intval($value), "str" => strval($value), "flo" => floatval($value), "dbl" => doubleval($value), "arr" => $value);
		foreach($valueArray as $key => $returnValue){
			if($data_type == $key){
				if($advance != 0){
					switch($data_type){
						case ($data_type == "int")||($data_type == "dbl")||($data_type == "flo"):
							$returnValue = abs($returnValue);
							break;
						case "str":
							$returnValue = str_replace("\'", "'", $returnValue);
							$returnValue = str_replace("'", "&prime;", $returnValue);
							$returnValue = str_replace("\"", "&quot;", $returnValue);
							$returnValue = str_replace('"', "&quot;", $returnValue);
							//$returnValue = str_replace("<", "&lt;", $returnValue);
							//$returnValue = str_replace(">", "&gt;", $returnValue);
							break;
					}
				}
				return $returnValue;
				break;
			}
		}
		return (intval($value));
	}
	
	/* Remove all html tag when insert data Description */
	public static function removeHTML($string){
		$str	= array(chr(9),chr(10),chr(13));
		$string	= mb_strtolower($string,"UTF-8");
		$string	= str_replace($str," ",$string);
		$string = preg_replace("/<script.*?\>.*?<\/script>/si","",$string); 
		$string = preg_replace("/<style.*?\>.*?<\/style>/si","",$string); 
		$string = preg_replace("/<.*?\>/si","",$string); 
		$string = str_replace("&nbsp;"," ",$string);
		$string = str_replace("\'","&prime;",$string);
		$string = str_replace("'","&prime;",$string);
		$string = str_replace("\"","&quot;",$string);
		$string = str_replace('"',"&quot;",$string);
		//$string = html_entity_decode ($string);
		//$string = htmlentities($string,ENT_QUOTES);
		return $string;
	}
	
	//Quay lai URL sau khi thuc hien 1 so cong doan xu ly
	public static function returnURL($serverName = 0, $scriptName = 0, $fileName = 1, $queryString = 1, $otherString = 0){
		$url   = "";
		$slash = "../";
		if($scriptName != 0)$slash	= "";
		if($serverName != 0){
			if(isset($_SERVER['SERVER_NAME']))	$url .= "http://" . $_SERVER['SERVER_NAME'] . $slash;
		}
		if($scriptName != 0){
			if(isset($_SERVER['SCRIPT_NAME']))	$url .= substr($_SERVER['SCRIPT_NAME'], 0, strrpos($_SERVER['SCRIPT_NAME'], "/") + 1);
		}
		if($fileName != 0){
			if(isset($_SERVER['SCRIPT_NAME']))	$url .= substr($_SERVER['SCRIPT_NAME'], strrpos($_SERVER['SCRIPT_NAME'], "/") + 1);
		}
		if($queryString!= 0){
			$url .= "?";
			if(isset($_SERVER['QUERY_STRING'])) $url .= $_SERVER['QUERY_STRING'];
		}
		if($otherString == 0){
			if(strrpos($url, "#") !== false){
				$url = substr($url, 0, strrpos($url, "#"));
			}
		}
		return $url;
	}
	
	/*-------Repalce string-------*/
	public static function replaceStr($string){
		$string = str_replace("'","\'",$string);
		$string = str_replace("\"","\'",$string);
		$string = str_replace("\n \r"," ",$string);
		$string = str_replace("&#39 &#39;"," ",$string);
		$string = str_replace(chr(9)," ",$string);
		$string = str_replace(chr(10)," ",$string);
		$string = str_replace(chr(13)," ",$string);
		return $string;
	}
	
	/*--------------------------Resize Image-------------------------*/
	public static function resize_image($path,$filename,$maxwidth,$maxheight,$quality,$type="small_"){
		//Load library extensiton in php.ini
		if(!extension_loaded("gd")){
			if(strtoupper(substr(PHP_OS,0,3) == "WIN")) dl("php_gd2.dll");
			else dl("gd2.so");
		}
		$sExtension = substr($filename,(strrpos($filename,'.') + 1));
		$sExtension = strtolower($sExtension);
		// Get new dimensions
		list($width,$height) = getimagesize($path.$filename);
		if($width != 0 && $height !=0){
			if($maxwidth/$width>$maxheight/$height){
				$percent = $maxheight/$height;
			}
			else{
				$percent = $maxwidth/$width;
			}
		}
		$new_width = $width*$percent;
		$new_height = $height*$percent;
		// Resample
		$image_p = imagecreatetruecolor($new_width,$new_height);
		//check extension file
		switch($sExtension){
			case "jpg":
				$image = imagecreatefromjpeg($path.$filename);
			break;
			case "gif":
				$image = imagecreatefromgif($path.$filename);
			break;
			case "png":
				$image = imagecreatefrompng($path.$filename);
			break;
		}
		//$image = imagecreatefromjpeg($path.$filename);
		imagecopyresampled($image_p,$image,0,0,0,0,$new_width,$new_height,$width,$height);
		// Output	
		switch($sExtension){
			case "jpg":
				imagejpeg($image_p,$path.$type.$filename,$quality);
			break;
			case "gif":
				imagegif($image_p,$path.$type.$filename);
			break;
			case "png":
				imagepng($image_p,$path.$type.$filename);
			break;
		}
		//imagejpeg($image_p,$path . "small_".$filename,$quality);
		imagedestroy($image_p);
	}
	
	//Delete file
	public static function delete_file($table_name,$id_field,$id_field_value,$field_select,$ff_imagepath){
		$db_select = new db_query("SELECT " . $field_select . " " .
								  "FROM " . $table_name . " " .
								  "WHERE " . $id_field . "=" . $id_field_value);
		if($row=mysql_fetch_array($db_select->result)){
			if(file_exists($ff_imagepath . $row[$field_select])) @unlink($ff_imagepath . $row[$field_select]);
			if(file_exists($ff_imagepath . "small_" . $row[$field_select])) @unlink($ff_imagepath . "small_" . $row[$field_select]);
			if(file_exists($ff_imagepath . "medium_" . $row[$field_select])) @unlink($ff_imagepath . "medium_" . $row[$field_select]);
			if(file_exists($ff_imagepath . "large_" . $row[$field_select])) @unlink($ff_imagepath . "large_" . $row[$field_select]);
		}	
		unset($db_select);					
	}
	//Chmod file after update
	public static function chmod_file_update($ff_table,$id_field,$id_field_value,$field_select,$ff_imagepath){
		$db_select = new db_query("SELECT " . $field_select .
								  " FROM " . $ff_table .
								  " WHERE " . $id_field . " = " . $id_field_value);
		if($row = mysql_fetch_array($db_select->result)){
			if(file_exists($ff_imagepath . $row[$field_select])) @chmod($ff_imagepath . $row[field_select],0644);
			if(file_exists($ff_imagepath . "small_" . $row[$field_select])) @chmod($ff_imagepath . "small_" . $row[field_select],0644);
			if(file_exists($ff_imagepath . "medium_" . $row[$field_select])) @chmod($ff_imagepath . "medium_" . $row[field_select],0644);
			if(file_exists($ff_imagepath . "large_" . $row[$field_select])) @chmod($ff_imagepath . "large_" . $row[field_select],0644);
			/*echo "<script>alert('Chmod Ok')</script>";*/
		}
		unset($db_select);
	}
	//Chmod file after Insert
	public static function chmod_file_insert($ff_imagepath,$filename){
		chmod($ff_imagepath . $filename,0644);
		chmod($ff_imagepath . "small_" . $filename,0644);
		chmod($ff_imagepath . "medium_" . $filename,0644);
		chmod($ff_imagepath . "large_" . $filename,0644);
		/*echo "<script>alert('Chmod Ok')</script>";*/
	}
	
	//Convert Format string to integer
	public static function convertDateTime($strDate = "", $strTime = ""){
		//Break string and create array date time
		$strDate		= str_replace("/", "-", $strDate);
		$strDateArray	= explode("-", $strDate);
		$countDateArr	= count($strDateArray);
		$strTime		= str_replace("-", ":", $strTime);
		$strTimeArray	= explode(":", $strTime);
		$countTimeArr	= count($strTimeArray);
		//Get Current date time
		$today			= getdate();
		$day			= $today["mday"];
		$mon			= $today["mon"];
		$year			= $today["year"];
		$hour			= $today["hours"];
		$min			= $today["minutes"];
		$sec			= $today["seconds"];
		//Get date array
		switch($countDateArr){
			case 2:
				$day	= intval($strDateArray[0]);
				$mon	= intval($strDateArray[1]);
				break;
			case $countDateArr >= 3:
				$day	= intval($strDateArray[0]);
				$mon	= intval($strDateArray[1]);
				$year 	= intval($strDateArray[2]);
				break;
		}
		//Get time array
		switch($countTimeArr){
			case 2:
				$hour		= intval($strTimeArray[0]);
				$min		= intval($strTimeArray[1]);
				break;
			case $countTimeArr >= 3:
				$hour		= intval($strTimeArray[0]);
				$min		= intval($strTimeArray[1]);
				$sec		= intval($strTimeArray[2]);
				break;
		}
		//Return date time integer
		if(@mktime($hour, $min, $sec, $mon, $day, $year) == -1) return $today[0];
		else return mktime($hour, $min, $sec, $mon, $day, $year);
	}
	
	/* Thay thế các ký tự tiếng Việt có dấu thành không dấu */
	public static function character_replace($str){
		if($str=="") return false;
		$str = trim($str);
		$chars = array(
						'a'=>array('ấ','ầ','ẩ','ẫ','ậ','Ấ','Ầ','Ẩ','Ẫ','Ậ','ắ','ằ','ẳ','ẵ','ặ','Ắ','Ằ','Ẳ','Ẵ','Ặ','á','à','ả','ã','ạ','â','ă','Á','À','Ả','Ã','Ạ','Â','Ă'),
						'e'=>array('ế','ề','ể','ễ','ệ','Ế','Ề','Ể','Ễ','Ệ','é','è','ẻ','ẽ','ẹ','ê','É','È','Ẻ','Ẽ','Ẹ','Ê'),
						'i'=>array('í','ì','ỉ','ĩ','ị','Í','Ì','Ỉ','Ĩ','Ị'),
						'o'=>array('ố','ồ','ổ','ỗ','ộ','Ố','Ồ','Ổ','Ô','Ộ','ớ','ờ','ở','ỡ','ợ','Ớ','Ờ','Ở','Ỡ','Ợ','ó','ò','ỏ','õ','ọ','ô','ơ','Ó','Ò','Ỏ','Õ','Ọ','Ô','Ơ'),
						'u'=>array('ứ','ừ','ử','ữ','ự','Ứ','Ừ','Ử','Ữ','Ự','ú','ù','ủ','ũ','ụ','ư','Ú','Ù','Ủ','Ũ','Ụ','Ư'),
						'y'=>array('ý','ỳ','ỷ','ỹ','ỵ','Ý','Ỳ','Ỷ','Ỹ','Ỵ'),
						'd'=>array('đ','Đ'),
						''=>array('/','\\',',','.','"','\"','-',"&quot;",'*','{','}','<','>','(',')','&lt;','&gt;','?',"'","\'",'~','#','^','“','”',':',';','&','&amp;','+','=','%','$','@','!'),
						'pc'=>array('%'),
						'_'=>array(' ','%20','_'),
						);
		foreach($chars as $key=>$arr)
		foreach($arr as $val)
		$str = str_replace($val,$key,$str);
		
		return $str;
	}
	//Cach 2 don gian hon
	public static function remove_unicode($str){
		if($str=="") return false;
		$str = trim($str);
		$chars = array(
						'a'=>array('ấ','ầ','ẩ','ẫ','ậ','Ấ','Ầ','Ẩ','Ẫ','Ậ','ắ','ằ','ẳ','ẵ','ặ','Ắ','Ằ','Ẳ','Ẵ','Ặ','á','à','ả','ã','ạ','â','ă','Á','À','Ả','Ã','Ạ','Â','Ă'),
						'e'=>array('ế','ề','ể','ễ','ệ','Ế','Ề','Ể','Ễ','Ệ','é','è','ẻ','ẽ','ẹ','ê','É','È','Ẻ','Ẽ','Ẹ','Ê'),
						'i'=>array('í','ì','ỉ','ĩ','ị','Í','Ì','Ỉ','Ĩ','Ị'),
						'o'=>array('ố','ồ','ổ','ỗ','ộ','Ố','Ồ','Ổ','Ô','Ộ','ớ','ờ','ở','ỡ','ợ','Ớ','Ờ','Ở','Ỡ','Ợ','ó','ò','ỏ','õ','ọ','ô','ơ','Ó','Ò','Ỏ','Õ','Ọ','Ô','Ơ'),
						'u'=>array('ứ','ừ','ử','ữ','ự','Ứ','Ừ','Ử','Ữ','Ự','ú','ù','ủ','ũ','ụ','ư','Ú','Ù','Ủ','Ũ','Ụ','Ư'),
						'y'=>array('ý','ỳ','ỷ','ỹ','ỵ','Ý','Ỳ','Ỷ','Ỹ','Ỵ'),
						'd'=>array('đ','Đ'),
						''=>array('/','\\',',','.','"','\"','-',"&quot;",'*','{','}','<','>','(',')','&lt;','&gt;','?',"'","\'",'~','#','^','“','”',':',';','&','&amp;','+','=','%','$','@','!'),
						'pc'=>array('%'),
						'-'=>array(' ','%20','_'),
						);
		foreach($chars as $key=>$arr)
		foreach($arr as $val)
		$str = str_replace($val,$key,$str);
		//$str = mb_convert_case($str,MB_CASE_LOWER,"UTF-8");
		
		return $str;
	}
	/* Cat doan van ban */
	public static function truncate($str, $len, $charset='UTF-8'){ 
		$str = html_entity_decode($str, ENT_QUOTES, $charset);    
		if(mb_strlen($str, $charset)> $len){ 
			$arr = explode(' ', $str); 
			$str = mb_substr($str, 0, $len, $charset); 
			$arrRes = explode(' ', $str); 
			$last = $arr[count($arrRes)-1]; 
			unset($arr);  
			if(strcasecmp($arrRes[count($arrRes)-1], $last)) 
			{ 
			 unset($arrRes[count($arrRes)-1]); 
			} 
		return implode(' ', $arrRes)." ..."; 
		} 
		return $str; 
	}
	/* Random */
	function random(){
		$rand_value = "";
		$rand_value.=rand(1000,9999);
		$rand_value.=chr(rand(65,90));
		$rand_value.=rand(1000,9999);
		$rand_value.=chr(rand(97,122));
		$rand_value.=rand(1000,9999);
		$rand_value.=chr(rand(97,122));
		$rand_value.=rand(1000,9999);
		return $rand_value;
	}
	/* valid Email */
	function validEmail($email){
		//First, we check that there's one @ symbol, and that the lengths are right
		if(!ereg("^[^@]{1,64}@[^@]{1,255}$", $email)){
		 //Email invalid because wrong number of characters in one section, or wrong number of @ symbols.
			return false;
		}
		//Split it into sections to make life easier
		$email_array = explode("@", $email);
		$local_array = explode(".", $email_array[0]);
		for($i = 0; $i < sizeof($local_array); $i++){
			if(!ereg("^(([A-Za-z0-9!#$%&'*+/=?^_`{|}~-][A-Za-z0-9!#$%&'*+/=?^_`{|}~\.-]{0,63})|(\"[^(\\|\")]{0,62}\"))$", $local_array[$i])){
				return false;
			}
		}
		if(!ereg("^\[?[0-9\.]+\]?$", $email_array[1])){
		//Check if domain is IP. If not, it should be valid domain name
			$domain_array = explode(".", $email_array[1]);
			if(sizeof($domain_array) < 2){
				return false; // Not enough parts to domain
			}
			for($i = 0; $i < sizeof($domain_array); $i++){
				if (!ereg("^(([A-Za-z0-9][A-Za-z0-9-]{0,61}[A-Za-z0-9])|([A-Za-z0-9]+))$", $domain_array[$i])){
					return false;
				}
			}
		}
		return true;
	}
}
?>