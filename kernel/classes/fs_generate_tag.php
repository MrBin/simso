<?
class generate_tag
{
	var $store_term			= array();
	var $array_term			= array();
	var $lightColorArray		= array('#00FFFF','#F0F8FF','#FFF0F5','#FFE4B5','#EEE8AA','#98FB98','#B0C4DE','#FF00FF','	#0000CD','#FDF5E6');
	var $darkColorArray		= array('#0000FF','#FF0000','#00008B','#A52A2A','#FF00FF','#008000','#C71585','#8B4513','#008080','#2F4F4F');
	var $_ignoreArray			= array('?','of','the','is','off','you','them','then','at','with','i','it','We','we');
	var $_ignoreCharArray	= array('?','!','.',',','~','@','#','$','%','^','&','*','(',')','-','_','+','=','<','>','/','[',']','{','}',':',';','"',"'",'~','`');
	var $text 			= '';
	function removeTagHtml($string){
		$string = preg_replace ('/<script.*?\>.*?<\/script>/si', ' ', $string); 
		$string = preg_replace ('/<style.*?\>.*?<\/style>/si', ' ', $string); 
		$string = preg_replace ('/<.*?\>/si', ' ', $string); 
		$string = str_replace ('&nbsp;', ' ', $string);
		$convmap = array(0x0, 0x2FFFF, 0, 0xFFFF);
		$string = @mb_decode_numericentity($string, $convmap, "UTF-8");
		//$string	=  trim(preg_replace("/[^A-Za-z0-9àáạảãâầấậẩẫăằắặẳẵèéẹẻẽêềếệểễìíịỉĩòóọỏõôồốộổỗơờớợởỡùúụủũưừứựửữỳýỵỷỹđÀÁẠẢÃÂẦẤẬẨẪĂẰẮẶẲẴÈÉẸẺẼÊỀẾỆỂỄÌÍỊỈĨÒÓỌỎÕÔỒỐỘỔỖƠỜỚỢỞỠÙÚỤỦŨƯỪỨỰỬỮỲÝỴỶỸ]/i"," ",$string));
		$str= array(chr(9),chr(10),chr(13),"”","“",'"');
		$arspace = array("  ","  ","  ","  ","  ","  ","  ","  ","  ","  ","  ","  ","  ","  ","  ");
		$content = str_replace($str,"",$string);
		$content = str_replace($arspace," ",$content);
		return $content;
	}
	function breakKey($content,$title,$tag=0){
		$title 			= mb_strtolower($title,"UTF-8");
		$content 		= $this->removeTagHtml($content);
		$array 			= explode(".",$content);
		$contentReturn = '';
		$arrayKey 		= explode(" ", $title);
		$arraySort 		= array();
		$arResult 		= array();
		foreach($array as $key=>$value){
			$value 				= mb_strtolower($value,"UTF-8");
			$arrayCau 			= explode(" ",$value);
			$result 	 			= array_intersect($arrayKey, $arrayCau);
			$arraySort[$key] 	= count($result);
			$arResult[$key]	= $result;
		}
		arsort($arraySort);
		$i=0;
		foreach($arraySort as $key=>$value){
			$i++;
			if(isset($array[$key]) && isset($arResult[$key])){
				$contentReturn = $contentReturn . $this->replaceTag($array[$key],$arResult[$key]) . '. ';
			}
			if($i==5) break;
		}
		unset($result);
		unset($arraySort);
		unset($arResult);
		if($tag==1) $this->text .= strip_tags($contentReturn);
		$contentReturn = str_replace("</b> <b>"," ",$contentReturn);
		
		return $contentReturn;
	}
	function replaceTag($content,$array=array()){
		
		if(count($array)>0){
			foreach($array as $key=>$value){
				$value = trim($value);
				if($value!=''){
					//echo $value. chr(13);
					$content = preg_replace("#" . $value . "#Usi","<b>$0</b>",$content);
					//echo $content;
				}
			}
		}
		return $content;
	}
	
	function create_tag($text=''){
		
		// Remove hết các ký tự xuống dòng, Tab và thẻ HTML
		$text	= $this->removeTagHtml($text);
		$text	= str_replace($this->_ignoreCharArray, "", $text);
		$text	= mb_strtolower($text, "UTF-8");
		
		$array_text	= explode(" ", $text);
		for($i=0; $i<count($array_text); $i++){
			if(mb_strlen($array_text[$i]) >= 3){
				$this->store_term[] = $array_text[$i];
				$this->array_term[] = $array_text[$i];
			}
		}
		
		$num_arr = count($this->array_term);
		for($i=0; $i<$num_arr; $i++){
			// Lưu Term 2
			if($i < $num_arr-1){
				$this->store_term[] = $this->array_term[$i] . " " . $this->array_term[$i+1];
			}
			// Lưu Term 3
			if($i < $num_arr-2){
				$this->store_term[] = $this->array_term[$i] . " " . $this->array_term[$i+1] . " " . $this->array_term[$i+2];
			}
			//* Lưu Term 3
			if($i < $num_arr-3){
				$this->store_term[] = $this->array_term[$i] . " " . $this->array_term[$i+1] . " " . $this->array_term[$i+2] . " " . $this->array_term[$i+3];
			}
			//*/
		}

		$array_count	= array_count_values($this->store_term);
		$array_remove	= array(1);
		
		$array_temp	= array_diff($array_count, $array_remove);
		//print_r($array_temp);
		//*/
		$new_array = array();
		
		foreach($array_temp as $key => $value){
			
			$arr_exp = explode(" ", $key);
			$num		= (count($arr_exp) > 1) ? $value*count($arr_exp)*1.7 : $value;
			$new_array[$key] = $num;
		}
		//*/
		arsort($new_array);
		array_splice($new_array, 10);
		$i=-1;
		foreach($new_array as $key=>$value){
			$i++;
			$new_array[$key] = '<a href="/'.  str_replace(" ","-",$key) .'.htm" style="font-size:' . (120+$value) . '%;color:' . $this->darkColorArray[$i]  . '">' . $key . '</a>';
			//echo '<a href="" style="font-size:' . (100+$value) . '%;color:' . $this->darkColorArray[9-$i]  . '">' . $key . '</a>&nbsp;';
		}
		ksort($new_array);
		echo implode(", ",$new_array);
		//print_r($new_array);
		//print_r($new_array); exit();
		
	}
}
?>