<?
class sphinxSearch{
   // Server Config here
   var $sphinx_host        = "127.0.0.1";
   var $sphinx_port        = 33120;
   var $sphinx_index       = "tbl_sim_boisim";

   /* Function sphinxSearch */
   function sphinxSearch($limit = 10000){
      
      // Khai bao bien
      $this->limit = $limit;

      // Khoi tao class
      $this->sphinx = new SphinxClient();
      
      // Ket noi sphinx
      $this->sphinx->SetServer($this->sphinx_host, $this->sphinx_port);
      
      // Timeout
      $this->sphinx->SetConnectTimeout(1.5);
      
      // Kieu search
      $this->sphinx->SetMatchMode( SPH_MATCH_EXTENDED ); // SPH_MATCH_ALL - SPH_MATCH_EXTENDED - SPH_MATCH_FULLSCAN
      
      // Array trả về sẽ không dùng doc_id làm key -> phục vụ việc groupby
      $this->sphinx->SetArrayResult(true);
      
      // Lấy max 5030 kết quả trả về
      $this->sphinx->_maxmatches = $this->limit;   
      
      // Open
      $this->sphinx->Open();
      
   }
   
   /* function search */
   function search($page_start = 0, $page_end = 0, $keyword = '', $module = '', $iCat = 0, $iType = 0, $iPrice = 0, $iDauSo = 0, $digit = 0, $iNguHanh = 0, $sort = 0, $iDaiLy = 0){
      global $iNguHanhSinh;

      // Lấy data
      $query      = "SELECT *
                     FROM tbl_sim_boisim
                     WHERE id = 326059059";
      $db_data = new db_query_sphinx($query);
      var_dump($db_data);
      exit();
      while($row = mysql_fetch_assoc($db_data->result)){
         var_dump($row);
      }
      exit();

      // Khai bao bien
      $arrayReturn   = array();
      $sphinx_keyword = $keyword;
      
      // Xy ly keyword
      $keywords = preg_replace("/[^0-9*xX]/si", "", $keyword);
      if ($keywords != ""){
         $spot = strpos($keywords, "*");
         $slen = strlen($keywords);
      }
      $keywords = str_replace(array("x", "X", "*"), array("*", "*", "*"), $keywords);
      for($i=0;$i<=15;$i++) $keywords = str_replace('**', '*', $keywords);
      

      if($module == "searchsim" && $keywords != ""){
         if(stristr($keywords, "*") === false){
            if (strlen($keywords) >= 10)
               $keywords = ltrim($keywords, 0);
               
            $sphinx_keyword = "*" . $keywords . "*";
         }else{
            $keywords = ltrim($keywords, 0);
            if ($spot == 0){
               $sphinx_keyword = "" . $keywords . "\$";
            }elseif ($spot == ($slen - 1)){
               if (($slen - 1) <= 3) $this->sphinx->SetFilter("sim_dausoid", array($iDauSo));
               $sphinx_keyword = "" . $keywords . "";
            }else{
               $sphinx_keyword = "*" . $keywords . "\$";
            }
         }
      }
      
      $this->keyword = $sphinx_keyword;

      // Select
      $this->sphinx->SetSelect("*");
      
      // Filter
      $this->sphinx->SetFilterFloatRange("sim_price", doubleval(10000), doubleval(900000000000));

      if($module == 'searchsim' || $module == 'simajax' || $module == 'simhoptuoi' || $module == 'simhopmenh'){
         
         if($iNguHanh != 0){
            $this->sphinx->SetFilter("sim_active", array(1) );
            $this->sphinx->SetFilter("sim_web", array(1) );
         }else{
            $this->sphinx->SetFilter("sim_active", array(1) );
            $this->sphinx->SetFilter("sim_web", array(1) );
         }  
      }else if($module == 'home' || $module == 'simhot'){
         $this->sphinx->SetFilter("sim_active", array(1) );
         $this->sphinx->SetFilter("sim_web", array(1) );
      }else{
         $this->sphinx->SetFilter("sim_active", array(1) );
         //$this->sphinx->SetFilter("sim_web", array(1) );
      }

      $this->sphinx->SetFilter("id", array(326059059) );
      
      // Category
      if($iCat != 0) $this->sphinx->SetFilter("sim_category", array($iCat) );
      
      // Type
      if($iType != 0) $this->sphinx->SetFilter("sim_typeid", array($iType) );
      
      // Price
      if($iPrice != 0) $this->sphinx->SetFilter("sim_priceid", array($iPrice) );
      
      // Dau so
      if($iDauSo != 0) $this->sphinx->SetFilter("sim_dausoid", array($iDauSo) );
      
      $iDaiLy = getValue('iDaiLy', 'int', 'GET', 0);
      if($iDaiLy > 0) $this->sphinx->SetFilter("sim_dailyid", array($iDaiLy) );

      // Dua ra sim 10 so hay 11 so
      if($digit == 1) $this->sphinx->SetFilter("sim_digit_1", array(1) );
      elseif($digit == 2) $this->sphinx->SetFilter("sim_digit_2", array(1) ); 
      
      // Dua ra nhung sim hop ngu hanh sinh khac
      if($iNguHanh != 0){
         $this->sphinx->SetFilter("sim_nguhanh", array($iNguHanhSinh) );
         
         $this->sphinx->SetFilterRange("sim_diem_stst", 7, 10);
         $this->sphinx->SetFilterRange("sim_diem_vietaa", 6, 10);
         $this->sphinx->SetFilterRange("sim_sonut", 6, 10);
      } // End if($iNguHanh != 0)

      /* Sort */
      #echo $sort;      
      if ($sort == 0) $this->sphinx->SetSortMode(SPH_SORT_EXTENDED, "sim_web DESC, sim_price ASC");
      elseif ($sort == 'random') $this->sphinx->SetSortMode(SPH_SORT_EXTENDED, "@random ASC");
      else $this->sphinx->SetSortMode(SPH_SORT_EXTENDED, "sim_web DESC, sim_price DESC");
      
      // Ngu hanh
      if($iNguHanh != 0) $this->sphinx->SetSortMode(SPH_SORT_ATTR_DESC, "sim_diem_stst");
      
      // Limit
      $this->sphinx->SetLimits($page_start, $page_end);
      // var_dump($this->keyword);
      $querySearch = $this->keyword;

      // var_dump($querySearch . " /2");
      // var_dump($this->sphinx_index);
      // Query
      $result     = $this->sphinx->Query($querySearch, $this->sphinx_index);
      var_dump($this->sphinx->GetLastError());
      // echo '<xmp>'; var_dump($result); echo '</xmp>';
      
      if( $result === false ) $this->saveErrorLog("Query failed: " . $this->sphinx->GetLastError() . ".\n");
      else{
         if ($this->sphinx->GetLastWarning()) $this->saveErrorLog("WARNING: " . $this->sphinx->GetLastWarning() . "");
         
         $arrayReturn = $result['matches'];
         
         //$list_sim_id = "0";
         foreach($arrayReturn as $k => $v){

            $sim_sim2 = $v["id"];
            //$list_sim_id .= "," . $sim_sim2;
            
            $arrQuery[$sim_sim2]["sim_sim2"] = $sim_sim2;
            foreach ($v["attrs"] as $ks => $vs) $arrQuery[$sim_sim2][$ks] = $vs;
         }
         
         /*/
         $db_sim = new db_query("SELECT sim_sim1, sim_sim2, sim_price 
                                 FROM tbl_sim 
                                 WHERE sim_sim2 IN (" . $list_sim_id . ")"
                                 );
         while($row = mysql_fetch_assoc($db_sim->result)) {
            $arrQuery[$row["sim_sim2"]]["sim_sim1"]   = $row["sim_sim1"];
            $arrQuery[$row["sim_sim2"]]["sim_price"]  = $row["sim_price"];
         }
         unset($db_sim);
         //*/
         
         if($result['total_found'] > $this->limit) $result['total_found'] = $this->limit;
         
         $arrayReturn['status']        = $result['success'];      
         $arrayReturn['total_record']  = $result['total_found'];
         $arrayReturn['query']         = $arrQuery;
      } // End if( $result === false )

      // Close
      $this->sphinx->Close();
      
      // Return
      return json_encode($arrayReturn);
      
   } // End function search

   function saveErrorLog($log_message){
      $filename = "../../store/logs/sphinx_search.cfn";
      $handle = @fopen($filename, 'a');
      
      //Nếu handle chưa có mở thêm ../
      if (!$handle) $handle = @fopen("../" . $filename, 'a');
      
      //Nếu ko mở đc lần 2 thì return luôn
      if (!$handle) return;
      
      fwrite($handle, date("d/m/Y h:i:s A") . " " . $_SERVER['REMOTE_ADDR'] . " " . $_SERVER['SCRIPT_NAME'] . "?" . @$_SERVER['QUERY_STRING'] . "\n" . $log_message . "\n");
      fclose($handle);  
   } // End function saveErrorLog
      
} // End class sphinxSearch
?>