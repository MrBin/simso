<?
/*
class generate form
Created by FinalStyle.com (dinhtoan1905)
*/
class generate_oto{
	
	//khai bao array thong so co ban
	var $table 		 			= "tbl_oto";
	var $arrayFeildreturn 	= array();
	
	
	var $arrayTTCB  = array(
							"xuat_xu"				=>array(1,array(1=>"Xe trong nước"
																			  ,2=>"Xe nhập khẩu"
																				))
						   ,"tinh_trang"			=>array(1,array(1=>"Mới"
												  					  ,2=>"Đã sử dụng"
												  						))
						   ,"dong_xe"				=>array(1,array(1=>"Sedan"
																			,2=>"SUV"									
																			,3=>"Coupe"
																			,4=>"Crossover"								
																			,5=>"Hatchback"
																			,6=>"Convertible/Cabriolet"
																			,7=>"Truck"
																			,8=>"Van/Minivan"										
																			,9=>"Wagon"
																			))
																			
						   ,"so_km"					=>array(0,array())
						   ,"mau_ngoai_that"		=>array(1,array(1=>"Bạc"
																				,2=>"Cát" 
																				,3=>"Ghi" 
																				,4=>"Hai màu" 
																				,5=>"Hồng" 					
																				,6=>"Nâu" 									
																				,7=>"Tím" 
																				,8=>"Trắng" 
																				,9=>"Vàng" 
																				,10=>"Xanh" 
																				,11=>"Xám" 
																				,12=>"Đỏ" 
																				,13=>"Đồng" 
																				,14=>"Đen" 
																				,99=>"Nhiều màu" 
																				,100=>"Màu khác"								
																			))
						   ,"mau_noi_that"		=>array(1,array(1=>"Bạc"
																				,2=>"Cát" 
																				,3=>"Ghi" 
																				,4=>"Hai màu" 
																				,5=>"Hồng" 					
																				,6=>"Nâu" 									
																				,7=>"Tím" 
																				,8=>"Trắng" 
																				,9=>"Vàng" 
																				,10=>"Xanh" 
																				,11=>"Xám" 
																				,12=>"Đỏ" 
																				,13=>"Đồng" 
																				,14=>"Đen" 
																				,99=>"Nhiều màu" 
																				,100=>"Màu khác"								
																			))
							,"so_cua"					=>array(0,array())
							,"cho_ngoi"					=>array(0,array())																			
							,"nhien_lieu"				=>array(1,array(1=>"Xăng"
																				,2=>"Diesel"
																				,3=>"Hybrid"
																				,4=>"Điện"
																				,10=>"Loại khác"									
																			))				
							,"ht_nap_nhien_lieu"		=>array(0,array())
							,"hop_so"					=>array(1,array(1=>"Số tay"
																				,2=>"Số tự động"
																				,3=>"Số hỗn hợp"
																				))		
							,"dan_dong"					=>array(1,array(1=>"FWD - Dẫn động cầu trước"
																				,2=>"RFD - Dẫn động cầu sau"										
																				,3=>"4WD - Dẫn động 4 bánh"
																				,4=>"AWD - 4 bánh toàn thời gian"
																				,5=>"4WD hoặc AWD"))
							,"tieu_thu_nhien_lieu"	=>array(0,array())
							,"phan_hang_model"		=>array(0,array())	
							,"nam_sx"					=>array(1,array(2010=>"2010"
																				,2009=>"2009"
																				,2008=>"2008"
																				,2007=>"2007"
																				,2006=>"2006"
																				,2005=>"2005"
																				,2004=>"2004"
																				,2003=>"2003"
																				,2002=>"2002"
																				,1999=>"1999"
																				,1998=>"1998"
																				,1997=>"1997"
																				,1996=>"1996"
																				,1995=>"1995"
																				,1994=>"1994"
																				,1993=>"1993"
																				,1992=>"1992"
																				,1991=>"1991"
																				,1990=>"1990"
																				,1960=>"Trước 1990"
																				))																																											
						 	 ); 
							 
	var $arrayAntoan  = array(
										"tui_khi"				=>array(1=>"Túi khí cho người lái"
																			 ,2=>"Túi khí cho hành khách phía trước"
																			 ,4=>"Túi khí cho hành khách phía sau"
																			 ,8=>"Túi khí hai bên hàng ghế"
																			 ,16=>"Túi khí treo phía trên hai hàng ghế trước và sau"
																				)
										,"phanh_dieu_khien"	=>array(1=>"Chống bó cứng phanh (ABS)"
																			 ,2=>"Phân bố lực phanh điện tử (EBD)"
																			 ,4=>"Trợ lực phanh khẩn cấp (EBA)"
																			 ,8=>"Tự động cân bằng điện tử (ESP)"
																			 ,16=>"Điều khiển hành trình"
																			 ,32=>"Hỗ trợ cảnh báo lùi "
																			 ,64=>"Hệ thống kiểm soát trượt"
																			 )
										,"khoa_chong_trom"	=>array(1=>"Chốt cửa an toàn"
																			 ,2=>"Khóa cửa tự động"
																			 ,4=>"Khóa cửa điện điều khiển từ xa"
																			 ,8=>"Khóa động cơ"
																			 ,16=>"Hệ thống báo trộm ngoại vi"
																			 )
										,"thong_so_khac"		=>array(1=>"Đèn sương mù"
																			 ,2=>"Đèn cảnh báo thắt dây an toàn"
																			 ,4=>"Đèn phanh phụ thứ 3 lắp cao"
																			 )
										);
										
	var $arrayTiennghi  = array(
										"thiet_bi_tieu_chuan"=>array(1=>array(0,"Thiết bị định vị")
																			 ,2=>array(0,"Cửa sổ nóc")
																			 ,4=>array(0,"Kính chỉnh điện")
																			 ,8=>array(0,"Tay lái trợ lực")
																			 ,16=>array("tn_chatlieu","Ghế:Chất liệu,tiện nghi..")
																			 ,32=>array(0,"Điều hòa trước")
																			 ,64=>array(0,"Điều hòa sau")
																			 ,128=>array(0,"Hỗ trợ xe tự động")
																			 ,256=>array(0,"Xấy kính sau")
																			 ,512=>array(0,"Quạt kính phía sau")
																			 ,1024=>array(0,"Kính mầu")
																			 ,2048=>array(0,"Màn hình LCD")
																			 ,4096=>array("tn_thietbi","Thiết bị giải trí Audio, Video")
																				)
										);
 var 	$arrayThongsokt  = array(
										"kich_thuoc_tl" => array(
																		 "Dài x Rộng x Cao (mm)"
																		,"Chiều dài cơ sở"
																		,"Chiều rộng cơ sở trước/sau (mm)"
																		,"Trọng lượng không tải (kg)"
																		,"Dung tích bình nhiên liệu (lít)"
																		)
									 ,"dong_co"		 => array(
																		 "Động cơ"
																		,"Kiểu động cơ"
																		,"Dung tích xi lanh (cc)"
																		)
									 ,"phanh_giam_xoc" => array(
																		 "Phanh"
																		,"Giảm xóc"
																		,"Lốp xe"
																		,"Vành mâm xe"
																		)
										);		
										
										
	//bắt đầu tạo form và hiển thị
	function formTTCB($key,$valuedefault='',$titleselect='',$width='',$onchange = ''){
		$str = '';
		if(isset($this->arrayTTCB[$key])){
			
			
			if($key == 'nam_sx')
			{
				$year = date("Y");
				for($i=$year+1; $i>=1990; $i--){
					$keys[] = $i;
					$values[] = $i;
				}
				
				$keys[] = '1960';
				$values[] = 'Trước 1990';

				$this->arrayTTCB[$key] = array(1, array_combine($keys, $values));
			}
			
			
			if($this->arrayTTCB[$key][0]==1){
				$str .= '<select name="oto_' . $key . '" id="oto_' . $key . '"' . $onchange . ' class="form">';
				$str .= '<option value="">' . $titleselect . '</option>';
				foreach($this->arrayTTCB[$key][1] as $value=>$title){
					
					$str .= '<option value="' . $value . '" ' . (($valuedefault==$value) ? 'selected' : '') . '>' . $title . '</option>';
					
				}
				$str .= '</select>';
			}else{
				$str .= '<input type="text" name="oto_' . $key . '" id="oto_' . $key . '" style="width:' . $width . 'px" value="' . $valuedefault . '" class="form">';
			}
			
			
		}
		return $str;

	}//end function
	function formTTCBdetail($key,$row){
		$str = '';
		if(isset($this->arrayTTCB[$key])){
			$value =  $row["oto_" . $key];
			$array =  $this->arrayTTCB[$key][1];
			if($this->arrayTTCB[$key][0]==1){
				$str = isset($array[$value]) ? $array[$value] : '';
			}else{
				$str = $row["oto_" . $key];
			}
			
		}
		//print_r($this->arrayTTCB[$key][1]);
		//$str = isset($this->arrayTTCB[$key][1][$row[$key]]) ? $this->arrayTTCB[$key][1][$row[$key]] : '';
		return $str;
	}
	//bắt đầu tạo form và hiển thị
	function formAntoan($key,$valuedefault='',$titleselect='',$width=''){
				$str = '';
		if(isset($this->arrayAntoan[$key])){
				$i=0;
				foreach($this->arrayAntoan[$key] as $value=>$title){
					$i++;
					
					$str .= '<tr ' . (($i%2==0) ? ' bgcolor="#f8f8f8"' : '') . '><td width="50%">' . $title . '</td><td>';
					$str .= '<input type="checkbox" ' . ((($valuedefault & $value) != 0) ? 'checked' : '') . ' name="oto_' . $key . '[]" id="oto_' . $key . '" value="' . $value . '" /></td></tr>';
					
				}
			
			
		}
		return $str;
	}//end function
	
	function formAntoandetail($key,$valuedefault){
		$str = '';
		if(isset($this->arrayAntoan[$key])){
				$i=0;
				foreach($this->arrayAntoan[$key] as $value=>$title){
					$i++;
					
					$str .= '<tr ' . (($i%2==0) ? ' bgcolor="#f8f8f8"' : '') . '><td width="50%">' . $title . '</td><td>';
					$str .= '<img src="/lib/img/check_' . ((($valuedefault & $value) != 0) ? '1' : '0') . '.gif" border="0"></td></tr>';
					
				}
			
			
		}
		return $str;
	}
	
	function formTiennghi($key,$valuedefault='',$titleselect='',$width=''){
				$str = '';
		if(isset($this->arrayTiennghi[$key])){
				$i=0;
				foreach($this->arrayTiennghi[$key] as $value=>$row){
					$i++;
					if($row[0] === 0){
						$str .= '<tr ' . (($i%2==0) ? ' bgcolor="#f8f8f8"' : '') . '><td width="50%">' . $row[1] . '</td><td>';
						$str .= '<input type="checkbox" name="oto_' . $key . '[]" id="oto_' . $key . '" ' . ((($valuedefault & $value) != 0) ? 'checked' : '') . ' value="' . $value . '" /></td></tr>';
					}else{
						$temp = 'oto_' . $row[0];
						global $$temp;
						$str .= '<tr ' . (($i%2==0) ? ' bgcolor="#f8f8f8"' : '') . '><td width="50%">' . $row[1] . '</td><td>';
						$str .= '<input type="text" name="oto_' . $row[0] . '" id="oto_' . $row[0] . '" value="' . $$temp . '" class="form" style="background:#FFFF99" /></td></tr>';						
					}
				}
			
			
		}
		return $str;
	}//end function
	
	function formTiennghidetail($key,$rowd){
				$str 				= '';
				$valuedefault	=	$rowd['oto_' . $key];
		if(isset($this->arrayTiennghi[$key])){
				$i=0;
				foreach($this->arrayTiennghi[$key] as $value=>$row){
					$i++;
					if($row[0] === 0){
						$str .= '<tr ' . (($i%2==0) ? ' bgcolor="#f8f8f8"' : '') . '><td width="50%">' . $row[1] . '</td><td>';
						$str .= '<img src="/lib/img/check_' . ((($valuedefault & $value) != 0) ? '1' : '0') . '.gif" border="0"></td></tr>';
					}else{
						$temp = 'oto_' . $row[0];
						global $$temp;
						$str .= '<tr ' . (($i%2==0) ? ' bgcolor="#f8f8f8"' : '') . '><td width="50%">' . $row[1] . '</td><td>';
						$str .= $rowd[$temp] .'</td></tr>';						
					}
				}
			
			
		}
		return $str;
	}//end function
		
	function formTSKT($key,$valuedefault='',$titleselect='',$width=''){
				$str = '';
		if(isset($this->arrayThongsokt[$key])){
				$i=0;
				foreach($this->arrayThongsokt[$key] as $i=>$title){
					$temp = 'oto_' . $key  . $i;
					global $$temp;
					$str .= '<tr ' . (($i%2==0) ? ' bgcolor="#f8f8f8"' : '') . '><td width="50%">' . $title . '</td><td>';
					$str .= '<input type="text" name="oto_' . $key  . $i . '" id="oto_' . $key  . $i . '" value="' . $$temp . '" class="form" style="background:#FFFF99" /></td></tr>';						
					$i++;
				}
			
			
		}
		return $str;
	}//end function
	
	function formTSKTdetail($key,$row){
				$str = '';
		if(isset($this->arrayThongsokt[$key])){
				$i=0;
				foreach($this->arrayThongsokt[$key] as $i=>$title){
					$temp = 'oto_' . $key  . $i;
					$str .= '<tr ' . (($i%2==0) ? ' bgcolor="#f8f8f8"' : '') . '><td width="50%">' . $title . '</td><td>';
					$str .= $row[$temp] . '</td></tr>';	
					$i++;
				}
			
			
		}
		return $str;
	}//end function\
	
		//tạo lệnh sql tạo trường
	function generate_feild(){
		$sql = '';
		//add trường phần thông tin cơ bản
		foreach($this->arrayTTCB as $key=>$value){
			$this->arrayFeildreturn['oto_' . $key . ''] = '';			
		}
		
		//add trường phần an toàn
		
		foreach($this->arrayAntoan as $key=>$value){
			$this->arrayFeildreturn['oto_' . $key . ''] = '';			
		}
		
		//add trường phần tiện nghi
		foreach($this->arrayTiennghi as $key=>$value){
			$this->arrayFeildreturn['oto_' . $key . ''] = '';
			foreach($value as $i=>$row){
				if($row[0] !== 0){
					$this->arrayFeildreturn['oto_' . $key . ''] = '';
				}
			}
		}
		
		//add trường phần tiện nghi
		foreach($this->arrayThongsokt as $key=>$value){
			foreach($value as $i=>$feild){
				$this->arrayFeildreturn['oto_' . $key . ''] = '';
			}
			
		}
		
		return $this->arrayFeildreturn;
		
	}																
	
	
	//tạo lệnh sql tạo trường
	function generate_sql(){
		$sql = '';
		//add trường phần thông tin cơ bản
		foreach($this->arrayTTCB as $key=>$value){
			if($value[0]==0){
				$sql = 'ALTER TABLE `' . $this->table . '` ADD `oto_' . $key . '` VARCHAR( 255 ) NULL';
			}else{
				$sql = 'ALTER TABLE `' . $this->table . '` ADD `oto_' . $key . '` INT( 11 ) NULL DEFAULT \'0\'';
			}
			$db_ex = new db_execute($sql);
			unset($db_ex);
			$db_ex = new db_execute('INSERT INTO `tbl_oto_required` ( `req_name`,`req_table` ) VALUES (\'oto_' . $key . '\',\'oto\')');
			unset($db_ex);
		}
		
		//add trường phần an toàn
		
		foreach($this->arrayAntoan as $key=>$value){
			$sql = 'ALTER TABLE `' . $this->table . '` ADD `oto_' . $key . '` INT( 11 ) NULL DEFAULT \'0\'';
			$db_ex = new db_execute($sql);
			unset($db_ex);
			$db_ex = new db_execute('INSERT INTO `tbl_oto_required` ( `req_name`,`req_table` ) VALUES (\'oto_' . $key . '\',\'oto\')');
			unset($db_ex);
		}
		
		//add trường phần tiện nghi
		foreach($this->arrayTiennghi as $key=>$value){
			$sql = 'ALTER TABLE `' . $this->table . '` ADD `oto_' . $key . '` INT( 11 ) NULL DEFAULT \'0\'';
			$db_ex = new db_execute($sql);
			unset($db_ex);
			$db_ex = new db_execute('INSERT INTO `tbl_oto_required` ( `req_name`,`req_table` ) VALUES (\'oto_' . $key . '\',\'oto\')');
			unset($db_ex);
			foreach($value as $i=>$row){
				if($row[0] !== 0){
					$sql = 'ALTER TABLE `' . $this->table . '` ADD `oto_' . $row[0] . '` INT( 11 ) NULL DEFAULT \'0\'';
					$db_ex = new db_execute($sql);
					unset($db_ex);
					$db_ex = new db_execute('INSERT INTO `tbl_oto_required` ( `req_name`,`req_table` ) VALUES (\'oto_' . $row[0] . '\',\'oto\')');
					unset($db_ex);
				}
			}
		}
		
		//add trường phần tiện nghi
		foreach($this->arrayThongsokt as $key=>$value){
			foreach($value as $i=>$feild){
				$sql = 'ALTER TABLE `' . $this->table . '` ADD `oto_' . $key . $i . '` VARCHAR( 255 ) NULL';
				$db_ex = new db_execute($sql);
				unset($db_ex);
				$db_ex = new db_execute('INSERT INTO `tbl_oto_required` ( `req_name`,`req_table` ) VALUES (\'oto_' . $key . $i . '\',\'oto\')');
				unset($db_ex);
			}
			
		}
		
		
	}																
}
?>