<?
# defined('_RIGHT_ACCESS') or die('No direct access');
class _temp extends menu{

	#+
	#+ Template News Body
	public static function _newsBody($row,$i){
		$lin_cat = createLink("detail_news",$row);
		#+
		$str  = '';
		$str .= '
				<li>
					<article>
						<a href="'.$lin_cat.'" title="'.trim($row["new_title"]).'">'.$row["new_title"].'</a>
					</article>
				</li>
				';
		return $str;
	}

	#+
	#+ Template News Home
	public static function _newsHome($arr = array()){
		global $lang_id;
		global $menuid;

		#+
		#+ Thuc hien query lay category
		$query = " SELECT * FROM tbl_category"
				." WHERE cat_type = 'tin-tuc' AND cat_active = 1 AND cat_parent_id = 0"
				." ORDER BY cat_order LIMIT 0,6"
				;
		$db_cat = new db_query($query);
		
		$str = '';
		
		$i = 0;
		while($cat = mysql_fetch_assoc($db_cat->result)){
			$i++;
			
			$link_cat = createLink("cat",$cat);
		
			$str .= '
					<section>
					<a href="'.$link_cat.'" title="'.$cat["cat_name"].'" class="cat">'.$cat["cat_name"].'</a> <br />
					
					<p class="pull-left" style="margin-right:10px;">
						<a href="'.$link_cat.'" title="'.$cat["cat_name"].'">
							<img class="img-thumbnail" src="/store/media/category/m_' . $cat["cat_picture"].'" width="133" height="100" alt="'.$cat["cat_name"].'" />
						</a>
					</p>
					
					<ul>
					';
	
					#+
					#+
					$sqlcategoryNews	= "";
					$listiCatNews		= $menuid->getAllChildId($cat["cat_id"]);
					$sqlcategoryNews	= " AND cat_id IN(" . $listiCatNews  . ")";
								
					$limitNews = 3;
					if($module == "home"){
						$limitNews = 3;
					}
					
					#+
					#+ Thuc hien query lay tin tuc
					$query = " SELECT *"
							." FROM tbl_news"
							." INNER JOIN tbl_category ON(cat_id = new_category)"
							." WHERE new_active = 1".$sqlcategoryNews
							." ORDER BY new_date DESC, new_id DESC"
							." LIMIT 0, " . $limitNews
							;
					$db_query = new db_query($query);
					while($row = mysql_fetch_assoc($db_query->result)){
						
						$lin_detail = createLink("detail_news",$row);
			
						$str .= '
								<li>
									<article>
										<a href="'.$lin_detail.'" title="'.replaceQ($row["new_title"]).'">'.$row["new_title"].'</a><br />
									</article>
								</li>
								'
								;
					} // End while($row = mysql_fetch_assoc($db_news->result))
		
			$str .= '
					</ul>
					<div class="clearfix"></div>
					</section>
					';
		} // End while($cat = mysql_fetch_assoc($db_catNews->result))
		
		#+
		#+ Huy bien
		#+
		unset($db_query);
		#+
		$db_cat->close();
		unset($db_cat);
		
		#+
		return $str;
	}
	
	
	#+
	#+ Template Don hang
	public static function _donHang($arr = array()){
		global $lang_id;

		#+
		#+ Xu ly du lieu
		$query = "SELECT MAX(id) FROM tbl_yeucau";
		$db_query = new db_query($query);
		$maxOrdYc 		= mysql_fetch_assoc($db_query->result);
		$maxOrdYc 		= $maxOrdYc["MAX(id)"] - 24;
		#+
		$query = " SELECT * FROM tbl_yeucau"
				." WHERE id > " . $maxOrdYc
				." ORDER BY tinhtrang, phanloai, thoigian DESC, id DESC"
				." LIMIT 0,10"
				;
		$db_query = new db_query($query);
		
		$str = '';
		$i = 0;
		while($row = mysql_fetch_assoc($db_query->result)){
		$i++;	
			$tinhtrang = '';
			if($row["tinhtrang"] == 0){
				$tinhtrang = ' text-danger';
			} // End if($row["tinhtrang"] == 0){
			
		
			$str .= '<li class="col-md-6">';
			
			if($row["tinhtrang"] == 0){
				$str .= '<img src="/lib/img/icon/icon-donhangmoi1.jpg" alt="Đơn hàng 1" />&nbsp;';
			}else{	
				$str .= '<img src="/lib/img/icon/icon-donhangmoi2.jpg" align="Đơn hàng 2" />&nbsp;';
			} // End if($row["tinhtrang"] == 0){
			
			$str .= '
					<span class="text-bold'.$tinhtrang.'">
						'.$row["hoten"].'
					</span>
					<br />
					';
			switch($row["phanloai"]){
				case 1:
					$str .= 'Đặt sim';
				break;
				case 2:
					$str .= 'Yêu cầu';
				break;
				case 3:
					$str .= 'Bán sim';
				break;
				case 4:
					$str .= 'Đăng ký';
				break;
			} // End switch($row["phanloai"]){
			
			$str .= '
					:
					'.$row["simcard"].'
					<br />
					
					<span class="text-muted">Thời gian : '.date("d/m H:i",strtotime($row["thoigian"])).'</span>
					';
			
			$str .= '</li>';
	
		}
		
		#+
		#+ Huy bien
		$db_query->close();
		unset($db_query);
		
		#+
		return $str;
	}

	#+
	#+ Template Sim Body
	public static function _simBody($row,$i){
		global $module;
		global $arrSimCat;
		global $arrSimType;
		global $iNamSinh;
		global $iNguHanh;
		global $sNguHanh;
		global $arrBanMenh;
		global $arrNguHanh;
		global $arrNguHanh1;
		
		#+
		$sim_sim1 		= $row['sim_sim1'];
		$sim_sim2 		= '0'.$row['sim_sim2'];
		$sim_price		= $row['sim_price'];
		$sim_price		= number_format($sim_price);
		$sim_category 	= $row['sim_category'];
		$sim_typeid 	= $row['sim_typeid'];
		#+
		$cat_name		= $arrSimCat[$sim_category]["cat_name"];
		$cat_picture	= $arrSimCat[$sim_category]["cat_picture"];
		#+
		$simtp_name		= $arrSimType[$sim_typeid]["simtp_name"];
		#+
		$sim_nguhanh 	= $row['sim_nguhanh'];
		$sim_sonut		= $row['sim_sonut'];
		$sim_diem_stst	= $row['sim_diem_stst'];
		
		#+
		#+ Tao link
		$row['cat_name_index'] = $arrSimCat[$sim_category]["cat_name_index"];
		$row['simtp_index'] = $arrSimType[$sim_typeid]["simtp_index"];

		if($module == 'simtype'){
			$lin_detail = createLink("detail_simtype",$row);
		}else{
			$lin_detail = createLink("detail_sim",$row);	
		}

		$str  = '';
		$str .= '
					
				<tr>
					<td class="active text-center">
						<strong>'.$i.'</strong>
					</td>
					<td>
						<a class="sim-digit" href="'.$lin_detail.'" title="'.$cat_name. ' ' . $sim_sim1.'"><strong>'.$sim_sim1.'</strong></a>
					</td>
					<td>
						<span class="sim-price text-success">'.$sim_price.'</span>
					</td>
					<td>
						<img src="/store/media/category/'.$cat_picture.'" alt="'.$cat_name.'" />&nbsp;
					</td>
				';
				
		if($iNguHanh != 0){
		#+
		$str .= '
					<td>
						<form action="/sim-phong-thuy.html" method="POST">
							<input type="hidden" name="sodienthoai" value="'.$sim_sim2.'">
							<input type="hidden" name="namsinh" value="'.$iNamSinh.'">
							<button style="margin:0; padding:0px 10px" data-toggle="tooltip" data-placement="right" data-original-title="Điểm phong thủy : <strong>'.$sim_diem_stst.'/10 điểm</strong>" id="sht_'.$sim_sim2.'" class="btn btn-warning">Xem điểm</button>
						</form>
					</td>
					
				';		
		}else{
		$str .= '
					<td>
						'.$simtp_name.'
					</td>
				</tr>
				';	
		} // End if($module == 'simhoptoi' || $module == 'simhopmenh')

		return $str;	
	}
	
	#+
	#+ Template Sim Head
	public static function _simHead(){
		$str = '';
		$str .= '
				<thead class="bg-primary">	
					<tr>
						<th>
							Stt
						</th>
						<th>
							Số sim
							<sup><a href="javascript:;" onclick="search10so()" class="text-yellow">10 số</a> /</sup>
							<sup><a href="javascript:;" onclick="search11so()" class="text-yellow">11 số</a></sup>
						</th>
						<th>
							Giá bán
							<sup><a href="javascript:;" onclick="submit_form(\'&sort=0\')" class="text-yellow">Số rẻ</a> /</sup>
							<sup><a href="javascript:;" onclick="submit_form(\'&sort=1\')" class="text-yellow">Số vip</a></sup>
						</th>
						<th>
							Mạng
						</th>
						<th>
							Phân loại sim
						</th>
					</tr>
				</thead>
				';
				
		return $str;	
	}
	
	#+
	#+ Query
	public static function _checkSimDauSo(){
		$query = " SELECT sds_id,sds_category,sds_name,sds_name,sds_home"
				." FROM tbl_simdauso"
				." WHERE sds_active = 1"
				." ORDER BY sds_id DESC"
				;
		$arrQuery = getArray($query,'sds_id');
		
		return $arrQuery;
	}
	
	#+
	#+ Query
	public static function _checkSimPrice(){
		$query = " SELECT pri_id,pri_name"
				." FROM tbl_simprice"
				." WHERE pri_active = 1"
				." ORDER BY pri_order, pri_id DESC"
				;
		$arrQuery = getArray($query,'pri_id');
		
		return $arrQuery;
	}
	
	#+
	#+ Query
	public static function _checkSimType(){
		$query = " SELECT simtp_id,simtp_name,simtp_index"
				." FROM tbl_simtype"
				." ORDER BY simtp_order, simtp_id DESC"
				;
		$arrQuery = getArray($query,'simtp_id');
		
		return $arrQuery;
	}
	
	#+
	#+ Query
	public static function _checkSimCat(){
		$query = " SELECT cat_id,cat_name,cat_name_index,cat_picture"
				." FROM tbl_category"
				." WHERE cat_active = 1 AND cat_type = 'sim'"
				." ORDER BY cat_order, cat_id DESC"
				;
		$arrQuery = getArray($query,'cat_id');
		
		return $arrQuery;
	}
	
	#+
	#+ Template Sim dau so
	public static function _simDauSo($arr = array()){
		global $lang_id;
		global $arrSimCat;
	
		#+
		#+ Xu ly du lieu
		$query = " SELECT *"
				." FROM tbl_simdauso"
				." WHERE sds_active = 1 AND sds_home = 1"
				." ORDER BY sds_category ASC, sds_order, sds_id ASC"
				;
		$db_query = new db_query($query);
		
		#+
		#+ Dua ra cache
		$str = '';
	
		while($row = mysql_fetch_assoc($db_query->result)){
			$row['cat_name_index'] = $arrSimCat[$row['sds_category']]['cat_name_index'];

			$link_dauso = createLink("simdauso",$row);
			
			$str .= '
					<li class="col-md-4">
						<a href="'.$link_dauso.'" title="Sim số đẹp '.$row['sds_name'].'" data-pjax="#main">Sim số đẹp '.$row['sds_name'].'</a>
					</li>
					';
			
		} // End while($row = mysql_fetch_assoc($db_simDauSo->result))
		
		unset($dauso);
		$db_query->close();
		unset($db_query);
		
		#+
		return $str;
	}
	
	#+
	#+ Template Sim Type
	public static function _simType($arr = array()){
		global $lang_id;

		#+
		#+ Thuc hien query
		$query = " SELECT simtp_id,simtp_name,simtp_index,simtp_description"
				." FROM tbl_simtype"
				." WHERE simtp_active = 1 AND simtp_home = 1"
				." ORDER BY simtp_order DESC, simtp_id DESC"
				;
		$db_query = new db_query($query);
		
		$str = '';
		$i=0;
		while($row = mysql_fetch_assoc($db_query->result)){
			$i++;
			$link_cat = createLink("simtype",$row);
			
			if($row['simtp_description'] != '')
				$str .= '
						<li class="col-md-4">
							<a href="'.$link_cat.'" title="" data-pjax="#main" data-toggle="tooltip" data-placement="top" data-original-title="'.$row['simtp_description'].'">
								'.$row["simtp_name"].'
							</a>
						</li>
						';
			else
				$str .= '
						<li class="col-md-4">
							<a href="'.$link_cat.'" title="'.$row["simtp_name"].'">'.$row["simtp_name"].'</a>
						</li>
						';
		
		}// End while($row = mysql_fetch_assoc($db_simType->result))
	
		$db_query->close();
		unset($db_query);
		
		#+
		return $str;
	}
	
	#+
	#+ Template Menu Top
	public static function _menuTop($arr = array()){
		global $lang_id;
		global $path_menu;	
		global $module;	
		global $iCat;	
		global $sCat;	
		global $iData;	
		global $sData;	
		
		#+
		#+ Cache
		$query = " SELECT mnu_id,mnu_name,mnu_target,mnu_link,mnu_check "
				." FROM tbl_menus "
				." WHERE lang_id = ". $lang_id ." AND mnu_parent_id = 0 AND mnu_type = 1 AND mnu_active = 1 "
				." ORDER BY mnu_order, mnu_name ASC "
				;
		$db_query = new db_query($query);

		#+
		#+ Dua ra du lieu
		$str  = '';
		$i = 0;
		$url = getURL(1,1,1,1);
		$url .= $module != '' ? '&iCat='.$module : '';
		$url .= $iCat != 0 ? '&iCat='.$iCat : '';
		$url .= $sCat != '' ? '&sCat='.$sCat : '';
		$url .= $iData != 0 ? '&iData='.$iData : '';
		$url .= $sData != '' ? '&sData='.$sData : '';
		
		$current_id = 0;

		$i=0;
		while($row = mysql_fetch_assoc($db_query->result)){
			$i++;
			#+
			if(@preg_match($row["mnu_check"], $url)){
				$current_id = $row["mnu_id"];	
			} // End if(@preg_match($row["mnu_check"], $url)){
	
			$select = $row["mnu_id"] == $current_id ? 'active' : '';
			
			#+
			$str .= '
					<li class="'.$select.'">
						<a href="'.$row["mnu_link"].'" target="'.$row["mnu_target"].'" title="'.$row["mnu_name"].'" data-pjax="body" data-ajax="#main">
							'.$row["mnu_name"].'
						</a>
					</li>
					';
		}// End while($row = mysql_fetch_assoc($db_menu_top->result))
		
		#+
		$db_query->close();
		unset($db_query);	
		
		#+
		return $str;
	}


} // End class _temp
?>
