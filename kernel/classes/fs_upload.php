<?
/*
class upload
Developed by FinalStyle.com
*/
class upload{
	/*
	upload function
	upload_name : Ten textbox upload vi du : new_picture
	upload_path : duong dan save file upload
	extension_list : danh sach cac duoi mo rong duoc phep upload vi du : gif,jpg
	limt_size : dung luong gioi han (tinh bang byte) vi du : 20000 
	*/
	var $common_error	= "";
	var $warning_error	= "";
	var $file_name		= "";
	
	function upload($upload_name,$upload_path,$extension_list,$limit_size,$new_filename = ''){
		$limit_size=$limit_size*1024;
		if(!isset($_FILES[$upload_name]['tmp_name'])){
			return;
		}
		// kiem tra xem co upload ko
		if (!is_uploaded_file($_FILES[$upload_name]['tmp_name'])){
			$this->common_error = "Không tìm thấy file tmp_name, file upload tạm<br>";
			return;
		}
		// check file_size
		if (filesize($_FILES[$upload_name]['tmp_name']) > $limit_size){
			$this->common_error = "Dung lượng file lớn hơn giới hạn cho phép : " . number_format($limit_size,0,0,".") . " bytes<br>";
			$this->warning_error = $this->common_error;
			return;
		}
		//check upload extension
		if ($this->check_upload_extension($_FILES[$upload_name]['name'],$extension_list) != 1){
			$this->common_error = "Phần mở rộng của file không đúng, bạn chỉ upload được file có phần mở rộng là : " . $extension_list . "<br>";
			$this->warning_error = $this->common_error;
			return;
		}
		//generate new filename
		if($new_filename == ''){
			$new_filename = $this->generate_name($_FILES[$upload_name]['name']);
		 	$this->file_name = $new_filename;
		}else{
			$filename 			= $_FILES[$upload_name]['name'];
			$new_filename 		= $new_filename . '.' . substr($filename, (strrpos($filename, ".") + 1));
			$this->file_name 	= $new_filename;
		}
		//move upload file
		move_uploaded_file($_FILES[$upload_name]['tmp_name'],$upload_path . $new_filename);
	}
	
	function show_common_error(){
		return $this->common_error;
	}
	function show_warning_error(){
		return $this->warning_error;
	}
	
	/*
	Kiem tra phan mo rong
	*/
	function check_upload_extension($filename,$allow_list){
		
		$sExtension = substr( $filename, ( strrpos($filename, '.') + 1 ) ) ;
		$sExtension = strtolower( $sExtension ) ;
		
		$allow_arr = explode(",",$allow_list);
		$pass = 0;
		
		for ($i=0;$i<count($allow_arr);$i++){
			if ($sExtension == $allow_arr[$i]) $pass = 1;
		}
		return $pass;
	}
	
	
	/*
	Generate file name
	*/
	function generate_name($filename)
	{
		$name = "";
		for($i=0; $i<3; $i++){
			$name .= chr(rand(97,122));
		}
		$today= getdate();
		$name.= $today[0];
		$ext	= substr($filename, (strrpos($filename, ".") + 1));
		return $name . "." . $ext;
	}
	//them text vao anh
	function add_logo_image($sartpath,$endpath,$filename,$filelogo = "/lib/img/logo.png",$vitri="duoi_trai"){
		$insertLogo 	= 0;
		if(file_exists($filelogo)){
			
			$img_logo 	= imagecreatefrompng($filelogo);
			
			$sExtension = substr( $filename, ( strrpos($filename, '.') + 1 ) ) ;
			$sExtension = strtolower($sExtension);
			
			switch ($sExtension){
			case "jpg" :
				$image = imagecreatefromjpeg($sartpath . $filename);
				$insertLogo 	= 1;
				break;
			case "gif" :
				$image = imagecreatefromgif($sartpath . $filename);
				$insertLogo 	= 1;
				break;
			}
			
			if($insertLogo==1){
				
				switch($vitri){
					//duoi trai
					default:
						$dpy 	= imagesy($image)-(imagesy($img_logo));
						$dpx 	= imagesx($image)-(imagesx($img_logo));
						$sx 	= imagesx($img_logo);
						$sy 	= imagesy($img_logo);
					break;
				}
				
				@imagecopy($image, $img_logo, $dpx, $dpy, 0, 0, $sx, $sy);
				
				switch ($sExtension){
				case "jpg" :
					@imagejpeg($image, $endpath . $filename,100);
					break;
				case "gif" :
					@imagegif($image, $endpath . $filename,100);
					break;
				}
				imagedestroy($image);
			}
		}
	}	
}
?>