<?
class classified{
	/*
	loại tin
	*/
	var $arrayLoaiTin 	= array(1=>"Tin miễn phí"
										 ,2=>"Tin in đậm"
										 ,3=>"Tin đóng khung"
										 ,4=>"Tin VIP");
	/*
	loại tiền tệ
	*/
	var $arrayCurrency 	= array(1=>"VND"
										 ,2=>"USD"
										 ,3=>"SJC"
										 );									 
	/*
	Tạo array cấu trúc site
	*/
	var $site_structure = array(); //Kết thúc array
	var $arrayTypeName  = array(); //Array chứa tên các kiểu dữ liệu

	//những trường cần thiết chèn vào database giá trị sẻ lấy từ biến không thuộc array cấu trúc site
	var $arrayFieldDefault = array("classified_id"=>1);
	
	//check javascript luc edit va them moi
	var $strCheckJavascript = '';
	
	//tao form them moi san pham
	var $str_form_add_product = '';
	
	//ten bang duoc lua chon
	var $table_name = '';
	
	//update teaser
	var $str_teaser = '';
	
	//keu du lieu
	var $array_Type_data = array(
										 0=>"Kieu_text_box"
										,1=>"Kieu_combobox"
										,2=>"Kieu_multi_check"
										//,3=>"Kieu_color"
										,4=>"Kieu_check_box"
										//,3=>tt("Kieu_multi_check")
										);
	
	//gia tri kieu and bit
	var $arrayValBit = array(1=>1);
	/*
	---------------------Kết thúc khai báo biến------------------------------------
	*/
	/*
	Gọi array từ bảng định nghĩa
	*/
	function classified(){
		$valtemp = 1;
		for($i=1;$i<=30;$i++){
			$valtemp = $valtemp*2;
			$this->arrayValBit[$valtemp] = $valtemp;
		}
		//print_r($this->arrayValBit);
		unset($valtemp);
		$db_structure		  = new db_query("SELECT * 
														FROM defines_classified
														WHERE def_active=1
														ORDER BY def_order ASC
														");
		//*/
		while($row=mysql_fetch_assoc($db_structure->result)){
			$this->arrayTypeName["classified_" . $row["def_id"]] = $row["def_name"];
			$site_structure["classified_" . $row["def_id"]] = array();
			$db_field = new db_query("SELECT fie_name,fie_type,fie_id,fie_require
												FROM fields_classified 
												WHERE fie_define = " . $row["def_id"] ." ORDER BY fie_order ASC");
			while($sub=mysql_fetch_assoc($db_field->result)){
				$this->site_structure["classified_" . $row["def_id"]]["d" . $sub["fie_id"]] = $sub;
			}	
			unset($db_field);
			
		}													
		//*/
	}
	/*
	Show ra combo box để add danh sách giá trị, dùng trong add value (bảng list_value_classified)
	*/
	function show_combo_add_list_value(){
		$new_site_structure = $this->create_new_site_structure();

		$lv_level_1 = getValue("lv_level_1","str","POST","",1);
		/*
		Loop cấp 1
		*/
		echo '<tr><td class="textBold" align="right">Kiểu sản phẩm: </td><td><select name="lv_level_1" id="lv_level_1" onchange="document.add_list.submit()"  class="form">';
			echo '<option value="">--[Bạn hãy lựa chọn]--</option>';
		foreach ($new_site_structure as $m_key => $m_value){
		
			if ($lv_level_1 == $m_key) echo '<option value="' . $m_key . '" selected>' . $this->arrayTypeName[$m_key] . '</option>';
			else echo '<option value="' . $m_key . '">' . $this->arrayTypeName[$m_key] . '</option>';
			
		}
		echo '</select></td></tr>';
		
		/*
		Nếu tồn tại $new_site_structure[$lv_level_1]
		=> Show tiếp cấp 2
		*/
		if (isset($new_site_structure[$lv_level_1])){
			$kieu = 0;
			$lv_level_2 = getValue("lv_level_2","str","POST","",1);
			echo '<tr><td class="textBold" align="right">Thuộc tính: </td><td><select name="lv_level_2" id="lv_level_2" class="form"  onchange="document.add_list.submit()">';
				echo '<option value="">--[Bạn hãy lựa chọn]--</option>';
				foreach ($new_site_structure[$lv_level_1] as $m_key => $m_value){
					if($m_value["fie_type"]!=0){ //khác kiểu textbox
						if ($lv_level_2 == $m_key){
							$kieu = $m_value["fie_type"];
							echo '<option value="' . $m_key . '" selected>' . $m_value["fie_name"] . '</option>';
						}else{
							echo '<option value="' . $m_key . '">' . $m_value["fie_name"] . '</option>';
						}
					}
				}
				echo '</select>';
				if($kieu==2){
					echo '<input type="hidden" class="form" name="ldv_valbit" id="ldv_valbit" value="' . $this->createValAndBit($lv_level_1,$lv_level_2) . '">';
				}
			echo '</td></tr>';
			
		}
	}
	
	//ham kiem tra gia tri and bit
	
	function createValAndBit($lv_level_1,$lv_level_2){
		foreach($this->arrayValBit as $key=>$value){
			$db_check = new db_query("SELECT ldv_valbit FROM list_value_classified WHERE ldv_level1='" . $lv_level_1 . "' AND ldv_level2='" . $lv_level_2 . "' AND ldv_valbit = " . $key);
			if(mysql_num_rows($db_check->result)==0){
				return $key;
				break;
				unset($db_check);
			}
		}
	}
	
	/*
	Show form trong mục add product (Thêm sp mới)
	$type_lv1 : kiểu cấp 1 : laptop, desktop ..v..v
	*/
	function form_in_add_product($type_lv1,$id_update = 0){
		
		$new_site_structure = $this->create_new_site_structure();
		//Nếu tồn tại phần tử $type_lv1 trong array

		if (isset($new_site_structure[$type_lv1])){
		
			$db_update = new db_query("SELECT * FROM " . $type_lv1 . " WHERE classified_id = " . $id_update);
			
			$rowv = mysql_fetch_assoc($db_update->result);
			unset($db_update);
			//echo $rowv["d7"];
			//Loop các value con
			foreach($new_site_structure[$type_lv1] as $m_key => $m_value){
				
				$db_list_value_classified = new db_query("SELECT * FROM list_value_classified WHERE ldv_level1='" . $this->removeQuote($type_lv1) . "' AND ldv_level2= '" . $this->removeQuote($m_key) . "' ORDER BY ldv_order ASC");
				$this->str_form_add_product .= '<tr><td  nowrap="nowrap" valign="top">' . $m_value["fie_name"] . " : </td>";
				$this->str_form_add_product .= '<td>';
					//kiem tra kieu du lieu
					switch($m_value["fie_type"]){
					
						case 0: //kieu text box
							//Nếu $m_value["fie_type"]==0 
							$this->str_form_add_product .= '<input type="text" name="' . $m_key . '" id="' . $m_key . '" value="' . getValue($m_key . '',"str","POST",$rowv[$m_key . '']) . '" class="form" size="50"> ';
							
							if($m_value["fie_require"]==1) $this->strCheckJavascript .= "if (checkblank(document.getElementById(\"" . $m_key . "\").value)) { alert('Bạn chưa nhập phần text " . $m_value["fie_name"] . "'); document.getElementById(\"" . $m_key . "\").style.border='solid 1px #FF0000'; document.getElementById(\"" . $m_key . "\").focus(); return false;}";
						break;
						case 1: //kiểu combox
							$this->str_form_add_product .= '<select name="' . $m_key . '" id="' . $m_key . '" class="form">';
							$this->str_form_add_product .= '<option value="">Chọn '. $m_value["fie_name"] .'</option>';	
							
							while($row=mysql_fetch_array($db_list_value_classified->result)){
								if(getValue($m_key,"int","POST",$rowv[$m_key]) == $row["ldv_id"]){
									$this->str_form_add_product .= '<option value="' . $row["ldv_id"] . '" selected >'. $row["ldv_name"] .'</option>';
								}else{
									$this->str_form_add_product .= '<option value="' . $row["ldv_id"] . '" >'. $row["ldv_name"] .'</option>';
								}
							}
							
							$this->str_form_add_product .= '</select>';
							
							//kiểm tra bằng javascript
							if($m_value["fie_require"]==1) $this->strCheckJavascript .= "if (checkblank(document.getElementById(\"" . $m_key . "\").value)) { alert('Bạn chưa chọn " . $m_value["fie_name"] . "'); document.getElementById(\"" . $m_key . "\").style.border='solid 1px #FF0000'; document.getElementById(\"" . $m_key . "\").focus(); return false;}";						
						break;
						case 2:
							//kieu nhieu lua chon thi phai chia cot
							$this->str_form_add_product .= '<table cellpadding="4" cellspacing="0" border="0">';
								$num_col = 3;
								$j=1;
								if($row = mysql_fetch_assoc($db_list_value_classified->result)) $go_next = 1;
								else $go_next = 0;
								while($go_next == 1){
									$this->str_form_add_product .= '<tr>';
									for($i=0;$i<$num_col;$i++){
										$this->str_form_add_product .= '<td valign="top">';
										if($go_next == 1){
											//--------------------------------------->
											$this->str_form_add_product .= '<input type="checkbox" name="' . $m_key . '[]" id="' . $m_key . '" value="' . $row["ldv_valbit"] . '" ' . (((intval($rowv[$m_key]) & intval($row["ldv_valbit"]))!=0) ? 'checked' : '') . ' /> ' . $row["ldv_name"];
											//--------------------------------------->											
										$j++;
										}
										if($row = mysql_fetch_assoc($db_list_value_classified->result)){
											$go_next = 1;
										}else{
											$go_next = 0;
										}
										$this->str_form_add_product .= '</td>';
									}
									$this->str_form_add_product .= '</tr>';
								}
							$this->str_form_add_product .= '</table>';
							
						break;
						case 4:
							$this->str_form_add_product .= '<input type="checkbox" name="' . $m_key . '" id="' . $m_key . '" value="' . getValue($m_key . '',"int","POST",$rowv[$m_key . '']) . '" class="form"> ';
						break;
					}
				$this->str_form_add_product .= '</td></tr> ';
				unset($db_list_value_classified);
				//add check javascript
			}
		}
	}
	







	/*
	Tạo ra string map dữ liệu phục vụ cho insert hoặc edit dữ liệu
	
	$type_lv1 : kiểu cấp 1
	$form_field : Tham trị, nhận giá trị trả về của $form_field
	$data_field : Tham trị, nhận giá trị trả về của $data_field
	
	*/
	function gennerate_To_Data($type_lv1,$id_update = 0){
		$new_site_structure = $this->create_new_site_structure();
		$errorMsg = "";
		//Nếu tồn tại phần tử $type_lv1 trong array
		if (isset($new_site_structure[$type_lv1])){
			
			//Loop các value con
			//Call Class generate_form();
			$myform = new generate_form();
			foreach($new_site_structure[$type_lv1] as $m_key => $m_value){
				
				//add to form
				switch($m_value["fie_type"]){
					case 0:
						//kiểu dữ liệu sting
						$myform->add($m_key . "",$m_key . "",0,0,"",0,"",0,"");
					break;
					case 2:
						//kiểu dữ liệu multicheck
						$$m_key = array_sum(getValue($m_key,"arr","POST",array()));
						$_POST[$m_key] = $$m_key;
						$myform->add($m_key,$m_key,1,0,0,0,"",0,"");
					break;
					default:
						$myform->add($m_key,$m_key,1,0,0,0,"",0,"");
					break;
				}								
			}
			
			//nếu không phải là cập nhật thì mới thêm các trường bắt buộc
			if($id_update == 0){
				foreach($this->arrayFieldDefault as $key=>$value){
					$myform->add($key,$key,$value,1,0,0,"",0,"");
				}
			}
			
			//Add table
			$myform->addTable($type_lv1);
			$errorMsg .= $myform->checkdata();
			//$errorMsg .= $myform->generate_insert_SQL();	
			//$errorMsg .=$myform->generate_update_SQL("classified_id", $id_update);		
			
			if($errorMsg == ""){
				//nếu id_update = 0 thì thêm mới
				if($id_update == 0){
				
					$db_update	= new db_execute_return();
					$last_id 	= $db_update->db_execute($myform->generate_insert_SQL());
					unset($db_update);
					unset($myform);
				
					if($last_id !=0 ) return '';
					 return 'Cập nhật vào bảng ' . $type_lv1 . ' không thành !<br>';
				
				}else{
				
					$db_update	= new db_execute($myform->generate_update_SQL("classified_id", $id_update));
					//echo $myform->generate_update_SQL("classified_id", $id_update); exit();
					unset($db_update);
				
				}
			}else{
				
				unset($myform);
				return $errorMsg;
			
			}
		}		
	}






	/*
	tạo ra form search bên ngoài
	$num_col = 2; //số cột
	*/
	function gennerate_Form_Search($type_lv1,$num_col = 2){
		$strform = '';
		
		$new_site_structure = $this->create_new_site_structure();
		
		//Nếu tồn tại phần tử $type_lv1 trong array
		if (isset($new_site_structure[$type_lv1])){

			//tạo form tìm kiếm chia làm cột
			$i			= 0;
			foreach($new_site_structure[$type_lv1] as $m_key => $m_value){
				$i++;
				//dưa ra các giá trị từ bản list_value_classified
				$db_list_value_classified = new db_query("SELECT * FROM list_value_classified WHERE ldv_level1='" . $this->removeQuote($type_lv1) . "' AND ldv_level2= '" . $this->removeQuote($m_key) . "' ORDER BY ldv_order ASC");

				if($i == 1) $strform .= '<table cellpadding="5" cellspacing="0" border="0"><tr>'; 	
				if($i%$num_col == 1 && $i != 1) $strform .= '<tr>'; 
						//bắt đầu td chứa nội dung --------------------------------------------
						
						$strform .= '<td width="100">' . $m_value["fie_name"] . " : </td>";
						$strform .= '<td><select name="' . $m_key . '" id="' . $m_key . '" class="form" style="width:150px;">';
						$strform .= '<option value="">Chọn '. $m_value["fie_name"] .'</option>';	
						
						while($row=mysql_fetch_array($db_list_value_classified->result)){
							if(getValue($m_key) == $row["ldv_id"]){
								$strform .= '<option value="' . $row["ldv_id"] . '" selected >'. $row["ldv_name"] .'</option>';
							}else{
								$strform .= '<option value="' . $row["ldv_id"] . '" >'. $row["ldv_name"] .'</option>';
							}
						}
						
						$strform .= '</select>';
						$strform .= '</td>';
						
						//kết thúc td chứa nội dung --------------------------------------------
				if($i%$num_col == 0 && $i != count($new_site_structure[$type_lv1])) $strform .= '</tr>';
				if($i == count($new_site_structure[$type_lv1])){
					
					//thêm các td còn thiếu để cho đúng với cấu trúc table html
					if(count($new_site_structure[$type_lv1])>$num_col){
						for($j = 0;$j < count($new_site_structure[$type_lv1])%$num_col;$j++) $strform .= '<td>&nbsp;</td><td>&nbsp;</td>';
					}
					
					$strform .= '</tr></table>';
				}
				unset($db_list_value_classified);
				//add check javascript
			}
		}
		return $strform;
	}
	
	
	//ham hien thi chi tiet
	function generate_detail($type_lv1,$id_update){
		$new_site_structure = $this->create_new_site_structure();
		//bien return
		$strt = '';
		
		//Nếu tồn tại phần tử $type_lv1 trong array

		if (isset($new_site_structure[$type_lv1])){
			$strt .= '<div id="des_1">';
			$strt .= '<table cellpadding="6" cellspacing="0" width="100%" border="1" bordercolor="#e4e4e4" class="table_detail">';
			$db_update = new db_query("SELECT * FROM " . $type_lv1 . " WHERE classified_id = " . $id_update);
			
			$rowv = mysql_fetch_assoc($db_update->result);
			unset($db_update);
			//echo $rowv["d7"];
			//Loop các value con
			$i=0;
			foreach($new_site_structure[$type_lv1] as $m_key => $m_value){
				$i++;
				$db_list_value_classified = new db_query("SELECT * FROM list_value_classified WHERE ldv_level1='" . $this->removeQuote($type_lv1) . "' AND ldv_level2= '" . $this->removeQuote($m_key) . "' ORDER BY ldv_order ASC");
				$strt .= '<tr ' . (($i%2==0) ? 'bgcolor="#f9f9f9"' : '') . '><td class="t" width="10%"  nowrap="nowrap">' . $m_value["fie_name"] . " : </td>";
				
					//kiem tra kieu du lieu
					switch($m_value["fie_type"]){
					
						case 0: //kieu text box
							$strt .= '<td class="c">';
							$strt .= $rowv[$m_key];
							$strt .= '</td>';
						break;
						case 1: //kiểu combox
							while($row=mysql_fetch_array($db_list_value_classified->result)){
								if($rowv[$m_key] == $row["ldv_id"]){
									$strt .= '<td class="c">';
									$strt .= $row["ldv_name"];
									$strt .= '</td>';
									break;
								}
							}
						break;
						case 2:
							//kieu nhieu lua chon thi phai chia cot
							$strt .= '<td class="m">';
							$strt .= '<table cellpadding="6" cellspacing="0" border="0" width="100%">';
								$num_col = 2;
								$j=1;
								if($row = mysql_fetch_assoc($db_list_value_classified->result)) $go_next = 1;
								else $go_next = 0;
								while($go_next == 1){
									$strt .= '<tr>';
									for($i=0;$i<$num_col;$i++){
										$strt .= '<td valign="top" width="50%">';
										if($go_next == 1){
											//--------------------------------------->
											$strt .= '<img src="/lib/img/check_' . (((intval($rowv[$m_key]) & intval($row["ldv_valbit"]))!=0) ? '1' : '0') . '.gif" border="0">&nbsp;' . $row["ldv_name"];
											//--------------------------------------->											
										$j++;
										}
										if($row = mysql_fetch_assoc($db_list_value_classified->result)){
											$go_next = 1;
										}else{
											$go_next = 0;
										}

										$strt .= '</td>';
									}
									$strt .= '</tr>';
								}
							$strt .= '</table>';
							$strt .= '</td>';
						break;
						case 4:
							$strt .= '<input type="checkbox" name="' . $m_key . '" id="' . $m_key . '" value="' . getValue($m_key . '',"int","POST",$rowv[$m_key . '']) . '" class="form"> ';
						break;
					}
				$strt .= '</tr> ';
				unset($db_list_value_classified);
				//add check javascript
			}
			
			$strt .= '</table>';
			$strt .= '</div>';
		}
		return $strt;
	}
	
	
	
	//ham lay name từ bảng list_value_classified khi có id
	
	function get_teaser_value($type_lv1,$description = 0){
		
		$str_teaser = '';
		$new_site_structure = $this->create_new_site_structure();
		
		//Nếu tồn tại phần tử $type_lv1 trong array
		if (isset($new_site_structure[$type_lv1])){
			$j = 0;
			foreach($new_site_structure[$type_lv1] as $key=>$value){
				
				//nếu kiểu liệt kê thì select từ bảng list_value_classified
				if($value["fie_type"] == 0){
					if(isset($_POST[$key])){
						$db_list_value_classified = new db_query("SELECT ldv_name 
																 FROM list_value_classified 
																 WHERE ldv_id = " . intval($_POST[$key]));
						
						if($row = mysql_fetch_array($db_list_value_classified->result)){
							//nếu là update description thì 
							if($description == 1){
								$j++;
								$bgcolor = ' bgcolor="#F8F8F8"';
								if($j%2==0) $bgcolor = ' bgcolor="#FFFFFF"';
								$str_teaser .= '<tr ' . $bgcolor .  '><td class="textBold" width="180">' . $value["fie_name"] . ': </td>';
								$str_teaser .= '<td>' . $row["ldv_name"] . '</td></tr>';
								
							}
							//ngược lại chỉ update teaser
							else{
								$str_teaser .= $value["fie_name"] . ': ' . $row["ldv_name"] . chr(13);
							}
							
						}
						
						unset($db_list_value_classified);
					}
					
				//nếu kiểu thứ hai thì chỉ lấy giá trị từ form submit lên
				}
				else{
					
					if(isset($_POST[$key . ''])){
						$j++;
						//nếu là update description thì 
						if($description == 1){
							$bgcolor = ' bgcolor="#F8F8F8"';
							if($j%2==0) $bgcolor = ' bgcolor="#FFFFFF"';
							$str_teaser .= '<tr ' . $bgcolor .  '><td class="textBold" width="180">' . $value["fie_name"] . ': </td>';
							$str_teaser .= '<td>' . $_POST[$key . ''] . '</td></tr>';
						}
						//ngược lại chỉ update teaser
						else{
							$str_teaser .= $value[0] . ': ' . $_POST[$key . ''] . chr(13);
						}
					}
				
				}
				
			}//end foreach
			
		}//end if
		
		return $str_teaser;
		
	}
	
	
		
	//tạo list search giống kiểu trang vật giá
	function gennerate_List_Search($type_lv1, $http_method = "GET"){
		$str = '';
		$new_site_structure = $this->create_new_site_structure();
	
		//Nếu tồn tại phần tử $type_lv1 trong array
		if (isset($new_site_structure[$type_lv1])){
		
			//tạo tạo list tìm kiếm
			$str =  '<table>';
			foreach($new_site_structure[$type_lv1] as $m_key => $m_value){
			
				//lấy dữ liệu từ bản list_value_classified
				$sql='';
				
				//nếu chỉ hiện mỗi tiêu chí mình chọn nếu hiển thị tất cả thì comment dòng dưới lại
					if(getValue($m_key,"int",$http_method) !=0 ) $sql .= " AND ldv_id = " . getValue($m_key,"int",$http_method);
				
				$db_list_value_classified = new db_query("SELECT * FROM list_value_classified WHERE ldv_level1='" . $this->removeQuote($type_lv1) . "' AND ldv_level2= '" . $this->removeQuote($m_key) . "' " . $sql . " ORDER BY ldv_order ASC" );
			
				//dua ra list_value_classified
				$i=0;
				while($row=mysql_fetch_array($db_list_value_classified->result)){
					
					//tính count cho từng tiêu chí
					$db_count 		= new db_query("SELECT count(*) AS count FROM " . $type_lv1 . ",products WHERE classified_id = pro_id AND " . $m_key . "=" . $row["ldv_id"] . $this->gennerate_SQL_Where($new_site_structure[$type_lv1]));
					$count = mysql_fetch_array($db_count->result);
					$count = $count["count"];
					if($count == 0) $count = '';
					else $count = '(' . $count . ')';
					unset($db_count);
					
					//nếu chỉ đưa ra những tiêu chí thỏa mãn tìm kiếm thì if($count != ''){	
					
					//if($count != ''){			
						$i++;
						if($i==1){					
							//đưa ra trường tìm kiếm
							$str .=  '<tr>';
							$str .=  '<td colspan="2"><b><a href="' . $_SERVER['SCRIPT_NAME'] . '?' . $this->getURL($new_site_structure[$type_lv1],$m_key) . '">' . $m_value["fie_name"] .  '</a></b></td>';
							$str .=  '</tr>';
						}
						
						//đưa ra tiêu chí tìm kiếm
						$str .=  '<tr>';
						$str .=  '<td>&nbsp;</td>';
						$str .=  '<td><a href="' . $_SERVER['SCRIPT_NAME'] . '?' . $m_key . '=' . $row["ldv_id"] . $this->getURL($new_site_structure[$type_lv1],$m_key) . '">' . $row["ldv_name"] . ' ' . $count . '</a></td>';
						$str .=  '</tr>';
						
					//}//end unset($db_count);
					
				}
				unset($db_list_value_classified);
			}
			$str .=  '</table>';
		}
		return $str;
	}
	
	
	/*
	hàm tạo url lụa chọn theo tiêu chí
	*/
	function getURL($arrayKey,$key_select=''){
		//chỉ chọn một tiêu chí
		return ''; 
		//chọn nhiều tiêu chí
		$str = '';
		
		//Nếu tồn tại phần tử $type_lv1 trong array
		if (count($arrayKey)>0){
			foreach($arrayKey as $m_key => $m_value){
				if(isset($_GET[$m_key])){
				  if($_GET[$m_key] != ''){
				  	 if($key_select != $m_key) $str .= '&' . $m_key . '=' . intval($_GET[$m_key]);
				  }
				}
			}
		}
		return $str;
	}


	/*
	Tạo ra câu lệnh SQL để tìm kiếm sản phẩm
	
	$type_lv1 : kiểu cấp 1
	$http_method : là method GET hoặc POST mặc định là GET
	*/
	function generate_SQL_search($type_lv1, $http_method = "GET"){
		$mysql = "";
		
		$new_site_structure = $this->create_new_site_structure();
		//Nếu tồn tại phần tử $type_lv1 trong array
		if (isset($new_site_structure[$type_lv1])){
			$strWhere = $this->gennerate_SQL_Where($new_site_structure[$type_lv1],$http_method);
			if($strWhere != ''){
				$mysql = "INNER JOIN " . $type_lv1 . " ON(pro_id = " . $type_lv1 . ".classified_id  ";
							 
				$mysql .= $strWhere;		 
				
				$mysql .= ")";
			}
		}		
		
		return $mysql;
	}
	
	/*
	Tạo ra câu lệnh đieu kiện where để tìm kiếm sản phẩm
	
	$type_lv1 : kiểu cấp 1
	$http_method : là method GET hoặc POST mặc định là GET
	*/
	function gennerate_SQL_Where($arrayField, $http_method = "GET"){
		$strWhere = '';
		
		if(count($arrayField)>0){
			foreach($arrayField as $m_key => $m_value){
				// tên_bảng.tên_trường = GET or POST ...., nếu = 0 có thể làm thêm trường hợp để ko add vào query
				if(getValue($m_key,"int",$http_method) != 0) $strWhere .= " AND " . $m_key . " = " . getValue($m_key,"int",$http_method);
			}			 
		}
		
		return $strWhere;
	}
	
	/*
	so sanh san pham
	*/
	function compare($type_lv1,$list_classified_id,$list_field_compare){
		$strreturn = '';
		$new_site_structure = $this->create_new_site_structure();
		//Nếu tồn tại phần tử $type_lv1 trong array
		if (isset($new_site_structure[$type_lv1])){
			//loc va kiem tra nhung id san pham so sanh
			$list			=	explode(",",$list_classified_id);
			$proid		=	'';
			$sql			=	'';
			if(isset($list[0])){
				for($i=0;$i<count($list);$i++){
					if(isset($list[$i]) && intval($list[$i])!=0){
						$proid	.=	intval($list[$i]) . ",";
					}
				}
			}
			$proid 		= $proid . "0";
			$sql			=	" AND pro_id IN(" . $proid . ")";
			$db_list_product = new db_query("SELECT *
														  FROM products,tbl_category," . $type_lv1 . "
														  WHERE classified_id = pro_id AND cat_id = pro_category AND cat_active=1 " . $sql . "
														  ORDER BY pro_date DESC
														  ");
			$arrayReturn = array();		
			$i=-1;
			while($row = mysql_fetch_array($db_list_product->result)){
				$i++;
				$j=1;
				foreach($new_site_structure[$type_lv1] as $m_key => $m_value){
					$arrayReturn[$i][$j]	=	$m_key;
					$j++;
				}
			}
			//print_r($arrayReturn);
		}
	}

	
	/*
	remove quote
	*/
	function removeQuote($value){
		$value = str_replace("\'","'",$value);
		$value = str_replace("'","''",$value);
		return $value;
	}
	
	/*
	tao url de phan trang 
	*/
	
	
	function generate_query_string(){
		$string = '';
		//goi mang cau truc
		$new_site_structure = $this->create_new_site_structure();
		//Nếu tồn tại phần tử table_name trong array
		if (isset($new_site_structure[$this->table_name])){
		
			foreach($new_site_structure[$this->table_name] as $m_key => $m_value){
				
				if(getValue($m_key) != 0) $string .= "&" . $m_key . "=" . getValue($m_key);
			
			}
			
		}
		
		return $string;
	}
		
	
	/*
	Tạo 1 array site_structure và reset nó để tránh loop bị sai
	*/
	function create_new_site_structure(){
		$new_site_structure = $this->site_structure;
		reset($new_site_structure);
		
		return $new_site_structure;
	}
	
	/*
	ham lay table search
	*/
	function getTableName($record_id){
		$db_delte = new db_query("SELECT cat_type_search FROM tbl_category,products WHERE cat_type_search<>'' AND cat_id=pro_category AND pro_id = " . $record_id);
		if($row=mysql_fetch_assoc($db_delte->result)){
			return $row["cat_type_search"];
		}else{
			return '';
		}

	}
	
	/*
	ham xoa du lieu
	*/
	function delte_record($record_id){
		$db_delte = new db_query("SELECT cat_type_search FROM tbl_category,products WHERE cat_type_search<>'' AND cat_id=pro_category AND pro_id = " . $record_id);
		if($row=mysql_fetch_assoc($db_delte->result)){
			$db_table = new db_query("SHOW TABLES");
			while($table=mysql_fetch_array($db_table->result)){
				if($table[0]==$row["cat_type_search"]){
					$db_deletetable	= new db_execute("DELETE FROM laptop WHERE classified_id = " . $record_id);
				}
			}
			
		}
	}
}
/*
echo '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />';
echo '<form action="' . $_SERVER['SCRIPT_NAME'] . '" method="get">';
$lang_id = 1;
$classified_id = 243242;
require_once("database.php");
require_once("../functions/functions.php");
require_once("generate_form_class.php");
$myDataMulti = new hishop();
$errorMsg = $myDataMulti->gennerate_Form_Search("laptop",2);
echo $myDataMulti->generate_SQL_search("laptop");
echo '<input type="submit" value="Tìm kiếm" /></form>';
echo $errorMsg;
echo $myDataMulti->gennerate_List_Search("laptop");
//*/
?>