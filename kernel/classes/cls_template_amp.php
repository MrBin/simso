<?
# defined('_RIGHT_ACCESS') or die('No direct access');
class _temp extends menu{
   
   #+
   #+ Template Detail Sim Other
   public static function _simDetailOther($row, $col_md){
      global $arrSimCat;

		#+
   	$sim_sim1 		= $row['sim_sim1'];
   	$sim_sim2 		= '0'.$row['sim_sim2'];
   	$sim_price		= $row['sim_price'];
   	$sim_price		= number_format($sim_price);
   	$sim_category 	= $row['sim_category'];
   	$sim_typeid 	= $row['sim_typeid'];
   	#+
   	$cat_name		= $arrSimCat[$sim_category]["cat_name"];
   	$cat_picture	= $arrSimCat[$sim_category]["cat_picture"];

   	#+
   	#+ Tao link
   	$row['cat_name_index'] = $arrSimCat[$sim_category]["cat_name_index"];

   	$lin_detail = createLink("detail_sim",$row);
      
      $html  = ''; 
   	$html .= '
   				<li class="col-xs-'.(12/$col_md).'" style="padding: 0;">
   					<a class="sim-digit" href="'.$lin_detail.'" rel="nofollow" title="'.$cat_name. ' ' . $sim_sim1.'"><strong>'.$sim_sim1.'</strong></a>-<span class="sim-price text-success">'.$sim_price.'</span>
   				</li>
   				';
      return $html;
	}

	#+
	#+ Template News Body
	public static function _newsBody($row,$i){
		$lin_cat = createLink("detail_news",$row);
		#+
		$str  = '';
		$str .= '
				<li>
					<article>
						<a href="'.$lin_cat.'" title="'.trim($row["new_title"]).'">'.$row["new_title"].'</a>
					</article>
				</li>
				';
		return $str;
	}

	#+
	#+ Template News Home
	public static function _newsHome($arr = array()){
		global $lang_id;
		global $menuid;
      global $myMemcache;

      // Tạo key cache cho toàn bộ web
      $key_cache = md5('key_cls_temp_news_home');

      // Kiểm tra xem có cache từ memcache hay không
      $data_memcache = $myMemcache->get($key_cache);
      
      if ($data_memcache == '') {

   		#+
   		#+ Thuc hien query lay category
   		$query = " SELECT * FROM tbl_category"
   				." WHERE cat_type = 'tin-tuc' AND cat_active = 1 AND cat_parent_id = 0"
   				." ORDER BY cat_order LIMIT 0,6"
   				;
   		$db_cat = new db_query($query);
   		
   		$str = '';
   		
   		$i = 0;
   		while($cat = mysql_fetch_assoc($db_cat->result)){
   			$i++;
   			
   			$link_cat = createLink("cat",$cat);
   		
   			$str .= '
   					<section>
   					<a href="'.$link_cat.'" title="'.$cat["cat_name"].'" class="cat">'.$cat["cat_name"].'</a> <br />
   					
   					<p class="pull-left" style="margin-right:10px;">
   						<a href="'.$link_cat.'" title="'.$cat["cat_name"].'">
   							<img class="img-thumbnail" src="/store/media/category/m_' . $cat["cat_picture"].'" width="133" height="100" alt="'.$cat["cat_name"].'" />
   						</a>
   					</p>
   					
   					<ul>
   					';
   	
   					#+
   					#+
   					$sqlcategoryNews	= "";
   					$listiCatNews		= $menuid->getAllChildId($cat["cat_id"]);
   					$sqlcategoryNews	= " AND cat_id IN(" . $listiCatNews  . ")";
   								
   					$limitNews = 3;
   					if($module == "home"){
   						$limitNews = 3;
   					}
   					
   					#+
   					#+ Thuc hien query lay tin tuc
   					$query = " SELECT *"
   							." FROM tbl_news"
   							." INNER JOIN tbl_category ON(cat_id = new_category)"
   							." WHERE new_active = 1".$sqlcategoryNews
   							." ORDER BY new_date DESC, new_id DESC"
   							." LIMIT 0, " . $limitNews
   							;
   					$db_query = new db_query($query);
   					while($row = mysql_fetch_assoc($db_query->result)){
   						
   						$lin_detail = createLink("detail_news",$row);
   			
   						$str .= '
   								<li>
   									<article>
   										<a href="'.$lin_detail.'" title="'.replaceQ($row["new_title"]).'">'.$row["new_title"].'</a><br />
   									</article>
   								</li>
   								'
   								;
   					} // End while($row = mysql_fetch_assoc($db_news->result))
   		
   			$str .= '
   					</ul>
   					<div class="clearfix"></div>
   					</section>
   					';
   		} // End while($cat = mysql_fetch_assoc($db_catNews->result))
   		
   		#+
   		#+ Huy bien
   		#+
   		unset($db_query);
   		#+
   		$db_cat->close();
   		unset($db_cat);
      
         $myMemcache->set($key_cache, $str, 60*30);
      }else{
         $str = $data_memcache;
      }

		#+
		return $str;
	}
	
	
	#+
	#+ Template Don hang
	public static function _donHang($arr = array()){
		global $lang_id;
      global $myMemcache;

      // Tạo key cache cho toàn bộ web
      $key_cache = md5('key_cls_temp_don_hang');

      // Kiểm tra xem có cache từ memcache hay không
      $data_memcache = $myMemcache->get($key_cache);
      
      if ($data_memcache == '') {

   		$query = "SELECT MAX(id) FROM tbl_yeucau";
   		$db_query = new db_query($query);
   		$maxOrdYc 		= mysql_fetch_assoc($db_query->result);
   		$maxOrdYc 		= $maxOrdYc["MAX(id)"] - 24;
         
         
   		$query = " SELECT * FROM tbl_yeucau"
   				." WHERE id > " . $maxOrdYc
   				." ORDER BY tinhtrang, phanloai, thoigian DESC, id DESC"
   				." LIMIT 0,10"
   				;
   		$arrQuery = getArray($query);
      
         $myMemcache->set($key_cache, $arrQuery, 30);
      }else{
         $arrQuery = $data_memcache;
      }

		$str = '';
		$i = 0;
		foreach($arrQuery as $key => $row){
		$i++;	
			$tinhtrang = '';
			if($row["tinhtrang"] == 0){
				$tinhtrang = ' text-danger';
			} // End if($row["tinhtrang"] == 0){
			
		
			$str .= '<li class="col-md-6">';
			
			if($row["tinhtrang"] == 0){
				$str .= '<img src="/lib/img/icon/icon-donhangmoi1.jpg" alt="Đơn hàng 1" />&nbsp;';
			}else{	
				$str .= '<img src="/lib/img/icon/icon-donhangmoi2.jpg" align="Đơn hàng 2" />&nbsp;';
			} // End if($row["tinhtrang"] == 0){
			
			$str .= '
					<span class="text-bold'.$tinhtrang.'">
						'.$row["hoten"].'
					</span>
					<br />
					';
			switch($row["phanloai"]){
				case 1:
					$str .= 'Đặt sim';
				break;
				case 2:
					$str .= 'Yêu cầu';
				break;
				case 3:
					$str .= 'Bán sim';
				break;
				case 4:
					$str .= 'Đăng ký';
				break;
			} // End switch($row["phanloai"]){
			
			$str .= '
					:
					'.$row["simcard"].'
					<br />
					
					<span class="text-muted">Thời gian : '.date("d/m H:i",strtotime($row["thoigian"])).'</span>
					';
			
			$str .= '</li>';
	
		}
		
		#+
		#+ Huy bien
		unset($arrQuery);
		
		#+
		return $str;
	}

	#+
	#+ Template Sim Body
	public static function _simBody($row,$i){
		global $module;
		global $arrSimCat;
		global $arrSimType;
		global $iNamSinh;
		global $iNguHanh;
		global $sNguHanh;
		global $arrBanMenh;
		global $arrNguHanh;
		global $arrNguHanh1;
		
		#+
		$sim_sim1 		= $row['sim_sim1'];
		$sim_sim2 		= '0'.$row['sim_sim2'];
		$sim_price		= $row['sim_price'];
		$sim_price		= number_format($sim_price);
		$sim_category 	= $row['sim_category'];
		$sim_typeid 	= $row['sim_typeid'];
		#+
		$cat_name		= $arrSimCat[$sim_category]["cat_name"];
		$cat_picture	= $arrSimCat[$sim_category]["cat_picture"];
		#+
		$simtp_name		= $arrSimType[$sim_typeid]["simtp_name"];
		#+
		$sim_nguhanh 	= $row['sim_nguhanh'];
		$sim_sonut		= $row['sim_sonut'];
		$sim_diem_stst	= $row['sim_diem_stst'];
		
		#+
		#+ Tao link
		$row['cat_name_index'] = $arrSimCat[$sim_category]["cat_name_index"];
		$row['simtp_index'] = $arrSimType[$sim_typeid]["simtp_index"];

		if($module == 'simtype'){
			$lin_detail = createLink("detail_simtype",$row);
		}else{
			$lin_detail = createLink("detail_sim",$row);	
		}

		$str  = '';
		$str .= '
					
				<tr>
					<td class="active text-center">
						<strong>'.$i.'</strong>
					</td>
					<td>
						<a class="sim-digit" href="'.$lin_detail.'" rel="nofollow" title="'.$cat_name. ' ' . $sim_sim1.'"><strong>'.$sim_sim1.'</strong></a>
					</td>
					<td>
						<span class="sim-price text-success">'.$sim_price.'</span>
					</td>
				';
				
		if($iNguHanh != 0){
		#+
		$str .= '
					<td>
						<form action="/sim-phong-thuy.html" method="POST">
							<input type="hidden" name="sodienthoai" value="'.$sim_sim2.'">
							<input type="hidden" name="namsinh" value="'.$iNamSinh.'">
							<button style="margin:0; padding:0px 10px" data-toggle="tooltip" data-placement="right" data-original-title="Điểm phong thủy : <strong>'.$sim_diem_stst.'/10 điểm</strong>" id="sht_'.$sim_sim2.'" class="btn btn-warning">Xem điểm</button>
						</form>
					</td>
					
				';		
		}else{
		$str .= '
					<td>
						<amp-img src="/store/media/category/'.$cat_picture.'" width="75" height="22" alt="'.$cat_name.'" />&nbsp;
					</td>
				</tr>
				';	
		} // End if($module == 'simhoptoi' || $module == 'simhopmenh')

		return $str;	
	}
	
	#+
	#+ Template Sim Head
	public static function _simHead(){
		$str = '';
		$str .= '
				<thead class="bg-primary">	
					<tr>
						<th>
							Stt
						</th>
						<th>
							Số sim
						</th>
						<th>
							Giá bán
						</th>
						<th>
							Mạng
						</th>
					</tr>
				</thead>
				';
				
		return $str;	
	}
	
	#+
	#+ Query
	public static function _checkSimDauSo(){

      global $myMemcache;
      
      // Tạo key cache cho toàn bộ web
      $key_cache = md5('key_cls_temp_check_sim_dau_so');

      // Kiểm tra xem có cache từ memcache hay không
      $data_memcache = $myMemcache->get($key_cache);
      
      if ($data_memcache == '') {
         $query = " SELECT sds_id,sds_category,sds_name,sds_name,sds_home"
   				." FROM tbl_simdauso"
   				." WHERE sds_active = 1"
   				." ORDER BY sds_id DESC"
   				;
   		$arrQuery = getArray($query,'sds_id');
      
         $myMemcache->set($key_cache, $arrQuery, 86400*30);
      }else{
         $arrQuery = $data_memcache;
      }

		return $arrQuery;
	}
	
	#+
	#+ Query
	public static function _checkSimPrice(){
	  
      global $myMemcache;
      
      // Tạo key cache cho toàn bộ web
      $key_cache = md5('key_check_sim_price');

      // Kiểm tra xem có cache từ memcache hay không
      $data_memcache = $myMemcache->get($key_cache);
      
      if ($data_memcache == '') {
         $query = " SELECT pri_id,pri_name"
   				." FROM tbl_simprice"
   				." WHERE pri_active = 1"
   				." ORDER BY pri_order, pri_id DESC"
   				;
   		$arrQuery = getArray($query,'pri_id');
      
         $myMemcache->set($key_cache, $arrQuery, 86400*30);
      }else{
         $arrQuery = $data_memcache;
      }

		return $arrQuery;
	}
	
	#+
	#+ Query
	public static function _checkSimType(){

		global $myMemcache;
      
      // Tạo key cache cho toàn bộ web
      $key_cache = md5('key_cls_temp_check_sim_type');

      // Kiểm tra xem có cache từ memcache hay không
      $data_memcache = $myMemcache->get($key_cache);
      
      if ($data_memcache == '') {
         $query = " SELECT simtp_id,simtp_name,simtp_index"
   				." FROM tbl_simtype"
   				." ORDER BY simtp_order, simtp_id DESC"
   				;
   		$arrQuery = getArray($query,'simtp_id');
      
         $myMemcache->set($key_cache, $arrQuery, 86400*30);
      }else{
         $arrQuery = $data_memcache;
      }

		return $arrQuery;
	}
	
	#+
	#+ Query
	public static function _checkSimCat(){
	  
      global $myMemcache;

		// Tạo key cache cho toàn bộ web
      $key_cache = md5('key_cls_temp_check_sim_cat');

      // Kiểm tra xem có cache từ memcache hay không
      $data_memcache = $myMemcache->get($key_cache);
      
      if ($data_memcache == '') {
         $query = " SELECT cat_id,cat_name,cat_name_index,cat_picture"
   				." FROM tbl_category"
   				." WHERE cat_active = 1 AND cat_type = 'sim'"
   				." ORDER BY cat_order, cat_id DESC"
   				;
   		$arrQuery = getArray($query,'cat_id');
      
         $myMemcache->set($key_cache, $arrQuery, 86400*30);
      }else{
         $arrQuery = $data_memcache;
      }

		return $arrQuery;
	}
	
	#+
	#+ Template Sim dau so
	public static function _simDauSo($arr = array()){
		global $lang_id;
		global $arrSimCat;
      global $myMemcache;
      
      // Tạo key cache cho toàn bộ web
      $key_cache = md5('key_cls_temp_sim_dau_so');

      // Kiểm tra xem có cache từ memcache hay không
      $data_memcache = $myMemcache->get($key_cache);
      
      if ($data_memcache == '') {
         $query = " SELECT *"
   				." FROM tbl_simdauso"
   				." WHERE sds_active = 1 AND sds_home = 1"
   				." ORDER BY sds_category ASC, sds_order, sds_id ASC"
   				;
   		$arrQuery = getArray($query);
      
         $myMemcache->set($key_cache, $arrQuery, 86400*30);
      }else{
         $arrQuery = $data_memcache;
      }
		
		#+
		#+ Dua ra cache
		$str = '';
	
		foreach($arrQuery as $key => $row){
			$row['cat_name_index'] = $arrSimCat[$row['sds_category']]['cat_name_index'];

			$link_dauso = createLink("simdauso",$row);
			
			$str .= '<div class="item"><a href="'.$link_dauso.'" title="'.$row['sds_name'].'">Đầu số '.$row['sds_name'].'</a></div>';
			
		} // End while($row = mysql_fetch_assoc($db_simDauSo->result))

		unset($arrQuery);
		
		#+
		return $str;
	}
	
	#+
	#+ Template Sim Type
	public static function _simType($arr = array()){
		global $lang_id;
      global $myMemcache;
      
      // Tạo key cache cho toàn bộ web
      $key_cache = md5('key_cls_temp_sim_type');

      // Kiểm tra xem có cache từ memcache hay không
      $data_memcache = $myMemcache->get($key_cache);
      
      if ($data_memcache == '') {
         $query = " SELECT simtp_id,simtp_name,simtp_index,simtp_description"
   				." FROM tbl_simtype"
   				." WHERE simtp_active = 1 AND simtp_home = 1"
   				." ORDER BY simtp_order DESC, simtp_id DESC"
   				;
   		$arrQuery = getArray($query);
      
         $myMemcache->set($key_cache, $arrQuery, 86400*30);
      }else{
         $arrQuery = $data_memcache;
      }

		$str = '';
		$i=0;
		foreach($arrQuery as $key => $row){
			$i++;
			$link_cat = createLink("simtype",$row);
			
			$str .= '<div class="item"><a href="' . $link_cat . '" title="' . $row["simtp_name"] . '">' . $row["simtp_name"] . '</a></div>';
		
		}// End while($row = mysql_fetch_assoc($db_simType->result))

		unset($arrQuery);
		
		#+
		return $str;
	}
	
	#+
	#+ Template Menu Top
	public static function _menuTop($arr = array()){
		global $lang_id;
		global $path_menu;	
		global $module;	
		global $iCat;	
		global $sCat;	
		global $iData;
		global $sData;
      global $myMemcache;

      // Tạo key cache cho toàn bộ web
      $key_cache = md5('key_cls_temp_menu_top');

      // Kiểm tra xem có cache từ memcache hay không
      $data_memcache = $myMemcache->get($key_cache);
      
      if ($data_memcache == '') {
         $query = " SELECT mnu_id,mnu_name,mnu_target,mnu_link,mnu_check "
   				." FROM tbl_menus "
   				." WHERE lang_id = ". $lang_id ." AND mnu_parent_id = 0 AND mnu_type = 1 AND mnu_active = 1 "
   				." ORDER BY mnu_order, mnu_name ASC "
   				;
   		$arrQuery = getArray($query);
      
         $myMemcache->set($key_cache, $arrQuery, 86400*30);
      }else{
         $arrQuery = $data_memcache;
      }            
      

		#+
		#+ Dua ra du lieu
      $url = getURL(1,1,1,1);
		$url .= $module != '' ? '&iCat='.$module : '';
		$url .= $iCat != 0 ? '&iCat='.$iCat : '';
		$url .= $sCat != '' ? '&sCat='.$sCat : '';
		$url .= $iData != 0 ? '&iData='.$iData : '';
		$url .= $sData != '' ? '&sData='.$sData : '';
      
      $current_id = 0;
      
		$str  = '';
      
		$i=0;
		foreach($arrQuery as $key => $row){
			$i++;
			#+
			if(@preg_match($row["mnu_check"], $url)){
				$current_id = $row["mnu_id"];	
			} // End if(@preg_match($row["mnu_check"], $url)){
	
			$select = $row["mnu_id"] == $current_id ? 'active' : '';
			
			#+
			$str .= '
					<li class="'.$select.'">
						<a href="'.$row["mnu_link"].'" target="'.$row["mnu_target"].'" title="'.$row["mnu_name"].'" data-pjax="body" data-ajax="#main">
							'.$row["mnu_name"].'
						</a>
					</li>
					';
		}// End while($row = mysql_fetch_assoc($db_menu_top->result))
		
		#+
		unset($arrQuery);	
		
		#+
		return $str;
	}


} // End class _temp
?>
