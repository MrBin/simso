<?
$arrNguHanh = array(
	1 => 'Kim',
	2 => 'Thủy',
	3 => 'Mộc',
	4 => 'Hỏa',
	5 => 'Thổ',
	);
$arrNguHanhSinhKhac = array(
	12 => 'sinh',	
	14 => 'khac',
	23 => 'sinh',
	24 => 'khac',	
	34 => 'sinh',
	35 => 'khac',	
	45 => 'sinh',
	41 => 'khac',	
	51 => 'sinh',
	52 => 'khac',
	);
$arrNguHanh1 = array(
	'kim' 	=> 1,
	'thuy' 	=> 2,
	'moc' 	=> 3,
	'hoa' 	=> 4,
	'tho' 	=> 5,
	);
$arrBanMenh = array(
	0 => array(
		'menh' => 'Kim',
		'menh_id' => 1,
		'ten_han' => 'Hải Trung Kim',
		'ten_dich' => 'Vàng dưới biển',
		),
	1 => array(
		'menh' => 'Hỏa',
		'menh_id' => 4,
		'ten_han' => 'Sơn Đầu Hỏa',
		'ten_dich' => 'Lửa trên núi',
		),
	2 => array(
		'menh' => 'Thủy',
		'menh_id' => 2,
		'ten_han' => 'Tuyền Trung Thủy',
		'ten_dich' => 'Nước giữa dòng suối',
		),
	3 => array(
		'menh' => 'Kim',
		'menh_id' => 1,
		'ten_han' => 'Sa Trung Kim',
		'ten_dich' => 'Vàng trong cát',
		),	
	4 => array(
		'menh' => 'Hỏa',
		'menh_id' => 4,
		'ten_han' => 'Phúc Đăng Hỏa',
		'ten_dich' => 'Lửa ngọn đèn dầu',
		),	
	5 => array(
		'menh' => 'Thủy',
		'menh_id' => 2,
		'ten_han' => 'Đại Khê Thủy',
		'ten_dich' => 'Nước ở khe lạch lớn',
		),
	10 => array(
		'menh' => 'Hỏa',
		'menh_id' => 4,
		'ten_han' => 'Lư Trung Hỏa',
		'ten_dich' => 'Lửa trong lò',
		),	
	11 => array(
		'menh' => 'Thủy',
		'menh_id' => 2,
		'ten_han' => 'Giang Hà Thủy',
		'ten_dich' => 'Nước sương mù, nước mưa',
		),	
	12 => array(
		'menh' => 'Thổ',
		'menh_id' => 5,
		'ten_han' => 'Ốc Thượng Thổ',
		'ten_dich' => 'Đất trên nóc nhà',
		),	
	13 => array(
		'menh' => 'Hỏa',
		'menh_id' => 4,
		'ten_han' => 'Sơn Hạ Hỏa',
		'ten_dich' => 'Lửa dưới chân núi',
		),	
	14 => array(
		'menh' => 'Thủy',
		'menh_id' => 2,
		'ten_han' => 'Thiên Hà Thủy',
		'ten_dich' => 'Nước trên trời',
		),	
	15 => array(
		'menh' => 'Thổ',
		'menh_id' => 5,
		'ten_han' => 'Sa Trung Thổ',
		'ten_dich' => 'Đất lẫn trong cát',
		),
	20 => array(
		'menh' => 'Mộc',
		'menh_id' => 3,
		'ten_han' => 'Đại Lâm Mộc',
		'ten_dich' => 'Cây trong rừng lớn',
		),	
	21 => array(
		'menh' => 'Thổ',
		'menh_id' => 5,
		'ten_han' => 'Thành Đầu Thổ',
		'ten_dich' => 'Đất trên thành',
		),	
	22 => array(
		'menh' => 'Hoả',
		'menh_id' => 4,
		'ten_han' => 'Thích Lịch Hoả',
		'ten_dich' => 'Lửa sấm sét',
		),	
	23 => array(
		'menh' => 'Mộc',
		'menh_id' => 3,
		'ten_han' => 'Bình Địa Mộc',
		'ten_dich' => 'Cây ở đồng bằng',
		),	
	24 => array(
		'menh' => 'Thổ',
		'menh_id' => 5,
		'ten_han' => 'Đại Trạch Thổ',
		'ten_dich' => 'Đất thuộc 1 khu lớn',
		),	
	25 => array(
		'menh' => 'Hỏa',
		'menh_id' => 4,
		'ten_han' => 'Thiên Thượng Hỏa',
		'ten_dich' => 'Lửa trên trời',
		),	
	30 => array(
		'menh' => 'Thổ',
		'menh_id' => 5,
		'ten_han' => 'Lộ Bàng Thổ',
		'ten_dich' => 'Đất giữa đường',
		),	
	31 => array(
		'menh' => 'Kim',
		'menh_id' => 1,
		'ten_han' => 'Bạch Lạp Kim',
		'ten_dich' => 'Vàng trong nến',
		),	
	32 => array(
		'menh' => 'Mộc',
		'menh_id' => 3,
		'ten_han' => 'Tòng Bá Mộc',
		'ten_dich' => 'Cây tùng bách',
		),	
	33 => array(
		'menh' => 'Thổ',
		'menh_id' => 5,
		'ten_han' => 'Bích Thượng Thổ',
		'ten_dich' => 'Đất trên vách',
		),	
	34 => array(
		'menh' => 'Kim',
		'menh_id' => 1,
		'ten_han' => 'Thoa Xuyến Kim',
		'ten_dich' => 'Vàng trang sức',
		),	
	35 => array(
		'menh' => 'Mộc',
		'menh_id' => 3,
		'ten_han' => 'Thạch Lựu Mộc',
		'ten_dich' => 'Cây thạch lựu',
		),	
	40 => array(
		'menh' => 'Kim',
		'menh_id' => 1,
		'ten_han' => 'Kiếm Phong Kim',
		'ten_dich' => 'Vàng đầu mũi kiếm',
		),	
	41 => array(
		'menh' => 'Mộc',
		'menh_id' => 3,
		'ten_han' => 'Dương Liễu Mộc',
		'ten_dich' => 'Cây dương liễu',
		),	
	42 => array(
		'menh' => 'Thủy',
		'menh_id' => 2,
		'ten_han' => 'Trường Lưu Thủy',
		'ten_dich' => 'Nước chảy dài',
		),	
	43 => array(
		'menh' => 'Kim',
		'menh_id' => 1,
		'ten_han' => 'Kim Bạch Kim',
		'ten_dich' => 'Vàng pha bạch kim',
		),	
	44 => array(
		'menh' => 'Mộc',

		'menh_id' => 3,
		'ten_han' => 'Tang Đố Mộc',
		'ten_dich' => 'Gỗ cây dâu',
		),	
	45 => array(
		'menh' => 'Thủy',
		'menh_id' => 2,
		'ten_han' => 'Đại Hải Thủy',
		'ten_dich' => 'Nước đại dương',
		),	
	);
$arrNguHanhDaySo = array(
		0 => 'Thủy',
		9 => 'Thủy',
		1 => 'Mộc',
		2 => 'Mộc',
		3 => 'Hỏa',
		4 => 'Hỏa',
		5 => 'Thổ',
		6 => 'Thổ',
		7 => 'Kim',
		8 => 'Kim',
		);
$arrNguHanhDaySoSinhKhac = array(
	1 => 'sinh',
	2 => 'sinh',
	3 => 'khac',
	4 => 'khac',
	91 => 'sinh',
	92 => 'sinh',
	93 => 'khac',
	94 => 'khac',
	13 => 'sinh',
	14 => 'sinh',
	15 => 'khac',
	16 => 'khac',
	23 => 'sinh',
	24 => 'sinh',
	25 => 'khac',
	26 => 'khac',
	35 => 'sinh',
	36 => 'sinh',
	37 => 'khac',
	38 => 'khac',
	45 => 'sinh',
	46 => 'sinh',
	47 => 'khac',
	48 => 'khac',
	57 => 'sinh',
	58 => 'sinh',
	50 => 'khac',
	59 => 'khac',
	67 => 'sinh',
	68 => 'sinh',
	60 => 'khac',
	69 => 'khac',
	70 => 'sinh',
	79 => 'sinh',
	71 => 'khac',
	72 => 'khac',
	80 => 'sinh',
	89 => 'sinh',
	81 => 'khac',
	82 => 'khac',
	);
$arrCan = array(
	'0' => "Giáp",
    '1' => "Ất",
    '2' => "Bính",
    '3' => "Đinh",
    '4' => "Mậu",
    '5' => "Kỷ",
    '6' => "Canh",
    '7' => "Tân",
    '8' => "Nhâm",
    '9' => "Quý"
	);
$arrChi = array(
	'0' => 'Tý',
    '1' => 'Sửu',
    '2' => 'Dần',
    '3' => 'Mão',
    '4' => 'Thìn',
    '5' => 'Tỵ',
    '6' => 'Ngọ',
    '7' => 'Mùi',
    '8' => 'Thân',
    '9' => 'Dậu',
    '10' => 'Tuất',
    '11' => 'Hợi',
	);
$arrTuan = array(
	'0' => 'Chủ nhật',
    '1' => 'Thứ hai',
    '2' => 'Thứ ba',
    '3' => 'Thứ tư',
    '4' => 'Thứ năm',
    '5' => 'Thứ sáu',
    '6' => 'Thứ bảy',
	);
$arrGioHoangDao = array(
	'0' => '110100101100',
    '1' => '001101001011',
    '2' => '110011010010',
    '3' => '101100110100',
    '4' => '001011001101',
    '5' => '010010110011',
	);
$arrTietKhi = array(
	'0' => 'Xuân phân',
    '1' => 'Thanh minh',
    '2' => 'Cốc vũ',
    '3' => 'Lập hạ',
    '4' => 'Tiểu mãn',
    '5' => 'Mang chủng',
    '6' => 'Hạ chí',
    '7' => 'Tiểu thử',
    '8' => 'Đại thử',
    '9' => 'Lập thu',
    '10' => 'Xử thử',
    '11' => 'Bạch lộ',
    '12' => 'Thu phân',
    '13' => 'Hàn lộ',
    '14' => 'Sương giáng',
    '15' => 'Lập đông',
    '16' => 'Tiểu tuyết',
    '17' => 'Đại tuyết',
    '18' => 'Đông chí',
    '19' => 'Tiểu hàn',
    '20' => 'Đại hàn',
    '21' => 'Lập xuân',
    '22' => 'Vũ Thủy',
    '23' => 'Kinh trập',
	);

function getCanChi($dd,$mm,$yyyy,$hh = 0){
	global $arrCan,$arrChi,$arrTuan,$arrTietKhi,$arrGioHoangDao;
	
	#+
	$arrReturn = array();
	
	#+
	#+ Ngay Julius va Ngay thang nam am lich
	$am_lich 	= convertSolar2Lunar($dd,$mm,$yyyy);
	$jd 		= jdFromDate($dd,$mm,$yyyy);
	$djd		= $am_lich[0];
	$mjd		= $am_lich[1];
	$yjd 		= $am_lich[2];
	$ljd 		= $am_lich[3];
	
	#+
	$thu_trong_tuan = ($jd + 1) % 7;
	$thu_trong_tuan = $arrTuan[$thu_trong_tuan];
	#+
	$can_gio 	= ($jd - (1-$hh*0.5) )*2%10;
	$can_gio 	= $arrCan[$can_gio];
	#+
	$can_ngay 	= ($jd+9)%10;
	$can_ngay 	= $arrCan[$can_ngay];
	$chi_ngay 	= ($jd+1)%12;
	$chi_ngay	= $arrChi[$chi_ngay];
	#+
	$can_thang	= ($yjd*12+$mjd+3)%10;
	$can_thang	= $arrCan[$can_thang];
	$chi_thang	= ($mjd+1)%12;
	$chi_thang	= $arrChi[$chi_thang];
	#+
	$can_nam	= ($yjd+6)%10;
	$can_nam_id	= $can_nam;
	$can_nam 	= $arrCan[$can_nam];
	$chi_nam	= ($yjd+8)%12;
	$chi_nam_id	= $chi_nam;
	$chi_nam	= $arrChi[$chi_nam];
	#+
	$tiet_khi = getSunLongitude($jd+1,7)*2;
	$tiet_khi = $arrTietKhi[$tiet_khi];
	#+
	$chiOfDay 	= ($jd+1)%12%6;
	$gioHD		= $arrGioHoangDao[$chiOfDay];
	$gioHD		= str_split($gioHD);
	#+
	$gio_hoang_dao = '';
	$j = 0;
	for($i=0; $i<12; $i++){
		if ($gioHD[$i] == '1') {
			$j++;
			$gio_hoang_dao .= $j != 1 ? ', ' : '';
			$gio_hoang_dao .= $arrChi[$i];
			$gio_hoang_dao .= ' ('. ($i*2+23)%24 . '-' . ($i*2+1)%24 . ')';

		} // End if ($gioHD[$i] == '1')
	} // End for($i=0; $i<12; $i++)

	$arrReturn['thu_trong_tuan'] 		= $thu_trong_tuan;
	$arrReturn['ngay_duong_lich'] 		= $dd;
	$arrReturn['thang_duong_lich'] 		= $mm;
	$arrReturn['nam_duong_lich'] 		= $yyyy;

	$arrReturn['ngay_am_lich'] 			= $djd;
	$arrReturn['thang_am_lich'] 		= $mjd;
	$arrReturn['nam_am_lich'] 			= $yjd;
	$arrReturn['nam_nhuan'] 			= $ljd;

	$arrReturn['can_ngay'] 				= $can_ngay;
	$arrReturn['chi_ngay'] 				= $chi_ngay;
	$arrReturn['can_thang'] 			= $can_thang;
	$arrReturn['chi_thang'] 			= $chi_thang;
	$arrReturn['can_nam_id'] 			= $can_nam_id;
	$arrReturn['can_nam'] 				= $can_nam;
	$arrReturn['chi_nam'] 				= $chi_nam;
	$arrReturn['chi_nam_id'] 			= $chi_nam_id;
	
	$arrReturn['can_gio'] 				= $can_gio;

	$arrReturn['tiet_khi'] 				= $tiet_khi;
	$arrReturn['gio_hoang_dao'] 		= $gio_hoang_dao;

	return $arrReturn;
} // End function getCanChi


#+
#+ Đầu theo ngoại quái
#+ Đuôi theo nội quái
$arrBatQuai = array(
	0 => 'Khôn',
	1 => 'Càn',
	2 => 'Đoài',
	3 => 'Ly',
	4 => 'Chấn',
	5 => 'Tốn',
	6 => 'Khảm',
	7 => 'Cấn',
	);
#+
$arrQueCH = array(
	'0' => array(
		'ten_que' => 'Trung lập',
		'mo_ta_que' => 'Quẻ không phân định rõ cát hung, có thể chấp nhận được',
		),
	'1' => array(
		'ten_que' => 'Cát',
		'mo_ta_que' => 'Quẻ mang điềm cát, rất tốt.',
		),
	'2' => array(
		'ten_que' => 'Hung',
		'mo_ta_que' => 'Quẻ mang điềm hung, không tốt.',
		),
	);
#+
$arrQue = array(
	0 => array(
		'so_que' => 2,
		'ngoai_quai' => 0,
		'noi_quai' => 0,
		'ten_que' => 'Thuần Khôn',
		'ten_que_han' => '坤 kūn',
		'mo_ta_que' => 'Thuận dã. Nhu thuận. Thuận tòng, mềm dẻo, theo đường mà được lợi, hòa theo lẽ, chịu lấy.',
		'trang_thai_que' => 1,
		'que_ho_id' => 0,
		),
	1 => array(
		'so_que' => 11,
		'ngoai_quai' => 0,
		'noi_quai' => 1,
		'ten_que' => 'Địa Thiên Thái',
		'ten_que_han' => '泰 tài',
		'mo_ta_que' => 'Thông dã. Điều hòa. Thông hiểu, am tường, hiểu biết, thông suốt, quen biết, quen thuộc.',
		'trang_thai_que' => 1,
		'que_ho_id' => 42,
		),
	2 => array(
		'so_que' => 19,
		'ngoai_quai' => 0,
		'noi_quai' => 2,
		'ten_que' => 'Địa Trạch Lâm',
		'ten_que_han' => '臨 lín',
		'mo_ta_que' => 'Đại dã. Bao quản. Việc lớn, người lớn, cha nuôi, vú nuôi, giáo học, nhà sư, kẻ cả, dạy dân, nhà thầu.',
		'trang_thai_que' => 1,
		'que_ho_id' => 4,
		),
	3 => array(
		'so_que' => 36,
		'ngoai_quai' => 0,
		'noi_quai' => 3,
		'ten_que' => 'Địa Hỏa Minh Di',
		'ten_que_han' => '明夷 míng yí',
		'mo_ta_que' => 'Thương dã. Hại đau. Thương tích, bệnh hoạn, buồn lo, đau lòng, ánh sáng bị tổn thương.',
		'trang_thai_que' => 2,
		'que_ho_id' => 46,
		),
	4 => array(
		'so_que' => 24,
		'ngoai_quai' => 0,
		'noi_quai' => 4,
		'ten_que' => 'Địa Lôi Phục',
		'ten_que_han' => '復 fù',
		'mo_ta_que' => 'Phản dã. Tái hồi. Lại có, trở về, bên ngoài, phản phục.',
		'trang_thai_que' => 0,
		'que_ho_id' => 0,
		),
	5 => array(
		'so_que' => 46,
		'ngoai_quai' => 0,
		'noi_quai' => 5,
		'ten_que' => 'Địa Phong Thăng',
		'ten_que_han' => '升 shēng',
		'mo_ta_que' => 'Tiến dã. Tiến thủ. Thăng tiến, trực chỉ, tiến mau, bay lên, vọt tới trước, bay lên không trung, thăng chức, thăng hà. ',
		'trang_thai_que' => 1,
		'que_ho_id' => 42,
		),
	6 => array(
		'so_que' => 7,
		'ngoai_quai' => 0,
		'noi_quai' => 6,
		'ten_que' => 'Địa Thủy Sư',
		'ten_que_han' => '師 shī',
		'mo_ta_que' => 'Chúng dã. Chúng trợ. Đông chúng, vừa làm thầy, vừa làm bạn, học hỏi lẫn nhau, nắm tay nhau qua truông, nâng đỡ',
		'trang_thai_que' => 1,
		'que_ho_id' => 4,
		),
	7 => array(
		'so_que' => 15,
		'ngoai_quai' => 0,
		'noi_quai' => 7,
		'ten_que' => 'Địa Sơn Khiêm',
		'ten_que_han' => '謙 qiān',
		'mo_ta_que' => 'Thoái dã. Cáo thoái. Khiêm tốn, nhún nhường, khiêm từ, cáo thoái, từ giã, lui vào trong, giữ gìn, nhốt vào trong, đóng cửa.',
		'trang_thai_que' => 0,
		'que_ho_id' => 46,
		),
	10 => array(
		'so_que' => 12,
		'ngoai_quai' => 1,
		'noi_quai' => 0,
		'ten_que' => 'Thiên Địa Bĩ',
		'ten_que_han' => '否 pǐ',
		'mo_ta_que' => 'Tắc dã. Gián cách. Bế tắc, không thông, không tương cảm nhau, xui xẻo, dèm pha, chê bai lẫn nhau, mạnh ai nấy theo ý riêng.',
		'trang_thai_que' => 2,
		'que_ho_id' => 57,
		),
	11 => array(
		'so_que' => 1,
		'ngoai_quai' => 1,
		'noi_quai' => 1,
		'ten_que' => 'Thuần Càn',
		'ten_que_han' => '乾 qián',
		'mo_ta_que' => 'Chính yếu. Cứng mạnh, khô, lớn, khỏe mạnh, đức không nghỉ . tượng vạn vật có khởi đầu, lớn lên, toại chí, hóa thành.',
		'trang_thai_que' => 1,
		'que_ho_id' => 11,
		),
	12 => array(
		'so_que' => 10,
		'ngoai_quai' => 1,
		'noi_quai' => 2,
		'ten_que' => 'Thiên Trạch Lý',
		'ten_que_han' => '履 lǚ',
		'mo_ta_que' => 'Lễ dã. Lộ hành. Nghi lễ, có chừng mực, khuôn phép, dẫm lên, không cho đi sai, có ý chặn đường thái quá, hệ thống, pháp lý.',
		'trang_thai_que' => 1,
		'que_ho_id' => 53,
		),
	13 => array(
		'so_que' => 13,
		'ngoai_quai' => 1,
		'noi_quai' => 3,
		'ten_que' => 'Thiên Hỏa Đồng Nhân',
		'ten_que_han' => '同人 tóng rén',
		'mo_ta_que' => 'Thân dã. Thân thiện. Trên dưới cùng lòng, cùng người ưa thích, cùng một bọn người.',
		'trang_thai_que' => 1,
		'que_ho_id' => 15,
		),
	14 => array(
		'so_que' => 25,
		'ngoai_quai' => 1,
		'noi_quai' => 4,
		'ten_que' => 'Thiên Lôi Vô Vọng',
		'ten_que_han' => '無妄 wú wàng',
		'mo_ta_que' => 'Thiên tai dã. Xâm lấn. Tai vạ, lỗi bậy bạ, không lề lối, không quy củ, càn đại, chống đối, hứng chịu.',
		'trang_thai_que' => 2,
		'que_ho_id' => 57,
		),
	15 => array(
		'so_que' => 44,
		'ngoai_quai' => 1,
		'noi_quai' => 5,
		'ten_que' => 'Thiên Phong Cấu',
		'ten_que_han' => '姤 gòu',
		'mo_ta_que' => 'Ngộ dã. Tương ngộ. Gặp gỡ, cấu kết, liên kết, kết hợp, móc nối, mềm gặp cứng.',
		'trang_thai_que' => 0,
		'que_ho_id' => 11,
		),
	16 => array(
		'so_que' => 6,
		'ngoai_quai' => 1,
		'noi_quai' => 6,
		'ten_que' => 'Thiên Thủy Tụng',
		'ten_que_han' => '訟 sòng',
		'mo_ta_que' => 'Luận dã. Bất hòa. Bàn cãi, kiện tụng, bàn tính, cãi vã, tranh luận, bàn luận.',
		'trang_thai_que' => 2,
		'que_ho_id' => 53,
		),
	17 => array(
		'so_que' => 33,
		'ngoai_quai' => 1,
		'noi_quai' => 7,
		'ten_que' => 'Thiên Sơn Độn',
		'ten_que_han' => '(遯 dùn',
		'mo_ta_que' => 'Thoái dã. Ẩn trá. Lui, ẩn khuất, tránh đời, lừa dối, trá hình, có ý trốn tránh, trốn cái mặt thấy cái lưng.',
		'trang_thai_que' => 2,
		'que_ho_id' => 15,
		),
	20 => array(
		'so_que' => 45,		
		'ngoai_quai' => 2,
		'noi_quai' => 0,
		'ten_que' => 'Trạch Địa Tụy',
		'ten_que_han' => '萃 cuì',
		'mo_ta_que' => 'Nhóm họp, biểu tình, dồn đống, quần tụ nhau lại, kéo đến, kéo thành bầy.',
		'trang_thai_que' => 0,
		'que_ho_id' => 11,
		),
	21 => array(
		'so_que' => 43,
		'ngoai_quai' => 2,
		'noi_quai' => 1,
		'ten_que' => 'Trạch Thiên Quải',
		'ten_que_han' => '夬 guài',
		'mo_ta_que' => 'Quyết dã. Dứt khoát. Dứt hết, biên cương, ranh giới, thành phần, thành khoảnh, quyết định, quyết nghị, cổ phần, thôi, khai lề lối.',
		'trang_thai_que' => 0,
		'que_ho_id' => 11,
		),
	22 => array(
		'so_que' => 58,
		'ngoai_quai' => 2,
		'noi_quai' => 2,
		'ten_que' => 'Thuần Đoài',
		'ten_que_han' => '兌 duì',
		'mo_ta_que' => 'Duyệt dã. Hiện đẹp. Đẹp đẽ, ưa thích, vui hiện trên mặt, không buồn chán, cười nói, khuyết mẻ.',
		'trang_thai_que' => 1,
		'que_ho_id' => 53,
		),
	23 => array(
		'so_que' => 49,
		'ngoai_quai' => 2,
		'noi_quai' => 3,
		'ten_que' => 'Trạch Hỏa Cách',
		'ten_que_han' => '革 gé',
		'mo_ta_que' => 'Cải dã. Cải biến. Bỏ lối cũ, cải cách, hoán cải, cách tuyệt, cánh chim thay lông.',
		'trang_thai_que' => 1,
		'que_ho_id' => 15,
		),
	24 => array(
		'so_que' => 17,
		'ngoai_quai' => 2,
		'noi_quai' => 4,
		'ten_que' => 'Trạch Lôi Tùy',
		'ten_que_han' => '隨 suí',
		'mo_ta_que' => 'Thuận dã. Di động. Cùng theo, mặc lòng, không có chí hướng, chỉ chiều theo, đại thể chủ việc di động, thuyên chuyển như chiếc xe.',
		'trang_thai_que' => 2,
		'que_ho_id' => 57,
		),
	25 => array(
		'so_que' => 28,
		'ngoai_quai' => 2,
		'noi_quai' => 5,
		'ten_que' => 'Trạch Phong Đại Quá',
		'ten_que_han' => '大過 dà guò',
		'mo_ta_que' => 'Họa dã. Cả quá. Cả quá ắt tai họa, quá mực thường, quá nhiều, giàu cương nghị ở trong.',
		'trang_thai_que' => 0,
		'que_ho_id' => 11,
		),
	26 => array(
		'so_que' => 47,
		'ngoai_quai' => 2,
		'noi_quai' => 6,
		'ten_que' => 'Trạch Thủy Khốn',
		'ten_que_han' => '困 kùn',
		'mo_ta_que' => 'Nguy dã. Nguy lo. Cùng quẫn, bị người làm ác, lo lắng, cùng khổ, mệt mỏi, nguy cấp, lo hiểm nạn.',
		'trang_thai_que' => 2,
		'que_ho_id' => 53,
		),
	27 => array(
		'so_que' => 31,
		'ngoai_quai' => 2,
		'noi_quai' => 7,
		'ten_que' => 'Trạch Sơn Hàm',
		'ten_que_han' => '咸 xián',
		'mo_ta_que' => 'Cảm dã. Thụ cảm. Cảm xúc, thọ nhận, cảm ứng, nghĩ đến, nghe thấy, xúc động.',
		'trang_thai_que' => 1,
		'que_ho_id' => 15,
		),
	30 => array(
		'so_que' => 35,		
		'ngoai_quai' => 3,
		'noi_quai' => 0,
		'ten_que' => 'Hỏa Địa Tấn',
		'ten_que_han' => '晉 jìn',
		'mo_ta_que' => 'Tiến dã. Hiển hiện. Đi hoặc tới, tiến tới gần, theo mực thường, lửa đã hiện trên đất, trưng bày.',
		'trang_thai_que' => 1,
		'que_ho_id' => 67,
		),
	31 => array(
		'so_que' => 14,
		'ngoai_quai' => 3,
		'noi_quai' => 1,
		'ten_que' => 'Hỏa Thiên Đại Hữu',
		'ten_que_han' => '大有 dà yǒu',
		'mo_ta_que' => 'Khoan dã. Cả có. Thong dong, dung dưỡng nhiều, độ lượng rộng, có đức dầy, chiếu sáng lớn.',
		'trang_thai_que' => 0,
		'que_ho_id' => 21,
		),
	32 => array(
		'so_que' => 38,
		'ngoai_quai' => 3,
		'noi_quai' => 2,
		'ten_que' => 'Hỏa Trạch Khuê',
		'ten_que_han' => '睽 kuí',
		'mo_ta_que' => 'Quai dã. Hỗ trợ. Trái lìa, lìa xa, hai bên lợi dụng lẫn nhau, cơ biến quai xảo, như cung tên.',
		'trang_thai_que' => 2,
		'que_ho_id' => 63,
		),
	33 => array(
		'so_que' => 30,
		'ngoai_quai' => 3,
		'noi_quai' => 3,
		'ten_que' => 'Thuần Ly',
		'ten_que_han' => '(離 lí',
		'mo_ta_que' => 'Lệ dã. Sáng chói. Sáng sủa, trống trải, trống trơn, tỏa ra, bám vào, phụ bám, phô trương ra ngoài.',
		'trang_thai_que' => 0,
		'que_ho_id' => 25,
		),
	34 => array(
		'so_que' => 21,
		'ngoai_quai' => 3,
		'noi_quai' => 4,
		'ten_que' => 'Hỏa Lôi Phệ Hạp',
		'ten_que_han' => '噬嗑 shì kè',
		'mo_ta_que' => 'Khiết dã. Cấn hợp. Cẩu hợp, bấu víu, bấu quào, dày xéo, đay nghiến, phỏng vấn, hỏi han (học hỏi)',
		'trang_thai_que' => 2,
		'que_ho_id' => 67,
		),
	35 => array(
		'so_que' => 50,
		'ngoai_quai' => 3,
		'noi_quai' => 5,
		'ten_que' => 'Hỏa Phong Đỉnh',
		'ten_que_han' => '鼎 dǐng',
		'mo_ta_que' => 'Định dã. Nung đúc. Đứng được, chậm đứng, trồng, nung nấu, rèn luyện, vững chắc, ước hẹn.',
		'trang_thai_que' => 1,
		'que_ho_id' => 21,
		),
	36 => array(
		'so_que' => 64,
		'ngoai_quai' => 3,
		'noi_quai' => 6,
		'ten_que' => 'Hỏa Thủy Vị Tế',
		'ten_que_han' => '未濟 wèi jì',
		'mo_ta_que' => 'Thất dã. Thất cách. Thất bát, mất, thất bại, dở dang, chưa xong, nửa chừng.',
		'trang_thai_que' => 2,
		'que_ho_id' => 63,
		),
	37 => array(
		'so_que' => 56,
		'ngoai_quai' => 3,
		'noi_quai' => 7,
		'ten_que' => 'Hỏa Sơn Lữ',
		'ten_que_han' => '旅 lǚ',
		'mo_ta_que' => 'Khách dã. Thứ yếu. Đỗ nhờ, khách, ở đậu, tạm trú, kê vào, gá vào, ký ngụ bên ngoài, tính cách lang thang, ít người thân, không chính.',
		'trang_thai_que' => 2,
		'que_ho_id' => 25,
		),
	40 => array(
		'so_que' => 16,		
		'ngoai_quai' => 4,
		'noi_quai' => 0,
		'ten_que' => 'Lôi Địa Dự',
		'ten_que_han' => '豫 yù',
		'mo_ta_que' => 'Duyệt dã. Thuận động. Dự bị, dự phòng, canh chừng, sớm, vui vầy.',
		'trang_thai_que' => 1,
		'que_ho_id' => 67,
		),
	41 => array(
		'so_que' => 34,
		'ngoai_quai' => 4,
		'noi_quai' => 1,
		'ten_que' => 'Lôi Thiên Đại Tráng',
		'ten_que_han' => '大壯 dà zhuàng',
		'mo_ta_que' => 'Chí dã. Tự cường. Ý chí riêng, bụng nghĩ, hướng thượng, ý định, vượng sức, thịnh đại, trên cao, chót vót, lên trên, chí khí, có lập trường.',
		'trang_thai_que' => 1,
		'que_ho_id' => 21,
		),
	42 => array(
		'so_que' => 54,
		'ngoai_quai' => 4,
		'noi_quai' => 2,
		'ten_que' => 'Lôi Trạch Quy Muội',
		'ten_que_han' => '歸妹 guī mèi',
		'mo_ta_que' => 'Tai dã. Xôn xao. Tai nạn, rối ren, lôi thôi, nữ chi chung, gái lấy chồng.',
		'trang_thai_que' => 2,
		'que_ho_id' => 63,
		),
	43 => array(
		'so_que' => 55,
		'ngoai_quai' => 4,
		'noi_quai' => 3,
		'ten_que' => 'Lôi Hỏa Phong',
		'ten_que_han' => '豐 fēng',
		'mo_ta_que' => 'Thịnh dã. Hòa mỹ. Thịnh đại, được mùa, nhiều người góp sức.',
		'trang_thai_que' => 1,
		'que_ho_id' => 25,
		),
	44 => array(
		'so_que' => 51,
		'ngoai_quai' => 4,
		'noi_quai' => 4,
		'ten_que' => 'Thuần Chấn',
		'ten_que_han' => '震 zhèn',
		'mo_ta_que' => 'Động dã. Động dụng. Rung động, sợ hãi do chấn động, phấn phát, nổ vang, phấn khởi, chấn kinh.',
		'trang_thai_que' => 0,
		'que_ho_id' => 67,
		),
	45 => array(
		'so_que' => 32,
		'ngoai_quai' => 4,
		'noi_quai' => 5,
		'ten_que' => 'Lôi Phong Hằng',
		'ten_que_han' => '恆 héng',
		'mo_ta_que' => 'Cửu dã. Trường cửu. Lâu dài, chậm chạp, đạo lâu bền như vợ chồng, kéo dài câu chuyện, thâm giao, nghĩa cố tri, xưa, cũ.',
		'trang_thai_que' => 1,
		'que_ho_id' => 21,
		),
	46 => array(
		'so_que' => 40,
		'ngoai_quai' => 4,
		'noi_quai' => 6,
		'ten_que' => 'Lôi Thủy Giải',
		'ten_que_han' => '解 xiè',
		'mo_ta_que' => 'Tán dã. Nơi nơi. Làm cho tan đi, như làm tan sự nguy hiểm, giải phóng, giải tán, loan truyền, tuyên truyền, phân phát, lưu thông, ban rải, ân xá.',
		'trang_thai_que' => 1,
		'que_ho_id' => 63,
		),
	47 => array(
		'so_que' => 62,
		'ngoai_quai' => 4,
		'noi_quai' => 7,
		'ten_que' => 'Lôi Sơn Tiểu Quá',
		'ten_que_han' => '小過 xiǎo guò',
		'mo_ta_que' => 'Quá dã. Bất túc. Thiểu lý, thiểu não, hèn mọn, nhỏ nhặt, bẩn thỉu, thiếu cường lực.',
		'trang_thai_que' => 2,
		'que_ho_id' => 25,
		),
	50 => array(
		'so_que' => 20,		
		'ngoai_quai' => 5,
		'noi_quai' => 0,
		'ten_que' => 'Phong Địa Quan',
		'ten_que_han' => '觀 guān',
		'mo_ta_que' => 'Quan dã. Quan sát. Xem xét, trông coi, cảnh tượng xem thấy, thanh tra, lướt qua, sơ qua, sơn phết, quét nhà.',
		'trang_thai_que' => 0,
		'que_ho_id' => 70,
		),
	51 => array(
		'so_que' => 9,
		'ngoai_quai' => 5,
		'noi_quai' => 1,
		'ten_que' => 'Phong Thiên Tiểu Súc',
		'ten_que_han' => '小畜 xiǎo chù',
		'mo_ta_que' => 'Tắc dã. Dị đồng. Lúc bế tắc, không đồng ý nhau, cô quả, súc oán, chứa mọi oán giận, có ý trái lại, không hòa hợp, nhỏ nhen',
		'trang_thai_que' => 2,
		'que_ho_id' => 32,
		),
	52 => array(
		'so_que' => 61,
		'ngoai_quai' => 5,
		'noi_quai' => 2,
		'ten_que' => 'Phong Trạch Trung Phu',
		'ten_que_han' => '中孚 zhōng fú',
		'mo_ta_que' => 'Tín dã. Trung thật. Tín thật, không ngờ vực, có uy tín cho người tin tưởng, tín ngưỡng, ở trong.',
		'trang_thai_que' => 1,
		'que_ho_id' => 74,
		),
	53 => array(
		'so_que' => 37,
		'ngoai_quai' => 5,
		'noi_quai' => 3,
		'ten_que' => 'Phong Hỏa Gia Nhân',
		'ten_que_han' => '家人 jiā rén',
		'mo_ta_que' => 'Đồng dã. Nảy nở. Người nhà, gia đinh, cùng gia đình, đồng chủng, đồng nghiệp, cùng xóm, sinh sôi, khai thác mở mang thêm.',
		'trang_thai_que' => 1,
		'que_ho_id' => 36,
		),
	54 => array(
		'so_que' => 42,
		'ngoai_quai' => 5,
		'noi_quai' => 4,
		'ten_que' => 'Phong Lôi Ích',
		'ten_que_han' => '益 yì',
		'mo_ta_que' => 'Ích dã. Tiến ích. Thêm được lợi, giúp dùm, tiếng dội xa, vượt lên, phóng mình tới.',
		'trang_thai_que' => 1,
		'que_ho_id' => 70,
		),
	55 => array(
		'so_que' => 57,
		'ngoai_quai' => 5,
		'noi_quai' => 5,
		'ten_que' => 'Thuần Tốn',
		'ten_que_han' => '巽 xùn',
		'mo_ta_que' => 'Thuận dã. Thuận nhập. Theo lên theo xuống, theo tới theo lui, có sự giấu diếm ở trong.',
		'trang_thai_que' => 0,
		'que_ho_id' => 32,
		),
	56 => array(
		'so_que' => 59,
		'ngoai_quai' => 5,
		'noi_quai' => 6,
		'ten_que' => 'Phong Thủy Hoán',
		'ten_que_han' => '渙 huàn',
		'mo_ta_que' => 'Tán dã. Ly tán. Lan ra tràn lan, tán thất, trốn đi xa, lánh xa, thất nhân tâm, hao bớt.',
		'trang_thai_que' => 2,
		'que_ho_id' => 74,
		),
	57 => array(
		'so_que' => 53,
		'ngoai_quai' => 5,
		'noi_quai' => 7,
		'ten_que' => 'Phong Sơn Tiệm',
		'ten_que_han' => '漸 jiàn',
		'mo_ta_que' => 'Tiến dã. Tuần tự. Từ từ, thong thả đến, lần lần, bò tới, chậm chạp, nhai nhỏ nuốt vào, phúc lộc cùng đến.',
		'trang_thai_que' => 1,
		'que_ho_id' => 36,
		),
	60 => array(
		'so_que' => 8,		
		'ngoai_quai' => 6,
		'noi_quai' => 0,
		'ten_que' => 'Thủy Địa Tỷ',
		'ten_que_han' => '比 bǐ',
		'mo_ta_que' => 'Tư dã. Chọn lọc. Thân liền, gạn lọc, mật thiết, tư hữu riêng, trưởng đoàn, trưởng toán, chọn lựa.',
		'trang_thai_que' => 1,
		'que_ho_id' => 70,
		),
	61 => array(
		'so_que' => 5,
		'ngoai_quai' => 6,
		'noi_quai' => 1,
		'ten_que' => 'Thủy Thiên Nhu',
		'ten_que_han' => '需 xū',
		'mo_ta_que' => 'Thuận dã. Tương hội. Chờ đợi vì hiểm đằng trước, thuận theo, quây quần, tụ hội, vui hội, cứu xét, chầu về',
		'trang_thai_que' => 1,
		'que_ho_id' => 32,
		),
	62 => array(
		'so_que' => 60,
		'ngoai_quai' => 6,
		'noi_quai' => 2,
		'ten_que' => 'Thủy Trạch Tiết',
		'ten_que_han' => '節 jié',
		'mo_ta_que' => 'Chỉ dã. Giảm chế. Ngăn ngừa, tiết độ, kiềm chế, giảm bớt, chừng mực, nhiều thì tràn.',
		'trang_thai_que' => 1,
		'que_ho_id' => 74,
		),
	63 => array(
		'so_que' => 63,
		'ngoai_quai' => 6,
		'noi_quai' => 3,
		'ten_que' => 'Thủy Hỏa Ký Tế',
		'ten_que_han' => '既濟 jì jì',
		'mo_ta_que' => 'Hợp dã. Hiện hợp. Gặp nhau, cùng nhau, đã xong, việc xong, hiện thực, ích lợi nhỏ. ',
		'trang_thai_que' => 1,
		'que_ho_id' => 36,
		),
	64 => array(
		'so_que' => 3,
		'ngoai_quai' => 6,
		'noi_quai' => 4,
		'ten_que' => 'Thủy Lôi Truân',
		'ten_que_han' => '屯 chún',
		'mo_ta_que' => 'Nạn dã. Gian lao. Yếu đuối, chưa đủ sức, ngần ngại, do dự, vất vả, phải nhờ sự giúp đỡ',
		'trang_thai_que' => 2,
		'que_ho_id' => 70,
		),
	65 => array(
		'so_que' => 48,
		'ngoai_quai' => 6,
		'noi_quai' => 5,
		'ten_que' => 'Thủy Phong Tỉnh',
		'ten_que_han' => '井 jǐng',
		'mo_ta_que' => 'Tịnh dã. Trầm lặng. Ở chỗ nào cứ ở yên chỗ đó, xuống sâu, vực thẳm có nước, dưới sâu, cái giếng.',
		'trang_thai_que' => 0,
		'que_ho_id' => 32,
		),
	66 => array(
		'so_que' => 29,
		'ngoai_quai' => 6,
		'noi_quai' => 6,
		'ten_que' => 'Thuần Khảm',
		'ten_que_han' => '坎 kǎn',
		'mo_ta_que' => 'Hãm dã. Hãm hiểm. Hãm vào ở trong, xuyên sâu vào trong, đóng cửa lại, gập ghềnh, trắc trở, bắt buộc, kìm hãm, thắng.',
		'trang_thai_que' => 0,
		'que_ho_id' => 74,
		),
	67 => array(
		'so_que' => 39,
		'ngoai_quai' => 6,
		'noi_quai' => 7,
		'ten_que' => 'Thủy Sơn Kiển',
		'ten_que_han' => '蹇 jiǎn',
		'mo_ta_que' => 'Nạn dã. Trở ngại. Cản ngăn, chặn lại, chậm chạp, què, khó khăn. ',
		'trang_thai_que' => 2,
		'que_ho_id' => 36,
		),
	70 => array(
		'so_que' => 23,		
		'ngoai_quai' => 7,
		'noi_quai' => 0,
		'ten_que' => 'Sơn Địa Bác',
		'ten_que_han' => '剝 bō',
		'mo_ta_que' => 'Lạc dã. Tiêu điều. Đẽo gọt, lột cướp đi, không có lợi, rụng rớt, đến rồi lại đi, tản lạc, lạt lẽo nhau, xa lìa nhau, hoang vắng, buồn thảm',
		'trang_thai_que' => 2,
		'que_ho_id' => 0,
		),
	71 => array(
		'so_que' => 26,
		'ngoai_quai' => 7,
		'noi_quai' => 1,
		'ten_que' => 'Sơn Thiên Đại Súc',
		'ten_que_han' => '大畜 dà chù',
		'mo_ta_que' => 'Tụ dã. Tích tụ. Chứa tụ, súc tích, lắng tụ một chỗ, dự trữ, đựng, để dành.',
		'trang_thai_que' => 1,
		'que_ho_id' => 42,
		),
	72 => array(
		'so_que' => 41,
		'ngoai_quai' => 7,
		'noi_quai' => 2,
		'ten_que' => 'Sơn Trạch Tổn',
		'ten_que_han' => '損 sǔn',
		'mo_ta_que' => 'Thất dã. Tổn hại. Hao mất, thua thiệt, bớt kém, bớt phần dưới cho phần trên là tổn hại.',
		'trang_thai_que' => 2,
		'que_ho_id' => 4,
		),
	73 => array(
		'so_que' => 22,
		'ngoai_quai' => 7,
		'noi_quai' => 3,
		'ten_que' => 'Sơn Hỏa Bí',
		'ten_que_han' => '賁 bì',
		'mo_ta_que' => 'Sức dã. Quang minh. Trang sức, sửa sang, trang điểm, thấu suốt, rõ ràng.',
		'trang_thai_que' => 1,
		'que_ho_id' => 46,
		),
	74 => array(
		'so_que' => 27,
		'ngoai_quai' => 7,
		'noi_quai' => 4,
		'ten_que' => 'Sơn Lôi Di',
		'ten_que_han' => '頤 yí',
		'mo_ta_que' => 'Dưỡng dã. Dung dưỡng. Chăm lo, tu bổ, thêm, ăn uống, bổ dưỡng, bồi dưỡng, ví như trời nuôi muôn vật, thánh nhân nuôi người.',
		'trang_thai_que' => 1,
		'que_ho_id' => 0,
		),
	75 => array(
		'so_que' => 18,
		'ngoai_quai' => 7,
		'noi_quai' => 5,
		'ten_que' => 'Sơn Phong Cổ',
		'ten_que_han' => '蠱 gǔ',
		'mo_ta_que' => 'Sự dã. Sự biến. Có sự không yên trong lòng, làm ngờ vực, khua, đánh, mua chuốc lấy cái hại, đánh trống, làm cho sợ sệt, sửa lại cái lỗi trước đã làm',
		'trang_thai_que' => 2,
		'que_ho_id' => 42,
		),
	76 => array(
		'so_que' => 4,
		'ngoai_quai' => 7,
		'noi_quai' => 6,
		'ten_que' => 'Sơn Thủy Mông',
		'ten_que_han' => '蒙 méng',
		'mo_ta_que' => 'Muội dã. Bất minh. Tối tăm, mờ ám, không minh bạch, che lấp, bao trùm, phủ chụp, ngu dại, ngờ nghệch',
		'trang_thai_que' => 2,
		'que_ho_id' => 4,
		),
	77 => array(
		'so_que' => 52,
		'ngoai_quai' => 7,
		'noi_quai' => 7,
		'ten_que' => 'Thuần Cấn',
		'ten_que_han' => '艮 gèn',
		'mo_ta_que' => 'Chỉ dã. Ngưng nghỉ. Ngăn giữ, ở, thôi, dừng lại, đậy lại, gói ghém, ngăn cấm, vừa đúng chỗ. ',
		'trang_thai_que' => 0,
		'que_ho_id' => 46,
		),
	);
?>