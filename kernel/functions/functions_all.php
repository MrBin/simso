<?
function getDataSim2 ($str) {
	$arrCheck = array(".", " ");
	$arrReplace = array("", "");

	$str = str_replace($arrCheck, $arrReplace, $str);

	$str = (int)$str;

	return $str;
}
function generate_filter($arrOption, $name, $value){
	$strReturn	= '<select class="form_control" name="' . $name . '">';
	foreach($arrOption as $k => $v) $strReturn	.= '<option value="' . $k . '"' . ($k == $value ? ' selected="selected"' : '') . '>' . $v . '</option>';
	$strReturn	.= '</select>';
	return $strReturn;
}
//Đổi resultset ra dạng list
function convert_result_set_2_list($result_set, $id_field, $start_value="0", $type=0){
	$str_return = $start_value;

	//Move first resultset
	if (mysql_num_rows($result_set) > 0) mysql_data_seek($result_set, 0);
	while ($row_t = mysql_fetch_assoc($result_set)){
		switch($type){
			case 1 : if($row_t[$id_field] > 0) $str_return .= "," . $row_t[$id_field]; break;
			default: $str_return .= "," . $row_t[$id_field]; break;
		}
	}

	//Sau khi loop move first lại từ đầu
	if (mysql_num_rows($result_set) > 0) mysql_data_seek($result_set, 0);
	return $str_return;
}
function convert_list_to_list_id($string, $do_not_get_zero_value=1){
	// Bẻ $string để intval lại
	$arrTemp	= explode(",", $string);
	$list_id	= "";
	foreach($arrTemp as $key => $value){
		$value	= intval($value);
		if($do_not_get_zero_value == 1 && $value <= 0) continue;
		$list_id .= "," . $value;
	}
	// Remove dấu , đầu tiên
	if(strlen($list_id) > 0) $list_id = substr($list_id, 1);
	else $list_id	= 0;
	return $list_id;
}
function setCookieAdminLogin($admin_id){
	$private		= "wix2k0n7vc1dfx";
	$time			= time();
	$checksum	= md5($private . $admin_id . $time);
	setcookie("admin_login_id", $admin_id, 0, "/", "", false, true);
	setcookie("admin_login_time", $time, 0, "/", "", false, true);
	setcookie("admin_login_checksum", $checksum, 0, "/", "", false, true);
}

function checkCookieAdminLogin(){
	$private					= "wix2k0n7vc1dfx";
	$admin_login_id		= getValue("admin_login_id", "int", "COOKIE", 0);
	$admin_login_time		= getValue("admin_login_time", "int", "COOKIE", 0);
	$admin_login_checksum= getValue("admin_login_checksum", "str", "COOKIE", "");
	$checksum				= md5($private . $admin_login_id . $admin_login_time);
	return ($checksum == $admin_login_checksum ? $admin_login_id : 0);
}

function clearCookieAdminLogin(){
	if(isset($_COOKIE["admin_login_id"])){ unset($_COOKIE["admin_login_id"]); setcookie("admin_login_id", "", time() - 3600, "/"); }
	if(isset($_COOKIE["admin_login_time"])){ unset($_COOKIE["admin_login_time"]); setcookie("admin_login_time", "", time() - 3600, "/"); }
	if(isset($_COOKIE["admin_login_checksum"])){ unset($_COOKIE["admin_login_checksum"]); setcookie("admin_login_checksum", "", time() - 3600, "/"); }
}
function replace_double_space($string, $char=" "){
	$i		= 0;
	$max	= 10;
	if($char == "") return $string;
	while(mb_strpos($string, $char.$char, 0, "UTF-8") !== false){
		$i++;
		$string	= str_replace($char.$char, $char, $string);
		if($i >= $max) break;
	}
	return trim($string);
}

function getUserIpAddr(){
	$ip = 0;

	if(!empty($_SERVER['HTTP_CLIENT_IP'])){
		//ip from share internet
		$ip = $_SERVER['HTTP_CLIENT_IP'];
	}elseif(!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
		//ip pass from proxy
		$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
	}else{
		$ip = $_SERVER['REMOTE_ADDR'];
	}

	if($ip == "::1") $ip = 0;
	
	return $ip;
}
/**
Base64 để hiển thị trên URL để tránh lỗi
*/
function base64_url_encode($input){
	return strtr(base64_encode($input), '+/=', '_,-');
}

function base64_url_decode($input) {
	return base64_decode(strtr($input, '_,-', '+/='));
}
function cut_string2($str, $length, $char="..."){
	//Nếu chuỗi cần cắt nhỏ hơn $length thì return luôn
	$strlen	= mb_strlen($str, "UTF-8");
	if($strlen <= $length) return $str;

	//Cắt chiều dài chuỗi $str tới đoạn cần lấy
	$substr	= mb_substr($str, 0, $length, "UTF-8");
	if(mb_substr($str, $length, 1, "UTF-8") == " ") return $substr . $char;

	//Xác định dấu " " cuối cùng trong chuỗi $substr vừa cắt
	$strPoint= mb_strrpos($substr, " ", "UTF-8");

	//Return string
	if($strPoint < $length - 20) return $substr . $char;
	else return mb_substr($substr, 0, $strPoint, "UTF-8") . $char;
}

/**
 * Ham tra ve list heading parse duoc va chuoi da set id theo heading
 * @param  string $str_html chuoi can parse
 * @return array           tra ve dang array
 */
function parse_heading($str_html){
	require_once(dirname(__FILE__) . "/simple_html_dom.php");
	if (empty($str_html)) return array();

	$html = str_get_html($str_html);

	if (!$html) return array();

	$arr_elm = $html->find('h2, h3');
	if (empty($arr_elm)) return array();

	$arr_item = array();
	$key_item = 0;
	$key_item_child = 0;
	foreach ($arr_elm as $key=>$el) {
		$text = str_replace('&nbsp;', ' ', $el->plaintext);
		$text = str_replace('  ', ' ', $text);
		$text = trim($text);
		$text = cut_string2($text, 255);
		$textId = RemoveSign(removeHTML($text));
		$id =  str_replace(" ", "_", $textId);
		if (strlen($text) == 0) continue;
		if ($el->tag == 'h2') {
			$arr_item[$key_item]['name'] = $text;
			//$id =  'item-vg-' . $key_item
			$el->id = $id;
			$arr_item[$key_item]['id'] = $id;
			if (isset($arr_elm[$key+1]) && $arr_elm[$key+1]->tag == 'h2'){
				$key_item++;
			}
		} else {
			//$id = 'item-vg-' . $key_item . '-' . $key_item_child;
			$el->id = $id;
			$arr_item[$key_item]['child'][] = array(
				'name' => $text,
				'id' => $id
			);
			$key_item_child++;
			if (isset($arr_elm[$key+1]) && $arr_elm[$key+1]->tag == 'h2'){
				$key_item++;
				$key_item_child = 0;
			}
		}

	}
	return array('list' => $arr_item, 'data' => $html->outertext);
}
function dd($arrData){
	$strReturn = "";
	if(!empty($arrData)){
		$strReturn = '<pre>' . print_r($arrData) . '</pre>';
	}
	else return "Array Empty!!!";

	return $strReturn;
}
function clearSpaceBuffer($buffer){
	$arrStr	= array(chr(9), chr(10), chr(13).chr(13).chr(13), chr(13).chr(13));
	$arrRep	= array("", "", chr(13), chr(13));
	$buffer	= str_replace($arrStr, $arrRep, $buffer);
	return trim($buffer);
}
function checkso( $sim1x, $sim2x, $giaban, $i){
	if(strlen($sim2x)>11){
		echo $i.' - '.$sim1x.' - Sim lỗi'.'<br>';
		return false;	
	}
	return true;
}

function soreplace($data){
	$data=preg_replace('/[^0-9. ]/','',$data);
	return $data;	
}

function replace($data){
	$data=preg_replace('/[^0-9]/','',$data);
	return $data;
}

function getSimHot($iCat = 0, $limit = 0){

	global $arrMangSearch;
	global $arrMangData;
	global $con_sim_km;

	$arrReturn = array();

	if($con_sim_km == "") return $arrReturn;

	$arrSimPromotion = explode( "\r\n", $con_sim_km);

	if(!empty($arrSimPromotion)){
		
		$i = 0;
		for ( ; $i < count( $arrSimPromotion ) - 1; $i++ ){

			// if($limit > 0){
			// 	if($i > $limit) continue;
			// }

			list( $sosim, $giaban) = split( "\t", $arrSimPromotion[$i] );
			
			$sim1x = soreplace( $sosim );
			$sim2x = replace( $sosim );

			// Kiểm tra hợp lệ của số
			if ( checkso( $sim1x, $sim2x, $giaban, $i+1) ) $datapost['s'][$sim2x] = $sim1x . "-" . $giaban;
			
		} // End for ( ; $i < count( $arrSimPromotion ) - 1; $i++ )

		if(!empty($datapost['s'])){
			$step = 0;
			foreach ( $datapost['s'] as $kg => $sg ){
				$step++;
				
				list( $sim1x, $giaban) = split( "-", $sg );
				
				// Sim
				$sim_sim1      = $sim1x;
				$sim_sim2      = intval($kg);
				$sim_sim2_full = "0" . intval($kg);
				
				$sim_price = $giaban;

				// Xac dinh mang sim va dau so
				$sim_category	= 1;
				$sim_dausoid	= 1;
				$dau1 = substr($sim_sim2_full,0,1);
				$dau2 = substr($sim_sim2_full,0,2);
				$dau3 = substr($sim_sim2_full,0,3);

				if(array_key_exists($dau3, $arrMangSearch)){
					$sim_dausoid	= $arrMangData[$dau3];
					$sim_category	= $arrMangSearch[$dau3];
				}elseif(array_key_exists($dau2, $arrMangSearch)){
					$sim_dausoid	= $arrMangData[$dau2];
					$sim_category	= $arrMangSearch[$dau2];
				}elseif(array_key_exists($dau1, $arrMangSearch)){
					$sim_dausoid	= $arrMangData[$dau1];
					$sim_category	= $arrMangSearch[$dau1];
				}  // End if(array_key_exists($dau, $dauso_sim_idx))

				if($iCat > 0){
					if($sim_dausoid != $iCat) continue;
				}
				
				$arrReturn[$sim_sim2]["sim_sim1"]       = $sim_sim1;
				$arrReturn[$sim_sim2]["sim_sim2"]       = $sim_sim2;
				$arrReturn[$sim_sim2]["sim_price"]      = $sim_price;
				$arrReturn[$sim_sim2]["cat_name_index"] = $sim_category;

			}
		}
		
	}

	return $arrReturn;
}

function validate_mobile_phone($number){
	return preg_match('/^((849)|(09)|(848)|(08))([0-9]{8})$|((841)|(01))([0-9]{9})$/',$number);
}
function generate_product_json_ld($arrProductJsonData){
	global $con_server_name;

	$strReturn	= '';

	if(count($arrProductJsonData) == 0) return $strReturn;

	$strReturn .= '<script type="application/ld+json">
	{
	  "@context": "https://schema.org/",
	  "@type": "Product",
	  "name": "' . $arrProductJsonData["sim_name"] . '",
	  "image": [
	    "' . $arrProductJsonData["sim_picture"] . '"
	   ],
	  "description": "' . $arrProductJsonData["sim_description"] . '",
	  "sku": "' . $arrProductJsonData["sim_sku"] . '",
	  "mpn": "' . $arrProductJsonData["sim_mpn"] . '",
	  "brand": {
	    "@type": "Thing",
	    "name": "' . $arrProductJsonData["sim_brand"] . '"
	  },
	  "offers": {
	    "@type": "Offer",
	    "url": "' . $arrProductJsonData["sim_link"] . '",
	    "priceCurrency": "VND",
	    "price": "' . $arrProductJsonData["sim_price"] . '",
	    "priceValidUntil": "' . date("Y-m-d", (time() + 86400*365)) . '",
	    "itemCondition": "https://schema.org/NewCondition",
	    "availability": "https://schema.org/InStock"
	  }
	}
	</script>';

	return $strReturn;

}
function generate_breadcrumb_json_ld($arrBreadcrumb){

	global $con_server_name;

	$strReturn	= '';
	if(count($arrBreadcrumb) == 0) return $strReturn;

	$arrBreadcrumb	= array_merge(array(array("id" => $con_server_name, "name" => "Siêu Thị Sim Thẻ")), $arrBreadcrumb);

	$strReturn	.= '<script type="application/ld+json">{"@context": "http://schema.org", "@type": "BreadcrumbList", "itemListElement": [';
	$i	= 0;
	foreach($arrBreadcrumb as $value){
		$i++;
		if($i > 1) $strReturn	.= ',';
		if($i < count($arrBreadcrumb)){
			$strReturn	.= '{
									"@type": "ListItem",
									"position": ' . $i . ',
									"item": {
										"@id": "' . $value["id"] . '",
										"name": "' . htmlspecialbo(replaceMQ($value["name"])) . '"
									}
								}';
		}
		else{
			$strReturn	.= '{
									"@type": "ListItem",
									"position": ' . $i . ',
									"item": {
										"@id": "' . $con_server_name . $value["id"] . '",
										"name": "' . htmlspecialbo(replaceMQ($value["name"])) . '"
									}
								}';
		}
	}
	$strReturn	.= ']}</script>';

	return $strReturn;

}
function getBetween($content,$start,$end){
	$r = explode($start, $content);
	if (isset($r[1])){
		$r = explode($end, $r[1]);
		return $r[0];
	}
	return '';
}
function genTrigramsKeyword($keyword) {

    $trigrams = "";
    for ($i = 0; $i < mb_strlen($keyword,"UTF-8") - 1; $i++){
    	if($i == 0) $trigrams .= mb_substr($keyword, $i, 2, "UTF-8");
      else $trigrams .= " " . mb_substr($keyword, $i, 2, "UTF-8");
    }
    return $trigrams;
}
function BuildTrigramsKeyword($keyword) {
    $t = "__" . $keyword . "__";
    $trigrams = "";
    for ($i = 0; $i < mb_strlen($t,"UTF-8") - 2; $i++)
        $trigrams .= mb_substr($t, $i, 3, "UTF-8") . " ";
    return $trigrams;
}
function array_random($arrData, $limit, $preserve_keys=false){
	$arrReturn	= array();
	$count		= count($arrData);
	$maxRand		= ($count > $limit ? $limit : $count);
	$arrTemp		= $arrData;
	for($i=0; $i<$maxRand; $i++){
		$rand_keys	= array_rand($arrTemp);
		$arrReturn[!$preserve_keys ? $i : $rand_keys] = $arrData[$rand_keys];
		unset($arrTemp[$rand_keys]);
	}
	return $arrReturn;
}
function saveErrorLog($log_message){
   $filename = "../../store/logs/sphinx_search.cfn";
   $handle = @fopen($filename, 'a');
   
   //Nếu handle chưa có mở thêm ../
   if (!$handle) $handle = @fopen("../" . $filename, 'a');
   
   //Nếu ko mở đc lần 2 thì return luôn
   if (!$handle) return;
   
   fwrite($handle, date("d/m/Y h:i:s A") . " " . $_SERVER['REMOTE_ADDR'] . " " . $_SERVER['SCRIPT_NAME'] . "?" . @$_SERVER['QUERY_STRING'] . "\n" . $log_message . "\n");
   fclose($handle);  
} // End function saveErrorLog
function convertSimNumber($keyword, $sim_sim1){

	$strReturn	= "";
	
	$arrChar    = array(".", " ");
	$arrReplace = array("", "");

	if($keyword == "") return $strReturn;

	$keyword   = str_replace("*", "", $keyword);

	if(strpos($sim_sim1, $keyword)){

		$strReturn = str_replace($keyword, '<font color="#D8052B">' . $keyword . '</font>', $sim_sim1);
		
	}
	else{
		$firstChar = substr($keyword, 0, 1);
		$lastChar  = substr($keyword, -1);

		$strRegex = '/' . $firstChar . '(.*?)' . $lastChar . '/';
		
		preg_match_all($strRegex, $sim_sim1, $matches, PREG_SET_ORDER, 0);

		if(!empty($matches)){
			foreach ($matches as $key => $value) {

				// echo $value[0];
				
				if(isset($value[0]) && $value[0] != ""){
					$strResult =  str_replace($arrChar, $arrReplace, $value[0]);
					// var_dump($strResult);
					if($strResult == $keyword){
						$strReturn = str_replace($value[0], '<font color="#D8052B">' . $value[0] . '</font>', $sim_sim1);
					}
				}

			}
		}
	}

	return $strReturn;
}

function replaceSphinxMQ($str){
	$from	= array ( '\\', '(',')','|','-','!','@','~','"','&', '/', '^', '$', '=', "'", "\x00", "\n", "\r", "\x1a" );
	$to	= array ( '\\\\', '\\\(','\\\)','\\\|','\\\-','\\\!','\\\@','\\\~','\\\"', '\\\&', '\\\/', '\\\^', '\\\$', '\\\=', "\\'", "\\x00", "\\n", "\\r", "\\x1a" );
	return str_replace($from, $to, $str);
}
# if( strpos($_SERVER['SERVER_NAME'], 'sieuthisimthe.com') === false && strpos($_SERVER['SERVER_NAME'], 'localhost') === false ) exit();

#***
# * More Function VNAZ
#***
#

function generatePictureDetail($picRoot, $strNumber, $strPrice, $strType){

	$strNumber   = str_replace(array(" ", "."), array("", ""), $strNumber);
	$strPathSave = "../../store/media/products/" . $strNumber . ".jpg";

	if($picRoot == "Itelecom.jpg"){
		if (file_exists($strPathSave)){
			unlink($strPathSave);
		}
	}
	else{
		if (file_exists($strPathSave)){
			$pathNews = "/store/media/products/" . $strNumber . ".jpg";
			return $pathNews;
			// unlink($strPathSave);
		}
	}
	
	$srcPic = "";

	// Create image from existing file
	$createPic = imageCreateFromJpeg("../../lib/img/" . $picRoot);

	// pick color for the text
	$colorNumber = imagecolorallocate($createPic, 0, 0, 0);
	$colorPrice  = imagecolorallocate($createPic, 255, 0, 0);

	// Set font to the text
	$font_path   = '../../lib/fonts/SFUHelveticaRegular.ttf';
	$fontBodl 	 = '../../lib/fonts/SFUHelveticaCondensedBold.ttf';

	// Set text to be write on image
	if($strPrice > 0) $strPrice    = $strPrice . "đ";
	else $strPrice = "Sim đã bán";
	
	$strKho      = "Kho: Sieuthisimthe.com";

	// Write text on image
	if($picRoot == "Gmobile.jpg"){
		imagettftext($createPic, 28, 0, 50, 180, $colorNumber, $fontBodl, $strNumber);
		imagettftext($createPic, 22, 0, 45, 220, $colorPrice, $fontBodl, $strPrice);
		imagettftext($createPic, 18, 0, 45, 250, $colorNumber, $fontBodl, $strKho);
		imagettftext($createPic, 18, 0, 45, 280, $colorNumber, $fontBodl, $strType);
	}
	else if($picRoot == "Itelecom.jpg"){
		imagettftext($createPic, 28, 0, 218, 185, $colorNumber, $fontBodl, $strNumber);
		imagettftext($createPic, 22, 0, 218, 220, $colorPrice, $fontBodl, $strPrice);
		imagettftext($createPic, 18, 0, 218, 250, $colorNumber, $fontBodl, $strKho);
		imagettftext($createPic, 18, 0, 218, 280, $colorNumber, $fontBodl, $strType);
	}
	else{
		imagettftext($createPic, 28, 0, 218, 155, $colorNumber, $fontBodl, $strNumber);
		imagettftext($createPic, 22, 0, 218, 190, $colorPrice, $fontBodl, $strPrice);
		imagettftext($createPic, 18, 0, 218, 220, $colorNumber, $fontBodl, $strKho);
		imagettftext($createPic, 18, 0, 218, 250, $colorNumber, $fontBodl, $strType);
	}

	// ob_start();
	// output image to the browser
	imagejpeg($createPic, "../../store/media/products/" . $strNumber . ".jpg");
	
	// delete the image resource 
	imagedestroy ($createPic);

	$pathNews = "/store/media/products/" . $strNumber . ".jpg";

	return $pathNews;
}

function generatePictureDetail2($picRoot, $strNumber, $strPrice, $strType){

	$strNumber   = str_replace(array(" ", "."), array("", ""), $strNumber);
	$strPathSave = "../../store/media/products/" . $strNumber . ".jpg";

	// if($picRoot == "Itelecom.jpg"){
	// 	if (file_exists($strPathSave)){
	// 		unlink($strPathSave);
	// 	}
	// }
	// else{
	// 	if (file_exists($strPathSave)){
	// 		return $strPathSave;
	// 		// unlink($strPathSave);
	// 	}
	// }
	
	$srcPic = "";

	// Create image from existing file
	$createPic = imageCreateFromJpeg("../../lib/img/" . $picRoot);

	var_dump(file_exists("../lib/img/Itelecom.jpg"));
	exit();

	// pick color for the text
	$colorNumber = imagecolorallocate($createPic, 0, 0, 0);
	$colorPrice  = imagecolorallocate($createPic, 255, 0, 0);

	// Set font to the text
	$font_path   = '../../lib/fonts/SFUHelveticaRegular.ttf';
	$fontBodl 	 = '../../lib/fonts/SFUHelveticaCondensedBold.ttf';

	// Set text to be write on image
	if($strPrice > 0) $strPrice    = $strPrice . "đ";
	else $strPrice = "Sim đã bán";
	
	$strKho      = "Kho: Sieuthisimthe.com";

	// Write text on image
	if($picRoot == "Gmobile.jpg"){
		imagettftext($createPic, 28, 0, 50, 180, $colorNumber, $fontBodl, $strNumber);
		imagettftext($createPic, 22, 0, 45, 220, $colorPrice, $fontBodl, $strPrice);
		imagettftext($createPic, 18, 0, 45, 250, $colorNumber, $fontBodl, $strKho);
		imagettftext($createPic, 18, 0, 45, 280, $colorNumber, $fontBodl, $strType);
	}
	else if($picRoot == "Itelecom.jpg"){
		imagettftext($createPic, 28, 0, 218, 185, $colorNumber, $fontBodl, $strNumber);
		imagettftext($createPic, 22, 0, 218, 220, $colorPrice, $fontBodl, $strPrice);
		imagettftext($createPic, 18, 0, 218, 250, $colorNumber, $fontBodl, $strKho);
		imagettftext($createPic, 18, 0, 218, 280, $colorNumber, $fontBodl, $strType);
	}
	else{
		imagettftext($createPic, 28, 0, 218, 155, $colorNumber, $fontBodl, $strNumber);
		imagettftext($createPic, 22, 0, 218, 190, $colorPrice, $fontBodl, $strPrice);
		imagettftext($createPic, 18, 0, 218, 220, $colorNumber, $fontBodl, $strKho);
		imagettftext($createPic, 18, 0, 218, 250, $colorNumber, $fontBodl, $strType);
	}

	// ob_start();
	// output image to the browser
	imagejpeg($createPic, "../../store/media/products/" . $strNumber . ".jpg");
	
	// delete the image resource 
	imagedestroy ($createPic);

	$pathNews = "/store/media/products/" . $strNumber . ".jpg";

	return $pathNews;
}

function redirect($url, $http=0, $redirect_parent=0){
	if($http == 0) $url = str_replace("://", "", $url);
	//Remove " tranh pha ngoac
	$url = str_replace('"',"",$url);
	//echo '<meta http-equiv="refresh" content="0; URL=' . $url . '" />';
	if($redirect_parent == 1){
		echo '<script type="text/javascript">window.parent.location.href="' . $url . '"</script>';
	}else{
		echo '<script type="text/javascript">window.location.href="' . $url . '"</script>';
	}

	exit();
}

function redirectHTML($url, $http=0){
	if($http == 0) $url = str_replace("://", "", $url);
	echo '<meta http-equiv="refresh" content="0; URL=' . $url . '" />';
	exit();
}

function redirectHeader($url, $http=0, $nocache=0){
	if($http == 0) $url = str_replace("://", "", $url);

	if($nocache == 1){
		// Bỏ qua cache trình duyệt
		header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
		header('Expires: Sat, 26 Jul 1997 05:00:00 GMT');
		Header('Location:' . $url, true, 301);
	}else{
		Header( "HTTP/1.1 301 Moved Permanently" );
		Header( "Location: " . $url );
	}

	exit();
}

function format_number($number, $num_decimal=2, $edit=0){

	$sep		= ($edit == 0 ? array(",", ".") : array(".", ""));
	$stt		= -1;
	$return	= number_format($number, $num_decimal, $sep[0], $sep[1]);
	for($i=$num_decimal; $i>0; $i--){
		$stt++;
		if(intval(substr($return, -$i, $i)) == 0){
			$return = number_format($number, $stt, $sep[0], $sep[1]);
			break;
		}
	}
	return $return;

}

function saveLog($filename , $content, $show_time = 1){
	$log_path     =  "../../store/logs/";
	$handle       =   @fopen($log_path . $filename, "a");
	//Neu handle chua co mo thêm ../
	if (!$handle) $handle = @fopen($log_path . $filename, "a");
	//Neu ko mo dc lan 2 thi exit luon
	if (!$handle) exit("Error save log");
	fwrite($handle, (($show_time != 0) ? date("d/m/Y h:i:s A") . " " . @$_SERVER["REQUEST_URI"] . "\n" : "") . $content . "\n");
	fclose($handle);
}

function microtime_float(){
	list($usec, $sec) = explode(" ", microtime());
	return ((float)$usec + (float)$sec);
}

function _rawurlencode($url){
	$url = rawurlencode($url);	
	return $url;
}

function removeQueryString($url){
	# $url = substr( $url, 0, strrpos( $url, "?"));

    $url = strtok($url, '?');

    return $url;
}


function reconstruct_url($url){    
    $url_parts = parse_url($url);
    $constructed_url = $url_parts['scheme'] . '://' . $url_parts['host'] . (isset($url_parts['path'])?$url_parts['path']:'');

    return $constructed_url;
}

#+
#+ Curl Web
class curl
{
   var $channel ;
    
    function curl()
    {
        $this->channel = curl_init( );
		#+
		curl_setopt( $this->channel, CURLINFO_HEADER_OUT, false );
        curl_setopt( $this->channel, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
        curl_setopt( $this->channel, CURLOPT_FOLLOWLOCATION, true );
        #+
        curl_setopt( $this->channel, CURLOPT_RETURNTRANSFER, true );
		curl_setopt( $this->channel, CURLOPT_BINARYTRANSFER, true );
		#+
        curl_setopt( $this->channel, CURLOPT_COOKIEJAR, dirname(__FILE__).'/cookie.txt');
        curl_setopt( $this->channel, CURLOPT_COOKIEFILE, dirname(__FILE__).'/cookie.txt');    
		#+
		curl_setopt( $this->channel, CURLOPT_SSL_VERIFYPEER, false);
    	curl_setopt( $this->channel, CURLOPT_SSL_VERIFYHOST, false);
    }
    function makeRequest( $method, $url, $vars, $refere )
    {
		if(is_array($vars)){
			$fields_string = '';
			foreach($vars as $key=>$value) { $fields_string .= $key.'='.$value.'&'; }
			$fields_string = substr($fields_string, 0, -1);
			trim($fields_string);
		}else{
			$fields_string = $vars;
		}
		#+
		#+ setup the url to post / get from / to
        curl_setopt( $this->channel, CURLOPT_URL, $url );
        #+
		##+ the actual post bit
        if ( strtolower( $method ) == 'post' ) :
            curl_setopt( $this->channel, CURLOPT_POST, true );
            curl_setopt( $this->channel, CURLOPT_POSTFIELDS, $fields_string );
        endif;
		curl_setopt( $this->channel, CURLOPT_REFERER, $refere);
        #+
		#+ return data
		$result = curl_exec ($this->channel); 
		#+
		return $result;
    }
}  // End class curl

#+
#+ Function Kiem tra loai sim
function checkSimType($sim = "100990", $arrSimIndex = array() ){
	$sim        = preg_replace("/[^0-9]/si","",$sim);
	$sim        = substr($sim, -8, 8);
	$sim        = str_split($sim);

	$a1         = $sim[0];
	$a0         = $sim[1];
	$a          = $sim[2];
	$b          = $sim[3];
	$c          = $sim[4];
	$x          = $sim[5];
	$y          = $sim[6];
	$z          = $sim[7];
	$abc        = $a.$b.$c;
	$xyz        = $x.$y.$z;
	$yz         = $y.$z;
	$ab         = $a.$b;
	$cx         = $c.$x;
	$yz         = $y.$z;
	$zy         = $z.$y;
	$xc         = $x.$c;
	$zyx        = $z.$y.$x;
	$cxyz       = $c.$x.$y.$z;
	$bcxyz      = $b.$c.$x.$y.$z;
	$abcxyz     = $a.$b.$c.$x.$y.$z;
	
	//
	$arrLocPhat = array('68','86','688','866');
	$arrThanTai = array('39','79');
	$arrOngDia  = array('38','78', '3838', '7878', '3878');
	$arrSoTien   = array('012','123','234','345','456','567','789','135','357','579','024','246','468'
								,'0123','1234','2345','3456','4567','5678','6789','1357','3579','0246','2468'
								,'01234','12345','45678','56789','13579','02468'
								,'123456','234567','345678','456789'
								,'001122','112233','223344','334455','445566','667788','778899'
								,'123345','246357'
	);

	$simType    =  array();
	
	// Sim lục quý
	if($a == $b && $b == $c && $c == $x && $x == $y && $y == $z){

		$simType =  $arrSimIndex['sim-luc-quy'];

	}
	// Sim ngũ quý
	else if($b == $c && $c == $x && $x == $y && $y == $z){

		$simType =  $arrSimIndex['sim-ngu-quy'];

	}
	// SIm tứ quý
	else if($c == $x && $x == $y && $y == $z){

		$simType =  $arrSimIndex['sim-tu-quy'];

	}
	// Sim tam hoa
	else if($x == $y && $y == $z){

		$simType =  $arrSimIndex['sim-tam-hoa'];

		if($a == $b && $b == $c){

			$simType =  $arrSimIndex['sim-tam-hoa-kep'];

		}

	}
	// Sim tứ quý giữa
	else if($a1 == $a0 && $a0 == $a && $a == $b || $a0 == $a && $a == $b && $b == $c || $a == $b && $b == $c && $c == $x || $b == $c && $c == $x && $x == $y){

		$simType =  $arrSimIndex['sim-tu-quy-giua'];

		if($a1 == $a0 && $a0 == $a && $a == $b && $b == $c || $a0 == $a && $a == $b && $b == $c && $c == $x || $a == $b && $b == $c && $c == $x && $x == $y){

			$simType =  $arrSimIndex['sim-ngu-quy-giua'];

			if($a1 == $a0 && $a0 == $a && $a == $b && $b == $c && $c == $x || $a0 == $a && $a == $b && $b == $c && $c == $x && $x == $y){

				$simType =  $arrSimIndex['sim-luc-quy-giua'];

			}

		}

	}
	else{

		// Sim ông địa
		if(in_array($yz, $arrOngDia)  ||  in_array($cxyz, $arrOngDia)){
			$simType  =  $arrSimIndex['sim-ong-dia'];

		}
		// Sim lộc phát
		else if(in_array($yz, $arrLocPhat)  ||  in_array($xyz, $arrLocPhat)){
			$simType  =  $arrSimIndex['sim-loc-phat'];

		}
		// Sim thần tài
		else if(in_array($yz, $arrThanTai)){
			$simType  =  $arrSimIndex['sim-than-tai'];

		}
		// Sim taxi
		else if($abc == $xyz || $ab == $cx && $cx == $yz){

			$simType =  $arrSimIndex['sim-taxi'];

		}
		// sim gánh đảo
		else if($a == $z && $b == $y && $c == $x || $c == $z && $x == $y){

			$simType =  $arrSimIndex['sim-ganh-dao'];

		}
		// Sim số kép
		else if($a == $b && $c == $x && $y == $z || $c == $x && $y == $z){

			$simType =  $arrSimIndex['sim-so-kep'];

		}
		// Sim số lặp
		else if($cx == $yz){

			$simType =  $arrSimIndex['sim-so-lap'];

		}
		// Sim năm sinh
		else if($c == 1 && $x == 9 && $y >= 5 || 1 <= $ab && $ab <= 31 && 1<= $cx && $cx <= 12 && 50 <= $yz && $yz <= 99){

			$simType =  $arrSimIndex['sim-nam-sinh'];

		}
		// Sim số  tiến
		else if(in_array($xyz, $arrSoTien) || in_array($cxyz, $arrSoTien) || in_array($bcxyz, $arrSoTien) || in_array($abcxyz, $arrSoTien)){

			$simType =  $arrSimIndex['sim-so-tien'];

		}
		else $simType =  $arrSimIndex['sim-de-nho'];
	}

	return $simType;
} // End function checkSimType

function checkSimTypeSreach($sim = "100990", $arrSimIndex = array() ){

	$sim        = substr($sim, -8, 8);
	$sim        = str_split($sim);

	$a1         = $sim[0];
	$a0         = $sim[1];
	$a          = $sim[2];
	$b          = $sim[3];
	$c          = $sim[4];
	$x          = $sim[5];
	$y          = $sim[6];
	$z          = $sim[7];
	$abc        = $a.$b.$c;
	$xyz        = $x.$y.$z;
	$yz         = $y.$z;
	$ab         = $a.$b;
	$cx         = $c.$x;
	$yz         = $y.$z;
	$zy         = $z.$y;
	$xc         = $x.$c;
	$zyx        = $z.$y.$x;
	$cxyz       = $c.$x.$y.$z;
	$bcxyz      = $b.$c.$x.$y.$z;
	$abcxyz     = $a.$b.$c.$x.$y.$z;
	
	//
	$arrLocPhat = array('68','86','688','866');
	$arrThanTai = array('39','79');
	$arrOngDia  = array('38','78', '3838', '7878', '3878');
	$arrSoTien   = array('012','123','234','345','456','567','789','135','357','579','024','246','468'
								,'0123','1234','2345','3456','4567','5678','6789','1357','3579','0246','2468'
								,'01234','12345','45678','56789','13579','02468'
								,'123456','234567','345678','456789'
								,'001122','112233','223344','334455','445566','667788','778899'
								,'123345','246357'
	);

	$simType    =  array();
	
	// Sim lục quý
	if($a == $b && $b == $c && $c == $x && $x == $y && $y == $z){

		$simType =  $arrSimIndex['sim-luc-quy'];

	}
	// Sim ngũ quý
	else if($b == $c && $c == $x && $x == $y && $y == $z){

		$simType =  $arrSimIndex['sim-ngu-quy'];

	}
	// SIm tứ quý
	else if($c == $x && $x == $y && $y == $z){

		$simType =  $arrSimIndex['sim-tu-quy'];

	}
	// Sim tam hoa
	else if($x == $y && $y == $z){

		$simType =  $arrSimIndex['sim-tam-hoa'];

		if($a == $b && $b == $c){

			$simType =  $arrSimIndex['sim-tam-hoa-kep'];

		}

	}
	// Sim tứ quý giữa
	else if($a1 == $a0 && $a0 == $a && $a == $b || $a0 == $a && $a == $b && $b == $c || $a == $b && $b == $c && $c == $x || $b == $c && $c == $x && $x == $y){

		$simType =  $arrSimIndex['sim-tu-quy-giua'];

		if($a1 == $a0 && $a0 == $a && $a == $b && $b == $c || $a0 == $a && $a == $b && $b == $c && $c == $x || $a == $b && $b == $c && $c == $x && $x == $y){

			$simType =  $arrSimIndex['sim-ngu-quy-giua'];

			if($a1 == $a0 && $a0 == $a && $a == $b && $b == $c && $c == $x || $a0 == $a && $a == $b && $b == $c && $c == $x && $x == $y){

				$simType =  $arrSimIndex['sim-luc-quy-giua'];

			}

		}

	}
	else{

		// Sim ông địa
		if(in_array($yz, $arrOngDia)  ||  in_array($cxyz, $arrOngDia)){
			$simType  =  $arrSimIndex['sim-ong-dia'];

		}
		// Sim lộc phát
		else if(in_array($yz, $arrLocPhat)  ||  in_array($xyz, $arrLocPhat)){
			$simType  =  $arrSimIndex['sim-loc-phat'];

		}
		// Sim thần tài
		else if(in_array($yz, $arrThanTai)){
			$simType  =  $arrSimIndex['sim-than-tai'];

		}
		// Sim taxi
		else if($abc == $xyz || $ab == $cx && $cx == $yz){

			$simType =  $arrSimIndex['sim-taxi'];

		}
		// sim gánh đảo
		else if($a == $z && $b == $y && $c == $x || $c == $z && $x == $y){

			$simType =  $arrSimIndex['sim-ganh-dao'];

		}
		// Sim số kép
		else if($a == $b && $c == $x && $y == $z || $c == $x && $y == $z){

			$simType =  $arrSimIndex['sim-so-kep'];

		}
		// Sim số lặp
		else if($cx == $yz){

			$simType =  $arrSimIndex['sim-so-lap'];

		}
		// Sim năm sinh
		else if($c == 1 && $x == 9 && $y >= 5 || 1 <= $ab && $ab <= 31 && 1<= $cx && $cx <= 12 && 50 <= $yz && $yz <= 99){

			$simType =  $arrSimIndex['sim-nam-sinh'];

		}
		// Sim số  tiến
		else if(in_array($xyz, $arrSoTien) || in_array($cxyz, $arrSoTien) || in_array($bcxyz, $arrSoTien) || in_array($abcxyz, $arrSoTien)){

			$simType =  $arrSimIndex['sim-so-tien'];

		}
		else $simType =  $arrSimIndex['sim-de-nho'];
	}

	return $simType;
} // End function checkSimType


function checkSimTypeArr($sim = "100990", $arrSimIndex = array() ){
	
	$simType =  array();
	
	$sim = preg_replace("/[^0-9]/si","",$sim);
	$slen = strlen($sim);
	
	
	
	
	if($slen > 1){
		$sim_temp = '';
		for($i=1; $i<= (6-$slen); $i++){
			$sim_temp .= 0;
		}
		$sim = $sim_temp.$sim;
		
		$sim = substr($sim, -6, 6);
		$sim = str_split($sim);
		
		
		$a			= $sim[0];
		$b			= $sim[1];
		$c			= $sim[2];
		$x			= $sim[3];
		$y			= $sim[4];
		$z			= $sim[5];
		$abc 		= $a.$b.$c;
		$xyz 		= $x.$y.$z;
		$yz			= $y.$z;
		$ab			= $a.$b;
		$cx			= $c.$x;
		$yz			= $y.$z;
		$zy			= $z.$y;
		$xc			= $x.$c;
		$zyx		= $z.$y.$x;
		$cxyz		= $c.$x.$y.$z;
		$bcxyz		= $b.$c.$x.$y.$z;
		$abcxyz		= $a.$b.$c.$x.$y.$z;
		
		//
		$arrLocPhat = array('68','86','688','866');
		$arrThanTai = array('39','79');
		$arrSoTien 	= array('012','123','234','345','456','567','789','135','357','579','024','246','468'
						,'0123','1234','2345','3456','4567','5678','6789','1357','3579','0246','2468'
						,'01234','12345','45678','56789','13579','02468'
						,'123456','234567','345678','456789'
						,'001122','112233','223344','334455','445566','667788','778899'
						,'123345','246357'
						);

		if($a == $b && $b == $c && $c == $x && $x == $y && $y == $z){
			$simType[] =  $arrSimIndex['sim-luc-quy'];
		}
		if($b == $c && $c == $x && $x == $y && $y == $z || $a == $b && $b == $c && $c == $x && $x == $y){
			$simType[] =  $arrSimIndex['sim-ngu-quy'];
		}
		if($c == $x && $x == $y && $y == $z){
			$simType[] =  $arrSimIndex['sim-tu-quy'];
		}
		if($x == $y && $y == $z){
			$simType[] =  $arrSimIndex['sim-tam-hoa'];
		}
		if(in_array($yz, $arrLocPhat)  ||  in_array($xyz, $arrLocPhat)){
			$simType[] =  $arrSimIndex['sim-loc-phat'];
		}
		if(in_array($yz, $arrThanTai)){
			$simType[] =  $arrSimIndex['sim-than-tai'];
		}
		if($cx == $zy || $ab == $xc && $xc == $yz || $abc == $zyx){
			$simType[] =  $arrSimIndex['sim-ganh-dao'];
		}
		if($abc == $xyz || $ab == $cx && $cx == $yz  || $a == $b && $b == $c && $x == $y && $y == $z){
			$simType[] =  $arrSimIndex['sim-taxi'];
		}
		if($a == $b && $c == $x && $y == $z || $c == $x && $y == $z){
			$simType[] =  $arrSimIndex['sim-so-kep'];
		}
		if($cx == $yz){
			$simType[] =  $arrSimIndex['sim-so-lap'];
		}
		if($c == 1 && $x == 9 && $y >= 5 || 1 <= $ab && $ab <= 31 && 1<= $cx && $cx <= 12 && 50 <= $yz && $yz <= 99){
			$simType[] =  $arrSimIndex['sim-nam-sinh'];
		}
		if(in_array($xyz, $arrSoTien) || in_array($cxyz, $arrSoTien) || in_array($bcxyz, $arrSoTien) || in_array($abcxyz, $arrSoTien)){
			$simType[] =  $arrSimIndex['sim-so-tien'];
		}
	}
	
	return $simType;
	
	/*/
	if($a == $b && $b == $c && $c == $x && $x == $y && $y == $z){
		$simType[] =  'sim-luc-quy';
	}
	if($b == $c && $c == $x && $x == $y && $y == $z || $a == $b && $b == $c && $c == $x && $x == $y){
		$simType[] =  'sim-ngu-quy';
	}
	if($c == $x && $x == $y && $y == $z){
		$simType[] =  'sim-tu-quy';
	}
	if($x == $y && $y == $z){
		$simType[] =  'sim-tam-hoa';
	}
	if(in_array($yz, $arrLocPhat)  ||  in_array($xyz, $arrLocPhat)){
		$simType[] =  'sim-loc-phat';
	}
	if(in_array($yz, $arrThanTai)){
		$simType[] =  'sim-than-tai';
	}
	if($cx == $zy || $ab == $xc && $xc == $yz || $abc == $zyx){
		$simType[] =  'sim-ganh-dao';
	}
	if($abc == $xyz || $ab == $cx && $cx == $yz  || $a == $b && $b == $c && $x == $y && $y == $z){
		$simType[] =  'sim-taxi';
	}
	if($a == $b && $c == $x && $y == $z || $c == $x && $y == $z){
		$simType[] =  'sim-so-kep';
	}
	if($cx == $yz){
		$simType[] =  'sim-so-lap';
	}
	if($c == 1 && $x == 9 && $y >= 5 || 1 <= $ab && $ab <= 31 && 1<= $cx && $cx <= 12 && 50 <= $yz && $yz <= 99){
		$simType[] =  'sim-nam-sinh';
	}
	if(in_array($xyz, $arrSoTien) || in_array($cxyz, $arrSoTien) || in_array($bcxyz, $arrSoTien) || in_array($abcxyz, $arrSoTien)){
		$simType[] =  'sim-so-tien';
	}
	//*/

}

#+
#+ Dich tu khoa duoc cau hinh theo cac bien da co san
#+ 
function dich3theTpl($type, $arrRep3the, $mac_dinh){
	global $meta_title;
	global $meta_description;
	global $meta_keyword;
	global $meta_title_cat;
	global $meta_description_cat;
	global $meta_keyword_cat;
	global $meta_title_detail;
	global $meta_description_detail;
	global $meta_keyword_detail;
	global $current_page1;

	if($type == 'cat'){
		$title_can_dich = $meta_title != '' ? $meta_title : ($meta_title_cat != '' ? $meta_title_cat : '');
		$description_can_dich = $meta_description != '' ? $meta_description : ($meta_description_cat != '' ? $meta_description_cat : '');
		$keywords_can_dich = $meta_keyword != '' ? $meta_keyword : ($meta_keyword_cat != '' ? $meta_keyword_cat : '');
	}elseif($type == 'detail'){
		$title_can_dich = $meta_title != '' ? $meta_title : ($meta_title_detail != '' ? $meta_title_detail : '');
		$description_can_dich = $meta_description != '' ? $meta_description : ($meta_description_detail != '' ? $meta_description_detail : '');
		$keywords_can_dich = $meta_keyword != '' ? $meta_keyword : ($meta_keyword_detail != '' ? $meta_keyword_detail : '');	
	}

	#+
	if($title_can_dich != '')
		$tieu_de_theo_module 	= dich3the($title_can_dich, $arrRep3the);
	if($description_can_dich != '')
		$mo_ta_theo_module 		= dich3the($description_can_dich, $arrRep3the);
	if($keywords_can_dich != '')
		$tu_khoa_theo_module 	= dich3the($keywords_can_dich, $arrRep3the);

	$con_site_title 		= $tieu_de_theo_module != '' ? $tieu_de_theo_module : ($mac_dinh != '' ? $mac_dinh : $con_site_title);
	$con_meta_description 	= $mo_ta_theo_module != '' ? $mo_ta_theo_module : ($mac_dinh != '' ? $mac_dinh : $con_meta_description);
	$con_meta_keywords 		= $tu_khoa_theo_module != '' ? $tu_khoa_theo_module : ($mac_dinh != '' ? $mac_dinh : $con_meta_keywords);

	#+
	#+ Neu co phan trang
	if($current_page1 != 0){
		$con_site_title			.= " - Trang ".$current_page1;
		$con_meta_description	.= " - Trang ".$current_page1;
	} // End if($current_page1 != 0)
	
	$arrReturn = array(
		'con_site_title'=>$con_site_title,
		'con_meta_description'=>$con_meta_description,
		'con_meta_keywords'=>$con_meta_keywords,
		);
	return $arrReturn;
} // End function dich3theCat()
#+
function dich3the($chu_can_dich,$arr = array()){
	foreach($arr as $key => $value){
		$chu_can_dich = str_replace('{'.$key.'}', $value, $chu_can_dich);	
	} // End foreach($arr as $key => $value)
	
	$chu_can_dich = strip_tags($chu_can_dich);
	$chu_can_dich = htmlspecialchars($chu_can_dich);
	
	return $chu_can_dich;
} // End function dich3the

#+
#+ Lay dung luon cua file
function format_size($size) {
	$sizes = array(" Bytes", " KB", " MB", " GB", " TB", " PB", " EB", " ZB", " YB");
	if ($size == 0) { return('n/a'); } else {
	return (round($size/pow(1024, ($i = floor(log($size, 1024)))), $i > 1 ? 2 : 0) . $sizes[$i]); }
}

#+
#+ Lay dung luong cua 1 thu muc
function get_dir_size($dir_name){
	$dir_size =0;
	if (is_dir($dir_name)) {
		if ($dh = opendir($dir_name)) {
			while (($file = readdir($dh)) !== false) {
				if($file !="." && $file != ".."){
					
					if(is_file($dir_name."/".$file)){
						$dir_size += filesize($dir_name."/".$file);
					} // End if(is_file($dir_name."/".$file)){
					/* check for any new directory inside this directory */
					if(is_dir($dir_name."/".$file)){
						$dir_size +=  get_dir_size($dir_name."/".$file);
					} // End if(is_dir($dir_name."/".$file)){
						
				} // End if($file !="." && $file != ".."){
				
			} // End while (($file = readdir($dh)) !== false) {
			
		} // End if ($dh = opendir($dir_name)) {
			
	} // End if (is_dir($dir_name)) {
	closedir($dh);
	return $dir_size;
} // End function get_dir_size($dir_name){

#+
#+ Xoa toan bo file trong 1 thu muc nao do
function truncate_dir($path)
{
	$p = $path;
	$d = dir($p);
//	echo "Handle: " . $d->handle . "\n";
//	echo "Path: " . $d->path . "\n";
	while (false !== ($entry = $d->read())) {
	   if(strlen($entry)>2)unlink($p.'/'.$entry);
	}
	$d->close();
}

function rmkdir($path, $mode = 0755) {
	 $path = rtrim(preg_replace(array("/\\\\/", "/\/{2,}/"), "/", $path), "/");
	 $e = explode("/", ltrim($path, "/"));
	 if(substr($path, 0, 1) == "/") {
		 $e[0] = "/".$e[0];
	 }
	 $c = count($e);
	 $cp = $e[0];
	 for($i = 1; $i < $c; $i++) {
		 if(!is_dir($cp) && !@mkdir($cp, $mode)) {
			 return false;
		 }
		 $cp .= "/".$e[$i];
	 }
	 return @mkdir($path, $mode);
}

function sdir( $outerDir, $filters = array() ){ 
	$dirs = array_diff( scandir( $outerDir, 0 ), array_merge( array( ".", "..", "_notes", "Thumbs.db" ), $filters ) );
	 $dir_array = array();
	$i=0; 
	foreach( $dirs as $d ){
		$i++;
		$dir_array[ $i ] = is_dir($outerDir."/".$d) ? sdir( $outerDir."/".$d, $filters ) : $dir_array[ $i ] = $d;
	}
	 return $dir_array; 
} 

function reset_cache($level="",$folder)
{
	$p = $level."store/cache/".$folder;
	$d = dir($p);
//	echo "Handle: " . $d->handle . "\n";
//	echo "Path: " . $d->path . "\n";
	while (false !== ($entry = $d->read())) {
		echo $entry .'<br>';
		if(strlen($entry)>2)unlink($p.'/'.$entry);
	}
	$d->close();	
}

// Validate Domain
function is_valid_domain($domain)
{
	if( preg_match('/^([A-Z0-9][A-Z0-9_-]*(?:\.[A-Z0-9][A-Z0-9_-]*)+):?(\d+)?\/?/i', $domain) )
	{
		return 1;
	}
	else
	{
		return 0;	
	}
}

// Chi xoa bo thuoc tinh nhat dinh
function strip_only($str, $tags, $stripContent = false) {
    $content = '';
    if(!is_array($tags)) {
        $tags = (strpos($str, '>') !== false ? explode('>', str_replace('<', '', $tags)) : array($tags));
        if(end($tags) == '') array_pop($tags);
    }
    foreach($tags as $tag) {
        if ($stripContent)
             $content = '(.+</'.$tag.'[^>]*>|)';
         $str = preg_replace('#</?'.$tag.'[^>]*>'.$content.'#is', '', $str);
    }
    return $str;
}
 
function utf8html($utf8str)
{
	return trim(iconv("ISO-8859-1", "UTF-8", $utf8str));
	//return trim(iconv(mb_detect_encoding($utf8str), "UTF-8//TRANSLIT//IGNORE", $utf8str));
	//return htmlentities(mb_convert_encoding($utf8str,"ISO-8859-1","UTF-8"));
}

function securityText($str){
	$str = trim($str);
	$str = html_entity_decode($str, ENT_QUOTES, 'UTF-8');
	$str = mysql_escape_string($str);
	$str = strip_only($str,'<script><style>',true);
	$str = strip_only($str,'<a>');	
	return $str;
}
#***
# * Core Finalstyle
#***


/*/
function removeKey($string){
	$string 	=  trim(preg_replace("/[^A-Za-z0-9àáạảãâầấậẩẫăằắặẳẵèéẹẻẽêềếệểễìíịỉĩòóọỏõôồốộổỗơờớợởỡùúụủũưừứựửữỳýỵỷỹđÀÁẠẢÃÂẦẤẬẨẪĂẰẮẶẲẴÈÉẸẺẼÊỀẾỆỂỄÌÍỊỈĨÒÓỌỎÕÔỒỐỘỔỖƠỜỚỢỞỠÙÚỤỦŨƯỪỨỰỬỮỲÝỴỶỸ]/i"," ",$string));
	$string 	=  str_replace(" ","-",$string);
	$string		=  str_replace("--","-",$string);
	$string		=  str_replace("--","-",$string);
	$string		=  mb_convert_encoding($string, "UTF-8", "UTF-8");
	return $string;
}
//*/


# Lay du lieu va chuyen thanh 1 array
function getArray($sql,$field_id = ''){
	$db_query = new db_query($sql);
	$arrayReturn = array();
	$i=0;
	while($row=mysql_fetch_assoc($db_query->result)){
		if($field_id!=''){
			$arrayReturn[$row[$field_id]] = $row;
		}else{
			$i++;
			$arrayReturn[$i] = $row;
			
		}
	}
	unset($db_query);
	return $arrayReturn;
}

# Lay du lieu con cua 1 category
function getSub($id){
	$query = new db_query('SELECT cat_id FROM tbl_category WHERE cat_parent_id = '.$id);
	$array = array();
	while($row = mysql_fetch_assoc($query->result)){
		$array[] = $row['cat_id'];
	}
	return $array;
}

#+
#+ Lay Template Web Static
function getStaticTpl($id, $content){
	global $lang_id;
	
	#+
	#+ Lay du lieu
	$query = ' SELECT sta_active, sta_description'
			.' FROM tbl_statics'
			.' WHERE lang_id = '.$lang_id.' AND sta_active = 1 AND sta_id = '.$id
			.' LIMIT 0,1'
			;
	$db_query = new db_query($query);
	$row = mysql_fetch_assoc($db_query->result);
	
	#+
	#+ Kien tra dieu kien
	$str = '';
	if($row['sta_active'] == 1 && $row['sta_description'] != ''){
		$str .= $row['sta_description'];
	}else{
		$str .= $content;
	} // End if($row['sta_active'] == 1)
	
	return $str;	
} // End function getStaticTpl($id, $content)

# Lay du lieu tu bang static - co dung them video
function getStatic($iSta){
	$string = '';
	$db_static = new db_query("SELECT sta_description FROM tbl_statics WHERE sta_id = " . intval($iSta));
	if($row = mysql_fetch_assoc($db_static->result)){ 
		$string = $row["sta_description"];
	}
	unset($db_static);
	$idtemp 					= rand(1,9999);
	$pattern 					= "#\[video width=(.*?) height=(.*?)\](.*?)\[/video\]#si";
	$replacement				= '
									<span id="container' . $idtemp .'"></span>
									<script type="text/javascript" src="/lib/video/swfobject.js"></script>
									<script type="text/javascript">
										var so = new SWFObject("/lib/video/mediaplayer.swf","player","\\1","\\2","8");
										so.addParam("allowfullscreen","true");
										so.addParam("bgcolor","#333333");
										so.addVariable("file","\\3");
										so.addVariable("backcolor","0x000000");
										so.addVariable("frontcolor","0xCCCCCC");
										so.addVariable("lightcolor","0x996600");
										so.addVariable("image","/lib/img/bgvideo.jpg");
										so.addVariable(\'midroll\',\'668\');
										so.addVariable("width","\\1");
										so.addVariable("height","\\2");
										so.write("container' . $idtemp .'");
									</script>';
	$string = preg_replace($pattern,$replacement,$string);
	return $string;

}

# Update hits cua 1 bang theo - Chi dung khi luu truong hits ra rieng 1 bang
function updateHits($table,$iData){	

	$strsession = getValue("update_hits","str","SESSION","");

	$sql = "SELECT * FROM hits_" . $table . " WHERE hit_record = " . $iData;
	$db_select = new db_query($sql);
	if($row = mysql_fetch_assoc($db_select->result)){
		if(strpos($strsession,'[pro_' . $iData . ']')===false){
			$db_update = new db_execute("UPDATE hits_" . $table . " SET hit_hits = hit_hits+1 WHERE hit_record = " . $iData);
			unset($db_update);
		}
		$strsession .= '[pro_' . $iData . ']';
		$_SESSION["update_hits"] = $strsession;
		return $row["hit_hits"];
	}else{
		$db_update = new db_execute("INSERT INTO hits_" . $table . " VALUES(" . $iData . ", 1)");
	}
	return 1;
	unset($db_select);
	
}


# Xu ly du lieu html dc load ra khi view source se thay
function callback($buffer) {
	$strReplace	= array(chr(9), chr(10));
	$buffer		= str_replace($strReplace, "", $buffer);
	return $buffer;
}

# Dua ra bien 1 hoac 2 hoac 3
function s_null($value1,$value2,$value3=""){
	if($value1!=""){
		echo $value2;
	}else{
		echo $value3;
	}
}

# hàm cắt chuỗi
function limitWord($string, $maxOut){
    $string2Array = explode(' ', $string, ($maxOut + 1));
    
    if( count($string2Array) > $maxOut ){
       array_pop($string2Array);
       $output = implode(' ', $string2Array)."...";
    }else{
		$output = $string;
    }
    return $output;
}

# hàm cắt chuỗi
function cut_string($str,$length){
	if (mb_strlen($str,"UTF-8") > $length) return mb_substr($str,0,$length,"UTF-8") . "...";
	else return $str;
}

/*
 * truncate
 * @param string $text value of string
 * @return string 0,3x character in the $text (related space position)
*/
function truncate($var, $len) {
   if (empty ($var)) { return ""; }
   if (strlen ($var) < $len) { return $var; }
   if (preg_match ("/(.{1,$len})\s./ms", $var, $match)) { return $match [1] . " ..."; }
   else { return substr ($var, 0, $len) . " ..."; }
}


# Tra ve array ngon ngu cho website
function array_language(){
	return array(1=>"vn"
				,2=>"en"
				);
}

# Ho tro dinh dang lai tien te dua ra ngoai website
function formatNumber($value,$type="vnd"){
	//if($type=="vnd") return number_format(round($value/1000)*1000,0,"",".");
	return number_format($value,0,"",".");
}

function random(){
	$rand_value = "";
	$rand_value.=rand(1000,9999);
	$rand_value.=chr(rand(65,90));
	$rand_value.=rand(1000,9999);
	$rand_value.=chr(rand(97,122));
	$rand_value.=rand(1000,9999);
	$rand_value.=chr(rand(97,122));
	$rand_value.=rand(1000,9999);
	return $rand_value;
}

# Ho tro cho phan search keyword cua website
function getKeyword($value){
	$value = str_replace("\'","'",$value);
	$value = str_replace("'","''",$value);
	$value = str_replace(" ","%",$value);
	return $value;
}

# Loai bo cac the script va style
function removeCssJs($string){
	$string = preg_replace ('/<script.*?\>.*?<\/script>/si', ' ', $string); 
	$string = preg_replace ('/<style.*?\>.*?<\/style>/si', ' ', $string); 
	$string = str_replace ('&nbsp;', ' ', $string);
	$string = html_entity_decode ($string);
	return $string;
}

# Loai bo the <a></a>
function removeA($string){
	$string = preg_replace ('/<a.*?\>.*?<\/a>/si', ' ', $string); 
	$string = str_replace ('&nbsp;', ' ', $string);
	$string = html_entity_decode ($string);
	return $string;
}


# loại bỏ các thẻ html, javascript
function removeHTML($string){
	$string = preg_replace ('/<script.*?\>.*?<\/script>/si', ' ', $string); 
	$string = preg_replace ('/<style.*?\>.*?<\/style>/si', ' ', $string); 
	$string = preg_replace ('/<.*?\>/si', ' ', $string); 
	$string = str_replace ('&nbsp;', ' ', $string);
	$string = html_entity_decode ($string, ENT_COMPAT, 'UTF-8');
	return $string;
}

# loại bỏ hoạt động của các thẻ html, vô hiệu hóa
function htmlspecialbo($str){
	$arrDenied	= array('<', '>', '"');
	$arrReplace	= array('&lt;', '&gt;', '&quot;');
	$str = str_replace($arrDenied, $arrReplace, $str);
	return $str;
}
 
# Replace " -> &quot; (chống phá ngoặc)
function replaceQ($string){
	$string = str_replace('\"', '"', $string);
	$string = str_replace("\'", "'", $string);
	$string = str_replace("\&quot;", "&quot;", $string);
	$string = str_replace("\\\\", "\\", $string);
	return str_replace('"', "&quot;", $string);
}

# Replace " -> &quot; (chống phá ngoặc)
function replaceMQ($text){
	$text	= str_replace("\'", "'", $text);
	$text	= str_replace("'", "''", $text);
	return $text;
}


# Loai bo dau trong mot chuoi text
function RemoveSign($str)
{
	$coDau=array("à","á","ạ","ả","ã","â","ầ","ấ","ậ","ẩ","ẫ","ă",
	"ằ","ắ","ặ","ẳ","ẵ",
	"è","é","ẹ","ẻ","ẽ","ê","ề" ,"ế","ệ","ể","ễ",
	"ì","í","ị","ỉ","ĩ",
	"ò","ó","ọ","ỏ","õ","ô","ồ","ố","ộ","ổ","ỗ","ơ"
	,"ờ","ớ","ợ","ở","ỡ",
	"ù","ú","ụ","ủ","ũ","ư","ừ","ứ","ự","ử","ữ",
	"ỳ","ý","ỵ","ỷ","ỹ",
	"đ",
	"À","Á","Ạ","Ả","Ã","Â","Ầ","Ấ","Ậ","Ẩ","Ẫ","Ă"
	,"Ằ","Ắ","Ặ","Ẳ","Ẵ",
	"È","É","Ẹ","Ẻ","Ẽ","Ê","Ề","Ế","Ệ","Ể","Ễ",
	"Ì","Í","Ị","Ỉ","Ĩ",
	"Ò","Ó","Ọ","Ỏ","Õ","Ô","Ồ","Ố","Ộ","Ổ","Ỗ","Ơ"
	,"Ờ","Ớ","Ợ","Ở","Ỡ",
	"Ù","Ú","Ụ","Ủ","Ũ","Ư","Ừ","Ứ","Ự","Ử","Ữ",
	"Ỳ","Ý","Ỵ","Ỷ","Ỹ",
	"Đ","ê","ù","à");

	$khongDau=array("a","a","a","a","a","a","a","a","a","a","a"
	,"a","a","a","a","a","a",
	"e","e","e","e","e","e","e","e","e","e","e",
	"i","i","i","i","i",
	"o","o","o","o","o","o","o","o","o","o","o","o"
	,"o","o","o","o","o",
	"u","u","u","u","u","u","u","u","u","u","u",
	"y","y","y","y","y",
	"d",
	"A","A","A","A","A","A","A","A","A","A","A","A"
	,"A","A","A","A","A",
	"E","E","E","E","E","E","E","E","E","E","E",
	"I","I","I","I","I",
	"O","O","O","O","O","O","O","O","O","O","O","O"
	,"O","O","O","O","O",
	"U","U","U","U","U","U","U","U","U","U","U",
	"Y","Y","Y","Y","Y",
	"D","e","u","a");
	return str_replace($coDau,$khongDau,$str);
}

# Chi loai bo dau khong loai bo (space " ") 
function removeSeo($string,$keyReplace){
	$string		= html_entity_decode($string, ENT_COMPAT, 'UTF-8');
	$string		= mb_strtolower($string, 'UTF-8');
	$string	=	RemoveSign($string);
	//neu muon de co dau
	//$string 	=  trim(preg_replace("/[^A-Za-z0-9àáạảãâầấậẩẫăằắặẳẵèéẹẻẽêềếệểễìíịỉĩòóọỏõôồốộổỗơờớợởỡùúụủũưừứựửữỳýỵỷỹđÀÁẠẢÃÂẦẤẬẨẪĂẰẮẶẲẴÈÉẸẺẼÊỀẾỆỂỄÌÍỊỈĨÒÓỌỎÕÔỒỐỘỔỖƠỜỚỢỞỠÙÚỤỦŨƯỪỨỰỬỮỲÝỴỶỸ]/i"," ",$string));
	
	$string	=	str_replace($keyReplace,"-",$string);
	return $string;
}

# Loai bo dau va bien space thanh '-'
function removeTitle($string,$keyReplace){
	$string		= html_entity_decode($string, ENT_COMPAT, 'UTF-8');
	$string		= mb_strtolower($string, 'UTF-8');
	$string		= RemoveSign($string);
	//neu muon de co dau
	//$string 	=  trim(preg_replace("/[^A-Za-z0-9àáạảãâầấậẩẫăằắặẳẵèéẹẻẽêềếệểễìíịỉĩòóọỏõôồốộổỗơờớợởỡùúụủũưừứựửữỳýỵỷỹđÀÁẠẢÃÂẦẤẬẨẪĂẰẮẶẲẴÈÉẸẺẼÊỀẾỆỂỄÌÍỊỈĨÒÓỌỎÕÔỒỐỘỔỖƠỜỚỢỞỠÙÚỤỦŨƯỪỨỰỬỮỲÝỴỶỸ]/i"," ",$string));
	
	$string 	= trim(preg_replace("/[^A-Za-z0-9]/i"," ",$string)); // khong dau
	$string 	= str_replace(" ","-",$string);
	$string		= str_replace("--","-",$string);
	$string		= str_replace("--","-",$string);
	$string		= str_replace("--","-",$string);
	$string		= str_replace($keyReplace,"-",$string);
	return $string;
}
 

//hàm getvalue : 1 tên biến; 2 kiểu; 3 phương thức; 4 giá trị mặc định; 5 remove quote
function getValue($value_name, $data_type = "int", $method = "GET", $default_value = 0, $advance = 0){
	$value = $default_value;
	switch($method){
		case "GET": if(isset($_GET[$value_name])) $value = $_GET[$value_name]; break;
		case "POST": if(isset($_POST[$value_name])) $value = $_POST[$value_name]; break;
		case "COOKIE": if(isset($_COOKIE[$value_name])) $value = $_COOKIE[$value_name]; break;
		case "SESSION": if(isset($_SESSION[$value_name])) $value = $_SESSION[$value_name]; break;
		case "POSTGET":
			if(isset($_POST[$value_name])){
				 $value = $_POST[$value_name]; 
			}elseif(isset($_GET[$value_name])){
				$value = $_GET[$value_name];
			}
		break;
		default: if(isset($_GET[$value_name])) $value = $_GET[$value_name]; break;
	}
	$valueArray	= array("int" => intval($value), "str" => trim(strval($value)), "flo" => floatval($value), "dbl" => doubleval($value), "arr" => $value);
	foreach($valueArray as $key => $returnValue){
		if($data_type == $key){
			if($advance != 0){
				switch($advance){
					case 1:
						$returnValue = str_replace('"', '&quot;', $returnValue);
						$returnValue = str_replace("\'", "'", $returnValue);
						$returnValue = str_replace("'", "''", $returnValue);
						break;
					case 2:
						$returnValue = htmlspecialbo($returnValue);
						break;
				}
			}
			//Do số quá lớn nên phải kiểm tra trước khi trả về giá trị
			if((strval($returnValue) == "INF") && ($data_type != "str")) return 0;
			return $returnValue;
			break;
		}
	}
	return (intval($value));
}

//hàm tạo link khi cần thiết chuyển sang mod rewrite
function createLink($type="detail",$url=array(),$path="",$con_extenstion='html',$rewrite = 1){
	global $lang_name;
	global $lang_path;
	
	//
	$menuReturn = '';
	$keyReplace = '/';
	$path		= '/';
	
	switch($type){
		case "detail_simtype":			
				$menuReturn = '/0'.$url["sim_sim2"];
		break;
		case "detail_sim":			
				$menuReturn = '/sim-'.$url["cat_name_index"].'-0'.$url["sim_sim2"].'/';
		break;
		case "detail_news":			
				$menuReturn = '/'.$url['new_title_index'];
		break;
		case "detail_news_ko_dau":			
				$menuReturn = '/xem-'.$url['new_title_index'];
		break;
		case "detail_static":			
				$menuReturn = '/'.$url["sta_title_index"].'.htm';
		break;
		case "simdauso":				
				$menuReturn = '/'.$url["cat_name_index"].'-dau-'.$url["sds_name"].'/';
		break;
		case "simtype":				
				$menuReturn = '/'.strtolower($url["simtp_index"]).'/';
		break;
		case "page":
				$menuReturn = 'c'.$url['cat_id'].'/'.$url['cat_name_index'].'.html';
		break;
		case "cat":			
				$menuReturn = '/'.$url['cat_name_index'].'/';
		break;
		case "type":			
				$menuReturn = '/'.$url['cat_name_index'].'/';
		break;
	}
	return $menuReturn;
 
}
 

# 
function currentURL($remove=''){
	$url = '';
	$qString = $_SERVER['REQUEST_URI'];
	if(strpos($qString, '?') ==FALSE) return $qString;
	$qString = explode('?',$qString);
	$url.=$qString[0];
	$get	= explode('&',$qString[1]);
	$pre	='';
	foreach ($get as $value){
  		$val = explode('=', $value);
  		if(!(in_array($val[0], $remove)) AND $val[1]!=''){
  			if($pre=='') { $pre = '?';
  			}else{
  				$pre ='&';
  			}
  			$url.=$pre.$val[0].'='.$val[1];
  		}
	}
	return $url;
}


#hàm get URL
function getURL($serverName=0, $scriptName=0, $fileName=1, $queryString=1, $varDenied=''){
	$url	 = '';
	$slash = '/';
	if($scriptName != 0)$slash	= "";
	if($serverName != 0){
		if(isset($_SERVER['SERVER_NAME'])){
			$url .= 'http://' . $_SERVER['SERVER_NAME'];
			if(isset($_SERVER['SERVER_PORT'])) $url .= ":" . $_SERVER['SERVER_PORT'];
			$url .= $slash;
		}
	}
	if($scriptName != 0){
		if(isset($_SERVER['SCRIPT_NAME']))	$url .= substr($_SERVER['SCRIPT_NAME'], 0, strrpos($_SERVER['SCRIPT_NAME'], '/') + 1);
	}
	if($fileName	!= 0){
		if(isset($_SERVER['SCRIPT_NAME']))	$url .= substr($_SERVER['SCRIPT_NAME'], strrpos($_SERVER['SCRIPT_NAME'], '/') + 1);
	}
	if($queryString!= 0){
		$dau = 0;
		$url .= '?';
		reset($_GET);
		if($varDenied != ''){
			$arrVarDenied = explode('|', $varDenied);
			while(list($k, $v) = each($_GET)){
				if(array_search($k, $arrVarDenied) === false){
					 $url .= (($dau==0) ? '' : '&') . $k . '=' . urlencode($v);
					 $dau  = 1;
				}
			}
		}
		else{
			while(list($k, $v) = each($_GET)) $url .= '&' . $k . '=' . urlencode($v);
		}
	}
	$url = str_replace('"', '&quot;', strval($url));
	return $url;
}

#***
# * Core Finalstyle Admin
#***

# Lay du lieu theo thu tu ASC - DESC
function generate_sort($type, $sort, $current_sort, $image_path){
	if($type == "asc"){
		$title = "Tăng dần";
		if($sort != $current_sort) $image_sort = "sortasc.gif";
		else $image_sort = "sortasc_selected.gif";
	}
	else{
		$title = "Giảm dần";
		if($sort != $current_sort) $image_sort = "sortdesc.gif";
		else $image_sort = "sortdesc_selected.gif";
	}
	return '<a title="' . $title . '" href="' . getURL(0,0,1,1,"sort") . '&sort=' . $sort . '"><img border="0" vspace="2" src="' . $image_path . $image_sort . '" /></a>';
}

# get_class_tag + addRelate giup tu dong be tags news
function get_class_tag($khoang, $count){
	if($count < $khoang) return "tp_1";
	if($count < ($khoang*2)) return "tp_2";
	if($count < ($khoang*3)) return "tp_3";
	 return "tp_4";
}

function addRelate($table,$feild_id,$field_relate,$record_id,$arrayRelate=array()){
	$db_delete = new db_execute("DELETE FROM " . $table . " WHERE " . $feild_id . "=" . $record_id);
	unset($db_delete);
	foreach($arrayRelate as $key=>$value){
		$db_relate_execute = new db_execute("INSERT INTO " . $table . "(" . $feild_id . "," . $field_relate . ") VALUES(" . $record_id . ", " . intval($value) . ")");
		unset($db_relate_execute);
	}
}



/*/
//ham ma hoa
function fSencode($encodeStr=""){
	$returnStr = "";
	if(!empty($encodeStr)) {
		$enc = base64_encode($encodeStr);
		$enc = str_replace('=','',$enc);
		$enc = str_rot13($enc);
		$returnStr = $enc;
	}
	
	return $returnStr;
}

//ham giai mai
function fSdecode($encodedStr="",$type=0){
  $returnStr = "";
  $encodedStr = str_replace(" ","+",$encodedStr);
	if(!empty($encodedStr)) {
		 $dec = str_rot13($encodedStr);
		 $dec = base64_decode($dec);
		$returnStr = $dec;
	}
	switch($type){
		case 0:
			$returnStr = str_replace("\'","'",$returnStr);
			$returnStr = str_replace("'","''",$returnStr);
			return $returnStr;
		break;
		case 1:
			return intval($returnStr);
		break;
		case 3:
			return doubleval($returnStr);
		break;
	}
}

function str_encode($encodeStr="")
{
	$returnStr = "";
	if($encodeStr == '') return $encodeStr;
	if(!empty($encodeStr)) {
		$enc = base64_encode($encodeStr);
		$enc = str_replace('=','',$enc);
		$enc = str_rot13($enc);
		$returnStr = $enc;
	}
	return $returnStr;
}

function str_decode($encodedStr="",$type=0)
{
  $returnStr = "";
  $encodedStr = str_replace(" ","+",$encodedStr);
	if(!empty($encodedStr)) {
		 $dec = str_rot13($encodedStr);
		 $dec = base64_decode($dec);
		$returnStr = $dec;
	}
  return $returnStr;
}


function getKeyRef($query,$keyname="q"){
	$strreturn = '';
	preg_match("#" . $keyname . "=(.*)#si",$query,$match);
	if(isset($match[1])) $strreturn = preg_replace('#\&(.*)#si',"",$match[1]);
	return urldecode($strreturn);
}

function int_to_words($x)
{
	$nwords = array(    "không", "một", "hai", "ba", "bốn", "năm", "sáu", "bảy",
							 "tám", "chín", "mười", "mười một", "mười hai", "mười ba",
							 "mười bốn", "mười lăm", "mười sáu", "mười bảy", "mười tám",
							 "mười chín", "hai mươi", 30 => "ba mươi", 40 => "bốn mươi",
							 50 => "năm mươi", 60 => "sáu mươi", 70 => "bảy mươi", 80 => "tám mươi",
							 90 => "chín mươi" );
	if(!is_numeric($x))
	{
	  $w = '#';
	}else if(fmod($x, 1) != 0)
	{
	  $w = '#';
	}else{
	  if($x < 0)
	  {
		  $w = 'minus ';
		  $x = -$x;
	  }else{
		  $w = '';
	  }
	  if($x < 21)
	  {
		  $w .= $nwords[$x];
	  }else if($x < 100)
	  {
		  $w .= $nwords[10 * floor($x/10)];
		  $r = fmod($x, 10);
		  if($r > 0)
		  {
			  $w .= ' '. $nwords[$r];
		  }
	  } else if($x < 1000)
	  {
		  $w .= $nwords[floor($x/100)] .' trăm';
		  $r = fmod($x, 100);
		  if($r > 0)
		  {
			  $w .= '  '. int_to_words($r);
		  }
	  } else if($x < 1000000)
	  {
		  $w .= int_to_words(floor($x/1000)) .' ngàn';
		  $r = fmod($x, 1000);
		  if($r > 0)
		  {
			  $w .= ' ';
			  if($r < 100)
			  {
				  $w .= ' ';
			  }
			  $w .= int_to_words($r);
		  }
	  } else {
		  $w .= int_to_words(floor($x/1000000)) .' triệu';
		  $r = fmod($x, 1000000);
		  if($r > 0)
		  {
			  $w .= ' ';
			  if($r < 100)
			  {
				  $word .= ' ';
			  }
			  $w .= int_to_words($r);
		  }
	  }
	}
	return $w . '';
}
 
# Load vao 1 duong link
function cURL($url, $ref, $header, $cookie, $p)
{
    $ch = curl_init();//start curl
    curl_setopt($ch, CURLOPT_HEADER, $header);         //trace header response
    curl_setopt($ch, CURLOPT_NOBODY, $header);         //return body
    curl_setopt($ch, CURLOPT_URL, $url);             //curl Targeted URL
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);     
    curl_setopt($ch, CURLOPT_COOKIE, $cookie);      
    curl_setopt($ch, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
    curl_setopt($ch, CURLOPT_REFERER, $ref);         //fake referer
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);    
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);    
    if ($p) {
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $p);
    }
    $result = curl_exec($ch);
    curl_close($ch);
    if ($result)
        return $result;
}

# Kiem tra yahoo online - Luu file lai
function checkOnline($nickyahoo){
	$arrayOnline 	= array();
	$fileonline		= "../language/online.php";
	$filename 		= "../channel_category/avatar" . $nickyahoo . ".gif";
	//if((time() - filectime($fileonline)) > 000) @unlink($fileonline);
	if(file_exists($fileonline)){		
		include($fileonline);
		if((time()-filemtime($fileonline)) > 3000){
			@unlink($fileonline);
		};
	}else{
		
	}
	$arrayTemp = $arrayOnline;
	$tooltip = '';//file_exists($filename) ? 'onmouseover="showtip(\'<img src=\\\'/channel_category/avatar' . $nickyahoo . '.gif\\\'\',96)" onmouseout="hidetip();"' : '';
	if(isset($arrayTemp[$nickyahoo])){
		return '<a href="ymsgr:sendim?' . $nickyahoo . '" ' . $tooltip . ' ><img src="/lib/img/yahoo_' . $arrayOnline[$nickyahoo] . '.gif" border="0" align="absmiddle" /></a>';
	}else{
		$check_ym = cURL('http://opi.yahoo.com/online?u=' . $nickyahoo . '&m=t&t=1',null,0,0,null);
		
		$arrayTemp[$nickyahoo] = intval($check_ym);
		//phan ghi ra file online
		$file_content = "<? \n" .
						"\$arrayOnline = array(\n";
		$i=0;
		foreach($arrayTemp as $key=>$value){
			$i++;
			$file_content .= "\"" . $key . "\"=>" . $value . "" . ($i<count($arrayTemp) ? ',' : '') . "\n";
		}
		$file_content .= "); ?>";
		//echo htmlspecialchars($file_content);
		//luu file online lai
		require_once("../system/functions/file_functions.php");
		if($nickyahoo!=''){
			
			require_once("../system/functions/function_image.php");
			require_once("../system/functions/file_functions.php");
			if(file_exists($filename)){
				if(time() - filemtime($filename) > 945600){
					$data = @file_get_contents("http://img.msg.yahoo.com/avatar.php?yids=" . $nickyahoo . "&format=gif");
					if($data !== false){
						get_file($filename,$data);
						if(file_exists($filename)){
							resize_image("../channel_category/","avatar" . $nickyahoo . ".gif",23,20,100);
						}
					}
				}
			}else{
					$data = @file_get_contents("http://img.msg.yahoo.com/avatar.php?yids=" . $nickyahoo . "&format=gif");
					if($data !== false){
						get_file($filename,$data);
						if(file_exists($filename)){
							resize_image("../channel_category/","avatar" . $nickyahoo . ".gif",23,20,100);
						}
					}
			}
		}
		get_file($fileonline,$file_content);
		return '<a href="ymsgr:sendim?' . $nickyahoo . '" ' . $tooltip . '><img src="/lib/img/yahoo_' . intval($check_ym) . '.gif" border="0" align="absmiddle" /></a>';
	}
}

# Kiem tra yahoo online 
function isonline($nick){
	$query=file("http://opi.yahoo.com/online?u=$nick&m=t");
	if (strpos($query[0],"NOT ONLINE")==0){
		return true;
	}else{
		return false;
	}
}

# Random string
function randString($length)
{
	$random       = "";
	srand((double)microtime()*1000000);
	$charList    = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	$charList    .= "abcdefghijklmnopqrstuvwxyz";
	$charList     .= "1234567890";
	for($i = 0; $i < $length; $i++){
		$random .= substr($charList,(rand()%(strlen($charList))), 1);
	}
	return $random;
}
 
function randomkey($str,$keyword = 0){
	$return = '';
	$strreturn = explode(" ",trim($str));
	$i=0;
	$listid = '';
	while($i<count($strreturn)){
		$id = rand(0,count($strreturn));
		if(strpos($listid,'[' . $id . ']')===false){
			if(isset($strreturn[$id])){
				$return .= $strreturn[$id] . ' ';
				$i++;
				if($keyword == 1 && ($i%2)==0 && $i<count($strreturn))  $return .= ',';
				$listid .= '[' . $id . ']';
			}
		}
	}
	return $return;
}

# Lay bien theo rewrite hay ko rewrite
function getURLR($mod_rewrite = 1,$serverName=0, $scriptName=0, $fileName=1, $queryString=1, $varDenied=''){
	if($mod_rewrite==1){
		return $_SERVER['REQUEST_URI'];
	}else{
		return getURL($serverName, $scriptName, $fileName, $queryString, $varDenied);
	}
}

# Lay 1 phan bien trong URL
function getUrlPage($array=array(),$removeKey=''){
	$url = '';
	foreach($array as $key=>$value){
		$param = getValue($key,$value,"GET","");
		if($param != '' && $removeKey!=$key){
			$url .= '&' . $key . "=" . $param;
		}
	}
	return $url;
}


function removethuoctinh($value){
	$value = str_replace("|","",$value);
	$value = str_replace(";","",$value);
	return $value;
}

function bbcode($string){
	$string = str_replace("[b]","<b>",$string);
	$string = str_replace("[/b]","</b>",$string);
	$string = str_replace("[hr]","<hr>",$string);
	$string = str_replace("[br]","<br>",$string);
	$string = str_replace("<hr><br>","<hr>",$string);
	$string = preg_replace("#\[yahoo\](.*?)\[/yahoo\]#si",'<a href="ymsgr:sendim?$1"><img src="/lib/img/yahoo_1.gif" align="absmiddle" border="0"></a>',$string);
	$string = preg_replace("#\[skype\](.*?)\[/skype\]#si",'<a href="skype:$1?chat"><img src="/lib/img/skype.gif" align="absmiddle" border="0"></a>',$string);
	return $string;
}
//*/

//Đổi resultset ra array
function convert_result_set_2_array($result_set, $id_field="", $value_field=""){
	$array_return	= array();

	$id_field		= trim($id_field);
	$value_field	= trim($value_field);

	//Move first resultset
	if (mysql_num_rows($result_set) > 0) mysql_data_seek($result_set, 0);
	while ($row_t = mysql_fetch_assoc($result_set)){
		$value	= ($value_field == "" ? $row_t : $row_t[$value_field]);
		if($id_field == "") $array_return[] = $value;
		elseif(!isset($array_return[$row_t[$id_field]])) $array_return[$row_t[$id_field]] = $value;
	}

	//Sau khi loop move first lại từ đầu
	if (mysql_num_rows($result_set) > 0) mysql_data_seek($result_set, 0);

	return $array_return;
}
?>