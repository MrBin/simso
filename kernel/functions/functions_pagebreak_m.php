<?
//Function generatePageBar 2.0 (Support Ajax) -- Code Editor: boy_infotech
function generatePageBar($prefix, $current_page, $page_size, $total_record, $url, $normal_class, $selected_class, $rewrite = 0, $query_string = '', $pageType = "page", $previous = "&nbsp;&lt;&lt; ", $next = " &gt;&gt;&nbsp;"){
	global $lang_path;
	
	$str = '';
	#+
	#+ Phan trang kieu 0
	if ($rewrite == 0){
		if ($total_record % $page_size == 0){
			$num_of_page = $total_record / $page_size;
		}else{
			$num_of_page = (int)($total_record / $page_size) + 1;
		} // End if ($total_record % $page_size == 0){
		
		#+
		$page_num = 3;
				
		$start_page = $current_page - $page_num;
		if($start_page <= 0) $start_page = 1;
		
		if($current_page == 1){
			$end_page = $current_page + $page_num+2;
			if($end_page > $num_of_page) $end_page = $num_of_page;
		}elseif($current_page == 2){
			$end_page = $current_page + $page_num+1;
			if($end_page > $num_of_page) $end_page = $num_of_page;
		}else{
			$end_page = $current_page + $page_num;
			if($end_page > $num_of_page) $end_page = $num_of_page;	
		} // End if($current_page == 1){
		
		#+ 
		#+ Trang truoc
		if(($current_page > 1) && ($num_of_page > 1)){
			$str .= 
				'
				<li class="' . $normal_class . '">
					<a rel="nofollow" title="Previous page" href="' . $url . '&' . $pageType . '=1">' . $previous . '</a>
				</li>
				';
			if($start_page !=1) $str .= '  ';
		} // End if(($current_page > 1) && ($num_of_page > 1)){
			
		#+
		#+ Trang
		for($i=$start_page; $i<=$end_page; $i++){
			
			if($i != $current_page){
				$str .= 
					'
					<li class="' . $normal_class . '">
						<a rel="nofollow" title="Page '.$i.'" href="' . $url . '&' . $pageType . '='.$i.'">'.$i.'</a>
					</li>
					';
			}else{
				$str .= 
					'
					<li class="' . $selected_class . '">
						<a title="Page '.$i.'">'.$i.'</a>
					</li>
					';
			} // End if($i != $current_page){
				
		} // End for($i=$start_page; $i<=$end_page; $i++){
			
		#+
		#+ Trang sau
		if(($current_page < $num_of_page) && ($num_of_page > 1)){
			if($end_page < $num_of_page) $str .= '  ';
			$str .= 
				'
				<li class="' . $normal_class . '">
					<a rel="nofollow" title="Next page" href="' . $url . '&' . $pageType . '=' . ($num_of_page) . '">' . $next . '</a>
				</li>
				';
		} // End if(($current_page < $num_of_page) && ($num_of_page > 1)){

	#+
	#+ Phan trang kieu 1
	}elseif($rewrite == 1){
		
		if ($total_record % $page_size == 0){
			$num_of_page = $total_record / $page_size;
		}else{
			$num_of_page = (int)($total_record / $page_size) + 1;
		} // End if ($total_record % $page_size == 0){
		
		#+
		$page_num = 1;
				
		$start_page = $current_page - $page_num;
		if($start_page <= 0) $start_page = 1;
		
		if($current_page == 1){
			$end_page = $current_page + $page_num+2;
			if($end_page > $num_of_page) $end_page = $num_of_page;
		}elseif($current_page == 2){
			$end_page = $current_page + $page_num+1;
			if($end_page > $num_of_page) $end_page = $num_of_page;
		}else{
			$end_page = $current_page + $page_num;
			if($end_page > $num_of_page) $end_page = $num_of_page;	
		} // End if($current_page == 1){
		

		#+
		#+ Trang truoc
		if(($current_page > 1) && ($num_of_page > 1)){
			$str .= 
				'
				<li class="' . $normal_class . '">
					<a rel="nofollow" title="Previous page" href="'.$url.'1'.$query_string.'">' . $previous . '</a>
				</li>
				';
			if($start_page !=1) $str .= '  ';
		}
		
		#+
		#+ Trang
		for($i=$start_page; $i<=$end_page; $i++){
			if($i != $current_page){
				$str .= 
					'
					<li class="' . $normal_class . '">
						<a rel="nofollow" title="Page '.$i.'" href="'.$url.$i.$query_string.'">'.$i.'</a>
					</li>
					';
			}
			else{
				$str .= 
					'
					<li class="' . $selected_class . '">
						<a title="Page '.$i.'">'.$i.'</a>
					</li>
					';
			} // End if($i != $current_page){
		} // End for($i=$start_page; $i<=$end_page; $i++){
		
		#+
		#+ Trang sau
		if(($current_page < $num_of_page) && ($num_of_page > 1)){
			if($end_page < $num_of_page) $str .= '  ';
			$str .= 
				'
				<li class="' . $normal_class . '">
					<a rel="nofollow" title="Next page" href="'.$url.($num_of_page).$query_string.'">' . $next . '</a>
				</li>
				';
		} // End if(($current_page < $num_of_page) && ($num_of_page > 1)){


	#+
	#+ Phan trang kieu 2
	}elseif($rewrite == 2){
		
		#+
		if ($total_record % $page_size == 0){
			$num_of_page = $total_record / $page_size;
		}else{
			$num_of_page = (int)($total_record / $page_size) + 1;
		} // End if ($total_record % $page_size == 0){
		
		#+
		$page_num = 3;
				
		$start_page = $current_page - $page_num;
		if($start_page <= 0) $start_page = 1;
		
		if($current_page == 1){
			$end_page = $current_page + $page_num+2;
			if($end_page > $num_of_page) $end_page = $num_of_page;
		}elseif($current_page == 2){
			$end_page = $current_page + $page_num+1;
			if($end_page > $num_of_page) $end_page = $num_of_page;
		}else{
			$end_page = $current_page + $page_num;
			if($end_page > $num_of_page) $end_page = $num_of_page;	
		} // End if($current_page == 1){
		

		#+
		#+ Trang truoc
		if(($current_page > 1) && ($num_of_page > 1)){
			$str .= 
				'
				<li class="' . $normal_class . '">
					<a rel="nofollow" title="Previous page" href="/p1' . $url. '">' . $previous . '</a>
				</li>
				';
			if($start_page !=1) $str .= '  ';
		} // End if(($current_page >= 1) && ($num_of_page > 1)){
		
		#+ Trang
		for($i=$start_page; $i<=$end_page; $i++){
			if($i != $current_page){
				$str .= 
					'
					<li class="' . $normal_class . '">
						<a rel="nofollow" title="Page '.$i.'" href="/p' . $i . $url . '">'.$i.'</a>
					</li>
					';
			}else{
				$str .= 
					'
					<li class="' . $selected_class . '">
						<a title="Page '.$i.'">'.$i.'</a>
					</li>
					';
			} // End if($i != $current_page){
			
		} // End for($i=$start_page; $i<=$end_page; $i++){
		
		#+ 
		#+ Trang sau
		if(($current_page < $num_of_page) && ($num_of_page > 1)){
			if($end_page < $num_of_page) $str .= '  ';
			$str .= 
				'
				<li class="' . $normal_class . '">
					<a rel="nofollow" title="Next page" href="/p' . ($num_of_page) . $url . '">' . $next . '</a>
				</li>
				';
		} // End if(($current_page <= $num_of_page) && ($num_of_page > 1)){

	} // End if ($rewrite == 0){
	
	return $str;
} // End function generatePageBar


#+
#+ Phan trang cho ajax
function generatePageBar_2($prefix, $current_page, $page_size, $total_record, $url, $normal_class, $selected_class, $page_id = '', $pageType = "page", $previous = "<", $next = ">"){
	$str = '';
		
	if ($total_record % $page_size == 0){
		$num_of_page = $total_record / $page_size;
	}
	else{
		$num_of_page = (int)($total_record / $page_size) + 1;
	}

	//
	$page_num = 2;
		
	$start_page = $current_page - $page_num;
	if($start_page <= 0) $start_page = 1;

	if($current_page == 1){
		$end_page = $current_page + $page_num+2;
		if($end_page > $num_of_page) $end_page = $num_of_page;
	}elseif($current_page == 2){
		$end_page = $current_page + $page_num+1;
		if($end_page > $num_of_page) $end_page = $num_of_page;
	}else{
		$end_page = $current_page + $page_num;
		if($end_page > $num_of_page) $end_page = $num_of_page;	
	}

	//Write Previous
	if(($current_page >= 1) && ($num_of_page > 1)){
		$str .= '<li class="' . $normal_class . '"><a title="Trang truoc"  onclick="changePage(\''.$page_id.'\', \''.$pageType.'=1\', \''.$url.'\')" href="javascript:;">'.$previous.'</a></li>';
		if($start_page !=1) $str .= "  ";
	}
	for($i=$start_page; $i<=$end_page; $i++){
		if($i != $current_page){
			$str .= '<li class="' . $normal_class . '"><a title="Trang '.$i.'" onclick="changePage(\''.$page_id.'\', \''.$pageType.'='.$i.'\', \''.$url.'\')" href="javascript:;">'.$i.'</a></li>';
		}
		else{
			$str .= "<li class='" . $selected_class . "'><a title='Trang ".$i."'>" . $i . "</a></li>";
		}
	}
	//Write Next
	if(($current_page <= $num_of_page) && ($num_of_page > 1)){
		if($end_page < $num_of_page) $str .= "  ";
		$str .= '<li class="' . $normal_class . '"><a title="Trang sau"  onclick="changePage(\''.$page_id.'\', \''.$pageType.'='.$num_of_page.'\', \''.$url.'\')" href="javascript:;">'.$next.'</a></li>';
	}

	return $str;
}
?>