<?php
#+
#+ Function them cac san pham vao bao gia
function controlOrder($order,$row,$sl = 1,$action){
	#+
	#+ Khai bao bien
	$new_cookie = '';
	$pro_exist  = 0;
	$arrayOrder = array();

	#+
	#+ Be biến order trong cookie để lấy ra thông tin sản phẩm
	$arrayTemp  = explode(";",trim($order));
	
	#+
	#+ Xu ly thong tin trong order
	foreach($arrayTemp as $key=>$value){
		
		#+
		#+ Be tiep de lay ra cac thong tin chi tiet
		$arrayRow = explode("|",trim($value));

		#+
		#+ Cac thong tin trong array order duoc sap xep theo thu tu 0 -> id, 1-> so luong
		$pro_id	= isset($arrayRow[0]) ? intval($arrayRow[0]) : 0;
		$pSoLuong 	= isset($arrayRow[1]) ? intval($arrayRow[1]) : 1;
		$pPrice 	= isset($arrayRow[2]) ? intval($arrayRow[2]) : 0;

		#+
		#+ Nếu như order trong cookie đã có
		if($pro_id!=0){

			#+
			#+ Nếu như sản phẩm thêm vào báo giá này đã có
			if($row["pro_id"] == $pro_id){
				#+
				#+ Nếu như số lượng  < 1
				if($sl<=1){
					$soluong = (intval($pSoLuong)+1);
				#+
				#+ Neu như số lượng >= 1 thì
				}else{
				   	$soluong = $sl;
				} // End if($sl<1){
				
				#+
				#+ Tao ra str cookie
				$new_cookie .= $pro_id.'|'.$soluong.'|'.$pPrice;
				
				#+ 
				#+ Đánh dấu lại là sản phẩm đã có
				$pro_exist = 1;
			#+
			#+ Nếu như sản phẩm chưa có
			}else{
				#+
				#+ Update lai bao gia
				if($action == 'update'){
					#+
					#+ Update lai so luong san pham
					$soluong	= getValue("soluong".$pro_id,"int","POST",1);
					$soluong 	= $soluong > 1 ? intval($soluong) : 1;

					#+
					#+ Neu nhu ban ghi ko bi xoa	
					if(getValue("delete".$pro_id,"int","POST",0)==0){
						$new_cookie .= $pro_id.'|'.$soluong.'|'.$pPrice;
					} // End if(getValue("delete_".$pro_id,"int","POST")==0)
					
				}elseif($action == 'add'){
					$soluong = $pSoLuong;
					$soluong = $soluong > 1 ? intval($soluong) : 1;
					#+
					#+ Tao ra str cookie
					$new_cookie .= $pro_id.'|'.$soluong.'|'.$pPrice;
				} // End if($action == 'update')
				
			} // End if($row["pro_id"] == $pro_id){
				

			$new_cookie .= ';';
			
		} // End if($pro_id!=0){
	  
	} // End foreach($arrayTemp as $key=>$value){
	
	#+
	#+ Neu nhu chua ton tai order cookie
	if($pro_exist==0){
		if($row['pro_id'] != 0){
			$new_cookie = $new_cookie.$row["pro_id"].'|1|'.$row['pro_price'];
		} // End if($row['pro_id'] != 0)
	} // End if($pro_exist==0){
		
	#+
	#+ Setcookie order
	setcookie('order',$new_cookie,null,"/");
	return $new_cookie;
} // End function addToOrder

#+
#+ Function dua ra cac san pham bao gia
function breakOrder($order,$listField = 'pro_id'){
	#+
	#+ Khai bao bien
	$arrayOrder 	= array();
	$arrayOrderTemp = array();
	$listid       	= '0';
	
	#+
	#+ Be biến order trong cookie để lấy ra thông tin sản phẩm
	$arrayTemp  	= explode(";",trim($order));
	
	#+
	#+ Xu ly thong tin trong order
	foreach($arrayTemp as $key=>$value){
		
		#+
		#+ Be tiep de lay ra cac thong tin chi tiet
		$arrayRow = explode("|",trim($value));
		
		#+
		#+ Cac thong tin trong array order duoc sap xep theo thu tu 0 -> id, 1-> so luong
		$pro_id     = isset($arrayRow[0]) ? intval($arrayRow[0]) : 0;
		$listid    .= "," . $pro_id;
		$arrayOrderTemp[$pro_id]['psoluong']    	= isset($arrayRow[1]) ? $arrayRow[1] : 1;
		$arrayOrderTemp[$pro_id]['pgia']       		= isset($arrayRow[2]) ? $arrayRow[2] : 0;
		# $arrayOrderTemp[$pro_id]['pid']          	= $pro_id;
		# $arrayOrderTemp[$pro_id]['pgia']       	= isset($arrayRow[1]) ? $arrayRow[1] : 0;
		# $arrayOrderTemp[$pro_id]['pbaohanh']    	= isset($arrayRow[3]) ? $arrayRow[3] : '';
		# $arrayOrderTemp[$pro_id]['pvat']       	= isset($arrayRow[4]) ? $arrayRow[4] : 0;
		# $arrayOrderTemp[$pro_id]['pexchange']   	= isset($arrayRow[5]) ? $arrayRow[5] : 0;
		# $arrayOrderTemp[$pro_id]['pname']       	= isset($arrayRow[6]) ? $arrayRow[6] : '';
	}
	
	#+
	#+ Thuc hien query
	$query = ' SELECT '.$listField 
		   .' FROM tbl_products'
		   .' INNER JOIN tbl_category ON(pro_category = cat_id)'
		   .' WHERE pro_id IN('.$listid.')'
		   ;
	$db_query = new db_query($query);
	#+
	#+ Dua them vao san pham duoc lay nhung thong so can thiet
	while($row=mysql_fetch_assoc($db_query->result)){
		$arrayOrder[$row["pro_id"]]                     = $row;
		$arrayOrder[$row["pro_id"]]['psoluong']         = $arrayOrderTemp[$row["pro_id"]]['psoluong'];
		$arrayOrder[$row["pro_id"]]['pgia']         	= $arrayOrderTemp[$row["pro_id"]]['pgia'];
	} // End while($row=mysql_fetch_assoc($db_query->result)){
	
	#+
	#+ Huy bien
	$db_query->close;
	unset($db_query);
	#+
	unset($arrayOrderTemp);
	unset($arrayTemp);
	
	#+
	#+ Tra lai array hoan chinh
	return $arrayOrder;
} // End function breakOrder

#+
#+ Function dua ra cac san pham bao gia
function getInfoOrder($order){
	global $saleOff;
	#+
	#+ Khai bao bien
	$total 	= 0;
	$tsl    = 0;
	$arrayReturn = array();
	
	#+
	#+ Be biến order trong cookie để lấy ra thông tin sản phẩm
	$arrayTemp  = explode(";",trim($order));
	
	foreach($arrayTemp as $key=>$value){
		#+
		#+ Be tiep de lay ra cac thong tin chi tiet
		$arrayRow = explode("|",trim($value));
		
		#+
		#+ Xem co bao nhieu ban ghi
		$soluong    	= isset($arrayRow[1]) ? $arrayRow[1] : 0;
		$price    		= isset($arrayRow[2]) ? $arrayRow[2] : 0;
		$tSl         	+= $soluong;
		$tPri         	+= $soluong*$price*1000*(1-$saleOff);
	}
	
	$arrayReturn["tSl"]     = $tSl;
	$arrayReturn["tPri"]    = $tPri;
	return $arrayReturn;
}

#+
function orderStatic($content,$row,$returnarray=0){
	global $arrayFieldOrder;
   $arrayField = array("ord_name"=>"Tên khách đặt"
                       ,"ord_address"=>"Địa chỉ khách đặt"
                       ,"ord_email"=>"Emai khách đặt"
                       ,"ord_phone"=>"Điện thoại khách đặt"
                       ,"ord_fax"=>"Fax khách đặt"
                       ,"ord_otherinfo"=>"Thông tin khác của khách đặt"
                       ,"ord_sname"=>"Tên khách nhận"
                       ,"ord_saddress"=>"Địa chỉ khách nhân"
                       ,"ord_semail"=>"Emai khách nhận"
                       ,"ord_sphone"=>"Điện thoại khách nhận"
                       ,"ord_sfax"=>"Fax khách nhận"
                       ,"ord_sotherinfo"=>"Thông tin khác của khách nhận"
                       ,"adm_name"=>"Người làm đơn hàng"
                       ,"adm_email"=>"Email người làm ĐH"
                       ,"adm_phone"=>"Điện thoại người làm ĐH"
							  ,"ngay"=>"Ngày"
							  ,"thang"=>"Tháng"
							  ,"nam"=>"Năm"
                       );
	foreach($arrayFieldOrder as $key=>$value){
		$arrayField[$key] = $value;
	}							  
	$row["ngay"] = date("d");						  
	$row["thang"] = date("m");						  
	$row["nam"] = date("Y");						  
   if($returnarray == 1) return    $arrayField;   
   $db_usernv = new db_query("SELECT adm_name, adm_email,adm_phone 
                              FROM admin_user 
                              WHERE adm_id = " . $row["admin_id"]);
   $orderrow = mysql_fetch_assoc($db_usernv->result);
   unset($db_usernv);
   
   $row["adm_email"] = $orderrow["adm_email"];
   $row["adm_name"] = $orderrow["adm_name"];
   $row["adm_phone"] = $orderrow["adm_phone"];
              
   foreach($arrayField as $key=>$value){ 
		$strreplace = isset($row[$key]) ? $row[$key] : '';                   
      $content = str_replace("{#"  . $key . "#}",$strreplace,$content);
   }
   return $content;
}
function baogiaStatic($content,$row,$returnarray=0){
   $arrayField = array("ord_name"=>"Họ tên khách đặt báo giá"
                       ,"ord_phone"=>"Điện thoại khách đặt báo giá"
                       ,"ord_email"=>"Emai khách đặt báo giá"
                       ,"ord_address"=>"Điện thoại khách đặt báo giá"
							  ,"ord_company"=>"Điện thoại khách đặt báo giá"
							  ,"date"=>"Ngày"
							  ,"month"=>"Tháng"
							  ,"year"=>"Năm"
                       );
	$row["date"] = date("d");						  
	$row["month"] = date("m");						  
	$row["year"] = date("Y");						  
   foreach($arrayField as $key=>$value){                    
      if(isset($row[$key])){
			$content = str_replace("{#"  . $key . "#}",$row[$key],$content);
		}else{
			$content = str_replace("{#"  . $key . "#}","",$content);
		}
   }
   return $content;
}
?>