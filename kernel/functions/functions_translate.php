<?
#***
# * Core Finalstyle
#***

function translate_text($variable){
	return $variable;
}

function translate_display_text($variable){
	return $variable;
}

function tt($variable){
	return $variable;
}


function ttg($variable){
	return $variable;
}

function ttg_cat($variable){
	return $variable;
}


function tdt($variable){
	return $variable;
}

function gt($variable){
	return $variable;
}

#***
# * Core Finalstyle Old
#***

/*/
function translate_text($variable){
	global $lang;
	if (isset($lang[$variable])){
		if($lang[$variable] !=''){
			return $lang[$variable];
		}else{
			return "";
		}
	}
	else{
		writeToTranslate($variable,"../0/translate.txt");
		return "-{" . $variable . "}-";
	}
}
function tdt($variable){
	global $lang_display;
	if (isset($lang_display[$variable])){
		if($lang_display[$variable]==''){
			return "#" . $variable . "#";
		}else{
			return $lang_display[$variable];
		}
	}
	else{
		writeToTranslate($variable);
		return "-{" . $variable . "}-";
	}
}
function help($variable){
	global $arhelp;
	if (isset($arhelp[$variable])){
		if($arhelp[$variable] !=''){
			return $arhelp[$variable];
		}else{
			return "";
		}
	}
	else{
		//writeToTranslate($variable,"../0/help.txt");
		return "-{" . $variable . "}-";
	}
}

function writeToTranslate($variable,$url="../language/translate_display.txt"){
	$content = @file_get_contents($url);
	$arrayTranslate = explode("\n",$content);
	$arrayTranslate = array_flip($arrayTranslate);
	$arrayCheck = array();
	$content = '';
	//@arsort($arrayTranslate);
	foreach($arrayTranslate as $key=>$value){
		$arrayCheck[trim(str_replace("\n","",$key))]=0;
		$content .= trim(str_replace("\n","",$key)) . "\n";
	}
	//print_r($arrayCheck);
	if(!isset($arrayCheck[$variable])){
		$content = $content . $variable;
		@file_put_contents($url,$content);
	}
}
//*/
?>