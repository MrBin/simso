<?
//Function generatePageBar 2.0 (Support Ajax) -- Code Editor: boy_infotech
function generatePageBar($prefix, $current_page, $page_size, $total_record, $url, $normal_class, $selected_class, $query_string = '', $data_pjax = "", $page_num = 3, $previous = "&nbsp;&lt;&lt; ", $next = " &gt;&gt;&nbsp;"){
	
	#+
	#+ Tính số lượng trang
	if ($total_record % $page_size == 0){
		$num_of_page = $total_record / $page_size;
	}else{
		$num_of_page = (int)($total_record / $page_size) + 1;
	} // End if ($total_record % $page_size == 0){

	
	$start_page = $current_page - $page_num;
	if($start_page <= 0) $start_page = 1;
	
	if($current_page == 1){
		$end_page = $current_page + $page_num+1;
		if($end_page > $num_of_page) $end_page = $num_of_page;
	}elseif($current_page == 2){
		$end_page = $current_page + $page_num+1;
		if($end_page > $num_of_page) $end_page = $num_of_page;
	}else{
		$end_page = $current_page + $page_num;
		if($end_page > $num_of_page) $end_page = $num_of_page;	
	} // End if($current_page == 1){
	
	
	#+
	#+ Khai bao bien
	$str = '';
	$normal_class 		= $normal_class != '' ? ' class="'.$normal_class.'"' : '';
	$selected_class 	= $selected_class != '' ? ' class="'.$selected_class.'"' : '';

	#+
	#+ Trang truoc
	if(($current_page > 1) && ($num_of_page > 1)){
		$str .= '
				<li'.$normal_class.'>
					<a rel="nofollow" title="'.$prefix.' trước" href="'.$url.'1'.$query_string.'"'.$data_pjax.'>'.$previous.'</a>
				</li>
				';
	} // End if
	
	#+
	#+ Trang
	for($i=$start_page; $i<=$end_page; $i++){
		if($i != $current_page){
			#+
			#+ Trang đánh số
			$str .= '
					<li'.$normal_class.'>
						<a rel="nofollow" title="'.$prefix.' '.$i.'" href="'.$url.$i.$query_string.'"'.$data_pjax.'>'.$i.'</a>
					</li>
					';
		}else{
			#+
			#+ Trang hiện tại
			$str .= '
					<li'.$selected_class.'>
						<a title="'.$prefix.' '.$i.'">'.$i.'</a>
					</li>
					';
		} // End if
	} // End for
	
	#+
	#+ Trang sau
	if(($current_page < $num_of_page) && ($num_of_page > 1)){
		$str .= '
				<li'.$normal_class.'>
					<a rel="nofollow" title="'.$prefix.' sau" href="'.$url.($num_of_page).$query_string.'"'.$data_pjax.'>'.$next.'</a>
				</li>
				';
	} // End if
	
	return $str;
} // End function generatePageBar
?>