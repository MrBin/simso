<?
include('config.php');

function log_api($filename, $content){
		
	$log_path     =   $_SERVER["DOCUMENT_ROOT"] . "/store/logs/";
	$handle       =   @fopen($log_path . $filename . ".cfn", "a");
	//Neu handle chua co mo thêm ../
	if (!$handle) $handle = @fopen($log_path . $filename . ".cfn", "a");
	//Neu ko mo dc lan 2 thi exit luon
	if (!$handle) exit();		
	fwrite($handle, date("d/m/Y h:i:s A") . " " . @$_SERVER["REQUEST_URI"] . "\n" . $content . "\n");	
	fclose($handle); 	
		
}

$arrReturn 	= array("status" => 0, "message" => "", "success" => 0, "fail" => 0);
$fs_table 	= "tbl_sim";
$control		= getValue("control", "str", "POST", "");
$data_sim 	= getValue("data_sim", "str", "POST", json_encode(array()));
$token		= getValue("token", "str", "POST", "");

if($token != "e39bcbfc59b5fa5334677de1c1e53a49a96ad419112a6c1b220003e928132dbcc76ca962d7b243599f8e0151137fb082e9205a59b8f140c4b2166c3e2e600cee"){
	$arrReturn["message"] .= "Lỗi Token \n";
	echo json_encode($arrReturn);
	exit();
}

#$data_sim	= '"' . $data_sim . '"';
$data_sim	= json_decode($data_sim, true);
// var_dump($data_sim);
// exit();
#print_r($status); exit();
if(!empty($data_sim)){
	$arrReturn = quanlysim($data_sim, $control);

	if($arrReturn["message"] != ""){
		log_api("sim_error", $arrReturn["message"]);
	}
	echo json_encode($arrReturn);
}
else{
	$arrReturn["message"] .= "Data post error \n";
	echo json_encode($arrReturn);
}

function quanlysim( $data, $control ){

	global $arrReturn;

	$arrJsonError = array();

	if(empty($data)) return $countDataReturn;

	// Loai sim
	$db_sim_type  = new db_query("SELECT simtp_id,simtp_name,simtp_index
											FROM tbl_simtype
											WHERE simtp_active = 1
											ORDER BY simtp_order
											");
	$loai_sim_idx = array();
	while($row = mysql_fetch_assoc($db_sim_type->result)) $loai_sim_idx[$row["simtp_index"]] = $row["simtp_id"];
	unset($db_sim_type);
	
	// Gia sim
	$db_sim_price = new db_query("SELECT pri_id,pri_min,pri_max
											FROM tbl_simprice
											");
	$gia_sim_idx = array();
	while($row = mysql_fetch_assoc($db_sim_price->result)){
		$gia_sim_idx[$row['pri_id']] = array(
											'pri_min'=>$row['pri_min'],
											'pri_max'=>$row['pri_max']
											);
	} // End while($row = mysql_fetch_assoc($db_sim_price->result))
	unset($db_sim_price);
	
	// Dau so
	$db_sim_dauso = new db_query("SELECT sds_id,sds_category,sds_name,sds_name
											FROM tbl_simdauso
											WHERE sds_active = 1
											");
	$dauso_sim_idx = array();
	while($row = mysql_fetch_assoc($db_sim_dauso->result)){
		$index = intval($row['sds_name']);
		$dauso_sim_idx[$index] = array('sds_id'=>$row["sds_id"],
												'sds_category'=>$row["sds_category"]
												);
	} // End while($ds = mysql_fetch_assoc($db_sim_dauso->result))
	unset($db_sim_dauso);
	
	// Dai ly
	$db_sim_daily = new db_query("SELECT *
											FROM tbl_simdaily
											WHERE simch_active = 1
											ORDER BY simch_order
											");
	$daily_sim_idx = array();
	$i = 0;
	while($ch = mysql_fetch_assoc($db_sim_daily->result)){
		$i++;
		$daily_sim_idx[$ch["simch_viettat"]] = $ch['simch_id'];
	} // End while($ch = mysql_fetch_assoc($db_sim_daily->result))
	unset($db_sim_daily);
	
	$db_sim_daily = new db_query("SELECT simch_viettat 
											FROM tbl_simdaily 
											WHERE simch_show = 1"
											);
	$arrSimWeb = array();
	while($row = mysql_fetch_assoc($db_sim_daily->result)) $arrSimWeb[] = $row['simch_viettat'];
	unset($db_sim_daily);

	$m = 0;
	$j = 0;
	$k = 0;
	$strCheck = "";
	foreach ( $data as $kg => $sg ){
		$m++;

		$checkSim 	= true;

		// Sim
		$sim_sim1 	= soreplace($sg['sim_sim1']);
		$sim_sim2 	= getDataSim2($sim_sim1);
		// if(strlen($sim_sim2) > )
		$sim_price 	= $sg['sim_price'];
		$daily		= $sg['sim_daily'];
		$sim_date 	= date("Y-m-d H:i:s");

		$sim_keyword = genTrigramsKeyword("0" . $sim_sim2);
		
		// Xac dinh sim cua cua hang
		$sim_web = 0;
		if(in_array($daily, $arrSimWeb)) $sim_web = 1;
		
		// Xac dinh loai sim
		$sim_typeid		= checkSimType($sim_sim2, $loai_sim_idx);
		
		// Xac dinh dai ly sim
		$sim_dailyid  	= isset($daily_sim_idx[$daily]) ? $daily_sim_idx[$daily] : '';
		
		// Xac dinh gia sim
		$sim_priceid	= 1;
		foreach($gia_sim_idx as $key => $row){
			if($sim_price > $row['pri_min'] && $sim_price <= $row['pri_max']){
				$sim_priceid = $key;
				break;
			}
		} // End foreach($price_idx as $key => $row)
		
		// Xac dinh mang sim va dau so
		$sim_category	= 1;
		$sim_dausoid	= 1;
		$dau1 = substr($sim_sim2,0,1);
		$dau2 = substr($sim_sim2,0,2);
		$dau3 = substr($sim_sim2,0,3);

		if($dau1 == 2){
			if(strlen($sim_sim2) < 10 || strlen($sim_sim2) > 10){

				$checkSim = false;
			}
		}
		else{
			if(strlen($sim_sim2) < 9 || strlen($sim_sim2) > 9){
				$checkSim = false;
			}
		}

		if(!$checkSim){
			$arrJsonError[] = "Sim " . $sim_sim1 . " error!";
			$k++;
			
			// Log lại sim lỗi
			$db_sim_error = new db_execute("INSERT IGNORE INTO sim_error (se_sim,se_sim2,se_price,se_daily,se_date) VALUES ('" . $sim_sim1 . "', " . $sim_sim2 . ", " . $sim_price . ", '" . $sg['sim_daily'] . "', " . time() . ")");
			unset($db_sim_error);
		}
		else{
			$j++;
		}

		if(array_key_exists($dau3, $dauso_sim_idx)){
			$sim_dausoid	= $dauso_sim_idx[$dau3]['sds_id'];
			$sim_category	= $dauso_sim_idx[$dau3]['sds_category'];
		}elseif(array_key_exists($dau2, $dauso_sim_idx)){
			$sim_dausoid	= $dauso_sim_idx[$dau2]['sds_id'];
			$sim_category	= $dauso_sim_idx[$dau2]['sds_category'];
		}elseif(array_key_exists($dau1, $dauso_sim_idx)){
			$sim_dausoid	= $dauso_sim_idx[$dau1]['sds_id'];
			$sim_category	= $dauso_sim_idx[$dau1]['sds_category'];
		}  // End if(array_key_exists($dau, $dauso_sim_idx))
		// var_dump(strlen($sim_sim2));
		// exit();

		// Tao chuoi string theo control
		if($control == 'xoa_sim') $valuesx[] = $sim_sim2;
		else $valuesx[] = "('" . $sim_sim1 . "','" . $sim_sim2 . "','" . $sim_price . "','" . $sim_category . "','" . $sim_typeid . "','" . $sim_dailyid . "','" . $sim_priceid . "','" . $sim_dausoid . "','" . $sim_date . "','" . $sim_web . "', '" . $sim_keyword . "')";
			
		// Cứ đếm đủ 5k sim thì insert vào database 1 lần
		if($m%5000 == 0 ){

			quanlysim_control($control,$valuesx);

			// Chi truncate table 1 lan sau do la dang sim
			if($control == 'dang_xoa_sim') $control = 'dang_sim';
				
			$m = 0;
			unset( $valuesx );
		} // End if ( $m%5000 == 0 )

		
	} // End foreach ( $datapost['s'] as $kg => $sg )

	// Nếu không đủ 5k sim
	if ( 0 < $m && $m < 5000 ){
		quanlysim_control($control,$valuesx);
		unset( $valuesx );
	} // End if

	// Đưa ra thông báo đã insert thành công bao nhiêu sim
	$arrReturn["status"]     = 1;
	// $arrReturn["message"] = "Done " . $j . " number!";
	$arrReturn["success"]    = $j;
	$arrReturn["fail"]       = $k;

	return $arrReturn;
    
} // End function quanlysim


function quanlysim_control($control, $valuesx){
	
	global $fs_table;

	if($control == 'dang_sim' || $control == 'dang_xoa_sim'){

		if($control == 'dang_xoa_sim'){
			$db_ex = new db_execute("TRUNCATE TABLE " . $fs_table);
			unset($db_ex);

			$db_error = new db_execute("TRUNCATE TABLE sim_error");
			unset($db_error);
		}
		// echo "REPLACE INTO " . $fs_table . " (sim_sim1,sim_sim2,sim_price,sim_category,sim_typeid,sim_dailyid,sim_priceid,sim_dausoid,sim_date,sim_web,sim_keyword) VALUES " . join( ",", $valuesx );
		// exit();
		$db_ex = new db_execute("REPLACE INTO " . $fs_table . " (sim_sim1,sim_sim2,sim_price,sim_category,sim_typeid,sim_dailyid,sim_priceid,sim_dausoid,sim_date,sim_web,sim_keyword) VALUES " . join( ",", $valuesx ));
		unset($db_ex);

	}elseif($control == 'xoa_sim'){
	
		$db_ex = new db_execute("DELETE FROM " . $fs_table . " 
										WHERE sim_sim2 IN (" . join( ",", $valuesx ) . ")
										");	
		unset($db_ex);
		
	}
	
} // End function quanlysim_control

?>