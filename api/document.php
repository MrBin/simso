<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
    <meta name="robots" content="noindex, nofollow"/>

</head>
<body>
<h1>POST SIM: http://boisim.com.vn/api/auto_up.php</h1>
<h2>@Params:</h2>
<ul>
    <li><span class="param"><b>sim_sim1</b> :</span> <span class="value">string - Số đầy đủ</span> </li>
    <li><span class="param"><b>sim_sim2</b> :</span> <span class="value">string - Số liền không có số 0 và các ký tự (".", " ")</span> </li>
    <li><span class="param"><b>sim_price</b> :</span> <span class="value">string - Giá sim</span> </li>

    <li><span class="param"> <b>sim_daily</b>:</span> <span class="value">string - Đại lý (A1 => Hàng ngoài, ST => Hàng của nhà)</span> </li>
    <li><span class="param"> <b>control</b>:</span> <span class="value">string - Hành động xử lý sim ( <b style="color: red">dang_sim</b> => Đăng mới trên hệ thống, <b style="color: red">xoa_sim</b> => Xóa sim tương ứng trên hệ thống, <b style="color: red">dang_xoa_sim</b> => Đăng mới và xóa sim cũ trên hệ thống)</span> </li>
</ul>
<h2>Ví dụ :</h2>
<pre>Array
(
    [963453437] => Array
        (
            [sim_sim1]  => 0963 453 437
            [sim_price] => 599000
            [sim_daily] => ST
        )
    [control] => dang_sim
)
</pre>
</body>
</html>