<?
require_once(dirname(__FILE__) . "/../config.php");
require_once(dirname(__FILE__) . "/../kernel/classes/fs_database.php");
require_once(dirname(__FILE__) . "/../kernel/functions/functions_all.php");
require_once(dirname(__FILE__) . "/../kernel/classes/rating.php");

$type				= getValue("type");
$record_id		= getValue("record_id");

// Khai báo dữ liệu để phân trang
$page_size		= 10;
$current_page	= getValue("p");
if($current_page < 1)	$current_page = 1;
if($current_page > 1000)$current_page = 1000;

// Lấy dữ liệu
$db_rating		= new db_query("SELECT rat_id, rat_admin_id, rat_order_id, rat_rating, rat_name, rat_mobile, rat_content, rat_count_reply, rat_date
										 FROM rating
										 WHERE rat_type = " . $type . " AND rat_date < " . $con_time . " AND rat_record_id = " . $record_id . "
										 ORDER BY rat_date DESC
										 LIMIT " . ($current_page - 1) * $page_size . ", " . $page_size);
$arrRating		= convert_result_set_2_array($db_rating->result);
$db_rating->close();
unset($db_rating);

// Hiển thị rating
$class_rating	= new rating($type);
echo $class_rating->generate_rating($arrRating, array("ajax" => true));
unset($class_rating);

// Nếu là admin thì thêm nút Sửa, Xóa
if($con_admin_id > 0) echo '<script type="text/javascript">setTimeout(generateRatingAction, 500)</script>';
?>