<?
require_once(dirname(__FILE__) . "/../config.php");
require_once(dirname(__FILE__) . "/../kernel/classes/fs_database.php");
require_once(dirname(__FILE__) . "/../kernel/classes/rating.php");
require_once(dirname(__FILE__) . "/../kernel/functions/functions_all.php");
require_once(dirname(__FILE__) . "/../kernel/functions/functions_date.php");

$con_admin_id		= checkCookieAdminLogin();

$type			= getValue("type", "int", "POST", 0);
$record_id	= getValue("record_id", "int", "POST", 0);
$category_id= 0;
$rating		= getValue("rating", "int", "POST", 0);
$rating_id	= getValue("rating_id", "int", "POST", 0);
$content		= getValue("content", "str", "POST", "");
$name			= getValue("name", "str", "POST", "");
$mobile		= getValue("mobile", "str", "POST", "");
$order_id	= 0;
$admin_id	= 0;
$post_date	= time();
if($con_admin_id > 0){
	$date			= getValue("date", "str", "POST", date("d/m/Y"));
	$time			= getValue("time", "str", "POST", date("H:i:s"));
	$order_id	= getValue("order_id", "int", "POST", 0);
	$is_admin	= getValue("is_admin", "int", "POST", 0);
	if($is_admin == 1 || $rating_id == 0) $admin_id	= $con_admin_id;
	$post_date	= convertDateTime($date, $time);
}

// Check xem dữ liệu có tồn tại hay ko theo $type
if($record_id < 1 || $record_id > 2) exit("[error]Data not found.");

// Lưu dữ liệu
$opts	= array ("admin_id"		=> $admin_id,
					"record_id"		=> $record_id,
					"category_id"	=> $category_id,
					"order_id"		=> $order_id,
					"rating"			=> $rating,
					"name"			=> $name,
					"mobile"			=> $mobile,
					"content"		=> $content,
					"date"			=> $post_date,
					);

$class_rating	= new rating($type);

if($rating_id > 0) $class_rating->add_rating_reply($rating_id, $opts);
else $class_rating->add_rating($opts);
if($class_rating->error !== false) exit("[error]Có lỗi xảy ra trong quá trình thực hiện");

// Trả về HTML để hiển thị
if($rating_id > 0){
	$arrData[$class_rating->last_reply_id]	= array ("rr_id"				=> $class_rating->last_reply_id,
																	"rr_admin_id"		=> $admin_id,
																	"rr_rating_id"		=> $rating_id,
																	"rr_name"			=> $name,
																	"rr_content"		=> $content,
																	"rr_date"			=> $post_date,
																	);
	echo $class_rating->generate_rating_reply($arrData, array("ajax" => true));
}
else{
	$arrData[$class_rating->last_rating_id]	= array ("rat_id"				=> $class_rating->last_rating_id,
																		"rat_admin_id"		=> $admin_id,
																		"rat_order_id"		=> $order_id,
																		"rat_rating"		=> $rating,
																		"rat_name"			=> $name,
																		"rat_content"		=> $content,
																		"rat_count_reply"	=> 0,
																		"rat_date"			=> $post_date,
																		);
	echo $class_rating->generate_rating($arrData, array("ajax" => true));
}
// unset($class_rating);

// Nếu là admin thì thêm nút Sửa, Xóa
if($con_admin_id > 0){
	$selector	= ($class_rating->last_reply_id > 0 ? "#reply_" . $class_rating->last_reply_id : "#rating_" . $class_rating->last_rating_id);
	echo '<script type="text/javascript">setTimeout(function(){ generateRatingAction($(\'' . $selector . '\')); }, 500)</script>';
}
?>