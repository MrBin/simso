<?
require_once(dirname(__FILE__) . "/../config.php");
require_once(dirname(__FILE__) . "/../kernel/classes/fs_database.php");
require_once(dirname(__FILE__) . "/../kernel/functions/functions_all.php");

// Khai báo biến được post lên
$simcard   = getValue('simcard', 'str', 'POST', "");
$giaban    = getValue('giaban', 'int', 'POST', "");
$hoten     = getValue('hoten', 'str', 'POST', "");
$dienthoai = getValue('dienthoai', 'str', 'POST', "");
$diachi    = getValue('diachi', 'str', 'POST', "");
$action    = getValue('action', 'str', 'POST',"");
$time      = time();
$ct_error  = array();

$arrReturn = array("status" => 0, "msg" => "");

// Check validate form
if($action == 'send'){
	if($hoten == "" || $dienthoai == "")	$arrReturn["msg"] = "Vui lòng nhập đúng họ tên và số điện thoại";
	else if(!validate_mobile_phone($dienthoai)) $arrReturn["msg"] = "Vui lòng nhập đúng số điện thoại";
	else{
		$db_insert	= new db_execute_return();

		$last_id		= $db_insert->db_execute("INSERT INTO tbl_yeucau (simcard, giaban, hoten, dienthoai, diachi, thoigian, phanloai)
												  			VALUES ('" . replaceMQ($simcard) . "', " . intval($giaban) . ", '" . replaceMQ($hoten) . "', '" . replaceMQ($dienthoai) . "', '" . replaceMQ($diachi) . "', '" . date("Y-m-d H:i:s") . "', 1)");
		unset($db_insert);

		if($last_id > 0) $arrReturn["status"] = 1;
		else $arrReturn["msg"] = "Query error";
	}
}
echo json_encode($arrReturn);
?>