<?
header("Content-type: text/xml");
include('../ext/lang.php');
include('../../kernel/classes/fs_generate_xml.php');

#+
#+ Khai bao bien
$now = gmdate('D, d M Y h:i:s').' GMT';

#+
#+ Thuc hien query lay du lieu
$query = ' SELECT sim_sim1,sim_sim2,sim_price,sim_category,sim_typeid'
		.' FROM tbl_sim'
		.' WHERE sim_active = 1'
		.' ORDER BY sim_web DESC'	
		.' LIMIT 0,20'
		;
$db_query = new db_query($query);	
	
#+
#+ Dua ra du lieu	
$str = '';			 
if(mysql_num_rows($db_query->result) > 0){
	$rss_channel 				= new gennerate_channel();
	$rss_channel->atomLinkHref  = 'http://'.$_SERVER['HTTP_HOST'].'/feed';
	$rss_channel->title 		= 'Rss - Feed - '.$con_site_title;
	$rss_channel->link 			= 'http://'.$_SERVER['HTTP_HOST'];
	$rss_channel->description 	= $con_meta_description;
	
	$rss_channel->copyright 	= $con_site_title;
	$rss_channel->generator 	= $con_site_title;
	$rss_channel->pubDate 		= $now;
	$rss_channel->lastBuildDate = $now;

	
	@mysql_data_seek($db_rss->result,0);
	while($row = mysql_fetch_array($db_query->result)){
		#+
		$sim_sim1 		= $row['sim_sim1'];
		$sim_sim2 		= '0'.$row['sim_sim2'];
		$sim_price 		= $row['sim_price'];
		$sim_price		= number_format($sim_price);
		$sim_category 	= $row['sim_category'];
		$sim_typeid		= $row['sim_typeid'];
	
		#+
		#+ 
		$cat_name = $arrSimCat[$sim_category]["cat_name"];

		#+
		$simtp_name		= $arrSimType[$sim_typeid]["simtp_name"];
		
		#+Tao link	
		$row['cat_name_index'] = $arrSimCat[$sim_category]["cat_name_index"];
		$link = createLink("detail_sim",$row);
		
		$item						= new rssGenerator_item();
		$item->title				= 'Sim so dep gia re '.$cat_name.' '.$sim_sim1.", ".$simtp_name.' '.$sim_sim2;
		$item->link					= 'http://'.$_SERVER['HTTP_HOST'].''.$link;
		$item->pubDate 				= $now;
		$item->description			= "";
		$item->description		   .= 'Sim so dep gia re '.$cat_name.' '.$sim_price.' Vnđ';
		$rss_channel->items[]		= $item;
	
	}// End while($row = mysql_fetch_array($db_rss->result))
	
	$rss_feed = new rssGenerator_rss();
	$rss_feed->encoding	= 'utf-8';
	$rss_feed->version	= '2.0';

	$str = $rss_feed->createFeed($rss_channel);
	
}// End if(mysql_num_rows($db_rss->result) > 0)

#+
#+ Huy bien
$db_query->close();
unset($db_query);

#+
#+ Dua ra du lieu
echo $str;
?>