<?
include('../ext/lang.php');

#+
#+ Xem co search sim voi lon hon 2 ki tu hay khong
if($module == 'searchsim' && strlen(preg_replace("/[^0-9]/si","",$keyword)) < 3){
	echo '<div class="alert alert-info">Vui lòng nhập ít nhất 2 chữ số để tìm!</div>';	
	return;
} // End if(strlen(preg_replace("/[^0-9*]/si","",$keyword)) < 3)

#+
#+ Set bien
$page_size		= (getValue("page_size") > 0 ) ? getValue("page_size") : 55;
$sqlSelect		= '';
$sqlJoin			= '';
$sql 				= '';
$sqlOrder 		= '';
$sqlGroup 		= '';
$sqlOrderBy 	= '';
$sqlLimit		= '';
#+
$normal_class    	= "";
$selected_class  	= "active";
$page_prefix 	 	= "Trang";
$current_page    	= ( getValue("page") < 1 ) ? 1 : getValue("page");
#+
$query_string = '/';
if($module == 'home')
{
	$url	= '/trang-';	
}
elseif($module == 'simtype')
{
	$url	= '/'.$sType.'-';
}
elseif($module == 'simprice')
{
	$url	= '/gia-'.$iPriceFrom.'-'.$iPriceTo.'-';	
}
elseif($module == 'simdauso')
{
	$url	= '/'.$sCat.'-dau-'.$sDauSo.'-';	
}
elseif($module == 'simsodep')
{
	$url	= '/sim-so-dep-';
}
elseif($module == 'simgiare')
{
	$url	= '/sim-gia-re-'.$sCat.'-';
}
elseif($module == 'simre')
{
	$url	= '/Sim-gia-re-';
}
elseif($module == 'simhoptuoi')
{
	$url	= '/sim-phong-thuy-hop-tuoi-'.$iNamSinh.'-';
}
elseif($module == 'simhopmenh')
{
	$url	= '/sim-phong-thuy-hop-menh-'.$sNguHanh.'-';
}
elseif($module == 'tra-phong-thuy')
{
	$url	= "/load/?type=sim&module=".$module."&iNguHanh=".$iNguHanh."&page=";
	$query_string = '';
}
elseif($module == 'searchsim' || $module == 'simajax')
{
	$url	= "/load/?type=sim&module=".$module."&keyword=".$keyword.'&iCat='.$iCat.'&iType='.$iType.'&iPrice='.$iPrice.'&iDauSo='.$iDauSo."&digit=".$digit."&sort=".$sort."&iNguHanh=".$iNguHanh."&page=";	
	$query_string = '';
}
else
{
	$url	= '/'.$sCat.'-';	
}

/* Query */
/* Khai bao bien */
$sphinx_page_start   = ($current_page-1) * $page_size;
$sphinx_page_end     = $page_size;
$sphinx_keyword     = $keyword;

/* Thuc hien search */
$sphinxSearch  = new sphinxSearch();
$result = $sphinxSearch->search($sphinx_page_start, $sphinx_page_end, $sphinx_keyword, $module, $iCat, $iType, $iPrice, $iDauSo, $digit, $iNguHanh, $sort);
$result = json_decode($result, true);

$total_record  = $result['total_record'];
$arrQuery      = $result['query'];
?>
<style>
*{
	font-size: 12px;
	font-family: Arial;
}
.table {
    width: 100%
}
.table tbody tr td,
.table tbody tr th,
.table tfoot tr td,
.table tfoot tr th,
.table thead tr td,
.table thead tr th {
    padding: 8px;
    line-height: 1.428571429;
    vertical-align: top;
    border-top: 1px solid #ddd
}
.table thead tr th {
    vertical-align: bottom;
    border-bottom: 2px solid #ddd
}
.table caption+thead tr:first-child td,
.table caption+thead tr:first-child th,
.table colgroup+thead tr:first-child td,
.table colgroup+thead tr:first-child th,
.table thead:first-child tr:first-child td,
.table thead:first-child tr:first-child th {
    border-top: 0
}
.table tbody+tbody {
    border-top: 2px solid #ddd
}
.table .table {
    background-color: #fff
}
.table-condensed tbody tr td,
.table-condensed tbody tr th,
.table-condensed tfoot tr td,
.table-condensed tfoot tr th,
.table-condensed thead tr td,
.table-condensed thead tr th {
    padding: 5px
}
.table-bordered,
.table-bordered tbody tr td,
.table-bordered tbody tr th,
.table-bordered tfoot tr td,
.table-bordered tfoot tr th,
.table-bordered thead tr td,
.table-bordered thead tr th {
    border: 1px solid #ddd
}
.table-bordered thead tr td,
.table-bordered thead tr th {
    border-bottom-width: 2px
}
.table-striped tbody tr:nth-child(odd) td,
.table-striped tbody tr:nth-child(odd) th {
    background-color: #f9f9f9
}
.table-hover tbody tr:hover td,
.table-hover tbody tr:hover th {
    background-color: #f5f5f5
}
table col[class*=col-] {
    float: none;
    display: table-column
}
table td[class*=col-],
table th[class*=col-] {
    float: none;
    display: table-cell
}
</style>
Tổng: <?=$total_record?>
<table cellpadding="0" cellspacing="0" class="table table-hover table-sim" style="width: 500px; margin: auto;">
	<tr>
		<td>Stt</td>
		<td>Số</td>
		<td>Giá</td>
		<td>Loại</td>
		<td>Mạng</td>
	</tr>
	<?
	foreach($arrQuery as $key => $row){
		$i++;
	
		if($row['sim_sim1'] == '') continue;
		
		$sim_sim1 		= $row['sim_sim1'];
		$sim_sim2 		= '0'.$row['sim_sim2'];
		$sim_price		= $row['sim_price'];
		$sim_price		= number_format($sim_price);
		$sim_category 	= $row['sim_category'];
		$sim_typeid 	= $row['sim_typeid'];
		$cat_name		= $arrSimCat[$sim_category]["cat_name"];
		$cat_picture	= $arrSimCat[$sim_category]["cat_picture"];
		$simtp_name		= $arrSimType[$sim_typeid]["simtp_name"];
		$sim_nguhanh 	= $row['sim_nguhanh'];
		$sim_sonut		= $row['sim_sonut'];
		$sim_diem_stst	= $row['sim_diem_stst'];
		
		$row['cat_name_index'] 	= $arrSimCat[$sim_category]["cat_name_index"];
		$row['simtp_index'] 		= $arrSimType[$sim_typeid]["simtp_index"];
	
		#+
		$link_detail = createLink("detail_sim",$row);
		?>
		<tr>
			<td><?=$i?></td>
			<td><b style="color: #2196F3; font-size: 16px;"><?=$sim_sim1?></b></td>
			<td><b style="color: #F44336; font-size: 14px;"><?=$sim_price?></b></td>
			<td><?=$simtp_name?></td>
			<td><img src="/store/media/category/<?=$cat_picture?>" alt="<?=$cat_name?>" /></td>
		</tr>
		<?
	} // End while($row = mysql_fetch_assoc($db_simType->result))
	unset($arrQuery);
	?>
</table>
