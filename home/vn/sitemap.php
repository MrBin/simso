<?
header("Content-type: text/xml");
include("../ext/lang.php");

$page      = getValue("page", "int", "GET", 1);
$page_size = 5000;
$strSosim  = "";

/* Khai bao bien */
$searchConfig =  array ("page"		=> $page,
								"page_size"	=> $page_size,
								"iCat"		=> $iCat,
								"iType"		=> $iType,
								"iPrice"		=> $iPrice,
								"iDauSo"		=> $iDauSo,
								"iDaiLy"		=> $iDaiLy,
								"digit"		=> $digit,
								"iNguHanh"	=> $iNguHanh,
								"module"		=> $module,
								"sort"		=> $sort,
								);

/* Thuc hien search */
$sphinx_search = new sphinx_search();
$arrResult     = $sphinx_search->search($searchConfig);

$arrQuery		= array();
$total_record	= 0;

if(!empty($arrResult["data"])){
	$arrQuery		= $arrResult["data"];
	$total_record	= $arrResult["total_record"];
}

if(!empty($arrQuery)){

	if($total_record % $page_size == 0) $num_of_page = $total_record / $page_size;
	else $num_of_page = (int)($total_record / $page_size) + 1;

	for ($i=1; $i <= $num_of_page; $i++) { 
		$strSosim   .= '<sitemap>
						      <loc>' . $con_server_name . '/sosim-sitemap' . $i . '.xml</loc>
						      <lastmod>' . date("c") . '</lastmod>
						   </sitemap>';
	}

}
?>
<sitemapindex xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/siteindex.xsd">
   <sitemap>
      <loc><?=$con_server_name?>/policy-sitemap.xml</loc>
      <lastmod><?=date("c")?></lastmod>
   </sitemap>
   <sitemap>
      <loc><?=$con_server_name?>/catsim-sitemap.xml</loc>
      <lastmod><?=date("c")?></lastmod>
   </sitemap>
   <?=$strSosim?>
   <sitemap>
      <loc><?=$con_server_name?>/news-sitemap.xml</loc>
      <lastmod><?=date("c")?></lastmod>
   </sitemap>
</sitemapindex>