<?
require_once('../ext/lang.php');

ob_start('callback');

echo '<title>' . strip_tags($con_site_title) . '</title>';

include('../../inc/inc_pajax'.$deviceInclude.'.php');
include('time_load_page.php');

// Cache toan site
if($con_set_cache1 == 1){
	if($type == 'sim'){
		if ($str_cache_all == '') {
			$cache = ob_get_contents();
			$myMemcache->set($key_cache_all, $cache, $time_cache_1_month);
			unset($cache);	
		}
	}
}

ob_end_flush();
?>
