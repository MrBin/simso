<? if($pjax != 2){?>
<!DOCTYPE html>
<html lang="vi">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="description" content="<?=strip_tags($con_meta_description)?>" />
<meta name="keywords" content="<?=strip_tags($con_meta_keywords)?>" />
<? }?>
<title><?=strip_tags($con_site_title)?></title>
<?
if($pjax != 2){
include('../ext/index_head.php');
?>
</head>
<body>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-KRB2SRW"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<?
} else{
?>
<noscript id="deferred-styles"><link rel="stylesheet" type="text/css" href="<?=$path_css_pc?>stst_2.min.css"/></noscript>
<script>
var loadDeferredStyles = function() {
  var addStylesNode = document.getElementById("deferred-styles");
  var replacement = document.createElement("div");
  replacement.innerHTML = addStylesNode.textContent;
  document.body.appendChild(replacement);
  addStylesNode.parentElement.removeChild(addStylesNode);
};
var raf = window.requestAnimationFrame || window.mozRequestAnimationFrame ||
window.webkitRequestAnimationFrame || window.msRequestAnimationFrame;
if (raf) raf(function() { window.setTimeout(loadDeferredStyles, 0); });
else window.addEventListener('load', loadDeferredStyles);
</script>
<? } ?>
<div id="body">
	<? include('../../inc/ext/inc_temp_top_menubar.php');?>
	<div id="container" class="container">
		<header>
			<?
			include('../../inc/ext/inc_temp_top.php');
			include("../../inc/ext/inc_temp_top_col.php");	
			?>
		</header>
     
		<div class="row">
			<div id="main" class="container_main type_sim_main">
				<section class="main_left" id="main_left">
					<div class="box_simdep">
						<h1>SIM SỐ ĐẸP</h1>
						<span>Đại lý phân phối sim số đẹp giá rẻ các mạng, đăng ký thông tin chính chủ giao sim miễn phí tận tay khách hàng toàn quốc.</span>
					</div>
					<?
					include('../../inc/ext/inc_type_sim_hot.php');
					include('../../inc/ext/inc_temp_h2.php');
					include('../../inc/ext/inc_temp_jsload.php');

					?>
				</section>
				<aside class="main_right"><? include('../../inc/ext/inc_temp_sidebar.php');?></aside>
			</div>
		</div>  
	</div>
	<footer><? include('../../inc/ext/inc_bot.php');?></footer>    
	<?/* include('../../inc/ext/inc_bot_menubar.php');*/?>
</div>
<? if($pjax != 2){?>
</body>
</html>
<?}?>