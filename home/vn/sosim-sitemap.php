<?
header("Content-type: text/xml");
include("../ext/lang.php");

$page      = getValue("page", "int", "GET", 1);
$page_size = 5000;
$strSosim  = "";

/* Khai bao bien */
$searchConfig =  array ("page"		=> $page,
								"page_size"	=> $page_size,
								"iCat"		=> $iCat,
								"iType"		=> $iType,
								"iPrice"		=> $iPrice,
								"iDauSo"		=> $iDauSo,
								"iDaiLy"		=> $iDaiLy,
								"digit"		=> $digit,
								"iNguHanh"	=> $iNguHanh,
								"module"		=> $module,
								"sort"		=> $sort,
								);

/* Thuc hien search */
$sphinx_search = new sphinx_search();
$arrResult     = $sphinx_search->search($searchConfig);

$arrQuery		= array();
$total_record	= 0;

if(!empty($arrResult["data"])){
	$arrQuery		= $arrResult["data"];
	$total_record	= $arrResult["total_record"];
}

if(!empty($arrQuery)){

	foreach($arrQuery as $key => $row){
		$row["cat_name_index"] = @$arrSimCat[$row['sim_category']]['cat_name_index'];
		$link_detail           = createLink("detail_sim",$row);

		$strSosim   .= '<url>
							    <loc>' . $con_server_name . $link_detail . '</loc>
							    <lastmod>' . date("c") . '</lastmod>
							    <changefreq>daily</changefreq>
							    <priority>1</priority> 
							</url>';
	}

}

if($strSosim != ""){
?>
<urlset xmlns="https://www.sitemaps.org/schemas/sitemap/0.9" xmlns:xsi="https://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="https://www.sitemaps.org/schemas/sitemap/0.9 https://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">
	<url>
      <loc><?=$con_server_name?>/</loc>
      <lastmod><?=date("c")?></lastmod>
      <changefreq>daily</changefreq>
      <priority>1.00</priority>
   </url>
	<?=$strSosim?>
</urlset>
<?
}
// var_dump(date("c"));
?>