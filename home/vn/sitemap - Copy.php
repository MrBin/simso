<?
header("Content-type: text/xml");
include("../ext/lang.php");

// Dau so
$db_sim_dauso = new db_query("SELECT sds_id,sds_category,sds_name,sds_name
										FROM tbl_simdauso
										WHERE sds_active = 1
										");
while($row = mysql_fetch_assoc($db_sim_dauso->result)){
	$index = intval($row['sds_name']);
	$dauso_sim_idx[$index] = array('sds_id'=>$row["sds_id"],
											'sds_category'=>$row["sds_category"]
											);
} // End while($ds = mysql_fetch_assoc($db_sim_dauso->result))
unset($db_sim_dauso);

ob_start('callback');
?>
<?='<?xml version="1.0" encoding="UTF-8"?><?xml-stylesheet type="text/xsl" href="/lib/css/sitemap.xsl"?><!-- generator="wordpress/3.0.1" -->' . chr(13)?>
<!-- sitemap-generator-url="http://www.arnebrachhold.de" sitemap-generator-version="3.2.4" -->

<urlset xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd" xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">	

<?
// Lay site map cua tin tuc
$db_query = new db_query("SELECT new_title_index 
									FROM tbl_news 
									WHERE new_active = 1 AND new_date > " . (time()-7*86400) . "
									ORDER BY new_date DESC, new_id DESC
									LIMIT 10");
while($row = mysql_fetch_assoc($db_query->result)){
	$link = createLink("detail_news",$row);
	?>
	<url>
		<loc>http://<?=$_SERVER['HTTP_HOST'] . $link?></loc>
		<lastmod><?=date("c")?></lastmod>
		<changefreq>daily</changefreq>
		<priority>1</priority>
	</url>
	<?
} // End while($row = mysql_fetch_assoc($db_query->result)){

$arrSimSale = array();
if(trim($con_sim_km) != ""){
	$e = explode( "\r\n", $con_sim_km );

	// Xử lý dữ liệu sim đưa vào
	$i = 0;
	for ( ; $i < count( $e ) - 1; $i++ ){
		
		list( $sosim, $price, $price_sale ) = split( "\t", $e[$i] );
		
		if(trim($sosim) != ""){
		
			$sim1x = preg_replace('/[^0-9. ]/','',$sosim);
			$sim2x = intval(preg_replace('/[^0-9]/','',$sosim));
	 		
			// Kiểm tra hợp lệ của số
			$arrSimSale[$sim2x] 	 = array("sim_sim1" 			=> $sim1x,
													"sim_sim2" 			=> $sim2x, 
													"sim_price" 		=> $price, 
													"sim_price_sale" 	=> $price_sale);
												
		}// End if(trim($sosim) != "")
		
	}// End for ( ; $i < count( $e ) - 1; $i++ )
}// End if(trim($con_sim_km) != "")

//echo '<xmp>'; print_r($con_sim_km); echo '</xmp>';
foreach($arrSimSale as $key => $row){
	// Xac dinh mang sim va dau so
	$sim_sim2		= $row['sim_sim2'];
	$sim_category	= 1;
	$sim_dausoid	= 1;
	$dau1 = substr($sim_sim2,0,1);
	$dau2 = substr($sim_sim2,0,2);
	$dau3 = substr($sim_sim2,0,3);
	if(array_key_exists($dau3, $dauso_sim_idx)){
		$sim_dausoid	= $dauso_sim_idx[$dau3]['sds_id'];
		$sim_category	= $dauso_sim_idx[$dau3]['sds_category'];
	}elseif(array_key_exists($dau2, $dauso_sim_idx)){
		$sim_dausoid	= $dauso_sim_idx[$dau2]['sds_id'];
		$sim_category	= $dauso_sim_idx[$dau2]['sds_category'];
	}elseif(array_key_exists($dau1, $dauso_sim_idx)){
		$sim_dausoid	= $dauso_sim_idx[$dau1]['sds_id'];
		$sim_category	= $dauso_sim_idx[$dau1]['sds_category'];
	}  // End if(array_key_exists($dau, $dauso_sim_idx))

	$row["cat_name_index"]	= @$arrSimCat[$sim_category]["cat_name_index"];
	$link_detail 			= createLink("detail_sim",$row);
?>
<url>
    <loc>http://<?=$_SERVER['HTTP_HOST'] . $link_detail?></loc>
    <lastmod><?=date("c")?></lastmod>
    <changefreq>daily</changefreq>
    <priority>1</priority> 
</url>
<?
}
?>
</urlset>

<?
if($str_cache_all == '') {
	$cache = ob_get_contents();
	$myMemcache->set($key_cache_all, $cache, $time_cache_1_month);
	unset($cache);	
}

ob_end_flush();
?>