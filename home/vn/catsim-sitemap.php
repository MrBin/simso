<?
header("Content-type: text/xml");
include("../ext/lang.php");

$strSimType  = "";
$strSimPrice = "";
$strMang     = "";
$strDauso    = "";

if(!empty($arrSimCat)){
	foreach ($arrSimCat as $key => $value) {
		$strMang .= '<url>
						      <loc>' . $con_server_name . '/' . $value["cat_name_index"] . '/</loc>
						      <lastmod>2019-12-10</lastmod>
						      <changefreq>daily</changefreq>
						      <priority>0.85</priority>
						   </url>';
	}
}

if(!empty($arrSimType)){
	foreach ($arrSimType as $key => $value) {
		$strSimType .= '<url>
						      <loc>' . $con_server_name . '/' . $value["simtp_index"] . '/</loc>
						      <lastmod>2019-12-10</lastmod>
						      <changefreq>daily</changefreq>
						      <priority>0.85</priority>
						   </url>';
	}
}

if(!empty($arrSimPrice)){
	foreach ($arrSimPrice as $key => $value) {
		$strSimPrice .= '<url>
						      <loc>' . $con_server_name . '/' . $value["pri_name_index"] . '/</loc>
						      <lastmod>2019-12-10</lastmod>
						      <changefreq>daily</changefreq>
						      <priority>0.85</priority>
						   </url>';
	}
}

if(!empty($arrSimDauSo)){
	foreach ($arrSimDauSo as $key => $value) {
		$strDauso .= '<url>
						      <loc>' . $con_server_name . '/' . $arrSimCat[$row['sds_category']]["cat_name_index"] . '-dau-' . $value["sds_name"] . '/</loc>
						      <lastmod>2019-12-10</lastmod>
						      <changefreq>daily</changefreq>
						      <priority>0.85</priority>
						   </url>';
	}
}
$strCateSitemap = $strMang . $strSimType . $strSimPrice . $strDauso;
?>
<urlset xmlns="https://www.sitemaps.org/schemas/sitemap/0.9" xmlns:xsi="https://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="https://www.sitemaps.org/schemas/sitemap/0.9 https://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">
	<url>
      <loc><?=$con_server_name?>/</loc>
      <lastmod>2019-12-10</lastmod>
      <changefreq>daily</changefreq>
      <priority>1.00</priority>
   </url>
	<?=$strCateSitemap?>
</urlset>
