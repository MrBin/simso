<? if($pjax != 2){?>
<!DOCTYPE html>
<html lang="vi">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="description" content="<?=strip_tags($con_meta_description)?>" />
<meta name="keywords" content="<?=strip_tags($con_meta_keywords)?>" />
<? }?>
<title><?=strip_tags($con_site_title)?></title>
<?
if($pjax != 2){
include('../ext/index_head_phone.php');
?>
</head>
<body>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-KRB2SRW"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<?
} else{
?>
<noscript id="deferred-styles"><link rel="stylesheet" type="text/css" href="<?=$path_css_mobile?>stst_1.min.css"/></noscript>
<script>
var loadDeferredStyles = function() {
  var addStylesNode = document.getElementById("deferred-styles");
  var replacement = document.createElement("div");
  replacement.innerHTML = addStylesNode.textContent;
  document.body.appendChild(replacement);
  addStylesNode.parentElement.removeChild(addStylesNode);
};
var raf = window.requestAnimationFrame || window.mozRequestAnimationFrame ||
window.webkitRequestAnimationFrame || window.msRequestAnimationFrame;
if (raf) raf(function() { window.setTimeout(loadDeferredStyles, 0); });
else window.addEventListener('load', loadDeferredStyles);
</script>
<? } ?>
<div id="home_content"> 
	<div id="menu"><? include('../../inc/ext_phone/inc_left.php');?></div>
	<div id="right_menu"><? include('../../inc/ext_phone/inc_right.php'); ?></div>
	<div id="main_container">    	
		<?
		include('../../inc/ext_phone/inc_temp_top.php');
		echo '<div id="main">';
			include('../../inc/inc_type_phone.php');
			include('../../inc/ext/inc_temp_h2.php');
			echo '<div class="main_right">';
			if($module == "phongthuy" || $module == "sim-phong-thuy" || $module == "xem-sim-hop-tuoi") include('../../inc/ext_phone/inc_sidebar_phongthuy.php');
			else include('../../inc/ext_phone/inc_temp_sidebar.php');
			echo '</div>';
		echo '</div>';
		include('../../inc/ext_phone/inc_temp_bot.php');
		?>
	</div>
</div>

<? if($pjax != 2){?>
</body>
</html>
<? }?>
        