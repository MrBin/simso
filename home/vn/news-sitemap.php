<?
header("Content-type: text/xml");
include("../ext/lang.php");

$strNewsSitemap = "";
// Lay site map cua tin tuc
$db_query = new db_query("SELECT new_title_index 
									FROM tbl_news 
									WHERE new_active = 1 AND new_date > " . (time()-7*86400) . "
									ORDER BY new_date DESC, new_id DESC");
while($row = mysql_fetch_assoc($db_query->result)){
	$link = createLink("detail_news",$row);

	$strNewsSitemap   .= '<url>
									<loc>' . $con_server_name . $link . '</loc>
									<lastmod>' . date("c") . '</lastmod>
									<changefreq>daily</changefreq>
									<priority>1</priority>
								</url>';
} // End while($row = mysql_fetch_assoc($db_query->result)){
unset($db_query);

if($strNewsSitemap != ""){
?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">
	<url>
      <loc><?=$con_server_name?>/</loc>
      <lastmod><?=date("c")?></lastmod>
      <changefreq>daily</changefreq>
      <priority>1.00</priority>
   </url>
	<?=$strNewsSitemap?>
</urlset>
<?
}
?>