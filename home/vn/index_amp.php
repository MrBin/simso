<!doctype html>
<html amp lang="vi">
<head>
<meta charset="utf-8">   
<title>AMP - <?=strip_tags($con_site_title)?></title>
<meta name="description" content="<?=strip_tags($con_meta_description)?>" />
<meta name="keywords" content="<?=strip_tags($con_meta_keywords)?>" />
<? include('../ext/index_head_amp.php');?>
</head>

<body>
<?
include('../../inc/ext_amp/inc_temp_sidebar.php');
include('../../inc/ext_amp/inc_temp_header.php');
include('../../inc/ext_amp/inc_temp_main.php');
include('../../inc/ext_amp/inc_temp_footer.php');
?>  
</body>
</html>
