<?
$time_end_load_page 	= microtime_float();
$time_load_page 		= $time_end_load_page - $time_start_load_page;
$time_load_page      = format_number($time_load_page, 8);
$memory_get_usage    = format_number(memory_get_usage() / 1024, 0);

//
// Cấu hình thời gian cache
$time_cache_5_minute		= 60*5;
$time_cache_1_month 		= 86400*30;

//
// Tạo key cache cho toàn bộ web
$cache_ie = $versionIE != 0 && $versionIE < 8 ? 1 : 0;
$key_cache_all = $deviceInclude.'_'.$cache_ie.'_'.$pjax.'_'.$append_data.'_'.$_SERVER['QUERY_STRING'];
$key_cache_all = md5($key_cache_all);

//
// Kiểm tra xem có cache từ memcache hay không
$str_cache_all = $myMemcache->get($key_cache_all);

//
// Nếu như có cache
if($str_cache_all != ''){

	if(substr_count($_SERVER['HTTP_ACCEPT_ENCODING'],'gzip')){ 
		ob_start('ob_gzhandler');
		ob_start('callback');
	}
	else ob_start('callback');

	echo $str_cache_all;
	echo '<!-- '.$key_cache_all.' - '.$time_load_page.' - '.$memory_get_usage.' -->';

	ob_end_flush();
	exit();
}
?>