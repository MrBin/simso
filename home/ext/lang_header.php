<?
if($con_set_cacheHeader == 1){
	$expires = 600;	
	$now = gmdate('D, d M Y H:i:s').' GMT';
	header('Expires: ' . gmdate('D, d M Y H:i:s', time()+$expires) . ' GMT');
	header('Last-Modified: '.$now);
	header("Cache-Control: maxage=".$expires);
	header("Pragma: public");
} // End if($con_set_cacheHeader == 1)

if($module == 'feed' || $module == 'sitemap'){
	header('Content-Type: text/xml; charset=UTF-8');	
} // End if($module == 'feed'){
?>