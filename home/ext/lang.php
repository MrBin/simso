<?
#***
# * STST - PhongNT
#**
#+
#+ Kiểm tra phiên bản mobile
include('../../kernel/classes/Mobile_Detect.php');
include('../../kernel/functions/functions_all.php');

$detect        = new Mobile_Detect();
// $deviceType    = ($detect->isMobile() ? ($detect->isTablet() ? 'tablet' : '_phone') : '_desktop');
$deviceInclude = $detect->isMobile() ? '_phone' : '_desktop';
$con_admin_id		= checkCookieAdminLogin();
#+
#+ Kiểm tra phiên bản IE
$versionIE = 0;
foreach($detect->getProperties() as $name => $match){
	$check = $detect->version($name);
	if($check !== false && $name == 'IE'){
		$versionIE = intval($check);
	}
}
#+
include('../../config.php');
include('../../kernel/classes/fs_database.php');
include('../../kernel/classes/cache.php');
include('../../kernel/classes/memcached_store.php');
include('../../kernel/classes/rating.php');


$time_start_load_page	=	microtime_float();
$myMemcache      = memcached_store::getInstance();

#+
#+ Khai bao bien
$type          = getValue("type","str","GET","");
$module        = getValue("module","str","GET","");

$current_page  = getValue("page","INT","GET",1);
$current_page1 = getValue("page","INT","GET",0);
$cache_page   	= getValue("page","INT","GET",0);

$sCat 			= getValue("sCat",'str','GET','');
$sData			= getValue("sData",'str','GET','');
$sType 			= getValue("sType",'str','GET','');
$sDauSo			= getValue("sDauSo",'str','GET','');

$sNguHanh		= getValue("sNguHanh",'str','GET','');

$iCat 			= getValue('iCat','str','GET',0);
$iData 			= intval(getValue('iData','str','GET',0));
$iType 			= getValue('iType','str','GET',0);
$iPrice        = getValue('iPrice','int','GET',0);
$iDauSo 		   = getValue('iDauSo','int','GET',0);
$iVendor 		= getValue('iVendor','str','GET',0);
$iDaiLy 			= getValue("iDaiLy","int","GET",0);

$iNamSinh		= getValue('iNamSinh','int','GET',0);	
$iNguHanh		= getValue('iNguHanh','int','GET',0);
$iNguHanhSinh	= getValue('iNguHanhSinh','int','GET',0);
$iNguHanhKhac	= getValue('iNguHanhKhac','int','GET',0);

$iBirthday   = getValue('iBirthday','str','GET',"");
$iBirthMonth = getValue('iBirthMonth','str','GET',"");
$iBirthYear  = getValue('iBirthYear','str','GET',"");

$iPriceFrom 	= getValue("iPriceFrom","str","GET");
$iPriceTo 		= getValue("iPriceTo","str","GET");

$keyword 		= getValue('keyword','str','GET','');
$keyword 		= @mysql_escape_string($keyword);
$keyword_r 		= str_replace('-',' ',$keyword);

$sort 			= getValue("sort","int","GET",0);
$digit 			= getValue("digit","int","GET",0);

$pjax 			= getValue('_pjax','int','POSTGET','');
$append_data 	= getValue('append','int','POST');

$con_server_name    = "https://sieuthisimthe.com";
$arrBreadcrumb      = array();
$arrProductJsonData = array();

$arrPageRating = array(0 => "Đặt sim", 1 => "Thu mua sim");

#+
#+ Neu la search thi ko cache
#+
/*/
if($module == 'search'|| $module == 'tag' || $module == 'searchsim')
	$con_set_cache = 0;	
#+
if($module == 'simajax'){
	$con_set_cache 	= 0;	
	$con_set_cache1 = 0;
} // End if($module == 'simajax')


#+
#+ Cau hinh co ban
include('lang_header.php');

#+
#+ Thiet lap tuong lua
if($con_set_fireWall == 1) 
	include('../../kernel/functions/functions_firewall.php');
//*/


#+
#+ Cau hinh cache
if($con_set_cache1 == 1)
	include('lang_cache_all.php');

#+
#+ Cau hinh kernel cho website
include('../../kernel/classes/fs_generate_form.php');
include('../../kernel/classes/fs_menu.php');
include('../../kernel/classes/cls_template'.$deviceInclude.'.php');
// include('../../kernel/classes/sphinx_api.php');
// include('../../kernel/classes/sphinx_search.php');
include('../../kernel/classes/sphinx/sphinx_database.php');
include('../../kernel/classes/sphinx/sphinx_search.php');
include('../../kernel/functions/functions_pagebreak.php');
include('../../kernel/functions/functions_translate.php');
include('../../kernel/functions/functions_file.php');
include('../../kernel/functions/functions_date.php');
include('../../kernel/functions_ext/functions_phongthuy.php');
include('../../kernel/ext/config.php');

#+
#+ Khai bao bien
$change_bg			= 'onMouseOver="this.style.background=\'#EFEFEF\'" onMouseOut="this.style.background=\'#FFF\'"';
#+
$sqlcategory		= '';	
$sqlcategoryA		= '';								  
$listiCat			= 0;
$sqlmenusub			= '';
$cat_cha			= 0;
$cat_cha2			= 0;
$cat_child			= 0;

#+
$menuid 				= new menu();
$menuid->getArray("tbl_category","cat_id","cat_parent_id"," lang_id = ".$lang_id);

#+
#+ Hien thi cac thanh phan khac cua sim tu array
$arrSimCat	= _temp::_checkSimCat();
#+
$arrSimType = _temp::_checkSimType();
foreach($arrSimType as $key => $row){

	$arrSimTypeIndex[$row['simtp_index']]	= $row['simtp_id'];	
} // End foreach($arrSimType as $key => $row)

$arrMangSearch = array();
$arrMangData   = array();

#+
$arrSimPrice = _temp::_checkSimPrice();
#+
$arrSimDauSo = _temp::_checkSimDauSo();
foreach($arrSimDauSo as $key => $row){
	$arrSimDauSoIndex[$row['sds_name']] = $row['sds_id'];
	$arrMangData[$row['sds_name']]      =	$row['sds_category'];
	$arrMangSearch[$row['sds_name']]    = $arrSimCat[$row['sds_category']]["cat_name_index"];

} // ENd foreach($arrSimDauSo as $key => $row)

$jsonNetword = json_encode($arrMangSearch);

#+
#+ Xac dinh dau so
$str_iType         = '';
$strSimTypeKeyword = "";
$strKeyEnd         = "";
if($module == 'searchsim'){

	$keywordCheck   = preg_replace("/[^0-9*xX]/si", "", $keyword);
	
	$sim_check      = str_replace(array("x", "X", "**", "*"), array("x", "x", "*", "*"), $keywordCheck);
	
	$countXCheck    = substr_count($sim_check, "x");
	$countStarCheck = substr_count($sim_check, "*");
	
	if($countStarCheck > 0){
		$strKeyEnd      = substr($keywordCheck, strrpos($keywordCheck, '*' )+1);
	}
	else $strKeyEnd = $keywordCheck;

	if($strKeyEnd != false){

		if(strlen($strKeyEnd) < 8){
			$countZero     = 8 - strlen($strKeyEnd);
			
			$strSubKeyword = "abcdef";
			$strSubKeyword = substr($strSubKeyword, -$countZero, $countZero);
			$strKeyEnd     = $strSubKeyword . $strKeyEnd;

		}

		$iTypeCheck     = (int)checkSimTypeSreach($strKeyEnd,$arrSimTypeIndex);

	}


	if($iTypeCheck < 19 && $countXCheck == 0) $iType = $iTypeCheck;

	if($iType > 0){
		foreach ($arrSimType as $key => $value) {
			if($key == $iType) $strSimTypeKeyword = $value["simtp_name"];
		}
	}

	// $sphinx_search = new sphinx_search($keyword);

	// var_dump($sphinx_search);
	// exit();
}

// if($module == 'searchsim'){
	
// 	/*/
// 	$sim_check = preg_replace("/[^0-9*]/si","",$keyword);
// 	$sim_check = substr($sim_check, ( strrpos($sim_check, '*') ) );
// 	$sim_check = str_replace('*','',$sim_check);
	
// 	$arr_iType = checkSimTypeArr($sim_check,$arrSimTypeIndex);	
// 	$str_iType = implode(',', $arr_iType);
// 	//*/

// 	#+
// 	$keyword		= preg_replace("/[^0-9*xX]/si","",$keyword);
// 	$keywords		= preg_replace("/[^0-9*xX]/si","",$keyword);
// 	#+
// 	if ($keywords!="")
// 	{
// 		$spot = strpos($keywords,"*");
// 		$slen = strlen($keywords);
// 	} // End if ($keywords!="")
// 	$keywords	 	= str_replace(array('x','X','*'),array('[0-9]','[0-9]','.*'),$keywords);

// 	#+
// 	if( $spot != 0 ){
// 		$dau = '0'.intval($keyword);
// 		var_dump($dau);
// 		exit();
// 		if( strlen($dau) >= 2 )
// 			$dau1 = substr($dau,0,2);
// 		if( strlen($dau) >= 3 )
// 			$dau2 = substr($dau,0,3);
// 		if( strlen($dau) >= 4 )
// 			$dau3 = substr($dau,0,4);
			
// 		#+
// 		if(isset($dau3) && array_key_exists($dau3, $arrSimDauSoIndex)){
// 			$iDauSo	= $arrSimDauSoIndex[$dau3];
// 		}elseif(isset($dau2) && array_key_exists($dau2, $arrSimDauSoIndex)){
// 			$iDauSo	= $arrSimDauSoIndex[$dau2];
// 		}elseif(array_key_exists($dau1, $arrSimDauSoIndex)){
// 			$iDauSo	= $arrSimDauSoIndex[$dau1];
// 		}  // End if(array_key_exists($dau, $dauso_sim_idx))
// 	} // End if( strpos('*1990',"*") != 0 )
// } // End if($module == 'searchsim' && $keyword != '')


#+
#+ Dua ra nhung sim hop tuoi
if($iNamSinh != 0){
	#+
	$arrThongTin = getCanChi('01','06',$iNamSinh);

	#+
	#+ Tinh ban menh
	$ban_menh_cot = $arrThongTin['can_nam_id']%2 == 0 ? $arrThongTin['can_nam_id']/2 : ($arrThongTin['can_nam_id']-1)/2;
	#+
	#+ $x la tim nam sinh dau tien trong hoa giap ung voi giap ty
	#+ $y de quay ve vong lap dau tien trong luc thap hoa giap
	#+ $z de xem chi dang o hang may trong bang luc thap hoa giap
	$nam_sinh = $arrThongTin['nam_am_lich'];
	$x = abs($nam_sinh-4);
	$y = $x - 60*floor($x/60);
	$z = floor( $y / 10 );
	
	if($nam_sinh - 4 < 0)
		$ban_menh_hang =  5 - $z;
	else
		$ban_menh_hang =  $z;
		
	$ban_menh = $ban_menh_cot.$ban_menh_hang;
	$ban_menh = intval($ban_menh);

	#+
	$iNguHanh = $arrBanMenh[$ban_menh]['menh_id'];		
	$iNguHanhSinh = $iNguHanh == 1 ? 5 : $iNguHanh-1;
	$iNguHanhKhac = $iNguHanh >= 3 ? $iNguHanh-2 : $iNguHanh+3;	
	
} // End if($iNamSinh != 0)


#+
#+ Kiem tra ngu hanh theo 
if( isset($arrNguHanh1[$sNguHanh]) )
	$iNguHanh = $arrNguHanh1[$sNguHanh];	

if( $iNguHanh != 0 ){
	$iNguHanhSinh = $iNguHanh == 1 ? 5 : $iNguHanh-1;
	$iNguHanhKhac = $iNguHanh >= 3 ? $iNguHanh-2 : $iNguHanh+3;	
} // End if( isset($arrNguHanh1[$sNguHanh]) )


#+
#+ Cau hinh title + descripton + keyword cho web
include('lang_config.php');
include('lang_vn.php');

#+
#+ Check pjax
if($pjax == 1){
	include('../vn/pjax.php');
	exit();
}

/*/
echo $type.' | '.$module.' | '.$iCat.' | '.$iData.'<br>';
echo '<xmp>';
print_r($_GET);
echo '</xmp>';
//*/
?>