<?
/* Template */
include("../../lib/css/temp_abc.css");
include("../../lib/css/temp_grid.css");
include("../../lib/css/temp_glyphicons.css");	
include("../../lib/css/temp_tables.css");
include("../../lib/css/temp_forms.css");
include("../../lib/css/temp_buttons.css");
include("../../lib/css/temp_breadcrumbs.css");    
include("../../lib/css/temp_pagination.css");
include("../../lib/css/temp_alerts.css");
include("../../lib/css/temp_panels.css");	
include("../../lib/css/temp_tooltip.css");
include("../../lib/css/temp_popovers.css");
include("../../lib/css/temp_theme.css");

include("../../lib/css/temp_navs.css");
include("../../lib/css/temp_navbar.css");
include("../../lib/css/temp_dropdowns.css");

/* Top + Bot + Sidebar */
include("../../lib/css/bot.css");

/* Index */

/* Type */
include("../../lib/css/type_sim.css");
include("../../lib/css/type_news.css");
include("../../lib/css/type_phongthuy.css");

/* Detail */
include("../../lib/css/detail_news.css");
?>