<?
$og_image_str = '/lib/img/logo-200x200.png';
if(isset($og_image) && $og_image != ""){
  $og_image_str = $og_image;
}
else{
  $og_image_str = isset($srcPic) && $srcPic != '' ? $srcPic : $og_image_str;
}
?>
<meta name="robots" content="noodp, noydir" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, shrink-to-fit=no, maximum-scale=3.0">
<link rel="author" href="https://plus.google.com/106526090681705102835/" />
<link rel="shortcut icon" href="<?=$con_server_name?>/lib/img/favicon.ico" />
<link rel="alternate" type="application/rss+xml" href="<?=$con_server_name?>/feed/" title="Feed" />
<link rel="canonical" href="<?=$con_server_name . removeQueryString($_SERVER['REQUEST_URI'])?>" />

<meta property="og:locale" content="vi_VN"/>
<meta property="og:type" content="<?=$iData != 0 ? 'article' : 'website'?>"/>
<meta property="og:url" content="<?=$con_server_name?><?=removeQueryString($_SERVER['REQUEST_URI'])?>"/>
<meta property="og:title" content="<?=strip_tags($con_site_title)?>"/>
<meta property="og:description" content="<?=strip_tags($con_meta_description)?>"/>
<meta property="og:site_name" content="Siêu thị sim thẻ - Sim số đẹp - Sim phong thủy"/>
<meta property="og:image" itemprop="image" content="<?=$con_server_name . $og_image_str?>" />

<meta name="yandex-verification" content="ee30052d209a6a00" />
<meta name="msvalidate.01" content="C5F8632850C88D02973BDD80D5214C28" />
<meta name="google-site-verification" content="67ksKB3o5L93kIFQ-CAiqgqvHHOOpX1wlPhiVkisCMw" />

<meta property="fb:admins" content="100000160705263"/>
<meta property="fb:app_id" content="182834278554846"/>

<link rel="dns-prefetch" href="https://www.google-analytics.com">
<link href='https://www.google.com.vn' rel='preconnect' crossorigin>
<link href='https://www.google.com' rel='preconnect' crossorigin>
<link href='https://staticxx.facebook.com' rel='preconnect' crossorigin>
<link href='https://www.googleadservices.com' rel='preconnect' crossorigin>
<link href='https://googleads.g.doubleclick.net' rel='preconnect' crossorigin>

<link rel="preload" href="<?=$path_fonts?>glyphicons-halflings-regular.woff" as="font" type="font/woff" crossorigin="anonymous">

<?
$cssInlineMb        = trim(file_get_contents("../.." . $path_css_mobile . 'css_inline_mb.min.css'));
$cssInlineMb        = str_replace('../', $path_lib, $cssInlineMb);
?>
<style type="text/css"><?=$cssInlineMb?></style>
<noscript id="deferred-styles"><link rel="stylesheet" type="text/css" href="<?=$path_css_mobile?>stst_1.min.css"/></noscript>
<script>
var loadDeferredStyles = function() {
  var addStylesNode = document.getElementById("deferred-styles");
  var replacement = document.createElement("div");
  replacement.innerHTML = addStylesNode.textContent;
  document.body.appendChild(replacement);
  addStylesNode.parentElement.removeChild(addStylesNode);
};
var raf = window.requestAnimationFrame || window.mozRequestAnimationFrame ||
window.webkitRequestAnimationFrame || window.msRequestAnimationFrame;
if (raf) raf(function() { window.setTimeout(loadDeferredStyles, 0); });
else window.addEventListener('load', loadDeferredStyles);
</script>

<style>
.breadcrumb li{
	display: inline-block;
}
</style>
<script async type="text/javascript">
var jQl={q:[],dq:[],gs:[],ready:function(a){"function"==typeof a&&jQl.q.push(a);return jQl},getScript:function(a,c){jQl.gs.push([a,c])},unq:function(){for(var a=0;a<jQl.q.length;a++)jQl.q[a]();jQl.q=[]},ungs:function(){for(var a=0;a<jQl.gs.length;a++)jQuery.getScript(jQl.gs[a][0],jQl.gs[a][1]);jQl.gs=[]},bId:null,boot:function(a){"undefined"==typeof window.jQuery.fn?jQl.bId||(jQl.bId=setInterval(function(){jQl.boot(a)},25)):(jQl.bId&&clearInterval(jQl.bId),jQl.bId=0,jQl.unqjQdep(),jQl.ungs(),jQuery(jQl.unq()), "function"==typeof a&&a())},booted:function(){return 0===jQl.bId},loadjQ:function(a,c){setTimeout(function(){var b=document.createElement("script");b.src=a;document.getElementsByTagName("head")[0].appendChild(b)},1);jQl.boot(c)},loadjQdep:function(a){jQl.loadxhr(a,jQl.qdep)},qdep:function(a){a&&("undefined"!==typeof window.jQuery.fn&&!jQl.dq.length?jQl.rs(a):jQl.dq.push(a))},unqjQdep:function(){if("undefined"==typeof window.jQuery.fn)setTimeout(jQl.unqjQdep,50);else{for(var a=0;a<jQl.dq.length;a++)jQl.rs(jQl.dq[a]); jQl.dq=[]}},rs:function(a){var c=document.createElement("script");document.getElementsByTagName("head")[0].appendChild(c);c.text=a},loadxhr:function(a,c){var b;b=jQl.getxo();b.onreadystatechange=function(){4!=b.readyState||200!=b.status||c(b.responseText,a)};try{b.open("GET",a,!0),b.send("")}catch(d){}},getxo:function(){var a=!1;try{a=new XMLHttpRequest}catch(c){for(var b=["MSXML2.XMLHTTP.5.0","MSXML2.XMLHTTP.4.0","MSXML2.XMLHTTP.3.0","MSXML2.XMLHTTP","Microsoft.XMLHTTP"],d=0;d<b.length;++d){try{a= new ActiveXObject(b[d])}catch(e){continue}break}}finally{return a}}};if("undefined"==typeof window.jQuery){var $=jQl.ready,jQuery=$;$.getScript=jQl.getScript};
jQl.loadjQ('<?=$path_js_mobile?>stst_1.js');
</script>
<script type="application/ld+json">
{
  "@context": "https://schema.org",
  "@type": "LocalBusiness",
  "@id": "https://sieuthisimthe.com/",
  "url": "https://sieuthisimthe.com/",
  "logo": {
    "@type": "ImageObject",
    "url": "https://sieuthisimthe.com/lib/logos/sieuthisimthe-logo-1024x159.png",
    "width": {
      "@type": "QuantitativeValue",
        "value": 1024
    },
    "height": {
      "@type": "QuantitativeValue",
        "value": 159
    }
  },
  "image": "https://sieuthisimthe.com/lib/logos/sieuthisimthe-logo-1024x159.png",
  "priceRange": "30000VND - 1000000000VND",
  "slogan" : "Sim đẹp giá tốt",
  "name": "Siêu Thị Sim Thẻ",
  "description": "Đại lý phân phối sim số đẹp giá rẻ đăng ký thông tin chính chủ, nhận sim tại nhà trả tiền tại chỗ, miễn phí giao sim số đẹp toàn quốc",
  "email": "mailto:sieuthisimthe@gmail.com",
  "hasMap": "https://www.google.com/maps/place//data=!4m2!3m1!1s0x3135ac68acc50359:0xfd2031fba4f9395c?source=g.page.share",
  "founder": "Đoàn Văn Tuấn",
  "address": {
    "@type": "PostalAddress",
    "streetAddress": "575 Giải Phóng, Giáp Bát, Hoàng Mai, Hà Nội 100000",
    "addressLocality": "Hoàng Mai",
    "addressRegion": "Hà Nội",
    "postalCode": "100000",
    "addressCountry": "VIỆT NAM"
  },
  "geo": {
    "@type": "GeoCoordinates",
    "latitude": 20.990892,
    "longitude": 105.8390793
  },
  "telephone": "+84 0909 575 575",
  "openingHoursSpecification": [
    {
      "@type": "OpeningHoursSpecification",
      "dayOfWeek": [
        "Monday",
        "Tuesday",
        "Wednesday",
        "Thursday",
        "Friday",
          "Saturday",
          "Sunday"
     ],
     "opens": "08:00",
     "closes": "18:00"
    }
  ],
  "sameAs" : [
    "https://vnexpress.net/kinh-doanh/cach-chon-sim-so-dep-gia-khuyen-mai-khi-mua-online-4026904.html",
    "https://vietnamnet.vn/vn/cong-nghe/vien-thong/thu-thuat-mua-sim-online-so-dep-re-an-toan-598833.html",
    "https://www.facebook.com/sieuthisimthecom",
    "https://www.instagram.com/sieuthisimthe",
    "https://www.youtube.com/sieuthisimthe",
    "https://twitter.com/sieuthi_simthe",
    "https://www.pinterest.com/sieuthisimthe/",
    "https://sieuthisimthe.tumblr.com/",
    "https://soundcloud.com/sieuthi_simthe",
    "https://myspace.com/sieuthisimthe",
    "https://www.linkedin.com/in/sieu-thi-sim-the/"
  ]
}
</script>
<script type="application/ld+json">
{
  "@context": "https://schema.org",
  "@type": "Person",
  "name": "Đoàn Văn Tuấn",
  "jobTitle": "Ceo",
  "worksFor": "Siêu Thị Sim Thẻ",
  "url" : "https://www.facebook.com/sim.73truongchinh",
  "image" : "https://sieuthisimthe.com/lib/img/doan-van-tuan.jpg",
  "sameAs": [
    "https://www.facebook.com/sim.73truongchinh"
  ],
  "AlumniOf" : [
    "Trường Trung học phổ thông chuyên Hà Nội - Amsterdam",
    "Đại học Kinh tế Quốc dân"
  ],
  "address" : {
    "@type" : "PostalAddress",
    "addressLocality": "Hoàng Mai",
    "addressRegion": "Hà Nội",
    "postalCode": "100000",
    "addressCountry": "VIỆT NAM"
  }
}
</script>
<script type="application/ld+json">
{
	"@context": "https://schema.org",
	"@type": "WebSite",
	"url": "<?=$con_server_name?>/",
	"potentialAction": {
		"@type": "SearchAction",
		"target": "<?=$con_server_name?>/tim-sim/{search_term_string}.html",
		"query-input": "required name=search_term_string"
	}
}
</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-64579392-1', 'auto');
  ga('send', 'pageview');

</script>
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-KRB2SRW');</script>
<!-- End Google Tag Manager -->