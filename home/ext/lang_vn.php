<?
#+ -------------------- TYPE + MODULE -------------------- +#
#+
#+ Dua ra toan bo type
$key_cache = md5('key_tbl_category_type');

$data_memcache = $myMemcache->get($key_cache);

if ($data_memcache == '') {
   $query = ' SELECT typ_name,typ_name_index,typ_type,meta_title,meta_description,meta_keyword,meta_title_cat,meta_description_cat,meta_keyword_cat,meta_title_detail,meta_description_detail,meta_keyword_detail'
   		.' FROM tbl_category_type'
   		//.' WHERE lang_id = '.$lang_id.' AND typ_active = 1'
   		.' ORDER BY typ_order, typ_id, typ_name'
   		;
   $arrCatType = getArray($query, 'typ_name_index');

   $myMemcache->set($key_cache, $arrCatType, 86400*30);
}else{
   $arrCatType = $data_memcache;
}

#+
#+ Xac dinh type
$type = $type == '' ? $arrCatType[$module]['typ_type'] : $type;
		
#+ -------------------- CATEGORY -------------------- +#
#+ 
#+ Phan nay co the dung de chuyen sCat -> iCat neu link khong index = [0-9] ma = [A-z]
#+ Xac dinh type, module, iCat cua category
if($iCat != 0 || $sCat != ''){
	$sql = '';
	$sql .=  $iCat != 0 ? ' AND cat_id = '.$iCat : ($sCat != '' ? ' AND cat_name_index = "'.$sCat.'"' : '');
		
	$query = ' SELECT cat_id,cat_type'
			.' FROM tbl_category'
			.' WHERE 1'.$sql
			;
	$db_query = new db_query($query);
	# echo $query.'<br>';
	# echo 'Kết quả : '.mysql_num_rows($db_query->result).'<br>';
	#+
	if($row = mysql_fetch_assoc($db_query->result)){
		
		#+
		$module			= $module == '' ? $row["cat_type"] : $module;
		$type 			= $type == '' ? $arrCatType[$module]['typ_type'] : $type;
		$iCat			= $iCat == 0 ? $row['cat_id'] : $iCat;

		#+
		$cat_cha		=  $menuid->getCatcha($iCat); 
		$cat_child		= 	count(explode(",",$listiCat)); // tinh so cap con
		$sqlmenusub		= " AND cat_id IN(" .  $menuid->getAllChildId($cat_cha)  . ")";
		#+
		$listiCat		=  $menuid->getAllChildId($iCat);
		$sqlcategory	=  " AND cat_id IN(" . $listiCat  . ")";
		#+
		$listiCat1		=  $menuid->getAllChildId($cat_cha);
		$sqlcategoryA	=  " AND cat_id IN(" . $listiCat1  . ")";

		#+
		#+ Kiem tra xem ban ghi category nay co phai type "news" hay khong
		if($type == 'news'){
			$tb_select	= $arrType[$type]['tb_name'];
			$tb_prefix 	= $arrType[$type]['tb_prefix'];

			$query = ' SELECT COUNT(*) AS count'
					.' FROM '.$tb_select
					.' WHERE 1 AND '.$tb_prefix.'category = '.$iCat
					;
			$db_count = new db_query($query);
			$row = mysql_fetch_assoc($db_count->result);
			$toltal_record_news = $row['count'];
			unset($db_count);
			
			#+
			#+ Neu thay so luong ban ghi = 1
			if($toltal_record_news == 1){
				$iData = $iData == 0 ? $row['new_id'] : $iData;
			} // End if($row['count'] == 1)
			
		} // End if($arrCatType[$module]['typ_type'] == 'news')

	} // End if($row = mysql_fetch_assoc($db_category->result)){
		
	#+
	#+ Huy bien
	$db_query->close();
	unset($db_query);		
} // End if($iCat != 0)

#+ -------------------- DETAIL -------------------- +#
#+ 
#+ Phan nay co the dung de chuyen sData -> iData neu link khong index = [0-9] ma = [A-z]
#+ Xac dinh module, iCat, iData cua bai viet
if($sData != ''){

	$tb_select	= $arrType[$type]['tb_name'];
	$tb_prefix 	= $arrType[$type]['tb_prefix'];
	$field_name = $arrType['news']['field_name'];

	#+
	$sqlSelect 	= '';
	$sqlWhere	= '';
	
	#+
	$sqlSelect 	.= $tb_prefix.'id AS dat_id,'.$tb_prefix.'category AS dat_category';

	if($tb_select == "tbl_news"){
		$sqlSelect .= ",new_picture,new_title_index";
	}

	$sqlWhere	.= ($sData != '' ? ' AND '.$tb_prefix.$field_name.'_index = "'. $sData.'"' : '');

	$query = ' SELECT '.$sqlSelect
			.' FROM '.$tb_select
			.' WHERE 1'.$sqlWhere
			;
	$db_query 	= new db_query($query);
	// echo $query.'<br>';
	# echo 'Kết quả : '.mysql_num_rows($db_query->result).'<br>';
	#+
	if($row = mysql_fetch_assoc($db_query->result)){
		#+
		#+ Xac dinh thanh phan cua ban ghi
		$iCat			= $iCat == 0 ? $row['dat_category'] : $iCat;
		$iData		= $iData == 0 ? $row['dat_id'] : $iData;

		if(isset($row["new_picture"]) && $row["new_picture"] != ""){
			$og_image	= $row["new_picture"] != '' ? $path_news . $row["new_title_index"] . '/' . $row["new_picture"] : $path_news . 'no_image.jpg';
		}
		
		#+
		// if($type == 'sim'){
		// 	$iType = $iType == 0 ? $row['sim_typeid'] : $iType;
		// 	$iVendor = $iVendor == 0 ? $row['sim_dailyid'] : $iVendor;
		// 	$iPrice = $iPrice == 0 ? $row['sim_priceid'] : $iPrice;
		// 	$iDauSo = $iDauSo == 0 ? $row['sim_dausoid'] : $iDauSo;
		// } // End if($type == 'sim')
		
		#+
		#+ Huy bien
		$db_query->close();
		unset($db_query);
	} // End if($query!=''){	
} // End if($iCat != 0)


#+ -------------------- DETAIL RANDOM -------------------- +#
#+
#+ Neu ko xac dinh duoc dau so - Sim random
if($type == 'sim' && $iDauSo == 0 && $iType == 0 && $iData != 0){

	#+
	#+ Xac dinh dau so
	$dau1 = '0'.substr($iData,0,1);
	$dau2 = '0'.substr($iData,0,2);
	$dau3 = '0'.substr($iData,0,3);
	#+
	if(array_key_exists($dau3, $arrSimDauSoIndex))
		$iDauSo	= $arrSimDauSoIndex[$dau3];
	elseif(array_key_exists($dau2, $arrSimDauSoIndex))
		$iDauSo	= $arrSimDauSoIndex[$dau2];
	elseif(array_key_exists($dau1, $arrSimDauSoIndex))
		$iDauSo	= $arrSimDauSoIndex[$dau1];
	
	#+
	#+ Xac dinh loai sim
	$iType = checkSimType($iData,$arrSimTypeIndex);	

} // End if($sDauSo == '')


#+
#+ -------------------- THIET LAP 3 THE CHO WEBSITE VA 1 SO CHUC NANG -------------------- +#
#+
$meta_title = '';
$meta_description = '';
$meta_keyword = '';
#+
#+ Xac dinh title dang MODULE
$meta_title_module = $arrCatType[$module]['meta_title'];
$meta_description_module = $arrCatType[$module]['meta_description'];
$meta_keyword_module = $arrCatType[$module]['meta_keyword'];
#+
#+ Xac dinh title dang CAT
$meta_title_cat = $arrCatType[$module]['meta_title_cat'];
$meta_description_cat = $arrCatType[$module]['meta_description_cat'];
$meta_keyword_cat = $arrCatType[$module]['meta_keyword_cat'];
#+
#+  Xac dinh title dang DETAIL
$meta_title_detail = $arrCatType[$module]['meta_title_detail'];
$meta_description_detail = $arrCatType[$module]['meta_description_detail'];
$meta_keyword_detail = $arrCatType[$module]['meta_keyword_detail'];

#+
#+ Tao thanh address
$home_address = '
	<li>
		<a href="/">
			<span>Trang chủ</span>
		</a>
	</li>
	';
#+
if($type != 'sim' && array_key_exists($module,$arrCatType)){
	$home_address .= '
		<li>
			<a href="/'.$module.'.html">
				<span>' . $arrCatType[$module]['typ_name'] . '</span>
			</a>
		</li>
		';
}

#+ -------------------- MODULE -------------------- +#
#+
#+ Thiet lap Title + Meta cho module
$con_site_title 		= $meta_title_module != '' ? $meta_title_module : $con_site_title;
$con_meta_description 	= $meta_description_module != '' ? $meta_description_module : $con_meta_description;
$con_meta_keywords 		= $meta_keyword_module != '' ? $meta_keyword_module : $con_meta_keywords;
#+
#+ Neu co phan trang
$con_site_title			.= $current_page1 != 0 ? " - Trang " . $current_page1 : "";

#+ -------------------- CATEGORY -------------------- +#
#+
#+ Xy ly lien quan den category
if($iCat != 0){
	$query = ' SELECT cat_parent_id,cat_name,cat_name_index,cat_picture,meta_title,meta_description,meta_keyword'
			.' FROM tbl_category'
			.' WHERE cat_id = "'.$iCat.'"'
			;
	$db_query = new db_query($query);
	
	if($row = mysql_fetch_assoc($db_query->result)){
		
		#+
		$sCat				= $sCat == '' ? $row['cat_name_index'] : $sCat;
		#+
		$iCatParent			= $row["cat_parent_id"];
		$sCatName 			= $row['cat_name'];
		$sCatPicture 		= $row['cat_picture'];
		#+
		if($iData == 0){
			if($type == 'sim'){
				if($module == 'sim'){
					$meta_title 		= $row['meta_title'];
					$meta_description = $row['meta_description'];
					$meta_keyword 		= $row['meta_keyword'];
				} // End if($module == 'sim')
			}else{
				$meta_title 		   = $row['meta_title'];
				$meta_description    = $row['meta_description'];
				$meta_keyword        = $row['meta_keyword'];	
			} // End if($type == 'sim')	
		} // ENd if($iData == 0)
		

		#+
		$home_address	.= $menuid->getAllParentList("tbl_category","cat_id","cat_name","cat_parent_id",$iCat,' &raquo; ',"",$module,($module!='') ? 1 : 0,$lang_path,$con_extenstion,$con_mod_rewrite);

		$arrParent	= $menuid->getAllParentBread("tbl_category", "cat_id", "cat_parent_id", $iCat, "cat_id, cat_name, cat_name_index, cat_type");

		foreach($arrParent as $key => $row){
			$arrBreadcrumb[]	= array("id" => createLink("cat",$row), "name" => $row["cat_name"]);
		}

	} // End if($row = mysql_fetch_assoc($db_category->result)){	

} // End if($iCat != ''){


#+ -------------------- Phan loai sim -------------------- +#
#+
if($iType != 0 || $sType != ''){
	$sql = '';
	$sql .=  $iType != 0 ? ' AND simtp_id = '.$iType : ($sType != '' ? ' AND simtp_index = "'.$sType.'"' : '');
	
	$query = " SELECT simtp_id, simtp_name, simtp_index, meta_title, meta_description, meta_keyword
				FROM tbl_simtype
				WHERE 1".$sql."
				LIMIT 0,1"
				;
	$db_query 	= new db_query($query);
	# echo $query.'<br>';
	if($row = mysql_fetch_assoc($db_query->result)){
		#+
		$iType = $iType == 0 ? $row['simtp_id'] : $iType;
		$sTypeName		= $row["simtp_name"];
		$sTypeName_rep 	= str_replace('Sim ','',$sTypeName);
		
		#+
		if($iData == 0){
			if($type == 'sim'){
				if($module == 'simtype'){
					$meta_title 		= $row['meta_title'];
					$meta_description 	= $row['meta_description'];
					$meta_keyword 		= $row['meta_keyword'];
				} // End if($module == 'simtype')
			}else{
				$meta_title 		= $row['meta_title'];
				$meta_description 	= $row['meta_description'];
				$meta_keyword 		= $row['meta_keyword'];	
			} // End if($type == 'sim')	
		} // ENd if($iData == 0)

		#+
		#+ Home address
		$link = createLink('simtype',$row);
		$home_address .= '
			<li>	
				<a href="'.$link.'">
					<span>' . $sTypeName . '</span>
				</a>
			</li>
			';

		$arrBreadcrumb[] = array("id" => $link, "name" => $sTypeName);
		
	} // End if($row = mysql_fetch_assoc($db_query->result))
} // End if($iType != 0)


#+ -------------------- Dau so sim -------------------- +#
#+
if($iDauSo != 0 || $sDauSo != ''){
	$sql = '';
	$sql .=  $iDauSo != 0 ? ' AND sds_id = '.$iDauSo : ($sDauSo != '' ? ' AND sds_name = "'.$sDauSo.'"' : '');
	
	$query = ' SELECT sds_id,sds_name,sds_category, meta_title, meta_description, meta_keyword'
			.' FROM tbl_simdauso'
			.' WHERE 1'.$sql
			.' LIMIT 0,1';
	$db_query = new db_query($query);
	if($row = mysql_fetch_assoc($db_query->result)){
		
		#+
		$iDauSo	= $iDauSo == 0 ? $row['sds_id'] : $iDauSo;
		$sDauSo	= $sDauSo == 0 ? $row['sds_name'] : $sDauSo;
		$iCat 	= $iCat == 0 ? $row['sds_category'] : $iCat;

		#+
		if($iData == 0){
			if($type == 'sim'){
				if($module == 'simdauso'){
					$meta_title 		= $row['meta_title'];
					$meta_description 	= $row['meta_description'];
					$meta_keyword 		= $row['meta_keyword'];
				} // End if($module == 'simdauso')
			}else{
				$meta_title 		= $row['meta_title'];
				$meta_description 	= $row['meta_description'];
				$meta_keyword 		= $row['meta_keyword'];	
			} // End if($type == 'sim')	
		} // ENd if($iData == 0)
		
		#+
		#+ Home address
		$row['cat_name_index'] = $sCat;
		$link = createLink('simdauso',$row);
		$home_address .= '
			<li>	
				<a href="'.$link.'">
					<span>Đầu số '.$row['sds_name'].'</span>
				</a>
			</li>
			';

		$arrBreadcrumb[] = array("id" => $link, "name" => 'Đầu số '.$row['sds_name']);
		
	} // End if($row = mysql_fetch_assoc($db_query->result))
} // End if($module == 'simprice')


#+ -------------------- Gia sim -------------------- +#
#+
if($iPrice != 0 || $iPriceFrom != 0 || $iPriceTo != 0){
	$sql = '';
	$sql .=  $iPrice != 0 ? ' AND pri_id = '.$iPrice : ($iPriceFrom != 0 || $iPriceTo != 0  ? ' AND pri_name_index = "gia-'.$iPriceFrom.'-'.$iPriceTo.'"' : '');
	
	$query = ' SELECT pri_id, meta_title, meta_description, meta_keyword'
			.' FROM tbl_simprice'
			.' WHERE 1'.$sql
			.' LIMIT 0,1';
	$db_query = new db_query($query);
	if($row = mysql_fetch_assoc($db_query->result)){
		
		#+
		$iPrice	= $iPrice == 0 ? $row['pri_id'] : $iPrice;
		
		#+
		if($iData == 0){
			if($type == 'sim'){
				if($module == 'simprice'){
					$meta_title 		= $row['meta_title'];
					$meta_description 	= $row['meta_description'];
					$meta_keyword 		= $row['meta_keyword'];
				} // End if($module == 'simprice')
			}else{
				$meta_title 		= $row['meta_title'];
				$meta_description 	= $row['meta_description'];
				$meta_keyword 		= $row['meta_keyword'];	
			} // End if($type == 'sim')	
		} // ENd if($iData == 0)

		#+
		#+ Home address
		# $home_address	.= '&nbsp;&raquo;&nbsp;<a href="/gia-'.$row['pri_min'].'-'.$row['pri_max'].'/">Giá '.$row['pri_name'].'</a>';	
		
	} // End if($row = mysql_fetch_assoc($db_query->result))
} // End if($module == 'simprice')


#+ -------------------- Dai ly sim -------------------- +#
#+
if($iVendor || $sVendor != ''){
	$sql = '';
	$sql .=  $iVendor != 0 ? ' AND simch_id = '.$iVendor : ($sVendor != '' ? ' AND simch_index = "'.$sVendor.'"' : '');
	
	$query = " SELECT simch_name
				FROM tbl_simdaily
				WHERE 1".$sql."
				LIMIT 0,1"
				;
	$db_query 	= new db_query($query);
	if($row = mysql_fetch_assoc($db_query->result)){
		#+
		$sVenDorName		= $row["simch_name"];
	} // End if($row = mysql_fetch_assoc($db_query->result))
} // End if($iType != 0)


#+ -------------------- DETAIL -------------------- +#
#+
if($iData != 0){
	$tb_select      = $arrType[$type]['tb_name'];
	$tb_prefix      = $arrType[$type]['tb_prefix'];
	$tb_field_name  = $arrType[$type]['field_name'];
	$tb_select_more = isset($arrType[$type]['select_more']) ? $arrType[$type]['select_more'] : '';
	#+
	$sqlSelect      = '';
	$sqlWhere       = '';
	$simtp_name     = '';
	$simtp_index    = '';
	$srcPic			 = "";
	
	#+
	if($type == 'sim'){
		$sqlSelect 	.= ' sim_sim2 AS dat_id, sim_sim1 AS dat_title, sim_price AS dat_price, sim_typeid';
		$sqlWhere	.= ' AND sim_sim2 = "'. $iData.'"';
	}else{
		$sqlSelect 	.= $tb_prefix.'id AS dat_id, '.$tb_prefix.$tb_field_name.' AS dat_title, '.$tb_prefix.'teaser AS dat_teaser, '.$tb_prefix.'description AS dat_description, '.$tb_prefix.'date AS dat_date, meta_title, meta_description, meta_keyword'.$tb_select_more;
		$sqlWhere	.= ' AND '.$tb_prefix.'id = "'. $iData.'"';
	} // End if($module == 'sim')
		
	#+
	$query = ' SELECT '.$sqlSelect
			.' FROM '.$tb_select
			.' WHERE 1'.$sqlWhere
			;
	$db_query 	= new db_query($query);
	#+
	if($row = mysql_fetch_assoc($db_query->result)){
		#+
		#+ khai bao bien
		#+
		$sProName         = $row['dat_title'];
		$sProTes          = $row['dat_teaser'];
		$sProDes          = $row['dat_description'];
		$sProDate         = $row['dat_date'];
		$sProMetaKw       = $row['meta_keyword'];
		$sProPrice        = isset($row['dat_price']) ? $row['dat_price'] : '';
		$simtp_name       = isset($arrSimType[$row['sim_typeid']]["simtp_name"]) ? $arrSimType[$row['sim_typeid']]["simtp_name"] : '';
		$simtp_index      = isset($arrSimType[$row['sim_typeid']]["simtp_index"]) ? $arrSimType[$row['sim_typeid']]["simtp_index"] : '';

		if($type == "sim"){
			$pathMobi              = $sCatName . ".jpg";
			$strNumber             = $sProName;
			$strPrice              = number_format($sProPrice);
			$strType               = $simtp_name;
			$srcPic                = generatePictureDetail($pathMobi, $strNumber, $strPrice, $strType);
			$sim_description       = "Sim số đẹp giá gốc: " . $sProName . ", Giá bán: " . $strPrice . " đ, Mạng di động: " . $sCatName . ", Loại sim: " . $strType;
			$row["cat_name_index"] = isset($sCat) && $sCat != "" ? $sCat : "";
			$row["sim_sim2"]       = $row["dat_id"];
			$link_detail           = createLink("detail_sim",$row);
			$arrProductJsonData   	= array("sim_name" 			=> $sProName,
													"sim_picture"     => $srcPic,
													"sim_description" => $sim_description,
													"sim_sku"         => "0" . $row["dat_id"],
													"sim_mpn"         => "Sieuthisimthe_0" . $row["dat_id"],
													"sim_brand"       => "Sieuthisimthe",
													"sim_link"        => $link_detail,
													"sim_price" 		=> $sProPrice);
		}
		else if($type == "news"){
			$srcPic   = $row["new_picture"] != '' ? '/store/media/news/' . $row["new_title_index"] . '/' . $row["new_picture"] : '/store/media/no_image.jpg';
		}

		#+
		$meta_title       = isset($row['meta_title']) ? $row['meta_title'] : '';
		$meta_description = isset($row['meta_description']) ? $row['meta_description'] : '';
		$meta_keyword     = isset($row['meta_keyword']) ? $row['meta_keyword'] : '';
	} // End if($query!=''){
		
	#+
	#+ Huy bien
	$db_query->close();
	unset($db_query);
	
	#+
	$sProName  = $sProName != '' ? $sProName : '0' . $iData;
	$sVenDorName = $sVenDorName != '' ? $sVenDorName : 'STST';

} // End if($iData != ''){
 

#+ -------------------- DICH TITLE -------------------- +#
#+
#+ Thiet lap Title + Meta cho module Tim kiem va tag
if($keyword != '' || $iCat != 0 || $iType != 0 || $iDauSo != 0 || $iPrice != 0 || $iData != 0 || $iNguHanh != 0){
	
	#+
	#+ Dich title khi co keyword
	if($keyword != ''){
		#+
		$arrRep3the['tu_khoa'] = $keyword_r != '' ? $keyword_r : '';
		$macdinh = $keyword_r;
	} // End if($keyword != '')
	
	#+
	#+ Dich title khi co sCatName
	if($iCat != 0){
		$arrRep3the['ten_danh_muc'] = $sCatName != '' ? $sCatName : '';
		$arrRep3the['ten_nha_mang'] = $sCatName != '' ? $sCatName : '';
		$mac_dinh = $sCatName;	
	} // End if($iCat != 0 && $sCatName != '')
	
	#+
	#+ Dich title khi co sTypeName
	if($iType != 0){
		$arrRep3the['ten_phan_loai'] = $sTypeName_rep != '' ? $sTypeName_rep : '';
		$mac_dinh = $sTypeName;
	} // ENd if($iType != 0 && $sTypeName != '')
	
	#+
	#+ Dich title khi co sDauSo
	if($iDauSo != 0){
		$arrRep3the['ten_dau_so'] = $sDauSo != '' ? $sDauSo : '';
		$mac_dinh = 'Đầu số '.$sDauSo.' '.$sCatName;
	} // End if($iDauSo != 0 && $sDauSo != '')
	
	#+
	#+ Dich title khi co sDauSo
	if($iPrice != 0){
		$arrRep3the['gia_tu'] = $iPriceFrom != 0 ? $iPriceFrom : 0;
		$arrRep3the['gia_den'] = $iPriceTo != 0 ? $iPriceTo : 0;
		$mac_dinh = 'Tìm sim số đẹp';
	} // End if($iPriceFrom != 0 && $iPriceTo != '')
	
	#+
	#+ Dich title khi co sDauSo
	if($iNguHanh != 0){
		if($module == 'simhoptuoi'){
			$arrRep3the['ten_tuoi'] = $arrThongTin['can_nam'].' '.$arrThongTin['chi_nam'].' '.$iNamSinh;
			$mac_dinh = 'Sim số đẹp hợp tuổi';
		}elseif($module == 'simhopmenh'){
			$arrRep3the['ten_menh'] = $arrNguHanh[$iNguHanh];
			$mac_dinh = 'Sim số đẹp hợp mệnh';	
		}
	} // End if($iPriceFrom != 0 && $iPriceTo != '')

	 
	if($iData != 0){
		$arrRep3the['ten_bai_viet'] = $sProName != '' ? $sProName : '';
		$arrRep3the['mo_ta_bai_viet'] = $sProTes != '' ? $sProTes : '';
		$arrRep3the['ten_sim_1'] = $sProName != '' ? $sProName : '';
		$arrRep3the['ten_sim_2'] = '0'.$iData;
		$macdinh = $sProName;
	} // End if($iData != 0)

	#+
	#+ Dich 3 the
	$loai = 'cat';
	if($iData != 0 )
		$loai = 'detail';

	
	$arrReturn = dich3theTpl($loai,$arrRep3the,$macdinh);
	$con_site_title 		= $arrReturn['con_site_title'];
	$con_meta_description 	= $arrReturn['con_meta_description'];
	$con_meta_keywords 		= $arrReturn['con_meta_keywords'];
	unset($arrReturn);
	
} // End if($module == 'tim-kiem' || $module == 'tag')	


#+ -------------------- SEO LINK -------------------- +#	
#+
#+ Lay title + Des + Keyword theo table tbl_seolink
if($module == 'home'){
	$seo_link = '/';
}else{
	$seo_link = $_SERVER['REQUEST_URI'];
} // End if($module == 'home')

#+
$sql = '';
//if($iCat == 0 && $iData == 0) $sql .= ' OR seo_link LIKE "%'.$module.'%"';


#+
$seo_link = mysql_escape_string($seo_link);
#+

$arrTextSeo = array();
$query = ' SELECT seo_link, seo_link_redirect, seo_title, seo_description, seo_keyword, seo_h1, seo_h1d, seo_h2, seo_h2d, seo_h3, seo_h3d'
		.' FROM tbl_seolink'
		.' WHERE  lang_id = '.$lang_id.' AND seo_active = 1 AND seo_link = "'.$seo_link.'"' .$sql
		.' ORDER BY seo_date DESC LIMIT 1'
		;
$db_query = new db_query($query);
if($row = mysql_fetch_assoc($db_query->result)){
	
	$seo_link_redirect = trim($row['seo_link_redirect']);
	if($seo_link_redirect != '') redirectHeader($seo_link_redirect, 1);
	
	if($row['seo_title'] != ''){
		$con_site_title 			= $row['seo_title'];
		$con_site_title				.= $current_page1 != 0 ? " - Trang " . $current_page1 : "";	
	}
		
	if($row['seo_description'] != '')
		$con_meta_description		= $row['seo_description'];
	
	if($row['seo_keyword'] != '')
		$con_meta_keywords			= $row['seo_keyword'];
		
	$con_site_h1				= $row['seo_h1'];
	$con_site_h1d				= $row['seo_h1d'];
	$con_site_h2				= $row['seo_h2'];
	$con_site_h2d				= $row['seo_h2d'];
	$con_site_h3				= $row['seo_h3'];
	$con_site_h3d				= $row['seo_h3d'];

	$arrTextSeo = array(0 	=> array("title" => $con_site_h1, "content" => $con_site_h1d),
							  1	=> array("title" => $con_site_h2, "content" => $con_site_h2d),
							  2 	=> array("title" => $con_site_h3, "content" => $con_site_h3d),
	);

} // End if( mysql_num_rows($db_query)  > 0 )

#+ -------------------- TYPE -------------------- +#
#+
#+ Xac dinh type theo module
$type = $type == '' ? $arrCatType[$module]['typ_type'] : $type;
?>