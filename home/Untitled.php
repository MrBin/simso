<!DOCTYPE html><html manifest="">
<head>
<link rel="canonical" href="http://www.vatgia.com/home/"/>
<meta charset="utf-8"/>
<meta name="viewport" content="initial-scale=1, maximum-scale=1.0, minimum-scale=1.0">
<meta name="format-detection" content="telephone=yes">
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black">
<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<link rel="stylesheet" type="text/css" href="http://static.vatgia.com/cjs/mobile20150203/v1/css/style_all_v3.css">
<script type="text/javascript" src="http://static.vatgia.com/cjs/mobile20150203/v1/js/js_all_v3.js"></script>
<title>Vatgia.com | Sản phẩm | Rao vặt | Hỏi đáp tư vấn | Đánh giá (Review) | Cửa hàng | Tin tức - Touch Version</title>
<meta name="keywords" content="Touch Version, Mobile, Laptop, rao vặt, sản phẩm, hỏi đáp, tin tức, cửa hàng"/>
<meta name="description" content="Touch.Vatgia.com Touch Version - Website thương mại điện tử hàng đầu tại Việt Nam, cung cấp hàng trăm nghìn sản phẩm từ hàng nghìn nhà cung cấp khác nhau"/>
<meta name="revisit-after" content="1 days"/>
</head>
<body>
<div id="home">
	<div id="home_content">
		<div id="menu">
			<ul id="category">
				<li class="level_0"><a href="/xu-huong-mua-sam-2014/100.shtml"><span class="cat_text">Khuyến mại <i class="vcon-hot_menu"></i></span></a><a href="/xu-huong-mua-sam-2014/100.shtml" class="larrow"><i class="vcon-larrow"></i></a></li>
				<li id="mainCat_1300" idata="1300" class="level_0"><a href="javascript:;" onclick="loadSubCategory(1300,1);"><span class="cat_text">Điện thoại, viễn thông</span></a><a href="/1300/dien-thoai-vien-thong.html" class="larrow"><i class="vcon-darrow"></i></a>
				<ul class="sub_cat" id="sub_cat_1300">
				</ul>
				</li>
				<li id="mainCat_392" idata="392" class="level_0"><a href="javascript:;" onclick="loadSubCategory(392,1);"><span class="cat_text">Máy tính, linh kiện</span></a><a href="/392/may-tinh-linh-kien.html" class="larrow"><i class="vcon-darrow"></i></a>
				<ul class="sub_cat" id="sub_cat_392">
				</ul>
				</li>
				<li id="mainCat_331" idata="331" class="level_0"><a href="javascript:;" onclick="loadSubCategory(331,1);"><span class="cat_text">Điện tử, điện máy</span></a><a href="/331/dien-tu-dien-may.html" class="larrow"><i class="vcon-darrow"></i></a>
				<ul class="sub_cat" id="sub_cat_331">
				</ul>
				</li>
				<li id="mainCat_405" idata="405" class="level_0"><a href="javascript:;" onclick="loadSubCategory(405,1);"><span class="cat_text">Máy ảnh, máy quay</span></a><a href="/405/may-anh-may-quay.html" class="larrow"><i class="vcon-darrow"></i></a>
				<ul class="sub_cat" id="sub_cat_405">
				</ul>
				</li>
				<li id="mainCat_3544" idata="3544" class="level_0"><a href="javascript:;" onclick="loadSubCategory(3544,1);"><span class="cat_text">Ô tô, xe máy, xe đạp</span></a><a href="/3544/o-to-xe-may-xe-dap.html" class="larrow"><i class="vcon-darrow"></i></a>
				<ul class="sub_cat" id="sub_cat_3544">
				</ul>
				</li>
				<li id="mainCat_768" idata="768" class="level_0"><a href="javascript:;" onclick="loadSubCategory(768,1);"><span class="cat_text">Công nghiệp, Xây dựng </span></a><a href="/768/cong-nghiep-xay-dung.html" class="larrow"><i class="vcon-darrow"></i></a>
				<ul class="sub_cat" id="sub_cat_768">
				</ul>
				</li>
				<li id="mainCat_640" idata="640" class="level_0"><a href="javascript:;" onclick="loadSubCategory(640,1);"><span class="cat_text">Thời trang, phụ kiện</span></a><a href="/640/thoi-trang-phu-kien.html" class="larrow"><i class="vcon-darrow"></i></a>
				<ul class="sub_cat" id="sub_cat_640">
				</ul>
				</li>
				<li id="mainCat_586" idata="586" class="level_0"><a href="javascript:;" onclick="loadSubCategory(586,1);"><span class="cat_text">Mẹ & Bé</span></a><a href="/586/me-be.html" class="larrow"><i class="vcon-darrow"></i></a>
				<ul class="sub_cat" id="sub_cat_586">
				</ul>
				</li>
				<li id="mainCat_2434" idata="2434" class="level_0"><a href="javascript:;" onclick="loadSubCategory(2434,1);"><span class="cat_text">Sức khỏe, sắc đẹp</span></a><a href="/2434/suc-khoe-sac-dep.html" class="larrow"><i class="vcon-darrow"></i></a>
				<ul class="sub_cat" id="sub_cat_2434">
				</ul>
				</li>
				<li id="mainCat_912" idata="912" class="level_0"><a href="javascript:;" onclick="loadSubCategory(912,1);"><span class="cat_text">Đồ dùng sinh hoạt</span></a><a href="/912/do-dung-sinh-hoat.html" class="larrow"><i class="vcon-darrow"></i></a>
				<ul class="sub_cat" id="sub_cat_912">
				</ul>
				</li>
				<li id="mainCat_779" idata="779" class="level_0"><a href="javascript:;" onclick="loadSubCategory(779,1);"><span class="cat_text">Nội thất, ngoại thất</span></a><a href="/779/noi-that-ngoai-that.html" class="larrow"><i class="vcon-darrow"></i></a>
				<ul class="sub_cat" id="sub_cat_779">
				</ul>
				</li>
				<li id="mainCat_923" idata="923" class="level_0"><a href="javascript:;" onclick="loadSubCategory(923,1);"><span class="cat_text">Sách, đồ văn phòng</span></a><a href="/923/sach-do-van-phong.html" class="larrow"><i class="vcon-darrow"></i></a>
				<ul class="sub_cat" id="sub_cat_923">
				</ul>
				</li>
				<li id="mainCat_1169" idata="1169" class="level_0"><a href="javascript:;" onclick="loadSubCategory(1169,1);"><span class="cat_text">Hoa, quà tặng, đồ chơi</span></a><a href="/1169/hoa-qua-tang-do-choi.html" class="larrow"><i class="vcon-darrow"></i></a>
				<ul class="sub_cat" id="sub_cat_1169">
				</ul>
				</li>
				<li id="mainCat_561" idata="561" class="level_0"><a href="javascript:;" onclick="loadSubCategory(561,1);"><span class="cat_text">Dịch vụ, giải trí, du lịch</span></a><a href="/561/dich-vu-giai-tri-du-lich.html" class="larrow"><i class="vcon-darrow"></i></a>
				<ul class="sub_cat" id="sub_cat_561">
				</ul>
				</li>
				<li id="mainCat_913" idata="913" class="level_0"><a href="javascript:;" onclick="loadSubCategory(913,1);"><span class="cat_text">Thực phẩm, đồ uống</span></a><a href="/913/thuc-pham-do-uong.html" class="larrow"><i class="vcon-darrow"></i></a>
				<ul class="sub_cat" id="sub_cat_913">
				</ul>
				</li>
				<li id="mainCat_6623" idata="6623" class="level_0"><a href="/raovat/2588/bat-dong-san.html" onclick="loadSubCategory(6623,1);"><span class="cat_text">Bất động sản</span></a><a href="http://vatgia.com/raovat/2588/bat-dong-san-otc.html" class="larrow"><i class="vcon-darrow"></i></a>
				<ul class="sub_cat" id="sub_cat_6623">
				</ul>
				</li>
			</ul>
			<div class="cat_overlay">
				<div class="img">
					<img src="http://static.vatgia.com/cjs/mobile20150203/v1/css/loading.gif"/>
				</div>
			</div>
			<ul class="beacon_sprite">
				<li><a href="https://www.baokim.vn/wap/" rel="nofollow" title="Bảo Kim" target="_blank"><i class="beacon-baokim"></i> BaoKim.vn</a></li>
				<li><a href="http://m.mytour.vn/" rel="nofollow" title="mytour" target="_blank"><i class="beacon-mytour"></i>Mytour</a></li>
				<li><a href="http://123doc.org/trang-chu.htm" rel="nofollow" title="123 Đọc" target="_blank"><i class="beacon-123doc"></i>123Doc.org</a></li>
				<li><a href="http://nhanh.vn/" rel="nofollow" title="123TV" target="_blank"><i class="beacon-nhanh"></i>Nhanh.vn</a></li>
			</ul>
		</div>
		<div id="right_menu">
			<ul id="right_content">
				<li class="r_user menu_parent"><a href="javascript:;" title="user name" onclick="toggleUser();"><span class="r_avatar"><img src="http://media.vatgia.vn/user_image/small_teb1423282196.jpg"/></span><b class="user_name">txnc2002</b><span class="right"><i class="vcon-next-gray"></i><i class="vcon-down_gray"></i></span></a>
				<ul class="user_info">
					<li><span class="name">Tài khoản chính</span><b class="count">5.480 ₫</b></li>
					<li><span class="name">Tài khoản khuyến mại</span><b class="count">549.450 ₫</b></li>
					<li><span class="name">Tài khoản BaoKim.vn:</span><b class="count">0 ₫</b></li>
					<li class="vpoint"><span class="vpoint_content"><span class="vpoint_name">Tài khoản thưởng:</span><b class="vpoint_count">0đ</b></span><span style="color: #666; font-size:11px; margin-left:10px; margin-top: -5px; float:left;">( cập nhật lúc: 13/05/15 17:28 )</span></li>
				</ul>
				</li>
				<li class="menu_parent"><a href="/home/showcart.php" title="Giỏ hàng" style=""><span class="icon"><i class="vcon-cart r_cart" style=""></i></span>Giỏ hàng</a></li>
				<li class="menu_parent"><a href="/txnc2002" title="Gian hàng của tôi" style=""><span class="icon"><i class="vcon-estore" style=""></i></span>Gian hàng của tôi</a></li>
				<li class="menu_parent"><a href="javascript:void(0)" title="Chat ngay" style="" onclick="hide_panel_vgchat();"><span class="icon"><i class="vcon-vchat r_cart" style=""></i></span>Chat ngay<span class="right"><span class="vc_touch_messages touch_mess"></span></span></a></li>
				<li class="menu_parent"><a href="/home/user_view.php" title="Sản phẩm đã xem" style=""><span class="icon"><i class="vcon-eye_666" style=""></i></span>Sản phẩm đã xem</a></li>
				<li class="menu_parent"><a href="/home/post.php" title="Đăng sản phẩm" style=""><span class="icon"><i class="vcon-circle_plus" style=""></i></span>Đăng sản phẩm</a></li>
				<li class="menu_parent"><a href="#" title="Đơn hàng của tôi" style=""><span class="icon"><i class="vcon-order" style=""></i></span>Đơn hàng của tôi</a></li>
				<li class="menu_child"><a href="/home/profile.php?nType=order_listing" title="Đơn hàng đã đặt">Đơn hàng đã đặt<span class="right"><i class="vcon-next-gray"></i></span></a></li>
				<li class="menu_child"><a href="/home/profile.php?nType=listing_order" title="Đơn hàng từ người mua">Đơn hàng từ người mua<span class="count"> (3)</span><span class="right"><i class="vcon-next-gray"></i></span></a></li>
				<li class="menu_parent"><a href="#" title="Tin đã đăng tại Vatgia.com" style=""><span class="icon"><i class="vcon-list" style=""></i></span>Tin đã đăng tại Vatgia.com</a></li>
				<li class="menu_child"><a href="/home/profile.php?nType=listing_product" title="Danh sách sản phẩm">Danh sách sản phẩm<span class="right"><i class="vcon-next-gray"></i></span></a></li>
				<li class="menu_child"><a href="/profile/?module=raovat" title="Danh sách rao vặt">Danh sách rao vặt<span class="right"><i class="vcon-next-gray"></i></span></a></li>
				<li class="menu_child"><a href="/profile/?module=hoidap" title="Danh sách hỏi đáp">Danh sách hỏi đáp<span class="right"><i class="vcon-next-gray"></i></span></a></li>
				<li class="menu_parent"><a href="#" title="Quản lý tài khoản cá nhân" style=""><span class="icon"><i class="vcon-profile" style=""></i></span>Quản lý tài khoản cá nhân</a></li>
				<li class="menu_child"><a href="/home/profile.php" title="Thông tin tài khoản">Thông tin tài khoản<span class="right"><i class="vcon-next-gray"></i></span></a></li>
				<li class="menu_child"><a href="/home/profile.php?nType=change_password" title="Thay đổi mật khẩu">Thay đổi mật khẩu<span class="right"><i class="vcon-next-gray"></i></span></a></li>
				<li class="menu_parent"><a href="#" title="Hỗ trợ" style=""><span class="icon"><i class="vcon-support" style=""></i></span>Hỗ trợ</a></li>
				<li class="menu_child"><a href="tel:19002055" title="Hỗ trợ chung : 19002055">Hỗ trợ chung : <span style="color: #0072bd;">19002055<span></a></li>
				<li class="menu_parent"><a href="/home/help.php" title="Câu hỏi thường gặp" style=""><span class="icon"><i class="vcon-gray_question" style=""></i></span>Câu hỏi thường gặp</a></li>
				<li class="menu_parent"><a href="http://www.vatgia.com/home/pcswitch.php?redirect=L2hvbWUv" title="Xem bản đầy đủ Vatgia.com" style=""><span class="icon"><i class="vcon-view_full" style=""></i></span>Xem bản đầy đủ Vatgia.com</a></li>
				<li class="menu_parent"><a href="/home/act_logout.php" title="Thoát" style="color : red;"><span class="icon"><i class="vcon-logout" style="margin-top : 0;"></i></span>Thoát</a></li>
			</ul>
		</div>
		<div id="main_container" class="index">
			<script type="text/javascript">function closePopupApp(){$('#downloadApp').hide();$('.downloadApp_fix').hide();setCookieApp("closePopupApp","1",1);}function setCookieApp(cname,cvalue,exdays){var d = new Date();d.setTime(d.getTime()+(exdays*24*60*60*1000)); var expires = "expires="+d.toGMTString(); var path ="path=/";document.cookie = cname + "=" + cvalue + "; " + expires + "; " + path;}</script>
			<div id="header_bar">
				<table cellpadding="0" cellspacing="0" border="0" border-spacing="0" width="100%">
				<tr>
					<td width="50" class="menu_nav">
						<span class="left_nav" onclick="toggleMenu();trackEvent('Menu trái', 'Click', '');"><i class="vcon-menu"></i><span class="nav_text">Danh mục</span></span>
					</td>
					<td class="center_nav">
						<table class="table_tab_web" cellpadding="0" cellspacing="0" border="0" border-spacing="0" width="100%">
						<tr>
							<td class="active ">
								<a href="/home/" title="Sản phẩm" onclick="trackEvent('Top menu', 'Click', 'Tab Sản phẩm');"><span>Sản phẩm</span></a>
							</td>
							<td class="">
								<a href="/raovat/" title="Rao vặt" onclick="trackEvent('Top menu', 'Click', 'Tab Rao vặt');"><span>Rao vặt</span></a>
							</td>
							<td class="">
								<a href="/hoidap/" title="Hỏi đáp" onclick="trackEvent('Top menu', 'Click', 'Tab Hỏi đáp');"><span>Hỏi đáp</span></a>
							</td>
						</tr>
						</table>
					</td>
					<td align="right" width="50" class="menu_nav">
						<span class="right_nav" onclick="toggleRightMenu()"><i class="vcon-setting"></i><span class="nav_text">Cá nhân</span></span>
					</td>
				</tr>
				</table>
			</div>
			<div class="header_search">
				<form name="header_search" action="/home/quicksearch.php" method="get" onsubmit="trackEvent( 'Keyword', 'Search Sản phẩm', $('input[name=keyword].input_search_header').val()); checkForm(this.name, arrCtrlHeaderSearch); return false;">
					<table cellpadding="0" cellspacing="0" border="0" width="100%">
					<tr>
						<td class="left_search" width="30" align="center">
							<a href="/home/"><i class="vcon-logo"></i></a>
						</td>
						<td style="padding: 0 10px;">
							<div class="input_search">
								<table cellpadding="0" cellspacing="0" border="0" width="100%">
								<tr>
									<td width="30">
										<i class="vcon-rv_search"></i>
									</td>
									<td>
										<input type="search" name="keyword" class="input_search_header" onfocus="searchFullV3(true);trackEvent('Header Search', 'Click', 'Trang Sản phẩm');" placeholder="Nhập từ khóa tìm kiếm..." id="keyword_search" value=""/><input type="hidden" class="iCat" id="iCat" name="iCat" value="0"/>
									</td>
								</tr>
								</table>
							</div>
						</td>
						<td class="close_search" onclick="searchFullV3(false);" width="50">
							Đóng
						</td>
						<td width="40" class="hd_cart">
							<a href="/home/showcart.php" title="Giỏ hàng"><i class="vcon-cart r_cart"></i></a>
						</td>
					</tr>
					</table>
				</form>
			</div>
			<script type="text/javascript">var defaultSuggest = {"status":"status","result":[{"p_html":"<b>Surface<\/b>","p_text":"Surface","iCat":"","p_price":""},{"p_html":"<b>iphone<\/b>","p_text":"iphone","iCat":"","p_price":""},{"p_html":"<b>V\u00e1n tr\u01b0\u1ee3t r\u00e1p<\/b>","p_text":"V\u00e1n tr\u01b0\u1ee3t r\u00e1p","iCat":"","p_price":""},{"p_html":"<b>V\u00e1n tr\u01b0\u1ee3t r\u00e1p s\u1eb5n 1<\/b>","p_text":"V\u00e1n tr\u01b0\u1ee3t r\u00e1p s\u1eb5n 1","iCat":"","p_price":""},{"p_html":"<b>m\u00e1y \u1ea3nh<\/b>","p_text":"m\u00e1y \u1ea3nh","iCat":"","p_price":""},{"p_html":"<b>v\u00e1y<\/b>","p_text":"v\u00e1y","iCat":"","p_price":""},{"p_html":"<b>\u0111\u1ea7m li\u1ec1n<\/b>","p_text":"\u0111\u1ea7m li\u1ec1n","iCat":"","p_price":""},{"p_html":"<b>\u0111\u1ea7m ng\u1eafn<\/b>","p_text":"\u0111\u1ea7m ng\u1eafn","iCat":"","p_price":""},{"p_html":"<b>Sony Xperia Z1 Honami C6903 LTE Black Sony Xperia Z1 Honami C6903 LTE Black 6.000.000 \u0111<\/b>","p_text":"Sony Xperia Z1 Honami C6903 LTE Black Sony Xperia Z1 Honami C6903 LTE Black 6.000.000 \u0111","iCat":"","p_price":""},{"p_html":"<b>Apple iPhone 4S 16GB White (B\u1ea3n qu\u1ed1c t\u1ebf) Apple iPhone 4S<\/b>","p_text":"Apple iPhone 4S 16GB White (B\u1ea3n qu\u1ed1c t\u1ebf) Apple iPhone 4S","iCat":"","p_price":""},{"p_html":"<b>V\u00e1y fgh677<\/b>","p_text":"V\u00e1y fgh677","iCat":"","p_price":""},{"p_html":"<b>thi\u1ebft b\u1ecb \u0111\u1ecbnh v\u1ecb<\/b>","p_text":"thi\u1ebft b\u1ecb \u0111\u1ecbnh v\u1ecb","iCat":"","p_price":""},{"p_html":"<b>Apple iPhone 6 Plus 16GB Space Gray (B\u1ea3n qu\u1ed1c t\u1ebf)<\/b>","p_text":"Apple iPhone 6 Plus 16GB Space Gray (B\u1ea3n qu\u1ed1c t\u1ebf)","iCat":"","p_price":""},{"p_html":"<b>Apple iPhone 6 16GB Space Gray (B\u1ea3n qu\u1ed1c t\u1ebf)<\/b>","p_text":"Apple iPhone 6 16GB Space Gray (B\u1ea3n qu\u1ed1c t\u1ebf)","iCat":"","p_price":""},{"p_html":"<b>\u0111\u1ed3ng ph\u1ee5c glu<\/b>","p_text":"\u0111\u1ed3ng ph\u1ee5c glu","iCat":"","p_price":""},{"p_html":"<b>dong phuc<\/b>","p_text":"dong phuc","iCat":"","p_price":""},{"p_html":"<b>may hut sua<\/b>","p_text":"may hut sua","iCat":"","p_price":""},{"p_html":"<b>iphone<\/b>","p_text":"iphone","iCat":"","p_price":""},{"p_html":"<b>maia<\/b>","p_text":"maia","iCat":"","p_price":""},{"p_html":"<b>sasaba<\/b>","p_text":"sasaba","iCat":"","p_price":""},{"p_html":"<b>kafen<\/b>","p_text":"kafen","iCat":"","p_price":""},{"p_html":"<b>x\u1ecbt ch\u1ed1ng n\u1eafng<\/b>","p_text":"x\u1ecbt ch\u1ed1ng n\u1eafng","iCat":"","p_price":""},{"p_html":"<b>coram teething<\/b>","p_text":"coram teething","iCat":"","p_price":""},{"p_html":"<b>d\u00e2\u0300u ca\u0301<\/b>","p_text":"d\u00e2\u0300u ca\u0301","iCat":"","p_price":""},{"p_html":"<b>xi\u0323t ch\u00f4\u0301ng n\u0103\u0301ng<\/b>","p_text":"xi\u0323t ch\u00f4\u0301ng n\u0103\u0301ng","iCat":"","p_price":""},{"p_html":"<b>d\u00e2\u0300u ca\u0301 omega<\/b>","p_text":"d\u00e2\u0300u ca\u0301 omega","iCat":"","p_price":""},{"p_html":"<b>t\u1ea9y n\u1ed1t ru\u1ed3i<\/b>","p_text":"t\u1ea9y n\u1ed1t ru\u1ed3i","iCat":"","p_price":""},{"p_html":"<b>iphone 4<\/b>","p_text":"iphone 4","iCat":"","p_price":""},{"p_html":"<b>tour ph\u00fa qu\u1ed1c<\/b>","p_text":"tour ph\u00fa qu\u1ed1c","iCat":"","p_price":""},{"p_html":"<b>b\u00e1n chung c\u01b0<\/b>","p_text":"b\u00e1n chung c\u01b0","iCat":"","p_price":""},{"p_html":"<b>binh cuu hoa<\/b>","p_text":"binh cuu hoa","iCat":"","p_price":""},{"p_html":"<b>v\u00e1y ren<\/b>","p_text":"v\u00e1y ren","iCat":"","p_price":""},{"p_html":"<b>chu\u1ed9t<\/b>","p_text":"chu\u1ed9t","iCat":"","p_price":""},{"p_html":"<b>htc one max<\/b>","p_text":"htc one max","iCat":"","p_price":""},{"p_html":"<b>iphone<\/b>","p_text":"iphone","iCat":"","p_price":""},{"p_html":"<b>gi\u00e0y nike<\/b>","p_text":"gi\u00e0y nike","iCat":"","p_price":""},{"p_html":"<b>cardigan<\/b>","p_text":"cardigan","iCat":"","p_price":""},{"p_html":"<b>v\u1eadn chuy\u1ec3n nhanh<\/b>","p_text":"v\u1eadn chuy\u1ec3n nhanh","iCat":"","p_price":""},{"p_html":"<b>dien thoai<\/b>","p_text":"dien thoai","iCat":"","p_price":""},{"p_html":"<b>sony<\/b>","p_text":"sony","iCat":"","p_price":""},{"p_html":"<b>creepy pasta<\/b>","p_text":"creepy pasta","iCat":"","p_price":""},{"p_html":"<b>\u0111i\u1ec7n tho\u1ea1i n\u00e0o t\u1ed1t<\/b>","p_text":"\u0111i\u1ec7n tho\u1ea1i n\u00e0o t\u1ed1t","iCat":"","p_price":""},{"p_html":"<b>mua k\u1eb9o \u1edf \u0111\u00e2u<\/b>","p_text":"mua k\u1eb9o \u1edf \u0111\u00e2u","iCat":"","p_price":""},{"p_html":"<b>click h\u1ee3p l\u1ec7<\/b>","p_text":"click h\u1ee3p l\u1ec7","iCat":"","p_price":""},{"p_html":"<b>ch\u1ee3 kim bi\u00ean<\/b>","p_text":"ch\u1ee3 kim bi\u00ean","iCat":"","p_price":""},{"p_html":"<b>\u0111i\u00ea\u0309m thi l\u01a1\u0301p 10<\/b>","p_text":"\u0111i\u00ea\u0309m thi l\u01a1\u0301p 10","iCat":"","p_price":""},{"p_html":"<b>asus zenphone 5<\/b>","p_text":"asus zenphone 5","iCat":"","p_price":""},{"p_html":"<b>mua xe \u0111\u1ea9y h\u00e0ng<\/b>","p_text":"mua xe \u0111\u1ea9y h\u00e0ng","iCat":"","p_price":""},{"p_html":"<b>\u00e1o polo<\/b>","p_text":"\u00e1o polo","iCat":"","p_price":""},{"p_html":"<b>8510<\/b>","p_text":"8510","iCat":"","p_price":""},{"p_html":"<b>tin vip v\u00e0 si\u00eau vip<\/b>","p_text":"tin vip v\u00e0 si\u00eau vip","iCat":"","p_price":""},{"p_html":"<b>thanh to\u00e1n t\u1ea1m gi\u1eef<\/b>","p_text":"thanh to\u00e1n t\u1ea1m gi\u1eef","iCat":"","p_price":""},{"p_html":"<b>\u0111\u1ed5i tr\u1ea3<\/b>","p_text":"\u0111\u1ed5i tr\u1ea3","iCat":"","p_price":""},{"p_html":"<b>tam gi\u1eef<\/b>","p_text":"tam gi\u1eef","iCat":"","p_price":""}]};$("#keyword_search").autoSuggest('/ajax/autocomplete.php', {minChars : 1,selectionLimit : 1,selectedItemProp : 'p_text',searchObjProps : 'p_text',selectedItemiCat: 'iCat',startText: '',retrieveLimit : 15,defaultData:defaultSuggest,formatList : function(data, el) {var html = formatResults(data);el.html(html);$('.as-list').append(el);},retrieveComplete: function(data) {return data.result;}});</script>
			<div id="statusdiv">
			</div>
			<script type="text/javascript">var module= "product";var catParentId= 0;var catHasChild= 0;var user_logged= 1;var con_root_path= "/home/";var con_ajax_path= "/ajax/";var fs_imagepath= "http://static.vatgia.com/cjs/mobile20150203/v1/images/";var fs_redirect= "L2hvbWUv";</script>
			<div id="banner_top_360x110" style="margin: 0px auto;max-width: 360px;">
			</div>
			<script>$(document).ready(function(){var intCheck=setInterval(function(){if($('#banner_top_360x110').html() != ""){$('#banner_top_360x110 a').removeAttr('target');clearInterval(intCheck);}},100);});</script>
			<div class="home_cat">
				<ul class="cat_slide">
					<li class="cat_sl_item first active " data-item="0">
					<div class="cat_sl_info">
						<a href="/1300/dien-thoai-vien-thong.html" title="Điện thoại viễn thông">
						<p>
							<img src="http://static.vatgia.com/themes/images/category/dien-thoai.jpg" alt="Điện thoại viễn thông"/>
						</p>
						<span>Điện thoại viễn thông</span></a>
					</div>
					<div class="cat_sl_info">
						<a href="/392/may-tinh-linh-kien.html" title="Máy tính linh kiện">
						<p>
							<img src="http://static.vatgia.com/themes/images/category/may-tinh.jpg" alt="Máy tính linh kiện"/>
						</p>
						<span>Máy tính linh kiện</span></a>
					</div>
					<div class="cat_sl_info">
						<a href="/331/dien-tu-dien-may.html" title="Điện tử, điện máy">
						<p>
							<img src="http://static.vatgia.com/themes/images/category/dien-tu.jpg" alt="Điện tử, điện máy"/>
						</p>
						<span>Điện tử, điện máy</span></a>
					</div>
					<div class="cat_sl_info">
						<a href="/500/may-anh-may-quay.html" title="Máy ảnh máy quay">
						<p>
							<img src="http://static.vatgia.com/themes/images/category/may-anh.jpg" alt="Máy ảnh máy quay"/>
						</p>
						<span>Máy ảnh máy quay</span></a>
					</div>
					<div class="cat_sl_info">
						<a href="/3544/o-to-xe-may-xe-dap.html" title="Ô tô, xe máy, xe đạp">
						<p>
							<img src="http://static.vatgia.com/themes/images/category/oto.jpg" alt="Ô tô, xe máy, xe đạp"/>
						</p>
						<span>Ô tô, xe máy, xe đạp</span></a>
					</div>
					<div class="cat_sl_info">
						<a href="/640/thoi-trang-phu-kien.html" title="Thời trang phụ kiện">
						<p>
							<img src="http://static.vatgia.com/themes/images/category/thoi-trang.jpg" alt="Thời trang phụ kiện"/>
						</p>
						<span>Thời trang phụ kiện</span></a>
					</div>
					</li>
					<li class="cat_sl_item next " data-item="1">
					<div class="cat_sl_info">
						<a href="/586/me-be.html" title="Mẹ và bé">
						<p>
							<img src="http://static.vatgia.com/themes/images/category/me-be.jpg" alt="Mẹ và bé"/>
						</p>
						<span>Mẹ và bé</span></a>
					</div>
					<div class="cat_sl_info">
						<a href="/2434/suc-khoe-sac-dep.html" title="Sức khỏe, sắc đẹp">
						<p>
							<img src="http://static.vatgia.com/themes/images/category/suc-khoe.jpg" alt="Sức khỏe, sắc đẹp"/>
						</p>
						<span>Sức khỏe, sắc đẹp</span></a>
					</div>
					<div class="cat_sl_info">
						<a href="/912/do-dung-sinh-hoat.html" title="Đồ dùng sinh hoạt">
						<p>
							<img src="http://static.vatgia.com/themes/images/category/do-dung.jpg" alt="Đồ dùng sinh hoạt"/>
						</p>
						<span>Đồ dùng sinh hoạt</span></a>
					</div>
					<div class="cat_sl_info">
						<a href="/779/noi-that-ngoai-that.html" title="Nội thất, ngoại thất">
						<p>
							<img src="http://static.vatgia.com/themes/images/category/noi-that.jpg" alt="Nội thất, ngoại thất"/>
						</p>
						<span>Nội thất, ngoại thất</span></a>
					</div>
					<div class="cat_sl_info">
						<a href="/923/sach-do-van-phong.html" title="Sách, đồ văn phòng">
						<p>
							<img src="http://static.vatgia.com/themes/images/category/sach.jpg" alt="Sách, đồ văn phòng"/>
						</p>
						<span>Sách, đồ văn phòng</span></a>
					</div>
					<div class="cat_sl_info">
						<a href="/1169/hoa-qua-tang-do-choi.html" title="Hoa, quà tặng, đồ chơi">
						<p>
							<img src="http://static.vatgia.com/themes/images/category/hoa-qua.jpg" alt="Hoa, quà tặng, đồ chơi"/>
						</p>
						<span>Hoa, quà tặng, đồ chơi</span></a>
					</div>
					</li>
					<li class="cat_sl_item pre end " data-item="2">
					<div class="cat_sl_info">
						<a href="/768/cong-nghiep-xay-dung.html" title="Công nghiệp, xây dựng">
						<p>
							<img src="http://static.vatgia.com/themes/images/category/cong-nghiep.jpg" alt="Công nghiệp, xây dựng"/>
						</p>
						<span>Công nghiệp, xây dựng</span></a>
					</div>
					<div class="cat_sl_info">
						<a href="/561/dich-vu-giai-tri-du-lich.html" title="Dịch vụ, giải trí, du lịch">
						<p>
							<img src="http://static.vatgia.com/themes/images/category/dich-vu.jpg" alt="Dịch vụ, giải trí, du lịch"/>
						</p>
						<span>Dịch vụ, giải trí, du lịch</span></a>
					</div>
					<div class="cat_sl_info">
						<a href="/913/thuc-pham-do-uong.html" title="Thực phẩm, đồ uống">
						<p>
							<img src="http://static.vatgia.com/themes/images/category/thuc-pham.jpg" alt="Thực phẩm, đồ uống"/>
						</p>
						<span>Thực phẩm, đồ uống</span></a>
					</div>
					<div class="cat_sl_info">
						<a href="/raovat/2588/bat-dong-san.html" title="Bất động sản">
						<p>
							<img src="http://static.vatgia.com/themes/images/category/bat-dong-san.jpg" alt="Bất động sản"/>
						</p>
						<span>Bất động sản</span></a>
					</div>
					<div class="cat_sl_info">
						<a href="/1280/phu-kien-thiet-bi-sua-chua-phan-mem.html" title="Phụ kiện công nghệ">
						<p>
							<img src="http://static.vatgia.com/themes/images/category/phu-kien.jpg" alt="Phụ kiện công nghệ"/>
						</p>
						<span>Phụ kiện công nghệ</span></a>
					</div>
					<div class="cat_sl_info">
						<a href="/xu-huong-mua-sam-2014/100.shtml" title="Tưng bừng mua sắm">
						<p>
							<img src="http://static.vatgia.com/themes/images/category/tung-bung.jpg" alt="Tưng bừng mua sắm"/>
						</p>
						<span>Tưng bừng mua sắm</span></a>
					</div>
					</li>
				</ul>
				<div class="slide_dot">
					<span class="dot_item active" id="dot_item_0"></span><span class="dot_item" id="dot_item_1"></span><span class="dot_item" id="dot_item_2"></span>
				</div>
			</div>
			<script>$(document).ready(function(){resizeHomeCatSlide();setTimeout(resizeHomeCatSlide,1000);});var myslide = setInterval(function(){nextSlideCat()},5000);$(function () { imgsC = $(".cat_slide"); imgsC.swipe(swipeCatOptions); });$(function(){homeCatSlide()});$(window).resize(function(){resizeHomeCatSlide();})</script>
			<div class="key_suggest">
				<div class="suggest_wrap home_key">
					<div class="suggest_content">
						<a id="sugget_more_bt" class="sugget_more_bt" href="javascript:;" title="xem thêm" onclick="toggleMoreSuggest(this)"><i class="vcon-daquo"></i><i class="vcon-taquo"></i></a>
						<div class="sub_slide">
							<a href="/home/quicksearch.php?keyword=Surface&iCat=0">Surface</a>
						</div>
						<div class="sub_slide">
							<a href="/home/quicksearch.php?keyword=iphone&iCat=0">iphone</a>
						</div>
						<div class="sub_slide">
							<a href="/home/quicksearch.php?keyword=V%C3%A1n+tr%C6%B0%E1%BB%A3t+r%C3%A1p&iCat=0">Ván trượt ráp</a>
						</div>
						<div class="sub_slide">
							<a href="/home/quicksearch.php?keyword=V%C3%A1n+tr%C6%B0%E1%BB%A3t+r%C3%A1p+s%E1%BA%B5n+1&iCat=0">Ván trượt ráp sẵn 1</a>
						</div>
						<div class="sub_slide">
							<a href="/home/quicksearch.php?keyword=m%C3%A1y+%E1%BA%A3nh&iCat=0">máy ảnh</a>
						</div>
						<div class="sub_slide">
							<a href="/home/quicksearch.php?keyword=v%C3%A1y&iCat=0">váy</a>
						</div>
						<div class="sub_slide">
							<a href="/home/quicksearch.php?keyword=%C4%91%E1%BA%A7m+li%E1%BB%81n&iCat=0">đầm liền</a>
						</div>
						<div class="sub_slide">
							<a href="/home/quicksearch.php?keyword=%C4%91%E1%BA%A7m+ng%E1%BA%AFn&iCat=0">đầm ngắn</a>
						</div>
						<div class="sub_slide">
							<a href="/home/quicksearch.php?keyword=Sony+Xperia+Z1+Honami+C6903+LTE+Black++Sony+Xperia+Z1+Honami+C6903+LTE+Black+6.000.000+%C4%91&iCat=0">Sony Xperia Z1 Honami C6903 LTE Black Sony Xperia Z1 Honami C6903 LTE Black 6.000.000 đ</a>
						</div>
						<div class="sub_slide">
							<a href="/home/quicksearch.php?keyword=Apple+iPhone+4S+16GB+White+%28B%E1%BA%A3n+qu%E1%BB%91c+t%E1%BA%BF%29+Apple+iPhone+4S&iCat=0">Apple iPhone 4S 16GB White (Bản quốc tế) Apple iPhone 4S</a>
						</div>
						<div class="sub_slide">
							<a href="/home/quicksearch.php?keyword=V%C3%A1y+fgh677&iCat=0">Váy fgh677</a>
						</div>
						<div class="sub_slide">
							<a href="/home/quicksearch.php?keyword=thi%E1%BA%BFt+b%E1%BB%8B+%C4%91%E1%BB%8Bnh+v%E1%BB%8B&iCat=0">thiết bị định vị</a>
						</div>
						<div class="sub_slide">
							<a href="/home/quicksearch.php?keyword=Apple+iPhone+6+Plus+16GB+Space+Gray+%28B%E1%BA%A3n+qu%E1%BB%91c+t%E1%BA%BF%29&iCat=0">Apple iPhone 6 Plus 16GB Space Gray (Bản quốc tế)</a>
						</div>
						<div class="sub_slide">
							<a href="/home/quicksearch.php?keyword=Apple+iPhone+6+16GB+Space+Gray+%28B%E1%BA%A3n+qu%E1%BB%91c+t%E1%BA%BF%29&iCat=0">Apple iPhone 6 16GB Space Gray (Bản quốc tế)</a>
						</div>
						<div class="sub_slide">
							<a href="/764/man-hinh-lcd.html">Màn hình LCD</a>
						</div>
						<div class="sub_slide">
							<a href="/2088/tour-du-lich-trong-nuoc.html">Tour du lịch Trong nước</a>
						</div>
						<div class="sub_slide">
							<a href="/329/my-pham-cao-cap-trang-diem.html">Mỹ phẩm cao cấp - Trang điểm</a>
						</div>
						<div class="sub_slide">
							<a href="/438/mobile.html">Mobile</a>
						</div>
						<div class="sub_slide">
							<a href="/6605/thuc-an-vat-nuoi.html">Thức ăn vật nuôi</a>
						</div>
						<div class="sub_slide">
							<a href="/1153/do-boi-nu.html">Đồ bơi nữ</a>
						</div>
						<div class="sub_slide">
							<a href="/886/ao-so-mi-nu.html">Áo sơ mi nữ</a>
						</div>
						<div class="sub_slide">
							<a href="/888/vay-dam.html">Váy, đầm</a>
						</div>
						<div class="sub_slide">
							<a href="/1453/cham-soc-me-khi-sinh.html">Chăm sóc mẹ khi sinh</a>
						</div>
						<div class="sub_slide">
							<a href="/7160/giay-bet-nu.html">Giày bệt nữ</a>
						</div>
						<div class="sub_slide">
							<a href="/11135/bo-quan-ao-the-thao-nam.html">Bộ quần áo thể thao nam</a>
						</div>
						<div class="sub_slide">
							<a href="/5199/portable-media-player-pmp.html">Portable Media Player (PMP)</a>
						</div>
						<div class="sub_slide">
							<a href="/1423/may-nghe-nhac-mp4.html">Máy nghe nhạc Mp4</a>
						</div>
						<div class="sub_slide">
							<a href="/896/thoi-trang-nu-khac.html">Thời trang nữ khác</a>
						</div>
						<div class="sub_slide">
							<a href="/893/quan-bo-nu-jeans.html">Quần bò nữ (Jeans)</a>
						</div>
						<div class="sub_slide">
							<a href="/873/ao-thun-nam-ao-phong.html">Áo thun nam (áo phông)</a>
						</div>
						<div class="sub_slide">
							<a href="/1149/giay-the-thao-nu.html">Giày thể thao nữ</a>
						</div>
						<div class="sub_slide">
							<a href="/407/may-anh-so-chuyen-dung.html">Máy ảnh số chuyên dụng</a>
						</div>
						<div class="sub_slide">
							<a href="/743/che.html">Chè</a>
						</div>
						<div class="sub_slide">
							<a href="/4864/van-truot-skateboard.html">Ván trượt - SkateBoard</a>
						</div>
						<div class="sub_slide">
							<a href="/1030/bo-ban-ghe-phong-an-bep.html">Bộ bàn ghế ( phòng ăn, bếp )</a>
						</div>
						<div class="sub_slide">
							<a href="/6761/dich-vu-lap-may.html">Dịch vụ lắp máy</a>
						</div>
						<div class="sub_slide">
							<a href="/1285/bao-dung-op-lung-dien-thoai.html">Bao đựng, ốp lưng điện thoại</a>
						</div>
						<div class="sub_slide">
							<a href="/361/bep-tu.html">Bếp từ</a>
						</div>
						<div class="sub_slide">
							<a href="/1103/thu-nhoi-bong.html">Thú nhồi bông</a>
						</div>
						<div class="sub_slide">
							<a href="/885/ao-thun-nu-ao-pull.html">Áo thun nữ (áo pull)</a>
						</div>
						<div class="sub_slide">
							<a href="/10137/sac-pin-ngoai-cho-dien-thoai.html">Sạc pin ngoài cho điện thoại</a>
						</div>
						<div class="sub_slide">
							<a href="/344/head-phone-tai-nghe.html">Head Phone - Tai nghe</a>
						</div>
						<div class="sub_slide">
							<a href="/317/may-tinh-laptop.html">Máy tính laptop</a>
						</div>
						<div class="sub_slide">
							<a href="/10473/dong-ho-deo-tay-trung-quoc-nu.html">Đồng hồ đeo tay Trung Quốc nữ</a>
						</div>
					</div>
				</div>
			</div>
			<div style="clear: both;">
			</div>
			<div style="clear: both;">
			</div>
			<div class="home_filter_tab home_product_filter_tab">
				<table cellpadding="0" cellspacing="0">
				<tr>
					<td class="tab_slide active" data-index="1" onclick="trackEvent('Tab HOT','Click','Trang chủ');changeTabProduct(1)">
						Hot nhất
					</td>
					<td class="tab_slide " data-index="2" onclick="trackEvent('Tab Hot Friday','Click','Trang chủ');changeTabProduct(2)">
						<div class="sprite_text">
							KHUYẾN MẠI SỐC
						</div>
					</td>
					<td class="tab_slide " data-index="3" onclick="trackEvent('Tab Mới','Click','Trang chủ');changeTabProduct(3)">
						Mới nhất
					</td>
					<td class="tab_slide " data-index="4" onclick="trackEvent('Tab Đã xem','Click','Trang chủ');changeTabProduct(4)">
						Đã xem
					</td>
				</tr>
				</table>
			</div>
			<div class="home_product_bound no_swipe_index">
				<div class="home_product_tab">
					<span class="home_tab_box" id="tab_index_0">
					<div class="home_box_show">
						<div class="home_pro_item">
							<a onclick="trackEvent('Sản phẩm HOT','Click','Trang chủ');" href="/764/1253595/samsung-syncmaster-s19a300n-18-5-inch.html" title="Samsung SyncMaster S19A300N 18.5 inch" class="detail_home_product_item">
							<div class="h_picture product_picture home_product_picture_new">
								<img src="http://media.vatgia.vn/pictures_medium/small_odr1301022108.jpg" alt="Samsung SyncMaster S19A300N 18.5 inch"/>
							</div>
							<div class="h_p_info">
								<div class="h_info_text">
									<div class="price">
										1.950.000₫
									</div>
									<div class="h_name no_bold">
										Samsung SyncMaster S19A300N 18.5 inch
									</div>
								</div>
							</div>
							</a>
						</div>
						<div class="home_pro_item">
							<a onclick="trackEvent('Sản phẩm HOT','Click','Trang chủ');" href="/764/1054345/asus-vw197t-18-5-inch.html" title="ASUS VW197T 18.5 inch" class="detail_home_product_item">
							<div class="h_picture product_picture home_product_picture_new">
								<img src="http://media.vatgia.vn/pictures_medium/small_ktw1291103441.jpg" alt="ASUS VW197T 18.5 inch"/>
							</div>
							<div class="h_p_info">
								<div class="h_info_text">
									<div class="price">
										1.989.000₫
									</div>
									<div class="h_name no_bold">
										ASUS VW197T 18.5 inch
									</div>
								</div>
							</div>
							</a>
						</div>
						<div class="home_pro_item">
							<a onclick="trackEvent('Sản phẩm HOT','Click','Trang chủ');" href="/764/307611/acer-v173bdm.html" title="Acer V173bdm" class="detail_home_product_item">
							<div class="h_picture product_picture home_product_picture_new">
								<img src="http://media.vatgia.vn/pictures_medium/small_pnc1230282400.jpg" alt="Acer V173bdm"/>
							</div>
							<div class="h_p_info">
								<div class="h_info_text">
									<div class="price">
										1.950.300₫
									</div>
									<div class="h_name no_bold">
										Acer V173bdm
									</div>
								</div>
							</div>
							</a>
						</div>
						<div class="home_pro_item">
							<a onclick="trackEvent('Sản phẩm HOT','Click','Trang chủ');" href="/764/4611285/acer-k202hql-19-5inch.html" title="Acer K202HQL 19.5inch" class="detail_home_product_item">
							<div class="h_picture product_picture home_product_picture_new">
								<img src="http://media.vatgia.vn/pictures_medium/small_cyg1415334730.png" alt="Acer K202HQL 19.5inch"/>
							</div>
							<div class="h_p_info">
								<div class="h_info_text">
									<div class="price">
										2.050.000₫
									</div>
									<div class="h_name no_bold">
										Acer K202HQL 19.5inch
									</div>
								</div>
							</div>
							</a>
						</div>
						<div class="home_pro_item">
							<a onclick="trackEvent('Sản phẩm HOT','Click','Trang chủ');" href="/764/3426747/dell-e1914h-18-5-inch.html" title="Dell E1914H 18.5 inch" class="detail_home_product_item">
							<div class="h_picture product_picture home_product_picture_new">
								<img src="http://media.vatgia.vn/pictures_medium/small_uym1379911161.jpg" alt="Dell E1914H 18.5 inch"/>
							</div>
							<div class="h_p_info">
								<div class="h_info_text">
									<div class="price">
										2.000.000₫
									</div>
									<div class="h_name no_bold">
										Dell E1914H 18.5 inch
									</div>
								</div>
							</div>
							</a>
						</div>
						<div class="home_pro_item">
							<a onclick="trackEvent('Sản phẩm HOT','Click','Trang chủ');" href="/764/3658704/dell-1914h-19inch.html" title="Dell 1914H 19inch " class="detail_home_product_item">
							<div class="h_picture product_picture home_product_picture_new">
								<img src="http://media.vatgia.vn/pictures_medium/small_wxl1386660255.jpg" alt="Dell 1914H 19inch "/>
							</div>
							<div class="h_p_info">
								<div class="h_info_text">
									<div class="price">
										2.000.000₫
									</div>
									<div class="h_name no_bold">
										Dell 1914H 19inch
									</div>
								</div>
							</div>
							</a>
						</div>
						<div class="home_pro_item">
							<a onclick="trackEvent('Sản phẩm HOT','Click','Trang chủ');" href="/2088/1291486/tour-du-l%E1%BB%8Bch-h%C3%A0-n%E1%BB%99i-s%E1%BA%A7m-s%C6%A1n-2-ng%C3%A0y-1-%C4%91%C3%AAm.html" title="Tour du lịch Hà Nội - Sầm Sơn 2 ngày 1 đêm" class="detail_home_product_item">
							<div class="h_picture product_picture home_product_picture_new">
								<img src="http://media.vatgia.vn/pictures_medium/small_cut1335918080.jpg" alt="Tour du lịch Hà Nội - Sầm Sơn 2 ngày 1 đêm"/>
							</div>
							<div class="h_p_info">
								<div class="h_info_text">
									<div class="price">
										900.000₫
									</div>
									<div class="h_name no_bold">
										Tour du lịch Hà Nội - Sầm Sơn 2 ngày 1 đêm
									</div>
								</div>
							</div>
							</a>
						</div>
						<div class="home_pro_item">
							<a onclick="trackEvent('Sản phẩm HOT','Click','Trang chủ');" href="/2088/530222/tour-du-l%E1%BB%8Bch-h%C3%A0-n%E1%BB%99i-c%E1%BB%ADa-l%C3%B2-qu%C3%AA-b%C3%A1c-3-ng%C3%A0y-2-%C4%91%C3%AAm.html" title=" Tour du lịch Hà Nội - Cửa Lò - Quê Bác (3 ngày 2..." class="detail_home_product_item">
							<div class="h_picture product_picture home_product_picture_new">
								<img src="http://media.vatgia.vn/pictures_medium/small_mzg1259550833.jpg" alt=" Tour du lịch Hà Nội - Cửa Lò - Quê Bác (3 ngày 2..."/>
							</div>
							<div class="h_p_info">
								<div class="h_info_text">
									<div class="price">
										1.056.000₫
									</div>
									<div class="h_name no_bold">
										 Tour du lịch Hà Nội - Cửa Lò - Quê Bác (3 ngày 2...
									</div>
								</div>
							</div>
							</a>
						</div>
						<div class="home_pro_item">
							<a onclick="trackEvent('Sản phẩm HOT','Click','Trang chủ');" href="/2088/486268/tp-h%E1%BB%93-ch%C3%AD-minh-c%E1%BA%A7n-th%C6%A1-h%E1%BA%ADu-giang.html" title="TP Hồ Chí Minh- Cần Thơ - Hậu Giang" class="detail_home_product_item">
							<div class="h_picture product_picture home_product_picture_new">
								<img src="http://media.vatgia.vn/pictures_medium/small_edd1256003927.png" alt="TP Hồ Chí Minh- Cần Thơ - Hậu Giang"/>
							</div>
							<div class="h_p_info">
								<div class="h_info_text">
									<div class="price">
										890.000₫
									</div>
									<div class="h_name no_bold">
										TP Hồ Chí Minh- Cần Thơ - Hậu Giang
									</div>
								</div>
							</div>
							</a>
						</div>
						<div class="home_pro_item">
							<a onclick="trackEvent('Sản phẩm HOT','Click','Trang chủ');" href="/2088/422356/tp-h%E1%BB%93-ch%C3%AD-minh-phan-thi%E1%BA%BFt-s%C3%A0i-g%C3%B2n-2-ng%C3%A0y.html" title="TP Hồ Chí Minh- Phan Thiết- Sài Gòn 2 ngày" class="detail_home_product_item">
							<div class="h_picture product_picture home_product_picture_new">
								<img src="http://media.vatgia.vn/pictures_medium/small_iuz1248942728.jpg" alt="TP Hồ Chí Minh- Phan Thiết- Sài Gòn 2 ngày"/>
							</div>
							<div class="h_p_info">
								<div class="h_info_text">
									<div class="price">
										959.000₫
									</div>
									<div class="h_name no_bold">
										TP Hồ Chí Minh- Phan Thiết- Sài Gòn 2 ngày
									</div>
								</div>
							</div>
							</a>
						</div>
						<div class="home_pro_item">
							<a onclick="trackEvent('Sản phẩm HOT','Click','Trang chủ');" href="/2088/1706317/h%C3%A0-n%E1%BB%99i-h%E1%BA%A1-long-%C4%91%E1%BA%A3o-c%C3%A1t-b%C3%A0-h%C3%A0-n%E1%BB%99i-3-ng%C3%A0y-2-%C4%91%C3%AAm.html" title="Hà Nội - Hạ Long - Đảo Cát Bà - Hà Nội 3 ngày 2..." class="detail_home_product_item">
							<div class="h_picture product_picture home_product_picture_new">
								<img src="http://media.vatgia.vn/pictures_medium/small_xrw1332141737.jpg" alt="Hà Nội - Hạ Long - Đảo Cát Bà - Hà Nội 3 ngày 2..."/>
							</div>
							<div class="h_p_info">
								<div class="h_info_text">
									<div class="price">
										997.000₫
									</div>
									<div class="h_name no_bold">
										Hà Nội - Hạ Long - Đảo Cát Bà - Hà Nội 3 ngày 2...
									</div>
								</div>
							</div>
							</a>
						</div>
						<div class="home_pro_item">
							<a onclick="trackEvent('Sản phẩm HOT','Click','Trang chủ');" href="/2088/1711518/h%C3%A0-n%E1%BB%99i-h%E1%BA%A1-long-2-ng%C3%A0y-1-%C4%91%C3%AAm.html" title="Hà Nội - Hạ Long 2 ngày 1 đêm" class="detail_home_product_item">
							<div class="h_picture product_picture home_product_picture_new">
								<img src="http://media.vatgia.vn/pictures_medium/small_fip1320640474.jpg" alt="Hà Nội - Hạ Long 2 ngày 1 đêm"/>
							</div>
							<div class="h_p_info">
								<div class="h_info_text">
									<div class="price">
										999.000₫
									</div>
									<div class="h_name no_bold">
										Hà Nội - Hạ Long 2 ngày 1 đêm
									</div>
								</div>
							</div>
							</a>
						</div>
					</div>
					</span><span class="home_tab_box" id="tab_index_1">
					<div class="home_box_show">
						<div class="home_pro_item">
							<a onclick="trackEvent('Sản phẩm Hot Friday','Click','Trang chủ');" href="/noithatdaiphat&module=product&view=detail&record_id=4262805" title="Tủ nhựa Đài Loan 3 cánh màu xanh dương T-805" class="detail_home_product_item">
							<div class="h_picture product_picture home_product_picture_new">
								<img src="http://media.vatgia.vn/pictures_medium/small_oba1405767466.jpg" alt="Tủ nhựa Đài Loan 3 cánh màu xanh dương T-805"/>
								<div class='promotion_fire_sale vcon-sale_fire'>
									<div class='percent_value'>
										<span class='minus'>- </span>20<sup class='percent_text'>%</sup>
									</div>
								</div>
							</div>
							<div class="h_p_info">
								<div class="h_info_text">
									<div class="price">
										2.640.000 ₫
									</div>
									<div class="old_price">
										3.300.000 ₫
									</div>
									<div class="h_name no_bold">
										Tủ nhựa Đài Loan 3 cánh màu xanh dương T-805
									</div>
								</div>
							</div>
							</a>
						</div>
						<div class="home_pro_item">
							<a onclick="trackEvent('Sản phẩm Hot Friday','Click','Trang chủ');" href="/hoaixuan&module=product&view=detail&record_id=5043978" title="Đồ bộ dài mặc nhà cotton hàng quảng châu hình thỏ..." class="detail_home_product_item">
							<div class="h_picture product_picture home_product_picture_new">
								<img src="http://media.vatgia.vn/pictures_medium/small_mjr1430469883.jpg" alt="Đồ bộ dài mặc nhà cotton hàng quảng châu hình thỏ..."/>
								<div class='promotion_fire_sale vcon-sale_fire'>
									<div class='percent_value'>
										<span class='minus'>- </span>10<sup class='percent_text'>%</sup>
									</div>
								</div>
							</div>
							<div class="h_p_info">
								<div class="h_info_text">
									<div class="price">
										323.100 ₫
									</div>
									<div class="old_price">
										359.000 ₫
									</div>
									<div class="h_name no_bold">
										Đồ bộ dài mặc nhà cotton hàng quảng châu hình thỏ...
									</div>
								</div>
							</div>
							</a>
						</div>
						<div class="home_pro_item">
							<a onclick="trackEvent('Sản phẩm Hot Friday','Click','Trang chủ');" href="/sieuthialo24h&module=product&view=detail&record_id=5019545" title="Bộ ghép vần Kiwi Anh ngữ Antona024CTG" class="detail_home_product_item">
							<div class="h_picture product_picture home_product_picture_new">
								<img src="http://media.vatgia.vn/pictures_medium/small_oev1429612039.jpg" alt="Bộ ghép vần Kiwi Anh ngữ Antona024CTG"/>
								<div class='promotion_fire_sale vcon-sale_fire'>
									<div class='percent_value'>
										<span class='minus'>- </span>10<sup class='percent_text'>%</sup>
									</div>
								</div>
							</div>
							<div class="h_p_info">
								<div class="h_info_text">
									<div class="price">
										153.000 ₫
									</div>
									<div class="old_price">
										170.000 ₫
									</div>
									<div class="h_name no_bold">
										Bộ ghép vần Kiwi Anh ngữ Antona024CTG
									</div>
								</div>
							</div>
							</a>
						</div>
						<div class="home_pro_item">
							<a onclick="trackEvent('Sản phẩm Hot Friday','Click','Trang chủ');" href="/mayvanphongatm&module=product&view=detail&record_id=327593" title="Màn 3 chân DALITE 84x84 inch (2.2x2.2m)" class="detail_home_product_item">
							<div class="h_picture product_picture home_product_picture_new">
								<img src="http://media.vatgia.vn/pictures_medium/small_tyd1235009429.jpg" alt="Màn 3 chân DALITE 84x84 inch (2.2x2.2m)"/>
								<div class='promotion_fire_sale vcon-sale_fire'>
									<div class='percent_value'>
										<span class='minus'>- </span>5<sup class='percent_text'>%</sup>
									</div>
								</div>
							</div>
							<div class="h_p_info">
								<div class="h_info_text">
									<div class="price">
										997.500 ₫
									</div>
									<div class="old_price">
										1.050.000 ₫
									</div>
									<div class="h_name no_bold">
										Màn 3 chân DALITE 84x84 inch (2.2x2.2m)
									</div>
								</div>
							</div>
							</a>
						</div>
						<div class="home_pro_item">
							<a onclick="trackEvent('Sản phẩm Hot Friday','Click','Trang chủ');" href="/NewCen&module=product&view=detail&record_id=4927238" title="Máy đọc mã vạch Opticon OPI3301" class="detail_home_product_item">
							<div class="h_picture product_picture home_product_picture_new">
								<img src="http://media.vatgia.vn/pictures_medium/small_qtd1426837089.jpg" alt="Máy đọc mã vạch Opticon OPI3301"/>
								<div class='promotion_fire_sale vcon-sale_fire'>
									<div class='percent_value'>
										<span class='minus'>- </span>5<sup class='percent_text'>%</sup>
									</div>
								</div>
							</div>
							<div class="h_p_info">
								<div class="h_info_text">
									<div class="price">
										8.597.500 ₫
									</div>
									<div class="old_price">
										9.050.000 ₫
									</div>
									<div class="h_name no_bold">
										Máy đọc mã vạch Opticon OPI3301
									</div>
								</div>
							</div>
							</a>
						</div>
						<div class="home_pro_item">
							<a onclick="trackEvent('Sản phẩm Hot Friday','Click','Trang chủ');" href="/mun16487&module=product&view=detail&record_id=4664301" title="Son dưỡng môi pure line 3IN1" class="detail_home_product_item">
							<div class="h_picture product_picture home_product_picture_new">
								<img src="http://media.vatgia.vn/pictures_medium/small_xjr1416966973.jpg" alt="Son dưỡng môi pure line 3IN1"/>
								<div class='promotion_fire_sale vcon-sale_fire'>
									<div class='hund_text'>
										<span class='minus'>- </span>5<sup class='percent_text'>k</sup>
									</div>
								</div>
							</div>
							<div class="h_p_info">
								<div class="h_info_text">
									<div class="price">
										30.000 ₫
									</div>
									<div class="old_price">
										35.000 ₫
									</div>
									<div class="h_name no_bold">
										Son dưỡng môi pure line 3IN1
									</div>
								</div>
							</div>
							</a>
						</div>
					</div>
					</span><span class="home_tab_box" id="tab_index_2">
					<div class="home_box_show">
						<div class="home_pro_item">
							<a onclick="trackEvent('Sản phẩm Mới','Click','Trang chủ');" href="/764/4724412/hp-compaq-f191-18-5inch-led.html" title="HP Compaq F191 18.5inch LED " class="detail_home_product_item">
							<div class="h_picture product_picture home_product_picture_new">
								<img src="http://media.vatgia.vn/pictures_medium/small_ppd1419042214.png" alt="HP Compaq F191 18.5inch LED "/>
							</div>
							<div class="h_p_info">
								<div class="h_info_text">
									<div class="price">
										1.790.000₫
									</div>
									<div class="h_name no_bold">
										HP Compaq F191 18.5inch LED
									</div>
								</div>
							</div>
							</a>
						</div>
						<div class="home_pro_item">
							<a onclick="trackEvent('Sản phẩm Mới','Click','Trang chủ');" href="/764/1314623/lcd-dell-17inch-untrasharp-e1703fp.html" title="LCD Dell 17inch untrasharp E1703fp" class="detail_home_product_item">
							<div class="h_picture product_picture home_product_picture_new">
								<img src="http://media.vatgia.vn/pictures_medium/small_kay1303907931.jpg" alt="LCD Dell 17inch untrasharp E1703fp"/>
							</div>
							<div class="h_p_info">
								<div class="h_info_text">
									<div class="price">
										750.000₫
									</div>
									<div class="h_name no_bold">
										LCD Dell 17inch untrasharp E1703fp
									</div>
								</div>
							</div>
							</a>
						</div>
						<div class="home_pro_item">
							<a onclick="trackEvent('Sản phẩm Mới','Click','Trang chủ');" href="/764/3411088/acer-g206hql-19-5-inch.html" title="Acer G206HQL 19.5 Inch" class="detail_home_product_item">
							<div class="h_picture product_picture home_product_picture_new">
								<img src="http://media.vatgia.vn/pictures_medium/small_yuq1379400613.jpg" alt="Acer G206HQL 19.5 Inch"/>
							</div>
							<div class="h_p_info">
								<div class="h_info_text">
									<div class="price">
										1.900.000₫
									</div>
									<div class="h_name no_bold">
										Acer G206HQL 19.5 Inch
									</div>
								</div>
							</div>
							</a>
						</div>
						<div class="home_pro_item">
							<a onclick="trackEvent('Sản phẩm Mới','Click','Trang chủ');" href="/2088/1291486/tour-du-l%E1%BB%8Bch-h%C3%A0-n%E1%BB%99i-s%E1%BA%A7m-s%C6%A1n-2-ng%C3%A0y-1-%C4%91%C3%AAm.html" title="Tour du lịch Hà Nội - Sầm Sơn 2 ngày 1 đêm" class="detail_home_product_item">
							<div class="h_picture product_picture home_product_picture_new">
								<img src="http://media.vatgia.vn/pictures_medium/small_cut1335918080.jpg" alt="Tour du lịch Hà Nội - Sầm Sơn 2 ngày 1 đêm"/>
							</div>
							<div class="h_p_info">
								<div class="h_info_text">
									<div class="price">
										900.000₫
									</div>
									<div class="h_name no_bold">
										Tour du lịch Hà Nội - Sầm Sơn 2 ngày 1 đêm
									</div>
								</div>
							</div>
							</a>
						</div>
						<div class="home_pro_item">
							<a onclick="trackEvent('Sản phẩm Mới','Click','Trang chủ');" href="/438/3844597/sony-xperia-z2-sirius-d6503-black.html" title="Sony Xperia Z2 Sirius D6503 Black" class="detail_home_product_item">
							<div class="h_picture product_picture home_product_picture_new">
								<img src="http://media.vatgia.vn/pictures_medium/small_jyg1393311149.jpg" alt="Sony Xperia Z2 Sirius D6503 Black"/>
							</div>
							<div class="h_p_info">
								<div class="h_info_text">
									<div class="price">
										8.000.000₫
									</div>
									<div class="h_name no_bold">
										Sony Xperia Z2 Sirius D6503 Black
									</div>
								</div>
							</div>
							</a>
						</div>
						<div class="home_pro_item">
							<a onclick="trackEvent('Sản phẩm Mới','Click','Trang chủ');" href="/438/5061808/titan-q7-plus.html" title="Titan Q7 Plus" class="detail_home_product_item">
							<div class="h_picture product_picture home_product_picture_new">
								<img src="http://media.vatgia.vn/pictures_medium/small_fll1431400322.jpg" alt="Titan Q7 Plus"/>
							</div>
							<div class="h_p_info">
								<div class="h_info_text">
									<div class="price">
										5.400.000₫
									</div>
									<div class="h_name no_bold">
										Titan Q7 Plus
									</div>
								</div>
							</div>
							</a>
						</div>
						<div class="home_pro_item">
							<a onclick="trackEvent('Sản phẩm Mới','Click','Trang chủ');" href="/438/3090801/nokia-lumia-625-orange.html" title="Nokia Lumia 625 Orange" class="detail_home_product_item">
							<div class="h_picture product_picture home_product_picture_new">
								<img src="http://media.vatgia.vn/pictures_medium/small_whe1378528723.jpg" alt="Nokia Lumia 625 Orange"/>
							</div>
							<div class="h_p_info">
								<div class="h_info_text">
									<div class="price">
										1.800.000₫
									</div>
									<div class="h_name no_bold">
										Nokia Lumia 625 Orange
									</div>
								</div>
							</div>
							</a>
						</div>
						<div class="home_pro_item">
							<a onclick="trackEvent('Sản phẩm Mới','Click','Trang chủ');" href="/438/4885165/microsoft-lumia-640-xl-dual-sim-black.html" title="Microsoft Lumia 640 XL Dual SIM Black" class="detail_home_product_item">
							<div class="h_picture product_picture home_product_picture_new">
								<img src="http://media.vatgia.vn/pictures_medium/small_yux1426841372.jpg" alt="Microsoft Lumia 640 XL Dual SIM Black"/>
							</div>
							<div class="h_p_info">
								<div class="h_info_text">
									<div class="price">
										4.160.000₫
									</div>
									<div class="h_name no_bold">
										Microsoft Lumia 640 XL Dual SIM Black
									</div>
								</div>
							</div>
							</a>
						</div>
						<div class="home_pro_item">
							<a onclick="trackEvent('Sản phẩm Mới','Click','Trang chủ');" href="/438/2988021/pantech-sky-vega-im-a860-vega-no-6-black.html" title="Pantech Sky VEGA IM-A860 (Vega No 6) Black" class="detail_home_product_item">
							<div class="h_picture product_picture home_product_picture_new">
								<img src="http://media.vatgia.vn/pictures_medium/small_lhl1364789744.jpg" alt="Pantech Sky VEGA IM-A860 (Vega No 6) Black"/>
							</div>
							<div class="h_p_info">
								<div class="h_info_text">
									<div class="price">
										2.640.000₫
									</div>
									<div class="h_name no_bold">
										Pantech Sky VEGA IM-A860 (Vega No 6) Black
									</div>
								</div>
							</div>
							</a>
						</div>
						<div class="home_pro_item">
							<a onclick="trackEvent('Sản phẩm Mới','Click','Trang chủ');" href="/438/542647/nokia-6700-classic-gold-edition.html" title="Nokia 6700 Classic Gold Edition" class="detail_home_product_item">
							<div class="h_picture product_picture home_product_picture_new">
								<img src="http://media.vatgia.vn/pictures_medium/small_wyp1260409414.jpg" alt="Nokia 6700 Classic Gold Edition"/>
							</div>
							<div class="h_p_info">
								<div class="h_info_text">
									<div class="price">
										2.800.000₫
									</div>
									<div class="h_name no_bold">
										Nokia 6700 Classic Gold Edition
									</div>
								</div>
							</div>
							</a>
						</div>
						<div class="home_pro_item">
							<a onclick="trackEvent('Sản phẩm Mới','Click','Trang chủ');" href="/438/3391829/pantech-sky-vega-iron-a870s-black.html" title="Pantech Sky Vega Iron A870S Black" class="detail_home_product_item">
							<div class="h_picture product_picture home_product_picture_new">
								<img src="http://media.vatgia.vn/pictures_medium/small_qrk1378786915.jpg" alt="Pantech Sky Vega Iron A870S Black"/>
							</div>
							<div class="h_p_info">
								<div class="h_info_text">
									<div class="price">
										3.000.000₫
									</div>
									<div class="h_name no_bold">
										Pantech Sky Vega Iron A870S Black
									</div>
								</div>
							</div>
							</a>
						</div>
						<div class="home_pro_item">
							<a onclick="trackEvent('Sản phẩm Mới','Click','Trang chủ');" href="/438/2402889/apple-iphone-5-16gb-black-b%E1%BA%A3n-qu%E1%BB%91c-t%E1%BA%BF.html" title="Apple iPhone 5 16GB Black (Bản quốc tế)" class="detail_home_product_item">
							<div class="h_picture product_picture home_product_picture_new">
								<img src="http://media.vatgia.vn/pictures_medium/small_uaa1423936824.jpg" alt="Apple iPhone 5 16GB Black (Bản quốc tế)"/>
							</div>
							<div class="h_p_info">
								<div class="h_info_text">
									<div class="price">
										5.099.000₫
									</div>
									<div class="h_name no_bold">
										Apple iPhone 5 16GB Black (Bản quốc tế)
									</div>
								</div>
							</div>
							</a>
						</div>
					</div>
					</span><span class="home_tab_box" id="tab_index_3">
					<div class="home_box_show">
						<div class="home_pro_item">
							<a onclick="trackEvent('Sản phẩm Đã xem','Click','Trang chủ');" href="/764/4675508/samsung-ls20d300hym-19-5-inch.html" title="Samsung LS20D300HYM 19.5 inch" class="detail_home_product_item">
							<div class="h_picture product_picture home_product_picture_new">
								<img src="http://media.vatgia.vn/pictures_medium/small_ktg1417224802.jpg" alt="Samsung LS20D300HYM 19.5 inch"/>
							</div>
							<div class="h_p_info">
								<div class="h_info_text">
									<div class="price">
										2.150.000₫
									</div>
									<div class="h_name no_bold">
										Samsung LS20D300HYM 19.5 inch
									</div>
								</div>
							</div>
							</a>
						</div>
						<div class="home_pro_item">
							<a onclick="trackEvent('Sản phẩm Đã xem','Click','Trang chủ');" href="/2088/449909/gi%E1%BA%A3i-b%C3%B3ng-%C4%91%C3%A1-b%C3%A3i-bi%E1%BB%83n-2-ng%C3%A0y.html" title="Giải bóng đá bãi biển 2 ngày" class="detail_home_product_item">
							<div class="h_picture product_picture home_product_picture_new">
								<img src="http://media.vatgia.vn/pictures_medium/small_hzo1252549027.jpg" alt="Giải bóng đá bãi biển 2 ngày"/>
							</div>
							<div class="h_p_info">
								<div class="h_info_text">
									<div class="price">
										980.000₫
									</div>
									<div class="h_name no_bold">
										Giải bóng đá bãi biển 2 ngày
									</div>
								</div>
							</div>
							</a>
						</div>
						<div class="home_pro_item">
							<a onclick="trackEvent('Sản phẩm Đã xem','Click','Trang chủ');" href="/329/55759/5-color-eyeshadow-no-760-patchworkmania-m%C3%A0u-m%E1%BA%AFt-5-trong-1.html" title="5 Color Eyeshadow - No. 760 Patchworkmania-Màu mắt..." class="detail_home_product_item">
							<div class="h_picture product_picture home_product_picture_new">
								<img src="http://media.vatgia.vn/pictures_medium/small_52425011.jpg" alt="5 Color Eyeshadow - No. 760 Patchworkmania-Màu mắt..."/>
							</div>
							<div class="h_p_info">
								<div class="h_info_text">
									<div class="price">
										Liên hệ gian hàng...
									</div>
									<div class="h_name no_bold">
										5 Color Eyeshadow - No. 760 Patchworkmania-Màu mắt...
									</div>
								</div>
							</div>
							</a>
						</div>
						<div class="home_pro_item">
							<a onclick="trackEvent('Sản phẩm Đã xem','Click','Trang chủ');" href="/438/2495756/apple-iphone-5-16gb-white-b%E1%BA%A3n-qu%E1%BB%91c-t%E1%BA%BF.html" title="Apple iPhone 5 16GB White (Bản quốc tế)" class="detail_home_product_item">
							<div class="h_picture product_picture home_product_picture_new">
								<img src="http://media.vatgia.vn/pictures_medium/small_rot1423936799.jpg" alt="Apple iPhone 5 16GB White (Bản quốc tế)"/>
							</div>
							<div class="h_p_info">
								<div class="h_info_text">
									<div class="price">
										5.400.000₫
									</div>
									<div class="h_name no_bold">
										Apple iPhone 5 16GB White (Bản quốc tế)
									</div>
								</div>
							</div>
							</a>
						</div>
						<div class="home_pro_item">
							<a onclick="trackEvent('Sản phẩm Đã xem','Click','Trang chủ');" href="/438/3000893/apple-iphone-5s-16gb-white-silver-b%E1%BA%A3n-qu%E1%BB%91c-t%E1%BA%BF.html" title="Apple iPhone 5S 16GB White/Silver (Bản quốc tế)" class="detail_home_product_item">
							<div class="h_picture product_picture home_product_picture_new">
								<img src="http://media.vatgia.vn/pictures_medium/small_gfo1423937521.jpg" alt="Apple iPhone 5S 16GB White/Silver (Bản quốc tế)"/>
							</div>
							<div class="h_p_info">
								<div class="h_info_text">
									<div class="price">
										7.368.000₫
									</div>
									<div class="h_name no_bold">
										Apple iPhone 5S 16GB White/Silver (Bản quốc tế)
									</div>
								</div>
							</div>
							</a>
						</div>
						<div class="home_pro_item">
							<a onclick="trackEvent('Sản phẩm Đã xem','Click','Trang chủ');" href="/438/2402889/apple-iphone-5-16gb-black-b%E1%BA%A3n-qu%E1%BB%91c-t%E1%BA%BF.html" title="Apple iPhone 5 16GB Black (Bản quốc tế)" class="detail_home_product_item">
							<div class="h_picture product_picture home_product_picture_new">
								<img src="http://media.vatgia.vn/pictures_medium/small_uaa1423936824.jpg" alt="Apple iPhone 5 16GB Black (Bản quốc tế)"/>
							</div>
							<div class="h_p_info">
								<div class="h_info_text">
									<div class="price">
										5.500.000₫
									</div>
									<div class="h_name no_bold">
										Apple iPhone 5 16GB Black (Bản quốc tế)
									</div>
								</div>
							</div>
							</a>
						</div>
					</div>
					</span>
				</div>
			</div>
			<script>$(document).ready(function(){homeTabProductSwipe();});</script>
			<div class="home_product home_news_title">
				Tin tức mới nhất
			</div>
			<div class="home_news">
				<a class="list_news" href="/np/cellphones-len-tieng-vu-bi-khach-hang-to-tu-choi-bao-hanh-iphone-5s/23457483.html" title="CellphoneS lên tiếng vụ bị khách hàng “tố” từ chối bảo hành iPhone 5S">
				<div class="news_title">
					CellphoneS lên tiếng vụ bị khách hàng “tố” từ chối bảo hành iPhone 5S
				</div>
				<div class="news_image">
					<img src="http://img3.vatgia.vn/pictures/picsmall/2015/05/04/200/olu1430720831.jpg" alt="CellphoneS lên tiếng vụ bị khách hàng “tố” từ chối bảo hành iPhone 5S"/>
				</div>
				<p class="news_teaser">
					– Đại diện hệ thống CellphoneS cho hay việc hệ thống bán lẻ điện thoại này từ chối bảo hành chiếc iPhone 5S của khách hàng “Lương Minh Lợi” (TP.HCM) trong ngày 30/4 vừa qua là do phát hiện khách hàng này đã có hành vi lừa đảo, thay main hỏng vào máy vừa mua để chiếm đoạt tài sản.
				</p>
				</a><a class="list_news" href="/np/asus-ra-mat-transformer-book-chi-t300-sieu-mong-thoi-trang-an-tuong/23471330.html" title="ASUS ra mắt Transformer Book Chi T300: Siêu mỏng, thời trang, ấn tượng">
				<div class="news_title">
					ASUS ra mắt Transformer Book Chi T300: Siêu mỏng, thời trang, ấn tượng
				</div>
				<div class="news_image">
					<img src="http://img3.vatgia.vn/pictures/picsmall/2015/05/12/200/jih1431427423.jpg" alt="ASUS ra mắt Transformer Book Chi T300: Siêu mỏng, thời trang, ấn tượng"/>
				</div>
				<p class="news_teaser">
					 ASUS Transformer Book T300 Chi là chiếc laptop 2 trong 1 cao cấp với thiết kế có khả năng tháo rời. Đây cũng là mẫu máy tính bảng chạy Windows 8.1 với màn hình 12.5” mỏng nhất thế giới tại Việt Nam. Transformer Book T300 Chi là thiết bị 2-trong-1 hiện đại, đa năng cho những người dùng di động chuyên nghiệp để làm việc & tận hưởng cuộc sống. Máy sở hữu thiết kế nhôm nguyên khối với bản lề từ tính & màn hình IPS Full HD với độ sáng 400nit, cùng với đó là bộ vi xử lý thế hệ mới Intel® ...
				</p>
				</a><a class="list_news" href="/np/camera-tren-smartphone-cang-nhieu-cham-cang-tot/23454821.html" title="Camera trên smartphone: càng nhiều “chấm” càng tốt?">
				<div class="news_title">
					Camera trên smartphone: càng nhiều “chấm” càng tốt?
				</div>
				<div class="news_image">
					<img src="http://img3.vatgia.vn/pictures/picsmall/2015/05/02/200/gan1430531796.png" alt="Camera trên smartphone: càng nhiều “chấm” càng tốt?"/>
				</div>
				<p class="news_teaser">
					– Chiếc iPhone 6 được trang bị camera chỉ 8MP trong khi chiếc Galaxy S6 có camera lên tới 16 MP nhưng ảnh chụp từ hai thiết bị này có chất lượng tương đương? Vậy có phải càng nhiều “chấm” camera càng tốt?
				</p>
				</a><a class="list_news" href="/np/mot-so-quot-bi-kip-quot-su-dung-google-search-hieu-qua-ma-khong-phai-ai-cung-biet/23463872.html" title="Một số &quot;bí kíp&quot; sử dụng Google Search hiệu quả mà không phải ai cũng biết">
				<div class="news_title">
					Một số &quot;bí kíp&quot; sử dụng Google Search hiệu quả mà không phải ai cũng biết
				</div>
				<div class="news_image">
					<img src="http://img3.vatgia.vn/pictures/picsmall/2015/05/08/200/ykq1431021172.jpg" alt="Một số &quot;bí kíp&quot; sử dụng Google Search hiệu quả mà không phải ai cũng biết"/>
				</div>
				<p class="news_teaser">
					 Có thể nói công cụ tìm kiếm Google là một trong những công cụ tìm kiếm miễn phí nhanh và hiệu quả nhất hiện nay. Nguồn thông tin trên Internet gần như là vô tận, do đó, Google đã tích hợp sẵn một số tính năng hữu ích giúp người dùng phân loại, tìm kiếm thông tin dễ dàng hơn . Sau đây là một số thủ thuật hữu ích có thể giúp các bạn dễ dàng hơn trong việc tìm kiếm Internet thông qua Google: 1. Tìm kiếm đơn giản mà chính xác: Các bạn có thể ...
				</p>
				</a>
			</div>
			<script>homeNewSwipe();</script>
			<div class="v_div">
			</div>
			<div id="info">
				<div class="menu_bottom">
					<ul>
						<li><a href="/raovat/post.php" onclick="trackEvent('Menu Footer', 'Click', 'Đăng rao vặt');">
						<table cellpadding="0" cellspacing="0" border="0" border-spacing="0" width="100%">
						<tr>
							<td width="40" class="first_vcon">
								<i class="vcon-speaker"></i>
							</td>
							<td>
								Đăng tin mua bán
							</td>
							<td width="20" align="right">
								<i class="vcon-next_2"></i>
							</td>
						</tr>
						</table>
						</a></li>
						<li><a href="/hoidap/post.php" onclick="trackEvent('Menu Footer', 'Click', 'Đăng câu hỏi');">
						<table cellpadding="0" cellspacing="0" border="0" border-spacing="0" width="100%">
						<tr>
							<td width="40" class="first_vcon">
								<i class="vcon-hoidap_post"></i>
							</td>
							<td>
								Đăng câu hỏi
							</td>
							<td width="20" align="right">
								<i class="vcon-next_2"></i>
							</td>
						</tr>
						</table>
						</a></li>
						<li><a href="/home/" onclick="trackEvent('Menu Footer', 'Click', 'Trang sản phẩm');">
						<table cellpadding="0" cellspacing="0" border="0" border-spacing="0" width="100%">
						<tr>
							<td width="40" class="first_vcon">
								<i class="vcon-product"></i>
							</td>
							<td>
								Sản phẩm
							</td>
							<td width="20" align="right">
								<i class="vcon-next_2"></i>
							</td>
						</tr>
						</table>
						</a></li>
						<li><a href="/raovat/" onclick="trackEvent('Menu Footer', 'Click', 'Trang rao vặt');">
						<table cellpadding="0" cellspacing="0" border="0" border-spacing="0" width="100%">
						<tr>
							<td width="40" class="first_vcon">
								<i class="vcon-raovat"></i>
							</td>
							<td>
								Rao vặt
							</td>
							<td width="20" align="right">
								<i class="vcon-next_2"></i>
							</td>
						</tr>
						</table>
						</a></li>
						<li><a href="/hoidap/" onclick="trackEvent('Menu Footer', 'Click', 'Trang hỏi đáp');">
						<table cellpadding="0" cellspacing="0" border="0" border-spacing="0" width="100%">
						<tr>
							<td width="40" class="first_vcon">
								<i class="vcon-hoidap"></i>
							</td>
							<td>
								Hỏi đáp
							</td>
							<td width="20" align="right">
								<i class="vcon-next_2"></i>
							</td>
						</tr>
						</table>
						</a></li>
					</ul>
				</div>
				<div style="width:100%;">
					<script>var pageOptions = {'webId' : 3,'category':0,'cat_child' : 0,'banners' : [{ 'divIdShow': 'touchvg_banner_fbv_720x220', 'id_postion': 204, 'maxBan' : 1 },{'divIdShow' : 'banner_top_360x110','id_postion' : 131,'isWap':1,'type_show' : 2,'maxBan' : 1,'width': '100%'},{'divIdShow' : 'vatgia_touch_360x110','id_postion' : 21,'isWap':1,'type_show' : 2,'maxBan' : 1,'width': '100%'}]};var adblock = {'adblock1' : {}};$(document).ready(function(){var adVatgiaLoad= false;if(!adVatgiaLoad){var advatgia_js=document.getElementById('advatgia-jssdk');if(advatgia_js != null){advatgia_js.parentNode.removeChild(advatgia_js);}var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.id = "advatgia-jssdk";ga.src = "http://ad.vatgia.com/static/jsv2/advatgiav3.js";var s = document.getElementsByTagName('script');s[0].parentNode.insertBefore(ga, s[0]);}else{new vatgiaAd(pageOptions, adblock).Ads();}check_show_advatgia= 0;});</script>
					<div id="vatgia_touch_360x110" style="margin: 0px auto;max-width: 360px;">
					</div>
				</div>
				<div id="info-bottom">
					<div class="address">
						<span style="font-weight: bold;font-size: 14px;margin-bottom: 10px;display: block;">Công ty Cổ phần Vật Giá Việt Nam.</span>Lê Đại Hành, Hai Bà Trưng, Hà Nội<br/>Lữ Gia, Phường 15, Quận 11, Hồ Chí Minh<br/>Số GCNDT: 011032001615, cấp ngày 21/06/2012, nơi cấp: UBND thành phố Hà Nội.<span style="font-size: 12px;margin-top: 10px;display: block;color: #999;">© 2014 Vatgia.com. All rights reserved</span>
					</div>
					<div class="clear">
					</div>
					<div align="center">
						<a rel="nofollow" target="_blank" href="http://online.gov.vn/HomePage/WebsiteDisplay.aspx?DocId=88"><img border="0" style="width: 96px;" src="http://static.vatgia.com/themes/v1/images/licensed_2x.png"/></a>
					</div>
					<div class="clear">
					</div>
				</div>
			</div>
			<div id="loading">
				<div class="img">
					<img src="http://static.vatgia.com/cjs/mobile20150203/v1/css/loading.gif"/>
				</div>
			</div>
			<div class="overlay" onclick="closeAllMenu()">
			</div>
			<script type="text/javascript"></script>
			<script>setTimeout(function(){$(function(){ $('ins').each(function() { if($(this).html() == ''){ $(this).remove(); };});})}, 2000);</script>
			<script type="text/javascript">var _gaq = _gaq || []; (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){ (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o), m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m); })(window,document,'script','//www.google-analytics.com/analytics.js','ga'); ga('create', 'UA-2241410-6', 'auto'); ga('send', 'pageview'); $(function(){ initLoad(); })</script>
			<!-- Google Tag Manager -->
			<script>dataLayer = [];dataLayer.push({ 'ssoId': "1688998"});(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src='//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);})(window,document,'script','dataLayer','GTM-P3X76D');</script>
			<!-- End Google Tag Manager -->
			<script lang="javascript">function showNotificationBrowser(e){try{Notification.requestPermission(function(e){});execute_notification(e)}catch(t){}}function VatGiaChatCreateCookie(e,t,n){if(n){var r=new Date;r.setTime(r.getTime()+n*12*60*60*1e3);var i="; expires="+r.toGMTString()}else var i="";document.cookie=e+"="+t+i+"; path=/"}function VatGiaChatReadCookie(e){var t=e+"=";var n=document.cookie.split(";");for(var r=0;r<n.length;r++){var i=n[r];while(i.charAt(0)==" ")i=i.substring(1,i.length);if(i.indexOf(t)==0)return i.substring(t.length,i.length)}return null};function iniLoadChatVatGia(){socket.on("chat", function (raw) { fn_raw_chat(raw.data);});};var guest_id = VatGiaChatReadCookie("chat_guest_id");var vg_history = VatGiaChatReadCookie("vgchat_history");$(function() {var js_72ecdf602f1a0648af085a04bc47ba7c= false;if(!js_72ecdf602f1a0648af085a04bc47ba7c){var ga = document.createElement("script");ga.type = "text/javascript";ga.id = "js_72ecdf602f1a0648af085a04bc47ba7c";ga.src = "http://live.vnpgroup.net/js/chat.php?hash=ec9e7a82e598fbb9b3f549c8f8f3afdf&data=eyJhcHBfaWQiOjEsImNoIjpbMTY4ODk5OF0sImNzcyI6MSwiZmVlZCI6MCwiaVBybyI6MCwibGlzdGlkIjoiIiwicHJvaW5mbyI6IiIsInNzb19pZCI6MTY4ODk5OCwic3lzdGVtX2FjdGlvbiI6W119"+"&g="+guest_id+"&h="+vg_history;var s = document.getElementsByTagName("script");s[0].parentNode.insertBefore(ga, s[0]);}});</script>
			<div class="control_overlay" onclick="closeAllControl();trackEvent('Control button', 'Click', '');">
			</div>
			<a class="vg_control_btn" id="vg_control_btn" href="javascript:void(0)"><i class="ccon-control"><span class="vc_touch_messages touch_mess"></span><img src="http://static.vatgia.com/cjs/mobile20150203/v1/images/ccon-btn.png" id="control" alt="control"/></i></a>
			<div class="vg_control" id="vg_control">
				<div class="vg_control_group" id="control_0">
					<table class="control_row control_row_0">
					<tr>
						<td class="small_td">
							<a href="/home/showcart.php" onclick="showLoadding()" class="control_link" title="Giỏ hàng"><i class="ccon-cart"><img src="http://static.vatgia.com/cjs/mobile20150203/v1/images/ccon-cart.png"/></i><span class="title">Giỏ hàng</span></a>
						</td>
						<td class="cneter_td">
							<a href="javascript:void(0)" onclick="showSearch()" class="control_link" title="Tìm kiếm"><i class="ccon-search"><img src="http://static.vatgia.com/cjs/mobile20150203/v1/images/ccon-search.png"/></i><span class="title">Tìm kiếm</span></a>
						</td>
						<td class="small_td">
							<a href="javascript:void(0)" onclick="showChildControl(1)" class="control_link" title="Cá nhân"><i class="ccon-person"><img src="http://static.vatgia.com/cjs/mobile20150203/v1/images/ccon-person.png"/></i><span class="title">Cá nhân</span></a>
						</td>
					</tr>
					</table>
					<table class="control_row control_row_1">
					<tr>
						<td class="smal_4">
							<a href="http://www.vatgia.com/home/pcswitch.php?redirect=L2hvbWUv" onclick="" class="control_link" title="Xem bản PC"><i class="ccon-monitor"><img src="http://static.vatgia.com/cjs/mobile20150203/v1/images/ccon-monitor.png"/></i><span class="title">Xem bản PC</span></a>
						</td>
						<td class="smal_4">
							<a href="javascript:void(0)" onclick="redireactTo('/home/')" class="control_link" title="Trang chủ"><i class="ccon-home_2"><img src="http://static.vatgia.com/cjs/mobile20150203/v1/images/ccon-home_2.png"/></i><span class="title">Trang chủ</span></a>
						</td>
						<td class="smal_4">
							<a href="javascript:void(0)" onclick="showChildControl(3)" class="control_link" title="Danh mục"><i class="ccon-menu"><img src="http://static.vatgia.com/cjs/mobile20150203/v1/images/ccon-menu.png"/></i><span class="title">Danh mục</span></a>
						</td>
					</tr>
					</table>
					<table class="control_row control_row_2">
					<tr>
						<td class="small_td">
							<a href="/home/contact.php" onclick="showLoadding()" class="control_link" title="Liên hệ"><i class="ccon-email"><img src="http://static.vatgia.com/cjs/mobile20150203/v1/images/ccon-email.png"/></i><span class="title">Liên hệ</span></a>
						</td>
						<td class="cneter_td">
							<a href="javascript:void(0)" onclick="hide_panel_vgchat();" class="control_link" title="Tin nhắn"><i class="ccon-vchat_mess"><span class="vc_touch_messages touch_mess"></span><img src="http://static.vatgia.com/cjs/mobile20150203/v1/images/ccon-vchat_mess.png"/></i><span class="title">Tin nhắn</span></a>
						</td>
						<td class="small_td">
							<a href="javascript:void(0)" onclick="showChildControl(2)" class="control_link" title="Bán hàng"><i class="ccon-sale"><img src="http://static.vatgia.com/cjs/mobile20150203/v1/images/ccon-sale.png"/></i><span class="title">Bán hàng</span></a>
						</td>
					</tr>
					</table>
				</div>
				<div class="vg_control_group" id="control_1">
					<table class="control_row control_row_0">
					<tr>
						<td class="">
							<a href="/home/user_view.php" onclick="showLoadding()" class="control_link" title="Đã xem"><i class="ccon-eye"><img src="http://static.vatgia.com/cjs/mobile20150203/v1/images/ccon-eye.png"/></i><span class="title">Đã xem</span></a>
						</td>
					</tr>
					</table>
					<table class="control_row control_row_1">
					<tr>
						<td class="">
							<a href="https://www.baokim.vn" onclick="showLoadding()" class="control_link" title="Ví điện tử"><i class="ccon-purse"><img src="http://static.vatgia.com/cjs/mobile20150203/v1/images/ccon-purse.png"/></i><span class="title">Ví điện tử</span></a>
						</td>
						<td class="center">
							<a href="javascript:void(0)" onclick="hideChildControl(1)" class="control_link" title=""><i class="ccon-back"><img src="http://static.vatgia.com/cjs/mobile20150203/v1/images/ccon-back.png"/></i><span class="title"></span></a>
						</td>
						<td class="small_td">
							<a href="/home/profile.php?nType=order_listing" onclick="showLoadding()" class="control_link" title="Quản lý<br/>đơn hàng"><i class="ccon-box"><img src="http://static.vatgia.com/cjs/mobile20150203/v1/images/ccon-box.png"/></i><span class="title">Quản lý<br/>đơn hàng</span></a>
						</td>
					</tr>
					</table>
					<table class="control_row control_row_2">
					<tr>
						<td class="blank">
							<a href="" onclick="" class="control_link" title=""><i class="ccon-blank"><img src="http://static.vatgia.com/cjs/mobile20150203/v1/images/ccon-blank.png"/></i><span class="title"></span></a>
						</td>
						<td class="small_td">
							<a href="/home/profile.php" onclick="showLoadding()" class="control_link" title="Sửa thông tin"><i class="ccon-setting"><img src="http://static.vatgia.com/cjs/mobile20150203/v1/images/ccon-setting.png"/></i><span class="title">Sửa thông tin</span></a>
						</td>
						<td class="">
							<a href="tel:0473080008" onclick="closeAllControl()" class="control_link" title="Hỗ trợ"><i class="ccon-support"><img src="http://static.vatgia.com/cjs/mobile20150203/v1/images/ccon-support.png"/></i><span class="title">Hỗ trợ</span></a>
						</td>
						<td class="blank">
							<a href="" onclick="" class="control_link" title=""><i class="ccon-blank"><img src="http://static.vatgia.com/cjs/mobile20150203/v1/images/ccon-blank.png"/></i><span class="title"></span></a>
						</td>
					</tr>
					</table>
				</div>
				<div class="vg_control_group" id="control_2">
					<table class="control_row control_row_0">
					<tr>
						<td class="small_td">
							<a href="/profile/?module=raovat" onclick="showLoadding()" class="control_link" title="Quản lý<br/>rao vặt"><i class="ccon-ql_raovat"><img src="http://static.vatgia.com/cjs/mobile20150203/v1/images/ccon-ql_raovat.png"/></i><span class="title">Quản lý<br/>rao vặt</span></a>
						</td>
						<td class="center_td">
							<a href="/home/profile.php?nType=listing_order" onclick="showLoadding()" class="control_link" title="Quản lý<br/>đơn hàng"><i class="ccon-ql_donhang"><img src="http://static.vatgia.com/cjs/mobile20150203/v1/images/ccon-ql_donhang.png"/></i><span class="title">Quản lý<br/>đơn hàng</span></a>
						</td>
						<td class="small_td">
							<a href="/home/profile.php?nType=listing_product" onclick="showLoadding()" class="control_link" title="Quản lý<br/>sản phẩm"><i class="ccon-ql_sanpham"><img src="http://static.vatgia.com/cjs/mobile20150203/v1/images/ccon-ql_sanpham.png"/></i><span class="title">Quản lý<br/>sản phẩm</span></a>
						</td>
					</tr>
					</table>
					<table class="control_row control_row_1">
					<tr>
						<td class="">
							<a href="/home/profile.php?nType=listing_product" onclick="showLoadding()" class="control_link" title="Cập nhật giá"><i class="ccon-update_price"><img src="http://static.vatgia.com/cjs/mobile20150203/v1/images/ccon-update_price.png"/></i><span class="title">Cập nhật giá</span></a>
						</td>
						<td class="center">
							<a href="javascript:void(0)" onclick="hideChildControl(2)" class="control_link" title=""><i class="ccon-back"><img src="http://static.vatgia.com/cjs/mobile20150203/v1/images/ccon-back.png"/></i><span class="title"></span></a>
						</td>
						<td class="">
							<a href="/txnc2002" onclick="showLoadding()" class="control_link" title="Vào gian hàng"><i class="ccon-home_store"><img src="http://static.vatgia.com/cjs/mobile20150203/v1/images/ccon-home_store.png"/></i><span class="title">Vào gian hàng</span></a>
						</td>
					</tr>
					</table>
					<table class="control_row control_row_2">
					<tr>
						<td class="">
							<a href="/hoidap/post.php" onclick="showLoadding()" class="control_link" title="Đăng hỏi đáp"><i class="ccon-post_hd"><img src="http://static.vatgia.com/cjs/mobile20150203/v1/images/ccon-post_hd.png"/></i><span class="title">Đăng hỏi đáp</span></a>
						</td>
						<td class="">
							<a href="/home/post.php" onclick="" class="control_link" title="Đăng sản phẩm"><i class="ccon-post_sp"><img src="http://static.vatgia.com/cjs/mobile20150203/v1/images/ccon-post_sp.png"/></i><span class="title">Đăng sản phẩm</span></a>
						</td>
						<td class="">
							<a href="/raovat/post.php" onclick="showLoadding()" class="control_link" title="Đăng rao vặt"><i class="ccon-post_rv"><img src="http://static.vatgia.com/cjs/mobile20150203/v1/images/ccon-post_rv.png"/></i><span class="title">Đăng rao vặt</span></a>
						</td>
					</tr>
					</table>
				</div>
				<div class="vg_control_group" id="control_3">
					<table class="control_row control_row_0">
					<tr>
						<td class="">
							<a href="/home/" onclick="" class="control_link" title="Sản phẩm"><i class="ccon-product"><img src="http://static.vatgia.com/cjs/mobile20150203/v1/images/ccon-product.png"/></i><span class="title">Sản phẩm</span></a>
						</td>
					</tr>
					</table>
					<table class="control_row control_row_1">
					<tr>
						<td class="small_td">
							<a href="/raovat/" onclick="showLoadding()" class="control_link" title="Rao vặt"><i class="ccon-raovat"><img src="http://static.vatgia.com/cjs/mobile20150203/v1/images/ccon-raovat.png"/></i><span class="title">Rao vặt</span></a>
						</td>
						<td class="center">
							<a href="javascript:void(0)" onclick="hideChildControl(3)" class="control_link" title=""><i class="ccon-back"><img src="http://static.vatgia.com/cjs/mobile20150203/v1/images/ccon-back.png"/></i><span class="title"></span></a>
						</td>
						<td class="small_td">
							<a href="/hoidap/" onclick="showLoadding()" class="control_link" title="Hỏi đáp"><i class="ccon-hoidap"><img src="http://static.vatgia.com/cjs/mobile20150203/v1/images/ccon-hoidap.png"/></i><span class="title">Hỏi đáp</span></a>
						</td>
					</tr>
					</table>
					<table class="control_row control_row_2">
					<tr>
						<td class="small_td">
							<a href="/xu-huong-mua-sam-2014/100.shtml" onclick="showLoadding()" class="control_link" title="Khuyến mại"><i class="ccon-promotion"><img src="http://static.vatgia.com/cjs/mobile20150203/v1/images/ccon-promotion.png"/></i><span class="title">Khuyến mại</span></a>
						</td>
					</tr>
					</table>
				</div>
			</div>
		</div>
		<script>$(document).ready(function(){var arr = getStorate('offSetBtn');var a = arr[0]||5;var b = arr[1]||100;$('#vg_control').css({left:a,top:b,right:'auto',bottom:'auto'});$('#vg_control_btn').css({left:a,top:b,right:'auto',bottom:'auto'});});$(window).resize(function(){var arr = getStorate('offSetBtn');var a = arr[0]||5;var b = arr[1]||100;$('#vg_control').css({left:a,top:b,right:'auto',bottom:'auto'});$('#vg_control_btn').css({left:a,top:b,right:'auto',bottom:'auto'});})</script>
		<script>$(function(){initControl();})</script>
	</div>
</div>
</div>
<script type="text/javascript">$(function(){ initLoad(); })</script>
</body>
</html>