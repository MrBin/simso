<ol class="breadcrumb"><?=$home_address?></ol>
<style type="text/css">
.table tbody tr td,
.table tbody tr th{
   border: 0;
   padding: 8px;
}
<?
if($sProPrice <= 0 || $sCatName == "may-ban-VNPT"){
   echo 'table.detail-sim{width: 100%;}';
}
?>
</style>
<article>
   <table class="table table-sim detail-sim">
      <tbody>
         <tr>
            <td width="40%">Sim số đẹp</td>
            <td class="sim-digit text-primary"><h1><?=$sProName?></h1></td>
         </tr>
         
         <tr>
            <td>Giá bán</td>
            <td class="sim-price text-success text-bold"><strong><?=number_format($sProPrice) != 0 ? number_format($sProPrice).' vnđ' : 'Sim đã bán hoặc chưa có'?></strong></td>
         </tr>
         
         <tr>
            <td>Mạng di động</td>
            <td>
               <a href="javascript:;">
                  <img src="<?=$root_path_web.$path_category.$sCatPicture?>" alt="Sim so dep <?=$sCatName?>" />
               </a>
            </td>
         </tr>
         <?
         if($strType != ""){
            $link_simtp_index = $simtp_index != "" ? '/'.strtolower($simtp_index).'/' : 'javascript:;';
         ?>
         <tr>
            <td>Loại sim</td>
            <td><strong><a href="<?=$link_simtp_index?>" title="<?=$strType?>"><?=$strType?></a></strong></td>
         </tr>
         <?
         }
         ?>
         <? /*/?>
         <tr>
            <td>Tình trạng sim</td>
            <td>
               <input type="checkbox" checked="checked" /> Bộ simcard nguyên Kit có sẵn tài khoản<br />
               <input type="checkbox" checked="checked" /> Hưởng khuyến mãi hiện hành của nhà mạng<br />
               <input type="checkbox" checked="checked" /> Khách hàng sẽ được đăng ký thông tin khi mua sim<br /> 
            </td>
         </tr>
         <? //*/?>
      </tbody>
   </table>
   <?
   if($sProPrice > 0 && $sCatName != "may-ban-VNPT"){
   ?>
   <div class="ds_img_sim">
      <img src="<?=$srcPic?>" alt="<?=$sProName?>">
      <style type="text/css">
      .group_social{
         margin-top: 10px;
         text-align: right;
      }
      </style>
      <div class="group_social">
         <iframe src="https://www.facebook.com/plugins/share_button.php?href=<?=$con_server_name . $_SERVER["REQUEST_URI"]?>%2F&layout=button&size=small&appId=518289938685215&width=68&height=20" width="75" height="20" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allow="encrypted-media"></iframe>
         <div class="zalo-share-button" data-href="<?=$con_server_name . $_SERVER["REQUEST_URI"]?>" data-oaid="477951701879141919" data-layout="1" data-color="blue" data-customize=false></div>
      </div>
   </div>
   <?
   }// End if($sProPrice > 0){
   ?>
   <div class="clearfix"></div>
</article>
<?
if($sProPrice > 0)  include("inc_detail_sim_datsim.php");
else include("inc_detail_sim_search.php");
// include("inc_detail_sim_other1.php");
?>
<div class="box_info_detail">
   <h3>Các bước mua sim - <?=$sCatName?> - <?=$sProName?></h3>
   <ul>
      <li>
         <span class="icon icon-share-alt"></span>
         <span>Đặt mua sim trên website hoặc liên hệ trực tiếp <b style="color: #ed1d24;">0989.575.575</b></span>
      </li>
      <li>
         <span class="icon icon-share-alt"></span>
         <span>Nhân viên Sieuthisimthe.com sẽ liên hệ và xác nhận đơn hàng</span>
      </li>
      <li>
         <span class="icon icon-share-alt"></span>
         <span>Qúy Khách nhận sim tại nhà, kiểm tra thông tin và thanh toán cho người giao sim</span>
      </li>
   </ul>
   <h3>Hình thức giao sim - <?=$sCatName?> - <?=$sProName?></h3>
   <ul>
      <li>
         <span class="icon icon-share-alt"></span>
         <span>Cách 1: Giao sim ngay sau 15 – 30 phút ( Áp  dụng với sim có giá trị lớn )</span>
      </li>
      <li>
         <span class="icon icon-share-alt"></span>
         <span>Cách 2: Qúy Khách đến cửa hàng của Siêu Thị Sim Thẻ nhận sim trực tiếp</span>
      </li>
      <li>
         <span class="icon icon-share-alt"></span>
         <span>Cách 3: Siêu Thị Sim Thẻ giao sim qua đường bưu điện và thu tiền tại nhà</span>
      </li>
   </ul>
   <p>CHÚ Ý: <i>Quý khách sẽ không phải thanh toán thêm bất kỳ 1 khoản nào khác ngoài giá sim</i></p>

   <i>Chúc quý khách gặp nhiều may mắn khi sở hữu thuê bao <?=$sProName?></i>
</div>
