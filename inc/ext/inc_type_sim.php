<?
#+
#+ Xem co search sim voi lon hon 2 ki tu hay khong
if($module == 'searchsim' && strlen(preg_replace("/[^0-9]/si","",$keyword)) < 2){
	echo '<div class="alert alert-info">Vui lòng nhập ít nhất 2 chữ số để tìm!</div>';	
	return;
} // End if(strlen(preg_replace("/[^0-9*]/si","",$keyword)) < 3)

#+
#+ Set bien
$page_size		= 55;
$sqlSelect		= '';
$sqlJoin			= '';
$sql 				= '';
$sqlOrder 		= '';
$sqlGroup 		= '';
$sqlOrderBy 	= '';
$sqlLimit		= '';
#+
$normal_class    	= "";
$selected_class  	= "active";
$page_prefix 	 	= "Trang";
$current_page    	= ( getValue("page") < 1 ) ? 1 : getValue("page");
#+

$query_string = '/';
if($module == 'home')
{
	$url	= '/trang-';	
}
elseif($module == 'simtype')
{
	$url	= '/'.$sType.'-';
}
elseif($module == 'simprice')
{
	$url	= '/gia-'.$iPriceFrom.'-'.$iPriceTo.'-';	
}
elseif($module == 'simdauso')
{
	$url	= '/'.$sCat.'-dau-'.$sDauSo.'-';	
}
elseif($module == 'simsodep')
{
	$url	= '/sim-so-dep-';
}
elseif($module == 'simgiare')
{
	$url	= '/sim-gia-re-'.$sCat.'-';
}
elseif($module == 'simre')
{
	$url	= '/Sim-gia-re-';
}
elseif($module == 'simhoptuoi' || $module == 'sim-hop-tuoi')
{
	$url	= '/sim-phong-thuy-hop-tuoi-'.$iNamSinh.'-';
}
elseif($module == 'simhopmenh')
{
	$url	= '/sim-phong-thuy-hop-menh-'.$sNguHanh.'-';
}
elseif($module == 'tra-phong-thuy')
{
	$url	= "/load/?type=sim&module=".$module."&iNguHanh=".$iNguHanh."&page=";
	$query_string = '';
}
elseif($module == 'searchsim' || $module == 'simajax')
{
	$url	= "/load/?type=sim&module=".$module."&keyword=".$keyword.'&iCat='.$iCat.'&iType='.$iType.'&iPrice='.$iPrice.'&iDauSo='.$iDauSo."&digit=".$digit."&sort=".$sort."&iNguHanh=".$iNguHanh."&page=";	
	$query_string = '';
}
elseif($module == 'simhot'){
	$url	= '/sim-hot-trong-ngay-';
	$query_string = '';
}
elseif($module == 'sim-ngay-sinh'){
	$url	= "/load/?type=sim&module=searchsim&keyword=" . $keyword . "&page=";	
	$query_string = '';
}
else
{
	$url	= '/'.$sCat.'-';	
}

if($module == 'sim-hop-tuoi'){
	$page_size = 30;
}

if($module != "simhot"){
	/* Query */
	include('inc_type_sim_sphinx.php');
	// include('inc_type_sim_sql.php');

	if($total_record <=0){
		$str_display  	= '<div class="bgEF tB tRed tS15" align="center" style="padding:20px;">
									<div class="no_result">Sim số <b>' . $keyword . '</b> hiện chưa được cập nhật trên hệ thống !</div>
									<div class="no_result_sub">Qúy Khách có thể liên hệ: <b>0909.575.575</b> đặt mua trực tiếp <br> hoặc <a href="/dat-sim-theo-yeu-cau.html" rel="nofollow" title="Đặt Sim Theo Yêu Cầu">Đặt Sim Theo Yêu Cầu.</a></div>
								</div>';
	}else{
		#+
		#+ Xem voi so luong sim nhu vay co bao nhieu trang
		if ($total_record % $page_size == 0){
			$num_of_page = $total_record / $page_size;
		}else{
			$num_of_page = (int)($total_record / $page_size) + 1;
		} // End if ($total_record % $page_size == 0)

		#+ 
		#+ Neu nhu co nguoi co tinh dien page lon hon so page hien tai
		if($current_page > $num_of_page) return;

		#+
		#+ Header Sim
		$str_head = $module != "tra-phong-thuy" && $append_data != 1 ? _temp::_simHead() : '';
		
		$str_display = '';
		$str = '';
		
		$str .= '
				<table class="table table-hover table-sim">
					'.$str_head.'
					<tbody>
				';
		unset($str_head);
		
		$i=($current_page-1) * $page_size;
		foreach($arrQuery as $key => $row){
			$i++;
			
			#+
			$str .= _temp::_simBody($row,$i);
		} // End while($row = mysql_fetch_assoc($db_simType->result))
		unset($arrQuery);
		
		
		$str .= '
					</tbody>
				</table>
				';
		if($module != 'sim-hop-tuoi'){
			if($total_record>$page_size){
			
				$str .= '<div class="text-center">
								<ul class="pagination">
									'.generatePageBar($page_prefix,$cache_page,$page_size,$total_record,$url,$normal_class,$selected_class,$query_string,'rel="nofollow"').'
								</ul>
							</div>
							';

			} // ENd if($total_record>$page_size)
		}

		$str_display = $str;

	} // End if($total_record <=0)

	if($module != 'home' && $module != 'xem-sim-hop-tuoi' && $module != 'sim-phong-thuy'){
		if($module == "searchsim"){
			if(isset($strSimTypeKeyword) && $strSimTypeKeyword != "") echo '<h1 class="title_main">' . $strSimTypeKeyword . ' <span>' . $keyword . '</span> giá tốt nhất hiện nay</h1>'; 
			else echo '<h1 class="title_main">Sim số đẹp <span>' . $keyword . '</span> giá tốt nhất hiện nay</h1>';
		}
		else if($module == "simhoptuoi" || $module == "simhopmenh" || $module == "simsodep" || $module == "simprice") echo '<h1 class="title_main">' . $con_site_title . '</h1>';
		else if($module == 'simtype') echo '<h1 class="title_main">' . $sTypeName . ' giá tốt nhất hiện nay</h1>';
		else if($module == 'simgiare') echo '<h1 class="title_main">SIM GIÁ RẺ ' . $sCat . '</h1>';
		else if($module == 'simre') echo '<h1 class="title_main">SIM GIÁ RẺ</h1>';
		else echo '<ol class="breadcrumb">' . $home_address . '</ol>';
	}
}
?>
<div id="main-page" class="type-sim">
	<?
	if($module != 'home' && $module != 'tra-phong-thuy' && $append_data != 1 && $module != 'xem-sim-hop-tuoi'){
		if($module == "sim" || $module == "simhot"){
			$arrLogo 	= array("viettel" 		=> "viettel",
									  "mobifone" 		=> "mobifone",
									  "vinaphone" 		=> "vinaphone",
									  "vietnamobile" 	=> "vietnamobile",
									  "vnpt" 			=> "vnpt");

			$arrQuery = getSimHot($iCat);

			if(!empty($arrQuery)){

				$classSimHot = "";
				$linkSimHot  = "/sim-gia-tot-trong-ngay";

				if($module == "simhot"){
					$classSimHot = 'class="list_simhot"';
					$linkSimHot  = 'javascript:;';
				}
			?>
			<div id="box_sim_hot" class="box_sim">
				<h1 class="bsh_title">
					<?
					if($module == "simhot") echo 'Sim giá tốt trong ngày chỉ bán giá gốc trong 24h';
					else echo 'Chọn số ' . $sCat . ' giá tốt nhất hiện nay';
					?>
				</h1>
				<ul <?=$classSimHot?>>
					<?
					$continue = 0;
					foreach ($arrQuery as $key => $row) {
						$continue++;

						if($module != "simhot"){
							if($continue > 17) continue;
						}

						$link_detail = createLink("detail_sim",$row);
						$cat_name    = $row['cat_name_index'];
					?>
					<li>
						<div class="sim_item">
							<img src="/lib/img/icon/icon_<?=strtolower($cat_name)?>.jpg" alt="<?=$cat_name?>">
							<div class="bs_main">
								<span class="sim_name"><?=$row["sim_sim1"]?></span>
								<span class="sim_price"><?=format_number($row['sim_price'])?>₫</span>
							</div>
						</div>
						<a href="<?=$link_detail?>" class="btn_cart" title="Đặt mua">Đặt mua</a>
					</li>
					<?
					}

					if($module != "simhot"){
					?>
					<li class="bs_more">
						<a href="<?=$linkSimHot?>" title="Sim hot">
							Xem thêm
						</a>
					</li>
					<?
					}
					?>
				</ul>
				<?
				if($module == "simhot"){
				?>
				<div class="bs_more_hot">
					<a href="<?=$linkSimHot?>" title="Sim hot">
						Xem thêm
					</a>
				</div>
				<?
				}
				?>
			</div>
			<?
			}// End if(!empty($result)){
		}
		if($module != 'sim-hop-tuoi' && $module != 'sim-phong-thuy' && $module != "simhot"){

			if($module == 'sim-ngay-sinh'){
			?>
			<div class="box_sim_ns">
				<select name="frm_birth_day" id="frm_birth_day" class="form-control">
					<option value="0">Ngày</option>
					<?
					for($i = 1; $i<=31; $i++){
						?>
						<option value="<?=$i?>"><?=$i?></option>
						<? 
					}
					?>
				</select>
				<select name="frm_birth_month" id="frm_birth_month" class="form-control">
					<option value="0">Tháng</option>
					<?
					for($i = 1; $i<=12; $i++){
						?>
						<option value="<?=$i?>"><?=$i?></option>
						<? 
					}
					?>
				</select>
				<select name="frm_birth_year" id="frm_birth_year" class="form-control">
					<option value="0">Năm</option>
					<?
					for($i = 1950; $i<=date('Y'); $i++){
						?>
						<option value="<?=$i?>"><?=$i?></option>
						<? 
					}
					?>
				</select>
				<button class="btn btn-warning" onclick="getSimBirthDay();"><i class="icon icon-search"></i></button>
			</div>
			<?
			}
			else{
			?>
			<div id="task_filter">
				<select name="searchMang" id="searchMang" class="form-control" onchange="searchMang('&sort=<?=$sort?>', '<?=$module?>')">
					<option value="0">Chọn mạng</option>
					<?
					foreach($arrSimCat as $key => $row){
					?>
					<option <? if($iCat == $key) echo 'selected="selected"'?> value="<?=$key?>"><?=$row['cat_name']?></option>
					<?	
					}
					?>
				</select>
		        
				<select name="searchDauSo" id="searchDauSo" class="form-control" onchange="<?=($module == 'searchsim' ? 'submit_form_v2()' : 'submit_form(\'&sort=' . $sort . '\')')?>">
					<option value="0">Chọn đầu số</option>
					<?
					$sds_category = 0;
					foreach($arrSimDauSo as $key => $row){
					if($iCat == 0){
						if($row['sds_home'] == 1){
							#+
							if($sds_category != $row["sds_category"]){
								$sds_category = $row["sds_category"];
							?>
							<optgroup label="<?=ucwords($arrSimCat[$row['sds_category']]['cat_name'])?>"></optgroup>
							<?
							} // End if($sds_category != $row["sds_category"])
							
							
							?>
							<option <? if($iDauSo == $key) echo 'selected="selected"'?> value="<?=$key?>">Đầu số <?=$row['sds_name']?></option>
							<?	
						} // End if($row['sds_home'] == 1)
					}else{
						if($row['sds_category'] == $iCat){
						?>
						<option <? if($iDauSo == $key) echo 'selected="selected"'?> value="<?=$key?>">Đầu số <?=$row['sds_name']?></option>
						<?	
						}
					}
					} // End foreach($arrSimDauSo as $key => $row)
					?>
				</select>
		        
				<select name="searchType" id="searchType" class="form-control" onchange="<?=($module == 'searchsim' ? 'submit_form_v2()' : 'submit_form(\'&sort=' . $sort . '\')')?>">
					<option value="0">Chọn phân loại</option>
					<?
					foreach($arrSimType as $key => $row){
					?>
					<option <? if($iType == $key) echo 'selected="selected"'?> value="<?=$key?>"><?=$row['simtp_name']?></option>
					<?	
					}
					?>
				</select>
		        
				<select name="searchPrice" id="searchPrice" class="form-control" onchange="<?=($module == 'searchsim' ? 'submit_form_v2()' : 'submit_form(\'&sort=0\')')?>">
					<option value="0">Chọn khoảng giá</option>
					<?
					foreach($arrSimPrice as $key => $row){
					?>
					<option <? if($iPrice == $key) echo 'selected="selected"'?> value="<?=$key?>"><?=$row['pri_name']?></option>
					<?	
					}
					?>
				</select>
				<select name="searchMenh" id="searchMenh" class="form-control" onchange="searchMenh('&sort=<?=$sort?>', '<?=$module?>')">
					<option value="0">Chọn mệnh</option>
					<?
					foreach($arrNguHanh as $key => $row){
					?>
					<option <? if($iNguHanh == $key) echo 'selected="selected"'?> value="<?=$key?>">Sim hợp Mệnh <?=$row?></option>
					<?	
					}
					?>
				</select>
				
				<select name="searchNamSinh" id="searchNamSinh" class="form-control" onchange="<?=($module == 'searchsim' ? 'submit_form_v2()' : 'submit_form(\'&sort=' . $sort . '\')')?>">
					<option value="0">Chọn năm sinh</option>
					<?
					for($i = 1950; $i<=date('Y'); $i++){
					?>
					<option <? if($iNamSinh == $i) echo 'selected="selected"'?> value="<?=$i?>">Sim hợp tuổi <?=$i?></option>
					<?	
					}
					?>
				</select>
				<?/*<button class="btn btn-danger" id="searchDelete" onclick="submit_form('&submit=submit');">Hủy chọn</button>*/?>
			</div>
			<?
			}
		}// End if($module == 'sim-ngay-sinh'){

	} // End if($module != 'home')
	if($module != "simhot"){
		echo '<div class="border_line"></div>';
		echo $str_display;
	
		unset($str_display);
		unset($str);
	}
	?>
</div>
<?include('inc_temp_h2.php');?>