<?
$ErrorCode	= "";

#+
#+ Khai bao bien
$name			= getValue("name", "str", "POST", "");
$address		= getValue("address", "str", "POST", "");
$email		= getValue("email", "str", "POST", "");
$title		= getValue("title", "str", "POST", "");
//$phone			= getValue("phone", "str", "POST", "");
$fax			= getValue("fax", "str", "POST", "");
$content		= getValue("content", "str", "POST", "");
$scode			= getValue("scode", "str", "POST", "");
$cscode			= getValue("cscode","str","POST","");
$action 		= getValue("action", "str", "POST", "");
if($action == "contact"){
	if(!isset($_SESSION["session_security_code"])) redirect("/",1);
	
	if($scode == $_SESSION["session_security_code"]){
		$message = $title . "<br>";
		$message = "You have new contact from: " . $email . "<br>";
		$message.= "Tên :" . $name . "<br>";
		//$message.= "Phone    : " . $phone . "<br>";
		$message.= "Nội dung  : <br>" . $content . "";
		
		$to      = $con_admin_email;
		$subject = "hello " . $name . " contact to wwww."  . $_SERVER['SERVER_NAME'];	
		$headers  = 'MIME-Version: 1.0' . "\r\n";
		$headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";
		// Additional headers
		$headers .= 'To: Admin <' . $to . '>' . "\r\n";
		$headers .= 'From: ' . $name . ' <' . $email . '>' . "\r\n";
		$headers .= 'Cc: '. $to . '' . "\r\n";
		
		mail($to, $subject, $message, $headers);
		$_SESSION["session_security_code"] = rand(1000,9999);
		//return true;
		echo "<script language='javascript'>alert('Gửi thành công')</script>";
		redirect("/");
		exit();
	}
	else{
		$ErrorCode = "Mã an toàn không chính xác";
	}
}
?>
<ol class="breadcrumb"><?=$home_address?></ol>
<article>

    <div class="text-center">
        <h3>
            Vui lòng điền đầy đủ thông tin, chúng tôi sẽ liên hệ với bạn trong thời gian sớm nhất !
        </h3>
    </div>
    
    <form action="" method="post" name="contact"  id="myform" class="formsentmail">
        <ul>
            <li class="text-bold">
                Tên của bạn : <span class="text-danger">*</span>
            </li>
            <li>
                <input type="text" name="name" id="name_contact" size="30" class="form-control" value="<?=$name?>" />
            </li>
            
            <li class="text-bold">
                Email : <span class="text-danger">*</span>
            </li>
            <li>
                <input type="text" name="email" id="email_contact" size="30" class="form-control" value="<?=$email?>" />
            </li>
            
            <li class="text-bold">
                Tiêu đề : <span class="text-danger">*</span>
            </li>
            <li>
                <input type="text" name="title" id="title_contact" size="30" class="form-control" value="<?=$title?>" />
            </li>
            
            <li class="text-bold">
                Nội dung : <span class="text-danger">*</span>
            </li>
            <li>
                <textarea name="content" id="content_contact" class="form-control" cols="50" rows="8"><?=$content?></textarea>
            </li>
            <li class="text-bold">
                Mã xác nhận : <span class="text-danger">*</span>
            </li>
            <li>
                <? $_SESSION["session_security_code"] = rand(1000,9999);?>
                <input type="text" name="scode" id="scode" size="5" maxlength="5" value="" class="form-control" />
                &nbsp;&nbsp;
                <img src="/home/ext/securitycode.php" alt="Mã xác nhận" />
                <?=$ErrorCode;?>
                
                
            </li>
            <li class="text-bold">&nbsp;</li>
            <li>
                <button class="btn btn-success" onclick="check_contact()">Gửi đi</button>				
                <button class="btn btn-primary">Làm lại</button>
                <input type="hidden" name="action" value="contact" />	
            </li>
        </ul>
    </form>	
</article>


<? //*/?>
<div class="gMap" id="map3" style="width:550px; height:400px; margin-top: 20px;"></div>
<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
<script src="/lib/js/gmap.js" type="text/javascript"></script>
<script type="text/javascript">
$(function(){
	var center = ['20.990697', '105.841266'];

	$('#map3').gmap3({
		action: 'init',
		options: {
			zoom: 16,
			center: center,
			mapTypeId: 'roadmap' // terrain
		},
	
	}, {
		action: 'addMarker',
		latLng: center,
		infowindow: {
			content: '<b>TRUNG TÂM SIÊU THỊ SIM THẺ</b> <br />575 Giải Phóng - Hà Nội<br /><span>0989 575.575 - 0932 33.8888</span>'
		}
	});
				
});
</script>
<? //*/?>