<?
// Lấy điểm bình luận dòng sản phẩm
$arrReviewStar	= array();
for($i = 1; $i <= 5; $i++){

	$db_review	= new db_query("SELECT COUNT(*) AS total
										 FROM rating
										 WHERE rat_record_id = " . $recodeID . " AND rat_rating = " . $i);
	if($row	= mysql_fetch_assoc($db_review->result))	$arrReviewStar[$i]	= $row["total"];
	$db_review->close();
	unset($db_review);
}

$rating_type	= RATING_TYPE_PRODUCT;
$rating_default= 5;
// Khai báo dữ liệu để phân trang
$page_size		= 10;
$current_page	= getValue("p");
$page_url		= "/ajax/load_rating.php?type=" . $rating_type . "&record_id=" . $recodeID;
// Đếm xem có bao nhiêu dữ liệu
$db_count		= new db_query("SELECT COUNT(*) AS count
										 FROM rating
										 WHERE rat_type = " . $rating_type . " AND rat_date < " . time() . " AND rat_record_id = " . $recodeID);
$row				= mysql_fetch_assoc($db_count->result);
$total_record	= $row["count"];
$max_page		= (($total_record % $page_size) > 0 ? intval($total_record / $page_size) + 1 : $total_record / $page_size);
if($current_page > $max_page) $current_page	= $max_page;
if($current_page < 1) $current_page	= 1;
$db_count->close();
unset($db_count);
// Lấy dữ liệu
$db_rating		= new db_query("SELECT rat_id, rat_admin_id, rat_order_id, rat_rating, rat_name, rat_mobile, rat_content, rat_count_reply, rat_date
										 FROM rating
										 WHERE rat_type = " . $rating_type . " AND rat_date < " . time() . " AND rat_record_id = " . $recodeID . " AND rat_status <> 2
										 ORDER BY rat_date DESC
										 LIMIT " . ($current_page - 1) * $page_size . ", " . $page_size);
$arrRating	= convert_result_set_2_array($db_rating->result);
$db_rating->close();
unset($db_rating);
?>
<style type="text/css">
#reviews{
	text-transform: uppercase;
	font-size: 18px;
	text-align: center;
	margin-bottom: 0;
	margin-top: 30px;
}
/* Detail Exclusive Rate Service */
#dps_review{
  margin: 20px 0;
}
#detail_product_content #dps_review{
  margin-top: 10px;
}
#detail_product_reply_rate .rate_title{
  margin-bottom: 10px;
  text-transform: uppercase;
}
#detail_product_reply_rate .col_title{
  padding: 15px 10px 10px;
}
#detail_product_reply_rate .col_rate_left{
  overflow: hidden;
}
#detail_product_reply_rate .col_rate_left, #detail_product_reply_rate .col_rate_right{
  float: left;
  width: 50%;
}
#detail_product_reply_rate .col_rate_right{
  margin-left: 0;
}
#detail_product_reply_rate #detail_product_rate_tab li{
  border: solid 1px transparent;
}
#detail_product_reply_rate #detail_product_rate_tab li a{
  color: #333;
  display: block;
  height: 35px;
  line-height: 35px;
  padding-left: 8px;
}
#detail_product_reply_rate #detail_product_rate_tab li a .point{
  color: #619C77;
  font-weight: bold;
}
.tab_content_detail{
  padding: 10px 15px 0 15px;
}
.tab_content_detail .point{
  color: #999999;
  font-size: 14px;
}
.tab_content_detail .percent_bar{
  border: 1px solid #E5E5E5;
  background: #EBEBEB;
  display: inline-block;
  font-size: 0;
  height: 15px;
  margin: 0 5px;
  width: 160px;
  vertical-align: sub;
}
.tab_content_detail .percent_bar span{
  background: #FFCC00;
  display: inline-block;
  height:100%;
}
.tab_content_detail .percent{
  color: #52B858;
  font-weight: bold;
}
.tab_content .tab_content_detail {
  display: none;
  visibility: hidden;
}
.tab_content .active {
  display: block;
  visibility: visible;
}
#service_rate div{
  padding-bottom: 5px;
}
/* End Detail Exclusive Rate Service */
#detail_product_reply_rate{
  padding: 10px 0;
}
/*.rate_estore_tb{
  border: 1px solid #E5E5E5;
  padding: 15px;
}*/
.rate_estore_tb .show_rate_estore{
  width: 50%;
  position: relative;
  float: left;
}
.rate_estore_review{
  float: left;
  padding: 15px 10px 0 10px;
  text-align: center;
  width: 30%;
}
.rate_estore_review::after{
  border-right: 1px solid #E5E5E5;
  content: "";
  height: 120px;
  position: absolute;
  top: 50%;
  right: 0px;
  margin-top: -60px;
}
.suggest_review i{
  font-size: 12px;
}
.rate_estore_review .text{
  color: #999999;
  font-size: 12px;
  line-height: 15px;
}
#service_rate{
  float: left;
}
.suggest_review{
  width: calc(100% - 35px);
  overflow: hidden;
  position: relative;
  padding: 10px 10px 0 25px;

}
.suggest_review .suggest_review_content{
  line-height: 20px;
  padding: 0 0 5px 0;
}
.suggest_review .suggest_review_content b{
  color: #F44F00;
}
.suggest_review .btn_buy_now_review{
  margin-top: 40px;
  text-align: center;
}
.suggest_review .btn_buy_now_review .btn_buy_now{
  background-color: #E51515;
  border-radius: 3px;
  color: #FFF;
  display: inline-block;
  padding: 8px 20px;
  font-size: 14px;
  transition: background-color .4s,color .3s;
  border: 1px solid #E51515;
}
.suggest_review .btn_buy_now_review .btn_buy_now.active,
.suggest_review .btn_buy_now_review .btn_buy_now:hover{
  background-color: #FFF;
  color: #E51515;
}
#reviews .count_rating{
  font-size: 20px;
}
.rating_filter{
  background: #F5F5F5;
  padding: 10px;
}
.rating_filter a{
  display: inline-block;
}
.rating_filter a+a{
  margin-left: 20px;
}
.rating_filter a span{
  font-size: 0.8rem;
  color: #999;
}
.rating_filter a.active, .rating_filter a.active span{
  color: #E51515;
  font-weight: bold;
}
.rating_form_control .frm_name{
  width: calc(100%/2 - 15px);
  margin-right: 15px;
}
.rating_form_control .frm_phone{
  width: calc(100%/2 - 15px);
  margin-left: 15px;
}
.rating_form .rating_form_control .form_button{
  max-width: 164px;
  margin: 0 auto;
}
.rating_form_star{
  width: 100%;
  overflow: hidden;
  margin-top: 20px;
  margin-bottom: 15px;
}
.rating_form_star > span{
  float: left;
}
.rating_form_star > span.clear{
  float: none;
}
.rating_form_star .name{
  color: #999;
  display: block;
  float: none;
  margin: 4px 5px 10px 4px;
}
.rating_form_star .star i{
  color: #CCC;
  cursor: pointer;
  font-size: 1.6rem;
  padding: 0 2px;
}
.rating_form_star .star i.active{
  color: #FFCC00;
}
.rating_form_star .text{
  margin-top: 3px;
}
.rating_form_star .text span{
  background: #52B858;
  border-radius: 2px;
  color: #fff;
  font-size: 14px;
  margin-left: 5px;
  position: relative;
  padding: 4px 8px;
}
.rating_form_star .text span:after{
  border: 5px solid transparent;
  border-right-color: #52B858;
  content: "";
  height: 0;
  width: 0;
  margin-top: -5px;
  position: absolute;
  pointer-events: none;
  right: 100%;
  top: 50%;
}
.rating_form_control{
  font-size: 0;
}
.rating_form_control textarea{
  margin-bottom: 10px;
  width: 100%;
  height: 88px;
  font-family: Arial;
}
.rating_form_control input{
  margin-bottom: 15px;
  width: 100%;
}
#rating_list li{
  display: block;
  margin-bottom: 20px;
}
#rating_list .wrapper{
  position: relative;
}
#rating_list li.reply{
  background: #F5F5F5;
  border: 1px solid #E5E5E5;
  border-radius: 2px;
  margin-top: -5px;
  padding: 10px 12px 5px;
  position: relative;
}
#rating_list li.reply .wrapper{
  background: #F5F5F5;
}
#rating_list li.reply:before, #rating_list li.reply:after{
  border: solid transparent;
  content: " ";
  position: absolute;
  height: 0;
  width: 0;
  top: -20px;
  left: 18px;
}
#rating_list li.reply:before{
  border-color: none;
  border-bottom-color: #F5F5F5;
  border-width: 11px;
  margin-left: -11px;
  z-index: 1;
}
#rating_list li.reply:after{
  border-color: none;
  border-bottom-color: #E5E5E5;
  border-width: 10px;
  margin-left: -10px;
}
#rating_list li.reply > div+div{
  border-top: 1px solid #E5E5E5;
  margin-top: 9px;
  padding-top: 8px;
}
#rating_list div{
  margin-bottom: 5px;
}
#rating_list .user{
  font-weight: bold;
}
#rating_list .user abbr{
  background: #CCC;
  border-radius: 50%;
  color: #FFF;
  display: inline-block;
  font-size: 13px;
  margin-right: 10px;
  text-align: center;
  text-decoration: none;
  text-transform: uppercase;
  width: 30px;
  height: 30px;
  line-height: 31px;
}
#rating_list .user img{
  border-radius: 50%;
  margin-right: 10px;
  width: 30px;
  height: 30px;
  vertical-align: middle;
}
#rating_list .user .admin{
  background: #E51515;
  border-radius: 2px;
  color: #FFF;
  display: inline-block;
  font-size: 11px;
  font-weight: normal;
  margin: 3px 0 0 10px;
  padding: 2px 5px;
  vertical-align: top;
}
#rating_list .user .ordered{
  color: #52B858;
  font-size: 0.9rem;
  font-weight: normal;
  margin-left: 10px;
  padding-left: 18px;
  position: relative;
  white-space: nowrap;
}
#rating_list .user .ordered .icm{
  margin-right: 5px;
  position: absolute;
  top: 1px;
  left: 0;
}
#rating_list .user .ordered::after{
  content: "Đã mua hàng";
}
#rating_list .content{
  line-height: 160%;
}
#rating_list .content a{
  color: #E51515;
}
#rating_list .content .rating_button{
  font-size: 0.8rem;
  padding: 3px;
  width: 100px;
  vertical-align: top;
}
#rating_list .content .rating_button .icm{
  margin-right: 5px;
}
#rating_list .rating_star{
  margin-right: 10px;
}
#rating_list .text{
  color: #999;
  font-size: 13px;
}
.rating_form_control input.btn_comment{
	width: 200px;
	height: 40px;
	background: #3276B1;
	color: #FFF;
	border: 0;
	border-radius: 5px;
	font-size: 15px;
}
.reply_form{
	text-align: center;
}
</style>
<h2 id="reviews">
	Bình luận
</h2>
<div id="detail_product_reply_rate" class="detail_product_rate_estore">
	<div class="rate_estore_tb" id="suggest_review">
		<div class="rating_form">
			<div id="rating_form_0" class="rating_form_control">
				<div class="group-input">
					<textarea class="form-control" name="content" placeholder="Nhập bình luận của bạn *"></textarea>
				</div>
				<div class="group_input text-center">
					<input class="form-control frm_name" type="text" name="name" value="" placeholder="Họ và tên *" maxlength="150" onkeypress="if(event.keyCode==13){ratingPostData(); return false;}" />
					<input class="form-control frm_phone" type="text" name="mobile" value="" placeholder="Số điện thoại" maxlength="150" onkeypress="if(event.keyCode==13){ratingPostData(); return false;}" />
					<input type="button" class="btn_comment" value="Gửi bình luận của bạn" onclick="ratingPostData()" />
				</div>
				
			</div>
		</div>
	</div>
</div>
<?
// Hiển thị rating
$class_rating	= new rating($rating_type);
echo '<ul id="rating_list">' . $class_rating->generate_rating($arrRating) . '</ul>';

// Nếu đủ record thì mới phân trang
if($total_record > $page_size){
?>
<div class="page_bar"></div>
<script type="text/javascript">
$(function(){
	var opts	= {
		total_record	: <?=$total_record?>,
		page_size		: <?=$page_size?>,
		current_page	: <?=$current_page?>,
		page_url			: "<?=$page_url?>",
		page_object		: $(".page_bar"),
		content_object	: $("#rating_list"),
	}
	page_bar(opts);
});
</script>
<?
}// End if($total_record > $page_size)
?>
<script type="text/javascript">
var ratingConfig	= {
	rating		: <?=$rating_default?>,
	text			: "",
	listDomEle	: null,
	starDomEle	: null,
};

function showFromRating(domEle){
	domEle.toggleClass("active");

	if(domEle.hasClass("active")){
		domEle.html("Đóng");
		$(".rating_form").fadeIn();
	}
	else{
		$(".rating_form").fadeOut();
		domEle.html("Gửi bình luận của bạn");
	}
}

function ratingChangeText(rating){
	var text	= "";
	if(rating > 0) text	= $(".rating_form_star").find("i[value='" + rating + "']").attr("text");
	if(text != "") text	= '<span>' + text + '</span>';
	$(".rating_form_star").find(".text").html(text);
}

function ratingPostData(){
	var rating_id	= arguments[0] || 0;
	var formDomEle	= $("#rating_form_" + rating_id);
	var content		= formDomEle.find("[name='content']");
	var name			= formDomEle.find("[name='name']");
	var mobile		= formDomEle.find("[name='mobile']");
	if(rating_id == 0 && ratingConfig.rating == 0){ windowPrompt({ content: "Bạn chưa chọn sao bình luận.", alert: true }); return false; }
	if(content.val().length <= 10){ windowPrompt({ content: "Nội dung phải lớn hơn 10 ký tự.", alert: true, onClosed: function(){ content.focus(); } }); return false; }
	if(name.val() == ""){ windowPrompt({ content: "Bạn chưa nhập họ và tên.", alert: true, onClosed: function(){ name.focus(); } }); return false; }
	formDomEle.find(".form_button").prop("disabled", true);
	$.post("/ajax/add_rating.php", {
		type		: <?=$rating_type?>,
		record_id: <?=$recodeID?>,
		rating	: ratingConfig.rating,
		rating_id: rating_id,
		content	: content.val(),
		name		: name.val(),
		mobile	: mobile.val(),
		<? if($con_admin_id > 0){?>
			is_admin	: (formDomEle.find("[name='is_admin']").is(":checked") ? 1 : 0),
		<? }?>
	}).done(function(data){
		if(data == "") return false;
		if(rating_id > 0){
			var domEle	= $("#reply_list_" + rating_id);
			if(domEle.length) domEle.append(data);
			else $("#rating_" + rating_id).after('<li id="reply_list_' + rating_id + '" class="reply">' + data + '</li>');
			$(".reply_form").remove();
		}
		else{
			ratingConfig.listDomEle.prepend(data);
			ratingResetForm(formDomEle);
		}
	<? if($con_admin_id == 0){?>
		windowPrompt({
			close			: false,
			content		: '<div style="padding: 15px; text-align: center; font-size: 14px;">Cảm ơn bạn đã bình luận sản phẩm này.<br /><input type="button" id="rating_form_button" class="btn btn_success" value="Đóng" style="margin-top: 15px; width: 100px;" onclick="closeWindowPrompt()" /></div>',
			onComplete	: function(domEle){ setTimeout(function(){ $("#rating_form_button").focus(); }, 10); },
		});
	<? }?>
	});
}

function generateRatingReplyForm(rating_id, name){
	var defText	= "@" + name + ": ";
	var defName	= "";
	<? if($con_admin_id > 0) echo 'defName = "Quản trị viên";';?>
	var html		= '<div id="rating_form_' + rating_id + '" class="rating_form_control reply_form">' +
							'<textarea class="form-control" name="content" placeholder="Nhập bình luận của bạn *"></textarea>' +
							'<input class="form-control frm_name" type="text" name="name" value="' + defName + '" placeholder="Họ và tên *" maxlength="150" onkeypress="if(event.keyCode==13){ratingPostData(' + rating_id + '); return false;}" />' +
							'<input class="form-control frm_phone" type="text" name="mobile" value="" placeholder="Số điện thoại" maxlength="150" onkeypress="if(event.keyCode==13){ratingPostData(' + rating_id + '); return false;}" />' +
							'<input type="button" class="btn_comment" value="Gửi bình luận" onclick="ratingPostData(' + rating_id + ')" />' +
							<? if($con_admin_id > 0){?>
								'<input type="checkbox" id="is_admin" name="is_admin" value="1" checked="checked" style="margin-left: 1.25%; width: auto;" /><label for="is_admin" style="font-size: 1rem; margin-left: 5px;">Quản trị viên</label>' +
							<? }?>
						'</div>';
	$(".reply_form").remove();
	var domEle	= (!$("#reply_list_" + rating_id).length ? $("#rating_" + rating_id) : $("#reply_list_" + rating_id));
	domEle.after(html);
	setTimeout(function(){ $("#rating_form_" + rating_id).find("textarea").focus().val(defText); }, 10);
}

function ratingResetForm(formDomEle){
	ratingConfig.rating	= 0;
	ratingConfig.text		= "";
	ratingConfig.starDomEle.removeClass("active");
	formDomEle.find(".form-control").val("");
	formDomEle.find(".btn_comment").prop("disabled", false);
<? if($con_admin_id > 0){?>
	var currentDate		= new Date();
	var strDate				= currentDate.getDate() + "/" + (currentDate.getMonth()+1) + "/" + currentDate.getFullYear();
	var strTime				= currentDate.getHours() + ":" + currentDate.getMinutes() + ":" + currentDate.getSeconds();
	formDomEle.find("[name='date']").val(strDate);
	formDomEle.find("[name='time']").val(strTime);
<? }?>
	ratingChangeText("");
}

$(function(){
	ratingConfig.listDomEle	= $("#rating_list");
	ratingConfig.starDomEle	= $(".rating_form_star").find("i");
	ratingConfig.starDomEle.hover(
		function(){
			ratingConfig.starDomEle.removeClass("active").end().find("i:lt(" + $(this).attr("value") + ")").addClass("active");
			ratingChangeText($(this).attr("value"));
			if($(window).width() < 600) ratingConfig.rating	= $(this).attr("value");
		},
		function(){
			ratingConfig.starDomEle.removeClass("active").end().find("i:lt(" + ratingConfig.rating + ")").addClass("active");
			ratingChangeText(ratingConfig.rating);
		}
	);
	if($(window).width() >= 600){
		ratingConfig.starDomEle.click(function(){
			ratingConfig.rating	= $(this).attr("value");
		});
	}
});
</script>
<?
// JS + CSS khi admin login
if($con_admin_id > 0){
?>
<script type="text/javascript">
function createLink(){
	var onPress	=  ' onkeypress="if(event.keyCode==13){createLinkAct(); return false;}"';
	var html		=  '<div id="create_link"><div style="margin-bottom: 15px;"><input type="text" placeholder="https://" class="form_control frm_link" name="link" style="width: 360px;"' + onPress + ' /></div>' +
						'<div style="margin-bottom: 15px;"><input type="text" placeholder="Text..." class="form_control frm_text" name="text" style="width: 360px;"' + onPress + ' /></div>' +
						'<div><input type="checkbox" id="frm_nofollow" class="frm_nofollow" value="1" /><label for="frm_nofollow" style="color: #333; margin-left: 5px;">No Follow</label><input type="button" class="form_button" value="Tạo Link" onclick="createLinkAct()" style="float: right;" /></div></div>';
	windowPrompt("Tạo link bài viết", html);
	$("#create_link").find(".frm_link").focus();
}
function createLinkAct(){
	var domEle	= $("#create_link");
	var link		= domEle.find(".frm_link").val().replace("<?=$con_server_name?>", "");
	var text		= domEle.find(".frm_text").val().replace("<?=$con_server_name?>", "");
	if(text == "") text = link;
	var html		= '<a href="' + link + '" target="_blank"' + (domEle.find(".frm_nofollow").is(":checked") ? ' rel="nofollow"' : '') + '>' + text + '</a>';
	insertText(html);
	closeWindowPrompt();
}
function insertText(domEle){
	var obj	= $(".reply_form textarea");
	var tav	= obj.val();
	var text	= (typeof(domEle) == "object" ? domEle.text() : domEle);
	strPos	= obj[0].selectionStart;
	front		= (tav).substring(0, strPos),
	back		= (tav).substring(strPos, tav.length);
	obj.val(front + text + back).focus();
}
function ratingEdit(type, record_id){
	<?
	if($detect->isMobile()){
	?>
	windowPrompt({ iframe: true, maxWidth: '90%', height: 300, href: "/atony/modules/rating/edit.php?popup=1&type=" + type + "&record_id=" + record_id });
	<?
	}
	else{
	?>
	windowPrompt({ iframe: true, width: 750, height: 570, href: "/atony/modules/rating/edit.php?popup=1&type=" + type + "&record_id=" + record_id });
	<?
	}
	?>
}
function ratingDelete(type, record_id){
	if(confirm("Bạn muốn xóa dữ liệu này không?")){
		$.get("/atony/modules/rating/delete.php?ajax=1&type=" + type + "&record_id=" + record_id, function(data){
			if(data == 1){
				if(type == "rating"){
					var domEle	= $("#rating_" + record_id);
					if(domEle.next().is(".reply")) domEle.next().fadeOut(function(){ $(this).remove(); });
					domEle.fadeOut(function(){ $(this).remove(); });
				}
				else{
					var domEle	= $("#reply_" + record_id);
					if(domEle.parent().find(".wrapper").length > 1) domEle.fadeOut(function(){ $(this).remove(); });
					else domEle.parent().fadeOut(function(){ $(this).remove(); });
				}
			}
		});
	}
}
function generateRatingAction(){
	var domEle			= arguments[0] || ratingConfig.listDomEle.find(".wrapper");
	domEle.each(function(i){
		var arrTemp		= $(this).attr("id").split("_");
		var type			= (arrTemp[0] == "reply" ? "rating_reply" : "rating");
		var record_id	= arrTemp[1];
		var html			=  '<a class="edit" href="javascript:;" onclick="ratingEdit(\'' + type + '\', ' + record_id + ')">Sửa</a>' +
								'<a class="edit" href="javascript:;" onclick="ratingDelete(\'' + type + '\', ' + record_id + ')">Xóa</a>';
		$(this).find(".text").append(html);
	});
}
$(function(){ generateRatingAction(); });
</script>
<style tyle="text/css">
#rating_list div.rating_content_option{ font-size: 13px; line-height: 160%; margin-bottom: 10px; }
#rating_list div.rating_content_option a{ white-space: nowrap; }
#rating_list .wrapper .edit{ display: none; font-size: 12px; margin-left: 10px; }
#rating_list .wrapper:hover .edit{ display: inline-block; }
</style>
<?
}// End if($con_admin_id > 0)
?>