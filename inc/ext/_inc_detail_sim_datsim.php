<? 
$fs_table			= "tbl_yeucau";
$fs_redirect 		= $_SERVER['REQUEST_URI'];
$thoigian			= date("Y-m-d H:i:s");
$errorMsg 			= "";
$action				= getValue("actions", "str", "POST", "");
$code				= getValue("code", "str", "POST", "");
$simcard			= $sProName;
$giaban				= $sProPrice;

$hoten				= getValue("hoten", "str", "POST", "");
$hoten				= strip_tags($hoten);
$hoten				= substr($hoten,0,25);
$hoten 				= preg_replace("/[0-9]/si"," ",$hoten);

$hoten_check = RemoveSign($hoten);
$hoten_check = explode(' ',$hoten_check);
if(strlen($hoten_check[0]) >= 8){
	echo "<script language='javascript'>alert('" . tdt("Đặt mua sim thành công") . "')</script>";
	redirect($fs_redirect);	
	return;
} 


//
$myform = new generate_form();
//$myform->removeHTML(0);

$myform->addTable($fs_table);


$myform->add("simcard","simcard",0,1,"",0,"",0,"");
$myform->add("giaban","giaban",0,1,"",0,"",0,"");
$myform->add("hoten","hoten",0,1,"",1,"Điền họ tên",0,"");
$myform->add("dienthoai","dienthoai",0,0,"",1,"Điền điện thoại",0,"");
$myform->add("diachi","diachi",0,0,"",0,"",0,"");
$myform->add("noidung","noidung",0,0,"",0,"",0,"");
$myform->add("phanloai","phanloai",1,1,1,0,"",0,"");
$myform->add("thoigian","thoigian",0,1,"",0,"",0,"");
$myform->addjavasrciptcode('if(document.getElementById("code").value == ""){ alert("' . tdt("Điền mã xác nhận") . ' !"); document.getElementById("code").focus(); return false;}');

//
if($action == "submitForm"){
	if(!isset($_SESSION["session_security_code"])) redirect("index.php");
	if($code == $_SESSION["session_security_code"]){
		$errorMsg .= $myform->checkdata();	//Check Error!
		$errorMsg .= $myform->strErrorFeld ;	//Check Error!
		if($errorMsg == ""){
			$db_ex = new db_execute($myform->generate_insert_SQL());
			//echo $myform->generate_insert_SQL();
			echo "<script language='javascript'>alert('" . tdt("Đặt mua sim thành công") . "')</script>";
			redirect($fs_redirect);
			exit();
		}else{
			echo "<script language='javascript'>alert('" . $errorMsg . "')</script>";
		} 
		$_SESSION["session_security_code"] = rand(1000,9999);
	}else{
		echo "<script language='javascript'>alert('" . tdt("Mã xác nhận không chính xác") . "')</script>";
	}
}

//add form for javacheck
$myform->addFormname("fBuySim");
$myform->checkjavascript();
$myform->evaluate();
//*/
?>

<div class="dtSimContact">
	<h3>Thông tin khách hàng - Vui lòng gõ tiếng việt có dấu</h3>	
	
    <form name="fBuySim" action="" method="post">
    	<input class="form-control" type="hidden" name="actions" value="submitForm" />
		<ul>
			<li class="text">
				Họ tên :
				<span class="textSiteRed">*</span>
			</li>
			<li>
				<input class="form-control" name="hoten" id="hoten" value="<?=$hoten?>" type="text" onkeyup="telexingVietUC(this,event)" />	
			</li>
			
			<li class="text">
				Di động : 
				<span class="textSiteRed">*</span>
			</li>
			<li>
				<input class="form-control" name="dienthoai" id="dienthoai" value="<?=$dienthoai?>" type="text" />	
			</li>
			
			<li class="text">
				Địa chỉ :
			</li>
			<li>
				<input class="form-control" name="diachi" id="diachi" value="<?=$diachi?>" type="text" onkeyup="telexingVietUC(this,event)" />	
			</li>
			
			<li class="text">
				Thông tin khác : 
			</li>
			<li>
				<textarea class="form-control" maxlength="255" name="noidung" id="noidung" class="form" cols="45" rows="5" onkeyup="telexingVietUC(this,event)"><?=$noidung?></textarea>	
			</li>
			
			<li class="text">
				Mã xác nhận :
			</li>
			<li>
				<input class="form-control" type="text" name="code" id="code" class="form" size="10"/>&nbsp;
				<? $_SESSION["session_security_code"] = rand(1000,9999);?>
				<img src="/home/ext/securitycode.php" height="20" alt="Security code" />	
			</li>
			<li class="text">
				
			</li>
			<li>
				<button class="btn btn-success" onclick="validateForm();"><?=tdt("Gửi đi")?></button>
				<button class="btn btn-primary"><?=tdt("Làm lại")?></button>
			</li>
		</ul>
    </form>
</div>