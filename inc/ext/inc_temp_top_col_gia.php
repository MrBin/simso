<div class="panel panel-sim-cat">
    <div class="panel-heading">
        <h3 class="panel-title">
			<i class="icon icon-usd"></i>
			Tìm nhanh theo giá
        </h3>
        <div class="clearfix"></div>
    </div>
    <div class="panel-body">
    	<ul>
			<li class="col-md-6">
                <a data-pjax="#main" href="/gia-0-200000/" title="Sim số đẹp giá dưới 200.000 đ"> 
                    Dưới 200.000 đ
                </a> 
            </li>
            <li class="col-md-6">
                <a data-pjax="#main" href="/gia-200000-500000/" title="Sim số đẹp giá từ 2 trăm đến 5 trăm">
                    2 trăm -&gt; 5 trăm
                </a>
            </li>
            <li class="col-md-6">
                <a data-pjax="#main" href="/gia-500000-1000000/" title="Sim số đẹp giá từ 5 trăm đến 1 triệu">
                    5 trăm -&gt; 1 triệu
                </a>
            </li>
            <li class="col-md-6">
                <a data-pjax="#main" href="/gia-1000000-3000000/" title="Sim số đẹp giá từ 1 triệu đến 3 triệu">
                    1 triệu -&gt; 3 triệu
                </a> 
            </li>
            <li class="col-md-6">
                <a data-pjax="#main" href="/gia-3000000-5000000/" title="Sim số đẹp giá từ 3 triệu đến 5 triệu"> 
                    3 triệu -&gt; 5 triệu
                </a> 
            </li>
            <li class="col-md-6">
                <a data-pjax="#main" href="/gia-5000000-10000000/" title="Sim số đẹp giá từ 5 triệu đến 10 triệu">
                    5 triệu -&gt; 10 triệu
                </a> 
            </li>
            <li class="col-md-6">
                <a data-pjax="#main" href="/gia-10000000-50000000/" title="Sim số đẹp giá từ 10 triệu đến 50 triệu">
                    10 triệu -&gt; 50 triệu
                </a> 
            </li>
            <li class="col-md-6">
                <a data-pjax="#main" href="/gia-50000000-0/" title="Sim số đẹp trên 50 triệu">
                    Trên 50 triệu
                </a> 
            </li>
        </ul>
    </div>
</div>