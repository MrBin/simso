<?
$sql = '';

#+
if($sProDate != 0){
	$sql .= ' AND new_date < '.$sProDate;
}else{
	$sql .= ' AND new_id < '.$iData;	
} // End if($sProDate != 0)

#+
if($module == 'tin-tuc' || $module == 'tin-tuc-ko-dau'){
	$sql .= $sqlcategory;
}else{
	$sql .= ' AND cat_type = "'.$module.'"';		
} // End if($module == 'tin-tuc')

$query = ' SELECT *'
        .' FROM tbl_news '
        .' INNER JOIN tbl_category ON (cat_id = new_category)'
        .' WHERE tbl_category.lang_id = ' . $lang_id .' AND cat_active = 1 AND new_active = 1 AND new_id <> '.$iData.$sql
        .' ORDER BY new_date DESC'
        .' LIMIT 10'
		;
$arrQuery = getArray($query,'new_date');
if(count($arrQuery) > 0){
?>
	<div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">
                <i class="icon icon-credit-card"></i>
                Các tin cũ hơn
            </h3>
            <div class="clearfix"></div>
        </div>
        <div class="panel-body content">
         	<?
            foreach($arrQuery as $key => $row){
            
			if($module == 'tin-tuc-ko-dau'){
				$new_title 	= removeSeo($row["new_title"]);
				$lin_detail = createLink("detail_news_ko_dau",$row);
			}else{
				$new_title 	= $row["new_title"];
				$lin_detail = createLink("detail_news",$row);
			}
            ?>
                <li>
                	<span class="text-muted">[ <?=date("d-m-Y",$row["new_date"])?> ]</span>
                    <a href="<?=$lin_detail?>" title="<?=$new_title?>"><?=$new_title?></a> 
                </li>
            <?
            } // End while($row = mysql_fetch_assoc($db_query->result))				
            ?>  
        </div>
    </div>
<?
} // End if(mysql_num_rows($db_query->result))
?>