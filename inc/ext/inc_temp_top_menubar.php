<div id="header_bar">
	<div class="main">
   	<ul class="header_menu pull-left">
      	<li class="dropdown" data-hover="dropdown">
         	<a href="#" rel="nofollow" title="Danh mục">Danh mục <b class="caret"></b></a>
            <div class="dropdown-menu dropdown-menu-cat">
               <ul>
                  <li class="divider"><a href="/sim-vip/" rel="nofollow" title="Sim vip" data-pjax="#main_left">Sim vip</a></li>
                  <li><a href="/vinaphone/" rel="nofollow" title="Sim số đẹp Vinaphone" data-pjax="#main_left">Vinaphone</a></li>
                  <li><a href="/mobifone/" rel="nofollow" title="Sim số đẹp Mobifone" data-pjax="#main_left">Mobifone</a></li>
                  <li><a href="/viettel/" rel="nofollow" title="Sim số đẹp Viettel" data-pjax="#main_left">Viettel</a></li>
                  <li><a href="/vietnamobile/" rel="nofollow" title="Sim số đẹp Vietnamobile" data-pjax="#main_left">Vietnamobile</a></li>
               </ul>
                    
               <ul>
                  <li class="divider"><a href="/Sim-gia-re/" rel="nofollow" title="Sim giá rẻ" data-pjax="#main_left">Sim giá rẻ <b class="caret"></b></a></li>
                  <li><a class="text" href="/sim-gia-re-vinaphone/" rel="nofollow" title="Sim giá rẻ Vinaphone" data-pjax="#main_left">Vinaphone</a></li>
                  <li><a class="text" href="/sim-gia-re-mobifone/" rel="nofollow" title="Sim giá rẻ Mobifone" data-pjax="#main_left">Mobifone</a></li>
                  <li><a class="text" href="/sim-gia-re-viettel/" rel="nofollow" title="Sim giá rẻ Viettel" data-pjax="#main_left">Viettel</a></li>
                  <li><a class="text" href="/sim-gia-re-vietnamobile/" rel="nofollow" title="Sim giá rẻ Vietnamobile" data-pjax="#main_left">Vietnamobile</a></li>
               </ul>
                    
            </div>
         </li>
  
         <li class="topbar-sim-nam-sinh simple_tip" js="loadAjaxContent('/ajax/load_year.php', 'menuYear')" hideArrow="1" addClass="header_bar_simple_tip">
            <a href="#" rel="nofollow" title="Sim năm sinh">Sim năm sinh <b class="caret"></b></a>
         </li>
            
         <li class="simple_tip" js="loadAjaxContent('/ajax/load_year_old.php', 'menuYearOld')" hideArrow="1" addClass="header_bar_simple_tip">
            <a href="#" rel="nofollow" title="Sim hợp tuổi">Sim hợp tuổi <b class="caret"></b></a>
         </li>
            
         <li class="dropdown" data-hover="dropdown">
            <a href="#" rel="nofollow" title="Sim hợp mệnh">Sim hợp mệnh <b class="caret"></b></a>
            <ul class="dropdown-menu" role="menu">
               <li><a class="text" href="/sim-phong-thuy-hop-menh-kim/" rel="nofollow" title="Sim hợp mệnh Kim" data-pjax="body" data-ajax="#main_left">Sim hợp mệnh Kim</a></li>
               <li><a class="text" href="/sim-phong-thuy-hop-menh-thuy/" rel="nofollow" title="Sim hợp mệnh Thủy" data-pjax="body" data-ajax="#main_left">Sim hợp mệnh Thủy</a></li>
               <li><a class="text" href="/sim-phong-thuy-hop-menh-moc/" rel="nofollow" title="Sim hợp mệnh Mộc" data-pjax="body" data-ajax="#main_left">Sim hợp mệnh Mộc</a></li>
               <li><a class="text" href="/sim-phong-thuy-hop-menh-hoa/" rel="nofollow" title="Sim hợp mệnh Hỏa" data-pjax="body" data-ajax="#main_left">Sim hợp mệnh Hỏa</a></li>
               <li><a class="text" href="/sim-phong-thuy-hop-menh-tho/" rel="nofollow" title="Sim hợp mệnh Thổ" data-pjax="body" data-ajax="#main_left">Sim hợp mệnh Thổ</a></li> 
            </ul>
         </li>
            
         <li class="search-bar"></li>
      </ul>
    
      <ul class="header_menu pull-right">
         <li><div class="phone">Liên hệ: <span>0989.575.575 - 0909.575.575</span></div></li>
         <li class="dropdown" data-hover="dropdown">
            <a href="#" rel="nofollow">Thêm <b class="caret"></b></a>
            <ul class="dropdown-menu pull-right" role="menu">
               <li><a class="text" href="/sim-phong-thuy.html" rel="nofollow" title="Sim phong thủy" data-pjax=".type-phong-thuy">Sim phong thủy</a></li>   
               <li><a class="text" href="/huong-dan-chung.htm" rel="nofollow" title="Sim giá rẻ Vinaphone" data-pjax="#main_left">Hướng dẫn</a></li>
               <li><a class="text" href="/thanh-toan-qua-ngan-hang.htm" rel="nofollow" title="Sim giá rẻ Mobifone" data-pjax="#main_left">Thanh toán</a></li>
               <li><a class="text" href="/dat-sim-theo-yeu-cau.html" rel="nofollow" title="Đặt theo yêu cầu" data-pjax=".main_static_pjax">Đặt theo yêu cầu</a></li>
               <li><a class="text" href="/mua-ban-sim-so-dep.html" rel="nofollow" title="Mua bán sim số đẹp" data-pjax="#main_left">Mua bán sim số đẹp</a></li>
               <li><a class="text" href="/dang-ky-tra-truoc.html" rel="nofollow" title="Đăng ký sim trả trước" data-pjax="#main_left">Đăng ký sim trả trước</a></li>
               <li><a class="text" href="/bao-gia-sim-the.html" rel="nofollow" title="Báo giá sim thẻ">Báo giá sim thẻ</a></li>
               <li><a class="text" href="/575-giai-phong.html" rel="nofollow" title="Sim giá rẻ Viettel" data-pjax="#main_left">Liên hệ</a></li>
            </ul>
         </li>
      </ul>
   </div>
   <div class="clearfix"></div>
</div>

<div class="navbar_space"></div>