<div class="type-phong-thuy">
	<h1 class="title_main">Sim phong thủy</h1>
	<div class="header">
		<div class="logo"><img src="/lib/img/phong-thuy/logo.gif" alt="Logo Phong Thủy"></div>
		<div class="slogan"><img src="/lib/img/phong-thuy/slogan.png" alt="Slogan Phong Thủy"></div>
	</div>

	<div class="nav">
		<ul class="nav-tabs nav-tabs-phong-thuy">
			<li class="active"><a href="#tra-cuu" data-toggle="tab">Tra cứu</a></li>
			<li><a href="#gioi-thieu" data-toggle="tab">Giới thiệu</a></li>
			<li><a href="#chia-se" data-toggle="tab">Chia sẻ</a></li>
		</ul>
		<div class="clearfix"></div>
	</div> 
   
	<div class="tab-content">
		<div class="tab-pane active" id="tra-cuu"><?include('inc_type_phongthuy_tool.php');?></div>
		<div class="tab-pane fade" id="gioi-thieu"><?include('inc_type_phongthuy_intro.php');?></div>
		<div class="tab-pane fade" id="chia-se"><? include('inc_type_phongthuy_share.php');?></div>
	</div>
</div>