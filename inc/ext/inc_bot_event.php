<?
$event_popup = getValue('check_event_quoc_khanh', 'int', 'COOKIE', 0);

if($event_popup == 0){
?>
<div id="event_popup" class="modal">
   <div class="modal-dialog">
      <div class="modal-content">
      	<div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" onclick="$.cookie('check_event_quoc_khanh', 1, { expires: 1, path: '/' });">&times;</button>
            <h3 class="modal-title">Thông báo lịch nghỉ tết Nguyên Đán 2015</h3>
         </div>
         <div class="modal-body" style="padding: 8px;">
         	<a onclick="$.cookie('check_event_quoc_khanh', 1, { expires: 1, path: '/' }); $('#event_popup').modal('hide');" href="javascript:;" target="_blank">
				   <div class="lich_nghi_tet">
				   	<img src="/lib/img/thong_bao_nghi_tet_2015.jpg" width="450" alt="Lịch nghỉ tết 2015 Siêu Thị Sim Thẻ">
				      <div>
				         Từ ngày 15/2/2015 đến ngày 24/2/2015 dương lịch
				      </div>
				      <div>
				         Liên hệ trực tiếp : 0906 22.88.99
				      </div>
				   </div>
				</a>
				<style>
				@font-face{
					font-family: SFUHelveticaCondensed;
					src: url(/lib/fonts/SFUHelveticaCondensed.ttf), url(/lib/fonts/SFUHelveticaCondensed.eot), url(/lib/fonts/SFUHelveticaCondensed.woff);
				}
				.lich_nghi_tet{
					background:#E82219;
					color:#FFF; 
					font-size:22px;
					font-family:SFUHelveticaCondensed;
					line-height:2em;
					padding: 0 0 20px 0;
					text-align:center;
				}
				</style>
            <div class="clear"></div>
         </div>
      </div>
   </div>
</div>
<script type="text/javascript">$(function(){ $('#event_popup').modal('show'); })</script>
<?
}
?>