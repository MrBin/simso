<style>
table.scroll {
	width: 100%;
}

table.scroll tbody,
table.scroll thead { display: block; }

table.scroll tbody {
	height: 298px;
	overflow-y: auto;
	overflow-x: hidden;
}

table.scroll tbody td, table.scroll thead th {
	width: 25%;
}

</style>
<div class="panel panel-sidebar">
	<? /*/?>
	<div class="panel-heading">
		<h3 class="panel-title"><i class="icon icon-credit-card"></i> Thông tin khuyến mãi</h3>
		<div class="clearfix"></div>
	</div>
	<? //*/?>
	<div class="panel-body">
		<table cellpadding="4" cellspacing="1" class="scroll">
			<thead style="background: #EBEBEB; display: block;">
				<tr>
					<td colspan="3">
						<div style="text-align:center"><img alt="" height="33" src="/store/media/images/banner-khuyen-mai-soc-2.gif" width="345"></div>
					</td>
				</tr>
				<tr>
					<th style="text-align: center;">Số mới</th>
					<th style="text-align: center;">Giá cũ</th>
					<th style="text-align: center;">Giá mới(KM)</th>
				</tr>
			</thead>
			<tbody>
				<?
				//$str = getStatic($con_static_simkm);
				$arrSimSale = array();
				if(trim($con_sim_km) != ""){
					$e = explode( "\r\n", $con_sim_km );

					// Xử lý dữ liệu sim đưa vào
					$i = 0;
					for ( ; $i < count( $e ) - 1; $i++ ){
						
						list( $sosim, $price, $price_sale ) = split( "\t", $e[$i] );
						
						if(trim($sosim) != ""){
						
							$sim1x = preg_replace('/[^0-9. ]/','',$sosim);
							$sim2x = intval(preg_replace('/[^0-9]/','',$sosim));
					 		
							// Kiểm tra hợp lệ của số
							$arrSimSale[$sim2x] 	 = array("sim_sim1" => $sim1x,
																	"sim_sim2" => $sim2x, 
																	"sim_price" => $price, 
																	"sim_price_sale" => $price_sale);
																
						}// End if(trim($sosim) != "")
						
					}// End for ( ; $i < count( $e ) - 1; $i++ )
				}// End if(trim($con_sim_km) != "")
				//echo '<xmp>'; print_r($con_sim_km); echo '</xmp>';
				foreach($arrSimSale as $key => $row){
					$temp_type 				= checkSimType($key,$arrSimTypeIndex);
					$row["simtp_index"]	= @$arrSimType[$temp_type]["simtp_index"];
					$link_detail 			= createLink("detail_simtype",$row);
				?>
				<tr style="text-align: center;">
					<td style="color: #3276B1; font-size: 15px;"><a href="<?=$link_detail?>" title=""><b><?=@$row["sim_sim1"]?></b></a></td>
					<td style="color: #E04435; font-size: 14px; text-decoration: line-through; color: #999"><?=format_number(@$row["sim_price"])?></td>
					<td style="color: #E04435; font-size: 15px;"><?=format_number(@$row["sim_price_sale"])?></td>
				</tr>
				<?
				}
				?>
			</tbody>
		</table>
	</div>
</div>
<script type="text/javascript">
// Change the selector if needed
var $table = $('table.scroll'),
    $bodyCells = $table.find('tbody tr:first').children(),
    colWidth;

// Adjust the width of thead cells when window resizes
$(window).resize(function() {
    // Get the tbody columns width array
    colWidth = $bodyCells.map(function() {
        return $(this).width();
    }).get();
    
    // Set the width of thead columns
    $table.find('thead tr').children().each(function(i, v) {
        $(v).width(colWidth[i]);
    });    
}).resize(); // Trigger resize handler
</script>



<? /*/?>
<div class="panel panel-sidebar" style="margin-top:10px;">
    <div class="panel-heading">
        <h3 class="panel-title">
        	<i class="icon icon-credit-card"></i>
        	Sim năm sinh
        </h3>
        <div class="clearfix"></div>
    </div>
    
    <div id="sim-nam-sinh" class="panel-body">
    	<ul>
			<?
			for($i=1955; $i<=1999 ; $i++){
			?>
				<li class="col-md-4">
					<a href="/Sim-nam-sinh-<?=$i?>/" title="Sim năm sinh <?=$i?>" data-pjax="body" data-ajax="#main">Sim năm sinh <?=$i?></a>
				</li>
			<?
			} // End for($i=1960; $i<=1974 ; $i++)
            ?>
        </ul>
        <div class="clearfix"></div>
    </div>
</div>

<div class="panel panel-sidebar">
    <div class="panel-heading">
        <h3 class="panel-title">
        	<i class="icon icon-gift"></i>
        	Hỗ trợ qua Facebook Fanpge
        </h3>
        <div class="clearfix"></div>
    </div>
    <div class="panel-body">
        <iframe src="//www.facebook.com/plugins/likebox.php?href=https%3A%2F%2Fwww.facebook.com%2Fkhosimthe&amp;width&amp;height=215&amp;colorscheme=light&amp;show_faces=true&amp;header=false&amp;stream=false&amp;show_border=false&amp;appId=154521394741486" scrolling="no" frameborder="0" style="border:none; overflow:hidden; height:215px;" allowTransparency="true"></iframe>
    </div>
</div>
<? //*/?>