<?
#+
#+ Kiểm tra cache
$cache_check = 0; // 0.Chua cache - 1.Da cache
#+
if($con_set_cache != 0){
	#+
	$cache_file_time	= $cache_file_time_ext;
	#+ Ten file cache
	$cache_file_path	= 'tpl_simType';
	#+ Folder cache
	$cache_folder_path	= '../../store/cache/all/ext/ext';
	
	#+
	#+ Kiem tra xem co file cache khong
	$sCache = new Cache($cache_file_path,$cache_file_time,'',$cache_folder_path);
	if($sCache->is_cache()) {
		$cache_check = 1;
	} // End if(!$sCache->is_cache()) 	
} // End if($con_set_cache == 0)

#+
#+ Neu nhu khong co cache thi thuc hien
if($cache_check == 0){
	$str  = '';
	$str .= _temp::_simType();

	#+
	#+ Ghi cache
	if($con_set_cache != 0){
		$sCache->cache($str);	
	} // End if($con_set_cache != 0)
} // End if($cache_check == 1){
?>

<div class="panel panel-sim-cat">
	<div class="panel-heading">
		<? if($con_site_h1 != ''){?>
		<h1 style="margin: 0; padding: 5px 0; font-size: 14px;"><?=strip_tags($con_site_h1)?></h1>
		<? }else{?>
		<h3 class="panel-title"><i class="icon icon-credit-card"></i> Tìm nhanh theo loại</h3>
		<? }?>
		<div class="clearfix"></div>
	</div>
	<div class="panel-body">
		<ul>
			<?
         #+
         #+ Xem co cache hay chua. Neu co thi dung lai
         if($cache_check == 0){
             echo $str;
         }else{
             echo $sCache->get_cache();	
         } // End if($cache_check == 0)
         unset($str);
         ?>	
		</ul>
	</div>
</div>