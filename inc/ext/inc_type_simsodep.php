<?
# khai bao bien
$sort 		= getValue("sort","int","GET",1);
if($dauso != 0){
	$cacheType = $dauso; 	
}else{
	$cacheType = $iCat; 		
}
#+
$iPriceFrom 	= getValue("iPriceFrom","str","GET");
$iPriceTo 		= getValue("iPriceTo","str","GET");
#+
$dauso			= getValue("dauso","str","GET","");
$dauso 			= preg_replace("/[^0-9]/si","",$dauso);
$dauso			= (int)$dauso;
#+
$normal_class    	= "page";
$selected_class  	= "pageselect";
$page_prefix 	 	= tdt("Trang");
$page_size			= 45;
$current_page    	= ( getValue("page") < 1 ) ? 1 : getValue("page");
$url		 		= '/sim-so-dep.html';


# Xem co cache hay chua. Neu co thi dung lai
$sCache = new Cache("inc_type_simsodep_".$current_page,7*86400,'','inc_type_simsodep');	
$cache_check = 1;
if(!$sCache->is_cache()) 
{
	$cache_check = 0;
}


if($cache_check == 0){
	# Sau khi cache xong minh se set bien $con_set_cache=0 de ko cache nua cau hinh o lang.php
	if($con_set_cache == 0) return;
	
	# Tao ra truong search hop voi cac bien truyen vao
	$sqlSearch = "";
	if($dauso != 0){
		$sqlSearch 		.=" AND sim_sim2 LIKE '" . $dauso . "%'";
	}
	if($iCat != 0) $sqlSearch .= " AND sim_category = " . $iCat;
	if($iType != 0) $sqlSearch .= " AND simtp_id = " . $iType;
	
	if($iPriceFrom != 0) $sqlSearch .= " AND sim_price >= " . $iPriceFrom;
	if($iPriceTo != 0) $sqlSearch .= " AND sim_price <= " . $iPriceTo;
	#+
	#+ Dua ra sim 10 so hay 11 so
	$sort1 		= getValue("sort1","int","GET",2);
	if($sort1 == 0){
		$sqlSearch  .= " AND LENGTH(sim_sim2) = 9 ";
	}else if($sort1 == 1){
		$sqlSearch  .= " AND LENGTH(sim_sim2) = 10 ";
	}
	#+
	#+ Xem sap xep sim the nao
	$sqlOrder 	= "";
	$sort 		= getValue("sort","int","GET",1);
	$sqlOrder  .= $sort == 0 ? "sim_price," :  "sim_price DESC,";
	 
	# Lay tat ca cac sim phu hop cho vao array
	$arrSim = array();
	$query = " SELECT sim_sim1,sim_price,sim_category,cat_name,cat_picture,simtp_name"
			." FROM sim"
			." INNER JOIN simdaily ON (sim_cuahang = simch_id)"
			." INNER JOIN categories_multi ON (cat_id = sim_category)"
			." INNER JOIN simtype ON (simtp_id = sim_type)"
			." WHERE simch_show = 1 AND cat_type = 'sim' AND sim_active = 1 AND sim_price > 50 " . $sqlSearch 
			." ORDER BY sim_price DESC, sim_date DESC ";
	$arrSim = getArray($query);
	
	# So luong sim dem duoc
	$total_record 	= count($arrSim);
	
	# Xem voi so luong sim nhu vay co bao nhieu trang
	if ($total_record % $page_size == 0){
		$num_of_page = $total_record / $page_size;
	}
	else{
		$num_of_page = (int)($total_record / $page_size) + 1;
	}
	
	# Bat dau vong lap de tao cache
	for($so_trang=1; $so_trang<=$num_of_page;$so_trang++){
		
		# Trang hien tai cua cache
		$cache_page    	= $so_trang;
	
		# Chia sim thanh cac trang nho
		$arrSim1 			= array_slice($arrSim,($cache_page-1) * $page_size,$page_size);
		
		# Bat dau tao cache
		$sCache = new Cache("inc_type_simsodep_".$cache_page,7*86400,'','inc_type_simsodep');	
	
		# Bat dau ghi cache
		$str = '';
	
		$i=($cache_page-1) * $page_size;
		foreach($arrSim1 as $key => $row){
			$i++;
			$row['sim_sim2'] = preg_replace("/[^0-9]/si","",$row['sim_sim1']);
			
			$link_cat = createLink("cat",array("catname"=>$row["cat_name"],"iCat"=>$row["sim_category"]),$lang_path,$con_extenstion,$con_mod_rewrite);
			$lin_detail = createLink("detail_sim",array("title"=>'Sim số đẹp '.$row["cat_name"].'-' . $row["sim_sim2"],"iCat"=>$row["sim_category"],"iData"=>$row["sim_sim2"]));
			
			$str .= '
					<ul class="s">
						<li class="stt">
							'.$i.'
						</li>
						<li class="sim">
							<a class="textBlue textBold font15" href="'.$lin_detail.'" title="Sim số đẹp '.$row["cat_name"] . ' ' . $row["sim_sim1"].'">'.$row["sim_sim1"].'</a>
						</li>
						<li class="price">
							'.number_format($row["sim_price"] * 100000).'
						</li>
						<li class="cat">
							<img src="/store/media/category/'. $row["cat_picture"].'" alt="'.$row["cat_name"].'" align="absmiddle" />&nbsp;
						</li>
						<li class="type">
							'.$row["simtp_name"].'
						</li>
					</ul>
					';
	
		} // End while($row = mysql_fetch_assoc($db_simType->result))
	
		if($total_record>$page_size){
			$str .= '<ul class="pagi">';
			$str .= generatePageBar($page_prefix,$cache_page,$page_size,$total_record,$url,$normal_class,$selected_class,2);
			$str .= '</ul>';
		} // ENd if($total_record>$page_size)
	
		$sCache->cache($str);
		unset($str);
		
		# Huy array sim da chia
		unset($arrSim1);
	}
	
	# Huy toan bo sim trong array
	unset($arrSim);
} // End if($cache_check == 0){
?>	

<div class="tplSimHome">
	<ul class="s">
		<li class="sttTitle">
			Stt				
		</li>
		<li class="simTitle">
			Số sim
		</li>
		<li class="priceTitle">
			Giá bán
		</li>
		<li class="catTitle">
			Mạng
		</li>
		<li class="typeTitle">
			Phân loại sim
		</li>
	</ul>	
	<?
	$sCache = new Cache("inc_type_simsodep_".$current_page,7*86400,'','inc_type_simsodep');	
	if($sCache->is_cache()){
		echo $sCache->get_cache();
	} // End if($sCache->is_cache()){
	?>
</div>