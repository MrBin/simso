<style>
.text_overflow{ overflow: hidden; text-overflow: ellipsis; white-space: nowrap; }
.carousel{position:relative;line-height:1}
.carousel-inner{overflow:hidden;width:100%;position:relative}
.carousel-inner>.item{display:none;position:relative;transition:.6s ease-in-out left;-webkit-transition:.6s ease-in-out left;}
.carousel-inner>.item>a>img,.carousel-inner>.item>img{display:block;line-height:1}
.carousel-inner>.active,.carousel-inner>.next,.carousel-inner>.prev{display:block}
.carousel-inner>.active{left:0}
.carousel-inner>.next,.carousel-inner>.prev{position:absolute;top:0;width:100%}
.carousel-inner>.next{left:100%}.carousel-inner>.prev{left:-100%}
.carousel-inner>.next.left,.carousel-inner>.prev.right{left:0}.carousel-inner>.active.left{left:-100%}
.carousel-inner>.active.right{left:100%}
.carousel-control{background:url(beacon_sprite.png) no-repeat 0 -510px;display:block;position:absolute;top:42%;left:0px;width:13px;height:20px;opacity:.3;filter:alpha(opacity=30);transition:opacity 0.2s ease 0s;-webkit-transition:opacity 0.2s ease 0s;z-index:2;font-size: 25px;}
.carousel-control.right{background:url(beacon_sprite.png) no-repeat -35px -510px;left:auto;right:14px}
.carousel:hover .carousel-control{opacity:1;filter:alpha(opacity=100)}
.carousel-indicators{position:absolute;bottom:15px;right:100px;z-index:5;margin:0;list-style:none}
.carousel-indicators li{display:block;float:left;width:24px;height:24px;margin-left:5px;text-indent:-999px;background-color:#ccc;background-color:rgba(255,255,255,.9);border-radius:12px;cursor:pointer;box-shadow:0 0 5px 2px rgba(1,1,1,.2)}
.carousel-indicators .active{background-color:#0072BD}
.carousel-fade .carousel-inner .item{opacity:0;transition-property:opacity;}
.carousel-fade .carousel-inner .active.left,.carousel-fade .carousel-inner .active.right{left:0;opacity:0;}
.carousel-fade .carousel-inner .active,.carousel-fade .carousel-inner .next.left,.carousel-fade .carousel-inner .prev.right{opacity:1;}
.vertical .carousel-inner>.item{-webkit-transition:.6s ease-in-out top;transition:.6s ease-in-out top}.vertical .carousel-inner>.active,.vertical .carousel-inner>.next.left,.vertical .carousel-inner>.prev.right{top:0;left:0}
.vertical .carousel-inner .next.left,.vertical .carousel-inner .prev.right{top:0}.vertical .carousel-inner .active.left,.vertical .carousel-inner .active.right,.vertical .carousel-inner .next,.vertical .carousel-inner .prev{top:80px}
.tpl_carousel_1 .control{margin-top:-15px;position:absolute;top:50%;z-index:1}.tpl_carousel_1 .control_prev{left:0}.tpl_carousel_1 .control_next{right:0}
</style>
<script type="text/javascript">
+function(e){"use strict";function t(){var e=document.createElement("bootstrap");var t={WebkitTransition:"webkitTransitionEnd",MozTransition:"transitionend",OTransition:"oTransitionEnd otransitionend",transition:"transitionend"};for(var n in t){if(e.style[n]!==undefined){return{end:t[n]}}}}e.fn.emulateTransitionEnd=function(t){var n=false,r=this;e(this).one(e.support.transition.end,function(){n=true});var i=function(){if(!n)e(r).trigger(e.support.transition.end)};setTimeout(i,t);return this};e(function(){e.support.transition=t()})}(window.jQuery);+function(e){"use strict";var t=function(t,n){this.$element=e(t);this.$indicators=this.$element.find(".carousel-indicators");this.options=n;this.paused=this.sliding=this.interval=this.$active=this.$items=null;this.options.pause=="hover"&&this.$element.on("mouseenter",e.proxy(this.pause,this)).on("mouseleave",e.proxy(this.cycle,this))};t.DEFAULTS={interval:5e3,pause:"hover",wrap:true};t.prototype.cycle=function(t){t||(this.paused=false);this.interval&&clearInterval(this.interval);this.options.interval&&!this.paused&&(this.interval=setInterval(e.proxy(this.next,this),this.options.interval));return this};t.prototype.getActiveIndex=function(){this.$active=this.$element.find(".item.active");this.$items=this.$active.parent().children();return this.$items.index(this.$active)};t.prototype.to=function(t){var n=this;var r=this.getActiveIndex();if(t>this.$items.length-1||t<0)return;if(this.sliding)return this.$element.one("slid",function(){n.to(t)});if(r==t)return this.pause().cycle();return this.slide(t>r?"next":"prev",e(this.$items[t]))};t.prototype.pause=function(t){t||(this.paused=true);if(this.$element.find(".next, .prev").length&&e.support.transition.end){this.$element.trigger(e.support.transition.end);this.cycle(true)}this.interval=clearInterval(this.interval);return this};t.prototype.next=function(){if(this.sliding)return;return this.slide("next")};t.prototype.prev=function(){if(this.sliding)return;return this.slide("prev")};t.prototype.slide=function(t,n){var r=this.$element.find(".item.active");var i=n||r[t]();var s=this.interval;var o=t=="next"?"left":"right";var u=t=="next"?"first":"last";var a=this;if(!i.length){if(!this.options.wrap)return;i=this.$element.find(".item")[u]()}this.sliding=true;s&&this.pause();var f=e.Event("slide.bs.carousel",{relatedTarget:i[0],direction:o});if(i.hasClass("active"))return;if(this.$indicators.length){this.$indicators.find(".active").removeClass("active");var l=e(a.$indicators.children()[i.index()]);l&&l.addClass("active")}if(e.support.transition&&this.$element.hasClass("slide")){this.$element.trigger(f);if(f.isDefaultPrevented())return;i.addClass(t);i[0].offsetWidth;r.addClass(o);i.addClass(o);r.one(e.support.transition.end,function(){i.removeClass([t,o].join(" ")).addClass("active");r.removeClass(["active",o].join(" "));a.sliding=false;setTimeout(function(){a.$element.trigger("slid")},0)}).emulateTransitionEnd(600)}else{this.$element.trigger(f);if(f.isDefaultPrevented())return;r.removeClass("active");i.addClass("active");this.sliding=false;this.$element.trigger("slid")}s&&this.cycle();return this};var n=e.fn.carousel;e.fn.carousel=function(n){return this.each(function(){var r=e(this);var i=r.data("bs.carousel");var s=e.extend({},t.DEFAULTS,r.data(),typeof n=="object"&&n);var o=typeof n=="string"?n:s.slide;if(!i)r.data("bs.carousel",i=new t(this,s));if(typeof n=="number")i.to(n);else if(o)i[o]();else if(s.interval)i.pause().cycle()})};e.fn.carousel.Constructor=t;e.fn.carousel.noConflict=function(){e.fn.carousel=n;return this};e(document).on("click.bs.carousel.data-api","[data-slide], [data-slide-to]",function(t){var n=e(this),r;var i=e(n.attr("data-target")||(r=n.attr("href"))&&r.replace(/.*(?=#[^\s]+$)/,""));var s=e.extend({},i.data(),n.data());var o=n.attr("data-slide-to");if(o)s.interval=false;i.carousel(s);if(o=n.attr("data-slide-to")){i.data("bs.carousel").to(o)}t.preventDefault()});e(window).on("load",function(){e('[data-ride="carousel"]').each(function(){var t=e(this);t.carousel(t.data())})})}(window.jQuery);
$(function(){
	$('.carousel').carousel()
})
</script>
<?
$db_query = new db_query("SELECT * FROM tbl_news WHERE new_hot = 1");
?>
<div id="carousel-example-generic" class="tpl_carousel_1 carousel slide" data-ride="carousel" style="border: solid 1px #DDD; margin: 10px 0; background: #F6F6F6;">
	<div class="carousel-inner">
		<?
		$i = 0;
		while($row = mysql_fetch_assoc($db_query->result)){
			$i++;
			$link_detail = createLink("detail_news",$row);
		?>
		<div class="item<?=($i == 1 ? ' active' : '')?>">
			<a href="<?=$link_detail?>" title="<?=$row["new_title"]?>">
				
				<img alt="<?=$row["new_title"]?>" src="/store/media/news/<?=$row["new_title_index"]?>/<?=$row["new_picture"]?>" style="width: 100%; height: 250px;" />
				<div class="text_overflow" style="margin: 10px 0; text-align: center; padding: 0 10px;"><?=$row["new_title"]?></div>
			</a>
		</div>
		<?
		}
		?>
	</div>
	<a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
		<span class="icon icon-chevron-left"></span>
	</a>
	<a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
		<span class="icon icon-chevron-right"></span>
	</a>
</div>