<?
#+
#+ Kiểm tra cache
$cache_check = 0; // 0.Chua cache - 1.Da cache
#+
if($con_set_cache != 0){
	#+
	$cache_file_time	= $cache_file_time_ext;
	#+ Ten file cache
	$cache_file_path	= 'tp_PhongThuy1';
	#+ Folder cache
	$cache_folder_path	= '../../store/cache/all/ext/ext';
	
	#+
	#+ Kiem tra xem co file cache khong
	$sCache = new Cache($cache_file_path,$cache_file_time,'',$cache_folder_path);
	if($sCache->is_cache()) {
		$cache_check = 1;
	} // End if(!$sCache->is_cache()) 	
} // End if($con_set_cache == 0)

#+
#+ Neu nhu khong co cache thi thuc hien
if($cache_check == 0){
	#+
	$str = getStatic($con_static_simphongthuy1);
	
	#+
	#+ Ghi cache
	if($con_set_cache != 0){
		$sCache->cache($str);	
	} // End if($con_set_cache != 0)
} // End if($cache_check == 1){
?>

<div>
	<?
    #+
    #+ Xem co cache hay chua. Neu co thi dung lai
    if($cache_check == 0){
        echo $str;
    }else{
        echo $sCache->get_cache();	
    } // End if($cache_check == 0)
    unset($str);
    ?>
</div>