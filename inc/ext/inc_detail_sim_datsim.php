<style>
.text{
	margin-top: 10px;
}
.text label{
	width: 120px;
}
.text label .textSiteRed{
	color: #F00;
}
.text input{
	width: calc(100% - 125px);
}
.btn_control{
	padding-left: 125px;
	margin-top: 10px;
}
.btn_send{
	padding: 6px 8px;
	background-color: #47a447;
}
.hotline_frm{
	margin-right: 10px;
}
.dtSimContact label.error{
	display: block;
	width: calc(100% - 125px);
	margin-top: 5px;
	color: #F00;
	padding-left: 125px;
}
.errorMsg{
	text-align: center;
	color: #F00;
}
</style>
<div class="dtSimContact hidden" id="box_contact_success">
	<div class="btn_success">
		Đã đặt mua thành công
	</div>
	<p>Chúng tôi sẽ kiểm tra đơn hàng <br> và chủ động liên hệ với quý khách trong thời gian sớm nhất</p>
	<p>Xin chân thành cảm ơn!</p>
</div>
<div class="dtSimContact" id="box_contact">
	<h3>Thông tin khách hàng</h3>	
	
	<form name="fBuySim" action="" method="post" id="frm_checkout">
    	<input class="form-control" type="hidden" name="actions" value="submitForm" />
    	<div class="errorMsg"></div>
		<ul>
			<li class="text">
				<label>Họ tên: <span class="textSiteRed">*</span></label>
				<input class="form-control" placeholder="Vui lòng nhập họ tên" name="billing_name" id="billing_name" value="<?=$hoten?>" type="text" />
			</li>
			
			<li class="text">
				<label>Số điện thoại: <span class="textSiteRed">*</span></label>
				<input class="form-control" placeholder="Vui lòng nhập số điện thoại" name="billing_phone" id="billing_phone" value="<?=$dienthoai?>" type="text" />
			</li>
			
			<li class="text">
				<label>Địa chỉ:</label>
				<input class="form-control" name="billing_address" placeholder="Vui lòng nhập địa chỉ" id="billing_address" value="<?=$diachi?>" type="text" />
			</li>
			<li class="btn_control">
				<span class="hotline_frm"><strong>Giao sim miễn phí tận tay trong ngày! Lh</strong> <strong class="sim-digit text-danger text-bold id_hotline">0909.575.575</strong></span>
				<button class="btn btn-success btn_send"><?=tdt("Đặt mua")?></button>
			</li>
		</ul>
		<input type="hidden" id="action" name="action" value="send">
		<input type="hidden" id="giaban" name="giaban" value="<?=$sProPrice?>">
		<input type="hidden" id="simcard" name="simcard" value="<?=$sProName?>">
    </form>
</div>