<?
// Google Captcha
// require_once('../../kernel/classes/gcaptcha/autoload.php');
// // Register API keys at https://www.google.com/recaptcha/admin
// $siteKey 	= '6LcUBCITAAAAAJBZuIfXTMQl77tyiJAAyn2e-JsA';
// $secret 		= '6LcUBCITAAAAAKIJTnEG3d7QHDuz08rQHCIpqNb-';
// // reCAPTCHA supported 40+ languages listed here: https://developers.google.com/recaptcha/docs/language
// $lang 		= 'vi';

// Khai bao bien
$fs_table			= "tbl_yeucau";
$fs_redirect 		= $_SERVER['REQUEST_URI'];
$thoigian			= date("Y-m-d H:i:s");
$errorMsg 			= "";
$action				= getValue("actions", "str", "POST", "");
$code					= getValue("code", "str", "POST", "");
$simcard				= getValue("simcard", "str", "POST", "");
$simcard				= substr($simcard,0,15);
$hoten				= getValue("hoten", "str", "POST", "");
$hoten				= strip_tags($hoten);
$hoten				= substr($hoten,0,25);
$hoten 				= preg_replace("/[0-9]/si"," ",$hoten);

$hoten_check = RemoveSign($hoten);
$hoten_check = explode(' ',$hoten_check);
if(strlen($hoten_check[0]) >= 8){
	echo "<script language='javascript'>alert('" . tdt("Vui lòng kiểm tra lại họ tên") . "')</script>";
	redirect($fs_redirect);	
	exit();
}

#+
$myform = new generate_form();
//$myform->removeHTML(0);
#+
$myform->addTable($fs_table);
#+
$myform->add("simcard","simcard",0,1,"",1,"Điền số cần mua",0,"");
$myform->add("giaban","giaban",0,0,"",1,"Điền giá mua",0,"");
$myform->add("dienthoai","dienthoai",0,0,"",1,"Điền số điện thoại",0,"");
$myform->add("hoten","hoten",0,1,"",1,"Điền họ tên",0,"");
$myform->add("diachi","diachi",0,0,"",0,"",0,"");
$myform->add("dtban","dtban",0,0,"",0,"",0,"");
$myform->add("thanhpho","thanhpho",0,0,"",0,"",0,"");
$myform->add("noidung","noidung",0,0,"",0,"",0,"");
$myform->add("phanloai","phanloai",1,1,2,0,"",0,"");
$myform->add("thoigian","thoigian",0,1,"",0,"",0,"");
// $myform->addjavasrciptcode('if(document.getElementById("code").value == ""){ alert("Điền mã xác nhận !"); document.getElementById("code").focus(); return false;}');

#+
#+ Neu co gui form thi thuc hien
if($action == "submitForm"){
	$errorMsg .= $myform->checkdata(); //Check Error!
    $errorMsg .= $myform->strErrorFeld ;    //Check Error!
    if($errorMsg == ""){
        $db_ex = new db_execute($myform->generate_insert_SQL());
        ?>
        <script type="text/javascript">
        $(function(){
            $("#box_contact_success").removeClass("hidden");
            $("#frm_buy_request").remove();
        });
        </script>
        <?
        //echo $myform->generate_insert_SQL();
        // echo '<script type="text/javascript">alert("Đặt mua sim thành công - Quý khách vui lòng đợi để cửa hàng kiểm duyệt")</script>';
        // redirect($fs_redirect);
        // exit();
    }else{
        echo '<script type="text/javascript">alert("' . $errorMsg . '")</script>';
    }
	
} // End if($action == "submitForm"){

//add form for javacheck
$myform->addFormname("submitForm");
$myform->checkjavascript();
$myform->evaluate();
//*/
?>
<style>
.text{
	margin-top: 10px;
}
.text input {
    width: calc(100% - 105px);
}
.text label {
    width: 100px;
}
.text label .tRed {
    color: #F00;
}
</style>
<article class="buy_request main_static_pjax">
    
    <h1 class="ss_title">Đặt sim số đẹp theo yêu cầu</h1>
    <div class="dtSimContact hidden" id="box_contact_success">
        <div class="btn_success">
            Đã đặt hàng thành công
        </div>
        <p>Chúng tôi sẽ kiểm tra đơn hàng <br> và chủ động liên hệ với quý khách trong thời gian sớm nhất</p>
        <p>Xin chân thành cảm ơn!</p>
    </div>
    <form name="submitForm" action="" method="post" id="frm_buy_request">
        <input class="form-control" type="hidden" name="actions" value="submitForm" />
        
        <ul>
            <li class="text">
                <label for="simcard">Đặt số : <span class="tRed">*</span></label>
                <input class="form-control" type="text" name="simcard" id="simcard" placeholder="Nhập số cần đặt mua" value="<?=$simcard?>" />
            </li>
            <li class="text">
                <label for="giaban">Giá mua : <span class="tRed">*</span></label>
                <input class="form-control" type="text" name="giaban" id="giaban" placeholder="Nhập giá có thể mua" value="<?=$giaban?>" />
            </li>
            <li class="text">
                <label for="dienthoai">Di động : <span class="tRed">*</span></label>
                <input class="form-control" type="text" name="dienthoai" id="dienthoai" placeholder="Nhập số điện thoại có thể liên hệ với bạn" value="<?=$dienthoai?>" />
            </li>
            <li class="text">
                <label for="hoten">Họ tên : <span class="tRed">*</span></label>
                <input class="form-control" type="text" name="hoten" id="hoten" value="<?=$hoten?>" placeholder="Nhập họ tên của bạn" />
            </li>
            <li class="text">
                <label for="diachi"> Địa chỉ : </label>
                <input class="form-control" type="text" name="diachi" id="diachi" value="<?=$diachi?>" placeholder="Nhập địa chỉ của bạn" />
            </li>
            <li class="text text-center">
                <button class="btn btn-success" style="width: 118px;" onclick="validateForm();">Đặt số</button>
            </li>
        </ul>
    </form>
</article>