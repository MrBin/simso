<? #include('inc_bot_event.php')?>
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h3 class="modal-title" id="myModalLabel">Siêu thị sim thẻ - Kho sim số đẹp lớn nhất toàn quốc</h3>
            </div>
            <div class="modal-body"></div>
        </div>
    </div>
</div>
<div id="right_bar">
	<div>
    	<a href="https://www.facebook.com/khosimthe" target="_blank" title="Facebook ">
        	<button class="btn btn btn-primary" data-toggle="tooltip" data-placement="left" title="Tìm sim giúp bạn"> <i class="icon icon-plus"></i> </button>
        </a>
    </div>
    <div>
        <button class="btn btn btn-primary modal-click-load" data-href="/73-truong-chinh.html" data-toggle="tooltip" data-placement="left" title="Liên hệ"> <i class="icon icon-envelope"></i> </button>
    </div>
    <div>
        <button class="btn btn btn-primary modal-click-load" data-href="/huong-dan-chung.htm" data-toggle="tooltip" data-placement="left" title="Hướng dẫn đặt sim"> <i class="icon icon-comment"></i> </button>
    </div>
    <div>
        <button class="btn btn btn-primary modal-click-load" data-href="/thanh-toan-qua-ngan-hang.htm" data-toggle="tooltip" data-placement="left" title="Hướng dẫn thanh toán"> <i class="icon icon-tasks"></i> </button>
    </div>
    <div>
        <button id="go-to-top" class="btn btn btn-primary" data-toggle="tooltip" data-placement="left" title="Lên trên"> <i class="icon icon-chevron-up"></i> </button>
    </div>
</div>
<div id="footer_bar">
	<div class="main">
		<div class="tit-hotline"> Liên hệ <span class="arrow-hotline"></span> </div>
		<div class="hotline" style="float: left;">
			<span style="display: inline-block; margin-top: -10px; color: #0093DD; font-weight: bold; vertical-align: text-top;">(Thời gian làm việc từ 8h - 18h)</span>
			<span class="phone">0989 575.575</span>&nbsp;&nbsp;&nbsp;-&nbsp;&nbsp;&nbsp;<span class="phone">0932 33.8888</span>
		</div>
		<div style="float: left; margin-left: 10px;">
			<a href="http://online.gov.vn/HomePage/CustomWebsiteDisplay.aspx?DocId=13147" rel="nofollow" target="_blank">
				<span style="display: inline-block; margin-top: -30px; vertical-align: top;"><img src="/lib/img/bo_cong_thuong_new.png" /></span>
			</a>
		</div>
		<div class="clearfix"></div>
	</div>
	
</div>
<div class="footer_bar_space"></div>

<? include('inc_bot_floatbanner.php')?>
<? #include('inc_bot_event_tet.php')?>

<style>
.animated {
	-webkit-animation-duration: 1s;
	   -moz-animation-duration: 1s;
	     -o-animation-duration: 1s;
	        animation-duration: 1s;
	-webkit-animation-fill-mode: both;
	   -moz-animation-fill-mode: both;
	     -o-animation-fill-mode: both;
	        animation-fill-mode: both;
}
@-webkit-keyframes flash {
	0%, 50%, 100% {opacity: 1;}
	25%, 75% {opacity: 0;}
}

@-moz-keyframes flash {
	0%, 50%, 100% {opacity: 1;}
	25%, 75% {opacity: 0;}
}

@-o-keyframes flash {
	0%, 50%, 100% {opacity: 1;}
	25%, 75% {opacity: 0;}
}

@keyframes flash {
	0%, 50%, 100% {opacity: 1;}
	25%, 75% {opacity: 0;}
}

.animated.flash {
	-webkit-animation-name: flash;
	-moz-animation-name: flash;
	-o-animation-name: flash;
	animation-name: flash;
}
@-webkit-keyframes bounce {
	0%, 20%, 50%, 80%, 100% {-webkit-transform: translateY(0);}
	40% {-webkit-transform: translateY(-30px);}
	60% {-webkit-transform: translateY(-15px);}
}

@-moz-keyframes bounce {
	0%, 20%, 50%, 80%, 100% {-moz-transform: translateY(0);}
	40% {-moz-transform: translateY(-30px);}
	60% {-moz-transform: translateY(-15px);}
}

@-o-keyframes bounce {
	0%, 20%, 50%, 80%, 100% {-o-transform: translateY(0);}
	40% {-o-transform: translateY(-30px);}
	60% {-o-transform: translateY(-15px);}
}
@keyframes bounce {
	0%, 20%, 50%, 80%, 100% {transform: translateY(0);}
	40% {transform: translateY(-30px);}
	60% {transform: translateY(-15px);}
}
.animated.bounce {
	-webkit-animation-name: bounce;
	-moz-animation-name: bounce;
	-o-animation-name: bounce;
	animation-name: bounce;
}
#footer_bar{
	background: transparent;
	border: none;
}
#footer_bar .main{
	background: #EEE;
	border: solid 1px #DDD;
	border-left: none;
	float: none;
	margin: auto;
	width: 975px;
}
#footer_bar .tit-hotline{
	padding: 0 30px;
}
#footer_bar .hotline{
	float: none;
	margin: auto;
	width: 520px;
}
</style>
<script src="/lib/js/bot.js" type="text/javascript"></script>
<? 
#!function(t){function n(n,e,i,r){var c=n.text().split(e),s="";c.length&&(t(c).each(function(t,n){s+='<span class="'+i+(t+1)+'">'+n+"</span>"+r}),n.empty().append(s))}var e={init:function(){return this.each(function(){n(t(this),"","char","")})},words:function(){return this.each(function(){n(t(this)," ","word"," ")})},lines:function(){return this.each(function(){var e="eefec303079ad17405c889e092e105b0";n(t(this).children("br").replaceWith(e).end(),e,"line","")})}};t.fn.lettering=function(n){return n&&e[n]?e[n].apply(this,[].slice.call(arguments,1)):"letters"!==n&&n?(t.error("Method "+n+" does not exist on jQuery.lettering"),this):e.init.apply(this,[].slice.call(arguments,0))}}(jQuery);
#!function(t){"use strict";function e(e){return/In/.test(e)||t.inArray(e,t.fn.textillate.defaults.inEffects)>=0}function n(e){return/Out/.test(e)||t.inArray(e,t.fn.textillate.defaults.outEffects)>=0}function i(t){return"true"!==t&&"false"!==t?t:"true"===t}function a(e){var n=e.attributes||[],a={};return n.length?(t.each(n,function(t,e){var n=e.nodeName.replace(/delayscale/,"delayScale");/^data-in-*/.test(n)?(a.in=a.in||{},a.in[n.replace(/data-in-/,"")]=i(e.nodeValue)):/^data-out-*/.test(n)?(a.out=a.out||{},a.out[n.replace(/data-out-/,"")]=i(e.nodeValue)):/^data-*/.test(n)&&(a[n]=i(e.nodeValue))}),a):a}function s(t){for(var e,n,i=t.length;i;e=parseInt(Math.random()*i),n=t[--i],t[i]=t[e],t[e]=n);return t}function l(t,e,n){t.addClass("animated "+e).css("visibility","visible").show(),t.one("animationend webkitAnimationEnd oAnimationEnd",function(){t.removeClass("animated "+e),n&&n()})}function o(i,a,o){var r=i.length;return r?(a.shuffle&&(i=s(i)),a.reverse&&(i=i.toArray().reverse()),void t.each(i,function(i,s){function c(){e(a.effect)?u.css("visibility","visible"):n(a.effect)&&u.css("visibility","hidden"),r-=1,!r&&o&&o()}var u=t(s),f=a.sync?a.delay:a.delay*i*a.delayScale;u.text()?setTimeout(function(){l(u,a.effect,c)},f):c()})):void(o&&o())}var r=function(i,s){var l=this,r=t(i);l.init=function(){l.$texts=r.find(s.selector),l.$texts.length||(l.$texts=t('<ul class="texts"><li>'+r.html()+"</li></ul>"),r.html(l.$texts)),l.$texts.hide(),l.$current=t("<span>").text(l.$texts.find(":first-child").html()).prependTo(r),e(s.in.effect)?l.$current.css("visibility","hidden"):n(s.out.effect)&&l.$current.css("visibility","visible"),l.setOptions(s),l.timeoutRun=null,setTimeout(function(){l.options.autoStart&&l.start()},l.options.initialDelay)},l.setOptions=function(t){l.options=t},l.triggerEvent=function(e){var n=t.Event(e+".tlt");return r.trigger(n,l),n},l.in=function(i,s){i=i||0;var r,c=l.$texts.find(":nth-child("+(i+1)+")"),u=t.extend(!0,{},l.options,c.length?a(c[0]):{});c.addClass("current"),l.triggerEvent("inAnimationBegin"),l.$current.text(c.html()).lettering("words"),l.$current.find('[class^="word"]').css({display:"inline-block","-webkit-transform":"translate3d(0,0,0)","-moz-transform":"translate3d(0,0,0)","-o-transform":"translate3d(0,0,0)",transform:"translate3d(0,0,0)"}).each(function(){t(this).lettering()}),r=l.$current.find('[class^="char"]').css("display","inline-block"),e(u.in.effect)?r.css("visibility","hidden"):n(u.in.effect)&&r.css("visibility","visible"),l.currentIndex=i,o(r,u.in,function(){l.triggerEvent("inAnimationEnd"),u.in.callback&&u.in.callback(),s&&s(l)})},l.out=function(e){var n=l.$texts.find(":nth-child("+(l.currentIndex+1)+")"),i=l.$current.find('[class^="char"]'),s=t.extend(!0,{},l.options,n.length?a(n[0]):{});l.triggerEvent("outAnimationBegin"),o(i,s.out,function(){n.removeClass("current"),l.triggerEvent("outAnimationEnd"),s.out.callback&&s.out.callback(),e&&e(l)})},l.start=function(t){l.triggerEvent("start"),function e(t){l.in(t,function(){var n=l.$texts.children().length;t+=1,!l.options.loop&&t>=n?(l.options.callback&&l.options.callback(),l.triggerEvent("end")):(t%=n,l.timeoutRun=setTimeout(function(){l.out(function(){e(t)})},l.options.minDisplayTime))})}(t||0)},l.stop=function(){l.timeoutRun&&(clearInterval(l.timeoutRun),l.timeoutRun=null)},l.init()};t.fn.textillate=function(e,n){return this.each(function(){var i=t(this),s=i.data("textillate"),l=t.extend(!0,{},t.fn.textillate.defaults,a(this),"object"==typeof e&&e);s?"string"==typeof e?s[e].apply(s,[].concat(n)):s.setOptions.call(s,l):i.data("textillate",s=new r(this,l))})},t.fn.textillate.defaults={selector:".texts",loop:!1,minDisplayTime:2e3,initialDelay:0,"in":{effect:"fadeInLeftBig",delayScale:1.5,delay:50,sync:!1,reverse:!1,shuffle:!1,callback:function(){}},out:{effect:"hinge",delayScale:1.5,delay:50,sync:!1,reverse:!1,shuffle:!1,callback:function(){}},autoStart:!0,inEffects:[],outEffects:["hinge"],callback:function(){}}}(jQuery);
/*/
$(function(){
	
	$('#footer_bar .hotline .phone').eq(1).textillate({
		loop: true,
		in: { effect: 'bounce' },
		out: { effect: 'flash' }
	})
})
//*/ 
?>