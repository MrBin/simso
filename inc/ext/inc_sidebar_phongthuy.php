<div class="panel panel-sidebar">
   <div class="panel-body box_filter" id="filter_top">
      <ul>
         <li data-checked="1">
            <a data-pjax="#main_left" href="/sim-phong-thuy.html" rel="nofollow" title="Sim phong thủy">
               <i class="icon_check"></i>
               <span>Sim phong thủy</span>
            </a>
         </li>
         <li data-checked="2">
            <a data-pjax="#main_left" href="/xem-sim-hop-tuoi.html" title="Xem sim hợp tuổi">
               <i class="icon_check"></i>
               <span>Xem sim hợp tuổi</span>
            </a>
         </li>
      </ul>
   </div>
</div>
<div class="panel panel-sidebar">
   <div class="panel-heading">
      <h2 class="panel-title">
         <i class="icon icon-filter"></i>
         SIM HỢP MỆNH
      </h2>
      <div class="clearfix"></div>
   </div>
   <div class="panel-body box_filter" id="filter_menh">
      <ul>
         <li data-checked="1">
            <a data-pjax="#main_left" href="/sim-phong-thuy-hop-menh-kim/" title="Mệnh Kim">
               <i class="icon_check"></i>
               <span>Mệnh Kim</span>
            </a>
         </li>
         <li data-checked="2">
            <a data-pjax="#main_left" href="/sim-phong-thuy-hop-menh-moc/" title="Mệnh Mộc">
               <i class="icon_check"></i>
               <span>Mệnh Mộc</span>
            </a>
         </li>
         <li data-checked="3">
            <a data-pjax="#main_left" href="/sim-phong-thuy-hop-menh-thuy/" title="Mệnh Thủy">
               <i class="icon_check"></i>
               <span>Mệnh Thủy</span>
            </a>
         </li>
         <li data-checked="4">
            <a data-pjax="#main_left" href="/sim-phong-thuy-hop-menh-hoa/" title="Mệnh Hỏa">
               <i class="icon_check"></i>
               <span>Mệnh Hỏa</span>
            </a>
         </li>
         <li data-checked="5">
            <a data-pjax="#main_left" href="/sim-phong-thuy-hop-menh-tho/" title="Mệnh Thổ">
               <i class="icon_check"></i>
               <span>Mệnh Thổ</span>
            </a>
         </li>
      </ul>
   </div>
</div>
<div class="panel panel-sidebar">
   <div class="panel-heading">
      <h2 class="panel-title">
         <i class="icon icon-filter"></i>
         SIM HỢP TUỔI
      </h2>
      <div class="clearfix"></div>
   </div>
   <div class="panel-body box_filter" id="filter_old">
      <ul>
         <li data-checked="1">
            <a data-pjax="#main_left" href="https://sieuthisimthe.com/sim-hop-tuoi-ty" title="Sim hợp tuổi Tý">
               <i class="icon_check"></i>
               <span>Sim hợp tuổi Tý</span>
            </a>
         </li>
         <li data-checked="2">
            <a data-pjax="#main_left" href="https://sieuthisimthe.com/sim-hop-tuoi-suu" title="Sim hợp tuổi Sửu">
               <i class="icon_check"></i>
               <span>Sim hợp tuổi Sửu</span>
            </a>
         </li>
         <li data-checked="3">
            <a data-pjax="#main_left" href="https://sieuthisimthe.com/sim-hop-tuoi-dan" title="Sim hợp tuổi Dần">
               <i class="icon_check"></i>
               <span>Sim hợp tuổi Dần</span>
            </a>
         </li>
         <li data-checked="4">
            <a data-pjax="#main_left" href="https://sieuthisimthe.com/sim-hop-tuoi-mao" title="Sim hợp tuổi Mão">
               <i class="icon_check"></i>
               <span>Sim hợp tuổi Mão</span>
            </a>
         </li>
         <li data-checked="5">
            <a data-pjax="#main_left" href="https://sieuthisimthe.com/sim-hop-tuoi-thin" title="Sim hợp tuổi Thìn">
               <i class="icon_check"></i>
               <span>Sim hợp tuổi Thìn</span>
            </a>
         </li>
         <li data-checked="6">
            <a data-pjax="#main_left" href="https://sieuthisimthe.com/sim-hop-tuoi-t%E1%BB%B5" title="Sim hợp tuổi Tị">
               <i class="icon_check"></i>
               <span>Sim hợp tuổi Tị</span>
            </a>
         </li>
         <li data-checked="7">
            <a data-pjax="#main_left" href="https://sieuthisimthe.com/sim-hop-tuoi-ngo" title="Sim hợp tuổi Ngọ">
               <i class="icon_check"></i>
               <span>Sim hợp tuổi Ngọ</span>
            </a>
         </li>
         <li data-checked="8">
            <a data-pjax="#main_left" href="https://sieuthisimthe.com/sim-hop-tuoi-mui" title="Sim hợp tuổi Mùi">
               <i class="icon_check"></i>
               <span>Sim hợp tuổi Mùi</span>
            </a>
         </li>
         <li data-checked="9">
            <a data-pjax="#main_left" href="https://sieuthisimthe.com/sim-hop-tuoi-than" title="Sim hợp tuổi Thân">
               <i class="icon_check"></i>
               <span>Sim hợp tuổi Thân</span>
            </a>
         </li>
         <li data-checked="10">
            <a data-pjax="#main_left" href="https://sieuthisimthe.com/sim-hop-tuoi-dau" title="Sim hợp tuổi Dậu">
               <i class="icon_check"></i>
               <span>Sim hợp tuổi Dậu</span>
            </a>
         </li>
         <li data-checked="11">
            <a data-pjax="#main_left" href="https://sieuthisimthe.com/sim-hop-tuoi-tuat" title="Sim hợp tuổi Tuất">
               <i class="icon_check"></i>
               <span>Sim hợp tuổi Tuất</span>
            </a>
         </li>
         <li data-checked="12">
            <a data-pjax="#main_left" href="https://sieuthisimthe.com/sim-hop-tuoi-hoi" title="Sim hợp tuổi Hợi">
               <i class="icon_check"></i>
               <span>Sim hợp tuổi Hợi</span>
            </a>
         </li>
      </ul>
   </div>
</div>
<div class="panel panel-sidebar">
   <div class="panel-heading">
      <h2 class="panel-title">
         <i class="icon icon-filter"></i>
         SIM HỢP NĂM SINH
      </h2>
      <div class="clearfix"></div>
   </div>
   <div class="panel-body box_filter" id="filter_birthyear">
      <ul>
         <?
         for($i=date("Y");$i>=1960;$i--){
            ?>
            <li>
               <a href="/sim-phong-thuy-hop-tuoi-<?=$i?>/" title="Sim phong thủy hợp tuổi <?=$i?>" data-pjax="body" rel="nofollow" data-ajax="#main"> 
                  <i class="icon_check"></i>
                  <span>Sim hợp tuổi <?=$i?></span>
               </a> 
            </li>
            <? 
         }
         ?>
      </ul>
   </div>
</div>