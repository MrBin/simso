<?
#+
#+ Set bien
$page_size		= 55;
$sqlSelect		= '';
$sqlJoin			= '';
$sql 				= '';
$sqlOrder 		= '';
$sqlGroup 		= '';
$sqlOrderBy 	= '';
$sqlLimit		= '';
#+
$normal_class    	= "";
$selected_class  	= "active";
$page_prefix 	 	= "Trang";
$current_page    	= ( getValue("page") < 1 ) ? 1 : getValue("page");
#+

$keyword    = "*" . substr($sProName, -6);
$keywordSix = substr($sProName, -6);

/* Khai bao bien */
$searchConfig =  array ("page"		=> $current_page,
								"page_size"	=> $page_size,
								);

/* Thuc hien search */
$sphinx_search = new sphinx_search($keyword);
$arrResult     = $sphinx_search->search($searchConfig);

$arrQuery		= array();
$total_record	= 0;

if(!empty($arrResult["data"])){
	$arrQuery		= $arrResult["data"];
	$total_record	= $arrResult["total_record"];
}

if($total_record > 0){
	#+
	#+ Xem voi so luong sim nhu vay co bao nhieu trang
	if ($total_record % $page_size == 0){
		$num_of_page = $total_record / $page_size;
	}else{
		$num_of_page = (int)($total_record / $page_size) + 1;
	} // End if ($total_record % $page_size == 0)

	#+ 
	#+ Neu nhu co nguoi co tinh dien page lon hon so page hien tai
	if($current_page > $num_of_page) return;

	#+
	#+ Header Sim
	$str_head    = _temp::_simHead();
	$str_display = '';
	$str         = '';
	
	$str .= '
			<table class="table table-hover table-sim">
				'.$str_head.'
				<tbody>
			';
	unset($str_head);
	
	$i=($current_page-1) * $page_size;
	foreach($arrQuery as $key => $row){
		$i++;
		
		#+
		$str .= _temp::_simBody($row,$i);
	} // End while($row = mysql_fetch_assoc($db_simType->result))
	unset($arrQuery);
	
	
	$str .= '
				</tbody>
			</table>
			';

	if($total_record>$page_size){
		
		$str .= '<div class="text-center">
						<ul class="pagination">
							'.generatePageBar($page_prefix,$cache_page,$page_size,$total_record,$url,$normal_class,$selected_class,$query_string,'rel="nofollow"').'
						</ul>
					</div>
					';

	} // ENd if($total_record>$page_size)

	$str_display = $str;
	?>
	<div class="sim_detail_search type-sim">
		<div class="border_line"></div>
		<h3>Mời bạn tham khảo số gần giống đang còn trong kho</h3>
		<?=$str_display?>
		<div class="btn_see_more">
			<a href="/tim-sim/*<?=$keywordSix?>.html">Xem tất cả sim đuôi <?=$keywordSix?></a>
		</div>
	</div>
	<?
	unset($str_display);
	unset($str);
}
else{
	echo '<div class="sim_detail_search type-sim">
				<div class="border_line"></div>
				<div class="bgEF tB tRed tS15" align="center" style="padding:20px;">
					<div class="no_result">Sim số <b>' . $sProName . '</b> hiện chưa được cập nhật trên hệ thống !</div>
					<div class="no_result_sub">Qúy Khách có thể liên hệ: <b>0909.575.575</b> đặt mua trực tiếp <br> hoặc <a href="/dat-sim-theo-yeu-cau.html" rel="nofollow" title="Đặt Sim Theo Yêu Cầu">Đặt Sim Theo Yêu Cầu.</a></div>
				</div>
			</div>';
}

?>
