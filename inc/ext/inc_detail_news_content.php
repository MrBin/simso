<?
$new_title = $row['new_title'];
$description = $row["new_description"];

$strHeading = '';
if(isset($row["new_title_heading"]) && $row["new_title_heading"] != ""){
    $arrHeading = json_decode(base64_url_decode($row["new_title_heading"]), 1);
    if(is_array($arrHeading) && count($arrHeading) > 0){
        foreach ($arrHeading as $key => $value) {
            if(isset($value["id"]) && isset($value["name"])){
                $strHeading .= '<li><a href="#' . $value["id"] . '" title="' . $value["name"] . '" >' . $value["name"] . '</a></li>';
                if(isset($value["child"]) && is_array($value["child"]) && count($value["child"]) > 0){
                    foreach ($value["child"] as $key => $value) {
                        $strHeading .= '<li class="sub_item"><a href="#' . $value["id"] . '" title="' . $value["name"] . '">' . $value["name"] . '</a></li>';
                    }
                }
            }
        }
    }
}


if($module == 'tin-tuc-ko-dau'){
	$new_title 		= removeSeo($row['new_title']);
	$description 	= removeSeo($row["new_description"]);		
}

// Replace ảnh lazy load
$arrPicContent = array();

preg_match_all('~<img.*?src=["\']+(.*?)["\']+~', $description, $match);
$arrPicContent = isset($match[1]) ? $match[1] : $arrPicContent;

if(!empty($arrPicContent)){
    foreach ($arrPicContent as $key => $value) {
        $description = str_replace('src="' . $value . '"', 'src="' . $path_lib . 'img/dot.gif" class="b-lazy" data-src="' . $value . '"', $description);
    }
}
?>
<style type="text/css">
#toc_container ul li.sub_item{
   padding-left: 10px;
}
.anchor{
   margin-bottom: 15px;
}
.anchor .tc_head{
   width: 100%;
   background: #3276B1;
   height: 40px;
   line-height: 40px;
   text-align: center;
   color: #FFF;
}
.tc_head h2{
   font-size: 14px;
   margin: 0;
   color: #FFF;
   display: inline-block;
   text-transform: uppercase;
}
.tc_head span{
   padding-left: 10px;
   cursor: pointer;
   line-height: 40px;
   color: #FFF;
}
.tc_head h4{
    font-size: 18px;
    margin: 0;
    color: #FFF;
    display: inline-block;
}
.tc_main{
   background-color: #eaecf1;
   padding: 10px;
   margin-bottom: 20px;
   clear: both;
   overflow: hidden;
}
.tc_main ul{
   padding: 0;
   list-style-type: circle;
   list-style-position: inside;
   clear: both;
   height: auto;
}
.tc_main ul li{
   margin-left: 5px;
   padding: 0 8px 8px 0;
}
.tc_main ul li a{
   line-height: 140%;
   color: #0052a5;
}

.tc_main ul li.sub_item{
   padding-left: 10px;
}
.tc_main ul li.sub_item a{
   color: #0052a5;
}
</style>
<h1>
	<?=$new_title?>
</h1>
<p class="date">
    <?
    if($row["new_date"] != ""){
        $date = getDateTime("vn", 1, 1, 1, "GMT+7", $row["new_date"]); 
        print_r($date);
    }
    ?>
</p>

<?
if($row["new_phongthuy"] == 1){
?>
<div class="type-phong-thuy">
    <div class="header">
        <div class="logo"><img src="/lib/img/phong-thuy/logo.gif" alt="Logo Phong Thủy"></div>
        <div class="slogan"><img src="/lib/img/phong-thuy/slogan_choose.png" alt="Slogan Phong Thủy"></div>
    </div>

    <div class="nav">
        <ul class="nav-tabs nav-tabs-phong-thuy">
            <li class="active"><a href="#tra-cuu" data-toggle="tab">Tra cứu</a></li>
            <li><a href="#chia-se" data-toggle="tab">Chia sẻ</a></li>
        </ul>
        <div class="clearfix"></div>
    </div> 
   
    <div class="tab-content">
        <div class="tab-pane active" id="tra-cuu">
            <?
            // Google Captcha
            // require_once('../../kernel/classes/gcaptcha/autoload.php');
            // // Register API keys at https://www.google.com/recaptcha/admin
            // $siteKey     = '6LcUBCITAAAAAJBZuIfXTMQl77tyiJAAyn2e-JsA';
            // $secret      = '6LcUBCITAAAAAKIJTnEG3d7QHDuz08rQHCIpqNb-';
            // // reCAPTCHA supported 40+ languages listed here: https://developers.google.com/recaptcha/docs/language
            // $lang        = 'vi';
            // $g_captcha   = getValue('g-recaptcha-response', 'str', 'GET', '');

            //
            $arrGioSinh = array(
                0=>"Tý (23h -> 1h)" , 
                1=>"Sửu (1h -> 3h)" , 
                2=>"Dần (3h -> 5h)" , 
                3=>"Mão (5h -> 7h)" , 
                4=>"Thìn (7h -> 9h)" , 
                5=>"Tị (9h -> 11h)" , 
                6=>"Ngọ (11h -> 13h)" , 
                7=>"Mùi (13h -> 15h)" , 
                8=>"Thân (15h -> 17h)" , 
                9=>"Dậu (17h -> 19h)" , 
                10=>"Tuất (19h -> 21h)" , 
                11=>"Hợi (21h -> 23h)" , 
                );
                
            //  
            $arrNgaySinh = array(
                1=>1 , 2=>2 , 3=>3 , 4=>4 , 5=>5 ,
                6=>6 , 7=>7 , 8=>8 , 9=>9 , 10=>10 ,
                11=>11 , 12=>12 , 13=>13 , 14=>14 , 15=>15 ,
                16=>16 , 17=>17 , 18=>18 , 19=>19 , 20=>20 ,
                21=>21 , 22=>22 , 23=>23 , 24=>24 , 25=>25 ,
                26=>26 , 27=>27 , 28=>28 , 29=>29 , 30=>30 ,
                31=>31 ,    
                );
                
            //  
            $arrThangSinh = array(
                1=>1 , 2=>2 , 3=>3 , 4=>4 , 5=>5 ,
                6=>6 , 7=>7 , 8=>8 , 9=>9 , 10=>10 ,
                11=>11 , 12=>12
                );
                
            //
            $arrNamSinh = array();
            for($i = 1950; $i<=date('Y')-3; $i++){
                $arrNamSinh[$i] = $i;   
            }

            //
            $arrSex = array(
                "Nam"=>"Nam" , 
                "Nữ"=>"Nữ"
                );

            $submit             = getValue('action','str','POSTGET','');
            $gio_sinh       = getValue('giosinh','int','POSTGET','');
            $ngay_sinh      = getValue('ngaysinh','int','POSTGET','');
            $thang_sinh     = getValue('thangsinh','int','POSTGET','');
            $nam_sinh       = getValue('namsinh','int','POSTGET','');
            $gioi_tinh      = getValue('gioitinh','str','POSTGET','');

            if($module == 'simrandom'){
                $gioi_tinh      = "Nam";
                $gio_sinh           = rand(1,12);
                $ngay_sinh      = rand(1,28);
                $thang_sinh     = rand(1,12);
                $nam_sinh       = rand(1950,1999);      
            }
            ?>
            <form action="/home/vn/type.php" method="GET">
                <input type="hidden" name="type" value="static" />
                <input type="hidden" name="module" value="xem-sim-hop-tuoi" />
                <div class="body-tool" style="padding-top: 10px;">
                    <input type="hidden" name="module_check" id="module_check" value="xem-sim-hop-tuoi" />
                    
                    <table cellpadding="4" cellspacing="0" width="100%">
                        <tr>
                            <td class="text-bold">Giờ sinh:</td>
                            <td>
                                <select name="giosinh" id="giosinh" class="form-control">
                                    <option value="">Giờ</option>
                                    <? foreach($arrGioSinh as $kgio => $gio){?>
                                    <option value="<?=$kgio?>" <? if($gio_sinh == $kgio) echo 'selected="selected"'?>><?=$gio?></option>
                                    <? }?>
                                </select>
                            </td>
                        </tr>
                        <tr>
                           <td class="text-bold">Ngày sinh( Dương lịch):</td>
                           <td>
                                
                                <select name="ngaysinh" id="ngaysinh" class="form-control">
                                    <option selected="selected" value="">Ngày</option>               
                                    <? foreach($arrNgaySinh as $kngay => $ngay){?>
                                    <option value="<?=$kngay?>" <? if($ngay_sinh == $kngay) echo 'selected="selected"'?>><?=$ngay?></option>
                                    <? }?>     
                                </select>
                                
                                <select name="thangsinh" id="thangsinh" class="form-control">
                                    <option selected="selected" value="">Tháng</option>             
                                    <? foreach($arrThangSinh as $kthang => $thang){?>
                                    <option value="<?=$kthang?>" <? if($thang_sinh == $kthang) echo 'selected="selected"'?>><?=$thang?></option>
                                <? }?>  
                                </select>
                                
                                <select name="namsinh" id="namsinh" class="form-control">
                                    <option selected="selected" value="">Năm</option>           
                                    <? foreach($arrNamSinh as $knam => $nam){?>
                                    <option value="<?=$knam?>" <? if($nam_sinh == $knam) echo 'selected="selected"'?>><?=$nam?></option>
                                    <? }?>  
                                </select>       
                           </td>
                        </tr>
                        <tr>
                            <td class="text-bold">Giới tính</td>
                            <td> 
                                <select name="gioitinh" id="gioitinh" class="form-control">
                                    <option value="">Chọn</option>
                                    <? foreach($arrSex as $ksex => $sex){?>
                                    <option value="<?=$ksex?>" <? if($gioi_tinh == $ksex) echo 'selected="selected"'?>><?=$sex?></option>
                                    <? }?> 
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>
                                <input type="submit" name="submit" id="submit" value="Tìm sim hợp tuổi" class="btn btn-danger" style="font-size:18px; cursor:pointer; margin-bottom: 20px;" />
                                <input type="hidden" value="Xem" name="action">
                            </td>
                        </tr>
                    </table>
                </div>
            </form>
        </div>
    </div>
</div>
<?
}
?>
<p class="news_teaser"><?=$row["new_teaser"]?></p>
<?
// Nếu Heading khác rỗng thì show
if($strHeading != ""){
    ?>
    <div id="toc_container" class="anchor">
        <div class="tc_head">
            <h4>Nội dung chính trong bài</h4><span>[Ẩn]</span>
        </div>
        <div class="tc_main">
            <ul>
                <?=$strHeading;?>
            </ul>
        </div>
    </div>
    <?
}
?>
<div class="description">
	<?=$description?>
</div>
<div class="clearfix"></div>
<script type="text/javascript">
    $(function(){
        
        $('#toc_container span').click(function(){
            $('#toc_container .tc_main').toggleClass('hidden');
            if($('#toc_container .tc_main').hasClass('hidden'))
                $(this).text('[Xem]');
            else
                $(this).text('[Ẩn]');
        });
    });
</script>