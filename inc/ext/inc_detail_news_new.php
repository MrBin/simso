<?
$sql = '';

#+
// if($sProDate != 0){
// 	$sql .= ' AND new_date >= '.$sProDate;
// }else{
// 	$sql .= ' AND new_id > '.$iData;	
// } // End if($sProDate != 0)

#+
if($module == 'tin-tuc' || $module == 'tin-tuc-ko-dau'){
	$sql .= $sqlcategory;
}else{
	$sql .= ' AND cat_type = "'.$module.'"';		
} // End if($module == 'tin-tuc')

$query = ' SELECT *'
        .' FROM tbl_news '
        .' INNER JOIN tbl_category ON (cat_id = new_category)'
        .' WHERE tbl_category.lang_id = ' . $lang_id .' AND cat_active = 1 AND new_active = 1 AND new_id <> '.$iData.$sql
        .' ORDER BY new_date DESC'
        .' LIMIT 6'
		;
$db_news = new db_query($query);
$arrQuery = convert_result_set_2_array($db_news->result);
unset($db_news);
if(!empty($arrQuery)){
?>
    <div class="box_news_relate">
        <h5 class="titlerelate">Tin tức liên quan</h5>
        <ul class="newsrelate">
            <?
            foreach($arrQuery as $key => $row){
                $srcImg = $row["new_picture"] != '' ? $path_news . $row["new_title_index"] . '/' . $row["new_picture"] : $path_news . 'no_image.jpg';
                 $srcPicSmall = $path_lib . 'img/dot.gif';

            if($module == 'tin-tuc-ko-dau'){
                $new_title  = removeSeo($row["new_title"]);
                $lin_detail = createLink("detail_news_ko_dau",$row);
            }else{
                $new_title  = $row["new_title"];
                $lin_detail = createLink("detail_news",$row);
            }
            ?>
                <li>
                    <a href="<?=$lin_detail?>" title="<?=$new_title?>" class="linkimg">
                        <div class="tempvideo">
                            <img width="100" class="b-lazy" data-src="<?=$srcImg?>" src="<?=$srcPicSmall?>" alt="<?=$new_title?>">
                        </div>
                        <h3 class="t_ov_3"><?=$new_title?></h3>
                        <span class="timepost"><?=date("d/m/Y",$row["new_date"])?></span>
                    </a>
                </li>
                
            <?
            } // End while($row = mysql_fetch_assoc($db_query->result))             
            ?>
        </ul>
    </div>
<?
} // End if(mysql_num_rows($db_query->result))
?>