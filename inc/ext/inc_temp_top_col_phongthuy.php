<div class="col-md-12">
    <div class="panel panel-sim-cat">
        <div class="panel-heading">
        		<? if($con_site_h1 != ''){?>
				<h1 style="margin: 0; padding: 5px 0; font-size: 14px;"><?=strip_tags($con_site_h1)?></h1>
				<? }else{?>
				<h3 class="panel-title"><i class="icon icon-usd"></i> Tìm sim phong thủy hợp tuổi theo Năm Sinh</h3>
				<? }?>
            <div class="clearfix"></div>
        </div>
        <div class="panel-body">
            <ul>
                <?
                for($i=1950;$i<=2009;$i++){
                ?>
                    <li class="col-md-1">
                        <a href="/sim-phong-thuy-hop-tuoi-<?=$i?>/" title="Sim phong thủy hợp tuổi <?=$i?>" data-pjax="body" data-ajax="#main"> 
                            Năm <?=$i?>
                        </a> 
                    </li>
                <?	
                }
                ?>
            </ul>
        </div>
    </div>
</div>



<?
/*/
for($i=1950;$i<=2009;$i++){
	$nam_sinh = $i;
	
	$arrThongTin = getCanChi('01','06',$nam_sinh);

	#+
	#+ Kiem tra xem tuoi vuong duong hay vuong am
	if($arrThongTin['can_nam_id']%2 == 0){
		$dau_tuoi = '+';
	}else{
		$dau_tuoi = '-';	
	} // End if($arrThongTin['can_nam_id']%2 == 0)
	$tuoi_duong_am = $arrThongTin['can_nam_id']%2 == 0 ? 'Dương' : 'Âm';
	$tuoi_duong_am_nguoc = $arrThongTin['can_nam_id']%2 == 0 ? 'Âm' : 'Dương';
	
	#+
	#+ Tinh ban menh
	$ban_menh_cot = $arrThongTin['can_nam_id']%2 == 0 ? $arrThongTin['can_nam_id']/2 : ($arrThongTin['can_nam_id']-1)/2;
	#+
	#+ $x la tim nam sinh dau tien trong hoa giap ung voi giap ty
	#+ $y de quay ve vong lap dau tien trong luc thap hoa giap
	#+ $z de xem chi dang o hang may trong bang luc thap hoa giap
	$x = abs($nam_sinh-4);
	$y = $x - 60*floor($x/60);
	$z = floor( $y / 10 );
	
	if($nam_sinh - 4 < 0)
		$ban_menh_hang =  5 - $z;
	else
		$ban_menh_hang =  $z;
		
	$ban_menh = $ban_menh_cot.$ban_menh_hang;
	$ban_menh = intval($ban_menh);
	
	#+
	#+
	$ban_menh_day_so_sinh = $arrBanMenh[$ban_menh]['menh_id'] == 1 ? 5 : $arrBanMenh[$ban_menh]['menh_id']-1;
	$ban_menh_day_so_khac = $arrBanMenh[$ban_menh]['menh_id'] >= 3 ? $arrBanMenh[$ban_menh]['menh_id']-2 : $arrBanMenh[$ban_menh]['menh_id']+3;
?>
	<div id="tooltip-spt-<?=$nam_sinh?>" class="hide">
    	<p class="tB tRed tS15" align="center">Sim phong thủy hợp tuổi cho người sinh năm <?=$nam_sinh?></p>
        - Bạn <b>sinh năm <?=$nam_sinh?></b> tức năm <b><?=$arrThongTin['can_nam']?> <?=$arrThongTin['chi_nam']?></b> Âm lịch
        <br />
        - Thuộc tuổi <b>vượng về <?=$tuoi_duong_am?></b> nên chọn cho mình những <b>số vượng về <?=$tuoi_duong_am_nguoc?></b> để bổ trợ hoặc những <b>số cân bằng âm dương</b> để trung hòa. <b>Tránh những số vượng <?=$tuoi_duong_am?></b> gây tình trạng thiên lệch.
        <br />
        - Thuộc <b>mệnh <?=$arrBanMenh[$ban_menh]['menh']?></b> (<?=$arrBanMenh[$ban_menh]['ten_han']?> - <?=$arrBanMenh[$ban_menh]['ten_dich']?>) nên chọn cho mình những dãy <b>số thuộc mệnh <?=$arrNguHanh[$ban_menh_day_so_sinh]?></b> để bổ trợ. <b>Tránh những số thuộc mệnh <?=$arrNguHanh[$ban_menh_day_so_khac]?></b> tương khắc với bản mệnh của bạn.
        <br />
        - <b>Chú ý</b> : Nên dùng công cụ để tra cứu xem bạn sinh năm nào theo âm lịch để đìm được đúng bản mệnh của bạn.
    </div>
<?	
}
//*/
?>