<div class="row">
	<div class="container_main">
		<div id="search_left">
			<div class="header_search">
				<input type="search" id="searchText" name="keyword" autocomplete="off" class="form-control searchText" value="<?=$keyword?>" placeholder="Nhập số bạn muốn tìm ..." data-placement="bottom"  title="Hướng dẫn tìm kiếm" /> <button class="btn btn-success" onclick="submit_form_v2('all');"><i class="icon icon-search"></i></button>
				<div id="search-popover" class="hide">
					+ Tìm sim có số <strong>6789</strong> bạn hãy gõ <strong>6789</strong> <br>
					+ Tìm sim có đầu <strong>098</strong> đuôi <strong>8888</strong> hãy gõ <strong>098*8888</strong> <br>
					+ Tìm sim bắt đầu bằng <strong>0989</strong> đuôi bất kỳ, hãy gõ: <strong>0989*</strong> <br>
				</div>
			</div>
			<div class="arrow-search"></div>		
		</div>
		<div class="search_right">
			<div><a class="btn btn-warning" href="/sim-gia-re-viettel/" title="Sim giá rẻ Viettel" data-pjax="#main_left">Sim giá rẻ Viettel</a></div>
			<div><a class="btn btn-danger" href="/sim-gia-re-vinaphone/" title="Sim giá rẻ Vinaphone" data-pjax="#main_left">Sim giá rẻ Vinaphone</a></div>
			<div><a class="btn btn-success" href="/sim-gia-re-mobifone/" title="Sim giá rẻ Mobifone" data-pjax="#main_left">Sim giá rẻ Mobifone</a></div>
			<div><a class="btn btn-info" href="/sim-gia-re-vietnamobile/" title="Sim giá rẻ Vietnamobile" data-pjax="#main_left">Sim giá rẻ Vietnamobile</a></div>
			<div class="clearfix"></div>
		</div>
	</div>
</div>
<?/*
<div class="row">
	<?
	if($module == 'sim-phong-thuy' || $module == 'simhoptuoi' || $module == 'simhopmenh'){
		include('inc_temp_top_col_phongthuy.php');	
	}else{
	?>
		<div class="col-md-4"><? include('inc_temp_top_col_loai.php');?></div>
		<div class="col-md-3" style="padding:0;"><? include('inc_temp_top_col_gia.php');?></div>
		<div class="col-md-5"><? include('inc_temp_top_col_dauso.php');?></div>
	<? 
	}
	?>    
</div>
*/?>