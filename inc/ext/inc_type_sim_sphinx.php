<?
/* Khai bao bien */
$searchConfig =  array ("page"		=> $current_page,
								"page_size"	=> $page_size,
								"iCat"		=> $iCat,
								"iType"		=> $iType,
								"iPrice"		=> $iPrice,
								"iDauSo"		=> $iDauSo,
								"iDaiLy"		=> $iDaiLy,
								"digit"		=> $digit,
								"iNguHanh"	=> $iNguHanh,
								"module"		=> $module,
								"sort"		=> $sort,
								);

/* Thuc hien search */
$sphinx_search = new sphinx_search($keyword);
$arrResult     = $sphinx_search->search($searchConfig);

$arrQuery		= array();
$total_record	= 0;

if(!empty($arrResult["data"])){
	$arrQuery		= $arrResult["data"];
	$total_record	= $arrResult["total_record"];
}
?>