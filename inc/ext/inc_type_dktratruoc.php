<?
#+
#+ Kiểm tra cache
$cache_check = 0; // 0.Chua cache - 1.Da cache
#+
if($con_set_cache != 0){
	#+
	$cache_file_time	= $cache_file_time_ext;
	#+ Ten file cache
	$cache_file_path	= 'tp_DangKyTraTruoc';
	#+ Folder cache
	$cache_folder_path	= '../../store/cache/all/ext/ext';
	
	#+
	#+ Kiem tra xem co file cache khong
	$sCache = new Cache($cache_file_path,$cache_file_time,'',$cache_folder_path);
	if($sCache->is_cache()) {
		$cache_check = 1;
	} // End if(!$sCache->is_cache()) 	
} // End if($con_set_cache == 0)

#+
#+ Neu nhu khong co cache thi thuc hien
if($cache_check == 0){
	#+
	$str = getStatic($con_static_dktratruoc);
	
	#+
	#+ Ghi cache
	if($con_set_cache != 0){
		$sCache->cache($str);	
	} // End if($con_set_cache != 0)
} // End if($cache_check == 1){
?>

<article>
	<div class="breadcrumb">
		Đăng ký sim trả trước
	</div>
	<div>
		<?
		#+
        #+ Xem co cache hay chua. Neu co thi dung lai
        if($cache_check == 0){
            echo $str;
        }else{
            echo $sCache->get_cache();	
        } // End if($cache_check == 0)
        unset($str);
		?>
	</div>	
</article>


<? 
/*/
$fs_table			= "tbl_yeucau";
$fs_redirect 		= $_SERVER['REQUEST_URI'];
$thoigian			= date("Y-m-d H:i:s");
$errorMsg 			= "";
$action				= getValue("actions", "str", "POST", "");
$code				= getValue("code", "str", "POST", "");

$arrTypeTraTruoc = array(
	1=>'Sim di động trả trước'
	,2=>'Sim dữ liệu D-com 3G'
	);

$loaidangky 		= getValue("loaidangky", "int", "POST", "1");
$simcard			= getValue("simcard", "str", "POST", "");
$serisim			= getValue("serisim", "str", "POST", "");
$hoten				= getValue("hoten", "str", "POST", "");
$ngaysinh			= getValue("ngaysinh", "int", "POST", "");
$thangsinh			= getValue("thangsinh", "int", "POST", "");
$namsinh			= getValue("namsinh", "int", "POST", "");
$socmt				= getValue("socmt", "str", "POST", "");
$socmt_noicap		= getValue("socmt_noicap", "str", "POST", "");
$socmt_ngay			= getValue("socmt_ngay", "int", "POST", "");
$socmt_thang		= getValue("socmt_thang", "int", "POST", "");
$socmt_nam			= getValue("socmt_nam", "int", "POST", "");
$dienthoai			= getValue("dienthoai", "str", "POST", "");

$noidung			= 'Loại đăng ký : '.$arrTypeTraTruoc[$loaidangky].'<br>';
$noidung			.= 'Số điện thoại đăng ký thông tin : '.$simcard.' - 4 số seri cuối của sim : '.$serisim.'<br>';
$noidung			.= 'Họ và tên : '.$hoten.'<br>';
$noidung			.= 'Ngày sinh : '.$ngaysinh.' - tháng '.$thangsinh.' - năm '.$namsinh.'<br>';
$noidung			.= 'Số CMT : '.$socmt.' - nơi cấp '.$socmt_noicap.'<br>';
$noidung			.= 'Ngày cấp CMT : '.$socmt_ngay.' - tháng '.$socmt_thang.' - năm '.$socmt_nam.'<br>';
$noidung			.= 'Điện thoại liên hệ : '.$dienthoai.'<br>';


//
$myform = new generate_form();
//$myform->removeHTML(0);

$myform->addTable($fs_table);

$myform->addjavasrciptcode('if(document.getElementById("loaidangky").value == ""){ alert("1. Chọn loại thuê bao đăng ký thông tin!"); document.getElementById("loaidangky").focus(); return false;}');
$myform->add("simcard","simcard",0,1,"",1,"Điền số điện thoại đăng ký thông tin",0,"");
$myform->addjavasrciptcode('if(document.getElementById("serisim").value == ""){ alert("2. 4 số cuối seri sim!"); document.getElementById("serisim").focus(); return false;}');
$myform->add("hoten","hoten",0,1,"",1,"Điền họ tên",0,"");
$myform->addjavasrciptcode('if(document.getElementById("ngaysinh").value == ""){ alert("4. Ngày sinh của bạn!"); document.getElementById("ngaysinh").focus(); return false;}');
$myform->addjavasrciptcode('if(document.getElementById("thangsinh").value == ""){ alert("4. Tháng sinh của bạn!"); document.getElementById("thangsinh").focus(); return false;}');
$myform->addjavasrciptcode('if(document.getElementById("namsinh").value == ""){ alert("4. Năm sinh của bạn!"); document.getElementById("namsinh").focus(); return false;}');
$myform->addjavasrciptcode('if(document.getElementById("socmt").value == ""){ alert("5. Số Chứng minh thư!"); document.getElementById("socmt").focus(); return false;}');
$myform->addjavasrciptcode('if(document.getElementById("socmt_noicap").value == ""){ alert("5. Nơi cấp chứng minh thư!"); document.getElementById("socmt_noicap").focus(); return false;}');
$myform->addjavasrciptcode('if(document.getElementById("socmt_ngay").value == ""){ alert("6. Ngày cấp chứng minh thư!"); document.getElementById("socmt_ngay").focus(); return false;}');
$myform->addjavasrciptcode('if(document.getElementById("socmt_thang").value == ""){ alert("6. Tháng cấp chứng minh thư!"); document.getElementById("socmt_thang").focus(); return false;}');
$myform->addjavasrciptcode('if(document.getElementById("socmt_nam").value == ""){ alert("6. Năm cấp chứng minh thư!"); document.getElementById("socmt_nam").focus(); return false;}');
$myform->add("dienthoai","dienthoai",0,1,"",0,"",0,"");
$myform->add("noidung","noidung",0,1,"",0,"",0,"");
$myform->add("phanloai","phanloai",1,1,4,0,"",0,"");
$myform->add("thoigian","thoigian",0,1,"",0,"",0,"");
$myform->addjavasrciptcode('if(document.getElementById("code").value == ""){ alert("Điền mã xác nhận !"); document.getElementById("code").focus(); return false;}');


#+
#+ Neu co gui form thi thuc hien
if($action == "submitForm"){
	#+
	#+ Khong thay co ma bao ve thi tro ve trang chu
	if(!isset($_SESSION["session_security_code"])) redirect("/");
	
	#+
	#+ Neu nhap dung ma bao ve thi thuc hien
	if($code == $_SESSION["session_security_code"]){
		#+
		#+ Kiem tra loi
		$errorMsg .= $myform->checkdata();	//Check Error!
		$errorMsg .= $myform->strErrorFeld ;	//Check Error!
		
		#+
		#+ neu khong co loi thi thuc hien
		if($errorMsg == ""){
			$db_ex = new db_execute($myform->generate_insert_SQL());
			//echo $myform->generate_insert_SQL();
			echo "<script language='javascript'>alert('Đăng ký trả trước thành công. Chúng tôi sẽ phản hồi lại trong thời gian sớm nhất')</script>";
			redirect($fs_redirect);
			exit();
		}else{
			echo "<script language='javascript'>alert('" . $errorMsg . "')</script>";
		}  // End if($errorMsg == ""){
		
		#+
		#+ Tao ra ma bao ve khac khi submit xong
		$_SESSION["session_security_code"] = rand(1000,9999);
	}else{
		echo "<script language='javascript'>alert('Mã xác nhận không chính xác')</script>";
	} // End if($code == $_SESSION["session_security_code"]){
		
} // End if($action == "submitForm"){

//add form for javacheck
$myform->addFormname("submitForm");
$myform->checkjavascript();
$myform->evaluate();



?>
<div class="tpSimContact">
	<p class="title">
		Đăng ký sim trả trước
	</p>
	<div class="content">
		<?
		#+
        #+ Xem co cache hay chua. Neu co thi dung lai
        if($cache_check == 0){
            echo $str;
        }else{
            echo $sCache->get_cache();	
        } // End if($cache_check == 0)
        unset($str);
		?>
		<center>
			<span class="tRed tB">
				Thông tin cung cấp dưới đây cần phải đầy đủ, chính xác để đảm bảo quá trình đăng ký thành công
			</span>
		</center>
		
		<form name="submitForm" action="" method="post">
			<input type="hidden" name="actions" value="submitForm" />		
			<ul class="dktt">
				<li>
					1. Loại thuê bao đăng ký thông tin :
					<select class="form" name="loaidangky" id="loaidangky">
						<option value="">---</option>
						<?
						foreach($arrTypeTraTruoc as $kType => $vType){
						?>
						<option value="<?=$kType?>"><?=$vType?></option>
						<?
						}
						?>
					</select>
				</li>
				<li>
					2. Số điện thoại đăng ký thông tin :
					<input name="simcard" id="simcard" value="<?=$dienthoai?>" type="text" />
				</li>
				<li>
					4 số seri cuối của Sim : 
					<input name="serisim" id="serisim" value="<?=$serisim?>" type="text" />
				</li>
				<li>
					3. Họ và tên (vd: Nguyen Van A):
					<input name="hoten" id="hoten" value="<?=$hoten?>" type="text" onkeyup="telexingVietUC(this,event)" />
				</li>
				<li>
					4. Ngày sinh : 
					<select class="form" name="ngaysinh" id="ngaysinh">
						<option value="">---</option>
						<?
						for($i=1;$i<=31;$i++){
						?>
						<option value="<?=$i?>"><?=$i <=9 ? '0'.$i : $i?></option>
						<?
						}
						?>
					</select>
					
					Tháng :
					<select class="form" name="thangsinh" id="thangsinh">
						<option value="">---</option>
						<?
						for($i=1;$i<=12;$i++){
						?>
						<option value="<?=$i?>"><?=$i <=9 ? '0'.$i : $i?></option>
						<?
						}
						?>
					</select>
					
					Năm :
					<select class="form" name="namsinh" id="namsinh">
						<option value="">---</option>
						<?
						for($i=(date('Y')-9);$i>=1920;$i--){
						?>
						<option value="<?=$i?>"><?=$i <=9 ? '0'.$i : $i?></option>
						<?
						}
						?>
					</select>
				</li>
				<li>
					5. Số CMT : 
					<input class="form" name="socmt" id="socmt" type="text" />
					Nơi cấp :
					<input class="form" name="socmt_noicap" id="socmt_noicap" type="text" />
				</li>
				<li>
					6. Ngày cấp chứng minh nhân dân: 
					<select class="form" name="socmt_ngay" id="socmt_ngay">
						<option value="">---</option>
						<?
						for($i=1;$i<=31;$i++){
						?>
						<option value="<?=$i?>"><?=$i <=9 ? '0'.$i : $i?></option>
						<?
						}
						?>
					</select>
					
					Tháng :
					<select class="form" name="socmt_thang" id="socmt_thang">
						<option value="">---</option>
						<?
						for($i=1;$i<=12;$i++){
						?>
						<option value="<?=$i?>"><?=$i <=9 ? '0'.$i : $i?></option>
						<?
						}
						?>
					</select>
					
					Năm :
					<select class="form" name="socmt_nam" id="socmt_nam">
						<option value="">---</option>
						<?
						for($i=date('Y');$i>=(date('Y')-16);$i--){
						?>
						<option value="<?=$i?>"><?=$i <=9 ? '0'.$i : $i?></option>
						<?
						}
						?>
					</select>
				</li>
				<li>
					7. Số điện thoại liên hệ (đối với sim D-com 3G): 
					<input name="dienthoai" id="dienthoai" class="form" type="text" />	
				</li>
				<li>
					8. Mã bảo vệ :
					<input type="text" name="code" id="code" size="10"/>&nbsp;
					<? $_SESSION["session_security_code"] = rand(1000,9999);?>
					<img src="/home/ext/securitycode.php" height="20" alt="Mã bảo vệ"/>
				</li>
				<li class="tCenter">
					
					<input type="button" value="Gửi đi" onclick="validateForm();" class="button" />
					<input type="reset" value="Làm lại" class="button" />
				</li>
			</ul>
		</form>
		
		<div class="tRed tCenter tB">
			Quý khách có thể gọi trực tiếp để được tư vấn !<br />
			<span class="tS15">0932 33.8888 - 0904 73.73.73</span>
		</div>
	</div>	
</div>
<?
//*/
?>