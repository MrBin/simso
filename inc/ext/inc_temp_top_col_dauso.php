<?
#+
#+ Kiểm tra cache
$cache_check = 0; // 0.Chua cache - 1.Da cache
#+
if($con_set_cache != 0){
	#+
	$cache_file_time	= $cache_file_time_ext;
	#+ Ten file cache
	$cache_file_path	= 'tpl_simDauSo';
	#+ Folder cache
	$cache_folder_path	= '../../store/cache/all/ext/ext';
	
	#+
	#+ Kiem tra xem co file cache khong
	$sCache = new Cache($cache_file_path,$cache_file_time,'',$cache_folder_path);
	if($sCache->is_cache()) {
		$cache_check = 1;
	} // End if(!$sCache->is_cache()) 	
} // End if($con_set_cache == 0)

#+
#+ Neu nhu khong co cache thi thuc hien
if($cache_check == 0){
	$str  = '';
	$str .= _temp::_simDauSo();

	#+
	#+ Ghi cache
	if($con_set_cache != 0){
		$sCache->cache($str);	
	} // End if($con_set_cache != 0)
} // End if($cache_check == 0){
?>
<style>
.panel-sim-digit .panel-body li{
	line-height: 2em !important;
}
</style>
<div class="panel panel-sim-cat panel-sim-digit">
    <div class="panel-heading">
        <h3 class="panel-title">
        	<i class="icon icon-file"></i>
        	Tìm nhanh theo đầu số
        </h3>
        <div class="pull-left" style="line-height:30px; font-size:14px; font-weight:bold; margin-left:20px;">
            <i class="icon icon-fire" style="color:#428BCA"></i>
			<a href="/dat-sim-theo-yeu-cau.html" title="Đặt theo yêu cầu">Đặt theo yêu cầu</a>
        </div>
        
        <div class="clearfix"></div>
    </div>
    <div class="panel-body">
    	<ul>
			<?
			#+
			#+ Xem co cache hay chua. Neu co thi dung lai
			if($cache_check == 0){
				echo $str;
			}else{
				echo $sCache->get_cache();	
			} // End if($cache_check == 0)
			unset($str);
			?>		
        </ul>
    </div>
</div>