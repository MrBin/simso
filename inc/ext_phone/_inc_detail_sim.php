<div class="breadcrumb"><?=$home_address?></div>
 
<article>
    <table class="table table-sim table-striped">
        <tbody>
            <tr>
                <td>Sim số đẹp</td>
                <td class="sim-digit text-primary"><strong><?=$sProName?></strong></td>
            </tr>
            <tr>
                <td>Giá bán</td>
                <td class="sim-price text-success text-bold"><?=number_format($sProPrice) != 0 ? number_format($sProPrice).' vnđ' : 'Sim đã bán'?></td>
            </tr>
            <tr>
                <td>Mạng di động</td>
                <td>
                    <a href="<?=$link_cat?>">
                        <img src="<?=$root_path_web.$path_category.$sCatPicture?>" alt="Sim so dep <?=$sCatName?>" itemprop="image" />
                    </a>
                </td>
            </tr>
            <tr>
                <td>Chi nhánh</td>
                <td><?=$sVenDorName?></td>
            </tr>
            <tr>
                <td>Tình trạng sim</td>
                <td>
                    <input type="checkbox" checked="checked" /> Bộ simcard nguyên Kit có sẵn tài khoản<br />
                    <input type="checkbox" checked="checked" /> Hưởng khuyến mãi hiện hành của nhà mạng<br />
                    <input type="checkbox" checked="checked" /> Khách hàng sẽ được đăng ký thông tin khi mua sim<br /> 
                </td>
            </tr>
        </tbody>
    </table>
    
    <div class="alert bg-info">
        <center>
        Quý khách có thể gọi trực tiếp để mua sim nhanh hơn
        <br />
        
            <span class="phone">0932 33.8888 - 0904 73.73.73</span>
        <br />
        Liên hệ qua Yahoo :
        <br />
        <a href="ymsgr:sendIM?hotromuasim73&amp;m=Chào bạn, tôi muốn mua sim số đẹp"><img alt="yahoo" height="25px" src="http://opi.yahoo.com/online?u=hotromuasim73&amp;t=2" width="125px"></a>
        <br />
        hotromuasim73@yahoo.com
        <br />
        Hoặc liên hệ với cửa hàng theo mẫu dưới
        </center>
    </div>
    
    <?
	$server_url = 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
	$server_url = removeQueryString($server_url);
	?>
	<div class="fb-like" data-href="<?=$server_url?>" data-layout="button_count" data-action="like" data-show-faces="false" data-share="true" style="margin-right:30px;"></div>
	<g:plusone size="medium" href="<?=$server_url?>"></g:plusone>
	<div class="fb-comments" data-href="<?=$server_url?>" data-numposts="5" data-colorscheme="light"></div>
    <script>
	// Facebook
	(function(d, s, id) {
	  var js, fjs = d.getElementsByTagName(s)[0];
	  if (d.getElementById(id)) return;
	  js = d.createElement(s); js.id = id;
	  js.src = "//connect.facebook.net/vi_VN/all.js#xfbml=1&appId=182834278554846";
	  fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));

	// Google Plus
	(function(){
	var po = document.createElement('script'); 
	po.type = 'text/javascript'; 
	po.async = true; 
	po.src = 'https://apis.google.com/js/plusone.js';
	var s = document.getElementsByTagName('script')[0]; 
	s.parentNode.insertBefore(po, s);
	})();
	</script>
    
    <?
    include("inc_detail_sim_datsim.php");
    ?>
</article>
