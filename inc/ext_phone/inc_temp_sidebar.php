<?/*<div id="fb-root"></div>
<?
if($type == 'sim' && $iData != 0){
include('inc_detail_sim_other.php');
}

# include('inc_temp_sidebar_search.php');
include('inc_temp_sidebar_simKhuyenMai.php');
include('inc_temp_sidebar_khachhang_slide.php');
# include('inc_temp_sidebar_simDacBiet.php');
include('inc_temp_sidebar_news.php');
include('inc_temp_sidebar_donhang.php');
# include('inc_temp_sidebar_stst.php');
include('inc_temp_sidebar_lienket.php');
*/?>
<?
$query = " SELECT * FROM tbl_category"
      ." WHERE cat_type = 'tin-tuc' AND cat_active = 1 AND cat_parent_id = 0"
      ." ORDER BY cat_order LIMIT 5"
      ;
$db_cat = new db_query($query);
$arrNews = convert_result_set_2_array($db_cat->result);
unset($db_cat);
?>
<div class="panel panel-sidebar">
   <div class="panel-body box_filter" id="filter_top">
      <ul>
         <li data-checked="1">
            <a data-pjax="#main_left" href="/sim-phong-thuy.html" rel="nofollow" title="Sim phong thủy">
               <i class="icon_check"></i>
               <span>Sim phong thủy</span>
            </a>
         </li>
         <li data-checked="2">
            <a data-pjax="#main_left" href="/xem-sim-hop-tuoi.html" title="Xem sim hợp tuổi">
               <i class="icon_check"></i>
               <span>Xem sim hợp tuổi</span>
            </a>
         </li>
      </ul>
   </div>
</div>
<div class="panel panel-sidebar">
   <div class="panel-heading">
      <h2 class="panel-title">
         <i class="icon icon-filter"></i>
         Sim theo giá
      </h2>
      <div class="clearfix"></div>
   </div>
   <div class="panel-body box_filter filter_price">
      <ul>
         <li data-checked="1">
            <a data-pjax="#main_left" href="/gia-0-200000/" title="Sim số đẹp giá dưới 200.000 đ">
               <i class="icon_check"></i>
               <span>Dưới 200.000 đ</span>
            </a>
         </li>
         <li data-checked="2">
            <a data-pjax="#main_left" href="/gia-200000-500000/" title="Sim số đẹp giá từ 2 trăm đến 5 trăm">
               <i class="icon_check"></i>
               <span>2 trăm - 5 trăm</span>
            </a>
         </li>
         <li data-checked="3">
            <a data-pjax="#main_left" href="/gia-500000-1000000/" title="Sim số đẹp giá từ 5 trăm đến 1 triệu">
               <i class="icon_check"></i>
               <span>5 trăm - 1 triệu</span>
            </a>
         </li>
         <li data-checked="4">
            <a data-pjax="#main_left" href="/gia-1000000-3000000/" title="Sim số đẹp giá từ 1 triệu đến 3 triệu">
               <i class="icon_check"></i>
               <span>1 triệu - 3 triệu</span>
            </a>
         </li>
         <li data-checked="5">
            <a data-pjax="#main_left" href="/gia-3000000-5000000/" title="Sim số đẹp giá từ 3 triệu đến 5 triệu">
               <i class="icon_check"></i>
               <span>3 triệu - 5 triệu</span>
            </a>
         </li>
         <li data-checked="6">
            <a data-pjax="#main_left" href="/gia-5000000-10000000/" title="Sim số đẹp giá từ 5 triệu đến 10 triệu">
               <i class="icon_check"></i>
               <span>5 triệu - 10 triệu</span>
            </a>
         </li>
         <li data-checked="7">
            <a data-pjax="#main_left" href="/gia-10000000-50000000/" title="Sim số đẹp giá từ 10 triệu đến 50 triệu">
               <i class="icon_check"></i>
               <span>10 triệu - 50 triệu</span>
            </a>
         </li>
         <li data-checked="8">
            <a data-pjax="#main_left" href="/gia-50000000-0/" title="Sim số đẹp trên 50 triệu">
               <i class="icon_check"></i>
               <span>Trên 50 triệu</span>
            </a>
         </li>
      </ul>
   </div>
</div>
<div class="panel panel-sidebar">
   <div class="panel-heading">
      <h2 class="panel-title">
         <i class="icon icon-filter"></i>
         Sim theo loại
      </h2>
      <div class="clearfix"></div>
   </div>
   <div class="panel-body box_filter filter_type">
      <?
      #+
      #+ Kiểm tra cache
      $cache_check = 0; // 0.Chua cache - 1.Da cache
      #+
      if($con_set_cache != 0){
         #+
         $cache_file_time  = $cache_file_time_ext;
         #+ Ten file cache
         $cache_file_path  = 'tpl_simType';
         #+ Folder cache
         $cache_folder_path   = '../../store/cache/all/ext/ext';
         
         #+
         #+ Kiem tra xem co file cache khong
         $sCache = new Cache($cache_file_path,$cache_file_time,'',$cache_folder_path);
         if($sCache->is_cache()) {
            $cache_check = 1;
         } // End if(!$sCache->is_cache())   
      } // End if($con_set_cache == 0)

      #+
      #+ Neu nhu khong co cache thi thuc hien
      if($cache_check == 0){
         $str  = '';
         $str .= _temp::_simType_v2(array(), $iType);

         #+
         #+ Ghi cache
         if($con_set_cache != 0){
            $sCache->cache($str);   
         } // End if($con_set_cache != 0)
      } // End if($cache_check == 1){
      ?>
      <ul>
         <?=$str?>
      </ul>
   </div>
</div>
<?
$query = " SELECT * FROM tbl_category"
      ." WHERE cat_type = 'sim' AND cat_active = 1"
      ." ORDER BY cat_order ASC LIMIT 6"
      ;
$db_cat = new db_query($query);
$arrCatSim = convert_result_set_2_array($db_cat->result);
unset($db_cat);

if(!empty($arrCatSim)){
?>
<div class="panel panel-sidebar">
   <div class="panel-heading">
      <h2 class="panel-title">
         <i class="icon icon-filter"></i>
         Sim theo mạng
      </h2>
      <div class="clearfix"></div>
   </div>
   <div class="panel-body box_filter">
      <ul>
         <?
         foreach ($arrCatSim as $key => $value) {
            $link_cat = createLink("cat",$value);
            $active   = ($value["cat_id"] == $iCat ? ' class="active"' : '');
         ?>
         <li<?=$active?>>
            <a href="<?=$link_cat?>">
               <i class="icon_check"></i>
               <span>Sim <?=$value["cat_name"]?></span>
            </a>
         </li>
         <?
         }
         ?>
      </ul>
   </div>
</div>
<?
}
?>

<div class="box_sim_select">
   <h2>
      <i class="icon icon-gift"></i>
      <a href="javascript:;" title="Sim ngày sinh">Sim ngày sinh</a>
   </h2>
   <div class="bss_main">
      <select name="frm_birth_day" id="frm_birth_day" class="form-control">
         <option value="0">Ngày</option>
         <?
         for($i = 1; $i<=31; $i++){
         ?>
         <option value="<?=$i?>"><?=$i?></option>
         <? 
         }
         ?>
      </select>
      <select name="frm_birth_month" id="frm_birth_month" class="form-control">
         <option value="0">Tháng</option>
         <?
         for($i = 1; $i<=12; $i++){
         ?>
         <option value="<?=$i?>"><?=$i?></option>
         <? 
         }
         ?>
      </select>
      <select name="frm_birth_year" id="frm_birth_year" class="form-control">
         <option value="0">Năm</option>
         <?
         for($i = 1960; $i<=date('Y'); $i++){
         ?>
         <option value="<?=$i?>"><?=$i?></option>
         <? 
         }
         ?>
      </select>
      <button class="btn btn-warning" onclick="getSimBirthDay();"><i class="icon icon-search"></i></button>
   </div>
</div>

<div class="box_sim_select">
   <h2>
      <i class="icon icon-heart"></i>
      <a href="javascript:;" title="Sim theo cặp">Sim theo cặp</a>
   </h2>
   <div class="bss_main bss_couple">
      <input type="text" class="form-control" name="couple_number" id="couple_number" placeholder="Số">
      <select name="couple_type" id="couple_type" class="form-control">
         <option value="0">Mạng</option>
         <?
         foreach($arrSimCat as $key => $row){
            ?>
            <option <? if($iCat == $key) echo 'selected="selected"'?> value="<?=$key?>"><?=$row['cat_name']?></option>
            <? 
         }
         ?>
      </select>
      <select name="couple_price" id="couple_price" class="form-control">
         <option value="0">Giá</option>
         <?
         foreach($arrSimPrice as $key => $row){
         ?>
         <option <? if($iPrice == $key) echo 'selected="selected"'?> value="<?=$key?>"><?=$row['pri_name']?></option>
         <? 
         }
         ?>
      </select>
      <button class="btn btn-warning" onclick="getSimCouple();"><i class="icon icon-search"></i></button>
   </div>
</div>
<?
$arrGioSinh = array(
   0=>"Tý (23h -> 1h)" , 
   1=>"Sửu (1h -> 3h)" , 
   2=>"Dần (3h -> 5h)" , 
   3=>"Mão (5h -> 7h)" , 
   4=>"Thìn (7h -> 9h)" , 
   5=>"Tị (9h -> 11h)" , 
   6=>"Ngọ (11h -> 13h)" , 
   7=>"Mùi (13h -> 15h)" , 
   8=>"Thân (15h -> 17h)" , 
   9=>"Dậu (17h -> 19h)" , 
   10=>"Tuất (19h -> 21h)" , 
   11=>"Hợi (21h -> 23h)" , 
);
?>
<div class="box_sim_select">
   <h2>
      <div id="yinYang"><div><div></div><div><div></div></div><div><div></div></div></div></div>
      <a href="/xem-sim-hop-tuoi.html" title="Sim hợp tuổi theo năm sinh">Sim hợp tuổi theo năm sinh</a>
   </h2>
   <div class="bss_main_love">
      <div class="bml_date">
         <select name="bss_birth_day" id="bss_birth_day" class="form-control">
            <option value="0">Ngày sinh</option>
            <?
            for($i = 1; $i<=31; $i++){
            ?>
            <option value="<?=$i?>"><?=$i?></option>
            <? 
            }
            ?>
         </select>
         <select name="bss_birth_month" id="bss_birth_month" class="form-control">
            <option value="0">Tháng sinh</option>
            <?
            for($i = 1; $i<=12; $i++){
            ?>
            <option value="<?=$i?>"><?=$i?></option>
            <? 
            }
            ?>
         </select>
         <select name="bss_birth_year" id="bss_birth_year" class="form-control">
            <option value="0">Năm sinh</option>
            <?
            for($i = 1960; $i<=date('Y'); $i++){
            ?>
            <option value="<?=$i?>"><?=$i?></option>
            <? 
            }
            ?>
         </select>
      </div>
      <div>
         <select name="pt_birth_hour" id="pt_birth_hour" class="form-control" style="width: 50%;">
            <? foreach($arrGioSinh as $kgio => $gio){?>
            <option value="<?=$kgio?>"><?=$gio?></option>
            <? }?>
         </select>
      </div>
      <div class="bml_sex">
         <ul>
            <li>
               <a href="javascript:;">
                  <i class="icon_check"></i>
                  <span>Nam</span>
               </a>
            </li>
            <li>
               <a href="javascript:;">
                  <i class="icon_check"></i>
                  <span>Nữ</span>
               </a>
            </li>
            <li>
               <button class="btn btn-warning" onclick="getSimHopMenh();"><i class="icon icon-search"></i></button>
            </li>
         </ul>
      </div>
   </div>
</div>
<div class="box_home_news">
   <ul class="nav nav-tabs">
      <?
      $i = 0;
      foreach ($arrNews as $key => $value) {

         $i++;
         $active = ($i == 1) ? ' active' : '';
      ?>
      <li class="nav-item<?=$active?>">
         <a class="nav-link<?=$active?>" data-toggle="tab" href="#tab_<?=$value['cat_id']?>"><?=$value["cat_name"]?></a>
      </li>
      <?
      }
      ?>
   </ul>

   <!-- Tab panes -->
   <div class="tab-content">
      <?
      $i = 0;
      foreach ($arrNews as $key => $value) {
         $i++;

         $sqlcategoryNews  = "";
         $listiCatNews     = $menuid->getAllChildId($value["cat_id"]);
         $sqlcategoryNews  = " AND cat_id IN(" . $listiCatNews  . ")";

         $active = ($i == 1) ? ' active' : '';
      ?>
      <div class="tab-pane<?=$active?>" id="tab_<?=$value["cat_id"]?>">
         <?
         $query = " SELECT *"
               ." FROM tbl_news"
               ." INNER JOIN tbl_category ON(cat_id = new_category)"
               ." WHERE new_active = 1".$sqlcategoryNews
               ." ORDER BY new_date DESC, new_id DESC"
               ." LIMIT 3"
               ;
         $db_query = new db_query($query);
         ?>
         <div id="carousel-example-generic" class="tpl_carousel_1 carousel slide" data-ride="carousel">
            <div class="carousel-inner">
               <?
               $i = 0;
               while($row = mysql_fetch_assoc($db_query->result)){
                  $i++;
                  $link_detail = createLink("detail_news",$row);

                  $srcPicNews  = $row["new_picture"] != '' ? $path_news . $row["new_title_index"] . '/' . $row["new_picture"] : $path_media . 'no_image.jpg';
                  $srcPicSmall = $path_lib . 'img/dot.gif';
               ?>
               <div class="item<?=($i == 1 ? ' active' : '')?>">
                  <a href="<?=$link_detail?>" title="<?=$row["new_title"]?>">
                     
                     <img alt="<?=$row["new_title"]?>" class="b-lazy" data-src="<?=$srcPicNews?>" src="<?=$srcPicSmall?>" style="width: 100%; height: 250px;" />
                     <div class="text_overflow title_new_sidebar" style="margin: 10px 0; text-align: center; padding: 0 10px; line-height: 140%; font-size: 14px;"><?=$row["new_title"]?></div>
                  </a>
               </div>
               <?
               }
               ?>
            </div>
            <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
               <span class="icon icon-chevron-left"></span>
            </a>
            <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
               <span class="icon icon-chevron-right"></span>
            </a>
         </div>
      </div>
      <?
      }
      ?>
   </div>
</div>
<?
include('inc_temp_sidebar_donhang.php');
?>
<?/*
<div class="box_home_news box_sim_4g">
   <ul class="nav nav-tabs">
      <li class="nav-item active">
         <a class="nav-link active" data-toggle="tab" href="#tab_4g_viettel">
            <i class="g4_viettel"></i>
         </a>
      </li>
      <li class="nav-item<?=$active?>">
         <a class="nav-link active" data-toggle="tab" href="#tab_4g_vina">
            <i class="g4_vina"></i>
         </a>
      </li>
      <li class="nav-item<?=$active?>">
         <a class="nav-link active" data-toggle="tab" href="#tab_4g_mobi">
            <i class="g4_mobi"></i>
         </a>
      </li>
      <li class="nav-item<?=$active?>">
         <a class="nav-link active" data-toggle="tab" href="#tab_4g_vnmobi">
            <i class="g4_vnmobi"></i>
         </a>
      </li>
   </ul>

   <!-- Tab panes -->
   <div class="tab-content">
      <div class="tab-pane active" id="tab_4g_viettel">
         <?
         $db_banner = new db_query("SELECT ban_name, ban_url, ban_target, ban_width, ban_height, ban_picture 
                                    FROM tbl_banners 
                                    WHERE ban_active = 1 AND ban_type = 5
                                    LIMIT 1");
         if($row = mysql_fetch_assoc($db_banner->result)){
            $check_display = 1;
         ?>
            <a href="javascript:;" class="banner_4g">
               <img src="../../store/media/banner/<?=$row["ban_picture"]?>" alt="<?=$row["ban_name"]?>">
            </a>
         <?
         }
         $db_banner->close();
         unset($db_banner);
         ?>
         <?/*
         <div class="g4_name">Gói cước: F90</div>
         <ul class="g4_list">
            <li>
               <i class="icon icon-globe"></i>
               <span class="g4_title">X GB</span>
               <span class="g4_sapo">Data/tháng</span>
            </li>
            <li>
               <i class="icon icon-phone"></i>
               <span class="g4_title">Free < xxx phút</span>
               <span class="g4_sapo">Nội mạng</span>
            </li>
            <li>
               <i class="icon icon-phone"></i>
               <span class="g4_title">xx phút</span>
               <span class="g4_sapo">Ngoại mạng</span>
            </li>
            <li>
               <i class="icon icon-envelope"></i>
               <span class="g4_title">xxx</span>
               <span class="g4_sapo">SMS</span>
            </li>
         </ul>
         <div class="g4_money">
            <div class="g4_title_cp">
               <span class="g4_title">Phí đăng kí:</span>
               <span class="g4_price">xxx.000₫</span>
            </div>
            <div class="g4_cuphap">
               Đăng ký: <span>MMM 0963754819</span> gửi <span>9123</span>
            </div>
         </div>
         
      </div>
      <div class="tab-pane<?=$active?>" id="tab_4g_vina">
         <div class="g4_name">Gói cước: F90</div>
         <ul class="g4_list">
            <li>
               <i class="icon icon-globe"></i>
               <span class="g4_title">X GB</span>
               <span class="g4_sapo">Data/tháng</span>
            </li>
            <li>
               <i class="icon icon-phone"></i>
               <span class="g4_title">Free < xxx phút</span>
               <span class="g4_sapo">Nội mạng</span>
            </li>
            <li>
               <i class="icon icon-phone"></i>
               <span class="g4_title">xx phút</span>
               <span class="g4_sapo">Ngoại mạng</span>
            </li>
            <li>
               <i class="icon icon-envelope"></i>
               <span class="g4_title">xxx</span>
               <span class="g4_sapo">SMS</span>
            </li>
         </ul>
         <div class="g4_money">
            <div class="g4_title_cp">
               <span class="g4_title">Phí đăng kí:</span>
               <span class="g4_price">xxx.000₫</span>
            </div>
            <div class="g4_cuphap">
               Đăng ký: <span>MMM 0963754819</span> gửi <span>9123</span>
            </div>
         </div>
      </div>
      <div class="tab-pane<?=$active?>" id="tab_4g_mobi">
         <div class="g4_name">Gói cước: F90</div>
         <ul class="g4_list">
            <li>
               <i class="icon icon-globe"></i>
               <span class="g4_title">X GB</span>
               <span class="g4_sapo">Data/tháng</span>
            </li>
            <li>
               <i class="icon icon-phone"></i>
               <span class="g4_title">Free < xxx phút</span>
               <span class="g4_sapo">Nội mạng</span>
            </li>
            <li>
               <i class="icon icon-phone"></i>
               <span class="g4_title">xx phút</span>
               <span class="g4_sapo">Ngoại mạng</span>
            </li>
            <li>
               <i class="icon icon-envelope"></i>
               <span class="g4_title">xxx</span>
               <span class="g4_sapo">SMS</span>
            </li>
         </ul>
         <div class="g4_money">
            <div class="g4_title_cp">
               <span class="g4_title">Phí đăng kí:</span>
               <span class="g4_price">xxx.000₫</span>
            </div>
            <div class="g4_cuphap">
               Đăng ký: <span>MMM 0963754819</span> gửi <span>9123</span>
            </div>
         </div>
      </div>
      <div class="tab-pane<?=$active?>" id="tab_4g_vnmobi">
         <div class="g4_name">Gói cước: F90</div>
         <ul class="g4_list">
            <li>
               <i class="icon icon-globe"></i>
               <span class="g4_title">X GB</span>
               <span class="g4_sapo">Data/tháng</span>
            </li>
            <li>
               <i class="icon icon-phone"></i>
               <span class="g4_title">Free < xxx phút</span>
               <span class="g4_sapo">Nội mạng</span>
            </li>
            <li>
               <i class="icon icon-phone"></i>
               <span class="g4_title">xx phút</span>
               <span class="g4_sapo">Ngoại mạng</span>
            </li>
            <li>
               <i class="icon icon-envelope"></i>
               <span class="g4_title">xxx</span>
               <span class="g4_sapo">SMS</span>
            </li>
         </ul>
         <div class="g4_money">
            <div class="g4_title_cp">
               <span class="g4_title">Phí đăng kí:</span>
               <span class="g4_price">xxx.000₫</span>
            </div>
            <div class="g4_cuphap">
               Đăng ký: <span>MMM 0963754819</span> gửi <span>9123</span>
            </div>
         </div>
      </div>
   </div>
</div>
*/?>
<div class="box_fanpage">
   <a href="https://www.facebook.com/khosimthe/">
      <img data-src="<?=$path_lib?>img/fanpage.jpeg" src="<?=$path_lib?>img/dot.gif" class="b-lazy" alt="Link fanpage">
   </a>
</div>