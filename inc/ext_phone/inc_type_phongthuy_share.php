<div class="share">
    <div>
        <label for="ifWidth">Chiều rộng : * Nên để nhỏ nhất là 400</label>
        <div>
            <input type="text" id="ifWidth" name="ifWidth" class="form-control input-lg" value="450" /> 
        </div>
    </div>
    <div>
        <label for="ifHeight">Chiều cao : * Nên để nhỏ nhất là 450</label>
        <div>
            <input type="text" id="ifHeight" name="ifHeight" class="form-control" value="450" />
        </div>
        
    </div>
    <div>
        <label for="ifCode">Mã nhúng :</label>
        <div>
            <textarea id="ifCode" rows="5" class="form-control" style="width:100%"></textarea>
        </div>  
    </div>
    <div>
        <button id="getIfCode" class="btn btn-danger">Lấy mã nhúng</button>
    </div>
</div>