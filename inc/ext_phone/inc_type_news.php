<?
#+
#+ Kiểm tra cache
$cache_check = 0; // 0.Chua cache - 1.Da cache	
#+
if($con_set_cache != 0){
	#+
	$cache_file_time	= $cache_file_time_1day;
	#+ Ten file cache
	$cache_file_path	= 'f_'.$current_page;
	#+ Folder cache
	$cache_folder_path	= '../../store/cache/web/news/';
	$cache_folder_path  .= $module != '' ? $module.'/tp' : '';
	$cache_folder_path  .= $iCat != 0 ? '_cat-'.$iCat : '';
	
	#+
	#+ Kiem tra xem co file cache khong
	$sCache = new Cache($cache_file_path,$cache_file_time,'',$cache_folder_path);
	if($sCache->is_cache()) {
		$cache_check = 1;
	} // End if(!$sCache->is_cache()) 	
} // End if($con_set_cache == 0)


#+
#+ Neu nhu khong co cache thi thuc hien
if($cache_check == 0){
	#+
	#+ Neu nhu chi co 1 ban ghi tin tuc
	if($toltal_record_news == 1){
		include("inc_detail_news.php");
		exit();
	}else{
		#+
		#+ Set bien
		$page_size		= 15;
		$sqlSelect		= '';
		$sqlJoin		= '';
		$sql 			= '';
		$sqlGroup 		= '';
		$sqlOrderBy 	= '';
		$sqlLimit		= '';
		#+
		$normal_class    	= "";
		$selected_class  	= "active";
		$page_prefix 	 	= "Trang";
		$current_page    	= ( getValue("page") < 1 ) ? 1 : getValue("page");
		#+
		#+ Tao duong dan phan trang
		$query_string = '';
		if($module == 'search' || $module == 'tim-kiem'){
			$url	= '/search.html?keyword='.$keyword;		
		}elseif($module == 'tag'){
			$url	= '/tag-'.$keyword.'-trang-';
			$query_string = '/';			
		}else{
			if($iCat != 0){
				$url	= '/'.$sCat.'-';
				$query_string = '/';
			}else{
				$url	= '/tin-tuc-trang-';	
				$query_string = '.html';
			} // End if($iCat != 0)
		} // End if($module == 'search')
		
		#+
		#+ Lay theo module
		if($module != 'search' && $module != 'tim-kiem' && $module != 'tag')
			$sql .= ' AND cat_type = "'.$module.'"';
			
		#+
		#+ Lay theo category
		if($iCat != 0)
			$sql .= $sqlcategory;
		
		#+
		#+ Search theo keyword
		# if($keyword != '') $sql .= ' AND new_search LIKE "%'.$keyword_r.'%" ';
		
		#+
		
		#+ Sap xep du lieu
		$sqlOrderBy	.= ' ORDER BY new_date DESC, new_id DESC, new_title';
		
		#+
		#+ Neu khong co cache
		if($con_set_cache < 2)
			$sqlLimit	.= ' LIMIT '.($current_page-1) * $page_size . ',' . $page_size;	

		#+
		#+ Thuc hien query
		$query = ' SELECT' 
				.' new_id,new_category,new_title,new_title_index,new_teaser,new_date,new_picture'
				.' FROM tbl_news'
				.' INNER JOIN tbl_category ON (cat_id = new_category)'
				.' WHERE tbl_category.lang_id = '.$lang_id.' AND new_active = 1'.$sql
				.$sqlGroup
				.$sqlOrderBy
				.$sqlLimit
				;
		# echo $query.'<br />';
		$arrQuery = getArray($query);
		if($con_set_cache < 2){
			#+
			#+ Day la noi khai bao query dem so ban ghi theo kieu ko cache	
			$query = ' SELECT Count(*) as count'
					.' FROM tbl_news'
					.' INNER JOIN tbl_category ON (cat_id = new_category)'
					.' WHERE tbl_category.lang_id = '.$lang_id.' AND new_active = 1'.$sql
					;
			$db_count = new db_query($query);
			$row = mysql_fetch_assoc($db_count->result);
			$total_record = $row['count'];
			$db_count->close();
			unset($db_count);	
		}else{
			#+
			$total_record 	= count($arrQuery);
		}// End if($con_set_cache != 0)	
		
		if($total_record <=0){
			$str  = '
					<span class="bgEF mar10 textSite666 textBold font20 pad20" align="center">
						Không có bản ghi được tìm thấy
					</span>
					';
		}else{
			#+
			#+ Xem voi so luong sim nhu vay co bao nhieu trang
			if ($total_record % $page_size == 0){
				$num_of_page = $total_record / $page_size;
			}else{
				$num_of_page = (int)($total_record / $page_size) + 1;
			} // End if ($total_record % $page_size == 0)
		
			#+ 
			#+ Neu nhu co nguoi co tinh dien page lon hon so page hien tai
			if($current_page > $num_of_page) return;
			
			#+
			#+ Dua ra so lieu de tao vong lap phu hop
			if($con_set_cache < 2){
				$start_page = $current_page;
				$end_page	= $current_page;
			}else{
				$start_page = $num_of_page;
				$end_page	= 1;		
			} // End if($con_set_cache != 0)
			
			#+ 
			#+ Bat dau vong lap de tao cache
			for($so_trang=$start_page; $so_trang>=$end_page;$so_trang--){
				# Trang hien tai cua cache
				$cache_page    		= $so_trang;
				
				# Chia game thanh cac trang nho
				if($con_set_cache < 2){
					$arrQuery1 			= $arrQuery;
				}else{
					$arrQuery1 			= array_slice($arrQuery,($cache_page-1) * $page_size,$page_size);	
				} // End if($con_set_cache != 0)
					
				#+
				#+ Bat dau tao cache
				if($con_set_cache != 0)
					$sCache = new Cache('f_'.$cache_page,$cache_file_time,'',$cache_folder_path);
				
				$str = '';
				$str .= '<ul class="newslist" id="mainlist">';
				$i=($cache_page-1) * $page_size;
				foreach($arrQuery1 as $key => $row){
					$i++;
			
					#+
					$str .= _temp::_newsBody($row,$i);
					
				} // End foreach($arrGame as $key => $row)
				$str .= '</ul>';
				#+
				#+ Phan trang
				if($total_record>$page_size){	

					$str .=	'
							<div align="center">
								<ul class="pagination">
									'.generatePageBar($page_prefix,$cache_page,$page_size,$total_record,$url,$normal_class,$selected_class,$query_string,'',2).'
								</ul>
							</div>
							';
								
				} // End if($total_record>$page_size)
				
				#+
				#+ Lay ra noi dung trang dang duoc load 
				if($cache_page == $current_page)
					$str_display .= $str;
			
				#+
				#+ Dua ra cache va cache lai
				if($con_set_cache != 0){
					$sCache->cache($str);	
				} // End if($con_set_cache != 0)
		
				# Huy array game da chia
				unset($arrQuery1);
			} // End for($so_trang=1; $so_trang<=$num_of_page;$so_trang++)	
			
		} // End if($total_record <=0)

		unset($arrQuery);
	
		#+
		#+ Ghi cache
		if($con_set_cache != 0){
			$sCache->cache($str);	
		} // End if($con_set_cache != 0)	
	} // End if($total_record == 1)
} // End if($cache_check == 0){
?>
<?
$query   = "SELECT * FROM tbl_category
				WHERE cat_type = 'tin-tuc' AND cat_active = 1 AND cat_parent_id = 0
				ORDER BY cat_order ASC";

$db_cat     = new db_query($query);
$arrCatNews = convert_result_set_2_array($db_cat->result);
unset($db_cat);
?>
<div class="type-news">
	<ul class="type_new_menu">
		<?
		if(!empty($arrCatNews)){
			foreach ($arrCatNews as $key => $value) {
				$link = createLink("type", $value);

				$active = ($_SERVER["REQUEST_URI"] == $link ? ' class="active"' : '');

			?>
			<li<?=$active?>>
				<a href="<?=$link?>" title="<?=$value["cat_name"]?>"><?=$value["cat_name"]?></a>
			</li>
			<?
			}
		}
		?>
	</ul>
	<div class="infopage">
		<span><?=$meta_description?></span>
	</div>
	<?=$str?>
</div>