<ol class="breadcrumb"><?=$home_address?></ol>
 
<article>
   <table class="table table-sim table-striped">
      <tbody>
         <tr>
            <td>Sim số đẹp</td>
            <td class="sim-digit text-primary"><?=$sProName?></td>
         </tr>
         <tr>
            <td>Giá bán</td>
            <td class="sim-price text-success text-bold"><?=number_format($sProPrice) != 0 ? number_format($sProPrice).' vnđ' : 'Sim đã bán'?></td>
         </tr>
         <tr>
            <td>Mạng di động</td>
            <td>
               <a href="javascript:;">
                  <img src="<?=$root_path_web.$path_category.$sCatPicture?>" alt="Sim so dep <?=$sCatName?>" />
               </a>
            </td>
         </tr>
         <? /*/?>
         <tr>
            <td>Tình trạng sim</td>
            <td>
               <input type="checkbox" checked="checked" /> Bộ simcard nguyên Kit có sẵn tài khoản<br />
               <input type="checkbox" checked="checked" /> Hưởng khuyến mãi hiện hành của nhà mạng<br />
               <input type="checkbox" checked="checked" /> Khách hàng sẽ được đăng ký thông tin khi mua sim<br /> 
            </td>
         </tr>
         <? //*/?>
      </tbody>
   </table>
    
   <div class="clearfix"></div>
	 
	<style type="text/css">
   .group_social{
      margin-top: 10px;
      text-align: right;
   }
   </style>
   <div class="group_social">
      <iframe src="https://www.facebook.com/plugins/share_button.php?href=<?=$con_server_name . $_SERVER["REQUEST_URI"]?>%2F&layout=button&size=small&appId=518289938685215&width=68&height=20" width="75" height="20" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allow="encrypted-media"></iframe>
      <div class="zalo-share-button" data-href="<?=$con_server_name . $_SERVER["REQUEST_URI"]?>" data-oaid="477951701879141919" data-layout="1" data-color="blue" data-customize=false></div>
   </div>
   
   <?
   if($sProPrice > 0)  include("inc_detail_sim_datsim.php");
   else include("inc_detail_sim_search.php");
   // include("inc_detail_sim_other.php");
   // include("inc_detail_sim_other1.php");
   ?>
</article>
<div class="box_info_detail">
   <h3>Các bước mua sim - <?=$sCatName?> - <?=$sProName?></h3>
   <ul>
      <li>
         <span class="icon icon-share-alt"></span>
         <span>Đặt mua sim trên website hoặc điện Hotline</span>
      </li>
      <li>
         <span class="icon icon-share-alt"></span>
         <span>NVKD sẽ gọi lại tư vấn và xác nhận đơn hàng</span>
      </li>
      <li>
         <span class="icon icon-share-alt"></span>
         <span>Nhận sim tại nhà, kiểm tra thông tin chính chủ và thanh toán cho người giao sim</span>
      </li>
   </ul>
   <h3>Hình thức giao sim - <?=$sCatName?> - <?=$sProName?></h3>
   <ul>
      <li>
         <span class="icon icon-share-alt"></span>
         <span>Cách 1: Sieuthisimthe.com sẽ giao sim trong ngày và thu tiền tại nhà (áp dụng tại các thành phố, thị trấn lớn)</span>
      </li>
      <li>
         <span class="icon icon-share-alt"></span>
         <span>Cách 2: Quý khách đến cửa hàng Sieuthisimthe.com để nhận sim trực tiếp (Danh sách của hàng ở chân website)</span>
      </li>
      <li>
         <span class="icon icon-share-alt"></span>
         <span>Cách 3: Sieuthisimthe.com sẽ giao sim theo đường bưu điện và thu tiền tại nhà.</span>
      </li>
   </ul>
   <p>CHÚ Ý: <i>Quý khách sẽ không phải thanh toán thêm bất kỳ 1 khoản nào khác ngoài giá sim</i></p>

   <i>Chúc quý khách gặp nhiều may mắn khi sở hữu thuê bao <?=$sProName?></i>
</div>
