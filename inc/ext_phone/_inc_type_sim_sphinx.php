<?
//
// Khai bao bien
$sphinx_host 			= "127.0.0.1";
$sphinx_port 			= 33120;
$index_sphinx        = "tbl_sim";
$page_size           = 55;
$page_start_sphinx   = ($current_page-1) * $page_size;
$page_end_sphinx     = $page_size;

$keywords		= preg_replace("/[^0-9*xX]/si","",$keyword);
#+
if ($keywords!="")
{
	$spot = strpos($keywords,"*");
	$slen = strlen($keywords);
} // End if ($keywords!="")
$keywords	 	= str_replace(array('x','X','*'),array('*','*','*'),$keywords);

$output_string = "";
$output_array	= array();
$arrayReturn	= array();
$arrQuery		= array();
	
//
// Sphinx Search
$sphinxSearch = new SphinxClient();

$sphinxSearch->SetServer($sphinx_host, $sphinx_port);
$sphinxSearch->SetConnectTimeout(1.5);
$sphinxSearch->SetMatchMode( SPH_MATCH_EXTENDED ); // SPH_MATCH_ALL - SPH_MATCH_EXTENDED - SPH_MATCH_FULLSCAN

// Array trả về sẽ không dùng doc_id làm key -> phục vụ việc groupby
$sphinxSearch->SetArrayResult(true);

// $sphinxSearch->SetRankingMode(SPH_RANK_WORDCOUNT); // SPH_RANK_WORDCOUNT - SPH_RANK_PROXIMITY

// Lấy max 5030 kết quả trả về
$sphinxSearch->_maxmatches = 5030;	

/* Open */
$sphinxSearch->Open();

/* Select */
$sphinxSearch->SetSelect ( "*" );

/* Filter */
$sphinxSearch->SetFilterFloatRange("sim_price", doubleval(10000), doubleval(900000000000));

if($module == 'searchsim' || $module == 'simajax'){
	if($iNguHanh != 0){
      $sphinxSearch->SetFilter("sim_active", array(1) );
      $sphinxSearch->SetFilter("sim_web", array(1) );
   }else{
      $sphinxSearch->SetFilter("sim_active", array(1) );
   }	
}else if($module == 'home'){
   $sphinxSearch->SetFilter("sim_active", array(1) );
   $sphinxSearch->SetFilter("sim_hot", array(1) );
}else{
	$sphinxSearch->SetFilter("sim_active", array(1) );
   # $sphinxSearch->SetFilter("sim_web", array(1) );
}

if($iCat != 0)
	$sphinxSearch->SetFilter("sim_category", array($iCat) );

if($iType != 0) 
   $sphinxSearch->SetFilter("sim_typeid", array($iType) );

if($iPrice != 0)
   $sphinxSearch->SetFilter("sim_priceid", array($iPrice) );

if($iDauSo != 0)
   $sphinxSearch->SetFilter("sim_dausoid", array($iDauSo) );

if($module == 'searchsim' && $keyword != ''){
	# $sqlWhere .= " AND sim_sim2 LIKE '".$keywords."'";
	if (stristr($keywords,"*") === false)
	{
		if(strlen($keywords) >= 10)
			$keywords = ltrim($keywords,0);
			
		$keyword_sphinx = "*".$keywords."*";
	}
	else 
	{
		$keywords = ltrim($keywords,0);
		
		#+
		if ($spot == 0){
         $keyword_sphinx = "".$keywords."$";
		}elseif($spot == ($slen -1)){
         if(($slen-1) <= 3)
            $sphinxSearch->SetFilter("sim_dausoid", array($iDauSo) );   
         else
            $keyword_sphinx = "".$keywords."";
		}else{
			$keyword_sphinx = "*".$keywords."$";
      }
	} // End if (stristr($keywords,"*")=== false)
} // End if($module == 'searchsim' && $keyword != '')

// echo $keyword_sphinx;

//
// Dua ra sim 10 so hay 11 so
if($digit == 1){
   $sphinxSearch->SetFilter("sim_digit_1", array(1) );
}else if($digit == 2){
   $sphinxSearch->SetFilter("sim_digit_2", array(1) );
} // End if($digit == 0){   

//
// Dua ra nhung sim hop ngu hanh sinh khac
if($iNguHanh != 0){
   $sphinxSearch->SetFilterRange("sim_diem_stst", 7, 10);
   $sphinxSearch->SetFilterRange("sim_diem_vietaa", 6, 10);
   $sphinxSearch->SetFilterRange("sim_sonut", 6, 10);
   $sphinxSearch->SetFilter("sim_nguhanh", array($iNguHanhSinh) );
   $sphinxSearch->SetSortMode(SPH_SORT_ATTR_DESC, 'sim_diem_stst'); 	
} // End if($iNguHanh != 0)

/* Order */ 
// SPH_SORT_ATTR_ASC - SPH_SORT_ATTR_DESC - SPH_SORT_EXPR
if($sort == 0){
   $sphinxSearch->SetSortMode(SPH_SORT_ATTR_ASC, 'sim_price');    
}else{
   $sphinxSearch->SetSortMode(SPH_SORT_ATTR_DESC, 'sim_price');       
}

/* Limit */     
$sphinxSearch->SetLimits($page_start_sphinx, $page_end_sphinx );

/* Query */
$result     = $sphinxSearch->Query($keyword_sphinx, $index_sphinx);

/* Notify */
$warning    = $sphinxSearch->GetLastWarning();
$error      = $sphinxSearch->GetLastError();

/* Close */
$sphinxSearch->Close();

$arrayReturn = $result['matches'];

$list_sim_id = '0';
foreach($arrayReturn as $k => $v){
   $sim_sim2 = $v['id'];
   
	// $arrQuery[$sim_sim2]['sim_sim1'] = '0'.$sim_sim2;
	$arrQuery[$sim_sim2]['sim_sim2'] = $sim_sim2;
   
   $list_sim_id .= ','.$sim_sim2;

	foreach($v['attrs'] as $ks => $vs){
		$arrQuery[$sim_sim2][$ks] = $vs;		
	}		
}

// echo $list_sim_id;
$query = 'SELECT sim_sim1, sim_sim2 FROM tbl_sim WHERE sim_sim2 IN ('.$list_sim_id.')';
$db_query = new db_query($query);
while($row = mysql_fetch_assoc($db_query->result)){
   $arrQuery[$row['sim_sim2']]['sim_sim1'] = $row['sim_sim1'];
}

$total_record = $result['total_found'];

//
# print_r($arrQuery);
# print_r($warning);
# print_r($error);
# return;
?>