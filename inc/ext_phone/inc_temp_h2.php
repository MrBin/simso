<?
if(!empty($arrTextSeo)){
?>
	<div class="border_line"></div>
	<article class="home_article">
		<div id="accordion">
			<?
			foreach ($arrTextSeo as $key => $value) {
				if($value["title"] != ""){
					?>
					<div class="card">
						<div class="card-header" id="heading_<?=$key?>">
							<h3 class="mb-0">
								<a href="javascript:;" class="collapsed" data-toggle="collapse" data-target="#collapse_<?=$key?>" aria-expanded="true" aria-controls="collapse_<?=$key?>">
									<i class="icon icon-chevron-right"></i>
									<?=$value["title"]?>
								</a>
							</h3>
						</div>

						<div id="collapse_<?=$key?>" class="collapse" aria-labelledby="heading_<?=$key?>" data-parent="#accordion">
							<div class="card-body">
								<?=$value["content"]?>
							</div>
						</div>
					</div>
					<?
				}
			}
			?>
		</div>
     	<div class="clearfix"></div>
    </article>
<?
}
?>