<div class="nav">
    <ul class="nav-tabs nav-justified">
        <li<?=$module != 'tin-tuc' && $iDauSo == 0 && ($type == 'static' || $module == 'home' || $iCat != 0 || $keyword != '' || $iNamSinh != 0 || $iNguHanh != 0) ? ' class="active"' : ''?>>
            <a href="#tra-cuu" data-toggle="tab">Kho sim</a>
        </li>
        <li<?=$module != 'searchsim' && ($iType != 0 || $iPriceFrom != 0 || $iPriceTo != 0 || $iDauSo != 0) ? ' class="active"' : ''?>>
            <a href="#gioi-thieu" data-toggle="tab">Phân loại</a>
        </li>
        <li<?=$module == 'tin-tuc' ? ' class="active"' : ''?>>
            <a href="#chia-se" data-toggle="tab">Tin tức</a>
        </li>
    </ul>
    <div class="clearfix"></div>
</div> 

<div class="tab-content">
    <div class="tab-pane<?=$module != 'tin-tuc' && $iDauSo == 0 && ($type == 'static' || $module == 'home' || $iCat != 0 || $keyword != '' || $iNamSinh != 0 || $iNguHanh != 0) ? ' active' : ''?>" id="tra-cuu">
        <?
        include('inc_left_tab1.php');
        ?>  
    </div>
    <div class="tab-pane<?=$module != 'searchsim' && ($iType != 0 || $iPriceFrom != 0 || $iPriceTo != 0 || $iDauSo != 0) ? ' active' : ''?>" id="gioi-thieu">
        <?
        include('inc_left_tab2.php');
        ?>
    </div>
    <div class="tab-pane<?=$module == 'tin-tuc' ? ' active' : ''?>" id="chia-se">
        <?
        include('inc_left_tab3.php');
        ?>
    </div>
</div>
