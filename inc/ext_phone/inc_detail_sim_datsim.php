<style>
.text{
	margin-top: 10px;
}
.text label{
	width: 100px;
}
.text label .textSiteRed{
	color: #F00;
}
.text input{
	width: 100%;
}
.btn_control{
	margin-top: 10px;
	text-align: center;
}
.hotline_frm{
	margin-right: 10px;
	display: block;
}
.dtSimContact .btn_send{
	margin-bottom: 10px;
	margin-top: 5px;
	padding: 6px 20px;
	font-size: 20px;
	background-color: #47a447;
}
.dtSimContact label.error{
	display: block;
	width: 100%;
	margin-top: 5px;
	padding-left: 5px;
	color: #F00;
}
.errorMsg{
	text-align: center;
	color: #F00;
}
</style>
<div class="dtSimContact hidden" id="box_contact_success">
	<div class="btn_success">
		Đã đặt hàng thành công
	</div>
	<p>Chúng tôi sẽ kiểm tra đơn hàng <br> và chủ động liên hệ với quý khách trong thời gian sớm nhất</p>
	<p>Xin chân thành cảm ơn!</p>
</div>
<div class="dtSimContact" id="box_contact">
	<h3>Thông tin khách hàng</h3>	
	<div class="errorMsg hidden"></div>
	<form name="fBuySim" action="" method="post" id="frm_checkout">
		<input class="form-control" type="hidden" name="actions" value="submitForm" />
		<div class="errorMsg"></div>
		<ul>
			<li class="text">
				<input class="form-control" name="billing_name" id="billing_name" value="<?=$hoten?>" placeholder="Vui lòng nhập họ tên" type="text" />
			</li>
			
			<li class="text">
				<input class="form-control" name="billing_phone" placeholder="Vui lòng nhập số điện thoại" id="billing_phone" value="<?=$dienthoai?>" type="tel" />
			</li>
			
			<li class="text">
				<input class="form-control" name="billing_address" id="billing_address" placeholder="Vui lòng nhập địa chỉ" value="<?=$diachi?>" type="text" />
			</li>
			<li class="btn_control">
				<button class="btn btn-success btn_send" type="submit"><?=tdt("Đặt mua")?></button>
				<span class="hotline_frm"><strong>Giao sim miễn phí tận tay trong ngày! <br>Liên hệ</strong> <strong class="sim-digit text-danger text-bold id_hotline">0909.575.575</strong></span>
			</li>
		</ul>
		<input type="hidden" id="action" name="action" value="send">
		<input type="hidden" id="giaban" name="giaban" value="<?=$sProPrice?>">
		<input type="hidden" id="simcard" name="simcard" value="<?=$sProName?>">
	</form>
</div>
