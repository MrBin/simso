<?
#+
#+ Tao ra truong search hop voi cac bien truyen vao
$sqlWhere = "";
$sqlWhere .= " AND sim_price >= 10000";

#+
if($module == 'searchsim' || $module == 'simajax')
	if($iNguHanh != 0)
		$sqlWhere .= ' AND sim_active = 1 AND sim_web = 1';
	else
		$sqlWhere .= ' AND sim_active = 1';	
else if($module == 'home')
	$sqlWhere .= ' AND sim_active = 1 AND sim_hot = 1';
else
	$sqlWhere .= ' AND sim_active = 1 AND sim_web = 1';	

#+
if($iCat != 0) 
	$sqlWhere .= " AND sim_category = ".$iCat;
#+
if($iType != 0) 
	$sqlWhere .= " AND sim_typeid = ".$iType;
#+
if($iPrice != 0)
	$sqlWhere .= " AND sim_priceid = ".$iPrice;
#+
if($iDauSo != 0)
	$sqlWhere .=" AND sim_dausoid = ".$iDauSo;
/*/
#+
if($str_iType != '')
	$sqlWhere .= " AND sim_typeid IN (".$str_iType.")";	

if($iDauSo != 0 || $sDauSo != '')
	$sqlWhere .=" AND sim_sim2 LIKE '".intval($sDauSo)."%'";
	$sqlWhere .=" AND sim_dausoid = ".$iDauSo;
//*/
#+
if($module == 'searchsim' && $keyword != ''){
	# $sqlWhere .= " AND sim_sim2 LIKE '".$keywords."'";
	if (stristr($keywords,"*") === false)
	{
		if(strlen($keywords) >= 10)
			$keywords = ltrim($keywords,0);
			
		$sqlWhere 	.= " AND sim_sim2 RLIKE '.*".$keywords.".*'";
	}
	else 
	{
		$keywords = ltrim($keywords,0);
		
		#+
		if ($spot == 0)
			$sqlWhere	.= " AND sim_sim2 RLIKE '".$keywords."$'";
		elseif ($spot == ($slen -1))
			$sqlWhere	.= " AND sim_sim2 RLIKE '^".$keywords."'";
		else 
			$sqlWhere	.= " AND sim_sim2 RLIKE '^".$keywords."$'";
	} // End if (stristr($keywords,"*")=== false)
} // End if($module == 'searchsim' && $keyword != '')

#+
#+ Dua ra sim 10 so hay 11 so
if($digit == 1){
	$sqlWhere  .= " AND LENGTH(sim_sim2) = 9 ";
}else if($digit == 2){
	$sqlWhere  .= " AND LENGTH(sim_sim2) = 10 ";
} // End if($digit == 0){


#+
#+ Dua ra nhung sim hop ngu hanh sinh khac
if($iNguHanh != 0){
	$sqlWhere .= " AND sim_diem_stst >= 7 AND sim_diem_vietaa >= 6 AND sim_sonut >= 6 AND sim_nguhanh = ".$iNguHanhSinh;	
	$sqlOrder .= "sim_diem_stst DESC,";
	# echo  'Mệnh : '.$arrNguHanh[$iNguHanh].' - Sinh : '.$arrNguHanh[$iNguHanhSinh].' - Khắc : '.$arrNguHanh[$iNguHanhKhac];	
} // End if($iNguHanh != 0)


if($iBirthday != "" && $iBirthMonth != "" && $iBirthYear != ""){
	$strBDFull = $iBirthday . $iBirthMonth . $iBirthYear;
	// $sqlWhere	.= " AND sim_sim2 RLIKE '". replaceMQ($iBirthday) ."$'";
}

#+
#+ Xem sap xep sim the nao
$sqlOrder  .= $sort == 0 ? "sim_price," :  "sim_price DESC,";

#+
#+ Neu khong co cache
if($con_set_cache < 2)
	$sqlLimit	= ' LIMIT '.($current_page-1) * $page_size.','.$page_size;	

# Lay tat ca cac sim phu hop cho vao array
$arrQuery = array();
$query = ' SELECT sim_sim1,sim_sim2,sim_price,sim_category,sim_typeid,sim_priceid,sim_nguhanh,sim_sonut,sim_diem_stst,sim_hot'
		.' FROM tbl_sim'
		.' WHERE 1'.$sqlWhere
		.' ORDER BY '.$sqlOrder.' sim_date DESC'
		.$sqlOrderBy
		.$sqlLimit
		;
// echo $query.'<br />';
$arrQuery = getArray($query);

if($con_set_cache < 2){
	#+
	#+ Day la noi khai bao query dem so ban ghi theo kieu ko cache	
	$query = ' SELECT Count(*) as count'
			.' FROM tbl_sim'
			.' WHERE 1'.$sqlWhere
			;
	$db_count = new db_query($query);
	$row = mysql_fetch_assoc($db_count->result);
	$total_record = $row['count'];
	$db_count->close();
	unset($db_count);	
}else{
	#+
	$total_record 	= count($arrQuery);
}// End if($con_set_cache != 0)
?>