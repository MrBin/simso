<?
$arrLogo 	= array("viettel" 		=> "viettel",
						  "mobifone" 		=> "mobifone",
						  "vinaphone" 		=> "vinaphone",
						  "vietnamobile" 	=> "vietnamobile",
						  "vnpt" 			=> "vnpt");

/*
$searchConfig =  array ("page"		=> getValue("page", "int", "GET", 1),
								"page_size"	=> 1000,
								"iCat"		=> $iCat,
								"iType"		=> $iType,
								"iPrice"		=> $iPrice,
								"iDauSo"		=> $iDauSo,
								"iDaiLy"		=> $iDaiLy,
								"digit"		=> $digit,
								"iNguHanh"	=> $iNguHanh,
								"module"		=> $module,
								"sort"		=> 2,
								);
/* Thuc hien search 
$sphinx_search  = new sphinx_search();
$arrQuery = $sphinx_search->search($searchConfig);
*/

$arrQuery = getSimHot();
$arrQuery = array_random($arrQuery, 17);


if(!empty($arrQuery)){
?>
<div id="box_sim_hot" class="box_sim">
	<h2 class="bsh_title">SIM GIÁ TỐT TRONG NGÀY</h2>
	<ul>
		<?
		foreach ($arrQuery as $key => $row) {
			$link_detail = createLink("detail_sim",$row);
			$cat_name    = $row['cat_name_index'];
		?>
		<li>
			<a href="<?=$link_detail?>" title="<?=$row["sim_sim1"]?>">
				<img class="b-lazy" data-src="<?=$path_lib?>img/icon/icon_<?=strtolower($cat_name)?>.jpg" src="<?=$path_lib?>img/dot.gif" alt="<?=$cat_name?>">
				<div class="bs_main">
					<span class="sim_name"><?=$row["sim_sim1"]?></span>
					<span class="sim_price"><?=format_number($row['sim_price'])?>₫</span>
				</div>
			</a>
		</li>
		<?
		}
		?>
		<li class="bs_more">
			<a href="/sim-gia-tot-trong-ngay" title="Sim hot trong ngày">
				Xem thêm
			</a>
		</li>
	</ul>
</div>
<?
}// End if(!empty($result)){


$query = " SELECT cat_id, cat_name_index, cat_name FROM tbl_category"
      ." WHERE cat_type = 'sim' AND cat_active = 1 AND cat_home = 1"
      ." ORDER BY cat_order DESC LIMIT 3"
      ;
$db_cat = new db_query($query);
$arrCatSim = convert_result_set_2_array($db_cat->result);
unset($db_cat);

if(!empty($arrCatSim)){
	foreach ($arrCatSim as $key => $value) {
		$logo     = isset($arrLogo[$value["cat_name_index"]]) ? $arrLogo[$value["cat_name_index"]] : "";
		// $iCat     = $value["cat_id"];
		$link_cat = createLink("cat",$value);

		/* Query */
		$searchConfigCat =  array ("page"		=> getValue("page", "int", "GET", 1),
											"page_size"	=> 17,
											"iCat"		=> $value["cat_id"],
											"iType"		=> $iType,
											"iPrice"		=> $iPrice,
											"iDauSo"		=> $iDauSo,
											"iDaiLy"		=> $iDaiLy,
											"digit"		=> $digit,
											"iNguHanh"	=> $iNguHanh,
											"module"		=> $module,
											"sort"		=> 2,
											);
		$sphinx_search   = new sphinx_search();
		$arrQueryCatHome = $sphinx_search->search($searchConfigCat);
		$arrQueryCatHome = $arrQueryCatHome["data"];
		$arrQueryCatHome = array_random($arrQueryCatHome, 17);

		if(!empty($arrQueryCatHome)){
		?>
		<div class="border_line"></div>
		<div id="box_sim_hot_<?=$value["cat_name_index"]?>" class="box_sim">
			<h2>SIM <?=$value["cat_name"]?></h2>
			<ul>
				<?
				foreach($arrQueryCatHome as $key => $row){

					$row['cat_name_index'] = $value["cat_name_index"];

					$link_detail = createLink("detail_sim",$row);
				?>
				<li>
					<a href="<?=$link_detail?>" title="<?=$row["sim_sim1"]?>">
						<img class="b-lazy" data-src="<?=$path_lib?>img/icon/icon_<?=$logo?>.jpg" src="<?=$path_lib?>img/dot.gif" alt="<?=$logo?>">
						<div class="bs_main">
							<span class="sim_name"><?=$row["sim_sim1"]?></span>
							<span class="sim_price"><?=format_number($row["sim_price"])?>₫</span>
						</div>
					</a>
				</li>
				<?
				} // End while($row = mysql_fetch_assoc($db_simType->result))
				unset($arrQuery);
				?>
				<li class="bs_more">
					<a href="<?=$link_cat?>" title="Xem thêm sim <?=$value["cat_name"]?>">
						Xem thêm
					</a>
				</li>
			</ul>
		</div>

		<?
		}
	}
}
include('../../inc/ext_phone/inc_temp_h2.php');
?>
<div class="search_right">
	<div><a class="btn btn-warning" href="/sim-gia-re-viettel/" title="Sim giá rẻ Viettel" data-pjax="#main">Sim giá rẻ Viettel</a></div>
	<div><a class="btn btn-danger" href="/sim-gia-re-vinaphone/" title="Sim giá rẻ Vinaphone" data-pjax="#main">Sim giá rẻ Vinaphone</a></div>
	<div><a class="btn btn-success" href="/sim-gia-re-mobifone/" title="Sim giá rẻ Mobifone" data-pjax="#main">Sim giá rẻ Mobifone</a></div>
	<div><a class="btn btn-info" href="/sim-gia-re-vietnamobile/" title="Sim giá rẻ Vietnamobile" data-pjax="#main">Sim giá rẻ Vietnamobile</a></div>
	<div class="clearfix"></div>
</div>
