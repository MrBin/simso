<?
// Google Captcha
// require_once('../../kernel/classes/gcaptcha/autoload.php');
// // Register API keys at https://www.google.com/recaptcha/admin
// $siteKey 	= '6LcUBCITAAAAAJBZuIfXTMQl77tyiJAAyn2e-JsA';
// $secret 		= '6LcUBCITAAAAAKIJTnEG3d7QHDuz08rQHCIpqNb-';
// // reCAPTCHA supported 40+ languages listed here: https://developers.google.com/recaptcha/docs/language
// $lang 		= 'vi';
// $g_captcha 	= getValue('g-recaptcha-response', 'str', 'GET', '');

//
$arrGioSinh = array(
	0=>"Tý (23h -> 1h)" , 
	1=>"Sửu (1h -> 3h)" , 
	2=>"Dần (3h -> 5h)" , 
	3=>"Mão (5h -> 7h)" , 
	4=>"Thìn (7h -> 9h)" , 
	5=>"Tị (9h -> 11h)" , 
	6=>"Ngọ (11h -> 13h)" , 
	7=>"Mùi (13h -> 15h)" , 
	8=>"Thân (15h -> 17h)" , 
	9=>"Dậu (17h -> 19h)" , 
	10=>"Tuất (19h -> 21h)" , 
	11=>"Hợi (21h -> 23h)" , 
	);
	
//	
$arrNgaySinh = array(
	1=>1 , 2=>2 , 3=>3 , 4=>4 , 5=>5 ,
	6=>6 , 7=>7 , 8=>8 , 9=>9 , 10=>10 ,
	11=>11 , 12=>12 , 13=>13 , 14=>14 , 15=>15 ,
	16=>16 , 17=>17 , 18=>18 , 19=>19 , 20=>20 ,
	21=>21 , 22=>22 , 23=>23 , 24=>24 , 25=>25 ,
	26=>26 , 27=>27 , 28=>28 , 29=>29 , 30=>30 ,
	31=>31 ,	
	);
	
//	
$arrThangSinh = array(
	1=>1 , 2=>2 , 3=>3 , 4=>4 , 5=>5 ,
	6=>6 , 7=>7 , 8=>8 , 9=>9 , 10=>10 ,
	11=>11 , 12=>12
	);
	
//
$arrNamSinh = array();
for($i = 1950; $i<=date('Y')-3; $i++){
	$arrNamSinh[$i] = $i;	
}

//
$arrSex = array(
	"Nam"=>"Nam" , 
	"Nữ"=>"Nữ"
	);

$submit 			= getValue('submit','str','POSTGET','');
$gio_sinh		= getValue('giosinh','int','POSTGET','');
$ngay_sinh		= getValue('ngaysinh','int','POSTGET','');
$thang_sinh		= getValue('thangsinh','int','POSTGET','');
$nam_sinh		= getValue('namsinh','int','POSTGET','');
$gioi_tinh 		= getValue('gioitinh','str','POSTGET','');
$so_dien_thoai	= getValue('sodienthoai','str','POSTGET','');
$so_dien_thoai = preg_replace("/[^0-9]/si","",$so_dien_thoai);
$so_dien_thoai = trim($so_dien_thoai);

if($module == 'simrandom'){
	$gioi_tinh 		= "Nam";
	$so_dien_thoai	= "0".$iData;
	$gio_sinh			= rand(1,12);
	$ngay_sinh		= rand(1,28);
	$thang_sinh		= rand(1,12);
	$nam_sinh		= rand(1950,1999);		
}
?>
<form action="/home/vn/<?=($module == 'xem-sim-phong-thuy' ? 'print' : 'type')?>.php" method="GET">
	<input type="hidden" name="type" value="static" />
	<input type="hidden" name="module" value="<?=$module?>" />
	<div class="body-tool" style="padding-top: 10px;">
		<input type="hidden" name="module_check" id="module_check" value="<?=$module?>" />
	    
		<table cellpadding="4" cellspacing="0" width="100%">
			<tr>
				<td class="text-bold">Số sim</td>
				<td><input type="text" name="sodienthoai" id="sodienthoai" value="<?=$so_dien_thoai?>" class="form-control" width="300" /></td>
			</tr>
			<tr>
			   <td class="text-bold">Giờ sinh</td>
			   <td>
			   	<select name="giosinh" id="giosinh" class="form-control">
						<option value="">Giờ</option>
						<? foreach($arrGioSinh as $kgio => $gio){?>
						<option value="<?=$kgio?>" <? if($gio_sinh == $kgio) echo 'selected="selected"'?>><?=$gio?></option>
						<? }?>
					</select>
				</td>					
			</tr>							
			<tr>
			   <td class="text-bold">Ngày sinh</td>
			   <td>
	                
					<select name="ngaysinh" id="ngaysinh" class="form-control">
						<option selected="selected" value="">Ngày</option>           	 
						<? foreach($arrNgaySinh as $kngay => $ngay){?>
						<option value="<?=$kngay?>" <? if($ngay_sinh == $kngay) echo 'selected="selected"'?>><?=$ngay?></option>
						<? }?>     
					</select>
					
					<select name="thangsinh" id="thangsinh" class="form-control">
						<option selected="selected" value="">Tháng</option>      	 	
						<? foreach($arrThangSinh as $kthang => $thang){?>
						<option value="<?=$kthang?>" <? if($thang_sinh == $kthang) echo 'selected="selected"'?>><?=$thang?></option>
					<? }?>  
					</select>
					
					<select name="namsinh" id="namsinh" class="form-control">
						<option selected="selected" value="">Năm</option>         	
						<? foreach($arrNamSinh as $knam => $nam){?>
						<option value="<?=$knam?>" <? if($nam_sinh == $knam) echo 'selected="selected"'?>><?=$nam?></option>
						<? }?>  
					</select>       
			   </td>
			</tr>
			<tr>
				<td class="text-bold">Giới tính</td>
				<td> 
					<select name="gioitinh" id="gioitinh" class="form-control">
						<option value="">Chọn</option>
						<? foreach($arrSex as $ksex => $sex){?>
						<option value="<?=$ksex?>" <? if($gioi_tinh == $ksex) echo 'selected="selected"'?>><?=$sex?></option>
						<? }?> 
					</select>
				</td>
			</tr>
			<tr>
				<td></td>
				<td><input type="submit" name="submit" id="submit" value="Xem" class="btn btn-danger" style="width: 150px; font-size:18px; cursor:pointer" /></td>
			</tr>
		</table>
	    
		<? if($module != 'xem-sim-phong-thuy'){?>
		<div class="social" style="padding: 10px;">
			<script src="https://apis.google.com/js/plusone.js" type="text/javascript" language="javascript"></script>
			<g:plusone size="medium" href="http://www.sieuthisimthe.com/sim-phong-thuy.html"></g:plusone>
			<iframe src="//www.facebook.com/plugins/like.php?href=http%3A%2F%2Fwww.sieuthisimthe.com%2Fsim-phong-thuy.html&amp;send=false&amp;layout=button_count&amp;width=120&amp;show_faces=false&amp;action=like&amp;colorscheme=light&amp;font&amp;height=21&amp;appId=173270492684652" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:120px; height:21px;" allowTransparency="true"></iframe>
		</div> 
		<? }?>
	</div>
</form>

<div class="main-phong-thuy">
   <div id="main-phong-thuy" style="padding-top: 10px;">
    	<?
      if($submit == 'Xem'){
			include("inc_type_phongthuy_tracuu.php");
						
		}else{

			// if($module != 'xem-sim-phong-thuy'){
			// 	include('inc_type_phongthuy_intro1.php');
			// }	
		}
		?>
    	<div class="sim_hop_menh">
    		<h2 class="ss_title">Sim hợp mệnh</h2>
    		<ul>
    			<li class="shm_kim">
	            <a href="/sim-phong-thuy-hop-menh-kim/" title="Mệnh Kim">
	               <i class="icon_check"></i>
	               <span>Mệnh Kim</span>
	            </a>
	         </li>
	         <li class="shm_moc">
	            <a href="/sim-phong-thuy-hop-menh-moc/" title="Mệnh Mộc">
	               <i class="icon_check"></i>
	               <span>Mệnh Mộc</span>
	            </a>
	         </li>
	         <li class="shm_thuy">
	            <a href="/sim-phong-thuy-hop-menh-thuy/" title="Mệnh Thủy">
	               <i class="icon_check"></i>
	               <span>Mệnh Thủy</span>
	            </a>
	         </li>
	         <li class="shm_hoa">
	            <a href="/sim-phong-thuy-hop-menh-hoa/" title="Mệnh Hỏa">
	               <i class="icon_check"></i>
	               <span>Mệnh Hỏa</span>
	            </a>
	         </li>
	         <li class="shm_tho">
	            <a href="/sim-phong-thuy-hop-menh-tho/" title="Mệnh Thổ">
	               <i class="icon_check"></i>
	               <span>Mệnh Thổ</span>
	            </a>
	         </li>
    		</ul>
    	</div>
    	<div class="sim_hop_menh">
    		<h2 class="ss_title">Sim hợp tuổi được chọn sẵn</h2>
    		<ul class="sim_hop_nam">
	         <?
	         for($i=1960;$i<=2010;$i++){
	         	?>
	         	<li>
	         		<a href="/sim-phong-thuy-hop-tuoi-<?=$i?>/" title="Sim phong thủy hợp tuổi <?=$i?>" data-pjax="body" data-ajax="#main"> 
	         			<i class="icon_check"></i>
	         			<span>Sim hợp tuổi <?=$i?></span>
	         		</a> 
	         	</li>
	         	<?	
	         }
	         ?>
    		</ul>
    	</div>
    </div>
</div>