<?
// Google Captcha
// require_once('../../kernel/classes/gcaptcha/autoload.php');
// // Register API keys at https://www.google.com/recaptcha/admin
// $siteKey 	= '6LcUBCITAAAAAJBZuIfXTMQl77tyiJAAyn2e-JsA';
// $secret 		= '6LcUBCITAAAAAKIJTnEG3d7QHDuz08rQHCIpqNb-';
// // reCAPTCHA supported 40+ languages listed here: https://developers.google.com/recaptcha/docs/language
// $lang 		= 'vi';

// Khai bao bien
$fs_table			= "tbl_yeucau";
$fs_redirect 		= $_SERVER['REQUEST_URI'];
$thoigian			= date("Y-m-d H:i:s");
$errorMsg 			= "";
$action				= getValue("actions", "str", "POST", "");
$code					= getValue("code", "str", "POST", "");
$simcard				= getValue("simcard", "str", "POST", "");
$simcard				= substr($simcard,0,15);
$hoten				= getValue("hoten", "str", "POST", "");
$hoten				= strip_tags($hoten);
$hoten				= substr($hoten,0,25);
$hoten 				= preg_replace("/[0-9]/si"," ",$hoten);

$hoten_check = RemoveSign($hoten);
$hoten_check = explode(' ',$hoten_check);
if(strlen($hoten_check[0]) >= 8){
	echo "<script language='javascript'>alert('" . tdt("Vui lòng kiểm tra lại họ tên") . "')</script>";
	redirect($fs_redirect);	
	exit();
}

#+
$myform = new generate_form();
//$myform->removeHTML(0);
#+
$myform->addTable($fs_table);
#+
$myform->add("simcard","simcard",0,1,"",1,"Điền số cần bán",0,"");
$myform->add("giaban","giaban",0,0,"",1,"Điền giá bán",0,"");
$myform->add("dienthoai","dienthoai",0,0,"",1,"Điền số điện thoại",0,"");
$myform->add("hoten","hoten",0,1,"",1,"Điền họ tên",0,"");
$myform->add("diachi","diachi",0,0,"",0,"",0,"");
$myform->add("dtban","dtban",0,0,"",0,"",0,"");
$myform->add("thanhpho","thanhpho",0,0,"",0,"",0,"");
$myform->add("noidung","noidung",0,0,"",0,"",0,"");
$myform->add("phanloai","phanloai",1,1,3,0,"",0,"");
$myform->add("thoigian","thoigian",0,1,"",0,"",0,"");
$myform->addjavasrciptcode('if(document.getElementById("code").value == ""){ alert("Điền mã xác nhận !"); document.getElementById("code").focus(); return false;}');

#+
#+ Neu co gui form thi thuc hien
if($action == "submitForm"){
	$errorMsg .= $myform->checkdata();	//Check Error!
	$errorMsg .= $myform->strErrorFeld ;	//Check Error!
	if($errorMsg == ""){
		$db_ex = new db_execute($myform->generate_insert_SQL());
		?>
	     <script type="text/javascript">
	     $(function(){
	         $("#box_contact_success").removeClass("hidden");
	         $("#frm_mua").remove();
	     });
	     </script>
	     <?
		//echo $myform->generate_insert_SQL();
		// echo '<script type="text/javascript">alert("Đăng bán sim thành công - Quý khách vui lòng đợi để cửa hàng kiểm duyệt")</script>';
		// redirect($fs_redirect);
		// exit();
	}else{
		echo '<script type="text/javascript">alert("' . $errorMsg . '")</script>';
	}
	
} // End if($action == "submitForm"){

//add form for javacheck
$myform->addFormname("submitForm");
$myform->checkjavascript();
$myform->evaluate();
//*/
?>

<style>
.text{
	margin-top: 10px;
}
.text input {
    width: calc(100% - 105px);
}
.text label {
    width: 100px;
}
.text label .tRed {
    color: #F00;
}
.main_static_pjax{
	margin-bottom: 15px;
}
</style>
<article class="main_static_pjax">
	<h1 class="ss_title">Thu mua sim số đẹp, sim vip</h1>
	<div>
		<div class="dtSimContact hidden" id="box_contact_success">
			<div class="btn_success">
				Gửi thành công
			</div>
			<p>Chúng tôi sẽ kiểm tra đơn hàng <br> và chủ động liên hệ với quý khách trong thời gian sớm nhất</p>
			<p>Xin chân thành cảm ơn!</p>
		</div>
		<form name="submitForm" action="" method="post" id="frm_mua">
			<input class="form-control" type="hidden" name="actions" value="submitForm" />	
			<ul>
				<li class="text">
					<label for="">Bán số : <span class="tRed">*</span></label>
					<input class="form-control" type="tel" name="simcard" id="simcard" placeholder="Nhập số bạn cần bán" value="<?=$simcard?>" />
				</li>
				<li class="text">
					<label for="">Giá bán : <span class="tRed">*</span></label>
					<input class="form-control" type="text" name="giaban" id="giaban" placeholder="Nhập giá bạn mong muốn bán" value="<?=$giaban?>" />
				</li>
				<li class="text">
					<label for="">Di động : <span class="tRed">*</span></label>
					<input class="form-control" type="tel" name="dienthoai" id="dienthoai" placeholder="Nhập số điện thoại của bạn" value="<?=$dienthoai?>" />
				</li>
				<li class="text">
					<label for="">Họ tên : <span class="tRed">*</span></label>
					<input class="form-control" type="text" name="hoten" id="hoten" value="<?=$hoten?>" placeholder="Nhập họ tên của bạn" />
				</li>
				<li class="text">
					<label for="">Địa chỉ : </label>
					<input class="form-control" type="text" name="diachi" id="diachi" value="<?=$diachi?>" placeholder="Nhập địa chỉ của bạn" />
				</li>
				<li class="text text-center">
					<button class="btn btn-success" style="width: 118px;" onclick="validateForm();">Gửi</button>
				</li>
			</ul>

		</form>
	</div>
</article>
<div class="border_line"></div>