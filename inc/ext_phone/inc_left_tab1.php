<div class="panel-group-primary" id="accordion-tab1">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h4 class="panel-title"> 
                <a<?=$module == 'home' || ($module != 'simgiare' && $iCat != 0) ? '' : ' class="collapsed"'?> data-toggle="collapse" data-parent="#accordion-tab1" href="#collapse-primary-simsodep"> 
                    <i class="icon icon-this"></i>&nbsp;
                    Sim số đẹp
                </a> 
            </h4>
        </div>
        <div id="collapse-primary-simsodep" class="panel-collapse <?=$module == 'home' || ($module != 'simgiare' && $iCat != 0) ? 'in' : 'collapse'?>">
            <div class="panel-body">
            	<?
                include('inc_left_simsodep.php');
				?>
            </div>
        </div>
    </div>
    
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h4 class="panel-title"> 
                <a class="collapsed" href="/sim-phong-thuy.html" style="background: #EFEFEF; color: #333;">
                	<i class="icon icon-this"></i>&nbsp;
                    Sim Phong Thủy
                </a> 
            </h4>
        </div>
    </div>

    <div class="panel panel-primary">
        <div class="panel-heading">
            <h4 class="panel-title"> 
                <a<?=$module == 'simgiare' && $iCat != 0 ? '' : ' class="collapsed"'?> data-toggle="collapse" data-parent="#accordion-tab1" href="#collapse-primary-simgiare"> 
                    <i class="icon icon-this"></i>&nbsp;
                    Sim giá rẻ
                </a> 
            </h4>
        </div>
        <div id="collapse-primary-simgiare" class="panel-collapse <?=$module == 'simgiare' && $iCat != 0 ? 'in' : 'collapse'?>">
            <div class="panel-body">
            	<?
                include('inc_left_simgiare.php');
				?>
            </div>
        </div>
    </div>

    <div class="panel panel-primary">
        <div class="panel-heading">
            <h4 class="panel-title"> 
                <a<?=$keyword != '' ? '' : ' class="collapsed"'?> data-toggle="collapse" data-parent="#accordion-tab1" href="#collapse-primary-simanmsinh"> 
                    <i class="icon icon-this"></i>&nbsp;
                    Sim năm sinh
                </a> 
            </h4>
        </div>
        <div id="collapse-primary-simanmsinh" class="panel-collapse <?=$keyword != '' ? 'in' : 'collapse'?>">
            <div class="panel-body">
            	<?
                include('inc_left_simnamsinh.php');
				?>
            </div>
        </div>
    </div>
	
	
    
    
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h4 class="panel-title"> 
                <a<?=$iNamSinh == 0 ? ' class="collapsed"' : ''?> data-toggle="collapse" data-parent="#accordion-tab1" href="#collapse-primary-simhoptuoi"> 
                    <i class="icon icon-this"></i>&nbsp;
                    Sim hợp tuổi
                </a> 
            </h4>
        </div>
        <div id="collapse-primary-simhoptuoi" class="panel-collapse  <?=$iNamSinh != 0 ? 'in' : 'collapse'?>">
            <div class="panel-body">
            	<?
                include('inc_left_simhoptuoi.php');
				?>
            </div>
        </div>
    </div>
    
    
    
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h4 class="panel-title"> 
                <a<?=$iNamSinh == 0 && $iNguHanh != 0 ? '' : ' class="collapsed"'?> data-toggle="collapse" data-parent="#accordion-tab1" href="#collapse-primary-simhopmenh"> 
                    <i class="icon icon-this"></i>&nbsp;
                    Sim hợp mệnh
                </a> 
            </h4>
        </div>
        <div id="collapse-primary-simhopmenh" class="panel-collapse  <?=$iNamSinh == 0 && $iNguHanh != 0 ? 'in' : 'collapse'?>">
            <div class="panel-body">
            	<?
                include('inc_left_simhopmenh.php');
				?>
            </div>
        </div>
    </div>
</div>