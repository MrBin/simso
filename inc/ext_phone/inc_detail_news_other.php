<?
$sql = '';
$str_title = '';
$sqlOrder = ' ORDER BY new_date DESC';

if($type_record == 'cat'){
	
	$sql .= ' AND new_category_id = '.$iCat;
	$str_title = 'Bài đăng cùng danh mục';	
	
}elseif($type_record == 'new'){
	
	if($sDataDate != 0){
		$sql .= ' AND new_date >= '.$sDataDate;
	}else{
		$sql .= ' AND new_id > '.$iData;	
	}
	
	$sqlOrder = ' ORDER BY new_date';
	
	$str_title = 'Bài đăng mới';
	
}else{
	
	if($sDataDate != 0){
		$sql .= ' AND new_date < '.$sDataDate;
	}else{
		$sql .= ' AND new_id < '.$iData;	
	} // End if($sDataDate != 0)	
	
	$str_title = 'Bài đăng cũ';
	
}

#+
$sql .= ' AND new_active = 1 AND new_date < '.time().' AND new_id <> '.$iData;

$query = ' SELECT new_id, new_title, new_title_index, new_date, new_picture
        FROM tbl_news
        WHERE 1'.$sql
        .$sqlOrder.' LIMIT 10'
		;
$arrQuery = getArray($query,'new_date');
krsort($arrQuery);
if(count($arrQuery) > 0){
?>

   <div class="box_news_relate">
     <h5 class="titlerelate"><?=$str_title?></h5>
     <ul class="newsrelate">
         <?
         foreach($arrQuery as $key => $row){

             $srcImg   = $row["new_picture"] != '' ? $path_news . $row["new_title_index"] . '/' . $row["new_picture"] : $path_news . 'no_image.jpg';

             $srcPicSmall = $path_lib . 'img/dot.gif';

         if($module == 'tin-tuc-ko-dau'){
             $new_title  = removeSeo($row["new_title"]);
             $lin_detail = createLink("detail_news_ko_dau",$row);
         }else{
             $new_title  = $row["new_title"];
             $lin_detail = createLink("detail_news",$row);
         }
         ?>
             <li>
                 <a href="<?=$lin_detail?>" title="<?=$new_title?>" class="linkimg">
                     <div class="tempcontent">
                     	<h3 class="t_ov_3"><?=$new_title?></h3>
                     	<span class="timepost"><?=date("d/m/Y",$row["new_date"])?></span>
                     </div>
                     <div class="tempvideo">
                         <img width="100" class="b-lazy" data-src="<?=$srcImg?>" src="<?=$srcPicSmall?>" alt="<?=$new_title?>">
                     </div>
                 </a>
             </li>
             
         <?
         } // End while($row = mysql_fetch_assoc($db_query->result))             
         ?>
     </ul>
 </div>
    
<?
} // End if(mysql_num_rows($db_query->result))
?>