<?
$key_cache = md5('key_inc_bot_copyright');

$data_memcache = $myMemcache->get($key_cache);

if ($data_memcache == '') {
   $html  = '';
    $html .= getStatic($con_static_footer_ststcom);
}else{
   $html = $data_memcache;
}
?>
<footer>
    <div class="footer_top">
        <div class="footer_link">
            <div class="fl_item">
                <div class="title">HƯỚNG DẪN KHÁCH HÀNG</div>

                <div><a href="http://sieuthisimthe.com/dat-sim-theo-yeu-cau.html" title="đặt sim theo yêu cầu">Đặt sim theo yêu cầu</a></div>

                <div><a href="http://sieuthisimthe.com/huong-dan-gui-thong-tin" rel="nofollow">Hướng dẫn gửi thông tin</a></div>

                <div><a href="http://sieuthisimthe.com/kiem-tra-thong-tin-thue-bao" rel="nofollow" title="Kiểm tra thông tin">Kiểm tra thông tin</a></div>

                <div><a href="http://www.sieuthisimthe.com/chinh-sach-van-chuyen-va-giao-nhan" rel="nofollow" style="line-height: 18px; background-color: rgb(255, 255, 255);">Chính sách vận chuyển và giao nhận</a></div>
            </div>
            <div class="fl_item">
                <div class="title">HỖ TRỢ KHÁCH HÀNG</div>

                <div><a href="http://sieuthisimthe.com/huong-dan-cach-nhan-sim-cod" rel="nofollow" title="Hướng dẫn nhận sim COD">Hướng dẫn nhận sim COD</a></div>

                <div><a href="http://www.sieuthisimthe.com/thanh-toan-qua-ngan-hang.htm" rel="nofollow" style="line-height: 18px; background-color: rgb(255, 255, 255);">Tài khoản ngân hàng</a></div>

                <div><a href="http://sieuthisimthe.com/575-giai-phong.html" rel="nofollow" title="Đường đến với chúng tôi"><span style="line-height: 18px;">Đường đến với chúng tôi</span></a></div>

                <div><a href="http://www.sieuthisimthe.com/chinh-sach-bao-mat-thong-tin" rel="nofollow" style="line-height: 18px; background-color: rgb(255, 255, 255);">Chính sách bảo mật thông tin</a></div>
            </div>
            <div class="fl_item">
                <div class="title">CHÍNH SÁCH VÀ QUY ĐỊNH</div>

                <div><a href="http://www.sieuthisimthe.com/chinh-sach-va-quy-dinh-chung" rel="nofollow">Những quy định chung </a></div>

                <div><a href="http://www.sieuthisimthe.com/hinh-thuc-thanh-toan" rel="nofollow">Hình thức thanh toán </a></div>

                <div><a href="http://www.sieuthisimthe.com/quy-dinh-doi-tra-hoan-tien" rel="nofollow">Quy định đổi trả hoàn tiền </a></div>

                <div><a href="http://www.sieuthisimthe.com/chinh-sach-bao-hanh" rel="nofollow">Chính sách bảo hành</a></div>
            </div>
            <div class="fl_item">
                <div class="title">KẾT NỐI VỚI CHÚNG TÔI</div>
                <div class="footer_right">
                    <div class="fr_bottom">
                       <a href="https://www.facebook.com/sieuthisimthecom" class="icon_fb">
                            <img class="b-lazy" data-src="<?=$path_lib?>img/icon/icon_fb.png" src="<?=$path_lib?>img/dot.gif" alt="Siêu thị sim thẻ Facebook">
                        </a>
                        <a href="https://www.instagram.com/sieuthisimthe/" class="icon_insta">
                            <img class="b-lazy" data-src="<?=$path_lib?>img/icon/icon_insta.png" src="<?=$path_lib?>img/dot.gif" alt="Siêu thị sim thẻ Instagram">
                        </a>
                        <a href="https://www.youtube.com/sieuthisimthe" class="icon_youtube">
                            <img class="b-lazy" data-src="<?=$path_lib?>img/icon/icon_youtube.png" src="<?=$path_lib?>img/dot.gif" alt="Siêu thị sim thẻ Youtube">
                        </a>
                        <a href="https://twitter.com/sieuthi_simthe" class="icon_twiter">
                            <img class="b-lazy" data-src="<?=$path_lib?>img/icon/icon_twiter.png" src="<?=$path_lib?>img/dot.gif" alt="Siêu thị sim thẻ Twitter">
                        </a>
                        <a href="https://www.pinterest.com/sieuthisimthe/" class="icon_preen">
                            <img class="b-lazy" data-src="<?=$path_lib?>img/icon/icon_preen.png" src="<?=$path_lib?>img/dot.gif" alt="Siêu thị sim thẻ Print">
                        </a>
                    </div>
                    <div class="fr_top">
                        <a class="icon_bct" href="http://online.gov.vn/HomePage/CustomWebsiteDisplay.aspx?DocId=13147">
                            <img class="b-lazy" data-src="<?=$path_lib?>img/icon/icon_bct.png" src="<?=$path_lib?>img/dot.gif" alt="Siêu thị sim thẻ thông báo bộ công thương">
                        </a>
                        <?/*
                        <div style="margin-top: 4px;">
                            <a href="javascript:;">
                                <img alt="" src="/lib/img/icon_baochi.png" style="vertical-align: middle;"> Chúng tôi trên báo chí
                            </a>
                        </div>
                        */?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer_main">
        <div class="container">
            <div class="fm_top"><span>SIEUTHISIMTHE.COM</span></div>
            <div>Đại lý phân phối sim số đẹp, sim giá rẻ</div>
            <div>Địa chỉ: 575 Giải Phóng, Giáp Bát, Hoàng Mai, Hà Nội</div>
            <div>
                <a href="//www.dmca.com/Protection/Status.aspx?ID=95097408-dd65-4f4a-a7e4-1482be8745c5" title="DMCA.com Protection Status" class="dmca-badge"> <img class="b-lazy" src="<?=$path_lib?>img/dot.gif" data-src="https://images.dmca.com/Badges/_dmca_premi_badge_4.png?ID=95097408-dd65-4f4a-a7e4-1482be8745c5"  alt="DMCA.com Protection Status" /></a>  <script async src="https://images.dmca.com/Badges/DMCABadgeHelper.min.js"> </script>
                &nbsp;<span>Bản quyền @ Siêu Thị Sim Thẻ</span>
            </div>
            <?/*
            <div class="hotline_fixed" id="hlbt">
                <a class="hotline" href="tel:0989575575">
                    <div id="phone">
                        <span>&nbsp;</span>
                    </div>
                    <div>
                        
                    </div>
                    0989.575.575
                </a>
            </div>
            */?>
            <div class="hotline-phone-ring-wrap">
                <div class="hotline-phone-ring">
                    <div class="hotline-phone-ring-circle"></div>
                    <div class="hotline-phone-ring-circle-fill"></div>
                    <div class="hotline-phone-ring-img-circle">
                        <a href="tel:0989575575" class="pps-btn-img">
                            <svg viewBox="0 0 512 512"><path d="m256 0c-141.363281 0-256 114.636719-256 256s114.636719 256 256 256 256-114.636719 256-256-114.636719-256-256-256zm5.425781 405.050781c-.003906 0 .003907 0 0 0h-.0625c-25.644531-.011719-50.84375-6.441406-73.222656-18.644531l-81.222656 21.300781 21.738281-79.375c-13.410156-23.226562-20.464844-49.578125-20.453125-76.574219.035156-84.453124 68.769531-153.160156 153.222656-153.160156 40.984375.015625 79.457031 15.96875 108.382813 44.917969 28.929687 28.953125 44.851562 67.4375 44.835937 108.363281-.035156 84.457032-68.777343 153.171875-153.21875 153.171875zm0 0"></path><path d="m261.476562 124.46875c-70.246093 0-127.375 57.105469-127.40625 127.300781-.007812 24.054688 6.726563 47.480469 19.472657 67.75l3.027343 4.816407-12.867187 46.980468 48.199219-12.640625 4.652344 2.757813c19.550781 11.601562 41.964843 17.738281 64.816406 17.746094h.050781c70.191406 0 127.320313-57.109376 127.351563-127.308594.011718-34.019532-13.222657-66.003906-37.265626-90.066406-24.042968-24.0625-56.019531-37.324219-90.03125-37.335938zm74.90625 182.035156c-3.191406 8.9375-18.484374 17.097656-25.839843 18.199219-6.597657.984375-14.941407 1.394531-24.113281-1.515625-5.5625-1.765625-12.691407-4.121094-21.828126-8.0625-38.402343-16.578125-63.484374-55.234375-65.398437-57.789062-1.914063-2.554688-15.632813-20.753907-15.632813-39.59375 0-18.835938 9.890626-28.097657 13.398438-31.925782 3.511719-3.832031 7.660156-4.789062 10.210938-4.789062 2.550781 0 5.105468.023437 7.335937.132812 2.351563.117188 5.507813-.894531 8.613281 6.570313 3.191406 7.664062 10.847656 26.5 11.804688 28.414062.957031 1.917969 1.59375 4.152344.320312 6.707031-1.277344 2.554688-5.519531 8.066407-9.570312 13.089844-1.699219 2.105469-3.914063 3.980469-1.679688 7.8125 2.230469 3.828125 9.917969 16.363282 21.296875 26.511719 14.625 13.039063 26.960938 17.078125 30.789063 18.996094 3.824218 1.914062 6.058594 1.59375 8.292968-.957031 2.230469-2.554688 9.570313-11.175782 12.121094-15.007813 2.550782-3.832031 5.105469-3.191406 8.613282-1.914063 3.511718 1.273438 22.332031 10.535157 26.160156 12.449219 3.828125 1.917969 6.378906 2.875 7.335937 4.472657.960938 1.597656.960938 9.257812-2.230469 18.199218zm0 0"></path></svg>
                        </a>
                    </div>
                </div>
            </div>
            <a id="zalo" class="button_icon" href="https://zalo.me/0989575575" target="_blank" rel="noopener"><i></i></a>
        </div>
    </div>
</footer>

<div class="overlay" onclick="closeAllMenu()"></div>
<script type="text/javascript">
$(function(){ 
    initLoad();

    $(".filter_type").find("ul li").each(function(){
        var dataChecked = $(this).attr("data-checked");
        $(this).click(function(){
            $(".box_filter").find("ul li").removeClass("active");
            $(this).addClass("active");
        })
    })

    var iPrice = <?=(isset($iPrice) ? $iPrice : 0)?>;
    $(".filter_price").find("ul li").each(function(){
        var dataChecked = $(this).attr("data-checked");

        if(dataChecked == iPrice) $(this).addClass("active");
        $(this).click(function(){
            $(".filter_price").find("ul li").removeClass("active");
            $(this).addClass("active");
        })
    })

    $(".bml_sex").find("ul li").each(function(){
        $(this).click(function(){
           $(".bml_sex").find("ul li").removeClass("active");
           $(this).addClass("active");
        })
     });
    
});
</script>
<script type="text/javascript">
$(function(){
    +function(e){"use strict";function t(){var e=document.createElement("bootstrap");var t={WebkitTransition:"webkitTransitionEnd",MozTransition:"transitionend",OTransition:"oTransitionEnd otransitionend",transition:"transitionend"};for(var n in t){if(e.style[n]!==undefined){return{end:t[n]}}}}e.fn.emulateTransitionEnd=function(t){var n=false,r=this;e(this).one(e.support.transition.end,function(){n=true});var i=function(){if(!n)e(r).trigger(e.support.transition.end)};setTimeout(i,t);return this};e(function(){e.support.transition=t()})}(window.jQuery);+function(e){"use strict";var t=function(t,n){this.$element=e(t);this.$indicators=this.$element.find(".carousel-indicators");this.options=n;this.paused=this.sliding=this.interval=this.$active=this.$items=null;this.options.pause=="hover"&&this.$element.on("mouseenter",e.proxy(this.pause,this)).on("mouseleave",e.proxy(this.cycle,this))};t.DEFAULTS={interval:5e3,pause:"hover",wrap:true};t.prototype.cycle=function(t){t||(this.paused=false);this.interval&&clearInterval(this.interval);this.options.interval&&!this.paused&&(this.interval=setInterval(e.proxy(this.next,this),this.options.interval));return this};t.prototype.getActiveIndex=function(){this.$active=this.$element.find(".item.active");this.$items=this.$active.parent().children();return this.$items.index(this.$active)};t.prototype.to=function(t){var n=this;var r=this.getActiveIndex();if(t>this.$items.length-1||t<0)return;if(this.sliding)return this.$element.one("slid",function(){n.to(t)});if(r==t)return this.pause().cycle();return this.slide(t>r?"next":"prev",e(this.$items[t]))};t.prototype.pause=function(t){t||(this.paused=true);if(this.$element.find(".next, .prev").length&&e.support.transition.end){this.$element.trigger(e.support.transition.end);this.cycle(true)}this.interval=clearInterval(this.interval);return this};t.prototype.next=function(){if(this.sliding)return;return this.slide("next")};t.prototype.prev=function(){if(this.sliding)return;return this.slide("prev")};t.prototype.slide=function(t,n){var r=this.$element.find(".item.active");var i=n||r[t]();var s=this.interval;var o=t=="next"?"left":"right";var u=t=="next"?"first":"last";var a=this;if(!i.length){if(!this.options.wrap)return;i=this.$element.find(".item")[u]()}this.sliding=true;s&&this.pause();var f=e.Event("slide.bs.carousel",{relatedTarget:i[0],direction:o});if(i.hasClass("active"))return;if(this.$indicators.length){this.$indicators.find(".active").removeClass("active");var l=e(a.$indicators.children()[i.index()]);l&&l.addClass("active")}if(e.support.transition&&this.$element.hasClass("slide")){this.$element.trigger(f);if(f.isDefaultPrevented())return;i.addClass(t);i[0].offsetWidth;r.addClass(o);i.addClass(o);r.one(e.support.transition.end,function(){i.removeClass([t,o].join(" ")).addClass("active");r.removeClass(["active",o].join(" "));a.sliding=false;setTimeout(function(){a.$element.trigger("slid")},0)}).emulateTransitionEnd(600)}else{this.$element.trigger(f);if(f.isDefaultPrevented())return;r.removeClass("active");i.addClass("active");this.sliding=false;this.$element.trigger("slid")}s&&this.cycle();return this};var n=e.fn.carousel;e.fn.carousel=function(n){return this.each(function(){var r=e(this);var i=r.data("bs.carousel");var s=e.extend({},t.DEFAULTS,r.data(),typeof n=="object"&&n);var o=typeof n=="string"?n:s.slide;if(!i)r.data("bs.carousel",i=new t(this,s));if(typeof n=="number")i.to(n);else if(o)i[o]();else if(s.interval)i.pause().cycle()})};e.fn.carousel.Constructor=t;e.fn.carousel.noConflict=function(){e.fn.carousel=n;return this};e(document).on("click.bs.carousel.data-api","[data-slide], [data-slide-to]",function(t){var n=e(this),r;var i=e(n.attr("data-target")||(r=n.attr("href"))&&r.replace(/.*(?=#[^\s]+$)/,""));var s=e.extend({},i.data(),n.data());var o=n.attr("data-slide-to");if(o)s.interval=false;i.carousel(s);if(o=n.attr("data-slide-to")){i.data("bs.carousel").to(o)}t.preventDefault()});e(window).on("load",function(){e('[data-ride="carousel"]').each(function(){var t=e(this);t.carousel(t.data())})})}(window.jQuery);
});
$(function(){
   $('.carousel').carousel();

    $('.carousel').on('slide.bs.carousel', function () {
        var bLazy = new Blazy();
    });

    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
      var bLazy = new Blazy();
    });
});
</script>
<script type="text/javascript">
var jsonNetwork = '<?=$jsonNetword?>';
</script>
<?/*<a id="fb_message" class="button_icon" href="https://m.me/sieuthisimthe.com" target="_blank" rel="noopener"><i></i></a>*/?>