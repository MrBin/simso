<div class="home-news">
<?
$query = ' SELECT cat_id, cat_name, cat_name_rewrite
			FROM categories_multi
			WHERE cat_active = 1 AND cat_type = "news" AND cat_parent_id = 0
			ORDER BY cat_order
			';
$db_cat = new db_query($query);
while($cat = mysql_fetch_assoc($db_cat->result)){
	$link_cat = createLink('cat',$cat);
?>
    <a href="<?=$link_cat?>" title="<?=$cat['cat_name']?>">
        <h2 class="head-line"><?=$cat['cat_name']?></h2>
    </a>

    <div>
        <?
        $query = ' SELECT new_id, new_category_id, new_title, new_title_rewrite, new_picture, new_date
                    FROM news_multi 
                    WHERE new_active = 1 AND new_picture != "" AND new_category_id = '.$cat['cat_id'].' 
                    ORDER BY new_id DESC, new_date DESC
                    LIMIT 5';
        $arrQuery = getArray($query, 'new_id');
        
        #+ Lấy ra list id
        $arrQueryId = array();
        foreach($arrQuery as $k => $v){
            $arrQueryId[] = $k;	
        }
        $new_id_in = implode(',', $arrQueryId);
        
        #+ Nếu như có list id
        if($new_id_in != ''){
            #+ lấy ra mô tả của list id
            $query = ' SELECT new_new_id AS new_id, new_teaser
                        FROM news_multi_description
                        WHERE new_new_id IN('.$new_id_in.')';
            $arrQueryDes = getArray($query, 'new_id');
            
            #+ Kết hợp 2 array
            foreach($arrQuery as $k => $v){
                $arrQuery[$k] = array_merge($v, $arrQueryDes[$k]);
            }
        }
        
        #+ Hủy biến
        unset($query, $new_id_in, $arrQueryId, $arrQueryDes);

        $i=0;
        foreach($arrQuery as $key => $row){
            $i++;
            
			$row['cat_name_rewrite'] = $cat['cat_name_rewrite'];
            $link_detail = createLink('detail_news',$row);
            
            if($i == 1){
            ?>
			<div>
            	<h3>
                    <a href="<?=$link_detail?>" title="<?=$row['new_title']?>">
                        <?=$row['new_title']?>
                    </a>
                </h3>
                <div class="image">
                    <a href="<?=$link_detail?>" title="<?=$row['new_title']?>">
                        <img width="120" src="<?=getImageSmall($row['new_picture'],250,1000)?>" alt="<?=$row['new_title']?>" />
                    </a>
                </div>
                <div>
                    <?=limitWord($row['new_teaser'],50)?>
                </div>
                <div class="clearfix"></div>
            </div>

			<ul>
            
            <?
			}else{
            ?>
                <li>
                    <a href="<?=$link_detail?>" title="<?=$row['new_title']?>">
						<?=$row['new_title']?>
                    </a>
                </li>
            <? 
            }
            //*/
            if($i == count($arrQuery)) echo '</ul>';
            
        }
        ?>
	</div>  
<?
}
?> 
</div>