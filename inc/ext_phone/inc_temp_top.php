<div id="header_bar">
    <div class="hb_top">
        <div class="td-left">
            <a href="javascript:;" class="icon icon icon-tasks" onclick="toggleMenu()"></a>
        </div>
        <a href="/" class="logo">
            <img src="../../lib/img/logo.png" alt="Siêu Thị Sim Thẻ - Đại Lý Phân Phối Sim Số Đẹp - Chọn Số Chỉ Từ 45K">
        </a>
        <div class="td-right">
            <a href="javascript:;" class="icon icon-tasks" data-action="show" onclick="showCenterMenu($(this))"></a>
        </div>
    </div>
    <div class="hb_search">
        <div class="hbs_group">
            <input type="search" id="searchText" name="keyword" class="form-control searchText" value="<?=$keyword?>" placeholder="Bạn muốn tìm sim gì hôm nay?" data-placement="bottom" title="Hướng dẫn tìm kiếm" />
            <span class="btn" onclick="submit_form_v2('all');">
                <i class="icon icon-search"></i>
            </span>
        </div>
        
        <div id="search-popover" class="hide">
            + Tìm sim có số <strong>6789</strong> bạn hãy gõ <strong>6789</strong> <br>
            + Tìm sim có đầu <strong>098</strong> đuôi <strong>8888</strong> hãy gõ <strong>098*8888</strong> <br>
            + Tìm sim bắt đầu bằng <strong>0989</strong> đuôi bất kỳ, hãy gõ: <strong>0989*</strong> <br>
        </div>
    </div>
</div>
<div id="menu_mobile"></div>