<div class="type-phong-thuy">

	<div class="header">
        <div class="logo">
            <img src="/lib/img/phong-thuy/logo.gif" alt="Logo Phong Thủy">
        </div>
        <div class="slogan">
            <img src="/lib/img/phong-thuy/slogan.png" alt="Slogan Phong Thủy">
        </div>
    </div>

    <div class="nav">
        <ul class="nav-tabs nav-tabs-phong-thuy">
            <li class="active"><a href="#tra-cuu" data-toggle="tab">Tra cứu</a></li>
            <li><a href="#chia-se" data-toggle="tab">Chia sẻ</a></li>
        </ul>
        <div class="clearfix"></div>
    </div> 
    
    <div class="tab-content">
        <div class="tab-pane active" id="tra-cuu">
            <?
            include('inc_type_phongthuy_tool.php');
            ?>
        </div>
        <div class="tab-pane" id="chia-se">
            <?
            include('inc_type_phongthuy_share.php');
            ?>
        </div>
    </div>
</div>