<?
$query = " SELECT * FROM tbl_category"
      ." WHERE cat_type = 'tin-tuc' AND cat_active = 1 AND cat_parent_id = 0"
      ." ORDER BY cat_order LIMIT 5"
      ;
$db_cat = new db_query($query);
$arrNews = convert_result_set_2_array($db_cat->result);
unset($db_cat);
?>
<style type="text/css">
#menu_mobile .search_right{
   margin-bottom: 0;
   padding-top: 0;
}
</style>
<div class="search_right">
   <div><a class="btn btn-warning" href="/sim-gia-re-viettel/" title="Sim giá rẻ Viettel" data-pjax="#main">Sim giá rẻ Viettel</a></div>
   <div><a class="btn btn-danger" href="/sim-gia-re-vinaphone/" title="Sim giá rẻ Vinaphone" data-pjax="#main">Sim giá rẻ Vinaphone</a></div>
   <div><a class="btn btn-success" href="/sim-gia-re-mobifone/" title="Sim giá rẻ Mobifone" data-pjax="#main">Sim giá rẻ Mobifone</a></div>
   <div><a class="btn btn-info" href="/sim-gia-re-vietnamobile/" title="Sim giá rẻ Vietnamobile" data-pjax="#main">Sim giá rẻ Vietnamobile</a></div>
   <div class="clearfix"></div>
</div>
<div class="panel panel-sidebar">
   <div class="panel-body box_filter">
      <ul>
         <li data-checked="1">
            <a data-pjax="#main_left" href="/sim-phong-thuy.html" rel="nofollow" title="Sim phong thủy">
               <i class="icon_check"></i>
               <span>Sim phong thủy</span>
            </a>
         </li>
         <li data-checked="2">
            <a data-pjax="#main_left" href="/xem-sim-hop-tuoi.html" title="Xem sim hợp tuổi">
               <i class="icon_check"></i>
               <span>Xem sim hợp tuổi</span>
            </a>
         </li>
      </ul>
   </div>
</div>
<div class="panel panel-sidebar">
   <div class="panel-heading">
      <h2 class="panel-title">
         <i class="icon icon-filter"></i>
         Sim theo giá
      </h2>
      <div class="clearfix"></div>
   </div>
   <div class="panel-body box_filter filter_price">
      <ul>
         <li data-checked="1">
            <a data-pjax="#main_left" href="/gia-0-200000/" title="Sim số đẹp giá dưới 200.000 đ">
               <i class="icon_check"></i>
               <span>Dưới 200.000 đ</span>
            </a>
         </li>
         <li data-checked="2">
            <a data-pjax="#main_left" href="/gia-200000-500000/" title="Sim số đẹp giá từ 2 trăm đến 5 trăm">
               <i class="icon_check"></i>
               <span>2 trăm -&gt; 5 trăm</span>
            </a>
         </li>
         <li data-checked="3">
            <a data-pjax="#main_left" href="/gia-500000-1000000/" title="Sim số đẹp giá từ 5 trăm đến 1 triệu">
               <i class="icon_check"></i>
               <span>5 trăm -&gt;1 triệu</span>
            </a>
         </li>
         <li data-checked="4">
            <a data-pjax="#main_left" href="/gia-1000000-3000000/" title="Sim số đẹp giá từ 1 triệu đến 3 triệu">
               <i class="icon_check"></i>
               <span>1 triệu -&gt; 3 triệu</span>
            </a>
         </li>
         <li data-checked="5">
            <a data-pjax="#main_left" href="/gia-3000000-5000000/" title="Sim số đẹp giá từ 3 triệu đến 5 triệu">
               <i class="icon_check"></i>
               <span>3 triệu -&gt; 5 triệu</span>
            </a>
         </li>
         <li data-checked="6">
            <a data-pjax="#main_left" href="/gia-5000000-10000000/" title="Sim số đẹp giá từ 5 triệu đến 10 triệu">
               <i class="icon_check"></i>
               <span>5 triệu -&gt; 10 triệu</span>
            </a>
         </li>
         <li data-checked="7">
            <a data-pjax="#main_left" href="/gia-10000000-50000000/" title="Sim số đẹp giá từ 10 triệu đến 50 triệu">
               <i class="icon_check"></i>
               <span>10 triệu -&gt; 50 triệu</span>
            </a>
         </li>
         <li data-checked="8">
            <a data-pjax="#main_left" href="/gia-50000000-0/" title="Sim số đẹp trên 50 triệu">
               <i class="icon_check"></i>
               <span>Trên 50 triệu</span>
            </a>
         </li>
      </ul>
   </div>
</div>
<div class="panel panel-sidebar">
   <div class="panel-heading">
      <h2 class="panel-title">
         <i class="icon icon-filter"></i>
         Sim theo loại
      </h2>
      <div class="clearfix"></div>
   </div>
   <div class="panel-body box_filter filter_type">
      <?
      #+
      #+ Kiểm tra cache
      $cache_check = 0; // 0.Chua cache - 1.Da cache
      #+
      if($con_set_cache != 0){
         #+
         $cache_file_time  = $cache_file_time_ext;
         #+ Ten file cache
         $cache_file_path  = 'tpl_simType';
         #+ Folder cache
         $cache_folder_path   = '../../store/cache/all/ext/ext';
         
         #+
         #+ Kiem tra xem co file cache khong
         $sCache = new Cache($cache_file_path,$cache_file_time,'',$cache_folder_path);
         if($sCache->is_cache()) {
            $cache_check = 1;
         } // End if(!$sCache->is_cache())   
      } // End if($con_set_cache == 0)

      #+
      #+ Neu nhu khong co cache thi thuc hien
      if($cache_check == 0){
         $str  = '';
         $str .= _temp::_simType_v2(array(), $iType);

         #+
         #+ Ghi cache
         if($con_set_cache != 0){
            $sCache->cache($str);   
         } // End if($con_set_cache != 0)
      } // End if($cache_check == 1){
      ?>
      <ul>
         <?=$str?>
      </ul>
   </div>
</div>
<?
$query = " SELECT * FROM tbl_category"
      ." WHERE cat_type = 'sim' AND cat_active = 1"
      ." ORDER BY cat_order ASC LIMIT 6"
      ;
$db_cat = new db_query($query);
$arrCatSim = convert_result_set_2_array($db_cat->result);
unset($db_cat);

if(!empty($arrCatSim)){
?>
<div class="panel panel-sidebar">
   <div class="panel-heading">
      <h2 class="panel-title">
         <i class="icon icon-filter"></i>
         Sim theo mạng
      </h2>
      <div class="clearfix"></div>
   </div>
   <div class="panel-body box_filter">
      <ul>
         <?
         foreach ($arrCatSim as $key => $value) {
            $link_cat = createLink("cat",$value);
            $active   = ($value["cat_id"] == $iCat ? ' class="active"' : '');
         ?>
         <li<?=$active?>>
            <a href="<?=$link_cat?>">
               <i class="icon_check"></i>
               <span>Sim <?=$value["cat_name"]?></span>
            </a>
         </li>
         <?
         }
         ?>
      </ul>
   </div>
</div>
<?
}
?>