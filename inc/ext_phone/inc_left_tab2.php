<div class="panel-group-primary" id="accordion-tab2">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h4 class="panel-title"> 
                <a<?=$iType != 0 ? '' : ' class="collapsed"'?> data-toggle="collapse" data-parent="#accordion-tab2" href="#collapse-primary-loai"> 
                    <i class="icon icon-this"></i>&nbsp;
                    Tìm nhanh theo loại
                </a> 
            </h4>
        </div>
        <div id="collapse-primary-loai" class="panel-collapse <?=$iType != 0 ? 'in' : 'collapse'?>">
            <div class="panel-body">
            	<ul>
					<?
                    include('inc_left_sim_loai.php');
                    ?>	
                </ul>
            </div>
        </div>
    </div>
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h4 class="panel-title"> 
                <a<?=$iPrice != 0 ? '' : ' class="collapsed"'?> data-toggle="collapse" data-parent="#accordion-tab2" href="#collapse-primary-gia"> 
                    <i class="icon icon-this"></i>&nbsp;
                    Tìm nhanh theo giá
                </a> 
            </h4>
        </div>
        <div id="collapse-primary-gia" class="panel-collapse  <?=$iPriceFrom != 0 || $iPriceTo != 0 ? 'in' : 'collapse'?>">
            <div class="panel-body">
            	<ul>
					<?
                    include('inc_left_sim_gia.php');
                    ?>
                </ul>
            </div>
        </div>
    </div>
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h4 class="panel-title"> 
                <a<?=$iDauSo != 0 ? '' : ' class="collapsed"'?> data-toggle="collapse" data-parent="#accordion-tab2" href="#collapse-primary-dauso">
                    <i class="icon icon-this"></i>&nbsp;
                    Tìm nhanh theo đầu số
                </a> 
            </h4>
        </div>
        <div id="collapse-primary-dauso" class="panel-collapse <?=$iDauSo != 0 ? 'in' : 'collapse'?>">
            <div class="panel-body">
            	<ul>
					<?
                    include('inc_left_sim_dauso.php');
                    ?>
                </ul>
            </div>
        </div>
    </div>
</div>