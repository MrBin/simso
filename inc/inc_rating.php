<?
// Lấy điểm đánh giá dòng sản phẩm
$arrReviewStar	= array();
for($i = 1; $i <= 5; $i++){

	$db_review	= new db_query("SELECT COUNT(*) AS total
										 FROM rating
										 WHERE rat_record_id = " . $recodeID . " AND rat_rating = " . $i);
	if($row	= mysql_fetch_assoc($db_review->result))	$arrReviewStar[$i]	= $row["total"];
	$db_review->close();
	unset($db_review);
}

$rating_type	= RATING_TYPE_PRODUCT;
$rating_default= 5;
// Khai báo dữ liệu để phân trang
$page_size		= 10;
$current_page	= getValue("p");
$page_url		= "/ajax/load_rating.php?type=" . $rating_type . "&record_id=" . $recodeID;
// Đếm xem có bao nhiêu dữ liệu
$db_count		= new db_query("SELECT COUNT(*) AS count
										 FROM rating
										 WHERE rat_type = " . $rating_type . " AND rat_date < " . time() . " AND rat_record_id = " . $recodeID);
$row				= mysql_fetch_assoc($db_count->result);
$total_record	= $row["count"];
$max_page		= (($total_record % $page_size) > 0 ? intval($total_record / $page_size) + 1 : $total_record / $page_size);
if($current_page > $max_page) $current_page	= $max_page;
if($current_page < 1) $current_page	= 1;
$db_count->close();
unset($db_count);
// Lấy dữ liệu
$db_rating		= new db_query("SELECT rat_id, rat_admin_id, rat_order_id, rat_rating, rat_name, rat_mobile, rat_content, rat_count_reply, rat_date
										 FROM rating
										 WHERE rat_type = " . $rating_type . " AND rat_date < " . time() . " AND rat_record_id = " . $recodeID . " AND rat_status <> 2
										 ORDER BY rat_date DESC
										 LIMIT " . ($current_page - 1) * $page_size . ", " . $page_size);
$arrRating	= convert_result_set_2_array($db_rating->result);
$db_rating->close();
unset($db_rating);
?>
<h2 id="reviews">
	<?
	if($total_record > 0) echo '<span class="count_rating">' . format_number($total_record) . '</span> đánh giá ' . $rowDetail["pro_name"];
	else echo 'Bạn hãy là người đầu tiên đánh giá ' . $rowDetail["pro_name"];
	?>
</h2>
<div id="detail_product_reply_rate" class="detail_product_rate_estore">
	<div class="rate_estore_tb" id="suggest_review">
		<div class="show_rate_estore fl">
			<div class="rate_estore_review">
				<div class="start_review"><?=($total_record > 0 ? format_number($productRating["rating"], 1) : 0)?></div>
				<div class="text">Sản phẩm như mô tả<br />(<?=format_number($total_record)?> đánh giá)</div>
			</div>
			<div id="service_rate" class="tab_content_detail">
			<?
			if(!empty($arrReviewStar)){
				krsort($arrReviewStar);
				foreach($arrReviewStar as $key => $value){
					$percent_vote	= 0;
					if($total_record > 0) $percent_vote	= $value / $total_record * 100;
					?>
					<div>
						<span class="point"><?=$key?> sao</span>
						<span class="percent_bar"><span style="width:<?=$percent_vote?>%"></span></span>
						<span class="percent"><?=format_number($percent_vote, 1)?>%</span>
					</div>
					<?
					} //End foreach($arrRepMedium["detail"] as $key => $value)
				} // End if(is_array($arrRepMedium["detail"]) && count($arrRepMedium["detail"]) > 0)
				?>
			</div>
		</div>
		<div class="suggest_review fl">
			<div class="btn_buy_now_review">
				<a href="javascript:;" onclick="showFromRating(jQuery(this));" title="Gửi đánh giá của bạn" class="btn_buy_now">Gửi đánh giá của bạn</a>
			</div>
		</div>
		<div class="clear"></div>
		<div class="rating_form">
			<div class="rating_form_star">
				<span class="name">
					Chọn đánh giá của bạn:
				</span>
				<span class="star">
					<?
					$arrStar	= array("Không thích", "Tạm được", "Bình thường", "Rất tốt", "Tuyệt vời quá");
					foreach($arrStar as $key => $value) echo '<i class="icm icm_star-full2' . ($key < $rating_default ? ' active' : '') . '" value="' . ($key + 1) . '" text="' . $value . '"></i>';
					?>
				</span>
				<span class="text"><span><?=$arrStar[$rating_default - 1]?></span></span>
				<span class="clear"></span>
			</div>
			<div id="rating_form_0" class="rating_form_control">
				<div class="row">
					<div class="group_input col-xs-6">
						<textarea class="form_control" name="content" placeholder="Nhập đánh giá về sản phẩm *"></textarea>
					</div>
					<div class="group_input col-xs-6">
						<input class="form_control frm_name" type="text" name="name" value="" placeholder="Họ và tên *" maxlength="150" onkeypress="if(event.keyCode==13){ratingPostData(); return false;}" />
						<input class="form_control frm_phone" type="text" name="mobile" value="" placeholder="Số điện thoại" maxlength="150" onkeypress="if(event.keyCode==13){ratingPostData(); return false;}" />
						<input type="button" class="form_button" value="Gửi đánh giá của bạn" onclick="ratingPostData()" />
					</div>
				</div>
				<span class="clear"></span>
				<?/* if($con_admin_id > 0){?>
					<input class="form_control" type="text" name="date" value="<?=date("d/m/Y")?>" placeholder="Ngày" maxlength="10" style="margin-left: 0; text-align: right; width: 110px;" onkeypress="if(event.keyCode==13){ratingPostData(); return false;}" />
					<input class="form_control" type="text" name="time" value="<?=date("H:i:s")?>" placeholder="Giờ" maxlength="8" style="margin-left: 1.25%; text-align: right; width: 90px;" onkeypress="if(event.keyCode==13){ratingPostData(); return false;}" />
					<input class="form_control" type="text" name="order_id" value="" placeholder="ID đơn hàng" maxlength="8" style="margin-left: 1.25%; text-align: right; width: 110px;" onkeypress="if(event.keyCode==13){ratingPostData(); return false;}" />
				<? }*/?>
			</div>
		</div>
	</div>
</div>
<?
// Hiển thị rating
$class_rating	= new rating($rating_type);
echo '<ul id="rating_list">' . $class_rating->generate_rating($arrRating) . '</ul>';

// Nếu đủ record thì mới phân trang
if($total_record > $page_size){
?>
<div class="page_bar"></div>
<script type="text/javascript">
jQuery(function(){
	var opts	= {
		total_record	: <?=$total_record?>,
		page_size		: <?=$page_size?>,
		current_page	: <?=$current_page?>,
		page_url			: "<?=$page_url?>",
		page_object		: jQuery(".page_bar"),
		content_object	: jQuery("#rating_list"),
	}
	page_bar(opts);
});
</script>
<?
}// End if($total_record > $page_size)
?>
<script type="text/javascript">
var ratingConfig	= {
	rating		: <?=$rating_default?>,
	text			: "",
	listDomEle	: null,
	starDomEle	: null,
};

function showFromRating(domEle){
	domEle.toggleClass("active");

	if(domEle.hasClass("active")){
		domEle.html("Đóng");
		jQuery(".rating_form").fadeIn();
	}
	else{
		jQuery(".rating_form").fadeOut();
		domEle.html("Gửi đánh giá của bạn");
	}
}

function ratingChangeText(rating){
	var text	= "";
	if(rating > 0) text	= jQuery(".rating_form_star").find("i[value='" + rating + "']").attr("text");
	if(text != "") text	= '<span>' + text + '</span>';
	jQuery(".rating_form_star").find(".text").html(text);
}

function ratingPostData(){
	var rating_id	= arguments[0] || 0;
	var formDomEle	= jQuery("#rating_form_" + rating_id);
	var content		= formDomEle.find("[name='content']");
	var name			= formDomEle.find("[name='name']");
	var mobile		= formDomEle.find("[name='mobile']");
	if(rating_id == 0 && ratingConfig.rating == 0){ windowPrompt({ content: "Bạn chưa chọn sao đánh giá.", alert: true }); return false; }
	if(content.val().length <= 10){ windowPrompt({ content: "Nội dung phải lớn hơn 10 ký tự.", alert: true, onClosed: function(){ content.focus(); } }); return false; }
	if(name.val() == ""){ windowPrompt({ content: "Bạn chưa nhập họ và tên.", alert: true, onClosed: function(){ name.focus(); } }); return false; }
	formDomEle.find(".form_button").prop("disabled", true);
	jQuery.post("/ajax/add_rating.php", {
		type		: <?=$rating_type?>,
		record_id: <?=$recodeID?>,
		rating	: ratingConfig.rating,
		rating_id: rating_id,
		content	: content.val(),
		name		: name.val(),
		mobile	: mobile.val(),
	<? if($con_admin_id > 0){?>
		date		: formDomEle.find("[name='date']").val(),
		time		: formDomEle.find("[name='time']").val(),
		order_id	: formDomEle.find("[name='order_id']").val(),
		is_admin	: (formDomEle.find("[name='is_admin']").is(":checked") ? 1 : 0),
	<? }?>
	}).done(function(data){
		if(data == "") return false;
		if(rating_id > 0){
			var domEle	= jQuery("#reply_list_" + rating_id);
			if(domEle.length) domEle.append(data);
			else jQuery("#rating_" + rating_id).after('<li id="reply_list_' + rating_id + '" class="reply">' + data + '</li>');
			jQuery(".reply_form").remove();
		}
		else{
			ratingConfig.listDomEle.prepend(data);
			ratingResetForm(formDomEle);
		}
	<? if($con_admin_id == 0){?>
		windowPrompt({
			close			: false,
			content		: '<div style="padding: 20px; text-align: center;">Cảm ơn bạn đã đánh giá sản phẩm này.<br /><input type="button" id="rating_form_button" class="form_button_2" value="Đóng" style="margin-top: 15px; width: 100px;" onclick="closeWindowPrompt()" /></div>',
			onComplete	: function(domEle){ setTimeout(function(){ jQuery("#rating_form_button").focus(); }, 10); },
		});
	<? }?>
	});
}

function generateRatingReplyForm(rating_id, name){
	var defText	= "@" + name + ": ";
	var defName	= "";
	<? if($con_admin_id > 0) echo 'defText	= "Chào anh,\n"; defName = "' . $con_site_name . '";';?>
	var html		= '<div id="rating_form_' + rating_id + '" class="rating_form_control reply_form">' +
							'<textarea class="form_control" name="content" placeholder="Nhập thảo luận của bạn *"></textarea>' +
						<?
						$arrText	= array("Thông tin đến anh.", "Chia sẻ cùng anh.", "Thân gửi đến anh.", "Mong hồi âm từ anh.", "Chúc anh chọn được sản phẩm ưng ý.", "Mong anh thông cảm.");
						if($con_admin_id > 0){
							$text	= '';
							foreach($arrText as $key => $value){
								if($key > 0) $text .= ' &nbsp; ';
								$text	.= '<a href="javascript:;" onclick="insertText(jQuery(this))">' . $value . '</a>';
							}
							$text	.= '<br />';
							$text	.= '<a href="javascript:;" onclick="insertText(jQuery(this))">Sản phẩm ' . $rowDetail["pro_name"] . '</a> &nbsp; ';
							$text	.= '<a href="javascript:;" onclick="createLink()">TẠO LINK</a>';
							foreach($class_rating->array_replace as $key => $value){
								$text .= ' &nbsp; ';
								$text	.= '<a href="javascript:;" onclick="insertText(jQuery(this))">' . $value . '</a>';
							}
							echo "'<div class=\"rating_content_option\">" . $text . "</div>' +";
						}
						?>
							'<input class="form_control" type="text" name="name" value="' + defName + '" placeholder="Họ và tên *" maxlength="150" onkeypress="if(event.keyCode==13){ratingPostData(' + rating_id + '); return false;}" />' +
							'<input class="form_control" type="text" name="mobile" value="" placeholder="Số điện thoại" maxlength="150" onkeypress="if(event.keyCode==13){ratingPostData(' + rating_id + '); return false;}" />' +
							'<input type="button" class="form_button" value="Gửi đánh giá" onclick="ratingPostData(' + rating_id + ')" />' +
						<? if($con_admin_id > 0){?>
							'<input class="form_control" type="text" name="date" value="' + jQuery("#rating_" + rating_id).find(".time").text().substr(0, 10) + '" placeholder="Ngày" maxlength="10" style="margin-left: 0; text-align: right; width: 110px;" onkeypress="if(event.keyCode==13){ratingPostData(' + rating_id + '); return false;}" />' +
							'<input class="form_control" type="text" name="time" value="<?=date("H:i:s")?>" placeholder="Giờ" maxlength="8" style="margin-left: 1.25%; text-align: right; width: 90px;" onkeypress="if(event.keyCode==13){ratingPostData(' + rating_id + '); return false;}" />' +
							'<input type="checkbox" id="is_admin" name="is_admin" value="1" checked="checked" style="margin-left: 1.25%; width: auto;" /><label for="is_admin" style="font-size: 1rem; margin-left: 5px;">Quản trị viên</label>' +
						<? }?>
						'</div>';
	jQuery(".reply_form").remove();
	var domEle	= (!jQuery("#reply_list_" + rating_id).length ? jQuery("#rating_" + rating_id) : jQuery("#reply_list_" + rating_id));
	domEle.after(html);
	setTimeout(function(){ jQuery("#rating_form_" + rating_id).find("textarea").focus().val(defText); }, 10);
}

function ratingResetForm(formDomEle){
	ratingConfig.rating	= 0;
	ratingConfig.text		= "";
	ratingConfig.starDomEle.removeClass("active");
	formDomEle.find(".form_control").val("");
	formDomEle.find(".form_button").prop("disabled", false);
<? if($con_admin_id > 0){?>
	var currentDate		= new Date();
	var strDate				= currentDate.getDate() + "/" + (currentDate.getMonth()+1) + "/" + currentDate.getFullYear();
	var strTime				= currentDate.getHours() + ":" + currentDate.getMinutes() + ":" + currentDate.getSeconds();
	formDomEle.find("[name='date']").val(strDate);
	formDomEle.find("[name='time']").val(strTime);
<? }?>
	ratingChangeText("");
}

jQuery(function(){
	ratingConfig.listDomEle	= jQuery("#rating_list");
	ratingConfig.starDomEle	= jQuery(".rating_form_star").find("i");
	ratingConfig.starDomEle.hover(
		function(){
			ratingConfig.starDomEle.removeClass("active").end().find("i:lt(" + jQuery(this).attr("value") + ")").addClass("active");
			ratingChangeText(jQuery(this).attr("value"));
			if(siteConfig.window_width < 600) ratingConfig.rating	= jQuery(this).attr("value");
		},
		function(){
			ratingConfig.starDomEle.removeClass("active").end().find("i:lt(" + ratingConfig.rating + ")").addClass("active");
			ratingChangeText(ratingConfig.rating);
		}
	);
	if(siteConfig.window_width >= 600){
		ratingConfig.starDomEle.click(function(){
			ratingConfig.rating	= jQuery(this).attr("value");
		});
	}
});
</script>
<?
// JS + CSS khi admin login
if($con_admin_id > 0){
?>
<script type="text/javascript">
function createLink(){
	var onPress	=  ' onkeypress="if(event.keyCode==13){createLinkAct(); return false;}"';
	var html		=  '<div id="create_link"><div style="margin-bottom: 15px;"><input type="text" placeholder="https://" class="form_control frm_link" name="link" style="width: 360px;"' + onPress + ' /></div>' +
						'<div style="margin-bottom: 15px;"><input type="text" placeholder="Text..." class="form_control frm_text" name="text" style="width: 360px;"' + onPress + ' /></div>' +
						'<div><input type="checkbox" id="frm_nofollow" class="frm_nofollow" value="1" /><label for="frm_nofollow" style="color: #333; margin-left: 5px;">No Follow</label><input type="button" class="form_button" value="Tạo Link" onclick="createLinkAct()" style="float: right;" /></div></div>';
	windowPrompt("Tạo link bài viết", html);
	jQuery("#create_link").find(".frm_link").focus();
}
function createLinkAct(){
	var domEle	= jQuery("#create_link");
	var link		= domEle.find(".frm_link").val().replace("<?=$con_server_name?>", "");
	var text		= domEle.find(".frm_text").val().replace("<?=$con_server_name?>", "");
	if(text == "") text = link;
	var html		= '<a href="' + link + '" target="_blank"' + (domEle.find(".frm_nofollow").is(":checked") ? ' rel="nofollow"' : '') + '>' + text + '</a>';
	insertText(html);
	closeWindowPrompt();
}
function insertText(domEle){
	var obj	= jQuery(".reply_form textarea");
	var tav	= obj.val();
	var text	= (typeof(domEle) == "object" ? domEle.text() : domEle);
	strPos	= obj[0].selectionStart;
	front		= (tav).substring(0, strPos),
	back		= (tav).substring(strPos, tav.length);
	obj.val(front + text + back).focus();
}
function ratingEdit(type, record_id){
	windowPrompt({ iframe: true, width: 750, height: 500, href: "/admin_tp/rating/edit.php?popup=1&type=" + type + "&record_id=" + record_id });
}
function ratingDelete(type, record_id){
	if(confirm("Bạn muốn xóa dữ liệu này không?")){
		jQuery.get("/admin_tp/rating/delete.php?ajax=1&type=" + type + "&record_id=" + record_id, function(data){
			if(data == 1){
				if(type == "rating"){
					var domEle	= jQuery("#rating_" + record_id);
					if(domEle.next().is(".reply")) domEle.next().fadeOut(function(){ jQuery(this).remove(); });
					domEle.fadeOut(function(){ jQuery(this).remove(); });
				}
				else{
					var domEle	= jQuery("#reply_" + record_id);
					if(domEle.parent().find(".wrapper").length > 1) domEle.fadeOut(function(){ jQuery(this).remove(); });
					else domEle.parent().fadeOut(function(){ jQuery(this).remove(); });
				}
			}
		});
	}
}
function generateRatingAction(){
	var domEle			= arguments[0] || ratingConfig.listDomEle.find(".wrapper");
	domEle.each(function(i){
		var arrTemp		= jQuery(this).attr("id").split("_");
		var type			= (arrTemp[0] == "reply" ? "rating_reply" : "rating");
		var record_id	= arrTemp[1];
		var html			=  '<a class="edit" href="javascript:;" onclick="ratingEdit(\'' + type + '\', ' + record_id + ')">Sửa</a>' +
								'<a class="edit" href="javascript:;" onclick="ratingDelete(\'' + type + '\', ' + record_id + ')">Xóa</a>';
		jQuery(this).find(".text").append(html);
	});
}
jQuery(function(){ generateRatingAction(); });
</script>
<style tyle="text/css">
#rating_list div.rating_content_option{ font-size: 13px; line-height: 160%; margin-bottom: 10px; }
#rating_list div.rating_content_option a{ white-space: nowrap; }
#rating_list .wrapper .edit{ display: none; font-size: 12px; margin-left: 10px; }
#rating_list .wrapper:hover .edit{ display: inline-block; }
</style>
<?
}// End if($con_admin_id > 0)
?>