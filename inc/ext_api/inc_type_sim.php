<?
#+
#+ Xem co search sim voi lon hon 2 ki tu hay khong
if($module == 'searchsim' && strlen(preg_replace("/[^0-9]/si","",$keyword)) < 2){
	echo '<div class="alert alert-info">Vui lòng nhập ít nhất 2 chữ số để tìm!</div>';	
	return;
} // End if(strlen(preg_replace("/[^0-9*]/si","",$keyword)) < 3)

#+
#+ Set bien
$page_size		= (getValue("page_size") > 0 ) ? getValue("page_size") : 55;
$sqlSelect		= '';
$sqlJoin			= '';
$sql 				= '';
$sqlOrder 		= '';
$sqlGroup 		= '';
$sqlOrderBy 	= '';
$sqlLimit		= '';
#+
$normal_class    	= "";
$selected_class  	= "active";
$page_prefix 	 	= "Trang";
$current_page    	= ( getValue("page") < 1 ) ? 1 : getValue("page");
#+

$query_string = '/';
if($module == 'home')
{
	$url	= '/trang-';	
}
elseif($module == 'simtype')
{
	$url	= '/'.$sType.'-';
}
elseif($module == 'simprice')
{
	$url	= '/gia-'.$iPriceFrom.'-'.$iPriceTo.'-';	
}
elseif($module == 'simdauso')
{
	$url	= '/'.$sCat.'-dau-'.$sDauSo.'-';	
}
elseif($module == 'simsodep')
{
	$url	= '/sim-so-dep-';
}
elseif($module == 'simgiare')
{
	$url	= '/sim-gia-re-'.$sCat.'-';
}
elseif($module == 'simre')
{
	$url	= '/Sim-gia-re-';
}
elseif($module == 'simhoptuoi' || $module == 'sim-hop-tuoi')
{
	$url	= '/sim-phong-thuy-hop-tuoi-'.$iNamSinh.'-';
}
elseif($module == 'simhopmenh')
{
	$url	= '/sim-phong-thuy-hop-menh-'.$sNguHanh.'-';
}
elseif($module == 'tra-phong-thuy')
{
	$url	= "/load/?type=sim&module=".$module."&iNguHanh=".$iNguHanh."&page=";
	$query_string = '';
}
elseif($module == 'searchsim' || $module == 'simajax')
{
	$url	= "/load/?type=sim&module=".$module."&keyword=".$keyword.'&iCat='.$iCat.'&iType='.$iType.'&iPrice='.$iPrice.'&iDauSo='.$iDauSo."&digit=".$digit."&sort=".$sort."&iNguHanh=".$iNguHanh."&page=";	
	$query_string = '';
}
elseif($module == 'simhot'){
	$url	= '/sim-hot-trong-ngay-';
	$query_string = '';
}
elseif($module == 'sim-ngay-sinh'){
	$url	= "/load/?type=sim&module=searchsim&keyword=" . $keyword . "&page=";	
	$query_string = '';
}
else
{
	$url	= '/'.$sCat.'-';	
}

/* Query */
/* Khai bao bien */
$sphinx_page_start   = ($current_page-1) * $page_size;
$sphinx_page_end     = $page_size;
$sphinx_keyword     = $keyword;

/* Thuc hien search */
$searchConfig =  array ("page"		=> $current_page,
								"page_size"	=> $page_size,
								"iCat"		=> $iCat,
								"iType"		=> $iType,
								"iPrice"		=> $iPrice,
								"iDauSo"		=> $iDauSo,
								"iDaiLy"		=> $iDaiLy,
								"digit"		=> $digit,
								"iNguHanh"	=> $iNguHanh,
								"module"		=> $module,
								"sort"		=> $sort,
								);

/* Thuc hien search */
$sphinx_search = new sphinx_search($sphinx_keyword);
$arrResult     = $sphinx_search->search($searchConfig);

$arrQuery		= array();
$total_record	= 0;

if(!empty($arrResult["data"])){
	$arrQuery		= $arrResult["data"];
	$total_record	= $arrResult["total_record"];
}

echo json_encode($arrQuery);
?>	