<?
#+
#+ Xem co search sim voi lon hon 2 ki tu hay khong
if($module == 'searchsim' && strlen(preg_replace("/[^0-9]/si","",$keyword)) < 3){
	echo '<div class="alert alert-info">Vui lòng nhập ít nhất 2 chữ số để tìm!</div>';	
	return;
} // End if(strlen(preg_replace("/[^0-9*]/si","",$keyword)) < 3)

#+
#+ Set bien
$page_size		= 55;
$sqlSelect		= '';
$sqlJoin			= '';
$sql 				= '';
$sqlOrder 		= '';
$sqlGroup 		= '';
$sqlOrderBy 	= '';
$sqlLimit		= '';
#+
$normal_class    	= "";
$selected_class  	= "active";
$page_prefix 	 	= "Trang";
$current_page    	= ( getValue("page") < 1 ) ? 1 : getValue("page");
#+
$query_string = '/';
if($module == 'home')
{
	$url	= '/trang-';	
}
elseif($module == 'simtype')
{
	$url	= '/'.$sType.'-';
}
elseif($module == 'simprice')
{
	$url	= '/gia-'.$iPriceFrom.'-'.$iPriceTo.'-';	
}
elseif($module == 'simdauso')
{
	$url	= '/'.$sCat.'-dau-'.$sDauSo.'-';	
}
elseif($module == 'simsodep')
{
	$url	= '/sim-so-dep-';
}
elseif($module == 'simgiare')
{
	$url	= '/sim-gia-re-'.$sCat.'-';
}
elseif($module == 'simre')
{
	$url	= '/Sim-gia-re-';
}
elseif($module == 'simhoptuoi')
{
	$url	= '/sim-phong-thuy-hop-tuoi-'.$iNamSinh.'-';
}
elseif($module == 'simhopmenh')
{
	$url	= '/sim-phong-thuy-hop-menh-'.$sNguHanh.'-';
}
elseif($module == 'tra-phong-thuy')
{
	$url	= "/load/?type=sim&module=".$module."&iNguHanh=".$iNguHanh."&page=";
	$query_string = '';
}
elseif($module == 'searchsim' || $module == 'simajax')
{
	$url	= "/load/?type=sim&module=".$module."&keyword=".$keyword.'&iCat='.$iCat.'&iType='.$iType.'&iPrice='.$iPrice.'&iDauSo='.$iDauSo."&digit=".$digit."&sort=".$sort."&iNguHanh=".$iNguHanh."&page=";	
	$query_string = '';
}
else
{
	$url	= '/'.$sCat.'-';	
}

include('inc_type_sim_sphinx.php');
// include('inc_type_sim_sql.php');

if($total_record <=0){
	$str_display  = '
			<div class="bgEF tB tRed tS15" align="center">
				Không có bản ghi được tìm thấy
			</div>
			';
}else{
	#+
	#+ Xem voi so luong sim nhu vay co bao nhieu trang
	if ($total_record % $page_size == 0){
		$num_of_page = $total_record / $page_size;
	}else{
		$num_of_page = (int)($total_record / $page_size) + 1;
	} // End if ($total_record % $page_size == 0)

	#+ 
	#+ Neu nhu co nguoi co tinh dien page lon hon so page hien tai
	if($current_page > $num_of_page) return;

	#+
	#+ Header Sim
	$str_head = $module != "tra-phong-thuy" && $append_data != 1 ? _temp::_simHead() : '';
	
	$str_display = '';
	$str = '';
	
	$str .= '
			<table class="table table-hover table-sim" style="width: 100%;">
				'.$str_head.'
				<tbody>
			';
	unset($str_head);
	
	$i=($current_page-1) * $page_size;
	foreach($arrQuery as $key => $row){
		$i++;
		
		#+
		$str .= _temp::_simBody($row,$i);
	} // End while($row = mysql_fetch_assoc($db_simType->result))
	unset($arrQuery);
	
	
	$str .= '
				</tbody>
			</table>
			';

	if($total_record>$page_size){
		$str .= '
				<div style="text-align: center">
					<ul class="pagination">
						'.generatePageBar($page_prefix,$cache_page,$page_size,$total_record,$url,$normal_class,$selected_class,$query_string,'data-pjax="#main"').'
					</ul>
				</div>
				';
		/*/
		$str_end_of_load_page = $cache_page >= $num_of_page ? '<span id="endOfLoadPage"></span>' : '';
		$str .= '
				<script type="text/javascript">
				var allowLoadPage = true; 
				var urlLoadPage = "'.$url.($cache_page+1).$query_string.'"; 
				loadPagePjax();
				</script>
				<div class="page-load-more" align="center">
					<div class="icon-load" align="center"><img src="/lib/img/load/icon-load.gif" alt="Icon Loading" /></div>
				</div>
				'.$str_end_of_load_page
				;
		//*/
	} // ENd if($total_record>$page_size)

	$str_display = $str;

} // End if($total_record <=0)
?>	


<div id="main-page" class="type-sim">
	<?
	if($module != 'home' && $module != 'tra-phong-thuy' && $append_data != 1){
	?>
	<style>
	.type-sim form select {
	    width: 49%;
	}
	</style>
	<form method="GET" action="/home/vn/type.php" target="_top" novalidate="" class="-amp-form">
		<input type="hidden" name="type" value="sim" />
		<input type="hidden" name="keyword" value="<?=$keyword?>" />
		<div class="alert-warning" align="center">
			<select name="iCat" id="searchMang" class="form-control" onchange="this.form.submit();">
				<option value="0">Chọn mạng</option>
				<?
				foreach($arrSimCat as $key => $row){
				?>
				<option <? if($iCat == $key) echo 'selected="selected"'?> value="<?=$key?>"><?=$row['cat_name']?></option>
				<?	
				}
				?>
			</select>
	        
			<select name="iDauSo" id="searchDauSo" class="form-control" onchange="this.form.submit();">
				<option value="0">Chọn đầu số</option>
				<?
				$sds_category = 0;
				foreach($arrSimDauSo as $key => $row){
				if($iCat == 0){
					if($row['sds_home'] == 1){
						#+
						if($sds_category != $row["sds_category"]){
							$sds_category = $row["sds_category"];
						?>
						<optgroup label="<?=ucwords($arrSimCat[$row['sds_category']]['cat_name'])?>"></optgroup>
						<?
						} // End if($sds_category != $row["sds_category"])
						
						
						?>
						<option <? if($iDauSo == $key) echo 'selected="selected"'?> value="<?=$key?>">Đầu số <?=$row['sds_name']?></option>
						<?	
					} // End if($row['sds_home'] == 1)
				}else{
					if($row['sds_category'] == $iCat){
					?>
					<option <? if($iDauSo == $key) echo 'selected="selected"'?> value="<?=$key?>">Đầu số <?=$row['sds_name']?></option>
					<?	
					}
				}
				} // End foreach($arrSimDauSo as $key => $row)
				?>
			</select>
	        
			<select name="iType" id="searchType" class="form-control" onchange="this.form.submit();">
				<option value="0">Chọn phân loại</option>
				<?
				foreach($arrSimType as $key => $row){
				?>
				<option <? if($iType == $key) echo 'selected="selected"'?> value="<?=$key?>"><?=$row['simtp_name']?></option>
				<?	
				}
				?>
			</select>
	        
			<select name="iPrice" id="searchPrice" class="form-control" onchange="this.form.submit();">
				<option value="0">Chọn khoảng giá</option>
				<?
				foreach($arrSimPrice as $key => $row){
				?>
				<option <? if($iPrice == $key) echo 'selected="selected"'?> value="<?=$key?>"><?=$row['pri_name']?></option>
				<?	
				}
				?>
			</select>
	
			<br />
			<select name="iNguHanh" id="searchMenh" class="form-control" onchange="this.form.submit();">
				<option value="0">Chọn mệnh</option>
				<?
				foreach($arrNguHanh as $key => $row){
				?>
				<option <? if($iNguHanh == $key) echo 'selected="selected"'?> value="<?=$key?>">Sim hợp Mệnh <?=$row?></option>
				<?	
				}
				?>
			</select>
			
			<select name="iNamSinh" id="searchNamSinh" class="form-control" onchange="this.form.submit();">
				<option value="0">Chọn năm sinh</option>
				<?
				for($i = 1950; $i<=date('Y'); $i++){
				?>
				<option <? if($iNamSinh == $i) echo 'selected="selected"'?> value="<?=$i?>">Sim hợp tuổi <?=$i?></option>
				<?	
				}
				?>
			</select>
			<input type="reset" value="Hủy chọn" class="button button-primary other-input" style="margin-top: 5px; min-width: 49%; line-height: 28px; height: 28px;" onclick="window.location.href='/home/vn/type.php?type=sim'" />
		</div>
		
	</form>
	<?
	} // End if($module != 'home')
	?>
    
    
	<?
	echo $str_display;
	
	unset($str_display);
	unset($str);
	?>
</div>