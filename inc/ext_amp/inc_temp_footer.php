<footer>
<div class="footer">
	<b>SIÊU THỊ SIM THẺ</b><br />
	Trung tâm phân phối sim số đẹp thẻ cào các loại<br />
	Địa chỉ : Số 575 Giải Phóng - Hoàng Mai - Hà Nội<br />
	Liên hệ : 0989 575.575 - 0932 33.8888<br />
	Website : www.sieuthisimthe.com<br />
	Email : sieuthisimthe.com@gmail.com<br />
</div>
</footer>

<amp-analytics type="googleanalytics" id="analytics">
<script type="application/json">
  {
    "vars": {
      "account": "UA-64579392-1"
    },
    "triggers": {
      "default pageview": {
        "on": "visible",
        "request": "pageview",
        "vars": {
          "title": "<?=$con_site_title?>"
        }
      }
    }
  }
</script>
</amp-analytics>