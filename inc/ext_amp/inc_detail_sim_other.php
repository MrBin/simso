<?
/* Khai bao bien */
$html_col      = 2;
$html_title    = 'Sim số đẹp có thể bạn quan tâm';

$sphinx_page_start   = 0;
$sphinx_page_end     = 50;	
$sphinx_keyword      = "(*".substr($iData, -8, 8)."$ | *".substr($iData, -7, 7)."$ | *".substr($iData, -6, 6)."$ | *".substr($iData, -5, 5)."$)";

/* Thuc hien search */
$sphinxSearch  = new sphinxSearch();
$result = $sphinxSearch->search($sphinx_page_start, $sphinx_page_end, $sphinx_keyword, 'simajax', 0, 0, 0, 0, 0, 0, 0);
$result = json_decode($result, true);

/* Tra ve ket qua */
$total_record  = $result['total_record'];
$arrQuery      = $result['query'];

/* Tra ve Html */
$html = '';

$i = 0;
foreach($arrQuery as $k => $row){
   $i++;  
    
   $html .= _temp::_simDetailOther($row, $html_col);		
}
?>

<div>
   <div><b><?=$html_title?></b></div>
    
   <div><?=$html?></div>
         
</div>