<?
// Google Captcha
require_once('../../kernel/classes/gcaptcha/autoload.php');
// Register API keys at https://www.google.com/recaptcha/admin
$siteKey 	= '6LcUBCITAAAAAJBZuIfXTMQl77tyiJAAyn2e-JsA';
$secret 		= '6LcUBCITAAAAAKIJTnEG3d7QHDuz08rQHCIpqNb-';
// reCAPTCHA supported 40+ languages listed here: https://developers.google.com/recaptcha/docs/language
$lang 		= 'vi';

// Khai bao bien
$fs_table			= "tbl_yeucau";
$fs_redirect 		= $_SERVER['REQUEST_URI'];
$thoigian			= date("Y-m-d H:i:s");
$errorMsg 			= "";
$action				= getValue("actions", "str", "POST", "");
$code					= getValue("code", "str", "POST", "");
$simcard				= getValue("simcard", "str", "POST", "");
$simcard				= substr($simcard,0,15);
$hoten				= getValue("hoten", "str", "POST", "");
$hoten				= strip_tags($hoten);
$hoten				= substr($hoten,0,25);
$hoten 				= preg_replace("/[0-9]/si"," ",$hoten);

$hoten_check = RemoveSign($hoten);
$hoten_check = explode(' ',$hoten_check);
if(strlen($hoten_check[0]) >= 8){
	echo "<script language='javascript'>alert('" . tdt("Vui lòng kiểm tra lại họ tên") . "')</script>";
	redirect($fs_redirect);	
	exit();
}

#+
$myform = new generate_form();
//$myform->removeHTML(0);
#+
$myform->addTable($fs_table);
#+
$myform->add("simcard","simcard",0,1,"",1,"Điền số cần mua",0,"");
$myform->add("giaban","giaban",0,0,"",1,"Điền giá mua",0,"");
$myform->add("dienthoai","dienthoai",0,0,"",1,"Điền số điện thoại",0,"");
$myform->add("hoten","hoten",0,1,"",1,"Điền họ tên",0,"");
$myform->add("diachi","diachi",0,0,"",0,"",0,"");
$myform->add("dtban","dtban",0,0,"",0,"",0,"");
$myform->add("thanhpho","thanhpho",0,0,"",0,"",0,"");
$myform->add("noidung","noidung",0,0,"",0,"",0,"");
$myform->add("phanloai","phanloai",1,1,2,0,"",0,"");
$myform->add("thoigian","thoigian",0,1,"",0,"",0,"");
$myform->addjavasrciptcode('if(document.getElementById("code").value == ""){ alert("Điền mã xác nhận !"); document.getElementById("code").focus(); return false;}');

#+
#+ Neu co gui form thi thuc hien
if($action == "submitForm"){
	$g_captcha = getValue('g-recaptcha-response', 'str', 'POST', '');

	if($g_captcha != ''){
		// Tạo captcha từ mã bảo mật
		$recaptcha = new \ReCaptcha\ReCaptcha($secret);
		
		// Xác thực mã captcha từ post và ip bắn lên
		$resp = $recaptcha->verify($g_captcha, $_SERVER['REMOTE_ADDR']);

		if ($resp->isSuccess()){
			$errorMsg .= $myform->checkdata();	//Check Error!
			$errorMsg .= $myform->strErrorFeld ;	//Check Error!
			if($errorMsg == ""){
				$db_ex = new db_execute($myform->generate_insert_SQL());
				//echo $myform->generate_insert_SQL();
				echo '<script type="text/javascript">alert("Đặt mua sim thành công - Quý khách vui lòng đợi để cửa hàng kiểm duyệt")</script>';
				redirect($fs_redirect);
				exit();
			}else{
				echo '<script type="text/javascript">alert("' . $errorMsg . '")</script>';
			}
		}else{
			// Error: $resp->getErrorCodes()
			echo '<script type="text/javascript">alert("Mã xác nhận không chính xác")</script>';
		}// End if ($resp->isSuccess())
		
	}else{
		echo '<script type="text/javascript">alert("Bạn chưa xác thực là người dùng")</script>';
	}// End if($g_captcha != '')
	
} // End if($action == "submitForm"){

//add form for javacheck
$myform->addFormname("submitForm");
$myform->checkjavascript();
$myform->evaluate();
//*/
?>
<style>
.text{
	margin-top: 10px;
}
</style>
<article>
	<div class="breadcrumb">
		Mua sim số đẹp theo yêu cầu
	</div>
    
    <b class="text-center">Hướng dẫn : Quý khách có thể gọi trực tiếp để mua sim nhanh hơn</b>

	<form name="submitForm" action="" method="post">
		<input class="form-control" type="hidden" name="actions" value="submitForm" />
		
		Đặt số: *<br />
      <input class="form-control" type="text" name="simcard" id="simcard" value="<?=$simcard?>" />
      
      Giá mua: *<br />
      <input class="form-control" type="text" name="giaban" id="giaban" value="<?=$giaban?>" />
		
		Di động: *<br />
		<input class="form-control" type="text" name="dienthoai" id="dienthoai" value="<?=$dienthoai?>" />
		
		
		Họ tên: *<br />
		<input class="form-control" type="text" name="hoten" id="hoten" value="<?=$hoten?>" onkeyup="telexingVietUC(this,event)" />
		
		 Địa chỉ:<br />
		<input class="form-control" type="text" name="diachi" id="diachi" value="<?=$diachi?>" onkeyup="telexingVietUC(this,event)" />

		Thông tin khác:<br />
		<textarea name="noidung" id="noidung" class="form-control" cols="50" rows="5" onkeyup="telexingVietUC(this,event)"><?=$noidung?></textarea>
		
		Xác thực người dùng:<br />
		<div class="g-recaptcha" data-sitekey="<?=$siteKey?>"></div>
		<script type="text/javascript" src="https://www.google.com/recaptcha/api.js?hl=<?=$lang?>"></script>

		<button class="button button-primary other-input" onclick="validateForm();">Gửi đi</button> &nbsp;
      <button class="button button-primary other-input">Làm lại</button>
		 
    </form>
</article>