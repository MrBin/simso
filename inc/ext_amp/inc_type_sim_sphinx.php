<?
/* Khai bao bien */
$sphinx_page_start   = ($current_page-1) * $page_size;
$sphinx_page_end     = $page_size;
$sphinx_keyword      = $keyword;

/* Thuc hien search */
$sphinxSearch  = new sphinxSearch();
$result = $sphinxSearch->search($sphinx_page_start, $sphinx_page_end, $sphinx_keyword, $module, $iCat, $iType, $iPrice, $iDauSo, $digit, $iNguHanh, $sort);
$result = json_decode($result, true);

/* Tra ve ket qua */
$total_record  = $result['total_record'];
$arrQuery      = $result['query'];

# print_r($result);
?>