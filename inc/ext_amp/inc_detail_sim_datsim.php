<?
// Google Captcha
require_once('../../kernel/classes/gcaptcha/autoload.php');
// Register API keys at https://www.google.com/recaptcha/admin
$siteKey 	= '6LcUBCITAAAAAJBZuIfXTMQl77tyiJAAyn2e-JsA';
$secret 		= '6LcUBCITAAAAAKIJTnEG3d7QHDuz08rQHCIpqNb-';
// reCAPTCHA supported 40+ languages listed here: https://developers.google.com/recaptcha/docs/language
$lang 		= 'vi';
 
$fs_table			= "tbl_yeucau";
$fs_redirect 		= $_SERVER['REQUEST_URI'];
$thoigian			= date("Y-m-d H:i:s");
$errorMsg 			= "";
$action				= getValue("actions", "str", "POST", "");
$code					= getValue("code", "str", "POST", "");
$simcard				= $sProName;
$giaban				= $sProPrice;

$hoten				= getValue("hoten", "str", "POST", "");
$hoten				= strip_tags($hoten);
$hoten				= substr($hoten,0,25);
$hoten 				= preg_replace("/[0-9]/si"," ",$hoten);

$hoten_check = RemoveSign($hoten);
$hoten_check = explode(' ',$hoten_check);
if(strlen($hoten_check[0]) >= 8){
	echo "<script language='javascript'>alert('" . tdt("Vui lòng kiểm tra lại họ tên") . "')</script>";
	redirect($fs_redirect);	
	return;
} 

//
$myform = new generate_form();
//$myform->removeHTML(0);

$myform->addTable($fs_table);


$myform->add("simcard","simcard",0,1,"",0,"",0,"");
$myform->add("giaban","giaban",0,1,"",0,"",0,"");
$myform->add("hoten","hoten",0,1,"",1,"Điền họ tên",0,"");
$myform->add("dienthoai","dienthoai",0,0,"",1,"Điền điện thoại",0,"");
$myform->add("diachi","diachi",0,0,"",0,"",0,"");
$myform->add("noidung","noidung",0,0,"",0,"",0,"");
$myform->add("phanloai","phanloai",1,1,1,0,"",0,"");
$myform->add("thoigian","thoigian",0,1,"",0,"",0,"");
$myform->addjavasrciptcode('if(document.getElementById("code").value == ""){ alert("' . tdt("Điền mã xác nhận") . ' !"); document.getElementById("code").focus(); return false;}');

//
if($action == "submitForm"){
	$g_captcha = getValue('g-recaptcha-response', 'str', 'POST', '');

	if($g_captcha != ''){
		// Tạo captcha từ mã bảo mật
		$recaptcha = new \ReCaptcha\ReCaptcha($secret);
		
		// Xác thực mã captcha từ post và ip bắn lên
		$resp = $recaptcha->verify($g_captcha, $_SERVER['REMOTE_ADDR']);

		if ($resp->isSuccess()){
			$errorMsg .= $myform->checkdata();	//Check Error!
			$errorMsg .= $myform->strErrorFeld ;	//Check Error!
			if($errorMsg == ""){
				$db_ex = new db_execute($myform->generate_insert_SQL());
				//echo $myform->generate_insert_SQL();
				echo '<script type="text/javascript">alert("Đặt mua sim thành công")</script>';
				redirect($fs_redirect);
				exit();
			}else{
				echo '<script type="text/javascript">alert("' . $errorMsg . '")</script>';
			}
		}else{
			// Error: $resp->getErrorCodes()
			echo '<script type="text/javascript">alert("Mã xác nhận không chính xác")</script>';
		}// End if ($resp->isSuccess())
		
	}else{
		echo '<script type="text/javascript">alert("Bạn chưa xác thực là người dùng")</script>';
	}// End if($g_captcha != '')
	
}// End if($action == "submitForm")

//add form for javacheck
$myform->addFormname("fBuySim");
$myform->checkjavascript();
$myform->evaluate();
//*/
?>
<style>
.text{
	margin-top: 10px;
}
</style>
<div style="margin-top: 10px;">
	<div><b>Thông tin khách hàng</b></div>
	
	<form name="fBuySim" action="" method="post" onsubmit="validateForm(); return false;">
		<input class="form-control" type="hidden" name="actions" value="submitForm" />
		<table cellpadding="0" cellspacing="0" style="width: 90%;">
			<tr>
				<td>Họ tên<span style="color: red;">*</span></td>
				<td><input class="form-control" name="hoten" id="hoten" value="<?=$hoten?>" type="text" /></td>
			</tr>
			<tr>
				<td>Di động<span style="color: red;">*</span></td>
				<td><input class="form-control" name="dienthoai" id="dienthoai" value="<?=$dienthoai?>" type="text" /></td>
			</tr>
			<tr>
				<td>Địa chỉ</td>
				<td><input class="form-control" name="diachi" id="diachi" value="<?=$diachi?>" type="text" /></td>
			</tr>	
		</table>
		<div class="g-recaptcha" data-sitekey="<?=$siteKey?>"></div>
		<script type="text/javascript" src="https://www.google.com/recaptcha/api.js?hl=<?=$lang?>"></script>
		<div style="margin-top: 5px;">
			<button class="button button-primary other-input"><?=tdt("Gửi đi")?></button>
			<button class="button button-primary other-input"><?=tdt("Làm lại")?></button>
		</div>
	</form>
</div>
