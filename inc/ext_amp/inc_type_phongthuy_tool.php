<?
// Google Captcha
require_once('../../kernel/classes/gcaptcha/autoload.php');
// Register API keys at https://www.google.com/recaptcha/admin
$siteKey 	= '6LcUBCITAAAAAJBZuIfXTMQl77tyiJAAyn2e-JsA';
$secret 		= '6LcUBCITAAAAAKIJTnEG3d7QHDuz08rQHCIpqNb-';
// reCAPTCHA supported 40+ languages listed here: https://developers.google.com/recaptcha/docs/language
$lang 		= 'vi';
$g_captcha 	= getValue('g-recaptcha-response', 'str', 'GET', '');

//
$arrGioSinh = array(
	0=>"23 giờ đến 1 giờ" , 
	1=>"1 giờ đến 3 giờ" , 
	2=>"3 giờ đến 5 giờ" , 
	3=>"5 giờ đến 7 giờ" , 
	4=>"7 giờ đến 9 giờ" , 
	5=>"9 giờ đến 11 giờ" , 
	6=>"11 giờ đến 13 giờ" , 
	7=>"13 giờ đến 15 giờ" , 
	8=>"15 giờ đến 17 giờ" , 
	9=>"17 giờ đến 19 giờ" , 
	10=>"19 giờ đến 21 giờ" , 
	11=>"21 giờ đến 23 giờ" , 
	);
	
//	
$arrNgaySinh = array(
	1=>1 , 2=>2 , 3=>3 , 4=>4 , 5=>5 ,
	6=>6 , 7=>7 , 8=>8 , 9=>9 , 10=>10 ,
	11=>11 , 12=>12 , 13=>13 , 14=>14 , 15=>15 ,
	16=>16 , 17=>17 , 18=>18 , 19=>19 , 20=>20 ,
	21=>21 , 22=>22 , 23=>23 , 24=>24 , 25=>25 ,
	26=>26 , 27=>27 , 28=>28 , 29=>29 , 30=>30 ,
	31=>31 ,	
	);
	
//	
$arrThangSinh = array(
	1=>1 , 2=>2 , 3=>3 , 4=>4 , 5=>5 ,
	6=>6 , 7=>7 , 8=>8 , 9=>9 , 10=>10 ,
	11=>11 , 12=>12
	);
	
//
$arrNamSinh = array();
for($i = 1950; $i<=date('Y')-3; $i++){
	$arrNamSinh[$i] = $i;	
}

//
$arrSex = array(
	"Nam"=>"Nam" , 
	"Nữ"=>"Nữ"
	);

$submit 			= getValue('submit','str','POSTGET','');
$gio_sinh		= getValue('giosinh','int','POSTGET','');
$ngay_sinh		= getValue('ngaysinh','int','POSTGET','');
$thang_sinh		= getValue('thangsinh','int','POSTGET','');
$nam_sinh		= getValue('namsinh','int','POSTGET','');
$gioi_tinh 		= getValue('gioitinh','str','POSTGET','');
$so_dien_thoai	= getValue('sodienthoai','str','POSTGET','');
$so_dien_thoai = preg_replace("/[^0-9]/si","",$so_dien_thoai);
$so_dien_thoai = trim($so_dien_thoai);

if($module == 'simrandom'){
	$gioi_tinh 		= "Nam";
	$so_dien_thoai	= "0".$iData;
	$gio_sinh			= rand(1,12);
	$ngay_sinh		= rand(1,28);
	$thang_sinh		= rand(1,12);
	$nam_sinh		= rand(1950,1999);		
}
?>
<form action="/home/vn/type.php" method="GET">
	<input type="hidden" name="module" value="sim-phong-thuy" />
	<div class="body-tool">
		<input type="hidden" name="module_check" id="module_check" value="<?=$module?>" />
	    
	    <table cellpadding="4" cellspacing="0" width="100%">
	        <tr>
	            <td nowrap>Số sim</td>
	            <td><input type="text" name="sodienthoai" id="sodienthoai" value="<?=$so_dien_thoai?>" class="form-control" style="width: 80%; margin: 0;" /></td>
	        </tr>
	        <tr>
	            <td nowrap>Giờ sinh</td>
	            <td>
	                <select name="giosinh" id="giosinh" class="form-control">
	                    <option value="">Giờ sinh</option>
	                    <? foreach($arrGioSinh as $kgio => $gio){?>
	                    <option value="<?=$kgio?>" <? if($gio_sinh == $kgio) echo 'selected="selected"'?>><?=$gio?></option>
	                    <? }?>
	                </select>
	            </td>
	        </tr>
	        <tr>
	            <td nowrap>Ngày sinh</td>
	            <td>
	                <select name="ngaysinh" id="ngaysinh" class="form-control">
	                    <option selected="selected" value="">Ngày</option>           	 
	                    <? foreach($arrNgaySinh as $kngay => $ngay){?>
	                    <option value="<?=$kngay?>" <? if($ngay_sinh == $kngay) echo 'selected="selected"'?>><?=$ngay?></option>
	                    <? }?>     
	                </select>
	                
	                <select name="thangsinh" id="thangsinh" class="form-control">
	                    <option selected="selected" value="">Tháng</option>      	 	
	                    <? foreach($arrThangSinh as $kthang => $thang){?>
	                    <option value="<?=$kthang?>" <? if($thang_sinh == $kthang) echo 'selected="selected"'?>><?=$thang?></option>
	                    <? }?>  
	                </select>
	                       
		            <select name="namsinh" id="namsinh" class="form-control">
		                <option selected="selected" value="">Năm</option>         	
		                
		                <? foreach($arrNamSinh as $knam => $nam){?>
		                <option value="<?=$knam?>" <? if($nam_sinh == $knam) echo 'selected="selected"'?>><?=$nam?></option>
		                <? }?>  
		            </select>       
	            </td>
	        </tr>
	        <tr>
	            <td nowrap>Giới tính : </td>
	            <td> 
	                <select name="gioitinh" id="gioitinh" class="form-control">
	                    <option value="">Chọn</option>
	                    <? foreach($arrSex as $ksex => $sex){?>
	                    <option value="<?=$ksex?>" <? if($gioi_tinh == $ksex) echo 'selected="selected"'?>><?=$sex?></option>
	                    <? }?> 
	                </select>
	            </td>
	        </tr>
			<tr>
	        	<td nowrap>Xác thực<br />người dùng</td>
	        	<td>
					<div class="g-recaptcha" data-sitekey="<?=$siteKey?>" style="transform:scale(0.9);transform-origin:0;"></div>
					<script type="text/javascript" src="https://www.google.com/recaptcha/api.js?hl=<?=$lang?>"></script>
	        	</td>
			</tr>
			<tr>
	        	<td></td>
	        	<td>
					<input type="submit" name="submit" id="submit" value="Xem" style="width: 160px; font-size:18px; cursor:pointer" />
	        	</td>
			</tr>
		</table>
	    
	    <?
	    if($module != 'xem-sim-phong-thuy'){
		?>
	    <div class="social" style="padding: 10px;">
	        <script src="https://apis.google.com/js/plusone.js" type="text/javascript" language="javascript"></script>
	        <g:plusone size="medium" href="http://www.sieuthisimthe.com/sim-phong-thuy.html"></g:plusone>
	    
	        <iframe src="//www.facebook.com/plugins/like.php?href=http%3A%2F%2Fwww.sieuthisimthe.com%2Fsim-phong-thuy.html&amp;send=false&amp;layout=button_count&amp;width=120&amp;show_faces=false&amp;action=like&amp;colorscheme=light&amp;font&amp;height=21&amp;appId=173270492684652" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:120px; height:21px;" allowTransparency="true"></iframe>
	    </div> 
	    <?
	    }
		?>
	</div>
</form>

<div class="main-phong-thuy">
    <div id="main-phong-thuy">
    	<b align="center">Tra cứu điểm sim phong thủy hợp tuổi, hợp mệnh</b>
		<?
      if($submit == 'Xem'){
			// Tạo captcha từ mã bảo mật
			$recaptcha = new \ReCaptcha\ReCaptcha($secret);
			
			// Xác thực mã captcha từ post và ip bắn lên
			$resp = $recaptcha->verify($g_captcha, $_SERVER['REMOTE_ADDR']);
		
			if ($resp->isSuccess()){
				include("inc_type_phongthuy_tracuu.php");	
			}else{
				echo '<div class="alert alert-info" style="margin:20px; font-size: 14px;">Bạn chưa xác thực người dùng</div>';
			}
						
		}else{

			if($module != 'xem-sim-phong-thuy'){
				include('inc_type_phongthuy_intro1.php');
			}	
		}
		?>
    </div>
</div>