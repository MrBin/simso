<?
$arrMenuCat = array(1 => array('name' => 'Viettel', 'rewrite' => 'viettel'),
							2 => array('name' => 'Vinaphone', 'rewrite' => 'vinaphone'),
							3 => array('name' => 'Mobifone', 'rewrite' => 'mobifone'),
							4 => array('name' => 'Vietnamobile', 'rewrite' => 'vietnamobile'),
							);
$arrMenuMenh = array(1 => array('name' => 'Kim', 'rewrite' => 'kim'),
							2 => array('name' => 'Thủy', 'rewrite' => 'thuy'),
							3 => array('name' => 'Mộc', 'rewrite' => 'moc'),
							4 => array('name' => 'Hỏa', 'rewrite' => 'hoa'),
							5 => array('name' => 'Thổ', 'rewrite' => 'tho'),
							);
$arrMenuPrice = array(1 => array('name' => 'Dưới 200.000 đ', 'rewrite' => '/gia-0-200000/'),
								2 => array('name' => '2 trăm -&gt; 5 trăm', 'rewrite' => '/gia-200000-500000/'),
								3 => array('name' => '5 trăm -&gt; 1 triệu', 'rewrite' => '/gia-500000-1000000/'),
								4 => array('name' => '1 triệu -&gt; 3 triệu', 'rewrite' => '/gia-1000000-3000000/'),
								5 => array('name' => '3 triệu -&gt; 5 triệu', 'rewrite' => '/gia-3000000-5000000/'),
								6 => array('name' => '5 triệu -&gt; 10 triệu', 'rewrite' => '/gia-5000000-10000000/'),
								7 => array('name' => '10 triệu -&gt; 50 triệu', 'rewrite' => '/gia-10000000-50000000/'),
								8 => array('name' => 'Trên 50 triệu', 'rewrite' => '/gia-50000000-0/'),																						
								);
?>
<amp-sidebar id='drawermenu' layout="nodisplay">
	<amp-img class='close' src="/lib/img_amp/ic_chevron_left_black_24dp_2x.png" width="32" height="32" alt="Close" on="tap:drawermenu.close" role="button" tabindex="0"></amp-img>
	<div class="topheader">
		<a href="/" class="home">
			<amp-img src="/lib/img/logo.png" width="200" height="45" alt="Siêu Thị Sim Thẻ Logo"></amp-img>
		</a>
	</div>
	<amp-accordion>
		<section>
			<h4>
				<div class="show-more category">Trình đơn</div>
				<div class="show-less category expanded">Trình đơn</div>
			</h4>
			<div>
				<div class="item"><a href="/sim-phong-thuy.html">Sim phong thủy</a></div>
				<div class="item"><a href="/huong-dan-chung.htm">Hướng dẫn chung</a></div>
				<div class="item"><a href="/thanh-toan-qua-ngan-hang.htm">Hướng dẫn thanh toán</a></div>
				<div class="item"><a href="/dat-sim-theo-yeu-cau.html" title="Đặt theo yêu cầu">Đặt sim theo yêu cầu</a></div>
				<div class="item"><a href="/mua-ban-sim-so-dep.html" title="Mua bán sim số đẹp">Mua bán sim số đẹp</a></div>
				<div class="item"><a href="/dang-ky-tra-truoc.html" title="Đăng ký sim trả trước">Đăng ký sim trả trước</a></div>
				<div class="item"><a href="/bao-gia-sim-the.html" title="Báo giá sim thẻ">Báo giá sim thẻ</a></div>
				<div class="item"><a href="/575-giai-phong.html" title="Sim giá rẻ Viettel">Liên hệ</a></div>
			</div>
		</section>

		<section>
			<h4>
				<div class="show-more category">Sim số đẹp</div>
				<div class="show-less category expanded">Sim số đẹp</div>
			</h4>
			<div>
				<? foreach($arrMenuCat as $key => $value){?>
				<div class="item"><a href="/<?=$value['rewrite']?>/" title="Sim số đẹp <?=$value['name']?>"><?=$value['name']?></a></div>
				<? }?>
			</div>
		</section>
		
		<section>
			<h4>
				<div class="show-more category">Sim giá rẻ</div>
				<div class="show-less category expanded">Sim giá rẻ</div>
			</h4>
			<div>
				<? foreach($arrMenuCat as $key => $value){?>
				<div class="item"><a href="/sim-gia-re-<?=$value['rewrite']?>/" title="Sim giá rẻ <?=$value['name']?>"><?=$value['name']?></a></div>
				<? }?>
			</div>
		</section>
		
		<section>
			<h4>
				<div class="show-more category">Sim năm sinh</div>
				<div class="show-less category expanded">Sim năm sinh</div>
			</h4>
			<div>
				<? for($i=1955; $i<=1999 ; $i++){?>
				<div class="item"><a href="/Sim-nam-sinh-<?=$i?>/" title="Sim năm sinh <?=$i?>">Sim năm sinh <?=$i?></a></div>
				<? } // End for($i=1960; $i<=1974 ; $i++)?>
			</div>
		</section>
		
		<section>
			<h4>
				<div class="show-more category">Sim hợp tuổi</div>
				<div class="show-less category expanded">Sim hợp tuổi</div>
			</h4>
			<div>
				<? for($i=1950;$i<=2009;$i++){ ?>
				<div class="item"><a href="/sim-phong-thuy-hop-tuoi-<?=$i?>/" title="Sim hợp tuổi <?=$i?>">Sim hợp tuổi <?=$i?></a></div>
				<? }?>
			</div>
		</section>
		
		<section>
			<h4>
				<div class="show-more category">Sim hợp mệnh</div>
				<div class="show-less category expanded">Sim hợp mệnh</div>
			</h4>
			<div>
				<? foreach($arrMenuMenh as $key => $value){?>
				<div class="item"><a href="/sim-phong-thuy-hop-menh-<?=$value['rewrite']?>/" title="Sim hợp mệnh <?=$value['name']?>">Sim hợp mệnh <?=$value['name']?></a></div>
				<? }?>
			</div>
		</section>
		
		<section>
			<h4>
				<div class="show-more category">Chọn sim theo loại</div>
				<div class="show-less category expanded">Chọn sim theo loại</div>
			</h4>
			<div><?=_temp::_simType();?></div>
		</section>
		
		<section>
			<h4>
				<div class="show-more category">Chọn sim theo giá</div>
				<div class="show-less category expanded">Chọn sim theo giá</div>
			</h4>
			<div>
				<? foreach($arrMenuPrice as $key => $value){?>
				  <div class="item"><a href="<?=$value['rewrite']?>" title="<?=$value['name']?>"><?=$value['name']?></a></div>
				<? }?>
			</div>
		</section>
		
		<section>
			<h4>
				<div class="show-more category">Chọn sim theo đầu số</div>
				<div class="show-less category expanded">Chọn sim theo đầu số</div>
			</h4>
			<div><?=_temp::_simDauSo();?></div>
		</section>
	</amp-accordion>
</amp-sidebar>