<style>
.type-phong-thuy .header {
	position:relative;
	background:#FFF url(/lib/img/phong-thuy/bgr-header.jpg) no-repeat top right;
	width:99.9%;
    height: 95px;
	z-index: 1;
	overflow: hidden;
	border-top: solid 3px #ddd;
}
.type-phong-thuy .header .logo{
	position:absolute;
	left: -120px;
	top: -65px;
	background: transparent;
}
.type-phong-thuy .header .slogan{
	position:absolute;
	left: 145px;
	top: 15px;
}
.type-phong-thuy .nav{
	background:#0082C8;
}
.type-phong-thuy .tab-content{
	background:url(../../lib/img/phongthuy/sim/bg1.jpg);	
}
.type-phong-thuy .body-tool, .type-phong-thuy .share {
	background:url(../../lib/img/phong-thuy/bgr-main.jpg);
}
.type-phong-thuy .body-tool{
	border-bottom: solid 8px #0082C8;	
}
.type-phong-thuy .share{
	background:url(../../lib/img/phong-thuy/bgr-main.jpg);
	padding:15px 50px; 
}
.type-phong-thuy .main-phong-thuy{
	padding:0 5px;
}
.type-phong-thuy h3{
	margin:10px 0 0 0;
}
</style>
<div class="type-phong-thuy">
    <div class="header">
        <div class="logo">
            <img src="/lib/img/phong-thuy/logo.gif" alt="Logo Phong Thủy" />
        </div>
        <div class="slogan">
            <img src="/lib/img/phong-thuy/slogan.png" alt="Slogan Phong Thủy" style="width: 230px;" />
        </div>
    </div>

	<div class="tab-pane fade in active" id="tra-cuu">
	   <?
	   include('inc_type_phongthuy_tool.php');
	   ?>
	</div>
	<div class="tab-pane fade" id="gioi-thieu">
	   <?
	   include('inc_type_phongthuy_intro.php');
	   ?>
	</div>
</div>