<header>
<table cellpadding="0" cellspacing="0" border="0" width="100%">
	<tr>
		<td width="55" align="center" class="td-left">
			<a id="hamburger" class="button icon icon-align-justify" on='tap:drawermenu.toggle'>
				<amp-img srcset="https://ampbyexample.com/img/ic_menu_white_1x_web_24dp.png 1x, https://ampbyexample.com/img/ic_menu_white_2x_web_24dp.png 2x" width="24" height="24" alt="navigation"></amp-img>
			</a>
		</td>
      <td>
      	<form method="GET" action="/home/vn/type.php" target="_top" novalidate="" class="-amp-form">
      		<input type="hidden" name="type" value="sim" />
				<input type="hidden" name="iCat" value="<?=$iCat?>" />
				<input type="hidden" name="iType" value="<?=$iType?>" />
				<input type="hidden" name="iPrice" value="<?=$iPrice?>" />
				<input type="hidden" name="iDauso" value="<?=$iDauso?>" />
				<input type="hidden" name="iNguHanh" value="<?=$iNguHanh?>" />
				<input type="hidden" name="iNamSinh" value="<?=$iNamSinh?>" />
				<table class="search" cellpadding="0" cellspacing="0">
					<tr>
						<td valign="top">
							<input type="search" id="searchText" name="keyword" class="form-control searchText" value="<?=$keyword?>" placeholder="Tôi muốn tìm" data-placement="bottom" title="Hướng dẫn tìm kiếm" />
						</td>
						<td>
							<input type="submit" value="Tìm" class="button button-primary other-input" style="line-height: 28px; height: 28px; margin-top: 2px;" />	
						</td>
					</tr>
				</table>
			</form>
      </td>
  </tr>
</table>
</header>