<?
$ErrorCode	= "";

#+
#+ Khai bao bien
$name			= getValue("name", "str", "POST", "");
$address		= getValue("address", "str", "POST", "");
$email			= getValue("email", "str", "POST", "");
$title			= getValue("title", "str", "POST", "");
//$phone			= getValue("phone", "str", "POST", "");
$fax			= getValue("fax", "str", "POST", "");
$content		= getValue("content", "str", "POST", "");
$scode			= getValue("scode", "str", "POST", "");
$cscode			= getValue("cscode","str","POST","");
$action 		= getValue("action", "str", "POST", "");
if($action == "contact"){
	if(!isset($_SESSION["session_security_code"])) redirect("/",1);
	
	if($scode == $_SESSION["session_security_code"]){
		$message = $title . "<br>";
		$message = "You have new contact from: " . $email . "<br>";
		$message.= "Tên :" . $name . "<br>";
		//$message.= "Phone    : " . $phone . "<br>";
		$message.= "Nội dung  : <br>" . $content . "";
		
		$to      = $con_admin_email;
		$subject = "hello " . $name . " contact to wwww."  . $_SERVER['SERVER_NAME'];	
		$headers  = 'MIME-Version: 1.0' . "\r\n";
		$headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";
		// Additional headers
		$headers .= 'To: Admin <' . $to . '>' . "\r\n";
		$headers .= 'From: ' . $name . ' <' . $email . '>' . "\r\n";
		$headers .= 'Cc: '. $to . '' . "\r\n";
		
		mail($to, $subject, $message, $headers);
		$_SESSION["session_security_code"] = rand(1000,9999);
		//return true;
		echo "<script language='javascript'>alert('Gửi thành công')</script>";
		redirect("/");
		exit();
	}
	else{
		$ErrorCode = "Mã an toàn không chính xác";
	}
}
?>
<div class="breadcrumb"><?=$home_address?></div>
<article>

	<div class="text-center">
		<b>Vui lòng điền đầy đủ thông tin, chúng tôi sẽ liên hệ với bạn trong thời gian sớm nhất !</b>
	</div>
    
	<form action="" method="post" name="contact"  id="myform" class="formsentmail">
		<input type="hidden" name="action" value="contact" />
		
		Tên của bạn:*<br />
		<input type="text" name="name" id="name_contact" size="30" class="form-control" value="<?=$name?>" />
		
		Email:*<br />
		<input type="text" name="email" id="email_contact" size="30" class="form-control" value="<?=$email?>" />
		
		Tiêu đề:*<br />
		<input type="text" name="title" id="title_contact" size="30" class="form-control" value="<?=$title?>" />

		Nội dung:*<br />
		<textarea name="content" id="content_contact" class="form-control" cols="50" rows="8"><?=$content?></textarea>
		
		Mã xác nhận:*<br />
		<? $_SESSION["session_security_code"] = rand(1000,9999);?>
		<input type="text" name="scode" id="scode" size="5" maxlength="5" value="" class="form-control" />
		&nbsp;&nbsp;
		<img src="/home/ext/securitycode.php" alt="Mã xác nhận" />
		<?=$ErrorCode;?>
            
  		<p>
			<button class="button button-primary other-input" onclick="check_contact()">Gửi đi</button> &nbsp;				
			<button class="button button-primary other-input">Làm lại</button>
		</p>
	</form>	
</article>